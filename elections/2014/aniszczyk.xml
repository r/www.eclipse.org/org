<boardmember id="aniszczyk" type="committer">

	<name>Chris Aniszczyk</name>

	<title>Engineering Manager, Open Source (Twitter)</title>

	<image>aniszczyk.jpg</image>

	<email>zx@twitter.com</email>

	<eclipse_affiliation> <![CDATA[		
        <a href="http://www.eclipse.org/technology/team-leaders.php">Technology</a> and Mylyn PMC Member<br>
        <a href="http://www.eclipse.org/egit">EGit</a> / JGit Committer<br>
        <a href="http://www.eclipse.org/orbit">Orbit</a> Committer<br>               
        <a href="http://planet.eclipse.org">PlanetEclipse.org</a> Maintainer<br> 
        <a href="http://www.eclipsecon.org">EclipseCon</a> Program Committee (2007-2012)<br> 
        Eclipse Board Committer Representative (2007-2013)
	]]> </eclipse_affiliation>

	<vision>  <![CDATA[
		<p>The 10th year anniversary of the Eclipse Foundation has filled me with joy and reflection. I've been grateful to be a part of the Eclipse community for a good portion of that time and honored to serve the committers on the Board for many years. We truly have a unique open source community at Eclipse that is comprised of a variety of parties, from individuals to academics to companies (spanning many industries if you take a look at the <a href="http://www.eclipse.org/org/workinggroups/">Eclipse Working Groups</a>). We need to keep all parties in mind, whether an individual or corporation, and make it easier for everyone to be successful at Eclipse.
                </p>
		
		<p>Since Eclipse has been around for over a decade, we have to keep moving forward to stay relevant. In the last couple years I've helped initiate our infrastructure by moving to 
		<a href="http://mmilinkov.wordpress.com/2012/12/21/eclipse-says-goodbye-to-cvs/">Git/Gerrit</a>, push the Board to <a href="http://mmilinkov.wordpress.com/2013/06/20/embracing-social-coding-at-eclipse/">embrace social coding at the Foundation</a> as an option for development (see <a href="http://www.infoq.com/news/2013/01/vertx-eclipse">Vertx as a case study</a>) and helped push for a simplified the IP process with <a href="http://mmilinkov.wordpress.com/2013/06/17/eclipse-clas-are-live/">CLAs</a>.
		
		However, there are more things we can do to move things forward. <b>My main focus will be to continue to push the social coding initiative while maintaining our reputation for a quality development process.</b> It would also make the the Eclipse Foundation 
		more adaptable to infrastructure changes in the future, further hone our development process and ensure that we're ready for 
		another successful decade of changing the industry. I would also like for us to <b>step back and evaluate our development process to see how we can improve it</b>, by either removing parts of the process or through automation (e.g., could a special type of git-tag initiate a release review).</p>

		<p>To ensure Eclipse continues to grow and thrive I will particularly focus on:</p>
		
		<ul>
			<li>Continue to mentor and seek new projects at the Eclipse Foundation</li>
			<li>Push the foundation to invest more in <a href="http://www.eclipse.org/org/workinggroups/">Working Groups</a></li>
			<li>Encourage more projects to adopt the social coding initiative</li>
			<li>Push the Eclipse Foundation update and modernize the website</li>
			<li>Push the Eclipse Foundation to hire at least one full-time technical evangelist</li>
			<li>Ensure Eclipse processes are transparent and lightweight as possible</li>
		</ul>
		
		<p>On the whole, my goal has always been to make Eclipse thrive and be transparent as possible about the board's 
		activities. I love bringing new projects to the Eclipse ecosystem and mentoring new committers. I will pay 
		special attention to how we can make the Eclipse development process easier for smaller projects. In the end, 
		I would be honored to represent you, the committers, for another term.</p>		
		]]> </vision>

	<bio> <![CDATA[
		<p>
		Chris Aniszczyk (<a href="http://twitter.com/cra">@cra</a>) is a software architect by trade with a 
		passion for open source and building communities. At Twitter, he's responsible for 
		creating their open source program and managing their open source efforts. He also has the honor 
		to sit on the Eclipse Architecture and Planning councils. In a previous life, he traveled the world and 
		consulted companies on Eclipse technology. He also lead and hacked on many eclipse.org and linux related 
		projects. In his spare time, you'll find him running, cycling or doing yoga.</p>		
	]]> </bio>

	<affiliation> <![CDATA[ 
		Twitter
	]]>
	</affiliation>
		
</boardmember>