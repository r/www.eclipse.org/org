<?php
/**
 * Copyright (c) 2005-2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

include ("scripts/candidate.php");
$year = "2024";
$candidates = get_all_candidates($year);

$pageTitle = "$year Board Candidates";

$committer_candidates = get_candidates_list_as_html($candidates, $year, 'committer');
$addin_candidates = get_candidates_list_as_html($candidates, $year, 'addin');

$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("foundation, governance, board, elections");
$Theme->setPageAuthor("Mike Milinkovich & Wayne Beaton");

ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();

function get_candidates_list_as_html(&$candidates, $year, $type) {
  $type_name = strcmp($type, 'committer') == 0 ? 'Committer' : 'Contributing Member';
  $html = "";

  if (count($candidates) == 0) {
    $html .= "There are no candidates at this time.";
  }
  else {
    foreach ($candidates as $candidate) {
      if (strcmp($candidate->type, $type) != 0)
        continue;
      $html .= <<<EOHTML
        <div class="clearfix margin-bottom-15 padding-bottom-10"  style="border-bottom: dashed 1px #494949;">
          <div class="col-xs-6 col-sm-8"><a href="candidate.php?year=$year&id=$candidate->id"><img class="candidate-list-item-img thumbnail" src="$candidate->image"></a></div>
          <div class="col-xs-18 col-sm-16" >
              <strong><a href="candidate.php?year=$year&id=$candidate->id">$candidate->name</a></strong>
              <br>$candidate->title
          </div>
      </div>
EOHTML;
    }
  }

  return $html;
}
