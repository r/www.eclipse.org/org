<?php
/**
 * Copyright (c) 2005-2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

include ("scripts/candidate.php");
// $id = $_GET['id'];
$year = "2024";
$sustaining = "three";
$committer = "three";
$candidates = get_all_candidates($year);
$committer_candidates = get_candidates_list_as_html($candidates, $year, 'committer');
$addin_candidates = get_candidates_list_as_html($candidates, $year, 'addin');

//
// Begin: page-specific settings. Change these.
$pageTitle = "Eclipse Foundation Elections";

$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("foundation, governance, board, elections");
$Theme->setPageAuthor("Mike Milinkovich & Wayne Beaton");

ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();

function get_candidates_list_as_html(&$candidates, $year, $type) {
  $type_name = strcmp($type, 'committer') == 0 ? 'Committer' : 'Contributing Member';
  $html = "<h3>$type_name Candidates</h3><table border=\"0\" cellpadding=\"5\">";
  foreach ($candidates as $candidate) {
    if (strcmp($candidate->type, $type) != 0) {
      continue;
    }
    $html .= <<<EOHTML
        <tr>
          <td valign="top"><a href="candidate.php?year=$year&id=$candidate->id"><img src="$candidate->image" width="75"></a></td>
          <td valign="top" style="border-bottom: dashed 1px #494949;">
            <strong><a href="candidate.php?year=$year&id=$candidate->id">$candidate->name</a></strong>
            <br>$candidate->title
            <p>$candidate->contact</p>
          </td>
        </tr>
EOHTML;
  }
  $html .= "</table>";

  return $html;
}
