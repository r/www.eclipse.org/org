<boardmember id="carrer" type="addin">

	<name> Marco Carrer </name>

	<title> CTO, Eurotech </title>

	<image>carrer.jpg</image>

	<email> marco.carrer at eurotech.com </email>

	<eclipse_affiliation> <![CDATA[
		Eclipse Kura, Project Lead<br>
		Eclipse Paho, Contributor<br>
		EclipseCon 2014 Program Committee	
	]]> </eclipse_affiliation>

	<vision>  <![CDATA[
<p>The Eclipse Foundation is way beyond the Eclipse IDE. It is a vibrant community of small and large software vendors, who understand the pivotal importance of open source in modern software projects. Over the course the years, the Eclipse Foundation has become the precious guardian for high quality and IP-clean open source software, which provides a decisive acceleration factor for companies going to market with new products or solutions.</p>

<p>The Eclipse Solutions Member membership has been essential to the Eclipse’s success so far as it is the first type of membership that organizations will normally subscribe with Eclipse. Solutions Member organizations are interested in starting new projects in Eclipse and/or leveraging existing Eclipse projects to develop new commercial products or services.</p>

<p>If elected, I will focus on fostering the adoption of Eclipse projects by existing and prospective Solutions Members through the following activities.</p>

<ol>
<li>
<b>Advocate more Challenges around Eclipse Projects</b> <br>
Promote the organization of community Challenges aimed at highlighting applications and solutions based on one or more Eclipse projects. 
<br>
Eclipse’s role in the Challenge is to recruit sponsors for the final prizes, give visibility to the challenge in the social media channels, and select the winners. As subject matter experts, Eclipse’s project leaders will be highly involved in the definition of the Challenge and the selection of the winners. 
<br>
The requirements for the community to participate include: use of one or more Eclipse projects; make the resulting code available under EPL; detail documentation of the solution; feedback on the project(s) used – e.g. what worked, what can be improved.
<br>
Recently, Eclipse organized a Challenge around the IoT theme and it was a huge success. There were more than 50 submissions and 10 exciting finalists. As project leader for Eclipse Kura, I was impressed by the creativity of the solutions proposed, and the quality of the documentation of some submissions. What I found particularly interesting were the comments on the technology; they provided precious feedback on how to improve our project going forward. 
<br>
I believe this is the kind of activity and material that can instill confidence in other organizations to adopt Eclipse’s code in their projects.
</li>

<li>
<b>Further promote existing Eclipse Projects</b><br>
Eclipse is now the repository of a very large number of projects. At first glance, the Eclipse project list can be overwhelming and wrongly perceived as a fragmented collection of independent projects. Such perception may be detrimental to the adoption of Eclipse technologies by prospective Solutions Members.
<br>
Similarly to the Apple App Store, it would be helpful for Eclipse to provide “curatorial” comments and further categorization of its projects to facilitate prospective organizations in finding the right match for their needs. For example, the Modeling category, one of the richest groups, has more than 60 projects spanning from low-level modeling frameworks to high-level visualization tools. It is hard for prospective users to identify where to start from.
<br>
The Eclipse IoT Working Group has tried to address this issue by creating a landing page for all Eclipse projects related to IoT. The page is useful to get the big picture and it guides the visitor in the exploration of each project.
<br>
</li>

<li>
<b>Foster more project collaboration among Eclipse Projects</b><br>
Finally, I believe it is extremely valuable to foster and encourage the integration of multiple Eclipse projects into added value solutions. Integrated solutions remove the burden of assembling “certified” software stacks from the prospective adopter, which is often one of the biggest barriers.
<br>
Fostering a successful collaboration among the projects can be achieved though
<ul>
<li>Clear documentation and visibility of the API docs</li>
<li>Release alignments</li>
<li>Documentation of dependencies and software stack requirements</li>
<li>Actively seek collaboration across different Eclipse projects</li>
<li>Loosely coupled software engineering principle</li>
</ul>
<br>
Working on finding a way to foster these principles across the Eclipse projects and their leaders would greatly benefit the existing Eclipse community and prospective Solutions Members.
	]]> </vision>


	<bio> <![CDATA[
<p>Marco Carrer is CTO of EUROTECH where he is leading the company towards the enablement of Internet of Things solutions and applications. Before, Marco had a successful 15-year career in the software industry. He started as an engineer at Sony where he contributed to a pioneering video streaming solution. Later, he joined Oracle in the Server Technologies group where, over the span of 13-year career, he became Senior Director and was directly responsible for the design and development of several innovative Oracle products in the areas of Web Services, Enterprise Collaboration, and CRM Service. Throughout his career, Marco loved collaborating on open source projects and he is currently project lead for  Eclipse Kura, a Java framework for IoT gateways. Marco Carrer holds a Laurea in Electronic Engineering from University of Padova and a Master in Computer Science from Cornell University, New York. He is also a certified SCRUM Master and has been awarded ten US patents.</p>

		
	]]> </bio>

	<affiliation> <![CDATA[ 
		
	CTO, Eurotech
	]]>
	</affiliation>

		
</boardmember>

