co<?php
/**
 * Copyright (c) 2005-2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

include ("scripts/candidate.php");
$id = preg_replace("/[^a-zA-Z0-9]+/", "", $_GET['id']);
$year = "2017";
$candidate = get_candidate($id, $year);
$candidates_summary = get_candidates_short_summary_as_html($year);

$pageTitle = "$year Elected Board Representitive: $candidate->name";
$pageKeywords = "";
$pageAuthor = "";

$type_name = strcmp($candidate->type, 'committer') == 0 ? 'Committer Member' : 'Contributing Member';

$Theme->setPageTitle($pageTitle);

ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();