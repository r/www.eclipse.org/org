<?php

/*******************************************************************************
 * Copyright (c) 2015, 2023, 2024 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eclipse Foundation - initial implementation
 *    Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *******************************************************************************/
?>

<style>
  .candidate-list-item-img {
    width: 8rem;
    height: 8rem;
    object-fit: cover;
  }
</style>

<div id="midcolumn">
  <h1><?php print $year; ?> Board Candidates</h1>
  <p>
    We are pleased to announce the results of the 2024 Eclipse Foundation
    Contributing Member and Committer Member elections for representatives to
    our Board.
  </p>
  <p>
    Please see the announcement <a href="https://newsroom.eclipse.org/news/community-news/2024-eclipse-foundation-board-election-results">here</a>.
  </p>
  <p>
    We would like to thank everyone who participated in the 2024 Elections
    process.
  </p>

  <!--
  <div class="row margin-bottom-40">
    <div class="col-md-12">
      <h3>Committer Candidates</h3>
      <a name="Candidates"></a><?php print $committer_candidates; ?>
    </div>
    <div class="col-md-12">
      <h3>Contributing Member Candidates</h3>
      <?php print $addin_candidates; ?>
    </div>
  </div>
  -->

  <!--<p>See the Quick Links here below for more details.</p>-->

  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Quick Links</h6>
      <ul>
        <li><a href="/org/elections">Election Home</a></li>
        <li><a href="/org/elections/nominees.php"><?php print $year; ?> Candidates</a></li>
        <li><a href="/org/elections/keydates.php"><?php print $year; ?> Key Dates</a></li>
        <li><a href="/org/elections/election_process.php">Election Process</a></li>
      </ul>
    </div>
  </div>
</div>
