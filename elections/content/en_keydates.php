<?php

/**
 * Copyright (c) 2005-2015, 2018, 2022, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>Each year, the Eclipse Foundation holds elections for board members representing two very
    important groups within the Eclipse membership: the Committer Members and the Contributing Members.
    The terms of office for these elected board members
    is one year, commencing April 1.</p>
  <div class="homeitem3col">
    <h3>
      <a name="Dates"></a><?php print $year ?> Key Dates
    </h3>

    <ul>
      <li>
        January 3, 2024: Nominations open. To nominate someone, send an email to
        <a href="mailto:elections@eclipse.org">elections@eclipse.org</a>
      </li>
      <li>
        January 31, 2024: Nominations close at 21h CET / 3 pm EST
      </li>
      <li>
        February 5, 2024: List of candidates published on <a href="/org/elections/nominees.php">2024 Board Candidates</a> 
        page
      </li>
      <li>
        February 5, 2024: Deadline for candidates to return their content to
        <a href="mailto:elections@eclipse.org">elections@eclipse.org</a>
      </li>
      <li>
        February 8, 2024: Deadline for candidates to review their individual candidate page
      </li>
      <li>
        February 9, 2024: List of candidates and links to individual candidate
        pages made available on <a href="/org/elections/nominees.php">2024 Board Candidates</a> 
        page
      </li>
      <li>
        February 12, 2024: Voting begins. Note: Eligible voters receive a notice to vote and credentials by email
      </li>
      <li>
        March 4, 2024: Voting ends at 21h CET / 3 pm EST
      </li>
      <li>
        March 6, 2024: New representatives announced via Community Bulletin
      </li>
    </ul>
  </div>
</div>
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Quick Links</h6>
    <ul>
      <li><a href="/org/elections">Election Home</a></li>
      <li><a href="/org/elections/nominees.php"><?php print $year; ?> Candidates</a></li>
      <li><a href="/org/elections/keydates.php"><?php print $year; ?> Key Dates</a></li>
      <li><a href="/org/elections/election_process.php">Election Process</a></li>
    </ul>
  </div>
</div>
