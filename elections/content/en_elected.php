<?php
/**
 * Copyright (c) 2005-2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <img src="<?php print $candidate->image; ?>" width="150" align="right">
  <h1><?php print $pageTitle; ?></h1>
  <p><?php print $candidate->title; ?></p>
  <em>Elected <?php print $type_name; ?> representative.</em>
  <p><?php print $candidate->eclipse_affiliation; ?></p>
  <table border="0">
    <tr valign="top">
      <td>e-mail:</td>
      <td><a href="mailto:$candidate->email"><?php print $candidate->email; ?></a></td>
    </tr>
    <tr valign="top">
      <td>Phone:</td>
      <td><?php print $candidate->phone; ?></td>
    </tr>
    <tr valign="top">
      <td>Contact:</td>
      <td><?php print $candidate->contact; ?></td>
    </tr>
  </table>
  <div class="homeitem3col">
    <h3>About the Representive</h3>
    <blockquote><?php print $candidate->bio; ?></blockquote>
  </div>
  <div class="homeitem3col">
    <h3>Vision</h3>
    <blockquote><?php print $candidate->vision; ?></blockquote>
  </div>
  <div class="homeitem3col">
    <h3>Affiliation</h3>
    <blockquote><?php print $candidate->affiliation; ?></blockquote>
  </div>
</div>
