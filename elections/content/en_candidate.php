<?php
/**
 * Copyright (c) 2013, 2018, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *   Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<style>
  .candidate-content .candidate-content-img {
    float: right;
    width: 15rem;
    height: 15rem;
    margin-left: 1.25em;
    margin-bottom: 1.25em;
    object-fit: cover;
  }
</style>

<div id="midcolumn" class="candidate-content">
  <img class="candidate-content-img" src="<?php print $candidate->image; ?>">
  <h1><?php print $pageTitle; ?></h1>
  <p><?php print $candidate->title?></p>
  <p>
    <em>Nominee for <?php print $type_name; ?> representative</em>
  </p>
  <p><?php print $candidate->eclipse_affiliation; ?></p>
  <table border="0">
    <tr valign="top">
      <td>email:&emsp;</td>
      <td><?php print $candidate->email; ?></td>
    </tr>
  </table>
  <div class="homeitem3col">
    <h3>Vision</h3>
    <blockquote><?php print $candidate->vision; ?></blockquote>
  </div>
  <div class="homeitem3col">
    <h3>About the Candidate</h3>
    <blockquote><?php print $candidate->bio; ?></blockquote>
  </div>
  <div class="homeitem3col">
    <h3>Affiliation</h3>
    <blockquote><?php print $candidate->affiliation; ?></blockquote>
  </div>
</div>
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Candidates</h6>
      <?php print $candidates_summary; ?>
    </div>
</div>
