<?php

/**
 * Copyright (c) 2005-2015, 2018, 2022, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *   Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>The Eclipse Foundation holds elections for board members representing two very important groups
    within the Eclipse membership: the Committer Members and the Contributing Members. The terms of office for these elected board members is one year, commencing
    April&nbsp;1.</p>
  <p>
    Candidates and the community are encouraged to discuss the issues on the <a href="/forums/eclipse.foundation">Foundation forum</a>.
  </p>
  <h3>Duties and Responsibilities of an Eclipse Foundation Director</h3>
  <ul>
    <li>Directors have a fiduciary responsibility to the Eclipse Foundation. In brief, this means
      that they must base their decisions on the needs of the Eclipse Foundation before those of
      either themselves or their employer.</li>
    <li>In terms of time requirements, the Board meets once a month for a one-hour
      conference call. It also meets face-to-face three times per year, typically in
      April, June, and October.</li>
    <li>Expenses incurred by Directors who are employed by a Member Company are to be covered by the
      Member company. Directors who are self-employed or employed by a non-Member may have their
      expenses covered by the Eclipse Foundation.</li>
  </ul>
  <div class="homeitem3col">
    <h3><?php print $year; ?> Elections </h3>
    <p>
      We are pleased to announce the results of the 2024 Eclipse Foundation
      Contributing Member and Committer Member elections for representatives to
      our Board.
    </p>
    <p>
      Please see the announcement <a href="https://newsroom.eclipse.org/news/community-news/2024-eclipse-foundation-board-election-results">here</a>.
    </p>
    <p>
      We would like to thank everyone who participated in the 2024 Elections
      process.
    </p>
    <p>
      See the Quick Links here below for more details.
    </p>

    <!--
      <p>Voting has ended in the 2020 elections. To learn about the results, 
        <a href="https://www.eclipse.org/org/press-release/20200319-boardelection.php" target="_blank">see this announcement.</a>
      </p>
    -->

  </div>
</div>

<div id="rightcolumn">
  <div class="sideitem">
    <h6>Quick Links</h6>
    <ul>
      <li><a href="/org/elections">Election Home</a></li>
      <li><a href="/org/elections/nominees.php"><?php print $year; ?> Candidates</a></li>
      <li><a href="/org/elections/keydates.php"><?php print $year; ?> Key Dates</a></li>
      <li><a href="/org/elections/election_process.php">Election Process</a></li>
    </ul>
  </div>
</div>
