<?php
/**
 * Copyright (c) 2005-2015, 2018, 2022, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <div class="homeitem3col">
    <h2>Election Process</h2>
    <p>The process for the Eclipse Foundation&rsquo;s annual elections is described below.</p>
    <ul>
      <li>Each year, the Eclipse Foundation holds elections for board members representing two very
        important groups within the Eclipse membership: the Committer Members and the Contributing Members.
        The number of these elected representatives is a ratio of the total number of Strategic
        Members.</li>
      <li>
        In 2024, we expect to elect three Committer Member representatives and
        three Contributing Member representatives.
      </li>
      <li>These elections are held pursuant to Articles 8.2 and 9.2 of the Eclipse Foundation <a
        href="../documents/eclipse_foundation-bylaws.pdf"
      >Bylaws</a>.
      </li>
      <li>The term of office for these elected board members is one year, commencing April 1.</li>
      <li>Voting for the elected board members is done using <a
        href="http://en.wikipedia.org/wiki/Single_Transferable_Vote"
      >single transferrable voting</a> and by secret ballot.
      </li>
      <li>Each Contributing Member gets one vote.</li>
      <li>Each Committer Member gets one vote. Note that Committers who are employees of Member
        companies have all the rights and privileges (including voting) of a Committer Member.
        Individual Committers must join the Eclipse Foundation as Committer Members by signing the <a
        href="/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202008_04_16%20Final.pdf"
      >Membership Agreement</a> in order to be allowed to vote.
      </li>
    </ul>
    
    <h3>How the voting works:</h3>
    <ul>
      <li>
        Eclipse Foundation Board elections are performed using a process known as single transferrable vote, which requires each voter to complete a ranked ballot. By ranked ballot, we mean that each voter ranks the candidates in their order of preference.
      </li>
      <li>
        The advantage of this approach is being able to elect multiple candidates at the same time which reflects the voters&rsquo; preferences.
      </li>
      <li>
        The Eclipse Foundation uses a third-party election system<sup><a href="#footnote1">[1]</a></sup> that performs the election calculations. 
        Elections are performed with high integrity where a unique link is created with a random and secret access key for each voter. 
        Additionally, voter anonymity uses secret ballots ensuring voters' choices cannot be linked to voters.
        Voters must rank all candidates and cannot award two candidates the same ranking.
      </li>
      <li>
        If you are a standing candidate in an election, please know you can certainly vote for yourself! In fact, it is quite normal to rank yourself (or the organization you are voting on behalf of) as your #1 (highest) selection.
      </li>
      <li>
        Ballots will be distributed by email on February 12, 2024, and the
        voting period will remain open through March 4, 2024.
      </li>
    </ul>

    <div class="margin-bottom-40">
      <hr class="margin-left-0" width="200">
      <ul class="list-unstyled">
        <li>[1] <a id="footnote1" href="https://electionbuddy.com">Election Buddy</a></li>
      </ul>
    </div>

  </div>
</div>
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Quick Links</h6>
    <ul>
      <li><a href="/org/elections">Election Home</a></li>
      <li><a href="/org/elections/nominees.php">2024 Candidates</a></li>
      <li><a href="/org/elections/keydates.php">2024 Key Dates</a></li>
      <li><a href="/org/elections/election_process.php">Election Process</a></li>
    </ul>
  </div>
</div>
