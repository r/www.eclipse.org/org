<!-- id is the same as the file name without the extension
     type is one of "committer" or "add-in-provider" -->
     
<boardmember id="merks" type="committer">

	<name>Ed Merks</name>

	<title>Director, macromodeling GmbH</title>

	<image>merks.jpg</image>

	<email>Ed.Merks@gmail.com</email>
	
	<phone>+41 78 400 26 49</phone>

	<eclipse_affiliation> <![CDATA[	
	<p>	
		BIRT<br>
		DataTools Committer<br>
		Eclipse Modeling Project Lead<br>
		Eclipse Modeling Framework Project Lead<br>
		EPP Committer<br>
		Equinox Committer<br>
		GEF Committer<br>
		JustJ Project Lead<br>
		Mylyn Committer<br>
		Oomph Committer<br>
		Orbit Committer<br>
		PDE Committer<br>
		Platform PMC Member<br>
		Platform Committer<br>
		SimRel Project Lead<br>
		WindowBuilder Committer
	</p>	
	]]> </eclipse_affiliation>
	
	<vision>  <![CDATA[
	
<p>
What is Eclipse?
That was an easy question to answer back in the early days: 
Eclipse is a highly-extensible, desktop IDE framework implemented in Java using native widgets.
Clearly Eclipse has evolved and diversified.
The Eclipse Foundation was formed to manage this technology, but it too has evolved and diversified dramatically and continues to do so.
Now the answer to this simple question is far from clear.
Is it a Foundation, a Project, a Community, a Java IDE, or Europe's largest open source organization?
It is of course all those things, but not merely these things.
For me personally, Eclipse is the world's biggest and best open-source Modeling community.
But in the end, for each of us committers, Eclipse is something quite different, 
and we each personally help to make Eclipse what we collectively want it to be and to become.
</p>

<p>
This election is about how you as a committer are represented on the Eclipse Foundation's Board of Directors.
The board is relatively small, so each and every representative has significant influence.
As your committer representative on the board, I will employ my seventeen years of experience to promote the interests of committers,
and to help the Eclipse Foundation evolve into something even greater.
The Eclipse Foundation needs to continue its growth beyond its early role as the manager of the Eclipse Project, 
while not losing sight of this core technology.
My role is not only to represent committers, but to ensure the Eclipse Foundation's success, growth, and long-term viability.
I will strive to be a visible and approachable technical representative, who navigates the political landscape on your behalf.
I feel I've demonstrated this with my long track record as your representative on the board.
My background as a Canadian immigrant, born in the Netherlands, living in Berlin and now in Switzerland and having learned Deutsch, 
leading Eclipse's Modeling Project,
sensitizes me to the fact that great communities span cultural, technical, national, and linguistic boundaries.
I am particularly proud to have worked with the board on the formation of an Eclipse IDE Working Group to help ensure the 
long term viability of Eclipse's flagship technology stack.
</p>

<p>
Cliche as it may sound, I believe strongly that helping is its own reward.
I answer questions daily on a broad range of forums, not just those for my own projects,
I fix Bugzillas quickly for the users of my software,
and I develop cool new features and improve performance whenever possible.
I endeavor to contribute constructively on the mailing lists.
I am particularly proud of the
 <a href="https://wiki.eclipse.org/Eclipse_Oomph_Installer" target="_blank">Oomph 
 technology</a> Eike Stepper and I have developed to help ease the 
 installation and configuration of development IDEs,
 making Eclipse projects more welcoming for contributors.
I am also proud of having worked with the board to enable the redistribution of Java Runtime Environments
 as well of the creation of
 <a href="/justj/" target="_blank">JustJ</a> to make JREs available as p2 installable units.
I am deeply honored to have received Eclipse's 
<a href="/org/press-release/20120326_awardwinners.php" 
target="_blank">Lifetime Contribution Award</a> in 2012.
I contribute actively to the Platform, and have been a driving force in helping to improve the consistency and quality to the simultaneous release.
I feel my track record as an Eclipse committer since 2002 is demonstrated with deeds not merely with words.
I focus on being a positive influence and on balancing the drive to fix problems and to make improvements
with recognition of the fact that real problems are difficult to solve and that what constitutes improvement is often highly subjective.
</p>

<p>
I will continue to work hard to ensure that Eclipse's processes are the best they can be and are ever improving.
I believe in minimal processes that focus on best practices and achievable, pragmatic results.
For example, do we really need to list each contributor in each and every file to which they have contributed?
No, this is tracked by Git commits.
Can we increase the CQ threshold for contributions from 250 lines to 10,000? No, but we can increase it to 1000.
Life is compromise.
I can be counted upon as the person who will speak up diplomatically even when personal risk is involved.
As your representative, I commit to working tirelessly to promote not only Eclipse itself but also the community that drives it.
It has been an honor and a privilege to serve as your representative these past years.
If reelected, you can count on the fact that Merks works for you.
</p>

]]> </vision>

	<bio> <![CDATA[
	<p>
		Ed Merks founded his own small consulting company, 
		<a href="https://www.macromodeling.com/" target="_blank">macromodeling</a>.
		He is a coauthor of the authoritative book <a href="https://www.informit.com/store/product.aspx?isbn=9780321331885" 
		target="_blank">EMF: Eclipse Modeling Framework</a> which is published as a second expanded edition. He has been 
		an elected member of the <a href="/org/foundation/directors.php" target="_blank">Eclipse Foundation 
		Board of Directors</a> for the past seventeen years and has been recognized by the Eclipse Community Awards as 
		Top Newcomer Evangelist, Top Ambassador, and Top Committer, as well as for Lifetime Achievement. Ed 
		is well known for his dedication to the 
		Eclipse community since 2002, having posted more than 33,000 forum answers. He spent 16 years at IBM, 
		achieving the level of Senior Technical Staff Member after completing his Ph.D. at 
		Simon Fraser University. He is currently 
		living in Switzerland and has learned to speak German. His 
		experience in modeling technology spans more than thirty years.
		</p>
	]]> </bio>

	<affiliation> <![CDATA[ 
			<p>
			<a href="https://www.macromodeling.com/" target="_blank">macromodeling</a>, Director
			</p>
	]]>
	</affiliation>
	
		
</boardmember>
