<?php
/**
 * Copyright (c) 2013, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eclipse Foundation - initial implementation
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

include ("scripts/candidate.php");
$id = preg_replace("/[^a-zA-Z0-9]+/", "", $App->getHTTPParameter('id', 'GET'));
$year = "";
for ($i = 2000; $i <= 2050; $i++) {
  if ($i == $App->getHTTPParameter('year', 'GET')) {
    $year = $App->getHTTPParameter('year', 'GET');
    break;
  }
}

if (empty($id) && is_null($year)) {
  header('Location: /org/elections/nominees.php', '302');
  exit();
}
$candidate = get_candidate($id, $year);
if ($candidate === FALSE) {
  header('Location: /org/elections/nominees.php', '302');
  exit();
}
$candidates_summary = get_candidates_short_summary_as_html($year);

//
// Begin: page-specific settings. Change these.
$pageTitle = $year . " Candidate: <br>" . $candidate->name;
$pageKeywords = "";
$pageAuthor = "";

$type_name = strcmp($candidate->type, 'committer') == 0 ? 'Committer Member' : 'Contributing Member';

$Theme->setPageTitle($pageTitle);

ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();
