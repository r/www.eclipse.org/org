<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */


require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
include ($App->getProjectCommon());
$Theme = $App->getThemeClass();

$pageTitle = "Eclipse Logos and Artwork";
$Theme->setPageTitle($pageTitle);
$Theme->setPageAuthor('Mike Milinkovich');
$Theme->setPageKeywords('eclipse, logo, eclipse logo, eclipse logos, artwork, logo, logos, trademark, trademarks, documents, about');

ob_start();
include ("content/en_old_artwork.php");
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();