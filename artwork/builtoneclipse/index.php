<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$App->setOutDated();
include($App->getProjectCommon());
$Theme = $App->getThemeClass();

$pageTitle = "Built on Eclipse Logos";
$Theme->setPageTitle($pageTitle);
$Theme->setPageAuthor('Wayne Beaton');
$Theme->setPageKeywords("eclipse, logos");

ob_start();
include ("content/en_builtoneclipse.php");
$html = ob_get_clean();
$Theme->setHtml($html);

$Theme->generatePage();