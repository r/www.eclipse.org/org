<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div class="col-md-24">
  <h1>
    <?php echo $pageTitle; ?>
  </h1>
  <h3>Usage Guidelines</h3>
  <p>
    The <i>Built on Eclipse</i> Logo is to be used to identify products
    that include the key components of the Eclipse platform. This type of
    product is able to run standalone with no prerequisites of other
    Eclipse components. For information about how these logos are to be
    used, please consult the <a href="/legal/logo_guidelines.php">Usage
    Guidelines</a>.
  </p>
  <h3>Logo ZIP Files</h3>
  <table>
    <tr>
      <td align=LEFT valign=TOP>
        <p>
          Logo sizes: <strong>extra small</strong> = 1x1&quot; inches; <strong>small</strong>
          = 2x2&quot;; <strong>medium</strong> = 5x5&quot;; <strong>large</strong>
          = 12.5x12.5&quot;; <strong>extra large</strong> = 24x24&quot;
        </p>
      </td>
    </tr>
  </table>
  <table class="table">
    <tr>
      <td width="18%" height="24" valign=TOP>Graphic</td>
      <td width="22%" valign=TOP>Description</td>
      <td width="15%" valign=TOP>Adobe Illustrator drawing [CMYK]<br>
      </td>
      <td width="15%" valign=TOP>Adobe Photoshop<br> hi-res bitmap [300
        dpi; CMYK]<br>
      </td>
      <td width="15%" valign=TOP>lo-res JPEGs<br> [72 dpi; RBG]
      </td>
      <td width="15%" valign=TOP>lo-res GIFs<br> [72 dpi; RGB]
      </td>
    </tr>
    <tr>
      <td height="113" valign=TOP>
        <p>
          <img src="images/bui_eclipse_pos_logo_fc_sm.jpg" width="144"
            height="144">
        </p>
      </td>
      <td valign=TOP>4 color logo graphic on positive background</td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_pos_logo_fc_xsm_ai.zip">extra small</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_sm_ai.zip">small</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_med_ai.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_lg_ai.zip">large</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_xlg_ai.zip">extra
        large</a>
      </td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_pos_logo_fc_xsm_psd.zip">extra
        small</a><br> <a
          href="zip_files/builton_eclipse_pos_logo_fc_sm_psd.zip">small</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_med_psd.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_lg_psd.zip">large</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_xlg_psd.zip">extra
        large</a>
      </td>
      <td valign=TOP>
        <p>
          <a href="zip_files/bui_eclipse_pos_logo_fc_xsm_jpg.zip">extra
          small</a><br> <a
            href="zip_files/bui_eclipse_pos_logo_fc_sm_jpg.zip">small</a><br>
          <a href="zip_files/bui_eclipse_pos_logo_fc_med_jpg.zip">medium</a><br>
          <br>
      <td valign=TOP><a
        href="zip_files/bui_eclipse_pos_logo_fc_xsm_gif.zip">extra small</a><br>
        <a href="zip_files/bui_eclipse_pos_logo_fc_sm_gif.zip">small</a><br>
        <a href="zip_files/bui_eclipse_pos_logo_fc_med_gif.zip">medium</a><br>
        <br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/bui_eclipse_neg_logo_fc_sm.jpg"
        width="144" height="144"></td>
      <td valign=TOP>4 color logo graphic on black (negative) background</td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_neg_logo_fc_xsm_ai.zip">extra small</a><br>
        <a href="zip_files/builton_eclipse_neg_logo_fc_sm_ai.zip">small</a><br>
        <a href="zip_files/builton_eclipse_neg_logo_fc_med_ai.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_neg_logo_fc_lg_ai.zip">large</a><br>
        <a href="zip_files/builton_eclipse_neg_logo_fc_xlg_ai.zip">extra
        large</a>
      </td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_neg_logo_fc_xsm_psd.zip">extra
        small</a><br> <a
          href="zip_files/builton_eclipse_neg_logo_fc_sm_psd.zip">small</a><br>
        <a href="zip_files/builton_eclipse_neg_logo_fc_med_psd.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_neg_logo_fc_lg_psd.zip">large</a><br>
        <a href="zip_files/builton_eclipse_neg_logo_fc_xlg_psd.zip">extra
        large</a>
      </td>
      <td valign=TOP><a
        href="zip_files/bui_eclipse_neg_logo_fc_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/bui_eclipse_neg_logo_fc_sm_jpg.zip">small</a><br>
        <a href="zip_files/bui_eclipse_neg_logo_fc_med_jpg.zip">medium</a><br>
        <br>
      </td>
      <td valign=TOP><a
        href="zip_files/bui_eclipse_neg_logo_fc_xsm_gif.zip">extra small</a><br>
        <a href="zip_files/bui_eclipse_neg_logo_fc_sm_gif.zip">small</a><br>
        <a href="zip_files/bui_eclipse_neg_logo_fc_med_gif.zip">medium</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/bui_eclipse_pos_wdmk_dt_sm.jpg"
        width="144" height="144"></td>
      <td valign=TOP>Duotone workmark logo on positive background</td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_pos_wdmk_dt_xsm_ai.zip">extra small</a><br>
        <a href="zip_files/builton_eclipse_pos_wdmk_dt_sm_ai.zip">small</a><br>
        <a href="zip_files/builton_eclipse_pos_wdmk_dt_med_ai.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_pos_wdmk_dt_lg_ai.zip">large</a><br>
        <a href="zip_files/builton_eclipse_pos_logo_fc_xlg_ai.zip">extra
        large</a>
      </td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_pos_wdmk_dt_xsm_psd.zip">extra
        small</a><br> <a
          href="zip_files/builton_eclipse_pos_wdmk_dt_sm_psd.zip">small</a><br>
        <a href="zip_files/builton_eclipse_pos_wdmk_dt_med_psd.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_pos_wdmk_dt_lg_psd.zip">large</a><br>
        <a href="zip_files/builton_eclipse_pos_wdmk_dt_xlg_psd.zip">extra
        large</a>
      </td>
      <td valign=TOP><a
        href="zip_files/bui_eclipse_pos_wdmk_dt_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/bui_eclipse_pos_wdmk_dt_sm_jpg.zip">small</a><br>
        <a href="zip_files/bui_eclipse_pos_wdmk_dt_med_jpg.zip">medium</a><br>
        <br>
      </td>
      <td valign=TOP><a
        href="zip_files/bui_eclipse_pos_wdmk_dt_xsm_gif.zip">extra small</a><br>
        <a href="zip_files/bui_eclipse_pos_wdmk_dt_sm_gif.zip">small</a><br>
        <a href="zip_files/bui_eclipse_pos_wdmk_dt_med_gif.zip">medium</a><br>
        <br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/bui_eclipse_neg_wdmk_dt_sm.jpg"
        width="144" height="144"></td>
      <td valign=TOP>Duotone wordmark logo on black (negative) background</td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_neg_wdmk_dt_xsm_ai.zip">extra small</a><br>
        <a href="zip_files/builton_eclipse_neg_wdmk_dt_sm_ai.zip">small</a><br>
        <a href="zip_files/builton_eclipse_neg_wdmk_dt_med_ai.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_neg_wdmk_dt_lg_ai.zip">large</a><br>
        <a href="zip_files/builton_eclipse_neg_wdmk_dt_xlg_ai.zip">extra
        large</a>
      </td>
      <td valign=TOP><a
        href="zip_files/builton_eclipse_neg_wdmk_dt_xsm_psd.zip">extra
        small</a><br> <a
          href="zip_files/builton_eclipse_neg_wdmk_dt_sm_psd.zip">small</a><br>
        <a href="zip_files/builton_eclipse_neg_wdmk_dt_med_psd.zip">medium</a><br>
        <a href="zip_files/builton_eclipse_neg_wdmk_dt_lg_psd.zip">large</a><br>
        <a href="zip_files/builton_eclipse_neg_wdmk_dt_xlg_psd.zip">extra
        large</a>
      </td>
      <td valign=TOP><a
        href="zip_files/bui_eclipse_neg_wdmk_dt_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/bui_eclipse_neg_wdmk_dt_sm_jpg.zip">small</a><br>
        <a href="zip_files/bui_eclipse_neg_wdmk_dt_med_jpg.zip">medium</a><br>
      </td>
      <td valign=TOP><a
        href="zip_files/bui_eclipse_neg_wdmk_dt_xsm_gif.zip">extra small</a><br>
        <a href="zip_files/bui_eclipse_neg_wdmk_dt_sm_gif.zip">small</a><br>
        <a href="zip_files/bui_eclipse_neg_wdmk_dt_med_gif.zip">medium</a><br>
      </td>
    </tr>
  </table>
</div>