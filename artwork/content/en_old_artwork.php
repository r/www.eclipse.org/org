<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle;?> <small>for Members and Providers</small></h1>
  <p>In order to help identify how Eclipse is incorporated into the
    projects and products developed by the community and Foundation members,
    the &quot;Eclipse Ready&quot; &quot;Built on Eclipse&quot; and &quot;Eclipse
    Foundation Member&quot; logos can be downloaded here. For information
    about how these logos are to be used, please consult the <a href="../legal/logo_guidelines.php">Usage
    Guidelines</a>. Each logo is available in a variety of styles and sizes
    from the links below.
  </p>
  <table >
    <tr>
      <td align=RIGHT valign=TOP width="3%">&nbsp;</td>
      <td width="32%">
        <div align="center"><img src="builtoneclipse/images/bui_eclipse_pos_logo_fc_sm.jpg" alt="Built on Eclipse" width="144" height="144"><br>
          <a href="builtoneclipse/builtoneclipse.html">Built on Eclipse</a>
        </div>
      </td>
      <td width="32%">
        <div align="center"><img src="eclipseready/images/rea_eclipse_pos_logo_fc_sm.jpg" alt="Eclipse Ready" width="144" height="144"><br>
          <a href="eclipseready/eclipseready.html">Eclipse Ready</a>
        </div>
      </td>
      <td width="32%">
        <div align="center"><img src="foundationmember/images/mem_eclipse_pos_logo_fc_sm.jpg" alt="Foundation Member" width="144" height="144"><br>
          <a href="foundationmember/foundationmember.html">Eclipse Foundation Member</a>
        </div>
      </td>
    </tr>
  </table>
  <h3>Graphic Guidelines for Eclipse Identity Logo</h3>
  <p>The Eclipse Identity logo is available in the table below. Please read
    the <strong><a href="eclipse_graphic_guidelines.pdf">E</a></strong><a href="eclipse_graphic_guidelines.pdf"><b>c</b></a><b><a target="_top" href="eclipse_graphic_guidelines.pdf">lipse
    Logo Graphic Guidelines (.pdf)</a></b> for instructions about appropriate
    uses; then download the desired style, format, size from the table below.
  </p>
  <h3>Eclipse identity logo downloads </h3>
  <p>Logo sizes: <strong>extra small</strong> = 1x1&quot;
    inches; <strong>small</strong> = 2x2&quot;; <strong>medium</strong> =
    5x5&quot;; <strong>large</strong> = 12.5x12.5&quot;; <strong>extra large</strong>
    = 24x24&quot;
  </p>
  <table class="table">
    <tr>
      <td width="18%" height="24" valign=TOP>Graphic</td>
      <td width="22%" valign=TOP>Description</td>
      <td width="15%" valign=TOP> Adobe Illustrator 9 drawing [CMYK]<br> </td>
      <td width="15%" valign=TOP>Adobe Photoshop<br>
        hi-res bitmap [300 dpi; CMYK]<br>
      </td>
      <td width="15%" valign=TOP> lo-res JPEGs<br>
        [72 dpi; RBG]
      </td>
      <td width="15%" valign=TOP>lo-res BMPs<br>
        [72 dpi; RGB]
      </td>
    </tr>
    <tr>
      <td height="113" valign=TOP>
        <img src="images/eclipse_bckgr_logo_fc_sm.jpg" hspace="5" vspace="5">
        <p></p>
      </td>
      <td valign=TOP>4 color logo graphic on illustrative 'eclipse' background</td>
      <td valign=TOP><a href="zip_files/eclipse_bckgr_logo_fc_xsm_ai.zip">extra
        small</a><br> <a href="zip_files/eclipse_bckgr_logo_fc_sm_ai.zip">small</a><br>
        <a href="zip_files/eclipse_bckgr_logo_fc_med_ai.zip">medium</a><br> <a href="zip_files/eclipse_bckgr_logo_fc_lg_ai.zip">large</a><br>
        <a href="zip_files/eclipse_bckgr_logo_fc_xlg_ai.zip">extra large</a>
      </td>
      <td  valign=TOP><a href="zip_files/eclipse_bckgr_logo_fc_xsm_psd.zip">extra
        small</a><br> <a href="zip_files/eclipse_bckgr_logo_fc_sm_psd.zip">small</a><br>
        <a href="zip_files/eclipse_bckgr_logo_fc_med_psd.zip">medium</a><br> <a href="zip_files/eclipse_bckgr_logo_fc_lg_psd.zip">large</a><br>
        <a href="zip_files/eclipse_bckgr_logo_fc_xlg_psd.zip">extra large</a>
      </td>
      <td  valign=TOP>
        <p><a href="zip_files/eclipse_bckgr_logo_fc_xsm_jpg.zip">extra
          small</a><br>
          <a href="zip_files/eclipse_bckgr_logo_fc_sm_jpg.zip">small</a><br>
          <a href="zip_files/eclipse_bckgr_logo_fc_med_jpg.zip">medium</a><br>
          <a href="zip_files/eclipse_bckgr_logo_fc_lg_jpg.zip">large</a><br>
      <td  valign=TOP><a href="zip_files/eclipse_bckgr_logo_fc_xsm_bmp.zip">extra
        small</a><br> <a href="zip_files/eclipse_bckgr_logo_fc_sm_bmp.zip">small</a><br>
        <a href="zip_files/eclipse_bckgr_logo_fc_med_bmp.zip">medium</a><br> <a href="zip_files/eclipse_bckgr_logo_fc_lg_bmp.zip">large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/eclipse_bckgr_logo_dt_sm.jpg" hspace="5" vspace="5"></td>
      <td valign=TOP>2 color (PMS 187-2 + black) logo graphic on illustrative 'eclipse'
        background
      </td>
      <td valign=TOP><a href="zip_files/eclipse_bckgr_logo_dt_xsm_ai.zip">extra
        small</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_sm_ai.zip">small</a><br>
        <a href="zip_files/eclipse_bckgr_logo_dt_med_ai.zip">medium</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_lg_ai.zip">large</a><br>
        <a href="zip_files/eclipse_bckgr_logo_dt_xlg_ai.zip">extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_bckgr_logo_dt_xsm_psd.zip">extra
        small</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_sm_psd.zip">small</a><br>
        <a href="zip_files/eclipse_bckgr_logo_dt_med_psd.zip">medium</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_lg_psd.zip">large</a><br>
        <a href="zip_files/eclipse_bckgr_logo_dt_xlg_psd.zip">extra large</a>
      </td>
      <td valign=TOP> <a href="zip_files/eclipse_bckgr_logo_dt_xsm_jpg.zip">extra
        small</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_sm_jpg.zip">small</a><br>
        <a href="zip_files/eclipse_bckgr_logo_dt_med_jpg.zip">medium</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_lg_jpg.zip">large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_bckgr_logo_dt_xsm_bmp.zip">extra
        small</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_sm_bmp.zip">small</a><br>
        <a href="zip_files/eclipse_bckgr_logo_dt_med_bmp.zip">medium</a><br> <a href="zip_files/eclipse_bckgr_logo_dt_lg_bmp.zip">large<br>
        </a>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/eclipse_neg_logo_fc_sm.jpg" hspace="5" vspace="5"></td>
      <td valign=TOP>4 color logo graphic on dark background</td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_fc_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_fc_sm_ai.zip">small</a><br> <a href="zip_files/eclipse_neg_logo_fc_med_ai.zip">medium</a><br>
        <a href="zip_files/eclipse_neg_logo_fc_lg_ai.zip">large</a><br> <a href="zip_files/eclipse_neg_logo_fc_xlg_ai.zip">extra
        large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_fc_xsm_psd.zip"> extra
        small</a><br>
        <a href="zip_files/eclipse_neg_logo_fc_sm_psd.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_fc_med_psd.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_fc_lg_psd.zip"> large</a><br>
        <a href="zip_files/eclipse_neg_logo_fc_xlg_psd.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_fc_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_fc_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_fc_med_jpg.zip">
        medium<br>
        </a><a href="zip_files/eclipse_neg_logo_fc_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_fc_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_fc_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_fc_med_bmp.zip">
        medium<br>
        </a><a href="zip_files/eclipse_neg_logo_fc_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/eclipse_pos_logo_fc_sm.jpg" hspace="5" vspace="5" border="1"></td>
      <td valign=TOP>4 color logo graphic on light background</td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_fc_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_fc_sm_ai.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_fc_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_fc_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_logo_fc_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_fc_xsm_psd.zip"> extra
        small</a>&nbsp;<br>
        <a href="zip_files/eclipse_pos_logo_fc_sm_psd.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_fc_med_psd.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_fc_lg_psd.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_logo_fc_xlg_psd.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_fc_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_fc_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_fc_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_fc_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_fc_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_fc_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_fc_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_fc_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP> <img src="images/eclipse_neg_logo_dt_sm.jpg" hspace="5" vspace="5"></td>
      <td valign=TOP>2 color (PMS 187-2 + black) logo graphic on dark background</td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_dt_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_dt_sm_ai.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_dt_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_dt_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_neg_logo_dt_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_dt_xsm_psd.zip"> extra
        small</a>&nbsp;<br> <a href="zip_files/eclipse_neg_logo_dt_sm_psd.zip">
        small</a><br> <a href="zip_files/eclipse_neg_logo_dt_med_psd.zip"> medium</a><br>
        <a href="zip_files/eclipse_neg_logo_dt_lg_psd.zip"> large</a><br> <a href="zip_files/eclipse_neg_logo_dt_xlg_psd.zip">
        extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_dt_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_dt_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_dt_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_dt_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_dt_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_dt_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_dt_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_dt_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/eclipse_pos_logo_dt_sm.jpg" hspace="5" vspace="5" border="1"></td>
      <td valign=TOP>2 color (PMS 187-2 + black) logo graphic on light background</td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_dt_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_dt_sm_ai.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_dt_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_dt_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_logo_dt_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_dt_xsm_psd.zip"> extra
        small&nbsp;</a><br> <a href="zip_files/eclipse_pos_logo_dt_sm_psd.zip">
        small</a><br> <a href="zip_files/eclipse_pos_logo_dt_med_psd.zip"> medium</a><br>
        <a href="zip_files/eclipse_pos_logo_dt_lg_psd.zip"> large</a><br> <a href="zip_files/eclipse_pos_logo_dt_xlg_psd.zip">
        extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_dt_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_dt_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_dt_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_dt_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_dt_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_dt_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_dt_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_dt_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP> <img src="images/eclipse_neg_wdmk_dt_sm.jpg" hspace="5" vspace="5"></td>
      <td valign=TOP>2 color (PMS 187-2 + black) wordmark on dark background</td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_dt_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_dt_sm_ai.zip"> small</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_neg_wdmk_dt_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_dt_xsm_psd.zip"> extra
        small</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_sm_psd.zip"> small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_dt_med_psd.zip"> medium</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_lg_psd.zip">
        large</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_xlg_psd.zip"> extra
        large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_dt_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_dt_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_dt_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_dt_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_wdmk_dt_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP> <img src="images/eclipse_pos_wdmk_dt_sm.jpg" hspace="5" vspace="5" border="1"></td>
      <td valign=TOP>2 color (PMS 187-2 + black) wordmark on light background</td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_dt_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_dt_sm_ai.zip"> small</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_wdmk_dt_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_dt_xsm_psd.zip"> extra
        small</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_sm_psd.zip"> small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_dt_med_psd.zip"> medium</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_lg_psd.zip">
        large</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_xlg_psd.zip"> extra
        large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_dt_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_dt_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_dt_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_dt_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_wdmk_dt_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP> <img src="images/eclipse_neg_wdmk_bw_sm.jpg" hspace="5" vspace="5"></td>
      <td valign=TOP>1 color (black) wordmark on black background</td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_bw_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_bw_sm_ai.zip"> small</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_neg_wdmk_bw_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_bw_xsm_psd.zip"> extra
        small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_bw_sm_psd.zip"> small</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_med_psd.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_lg_psd.zip"> large</a><br>
        <a href="zip_files/eclipse_neg_wdmk_bw_xlg_psd.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_bw_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_bw_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_wdmk_bw_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_wdmk_bw_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_wdmk_bw_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP> <img src="images/eclipse_pos_wdmk_bw_sm.jpg" hspace="5" vspace="5" border="1"></td>
      <td valign=TOP>1 color (black) wordmark on white background</td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_bw_xsm_ai.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_bw_sm_ai.zip">small</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_wdmk_bw_xlg_ai.zip"> extra large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_bw_xsm_psd.zip"> extra
        small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_bw_sm_psd.zip"> small</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_med_psd.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_lg_psd.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_wdmk_bw_xlg_psd.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_bw_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_bw_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_wdmk_bw_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_wdmk_bw_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_wdmk_bw_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/eclipse_neg_logo_bw_sm.jpg" hspace="5" vspace="5"></td>
      <td valign=TOP>1 color (black) logo graphic on black background (not recommended
        for use; see<a href="eclipse_graphic_guidelines.pdf"> guidelines</a>)
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_bw_xsm_ai.zip"> extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_bw_sm_ai.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_bw_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_bw_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_neg_logo_bw_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_bw_xsm_psd.zip"> extra
        small</a><br>
        <a href="zip_files/eclipse_neg_logo_bw_sm_psd.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_bw_med_psd.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_bw_lg_psd.zip"> large</a><br>
        <a href="zip_files/eclipse_neg_logo_bw_lg_psd.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_bw_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_bw_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_bw_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_bw_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_neg_logo_bw_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_neg_logo_bw_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_neg_logo_bw_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_neg_logo_bw_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
    <tr>
      <td valign=TOP><img src="images/eclipse_pos_logo_bw_sm.jpg" hspace="5" vspace="5" border="1"></td>
      <td valign=TOP>1 color (black) logo graphic on white background (not recommended
        for use; see<a href="eclipse_graphic_guidelines.pdf"> guidelines</a>)
      </td>
      <td valign=TOP> <a href="zip_files/eclipse_pos_logo_bw_xsm_ai.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_bw_sm_ai.zip">small</a><br> <a href="zip_files/eclipse_pos_logo_bw_med_ai.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_bw_lg_ai.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_logo_bw_xlg_ai.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_bw_xsm_psd.zip"> extra
        small</a><br>
        <a href="zip_files/eclipse_pos_logo_bw_sm_psd.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_bw_med_psd.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_bw_lg_psd.zip"> large</a><br>
        <a href="zip_files/eclipse_pos_logo_bw_xlg_psd.zip"> extra large</a>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_bw_xsm_jpg.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_bw_sm_jpg.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_bw_med_jpg.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_bw_lg_jpg.zip"> large</a><br>
      </td>
      <td valign=TOP><a href="zip_files/eclipse_pos_logo_bw_xsm_bmp.zip">extra small</a><br>
        <a href="zip_files/eclipse_pos_logo_bw_sm_bmp.zip"> small</a><br> <a href="zip_files/eclipse_pos_logo_bw_med_bmp.zip">
        medium</a><br> <a href="zip_files/eclipse_pos_logo_bw_lg_bmp.zip"> large</a><br>
      </td>
    </tr>
  </table>
</div>
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="../legal/logo_guidelines.php">Logo Usage Guidelines</a></li>
    </ul>
  </div>
</div>