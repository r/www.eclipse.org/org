<?php

/**
 * Copyright (c) 2014, 2018, 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Eric Poirier (Eclipse Foundation)
 *    Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>


<h1><?php print $pageTitle; ?></h1>
<p>We are providing copies of the Eclipse logo so our community can use it to show their support of Eclipse and link back to our community.
  These logos are the intellectual property of the Eclipse Foundation and cannot be altered without Eclipse&apos;s permission. They are provided for use under the <a href="http://www.eclipse.org/legal/logo_guidelines.php">Eclipse Foundation Trademark Usage Policy</a>.
</p>
<div class="logo-catalog margin-bottom-40 margin-top-40">
  <a href="#ef-logos">Eclipse Foundation Logos</a>
  <span>|</span>
  <a href="#ef-member-logos">Eclipse Foundation Member Logos</a>
  <span>|</span>
  <a href="#eclipse-ide-logos">Eclipse IDE Logos</a>
  <span>|</span>
  <a href="#generic-eclipse-project-logos">Generic Eclipse Project Logos</a>
  <span>|</span>
  <a href="#industry-collaboration-logos">Industry Collaboration Logos</a>
</div>

<!-- Eclipse Foundation logo-->
<div id="eclipse_logo_artworks">
  <h2 class="logo-group-title" id="ef-logos">Eclipse Foundation Logos</h2>
  <div class="logo-row">
    <div class="logo-card" id="eclipse-foundation">
      <div class="logo-title-container">
        <p class="logo-title">Eclipse Foundation</p>
      </div>
      <img src="images/eclipse_foundation_logo.jpg" alt="Eclipse Foundation logo">
      <p class="branding-guidelines">
        <a href="/legal/documents/eclipse_foundation_branding_guidelines.pdf">Brand Guidelines</a>
      </p>
      <p class="logo-types">Includes: <strong>Colour | Reversed Colour | Monochromatic</strong></p>
      <div class="logo-button-container">
        <a href="zip_files/eclipse-foundation-logo.zip" title="Download Eclipse Foundation logo">Download</a>
      </div>
    </div>
  </div>
  <!-- END #eclipse_logo_artworks_mp -->

  <h2 class="logo-group-title" id="ef-member-logos">Eclipse Foundation Member Logos</h2>
  <div class="logo-row">
    <div class="logo-card" id="ef-associate-member">
      <div class="logo-title-container">
        <p class="logo-title">Eclipse Associate Member</p>
      </div>
      <img src="images/EF_Associate_Member_PNG-s.png" alt="Eclipse Associate Member logo">
      <p class="logo-types">Includes: <strong>Colour | Monochromatic | Reversed Monochromatic</strong></p>
      <div class="logo-button-container">
        <a href="zip_files/ef-associate-member-logo.zip" title="Download Eclipse Foundation Associate Member logo">Download</a>
      </div>
    </div>
    <div class="logo-card" id="ef-contributing-member">
      <div class="logo-title-container">
        <p class="logo-title">Eclipse Contributing Member</p>
      </div>
      <img src="images/EF_Contributing_Member_PNG-s.png" alt="Eclipse Foundation white &amp; orange logo">
      <p class="logo-types">Includes: <strong>Colour | Monochromatic | Reversed Monochromatic</strong></p>
      <div class="logo-button-container">
        <a href="zip_files/ef-contributing-member-logo.zip" title="Download Eclipse Foundation Contributing Member logo">Download</a>
      </div>
    </div>
    <div class="logo-card" id="ef-strategic-member">
      <div class="logo-title-container">
        <p class="logo-title">Eclipse Strategic Member</p>
      </div>
      <img src="images/EF_Strategic_Member_PNG-s.png" alt="Eclipse Foundation white logo">
      <p class="logo-types">Includes: <strong>Colour | Monochromatic | Reversed Monochromatic</strong></p>
      <div class="logo-button-container">
        <a href="zip_files/ef-strategic-member-logo.zip" title="Download Eclipse Foundation Strategic Member logo">Download</a>
      </div>
    </div>
  </div>
  <!-- END #eclipse_member_logo_artworks_mp -->

  <h2 class="logo-group-title" id="eclipse-ide-logos">Eclipse IDE Logos</h2>
  <div class="logo-row">
    <div class="logo-card" id="eclipse-ide">
      <div class="logo-title-container">
        <p class="logo-title">Eclipse IDE</p>
      </div>
      <img class="big-logo" src="images/eclipse_ide_logo.png" alt="Eclipse IDE logo">
      <p class="logo-types">Includes: <strong>Colour | Reversed Colour | Monochromatic</strong></p>
      <div class="logo-button-container">
        <a href="zip_files/eclipse-ide-logo.zip" title="Download Eclipse IDE logo">Download</a>
      </div>
    </div>
    <div class="logo-card" id="eclipse-ide-built-on">
      <div class="logo-title-container">
        <p class="logo-title">Built on Eclipse</p>
      </div>
      <img class="big-logo" src="images/eclipse-builton-logo.png" alt="Built on Eclipse logo">
      <p class="logo-types">Includes: <strong>Colour</strong></p>
      <div class="logo-button-container">
        <a href="zip_files/eclipse-built-on-logo.zip" title="Download Built on Eclipse logo">Download</a>
      </div>
    </div>
    <div class="logo-card" id="eclipse-ide-ready">
      <div class="logo-title-container">
        <p class="logo-title">Eclipse Ready</p>
      </div>
      <img class="big-logo" src="images/eclipse-ready-logo.png" alt="Eclipse Ready logo">
      <p class="logo-types">Includes: <strong>Colour</strong></p>
      <div class="logo-button-container">
        <a href="zip_files/eclipse-ready-logo.zip" title="Download Eclipse Ready logo">Download</a>
      </div>
    </div>
  </div>
</div>
<!-- END #eclipse_ide_logo_artworks_mp -->
<!-- END Eclipse Foundation logo-->

<!-- Generic Eclipse Project Logos -->
<h2 class="logo-group-title" id="generic-eclipse-project-logos">Generic Eclipse Project Logos</h2>
<div class="logo-row">
  <!-- Incubation logo-->
  <div class="logo-card" id="incubation">
    <div class="logo-title-container">
      <p class="logo-title">Eclipse Incubation</p>
    </div>
    <img src="images/eclipse_incubation_horizontal-svg.svg" alt="Eclipse incubation logo">
    <p class="margin-left-20 margin-right-20 margin-bottom-20">For use exclusively by the Eclipse Foundation and the Eclipse projects to indicate the incubation status of the project.</p>
    <p class="logo-types">Includes: <strong>Vertical | Horizontal</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/incubation-logo.zip" title="Download Eclipse Incubation logo">Download</a>
    </div>
  </div>
  <!-- END Incubation logo -->
  <!-- Research@Eclipse logo-->
  <div class="logo-card" id="research-at-eclipse">
    <div class="logo-title-container">
      <p class="logo-title">Eclipse@Research</p>
    </div>
    <img src="images/eclipse-research-logo.svg" alt="Eclipse@Research logo">
    <p class="margin-left-20 margin-right-20 margin-bottom-20">For use exclusively by the Eclipse Foundation and the Eclipse research projects.</p>
    <p class="logo-types">Includes: <strong>Colour | Reversed Colour</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/eclipse-research-logo.zip" title="Download Eclipse@Research logo">Download</a>
    </div>
  </div>
  <!-- END Research@Eclipse logo -->
</div>
<!-- END Generic Eclipse Project Logos  -->

<!-- Working Groups logo-->
<h2 class="logo-group-title" id="industry-collaboration-logos">Industry Collaboration Logos</h2>
<div class="logo-row margin-bottom-60">
  <!-- Adoptium Logo -->
  <div class="logo-card" id="adoptium">
    <div class="logo-title-container">
      <p class="logo-title">Adoptium</p>
    </div>
    <img class="adoptium-logo" src="images/wg-adoptium-logo.svg" alt="Adoptium logo">
    <p class="branding-guidelines">
      <a href="guidelines/adoptium-brand-guidelines.pdf">Brand Guidelines</a>
    </p>
    <p class="logo-types">Includes: <strong>Colour | Reversed Colour | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/adoptium-logo.zip" title="Download Adoptium Tools logo">Download</a>
    </div>
  </div>
  <!-- AsciiDoc Logo -->
  <div class="logo-card" id="asciidoc">
    <div class="logo-title-container">
      <p class="logo-title">AsciiDoc</p>
    </div>
    <img src="images/wg_asciidoc-logo.svg" alt="AsciiDoc logo">
    <p class="branding-guidelines">
      <a href="guidelines/asciidoc-brand-guidelines.pdf">Brand Guidelines</a>
    </p>
    <p class="logo-types">Includes: <strong>Colour | Reversed Colour | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/asciidoc-logo.zip" title="Download AsciiDoc logo">Download</a>
    </div>
  </div>
  <!-- ECD Logo -->
  <div class="logo-card" id="ecd">
    <div class="logo-title-container">
      <p class="logo-title">Eclipse Cloud Devtools</p>
    </div>
    <img src="images/wg-ecd-tools-logo.svg" alt="ECD Tools logo">
    <p class="logo-types">Includes: <strong>Colour</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/ecd-tools-logo.zip" title="Download ECD Tools logo">Download</a>
    </div>
  </div>

  <!-- Eclipse Dataspace Logo -->
  <div class="logo-card" id="eclipse-dataspace">
    <div class="logo-title-container">
      <p class="logo-title">Eclipse Dataspace</p>
    </div>
    <img src="images/eclipse-dataspace-logo.svg" alt="Eclipse Dataspace logo">
    <p class="logo-types">Includes: <strong>Colour | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/eclipse-dataspace-logo.zip" title="Download Eclipse Dataspace logo">Download</a>
    </div>
  </div>

  <!-- Eclipse IDE Logo -->
  <div class="logo-card" id="eclipse-ide-wg">
    <div class="logo-title-container">
      <p class="logo-title">Eclipse IDE</p>
    </div>
    <img class="big-logo" src="images/eclipse_ide_logo.png" alt="Eclipse IDE logo">
    <p class="logo-types">Includes: <strong>Colour | Reversed Colour | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/eclipse-ide-logo.zip" title="Download Eclipse IDE logo">Download</a>
    </div>
  </div>
  <!-- IoT Logo -->
  <div class="logo-card" id="iot">
    <div class="logo-title-container">
      <p class="logo-title">Eclipse IOT</p>
    </div>
    <img class="iot-logo" src="images/iot_logo_clr.svg" alt="IoT logo">
    <p class="branding-guidelines"><a href="guidelines/eclipse-iot-brand-guidelines.pdf">Brand Guidelines</a></p>
    <p class="logo-types">Includes: <strong>Working Group | Edge Native</strong> in <strong>Colour | Reversed Colour | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/iot-logo.zip" title="Download IoT logo">Download</a>
    </div>
  </div>
  <!-- Jakarta EE logo -->
  <div class="logo-card" id="jakarta-ee">
    <div class="logo-title-container">
      <p class="logo-title">Jakarta EE</p>
    </div>
    <img src="images/jakarta_ee_logo.svg" alt="Jakarta EE logo">
    <p class="branding-guidelines">
      <a href="https://jakarta.ee/legal/trademark_guidelines/jakarta-ee-branding-guidelines.pdf">Brand Guidelines</a>
    </p>
    <p class="logo-types">Includes: <strong>Working Group | Compatibility | Member</strong> in <strong>Color | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/jakarta-logo.zip" title="Download Jakarta EE logo">Download</a>
    </div>
  </div>
  <!-- MicroProfile logo -->
  <div class="logo-card" id="microprofile">
    <div class="logo-title-container">
      <p class="logo-title">MicroProfile</p>
    </div>
    <img src="images/wg-microprofile-logo.svg" alt="MicroProfile logo">
    <p class="logo-types">Includes: <strong>Working Group | Compatibility | Member</strong> in <strong>Color | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/microprofile-logo.zip" title="Download MicroProfile logo">Download</a>
    </div>
  </div>
  <!-- Oniro logo -->
  <div class="logo-card" id="oniro">
    <div class="logo-title-container">
      <p class="logo-title">Oniro</p>
    </div>
    <img class="big-logo" src="images/wg-oniro-logo.svg" alt="Oniro logo">
    <p class="logo-types">Includes: <strong>Colour | Reversed Colour | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/oniro-logo.zip" title="Download Oniro logo">Download</a>
    </div>
  </div>
  <!-- OpenHW -->
  <div class="logo-card" id="openhw">
    <div class="logo-title-container">
      <p class="logo-title">Open HW</p>
    </div>
    <img src="images/wg-openhw.svg" alt="OpenHW logo">
    <p class="logo-types">Includes: <strong>OpenHW Group | OpenHW Asia | OpenHW Europe</strong> in <strong>Colour</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/openhw-logo.zip" title="Download OpenHW logo">Download</a>
    </div>
  </div>
  <!-- OpenMDM -->
  <div class="logo-card" id="open-mdm">
    <div class="logo-title-container">
      <p class="logo-title">OpenMDM</p>
    </div>
    <img class="square-logo" src="images/openmdm-logo.png" alt="OpenMDM logo">
    <p class="logo-types">Includes: <strong>Colour</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/openmdm-logo.zip" title="Download OpenMDM logo">Download</a>
    </div>
  </div>
  <!-- OpenMobility -->
  <div class="logo-card" id="open-mobility">
    <div class="logo-title-container">
      <p class="logo-title">openMobility</p>
    </div>
    <img src="images/wg-openmobility-logo.svg" alt="OpenMobility logo">
    <p class="logo-types">Includes: <strong>Colour</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/open-mobility-logo.zip" title="Download OpenMobility logo">Download</a>
    </div>
  </div>
  <!-- OpenPASS -->
  <div class="logo-card" id="open-pass">
    <div class="logo-title-container">
      <p class="logo-title">OpenPass</p>
    </div>
    <img class="big-logo" src="images/wg-openpass-logo.svg" alt="OpenPASS logo">
    <p class="logo-types">Includes: <strong>Colour</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/openpass-logo.zip" title="Download OpenPASS logo">Download</a>
    </div>
  </div>
  <!-- Open VSX -->
  <div class="logo-card" id="open-vsx">
    <div class="logo-title-container">
      <p class="logo-title">Open VSX</p>
    </div>
    <img class="big-logo" src="images/wg-open-vsx-logo.svg" alt="Open VSX logo">
    <p class="logo-types">Includes: <strong>Colour</strong> | <strong>Monochromatic</strong> | <strong>Reversed Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/open-vsx-logo.zip" title="Download Open VSX logo">Download</a>
    </div>
  </div>
  <!-- OSGI -->
  <div class="logo-card" id="osgi">
    <div class="logo-title-container">
      <p class="logo-title">OSGI</p>
    </div>
    <img class="big-logo" src="images/wg-osgi-logo.svg" alt="OSGI logo">
    <p class="logo-types">Includes: <strong>Colour</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/osgi-logo.zip" title="Download OSGI logo">Download</a>
    </div>
  </div>
  <!-- SDV logo -->
  <div class="logo-card" id="sdv">
    <div class="logo-title-container">
      <p class="logo-title">Software Defined Vehicle</p>
    </div>
    <img class="big-logo" src="images/wg-sdv-logo.svg" alt="Eclipse Software Defined Vehicle logo">
    <p class="branding-guidelines">
      <a href="guidelines/sdv-brand-guidelines.pdf">Brand Guidelines</a>
    </p>
    <p class="logo-types">Includes: <strong>Colour | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/sdv-logo.zip" title="Download Eclipse Software Defined Vehicle logo">Download</a>
    </div>
  </div>
  <!-- Sparkplug logo -->
  <div class="logo-card" id="sparkplug">
    <div class="logo-title-container">
      <p class="logo-title">Sparkplug</p>
    </div>
    <img src="images/wg-sparkplug-logo.svg" alt="Sparkplug logo">
    <p class="branding-guidelines">
      <a href="guidelines/sparkplug-brand-guidelines.pdf">Brand Guidelines</a>
    </p>
    <p class="logo-types">Includes: <strong>Working Group | Compatibility</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/sparkplug-logo.zip" title="Download Sparkplug logo">Download</a>
    </div>
  </div>
  <!-- ThreadX logo -->
  <div class="logo-card" id="threadx">
    <div class="logo-title-container">
      <p class="logo-title">ThreadX</p>
    </div>
    <img src="images/wg-threadx-logo.png" alt="ThreadX logo">
    <p class="logo-types">Includes: <strong>Color | Monochromatic</strong></p>
    <div class="logo-button-container">
      <a href="zip_files/threadx-logo.zip" title="Download ThreadX logo">Download</a>
    </div>
  </div>
</div>
<!-- END Working Groups logo-->
