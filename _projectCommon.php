<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation)
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$theme = NULL;
$App->Promotion = TRUE;

$Nav = new Nav();
$Nav->setType('collapse');
$Nav->addNavSeparator("About Us", "/org/");

$Nav->addCustomNav("Foundation", "/org/foundation/", "_self", 1);

$Nav->addCustomNav("Members & Supporters", "/membership", "_self", 1);
$Nav->addCustomNav("Our Members", "/membership/exploreMembership.php", "_self", 2);
$Nav->addCustomNav("Corporate Sponsors", "/org/corporate_sponsors", "_self", 2);
$Nav->addCustomNav("In-Kind Supporters", "/org/foundation/thankyou.php", "_self", 2);

$Nav->addCustomNav("Services", "#", "_self", 1);
$Nav->addCustomNav("IP Management", "/org/services/#IP_Management", "_self", 2);
$Nav->addCustomNav("Ecosystem Development", "/org/services/#Ecosystem", "_self", 2);
$Nav->addCustomNav("Development Process", "/org/services/#Development", "_self", 2);
$Nav->addCustomNav("IT Infrastructure", "/org/services/#IT", "_self", 2);
$Nav->addCustomNav("Marketing Services", "/org/services/marketing", "_self", 2);

$Nav->addCustomNav("Our Team", "#", "_self", 1);
$Nav->addCustomNav("Eclipse Foundation Staff", "/org/foundation/staff.php", "_self", 2);
$Nav->addCustomNav("Contact Us", "/org/foundation/contact.php", "_self", 2);
$Nav->addCustomNav("Careers", "/careers", "_self", 2);

$Nav->addCustomNav("Governance", "#", "_self", 1);
$Nav->addCustomNav("Board of Directors", "/org/foundation/directors.php", "_self", 2);
$Nav->addCustomNav("Board of Directors Elections", "/org/elections/", "_self", 2);
$Nav->addCustomNav("Councils", "/org/foundation/council.php", "_self", 2);
$Nav->addCustomNav("Annual Report", "/org/foundation/reports/annual_report.php", "_self", 2);
$Nav->addCustomNav("Governance Documents", "/org/documents/", "_self", 2);
$Nav->addCustomNav("Meeting Minutes", "/org/foundation/minutes.php", "_self", 2);

$Nav->addCustomNav("Our Brand", "#", "_self", 1);
$Nav->addCustomNav("Logos and Artwork", "/org/artwork/", "_self", 2);
$Nav->addCustomNav("Eclipse Foundation Brand Guidelines", "/legal/documents/eclipse_foundation_branding_guidelines.pdf", "_self", 2);
$Nav->addCustomNav("Trademark Usage Guidelines", "/legal/logo_guidelines.php", "_self", 2);
$Nav->addCustomNav("Writing Style Guide", "/org/documents/writing-style-guide/", "_self", 2);

$Nav->addCustomNav("Announcements", "#", "_self", 1);
$Nav->addCustomNav("Press Releases", "/org/press-release/", "_self", 2);
$Nav->addCustomNav("PR Guidelines", "/org/press-release/pressrelease_guidelines.php", "_self", 2);
$Nav->addCustomNav("2006 Archive", "/org/press-release/2006archive.php", "_self", 2);
$Nav->addCustomNav("2001-2005 Archive", "/org/press-release/main.html", "_self", 2);
