<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Public License Version 2.0 Approved By OSI and Eclipse Foundation Board of Directors";
	$pageKeywords	= "eclipse, epl, epl v.2, Eclipse Public License, Eclipse Public License V.2";
	$pageAuthor		= "Mike Milinkovich";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p>Ottawa, Canada - August 24, 2017 - The Eclipse Foundation is pleased to announce a new version 2 of the Eclipse Public License (<a href="https://www.eclipse.org/org/documents/epl-2.0/index.php">EPL v2</a>) has been approved by the Eclipse Foundation Board of Directors and the Open Source Initiative (OSI). The new EPL v2 is the result of a two year community process to identify and implement improvements to the existing <a href="https://www.eclipse.org/org/documents/epl-v10.php">EPL v1 license</a>. The new license will become the default license for Eclipse community projects.</p>

<p>The changes introduced in EPL v2 are intended to make the license easier to use outside of the United States, more compatible with other open source licenses and modernize some of the terminology. &nbsp;The specific changes include:</p>
<ul>
<li>The reference to the choice of law being New York State has been removed. This will make it easier for open source projects based outside of the USA to use the EPL.</li>
<li>The new EPL v2 will now optionally allow EPL licensed projects to be compatible with the GPL. EPL v1 and GPL v2/v3 were not considered to be compatible making it difficult to combine EPL and GPL source code. The new EPL v2 will allow initial contributors to new projects to specify if they want their EPL v2 licensed project to be GPL compatible.</li>
<li>The new EPL v2 has been improved to make it suitable for open source projects written in scripting languages such as JavaScript by defining Source Code and ensuring that any source must always be made available under the EPL v2.</li>
<li>Other EPL v2 improvements include:</li>
<ul>
<li>The concept of &ldquo;module&rdquo; as being the scope of contribution has been changed to &ldquo;file&rdquo;, to be more consistent with other open source licenses.</li>
<li>A definition of Modified Works has been added to align the scope of the copyleft provisions with community norms.</li>
<li>The scope of the license has been expanded to include all copyrightable material, rather than the &ldquo;code and documentation&rdquo; referenced in EPLv1.</li>
</ul>
</ul>

<p>&ldquo;The new EPL v2 license was a tremendous community effort so I would like to thank everyone in the Eclipse community and the open source legal community who participated in the process.&rdquo; explains Mike Milinkovich, Executive Director of the Eclipse Foundation. &ldquo;We believe the new EPL v2 license is an excellent license that protects the rights of contributors and communities but at the same time promotes widespread commercial adoption of open source technology. We look forward to having this as the new default license for Eclipse Foundation projects, and hope to see widespread adoption by other projects and communities as well.&rdquo;</p>

<p>&ldquo;The Open Source Initiative congratulates the Eclipse Foundation on the new EPL V2 license.&rdquo; states Patrick Masson, OSI General Manager. &ldquo;It is vital that open source licenses continue to be updated in order to reflect changing technology and legal trends. The new Eclipse Public License v2 reflects the best practices in open source licensing and we are happy to certify it as an OSI approved license.&rdquo;</p>

<p>The Eclipse open source project community will begin to migrate to the new EPL v2. Each Eclipse project will need to go through process of updating their project license and license headers to specify EPL v2. Users and adopters of Eclipse projects should expect the next release of each Eclipse project will be using EPL v2.</p>

<p>Questions about the new license and the migration can be directed to <a href="mailto:license@eclipse.org">license@eclipse.org</a>.</p>

<p>About the Eclipse Foundation</p>

<p>The Eclipse Foundation is a not-for-profit organization that supports a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects with the Eclipse Foundation, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. Eclipse Foundation Working Groups allow organizations to collaborate on building innovative new technology to meet specific industry needs. &nbsp;More information is available at eclipse.org.</p>
<p>If you have any questions about this bulletin, please send an email to <a href="mailto:emo@eclipse-foundation.org.">EMO</a>.</p>

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://marketplace.eclipse.org/">Eclipse Marketplace</a></li>
				<li><a target="_blank" href="https://eclipse.org">Eclipse Homepage</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

