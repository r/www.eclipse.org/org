<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include ("_projectCommon.php"); // All on the same line to unclutter the user's
// desktop'

//
// Begin: page-specific settings. Change these.
$pageTitle = "Eclipse Foundation’s New Bylaws Are in Effect";
$pageKeywords = "eclipse, Eclipse Foundation";
$pageAuthor = "Mike Milinkovich";

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<p><strong>OTTAWA – January 7, 2020</strong> – We are pleased to announce the Eclipse Foundation’s new <a href="https://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf">Bylaws</a> went into effect on January 1, 2020. The new Bylaws reflect the broader scope in technologies currently encompassed by Eclipse projects while enabling the Foundation to attract and foster new projects and working groups focused on emerging technologies.</p>

<p>The Eclipse Foundation received the approval of its Board of Directors to update the Eclipse Foundation’s Bylaws at its October 21, 2019 Board meeting. In addition to Board approval, Bylaw changes require a vote of the Membership At-Large. The voting period ended on December 12, 2019, with the new Bylaws being ratified by a vote of 186 members (96%) in favor, and 7 members (4%) against.</p>

<p>In summary, the new Bylaws:</p>
<ul>
<li>Realign the definition of the Purposes of the Foundation to reflect our current activities. This is a key update to recognize the vast portfolio of projects, technologies, and specifications hosted by the Foundation
<li>More clearly state the rights and benefits of affiliate organizations and clarify membership dues related to those affiliates, all with the intent to make clear affiliate organizations are welcome and encouraged each to participate
<li>Remove the requirement for Solutions Members and higher to release a commercial offering, as this requirement has caused significant confusion over the years
<li>Add a requirement that approval of changes to the recently introduced Eclipse Foundation Specification Process to require a super-majority vote by the Board, thus making it consistent with the Eclipse Development Process
<li>Remove the requirement for a super-majority of the Board to approve changes to PMC Leadership (and thus require only a simple majority)
<li>Realign the quarterly reporting requirements to members to enable the EMO to more effectively disseminate information to members
<li>Clarify and modernize the roles of the Architecture and Planning Councils
</ul>
<p>We would like to thank our members for participating in the Bylaw vote. If you have any questions regarding the Bylaw changes, please contact <a href="mailto:license@eclipse.org">license@eclipse.org</a>.</p>
  	</div>
</div>

EOHTML;

// Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);