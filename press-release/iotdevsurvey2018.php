<?php
/**
 * ************************************************************************
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which accompanies
 * this distribution, and is available at http://eclipse.org/legal/epl-2.0
 * ************************************************************************
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

// Begin: page-specific settings. Change these.
$pageTitle = "Participate in the IoT Developer Survey 2018";
$Theme->setPageTitle($pageTitle);
$Theme->setPageAuthor("Roxanne Joncas");
$Theme->setPageKeywords("eclipse, iot, developer, survey, internet of things,");

// Custom Metatags
$Theme->setMetaDescription("Eclipse IoT, in partnership with the IEEE IoT Initiative, the Open Mobile Alliance, and the AGILE-IoT is pleased to launch the fourth annual IoT Developer Survey.");
$Theme->setMetaImage("https://www.eclipse.org/org/workinggroups/assets/images/wg_iot.png");
$Theme->setMetaImageHeight('122');
$Theme->setMetaImageWidth('140');

include ("_projectCommon.php");

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML
<div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    <p>
<p>The <a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a> Working Group, in partnership with the <a target="_blank" href="http://iot.ieee.org/">IEEE IoT Initiative</a>, the <a target="_blank" href="http://openmobilealliance.org/iot">Open Mobile Alliance</a>, and the <a target="_blank" href="http://agile-iot.eu/">AGILE-IoT</a> H2020 Research Project is pleased to launch the fourth annual <a target="_blank" href="https://www.surveymonkey.de/r/eclipseorg">IoT Developer Survey</a>. The purpose of the survey is to gain a better understanding and insight into how developers are building IoT solutions. The survey will run until March 5, 2018, and the results will be published in early April 2018.</p>

<p>This is the fourth year for the IoT Developer Survey. The results from the previous years (<a target="_blank" href="https://www.slideshare.net/IanSkerrett/iot-developer-survey-2017">2017</a>, <a target="_blank" href="https://www.slideshare.net/IanSkerrett/iot-developer-survey-2016">2016</a>, <a target="_blank" href="https://www.slideshare.net/IanSkerrett/iot-developer-survey-2015">2015</a>) have provided some interesting insights and trends about the IoT industry.</p>
<p>Please take the time to complete this 5-8 minute survey. It allows us to analyze the latest trends in the industry and we want to make sure it reflects everyone's point of view, including yours!</p>
<p>&rarr; <a target="_blank" href="https://www.surveymonkey.de/r/eclipseorg">Complete the survey</a> &larr;</p><br/>

<p><a target="_blank" href="https://www.surveymonkey.de/r/eclipseorg"><img class="img-responsive" src="/org/images/iot_survey_2018.png" alt="iot survey partners 2018"/></a></p>

        </div>

  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
        <li><a target="_blank" href="https://www.surveymonkey.de/r/eclipseorg">IoT Developer Survey</a></li>
        <li><a target="_blank" href="https://ianskerrett.wordpress.com/2017/04/19/iot-developer-trends-2017-edition/">Survey Results 2017</a></li>
      </ul>
    </div>
  </div>
</div>
EOHTML;

$Theme->setHtml($html);
$Theme->generatePage();
