<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse Foundation Collaboration Yields Open Source Technology for Computational Science";
  $pageKeywords = "eclipse, eclipsescience, science, working groups, SWG";
  $pageAuthor   = "Kat Hirsch";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    		<p><strong>Ottawa, Canada – October 21, 2016</strong> - <i>Eclipse Foundation Collaboration Yields Open Source Technology for Computational Science</i>.</p>

			<p>The gap between the computational science and open source software communities just got smaller – thanks to a collaboration among national laboratories, universities and industry.</p>

			<p>The <a href="https://science.eclipse.org/">Eclipse Science Working Group (SWG)</a>, a global community for individuals and organizations who collaborate on commercially-friendly open source software, today released five projects aimed at expediting scientific breakthroughs by simplifying and streamlining computational science workflows.</p>

			<p>While computational science has made a great impact across the scientific spectrum, from climate change to astrophysics to genomics, today’s data-centric simulation environments require enhanced analysis, visualization, and workflow management to further reduce time to solution and increase the frequency of research breakthroughs. Hence Eclipse’s software release.</p>

			<p>“Oak Ridge National Laboratory is renowned for its hardware - like Titan, the world’s third fastest supercomputer - but we are also leaders in scientific software development.” said Jay Jay Billings, the SWG Chair and a research scientist at Oak Ridge National Laboratory. “ The Science Working Group has not only helped us create software to simplify complex computational science but it has also allowed us to become part of a thriving community that focuses on collaboration, interoperability, and cutting edge research .”</p>

      <p>Other U.S. National Laboratories are also benefitting from the Science Working Group.</p>

			<p>“The Eclipse Science Working Group provides an open, international forum for design and development of critical components of scientific software, bringing together a diverse set of application requirements drivers and software architects to address the challenges of next-generation scientific computing,” says Robert Clay of Sandia National Laboratory.</p>

      <p>Likewise, small businesses are also engaged as either members of the group or consumers of its software. Dr. Gerald Sabin from RNET Technologies, Inc., a Dayton Ohio-area firm focused on high-performance computing research, said, "The ICE workflow environment is an ideal platform for us to develop a cutting edge, cloud-based collaboration environment to support scientific computing. The licensing model and openness of the Science Working Group is to our advantage.”</p>

      <p>The open source projects, which represent years of development and thousands of users, are the product of intense collaboration among SWG members including Oak Ridge National Laboratory (ORNL), Diamond Light Source, Itema AS, iSencia, Kichwa Coders, Lablicate GmbH and others. The five projects released today are:</p>
      <ul>

      <li>The <a href="https://www.eclipse.org/ice/">Eclipse Integrated Computational Environment (ICE)</a>, led by Billings, is a scientific workbench and workflow environment developed to improve the user experience for computational scientists and make it possible for developers to deploy rich, graphical, interactive capabilities for their science codes and integrate many different scientific computing technologies in one common, cross-platform user environment. It is easily extended to new problems and has been successfully deployed for additive manufacturing, advanced batteries, advanced materials, nuclear energy, neutron science, quantum computing, and other areas.</li>
      <li>The <a href="http://projects.eclipse.org/projects/technology.eavp">Eclipse Advanced Visualization Project</a>, likewise led by Billings, provides advanced visualization technologies for plotting, geometry and meshing and 3D visualization to address the needs of projects in the working groups. It provides a simple framework for exposing and developing visualization services.</li>
      <li><a href="http://projects.eclipse.org/projects/technology.january">January</a>, led by Jonah Graham of Kichwa Coders and Peter Chang of Diamond Light Source, is a set of libraries for handling scientific data in Java. It includes support for n-dimensional arrays, hierarchical data, and 3D geometry and mesh data. Originally created as a spin-off of other group projects to provide a common data structure library, January promises to provide a large portion of the common code base of the group in the future.</li>
      <li><a href="http://projects.eclipse.org/projects/technology.triquetrum">Triquetrum</a>, led by Erwin De Ley of iSencia and Christopher Brooks of the University of California, Berkeley, delivers an open platform for managing and executing scientific workflows. The goal of Triquetrum is to support a wide range of use cases, from automated processes based on predefined models to replaying ad-hoc research workflows recorded from a user's actions in a scientific workbench UI. </li>
      <li><a href="http://projects.eclipse.org/projects/technology.chemclipse">Chemclipse</a>, led by Philip Wenig of Lablicate GmbH, supports users who analyse data acquired from systems used in analytical chemistry. In particular, chromatography coupled with mass spectrometry (GC/MS) or flame-ionization detectors (GC/FID) is used to identify and/or monitor chemical substances, which is important for quality control issues.</li>
       </ul>

       <p>“Open source is having tremendous impact on both productivity and innovation in industry, government, and academia,” explained Mike Milinkovich. “The Eclipse Science Working Group is a great example of how world-leading scientific research organizations like Oak Ridge National Laboratory, Diamond Light Source, and others can collaborate on delivering free software that will enable the next major scientific breakthrough. This is an important advancement for the scientific research community, and we are proud to be hosting the collaboration at the Eclipse Foundation.”</p>

       <p>More information on the Science Working Group is available on their <a href="https://science.eclipse.org/">website</a>.</p>

        <h3>Downloads</h3>
        <ul>
        <li><a href="https://www.eclipse.org/ice/">Eclipse Integrated Computational Environment (Eclipse ICE)</a></li>
       	<li><a href="http://projects.eclipse.org/projects/technology.eavp">Eclipse Advanced Visualization Project (EAVP)</a></li>
    		<li><a href="http://projects.eclipse.org/projects/technology.january">Eclipse January</a></li>
    		<li><a href="http://projects.eclipse.org/projects/technology.triquetrum">Eclipse Triquetrum</a></li>
    		<li><a href="http://projects.eclipse.org/projects/technology.chemclipse">Eclipse Chemclipse</a></li>
        </ul>

			<p><i>UT-Battelle manages ORNL for the DOE's Office of Science. The DOE Office of Science is the single largest supporter of basic research in the physical sciences in the United States and is working to address some of the most pressing challenges of our time. For more information, visit the Office of Science <a target="_blank" href="http://science.energy.gov/">website</a>.</i></p>

    		<h3>About the Eclipse Foundation</h3>

        <p>Eclipse is a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects in the Eclipse community, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. The Eclipse Foundation is a not-for-profit foundation that is the steward of the Eclipse community. More information is available at <a href="https://www.eclipse.org">www.eclipse.org</a></p>

  </div>

  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
         <li><a href="https://www.eclipse.org/ice/">Eclipse ICE</a></li>
       	<li><a href="http://projects.eclipse.org/projects/technology.eavp">EAVP</a></li>
    		<li><a href="http://projects.eclipse.org/projects/technology.january">Eclipse January</a></li>
    		<li><a href="http://projects.eclipse.org/projects/technology.triquetrum">Eclipse Triquetrum</a></li>
    		<li><a href="http://projects.eclipse.org/projects/technology.chemclipse">Eclipse Chemclipse</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
