<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "XtextCON - Three days of Xtext";
	$pageKeywords	= "eclipse, xtext, xtextcon";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

			<a target="_blank" href="http://xtextcon.org/"><img src="/org/images/logo-red-small.png" width="50%"/></a><br/>
			<p><a target="_blank" href="http://xtextcon.org/">XtextCON</a> is the place for new users to learn Xtext quickly and for experienced language designers to understand advanced use cases. Get in touch with Xtext users and the developers behind it. Discuss bugzilla and feature requests and get your individual problems solved. The second edition of XtextCON 2015, is once again taking place in Kiel, located on the Baltic Sea coast in northern Germany.</p>
			
			<p>XtextCON is a two-day event, taking place May 19-20, preceded by an optional full workshop day for beginners and those who need an update. The conference is composed of two tracks, with an additional special clinic track in parallel, where attendees get help and advice on individual Xtext-related problems. <a target="_blank" href="http://xtextcon.org/#program">The program is available here</a>.</p>
			
			<p><a target="_blank" href="http://xtextcon.org/#register">Registration is now open</a>. Don't wait to register, last year's conference was sold out three weeks in advance!</p>		
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://xtextcon.org/#program">XtextCON Program</a></li>	
				<li><a target="_blank" href="http://xtextcon.org/#register">XtextCON Registration</a></li>				
				<li><a href="https://eclipse.org/Xtext/">Xtext website</a></li>		
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

