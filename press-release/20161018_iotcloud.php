<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "New Industry Collaboration to Develop Interoperable IoT Components for the Cloud";
  $pageKeywords = "eclipse, eclipseiot, internet of things, iot, interoperable iot, cloud computing";
  $pageAuthor   = "Eric Poirier";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    		<p><strong>Ottawa, Canada – October 18, 2016</strong> - The Eclipse Foundation announces a new  collaboration between Internet of Things (IoT) industry leaders to develop interoperable IoT components that can be deployed to a Cloud platform. Bosch Software Innovations, Eurotech and Red Hat will be the initial participants in the collaboration. The collaboration will be part of the existing <a href="http://iot.eclipse.org/">Eclipse IoT Working Group</a>, a community of 26 open source IoT projects, hosted by the Eclipse Foundation.</p>
			
			<p>IoT platform components provide capabilities that are useful for developing and managing IoT solutions. These commonly include components to implement secure, reliable communication between devices and services, manage and update devices, work with device data and integrate device information into existing enterprise applications. Such components may also be delivered as services on top of an underlying cloud infrastructure platform, like Cloud Foundry or OpenShift.</p>
			
			<p>The initial release of the Eclipse Kapua source code, contributed by Eurotech and Red Hat, is the first important milestone on the roadmap of this collaboration. Eclipse Kapua offers a core integration framework and an initial set of modular components for device registry, device management, message routing, data management, and application enablement. As a next step, Eclipse Hono, a Bosch Software Innovations-initiated project that enables secure, reliable communication between devices and services, will be integrated as an extension to Kapua.</p>
			
			<p>In the future it is expected that other Eclipse IoT projects, other open source projects and commercial vendors will provide additional components that integrate with Kapua and Hono. This will lead to an ecosystem of IoT platform core components that work well together and that can be used to bootstrap IoT applications and solutions.</p> 
			
			<p>The work will be done through the Eclipse IoT Working Group and the Eclipse IoT open source projects. Other organizations are welcome and encouraged to join the collaboration.</p>
			
			<p>In parallel, GE Digital, which recently joined the Eclipse Foundation, has proposed a new project called Eclipse ACS. Eclipse ACS implements a simple, granular access control framework that makes it possible for IoT developers to integrate access control security into their IoT applications.</p>
			
			
			<h4>Bosch</h4>
			<p>"Bosch is a major industrial IoT company that supports a wide range of devices, in many different industries, such as automotive, manufacturing and home appliances. We believe the best way to support this complex environment is to base our commercial IoT platform, the Bosch IoT Suite, on open source components and open standards. These projects establish a horizontal open technology for IoT and provide the technical breeding grounds for successful business ecosystems."<br> 
    		Dr. Stefan Ferber, VP of Engineering, Bosch Software Innovations</p>
			
			
			<h4>Red Hat</h4>
			<p>"To best address the complexity of industrial IoT, solutions and infrastructure providers must maintain a high level of collaboration, which is really only sustainable through an open source community. The Eclipse Kapua project, started by joint work between Red Hat and Eurotech, provides this open and transparent community approach and aims to help solve the complexities of the IoT by providing the required level of interoperability, scale, and flexibility in open IoT platforms."<br>
			James Kirkland, Chief Architect for IoT, Red Hat</p>
			
			
			<h4>Eurotech</h4>
			<p>"Our commitment to open-source software is pivotal to our strategy in IoT. We consider our leadership in Eclipse Kapua as the logical next step to our previous contribution and commitment into Eclipse Kura, the open source application framework for IoT gateways. For Eurotech, having freely available production quality IoT software frameworks for gateways and for cloud platforms creates a larger opportunity to deliver end-to-end IoT products and solutions including Everyware Cloud and Eurotech IoT Gateways."<br>
			Marco Carrer - Eurotech, CTO</p>
			
			<p>More information about <a href="https://www.eclipse.org/kapua/">Eclipse Kapua</a>, <a href="https://projects.eclipse.org/projects/iot.hono">Eclipse Hono</a>, and <a href="https://projects.eclipse.org/proposals/access-control-service-acs-0">Eclipse ACS</a> is available from each project website.</p>
			
			    		
		<h3>About Eclipse IoT</h3>
    		<p>The Eclipse IoT Working Group is an industry collaboration, hosted at the Eclipse Foundation, that is focused on building open source IoT technology. Eclipse IoT has 26 open source projects building technology for IoT devices, IoT gateways and IoT cloud platforms.</p>	
       
    		<h3>About the Eclipse Foundation</h3>

        <p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>

        <p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="https://www.eclipse.org">www.eclipse.org</a>.</p>
  </div>

  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
          	<li><a href="http://iot.eclipse.org/">Eclipse IoT</a></li>
       		<li><a href="https://projects.eclipse.org/proposals/access-control-service-acs-0">Eclipse ACS</a></li>
    		<li><a href="https://projects.eclipse.org/projects/iot.hono">Eclipse Hono</a></li>
    		<li><a href="https://www.eclipse.org/kapua/">Eclipse Kapua</a></li>
    		<li><a href="http://iot.eclipse.org/resources/white-papers/Eclipse%20IoT%20White%20Paper%20-%20The%20Three%20Software%20Stacks%20Required%20for%20IoT%20Architectures.pdf">The Three Software Stacks Required for IoT Architectures</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
