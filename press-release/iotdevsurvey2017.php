<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Participate in the IoT Developer Survey 2017";
	$pageKeywords	= "eclipse, iot, developer, survey";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<p>The Eclipse IoT Working Group, in partnership with the <a target="_blank" href="http://www.theinternetofthings.eu/">IoT Council</a>, <a target="_blank" href="http://iot.ieee.org/">IEEE IoT</a>, and the <a target="_blank" href="http://agile-iot.eu/">AGILE-IoT</a> research project, is pleased to launch the third annual IoT Developer Survey. The purpose of the survey is to gain a better understanding and insight into how developers are building IoT solutions. The survey will run until March 17, 2017 and the results will be published in early April 2017.</p> 

		<p>This is the third year for the IoT Developer Survey. The results from the previous years (2016, 2015) have provided some interesting insights and trends about the IoT industry.</p>
		
		<p>Please take the time to complete this 5-8 minute survey. It not only allows us to analyze the latest trends, but it gives you a chance to get your voice heard. The results and analysis will be published in early April 2017.</p>
		
		<p><a target="_blank" href="https://www.surveymonkey.com/r/iotsurvey2017eclipse">Complete the survey.</a></p>
				
		<p><a target="_blank" href="https://www.surveymonkey.com/r/iotsurvey2017eclipse"><img class="img-responsive" src="/org/images/iotsurveypartners.png" alt="iot survey partners 2017"/></a></p>		

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.surveymonkey.com/r/iotsurvey2017eclipse">IoT Developer Survey</a></li>
				<li><a target="_blank" href="https://ianskerrett.wordpress.com/2016/04/14/profile-of-an-iot-developer-results-of-the-iot-developer-survey/">Survey Results 2016</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

