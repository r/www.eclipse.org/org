<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Participate in the IoT Developer Survey";
	$pageKeywords	= "eclipse, iot, developer, survey";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
		
			<p>The Eclipse IoT Working Group has launched the <a target="_blank" href="https://www.surveymonkey.com/s/IOT3">IoT Developer Survey</a>. The goal is to better understand how developers view the Internet 
				of Things (IoT), the relevant IoT standards and vendors.  The survey is 18 questions and should take 5-10 minutes to complete. 
				The survey will be open until March 23. At that time a report will be published based on the results.</p> 

			<p>All participants will qualify to enter a draw for $100 gift card. Please take the time to 
				<a target="_blank" href="https://www.surveymonkey.com/s/IOT3">participate in the survey</a> and provide us your input.</p>
				
							
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.surveymonkey.com/s/IOT3">IoT Developer Survey</a></li>		
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

