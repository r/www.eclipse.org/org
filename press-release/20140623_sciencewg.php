<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "New Global Collaboration for Scientific Software Announced by the Eclipse Foundation";
	$pageKeywords	= "eclipse, foundation, eclipsecon, france, keynote, program, 2014";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

<p>Ottawa, Canada, June 23, 2014 – The Eclipse Foundation today announced the creation of a new Science Working Group — a global collaboration 
between industry, academia, and governments to create open source software used in basic scientific research. Founding members of the Eclipse Science 
Working Group include Oak Ridge National Labs, Diamond Light Source, Lablicate UG, Uppsala University, Facility for Rare Isotope Beams, MARINTEK, 
Kichwa Coders, Clemson University, and Tech’Advantage.</p>

<p>According to <a href="http://www.battelle.org/docs/tpp/2014_global_rd_funding_forecast.pdf?sfvrsn=4">Battelle</a>, approximately $1.6 trillion dollars are spent each year on scientific research and development. As with many industries, 
software is becoming increasingly important to the success of applied and basic scientific research. Due to the nature of scientific research, much of 
the software is created with a single, specific purpose or even a single experiment. The lack of reusable software components can lead to increased 
errors, costs and schedules. As software for simulation and visualization becomes an ever-greater part of basic scientific research, the need for well 
understood, well documented components increases. The Eclipse Science Working Group aims to create a set of open source software tools, components and 
systems that can be used by the wider scientific research community.</p>

<p>The founding Steering Committee level members of the Science Working Group (SWG) include Oak Ridge National Laboratory in Oak Ridge Tennessee, and 
Diamond Light Source in Oxfordshire, UK. The group will collaborate on producing technologies used for interdisciplinary analysis of scientific data. 
The SWG plans to collaborate on open source software projects that will:
<ul>
<li>plot and visualize 1D, 2D, and 3D data, and process and manage data for structured and unstructured grids;
<li>develop modeling and simulation software for physical and social sciences, such as physics, chemistry, biology, sociology, psychology and others;
<li>provide standard descriptions and definitions for scientific data; and
<li>create infrastructure software to support scientific computing, such as tools job launching and monitoring, parallel debugging, and remote project management.
</ul>
</p>

<p>The first SWG open source projects are based on code contributions from Oak Ridge National Laboratory and Diamond Light Source:
<ul>

<li>The Eclipse Integrated Computational Environment (ICE) is a platform for modeling and simulation projects in science and engineering. It provides a standard set of tools for project scientists to set up the input model, launch a simulation job, analyse the results, and manage the input and output data. The code is based on technology created at Oak Ridge National Laboratory to develop a computational environment for modeling and simulation of nuclear reactors.
<li>Eclipse DawnSci defines Java interfaces for data description, plotting and plot tools, data slicing and file loading. The project aims to provide interoperability of algorithms between different scientific projects. It is based on a code contribution by Diamond Light Source.

</ul>
</p>

</p>‘Software is becoming increasing important in all facets of society, including scientific research. So creating efficiencies in the scientific 
software development lifecycle is becoming increasingly important. We are thrilled to have world renowned research organizations, like Oak Ridge 
National Labs and Diamond Light Source, recognize that Eclipse Working Groups are a recognized way to encourage collaborative development and sharing 
of software assets,’ explains Mike Milinkovich, executive director of the Eclipse Foundation. ‘The new Eclipse Science Working Group is off to a good 
start for creating a community for the scientific software development.’

<p>The Eclipse Science Working Group is open to any company, government agency, university and research institution that participates in basic 
scientific research. Details about the joining the working group are available at <a href="http://science.eclipse.org/">science.eclipse.org</a> </p>


</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

