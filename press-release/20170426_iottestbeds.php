<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "New Eclipse IoT Open Testbeds to Drive Industry Adoption of Open Source and Open Standards in IoT";
	$pageKeywords	= "eclipse, iot, internet of things, testbeds, open source, industry";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p><i>Red Hat, Eurotech, Samsung, Azul Systems, and Codenvy Collaborate in the First Testbed</i></p>

<p><b>Ottawa, Canada - April 27, 2017</b> -  The Eclipse Internet of Things (IoT) Working Group, an Eclipse Foundation collaborative working group, is pleased to announce the creation of the <a href="https://iot.eclipse.org/testbeds/">Eclipse IoT Open Testbeds</a>, a new initiative to drive adoption of IoT open source and IoT open standards. The Testbeds will showcase and demonstrate how open source software and open standards along with commercial solutions can be used to create real-world, industry-specific IoT solutions.</p>

<p>Companies implementing IoT solutions are often required to integrate technology from many vendors and open source communities. A typical IoT solution will require specialized sensors and actuators, embedded software, IoT gateways, networking and connectivity solutions, cloud computing and enterprise integration software, and specialized application knowledge. The Eclipse IoT Open Testbeds are collaborations between vendors and open source communities to demonstrate and test commercial and open source components needed to create specific industry solutions. Each testbed will deliver a running solution and make its source code available under an open source license.</p>

<p>The first Open Testbed is Asset Tracking Management. Many high-value assets move through airports, railways, and trucks. These assets need to be monitored in real-time for location and environmental conditions, like temperature, vibration, humidity, etc. The Asset Tracking Management testbed will demonstrate and test how assets with various sensors can be tracked via different IoT gateways that report in real time the data to an IoT cloud platform, or use the events to automate control of various systems. The participants in the testbed will be Red Hat (lead), Azul Systems, Eurotech, Samsung, and Codenvy. The companies will showcase how their commercial offerings support open source and open standards. The testbed will also showcase several Eclipse IoT projects, including Eclipse Kura, Eclipse Kapua, and Eclipse Paho.</p>

<p>"Collaboration between different technology providers is essential for the deployment of any IoT solution. The Eclipse IoT Open Testbeds will showcase integrations of open source and commercial software to create end-to-end IoT solutions,” explains Mike Milinkovich, executive director of the Eclipse Foundation. "A key benefit will be that the source code will be available as open source, so each testbed can be recreated and reused. It is exciting to have Red Hat lead our first testbed, and showcase it at their upcoming Red Hat Summit.”</p> 

<p>The debut of the <a href="https://iot.eclipse.org/testbeds/asset-tracking/">Asset Tracking Management Testbed</a> will be at the Red Hat Summit on May 2-4, 2017. The Eclipse IoT Working Group, along with Red Hat, Eurotech, and Codenvy, will be showing live demonstrations of the testbed running on Samsung ARTIK™ board, Eurotech IoT Gateways, Azul Zulu, and Red Hat OpenShift Container Platform in the Red Hat IoT booth in the Partner Pavilion.</p> 

<p>The Eclipse IoT community expects to launch other testbeds in 2017. More information about the Eclipse Open IoT Testbeds can be found at <a href="https://iot.eclipse.org/testbeds">https://iot.eclipse.org/testbeds</a>. Companies interested in showcasing their support of IoT open source and open standards are welcome to participate in the Asset Tracking Management testbed and any new testbeds.</p>

<p><b>Quote from Red Hat</b><br>
"The innovation required by enterprise IoT cannot be done in siloed, isolated projects; rather, it requires collaboration across an entire community to achieve a common goal,” says James Kirkland, chief architect, IoT, at Red Hat. "The Eclipse IoT Open Testbeds initiative helps to address this need by encouraging joint work on open, standards-based IoT projects. It’s an effort that will be critical to the future success of IoT in the enterprise, and an endeavor that Red Hat is very happy to support."</p>

<p><b>Quote from Samsung</b><br>
"As one of the first IoT companies to join the Eclipse Foundation, we’re excited to see the SAMSUNG ARTIK™ Smart IoT platform be a part of the Eclipse Open Testbed initiative to drive adoption of open source and open standards for IoT," says Curtis Sasaki, VP of ecosystems, Samsung Strategy and Innovation Center. "As a contributor and deployer of solutions leveraging open source, we’re excited to see our IoT platform help developers and companies jump-start their ideas and take them to market faster."</p>

<p><b>Quote from Eurotech</b><br>
"We are excited to be part of the Eclipse IoT Open Testbeds initiative together with our partners. Showcasing real-world applications is extremely effective in highlighting how open source IoT solutions and standards are providing a solid foundation of building blocks to accelerate the development and deployment of connected products and services," says Marco Carrer, CTO, Eurotech Group.</p>

<p><b>Quote from Azul Systems</b><br>
"Eclipse and its partners have taken a major step forward in driving an open source ecosystem for companies and others building IoT applications and services," says Scott Sellers, Azul Systems president and CEO. "Azul is pleased to support Zulu Embedded, our 100% open source high-performance build of OpenJDK, as part of the Eclipse IoT Open Testbed initiative."</p>

<p><b>Quote from Codenvy</b><br>
"Eclipse Che adds critical developer tooling to the Eclipse IoT Open Testbeds -- supporting development on C/C++, Java, JS, Python and many other languages through a powerful, browser-based IDE. Developers and makers are at the heart of IoT, and Che, as the most popular open cloud IDE, is a natural choice for powering the development of real-world IoT applications. As project lead for Eclipse Che, Codenvy has been working closely with Red Hat, Samsung, and others to advance the capabilities for IoT developers around the world," says Brad Micklea, Codenvy COO.</p>

<h3>About Eclipse IoT and Eclipse Foundation</h3>
<p>The Eclipse IoT Working Group, a collaborative working group hosted by the Eclipse Foundation, is working to create open source software for IoT solutions. Eclipse IoT is made up of over 30 member companies and 28 open source projects.</p>
Eclipse is a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects in the Eclipse community, ranging from tools for software developers, geo-spatial technology, systems engineering and embedded development tools, frameworks for IoT solutions, tools for scientific research, and much more. The Eclipse Foundation is a not-for-profit foundation that is the steward of the Eclipse community. More information is available at <a href="https://www.eclipse.org/">eclipse.org</a>.</p>

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://iot.eclipse.org/testbeds/">Eclipse IoT Open Testbeds</a></li>
				<li><a href="https://iot.eclipse.org/testbeds/asset-tracking/">Asset Tracking Management Testbed</a></li>
				<li><a href="https://iot.eclipse.org/">Eclipse IoT</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

