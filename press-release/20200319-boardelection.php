<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Eclipse Foundation Board Election Results";
$pageKeywords	= "eclipse, election, election results, Eclipse Foundation";
$pageAuthor		= "Mike Milinkovich";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
				
<p><strong>Ottawa, Canada - March 19, 2020</strong> - Today we are pleased to announce the results of the Eclipse Foundation Sustaining Member and Committer Member elections for representatives to our Board of Directors. The positions of Sustaining Members and Committer representatives are a vitally important part of the Eclipse Foundation's governance.</p>
<p>Congratulations to Farah Papaioannou, Gunnar Wagenknecht, and Torkild Ulvoy Resheim who are all returning as Sustaining Member representatives. Chris Aniszczyk, Dani Megert, and Ed Merks will also all be returning as Committer representatives. We're looking forward to working with you on the Eclipse Foundation Board of Directors, effective April 1.</p>
<p>We would like to thank Lisa Ruff and Angelo Corsaro for running for election.</p>
<h3>About the Eclipse Foundation</h3>
				
<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 375 open source projects, including runtimes, tools, and frameworks for a wide range of technology domains such as IoT, edge computing, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 300 members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter <a href="https://www.twitter.com/@EclipseFdn" target="_blank">@EclipseFdn</a>, <a href="https://www.linkedin.com/company/eclipse-foundation" target="_blank">LinkedIn</a> or visit <a href="https://www.eclipse.org">eclipse.org</a>.</p>
				
  	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);