<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include ("_projectCommon.php"); // All on the same line to unclutter the user's
// desktop'

//
// Begin: page-specific settings. Change these.
$pageTitle = "IOTA and the Eclipse Foundation Launch Working Group to Develop Commercial Applications on Distributed Ledger Technology";
$pageKeywords = "eclipse, Eclipse Foundation";
$pageAuthor = "Mike Milinkovich";

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<p><em>New Tangle EE Working Group Brings Together Fifteen Diverse Organizations to Foster Data and Payment Innovation on Distributed Ledger Technology (DLT)</em></p>

<p><strong>OTTAWA and BERLIN, February 11, 2020</strong> -- The IOTA Foundation, a non-profit organization developing the world&#39;s first scalable, feeless and decentralized distributed ledger technology (DLT) and the Eclipse Foundation, an open-source software foundation for commercial innovation and collaboration, today announced the launch of the <a href="https://tangle.ee">Tangle EE Working Group</a> (WG). Established under the Eclipse Foundation&#39;s proven governance framework and processes, the Tangle EE WG unites leading companies and academics in developing IOTA-based data and payment solutions for commercial use.</p>

<p><a href="https://www.delltechnologies.com/en-us/index.htm">Dell Technologies</a> and <a href="https://www.st.com/content/st_com/en.html/">STMicroelectronics</a> are among the founding members of the new working group, along with 13 other member organizations: <a href="https://www.softwareag.com/">Software AG</a>, <a href="https://www.omg.org/">Object Management Group</a>,<a href="https://energinet.dk/">&nbsp; </a><a href="https://accessec.com/">accessec</a>, <a href="https://energinet.dk/">Energinet</a>,<a href="https://biilabs.io/"> BiiLabs</a>,<a href="https://www.calypsonet-asso.org/"> Calypso Network Association</a>,<a href="http://www.geometricenergy.ca/"> </a><a href="https://www.engie.com/en/innovation-transition-energetique/centres-de-recherche/crigen">ENGIE Lab CRIGEN</a>, <a href="https://www.rwth-aachen.de/">RWTH Aachen University</a>,<a href="https://www.iiconsortium.org/"> </a><a href="https://m2m.akitablock.io/">AKITA</a>, <a href="https://www.geometricenergy.ca/">Geometric Energy Corporation</a>,<a href="https://www.tmforum.org/"> TMForum</a>, <a href="http://www.ovgu.de/">Otto von Guericke University Magdeburg</a> and <a href="https://iotify.io/">IoTIFY</a>.</p>

<p>Several projects will be formed under the Eclipse Foundation to drive open-source collaboration and commercial adoption. Business and academia will come together to develop tooling and provide thought-leadership in key IOTA use case areas. The first two projects will explore <a href="https://industrymarketplace.net/">decentralized marketplaces</a>, which facilitate real-time trading of data and services, and <a href="https://blog.iota.org/the-first-step-towards-a-unified-identity-protocol-7dc3988c8b0e">decentralized identity</a>, which enables a unified identity for people, organizations and devices.</p>

<p>IOTA&rsquo;s distributed ledger has already been used in a variety of sectors including<a href="https://www.iota.org/verticals/mobility-automotive"> Mobility and Automotive</a>,<a href="https://www.iota.org/verticals/global-trade-supply-chains"> Global Trade and Supply Chain</a>,<a href="https://www.iota.org/verticals/ehealth"> eHealth</a>,<a href="https://www.iota.org/verticals/smart-energy"> Smart Energy (including Smart Cities)</a>, and<a href="https://www.iota.org/verticals/industrial-iot"> Industrial IoT</a>.</p>

<p>&ldquo;The Eclipse Foundation will provide a vendor-neutral governance framework for open collaboration, with IOTA&rsquo;s scalable, feeless, and permissionless DLT as a base,&rdquo; said Mike Milinkovich, executive director of the Eclipse Foundation. &ldquo;By doing so, we will accelerate the development of new applications built with this transformative technology.&rdquo;&nbsp;</p>

<p>&ldquo;As an open-source project, we realize the importance of increasing access to IOTA&rsquo;s technology,&rdquo; said David S&oslash;nsteb&oslash;, IOTA co-founder. &ldquo;This is a significant step forward for the IOTA Foundation and our ecosystem to develop a diverse, global community of contributors and adopters collaborating on production-ready commercial applications.&rdquo;</p>

<p>&ldquo;As IOTA&#39;s technologies continue to be adopted in commercial use cases, this transparent and open governance framework will be essential in regulating code development, ensuring market fit, and fulfilling our promises of openness, interoperability, and sustainability. This will elevate IOTA to production-readiness,&rdquo; said Dominik Schiener, co-founder of IOTA Foundation.</p>

<h2>The TangleEE working group will continue to grow and is actively recruiting new members.</h2>

<p>For more information and how to get involved visit: <a href="https://tangle.ee">https://tangle.ee</a></p>

<p>See a video on the announcement here: <a href="https://youtu.be/R6E3HnK9Ycw">https://youtu.be/R6E3HnK9Ycw</a></p>

<h2>About IOTA Foundation</h2>

<p>IOTA is a global non-profit foundation supporting the research and development of new distributed ledger technologies (DLT), including the IOTA Tangle.</p>

<p>The IOTA Tangle solves the fundamental shortcomings of blockchain: scalability, environmental sustainability and cost. IOTA is an open-source protocol connecting the human economy with the machine economy by facilitating novel Machine-to-Machine (M2M) interactions, including secure data transfer and feeless micropayments. To learn more visit <a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=http%3A%2F%2Fwww.iota.org&amp;esheet=52017038&amp;newsitemid=20190723005376&amp;lan=en-US&amp;anchor=www.iota.org&amp;index=6&amp;md5=b229cd2a6061918de5b29717b6e374b3">www.iota.org</a>, the <a href="https://cts.businesswire.com/ct/CT?id=smartlink&amp;url=https%3A%2F%2Fwww.youtube.com%2Fchannel%2FUClxDa0qkOqxIguokXPhnuOA%3Fview_as%3Dsubscriber&amp;esheet=52017038&amp;newsitemid=20190723005376&amp;lan=en-US&amp;anchor=IOTA+Foundation&amp;index=7&amp;md5=a8a142783aefc9dd8466a77d0a812843">IOTA Foundation</a> Youtube channel and follow @iotatoken on Twitter.</p>

<h2>About Eclipse Foundation</h2>

<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable and commercially-focused environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 375 open source projects, including runtimes, tools, and frameworks for a wide range of technology domains such as IoT, edge computing, automotive, geospatial, systems engineering and many others. The Eclipse Foundation is a not-for-profit organization supported by over 275 members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter<a href="https://www.globenewswire.com/Tracker?data=pqjWFOdOQGxOyf1xE3vWLwTkfCCBofpc0Yfcc8gPQ_6unjvtebpg-QdGVTVw2SDW5ep4ijFSZXsnEkfS1Mo6Nu13P0kErW5UkNpNOlGKwDBB4yKQpowRqqbnom-nt3qO7ulCwV_9ckdJ3JteV5CE4sTCiuFa_JpDrXQVKlcijdJthA6iMRy45gatrsUOAyHcGsT_FKLIOzYhN2v2m4fDx5rDSQxXlHiTfwKBtdgXxls="> @EclipseFdn</a>,<a href="https://www.globenewswire.com/Tracker?data=oMd832NiFWUcnI74AERapMD3LoyQ5h3SGqVhB-HJNaliE4LTpqWh4_nwyLcJ9vdFVhUjcf1QkqL956yqdORo6hKv35i5WoS_77x7O50QS6qiyc2vD5NLD4nii4abupG3KfQRcXDMB7viDmrtKW1arnFefGdpsRtE-JuaNEuH4XScI04T97GPdMR9y7rM2nAHoc4RdtgTfhJwVRuUA4ePMC87PlXdr2ixj5jfffSGxrulufOnS2ErvDaTOsfAr5zKcot98Fqaf_5_SNDqvIWGIA=="> LinkedIn</a> or visit<a href="https://www.globenewswire.com/Tracker?data=14CIlIem4SSS_pV35saVr6NcqeJh4qQ0fARwu6M1bKPh6gH8hK9q8uf9NKcKUQcgntyNM7aWX1045dpVoKCbQt3LFmr1TA7yfNSqmuRn63wanaFC4GBa8a_sUaS41Ayrwgza8EymxrUEIsbrSF-kpxZBLpiLvWNtRrHKfN4PDV1lg535BtTkJgtquvIkbAcUooCBWu4ToJaisKYRSVL3UffOthHpM-Usr16lnsUQ0j0="> eclipse.org</a>.</p>

<h2>Media Contacts:</h2>

<p>IOTA Foundation:<br />
Cara Harbor<br />
<a href="mailto:cara.harbor@iota.org">cara.harbor@iota.org</a></p>

<p>Eclipse Foundation:<br />
Jay Nichols<br />
Nichols Communications on behalf of the Eclipse Foundation<br />
<a href="mailto:jay@nicholscomm.com">jay@nicholscomm.com</a><br />
408.772.1551</p>

  	</div>
</div>

EOHTML;

// Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);