<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse Neon Release Train Now Available";
  $pageKeywords = "eclipse, neon";
  $pageAuthor   = "Eric Poirier";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    <p><em>Eclipse ships eleventh annual release train featuring 84 projects and 69 million lines of code</em></p>
    <p><strong>Ottawa, Canada – June 22, 2016</strong> - The Eclipse Foundation is proud to
        announce the availability of the Neon release, the annual release
        train from the Eclipse community. This is the eleventh year the
        Eclipse community has shipped a coordinated release of multiple
        Eclipse projects. There are 84 projects in the Neon release,
        consisting of over 69 million lines of code, with contribution by
        779 developers, 331 of whom are Eclipse committers. Each release
        train is a collective accomplishment wherein the community has
        planned, developed, and delivered a scheduled, coordinated release
        that allows users and adopters to update their Eclipse technology
        in one instance.</p>

    <p>“It takes a great amount of coordination and effort by many developers
        within our community to ship a release that is on-time,” said Mike
        Milinkovich, Executive Director of the Eclipse Foundation. “Extending
        congratulations to all of the Eclipse committers, projects, and
        Foundation staff that made Neon possible, ensuring we remain a well
        adopted and trusted supplier of Open Source Technology.”</p>

        <h3>Key Highlights of Neon Release</h3>


        <ul>
          <li><strong>Eclipse JSDT 2.0:</strong>New tools for JavaScript developers, including a JSON editor, support for Grunt/Gulp and a new Chromium V8 Debugger.</li>
          <li><strong>Platform and JDT Features:</strong> Key Eclipse Platform improvements are HiDPI support and autosave. JDT's Content Assist now highlights matched characters and provides substring completion.</li>
          <li><strong>Updated PHP Development Tools Package (PDT):</strong> New Eclipse PDT 4.0 release for PHP developers with complete support for PHP 7 and improved performance.</li>
          <li><strong>Automated Error Reporting (AERI):</strong> The Eclipse Automated Error Reporting client can now be integrated into any third-party Eclipse plug-in or standalone RCP application.</li>
          <li><strong>Docker Tooling:</strong> Introducing improved support for Docker Tooling.</li>
          <li><strong>Eclipse User Storage Service (USS):</strong> Introducing the Eclipse USS, a new storage service from the Eclipse Foundation, that allows projects to store and retrieve user data and preferences from our servers creating a better User Experience (UX) for developers.</li>
          <li><strong>New Projects:</strong> Buildship: Eclipse Plug-ins for Gradle (first prime release), EGerrit, Paho, Andmore - Eclipse Android Tooling, EMF Parsley and Eclipse Tools for Cloud Foundry.</li>
        </ul>

        <p>To promote the Neon release, the Eclipse Foundation has also produced a 7-day Neon webinar series that is led by experts, to encourage discussion on the new features and projects within the Neon release. For more information please visit <a href="https://www.eclipse.org/community/webinars">www.eclipse.org/community/webinars</a>.</p>

        <p>Oxygen is the twelfth Eclipse release train, scheduled for June 2017.</p>

        <p>The full list of release trains includes:</p>


        <ul>
        <li>Neon, June 22, 2016</li>
        <li>Mars, June 24, 2015</li>
        <li>Luna, June 25, 2014</li>
        <li>Kepler, June 26, 2013</li>
        <li>Juno, June 27, 2012</li>
        <li>Indigo, June 22, 2011</li>
        <li>Helios, June 23, 2010</li>
        <li>Galileo, June 24, 2009</li>
        <li>Ganymede, June 25, 2008</li>
        <li>Europa, June 27, 2007</li>
        <li>Callisto, June 26, 2006</li>
        </ul>

        <p>The Neon release is available for download today. More information and downloads are available at <a href="https://www.eclipse.org/neon">www.eclipse.org/neon</a>.</p>

        <h3>About the Eclipse Foundation</h3>

        <p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>

        <p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="https://www.eclipse.org">www.eclipse.org</a>.</p>

        </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
          <li><a href="https://www.eclipse.org/neon">Neon Release</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
