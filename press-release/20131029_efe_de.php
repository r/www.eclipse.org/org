<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse Foundation gründet europäische Niederlassung";
	$pageKeywords	= "eclipse, eclipse foundation, europäische niederlassung, europe";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

<p>Die Eclipse Foundation freut sich, die Gründung ihrer europäischen Niederlassung Eclipse Foundation Europe bekannt zu geben. Die Niederlassung wird im hessischen Zwingenberg an der Bergstraße angesiedelt sein. Durch die Gründung erhofft sich die Eclipse Foundation, die Unterstützung für die weiterhin rasant wachsende europäische Eclipse Community nachhaltig zu verbessern.</p>
<p>Eclipse ist eine globale „Open Source“ Community mit über 250 Projekten und nunmehr über 1000 Entwicklern („Committer“). Getragen wird die Eclipse Foundation von mehr als 200 Mitgliedern, die Eclipse Plattform wird nach Schätzungen von mehr als 8 Millionen Entwicklern weltweit eingesetzt. Etwa ein Viertel dieser Community und etwa die Hälfte der Mitgliedsfirmen sind in Europa angesiedelt.</p>
<p>Das neue Unternehmen wird mit seinen Angestellten ein Ansprechpartner für die europäische Community sein. Weiterhin wird die Eclipse Foundation in die Lage versetzt, sich in europäische Forschungsvorhaben wie z.B. ITEA einzubringen und so die offene Entwicklung und Verbreitung   dieser Forschungsvorhaben aktiv zu unterstützen. Die Mitarbeit in Fachverbänden wie der deutschen Bitkom ist ein erklärtes Ziel des neuen Unternehmens: Somit können Wahrnehmung und Einsatz von Open Source in Europa verbessert werden.</p>
<p>Herr Ralph Müller übernimmt zusammen mit Mike Milinkovich und Chris Larocque Geschäftsführer des neu gegründeten Unternehmens. Nach einem Studium der INformatik and der Technischen Hochschule in Darmstadt blickt Herr Müller auf mehr als 30 Jahre Erfahrung in der IT Industry zurück. In dieser Zeit war er unter anderem bei Unternehmen wie Siemens Nixdorf, Object Technology International und IBM beschäftigt. Seit 2005 war Herr Müller bei der Eclipse Foundation angestellt.</p>
<p>„In Europa erleben wir eine rasante Adoption von Open Source Technologie, und Eclipse spielt in vielen Bereichen eine grosse Rolle“, führt Herr Müller aus. „Die Gründung der Eclipse Foundation Europe GmbH ermöglicht uns, diesen Trend in der Zukunft noch nachhaltiger zu unterstützen, sei dies durch unsere Konferenzen und Veranstaltungen oder auch mit verbesserter Kimmunikation mit unsereren Mitgliedern und der gesamten Community.“</p>
<p>Weitere Informationen über die Eclipse Foundation Europe GmbH finden Sie in unserem englischsprachigen <a href="http://eclipse.org/org/press-release/20131029_efe_faq.php">FAQ</a>.</p>

<h2>Über die Eclipse Foundation</h2>
<p>Eclipse ist eine Open Source-Community, deren Projekte sich auf die Entwicklung einer offenen Entwicklungsplattform konzentrieren, bestehend aus erweiterbaren Frameworks, Tools und Laufzeitumgebungen für die Entwicklung, Verteilung und Verwaltung von Software über ihren gesamten Lebenszyklus hinweg.</p>
<p>Es handelt sich dabei um ein großes, lebhaftes Umfeld aus maßgeblichen Technologieanbietern, innovativen Startup-Unternehmen, Universitäten, Forschungsinstitutionen und Einzelpersonen, die die Eclipse-Plattform erweitern, ergänzen und unterstützen.</p>
<p>Die Eclipse Foundation ist eine Non-Profit-Organisation und wird von ihren Mitgliedern unterstützt. Weitere Informationen über Eclipse und die Eclipse Foundation finden Sie auf <a href="http://eclipse.org/">www.eclipse.org</a></p>
				

	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/org/press-release/20131029_efe_faq.php">FAQ</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

