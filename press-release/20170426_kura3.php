<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "New Release of Eclipse Kura 3.0 Drives Simplification of IoT Edge Computing";
	$pageKeywords	= "eclipse, iot, internet of things, eclipse kura, edge computing";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p><b>Ottawa, Canada - April 27, 2017</b> - The Eclipse Foundation is pleased to announce a major new release of Eclipse Kura, an open source framework for building Internet of Things (IoT) gateways. The new Eclipse Kura 3.0 adds features that significantly simplify the process of connecting IoT devices with IoT gateways and IoT cloud services. The new release addresses the significant challenges facing the IoT industry for deploying IoT edge computing.</p>

<p>IoT edge computing allows for more efficient IoT solutions by pushing the data management and analysis from the cloud out to IoT gateways. Instead of each individual device connecting to the cloud, devices connect to a local computing device, i.e. a gateway, that is capable of managing the data, analyzing the data, and forwarding the data to an IoT cloud service. Some reports estimate that over 5.6 billion devices will be connected using edge computing.</p> 

<p>Eclipse Kura enables edge computing by providing IoT developers and solution providers the software services required to build IoT gateways. Kura 3.0 adds important features that simplify interoperability, connectivity, and hardware integration -- three important concerns identified in the recent <a target="_blank" href="https://www.slideshare.net/IanSkerrett/iot-developer-survey-2017">IoT Developer Survey</a>.</p>  

<p>The key new features of Kura 3.0 include the following:</p>

<ul>
<li>A new model is introduced to simplify the communication between the devices and the gateway -- first by encapsulating protocols such as Modbus and OPC-UA so that a common format can be reused across different devices, and then by automatically creating a digital image of a device to easily connect it to the gateway and the cloud.</li>

<li>Kura Wires is a new modular and visual data flow programming tool to define data collection and processing pipelines at the edge by simply selecting components from a palette and wiring them together. This way users can, for example, configure a device model, periodically acquire data from its channels, store them in the gateway, filter or aggregate them using powerful SQL queries, and send the results to the cloud. The Eclipse IoT Kura Market is a repository from which additional Wires components can be installed into a Kura runtime with a simple drag-and-drop.</li>

<li>Support for industrial IoT protocols such as OPC-UA will be pre-bundled as a Kura Driver based on the Eclipse Milo project. More industrial field protocols will be made available as Kura Drivers that support Smart Factory and Industry 4.0 applications. These will be available in the Eclipse IoT Kura Market.</li>

<li>Flexibility to connect with different IoT cloud providers is provided, including Microsoft Azure IoT Hub, Amazon AWS IoT, and Eclipse Kapua. This will make it possible to develop and deploy IoT gateway solutions that are not tied to a particular IoT cloud vendor’s solution. In addition, it will also allow to generate multiple data flows from a single IoT gateway routing messages based on privacy-policy or separating telemetry data from command and control message exchanges.</li>
</ul>

<p>Eclipse Kura 3.0 will be available for download in early May at <a href="https://eclipse.org/kura/">eclipse.org/kura</a>.</p>
 
<p>In addition to the Eclipse Kura 3.0 release, the <a href="https://eclipse.org/kapua/">Eclipse Kapua</a> project has made available the first M1 milestone release. Eclipse Kapua is a modular platform providing the services required to manage IoT gateways and smart edge devices. Kapua M1 provides a core integration framework and an initial set of core IoT services, including a device registry, device management services, messaging services, data management, and application enablement.</p>

<p>Eclipse Kura on edge IoT gateways and Eclipse Kapua provide an end-to-end open source foundation for IoT projects and applications.</p>
 
<p><b>Quote from Eurotech</b></p>
<blockquote>“The release of Kura 3.0 and the first milestone release of Kapua mark an important step in the evolution of the technology and tools available to the Eclipse open standards community for the development of their IoT projects. Eurotech is pleased that its ongoing investment in those projects, together with the collaboration among its ecosystem of partners, continues to provide the community with a simpler, more flexible framework for the development of Industrial IoT solutions.”<br>- Marco Carrer, CTO, Eurotech Group</blockquote>

<p><b>Quote from Red Hat</b></p>
<blockquote>“The maturation of Eclipse Kapua coupled with the new features in Eclipse Kura 3.0 such as Kura Wires and increased protocol support, creates a very powerful platform. Kapua and Kura provide a place for Red Hat, its partners and the community to work together to create flexible and open IoT software frameworks and tools.”<br>- James Kirkland – Chief Architect, IoT, Red Hat</blockquote>


<h3>About Eclipse IoT and Eclipse Foundation</h3>
<p>The Eclipse IoT Working Group is a collaborative working group hosted by the Eclipse Foundation that is working to create open source software for IoT solutions. Eclipse IoT is made up of over 30 member companies and 28 open source projects.</p>
<p>Eclipse is a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects in the Eclipse community, ranging from tools for software developers, geo-spatial technology, systems engineering and embedded development tools, frameworks for IoT solutions, tools for scientific research, and much more. The Eclipse Foundation is a not-for-profit foundation that is the steward of the Eclipse community. More information is available at <a href="https://www.eclipse.org/">eclipse.org</a>.</p>


				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://iot.eclipse.org/">Eclipse IoT</a></li>
				<li><a target="_blank" href="https://eclipse.org/kura/">Eclipse Kura</a></li>
				<li><a target="_blank" href="https://eclipse.org/kapua/">Eclipse Kapua</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

