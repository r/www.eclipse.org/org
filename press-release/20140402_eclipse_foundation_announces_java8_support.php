<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Foundation Announces Java 8 Support";
	$pageKeywords	= "eclipse, foundation, eclipse java 8, java 8 support, eclipse java8 support";
	$pageAuthor		= "Christopher Guindon";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – April 2, 2014</b> - The Eclipse top-level project is very proud to announce official support
				for Java&trade; 8 for Eclipse Kepler SR2 (Eclipse 4.3.2). For details on how to discover and install the Java 8
				support please visit our <a href="http://www.eclipse.org/downloads/java8/">Java&trade; 8 support page</a>.</p>

<p>The Java&trade; 8 support contains the following:</p>
<ul>
    <li>Eclipse compiler implementation for all the new Java&trade; 8 language enhancements</li>
    <li>Updated significant features to support Java&trade; 8, such as Search and Refactoring</li>
    <li>Quick Assist and Clean Up to migrate anonymous class creations to lambda expressions and back</li>
    <li>New formatter options for lambdas</li>
</ul>

<p>Note that PDE API Tools have not yet adopted the new language constructs. This will be completed in the Eclipse Luna
release available in June 2014. Java&trade; 8 support will also be available in the Eclipse Luna M7 packages expected
to be available on May 9, 2014.</p>

</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://www.eclipse.org/downloads/java8/">Installing Java&trade; 8 Support in Eclipse Kepler SR2</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

