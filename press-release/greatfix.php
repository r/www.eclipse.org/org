<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Help Improve Eclipse in the Great Fixes for Mars Competition";
	$pageKeywords	= "eclipse, great fix";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
		
			<p>The <a href="https://www.eclipse.org/projects/greatfix/">Great Fixes for Mars</a> competition is your opportunity to help participate in the success of the Eclipse platform and 
				Java IDE. Between now and the Mars release in June, we are encouraging everyone in the Eclipse community to contribute 
				fixes that will improve the functionality and performance of Eclipse. The contributors with the best fixes will be awarded a new Nexus 9 tablet, compliments of Google.</p>

			<p>The contest is <a href="https://www.eclipse.org/projects/greatfix/">open now</a>. The first set of prizes will be announced March 12. Now is the time to get coding and help improve Eclipse.</p> 
							
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipse.org/projects/greatfix/">Great Fixes for Mars</a></li>		
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

