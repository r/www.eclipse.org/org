<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse Che Release: New IDE Platform Reimagines the Developer Workspace";
  $pageKeywords = "eclipse, papyrus, modeling, polarsys, embedded development";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  		
  		<p>Eclipse Che open source project improves 
      agile development by integrating a cloud IDE,
      workspace server, and plug-ins into a universal 
      developer workspace.</p>
      
<p>OTTAWA, Canada – March 08, 2016 – The Eclipse Foundation announces the initial release of Eclipse Che, a new IDE platform. The Eclipse Che ecosystem – supported by Codenvy, Microsoft Corp., Red Hat, and SAP – is the only community-driven open source cloud IDE, workspace server, and plug-in platform.</p> 
<p>“Eclipse Che is moving development forward with a universal workspace,” said Tyler Jewell, Eclipse Che project lead and Codenvy CEO. “By making workspaces portable, they can relocate anywhere, giving development teams on-demand environments that can be part of an agile process.”</p>
<p>Eclipse Che workspaces are composed of projects and Docker-powered runtimes. Workspaces are accessible anytime by browsers, CLI and APIs. Their portable nature migrates development to any location, whether a desktop, mobile device or cloud. Other features include:</p>
<ul>
<li><strong>Collaborative Workspace Server.</strong> Host Eclipse Che as a workspace server, providing shared access to programming services to workspaces and teams. Workspace environments are given a hostname and accessible by remote clients.</li>
<li><strong>Cloud IDE.</strong> A no-installation browser IDE and IOE accessible from any local or remote device.</li>
<li><strong>Plug-In Framework.</strong> Che is extensible, by customizing built-in plug-ins or authoring your own extensions.</li>
<li><strong>Stacks.</strong> Build projects for any programming language and framework. Create runtimes from Che’s image and stack library, pull from DockerHub, or author custom images with Dockerfiles.</li>
</ul>
<p>Eclipse Che builds upon years of experience within the Eclipse community in developing compelling tools.  It integrates key elements of Eclipse technology such as the ubiquitous Eclipse Java development tools and the Orion web editor to provide a new developer experience. Che adds new workspace management and container-based runtime technology to modernize the developer experience.</p>
<p>“Eclipse Che is rethinking the way IDEs are built and used by developers. It uses Docker, Java and JavaScript to create a more flexible and dynamic developer work experience, ” said Mike Milinkovich, Executive Director of the Eclipse Foundation. “The initial feedback on Eclipse Che has been outstanding. The number of downloads and external contributions all point to Che being an incredibly successful open source project.”</p>
<p>Eclipse Che is available for download at <a href="http://eclipse.org/che">http://eclipse.org/che</a> and contributors can get involved at <a href="http://github.com/eclipse/che">http://github.com/eclipse/che</a>.</p>

<h2>About the Eclipse Foundation</h2>
<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse open source technology.</p>
<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="http://www.eclipse.org">http://www.eclipse.org</a>.</p>



<h2>Quote Sheet</h2>
<p>“We are excited about this partnership, and our role in helping shape the future of agile development. By attaching ready-to-code Codenvy workspaces to Visual Studio Team Services, we continue to improve our partner ecosystem and provide development teams with comprehensive solutions for developing any application.”  Shanku Niyogi, General Manager, Developer Division, Microsoft</p>
<p>“SAP is committed to the Eclipse Foundation and is continuing to invest in it, by building products on top of Eclipse Che as  well as ongoing contributions to Open Source. “With the new SAP Web IDE for SAP HANA, developers can expect to be able to design, build, and deploy SAP Fiori-like web applications based on SAPUI5.  It also  reaffirms our successful use of Eclipse Che in a productive, database-centric IDE, based on the SAP Web IDE front-end and Eclipse Che as the backend.”  Björn Goerke, Executive Vice President and Corporate Officer, SAP</p>
<p>“Eclipse Che is clearly something into which IDEs are evolving. We are about to release a solution based on Che, that allows developers to try out development with Vaadin, a server side JVM framework, even without installing JDK to their computers. That will dramatically lower the barrier to try developing with the Vaadin Framework. With Eclipse Che, quick hacking with even large Java projects becomes as easy as it has become to play with various online JavaScript editors.”  Matti Tahvonen, Developer Advocate, Vaadin</p>
<p>"Using the OpenShift plug-in for Eclipse Che to leverage universal workspaces, developers can more easily access a facsimile of a production environment during development and testing. Together, these technologies create a powerful combination for enabling more agile application development, and I expect Eclipse Che will quickly find its place in the developer’ss' arsenal. Red Hat welcomes the release of Eclipse Che and congratulates the team for its work on this project." Harry Mower, senior director, Developer Programs, Red Hat</p>
<p>“Eclipse Che's workspaces are provisioned on-demand and located anywhere we need to place them. Eclipse Che allows us to provide a rich array of developer environments that are launched as part of our QA and continuous delivery automation.”  Frederic Drouet, CTO, eXo Platform</p>
<p>"We are pleased by the strong interest in Web IDE technologies such as Eclipse Che and Orion. The fact that the Orion editor is now being used in Eclipse Che and services like our own Bluemix DevOps Services, demonstrates open source collaboration is advancing the pace of innovation for Web IDE technologies. We look forward to the continued collaboration between the Eclipse Che and Orion projects."  Dave Thomson, Director, IBM Bluemix DevOps Services</p> 
<p>“As modern development is reliant on distributed and cloud-based systems, it is necessary for the workspace to expand in a manner which is both open and API driven. A hybrid development environment is a necessary step to stay leading edge, and exposing the functionality with open technologies like Swagger is a huge win. We expect the ideas of Eclipse Che to shape the next generation of developer tools and enable integration with specialized tools like SwaggerHub for the best possible developer experience."  Tony Tam, VP of Products, Swagger, SmartBear</p>
<p>“Eclipse Che's vision for portable and universal workspaces makes it possible for our community to launch projects that always compile and just work. We are integrating Che into Sourcegraph to give our users and contributors instant workspaces that are powerful and extensible. We'll be closely following Che and its ecosystem."  Quinn Slack, CEO, Sourcegraph</p>
<p>“Eclipse Che gets developers running with TomEE quickly by providing a ready to go environment with a single click. It saves contributors from lost configuration time. Eclipse Che's Docker runtimes provide a rich developer experience with a gorgeous and fast UI." David Blevis, CEO, Tomitribe</p>
      
  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
  				<li><a href="https://eclipse.org/che/">Che</a></li>
  				<li><a target="_blank" href="http://github.com/eclipse/che">Che on Github</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
