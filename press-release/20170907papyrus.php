<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "First Release of Eclipse Papyrus for Real Time: Modeling Tool for UML-RT";
	$pageKeywords	= "eclipse, papyrus, UML-RT, real time, Eclipse Papyrus";
	$pageAuthor		= "Ian Skerrett";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<p>Ottawa, Canada - August 23, 2017 &ndash; The Papyrus Industry Consortium, an Eclipse Foundation Working Group, is pleased to announce the release of Eclipse Papyrus for Real Time v1.0 (Papyrus-RT), a fully open source modeling tool intended for teams working on the design and implementation of embedded reactive systems.</p>
<p>Papyrus-RT is the first &nbsp;production-ready tool to provide a no-cost open source solution that supports the domain-specific modeling language, UML-RT. UML-RT was designed specifically for real-time and embedded systems, and has been successfully applied in numerous large-scale industrial projects. Because it is based on the industry standard UML language, it allows easy integration with other system development disciplines and languages (e.g., SysML). Existing UML-RT tools are available from commercial vendors but the high costs typically limit their availability to high-end enterprise usage.</p>
<p>Eclipse Papyrus RT will be the first offering that makes UML-RT tools available to the broader industry. The first release of Papyrus for Real Time provides the user with a full UML-RT modeling environment, code generation capabilities, as well as a runtime library &ndash; all of it as open source. Papyrus-RT is not merely a model editing tool, but provides complete automatic generation of the equivalent C++ code, which can be executed in conjunction with a highly optimized and efficient runtime library.</p>
<p>Papyrus-RT&rsquo;s openness and adaptability provide toolsmiths with rich opportunities to modify, adapt, and augment the capabilities of the product to meet the specific needs of their particular users, projects, and environments. Compliance with the most recent revision of the UML standard makes it particularly well-suited for use in industry as well as research and teaching environments.</p>
<p>"I am using Papyrus for my teaching and research on modeling. I am very happy to see the kind of customization that can be done thanks to the strong standards conformance at its roots and, on the other hand, its extensibility.<br />I am using Papyrus-RT in my M.Sc. Systems Engineering class in the context of the Rover Case Study, as an illustration of how modeling and code generation can be a powerful combination." &nbsp;&nbsp;&ndash; Jean-Michel Bruel, Professor at University of Toulouse, CNRS/IRIT Laboratory (Head of the MACAO team)</p>
<p>"I am extremely pleased to see the official release of Papyrus RT 1.0. I think that it comes at exactly the right time to meet the daunting challenges posed by the emerging generation of highly complex interactive and real-time software systems. Its full support for the executable domain-specific modeling language, UML RT, is very well suited for designing various "smart" systems as well as applications comprising the Internet of Things." &nbsp;&nbsp;&ndash; Bran Selic, Malina Software Corp.</p>
<p>More information about &nbsp;Papyrus for Real Time is available at <a href="https://wiki.eclipse.org/Papyrus-RT">https://wiki.eclipse.org/Papyrus-RT</a>.</p> 
<p>Papyrus for Real Time is available for download for Linux, MacOS, and Windows from <a href="https://www.eclipse.org/papyrus-rt/content/download.php">https://www.eclipse.org/papyrus-rt/content/download.php</a></p>
<p>&nbsp;</p>
<h3>About the Eclipse Foundation</h3>
<p>The Eclipse Foundation is a not-for-profit organization that supports a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects with the Eclipse Foundation, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. Eclipse Foundation Working Groups allow organizations to collaborate on building innovative new technology to meet specific industry needs. &nbsp;More information is available at eclipse.org.</p>

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://marketplace.eclipse.org/">Eclipse Marketplace</a></li>
				<li><a target="_blank" href="https://eclipse.org">Eclipse Homepage</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

