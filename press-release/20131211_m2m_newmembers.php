<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse Foundation M2M Working Group Announces New Members";
	$pageKeywords	= "eclipse, m2m, members";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – December 12, 2013</b> – The Eclipse Foundation, the host of the Eclipse M2M Working Group, is pleased to welcome three new companies as members of the working group.  2lemetry, Bitreactive,  and M2M Alliance have all joined the working group to participate in building a collaborative M2M and IoT community. These companies join existing members Aexeda, Eurotech, ibh Consulting, IBM, and Sierra Wireless.</p>

		<p>The Eclipse M2M Working Group is a collaboration among organizations to create open source software that will become the building blocks for the M2M and Internet of Things (IoT) industry. The current technology used in the M2M and IoT industry is often closed and proprietary, making it difficult to create solutions that easily integrate and interoperate. The working group’s objective is to build a set of frameworks, protocol implementations, and tools to help developers quickly build IoT and M2M applications. All members of the working group currently build commercial products, services, and solutions for the M2M and IoT industry.</p>

		<p>The new members include these companies.</p>
				
			<ul>
				<li><b><a target="_blank" href="http://www.2lemetry.com/">2lemetry</a></b>, based in Denver, Colorado, has developed an IoT platform that makes it easy for companies to connect, control and manage devices – regardless of the type of device, the language (protocol) of the device, or its location around the globe. The platform supports proprietary protocols, as well as standard protocols like MQTT, CoAP, STOMP, and M3DA. Furthermore, the 2lemetry platform allows for intercommunication between protocols, which is unique in the IoT space. 2lemetry is also contributing to Paho project around objective-c and MQTT.</li>
				<li><b><a target="_blank" href="http://www.bitreactive.com/">Bitreative</a></b>, based in Trondheim, Norway, makes the Reactive Blocks SDK, which allows developers to easily build highly concurrent and event-driven systems for IoT and M2M applications. Reactive Blocks integrates with the Eclipse Java IDE and makes M2M technologies like MQTT and Eclipse Kura available as building blocks, which can be plugged together graphically.</li>
				<li><b><a target="_blank" href="http://www.m2m-alliance.de/">M2M Alliance</a></b>, based in Aachen, Germany, is the world’s largest M2M industry association dedicated to the promotion of M2M technologies and solutions. The M2M Alliance and Eclipse M2M will collaborate to promote open standards and open source technologies for the M2M industry.</li>
			</ul>
				
		<p>The Eclipse M2M community includes 11 different open source projects. The technology created in these projects is freely available for individual and commercial use under the Eclipse Public License (EPL). Individuals and companies are invited to use the technology and contribute to the ongoing development. Organizations are also invited to join the Eclipse M2M Working Group.</p>

		<p>Eclipse M2M is a special theme at the upcoming <a href="https://www.eclipsecon.org/na2014">EclipseCon</a> conference, March 17-20, 2014 in San Francisco. An entire day will be devoted to sessions and discussions on the Eclipse M2M projects. The Eclipse Foundation is also hosting an MQTT Interoperability Testing Day on March 17 at the same location.</p>

		<p>More information about Eclipse M2M are available at <a href="http://m2m.eclipse.org/">m2m.eclipse.org</a>.</p>
				

	<h2>About the Eclipse Foundation</h2>

		<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>

		<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="http://eclipse.org/">www.eclipse.org</a>.</p>
				

	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://m2m.eclipse.org/">Eclipse M2M</a></li>
				<li><a href="https://www.eclipsecon.org/na2014/">EclipseCon 2014</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

