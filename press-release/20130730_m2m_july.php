<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse M2M Initiative Gains Momentum with New Projects, Members and Commercial Adoption";
	$pageKeywords	= "eclipse, m2m, IoT, open source, mqtt";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>



<p><strong>Ottawa, Canada – July 30, 2013 </strong>– The Eclipse Foundation is pleased to announce that the <a href="http://m2m.eclipse.org/">Eclipse M2M Working Group</a> continues to make significant progress in establishing an 
open source software ecosystem for Machine-2-Machine (M2M) and Internet of Things (IoT) development. Eclipse M2M has added new open source projects, new releases of existing projects, 
new members, and new product announcements from member companies.</p>

<p>M2M, which deals with connecting different types of machines and devices via a network, is one of the fastest growing technology segments – by some estimates approaching $100 billion. 
This new technology segment presents a number of software development challenges due to the complexity of the hardware and network architectures, lack of open standards, and issues of 
interoperability between vertical solutions. </p>

<p>The Eclipse M2M Working Group is a collaboration of companies and organizations focused on developing open source protocols, frameworks, and tools for M2M software development. The goal of 
the Eclipse M2M Working Group is to make it easy for software developers to create M2M applications that are based on open standards and open source technology. Eclipse M2M has three existing 
open source projects – Koneki, Mihini, and Paho – that provide open source technology for M2M developers.</p>

<p><strong>New M2M Projects</strong><br>

Four new open source projects are being proposed to join the Eclipse M2M community:
<ol>
<li><a href="http://eclipse.org/proposals/technology.ponte/">Ponte</a> is a framework that will allow reading and writing of data from sensors and actuators via M2M protocols, accessible via a REST API. Ponte will also provide the ability to convert and exchange data between protocols. The initial protocols that will be supported include MQTT (a proposed OASIS standard) and CoAP (Constrained Application Protocol). Eclipse Paho hosts the reference implementation of MQTT.
<li><a href="http://www.eclipse.org/proposals/technology.eclipsescada/">Eclipse SCADA</a> is an open source implementation of a Supervisory Control and Data Acquisition system typically used to monitor and control large scale industrial processes, e.g. solar farms. Eclipse SCADA will provide connectivity to a variety of industrial devices, a monitoring system to create alarms and events, recording of historical data and a framework to build custom user interfaces and visualizations on top of those functions.
<li><a href="http://www.eclipse.org/proposals/rt.concierge/">Concierge</a> is an implementation of the OSGi core specifications that is well suited for embedded and mobile devices. Concierge will target a footprint with a jar file size of less than 400KB, allowing it to run on devices that have limited resources.
<li><a href="http://www.eclipse.org/proposals/technology.kura/">Kura</a> is a Java and OSGi based application framework for M2M service gateways. Kura will provide a set of common services for Java developers building M2M applications, including I/O access, data services, network configuration and remote management.
</ol>
</p>

<p><strong>Project Updates and Contributions</strong><br>

The existing Eclipse M2M projects continue to develop and release open source software for M2M developers. Some key new features and contribution for these projects include the following:
<br>
<ul>
<li><strong>Mihini</strong><br>
The initial code contribution, provided by Sierra Wireless, is now available for <a href="http://www.eclipse.org/mihini/">Mihini</a>. Mihini, a Lua-based framework for M2M gateways, provides connectivity management to ensure reliable network connection, provides an abstraction layer for underlying hardware and protocols, and enables the smart transmission of data between devices and servers. 
</li>

<li><strong>Paho</strong><br>
<a href="http://www.eclipse.org/paho/">Paho</a> provides open source implementations of the proposed OASIS MQTT protocol. In the last six months Paho has received community contributions for MQTT client implementations in Python, JavaScript, and Objective-C. 
</li>

<li><strong>Koneki</strong><br>
<a href="http://www.eclipse.org/koneki/">Koneki</a> is a Lua-based IDE with its first official 1.0 production release included as part of Kepler, the annual Eclipse release train. Koneki 1.0 featured updated support for the new Lua 5.2 runtime and new remote development support for the Mihini framework.
</li>
</ul>
</p>

<p><strong>Commercial Adoption of Eclipse M2M projects</strong><br>
A number of new product announcements from leading M2M companies have included Eclipse M2M technology.<br>
<ul>
<li>Sierra Wireless AirVantage M2M Cloud platform has recently announced support for the Mihini framework that allows users to build applications running on open hardware, such as Raspberry Pi, which connect with the Sierra Wireless AirVantage Cloud Platform. 
<li>The Sierra Wireless AirLink GX and LS series Intelligent Gateways use Mihini and Koneki as part of the ALEOS Application Framework that supports rapid development of embedded M2M applications
<li>The recently launched IBM MessageSight offering is a new messaging appliance designed for M2M and mobile environments.  It is capable of supporting millions of concurrent sensors or smart devices and can scale up to 13 million messages per second.  MessageSight supports the MQTT protocol and MQTT clients from Eclipse Paho. Sprint is the first mobile carrier to adopt IBM MessageSight and is using the technology to deliver advanced connected car services. 
<li>Eurotech Everyware Cloud 3.0 platform allows for device-independent M2M applications to connect sensors and devices with back-end business processes and reports. Everyware Cloud 3.0 includes MQTT support from Eclipse Paho and is based on the technology proposed in the Eclipse Kura project.
<li>The Axeda Machine Cloud ® supports open source protocol standards including Eclipse Paho.  Axeda Ready Gateways can now support MQTT networks by listening to any MQTT broker. This allows for support of local MQTT sensor and device networks as well as connectivity between MQTT device networks and the Axeda Machine Cloud.  
</ul>
</p>

<p><strong>New Members of the Eclipse M2M Working Group</strong><br>
IBH Systems GmbH has joined the Eclipse M2M working group. IBH provides software solutions and consulting with focus on the oil and gas and renewable energy industry. Their employees are also 
the founders of the Eclipse SCADA project.
</p>
<p>IBH will join the existing members of the working group: Axeda, Eurotech, IBM, and Sierra Wireless.</p>

<p>More information about Eclipse M2M is available at <a href="http://m2m.eclipse.org/">m2m.eclipse.org.</a> </p>


			<p><b>About the Eclipse Foundation</b></p>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="http://www.eclipse.org/">www.eclipse.org</a>.</p>			


	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://m2m.eclipse.org/">Eclipse M2M</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

