<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "The Eclipse Foundation Releases Eclipse Theia 1.0, a True Open Source Alternative to Visual Studio Code";
$pageKeywords	= "Eclipse Theia, Eclipse Foundation";
$pageAuthor		= "Mike Milinkovich";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p><em>&ndash; Leading open source adopters for Eclipse Theia include ARM, Arduino, EclipseSource, Ericsson, Gitpod, Google Cloud, IBM, Red Hat, SAP, and TypeFox &ndash; </em></p>

<p><strong>OTTAWA &ndash; MARCH 31, 2020 &ndash;</strong> The <a href="http://www.eclipse.org">Eclipse Foundation</a>, one of the world&rsquo;s largest open source foundations, today announced the release of Theia 1.0, a true open source alternative to Microsoft&rsquo;s popular Visual Studio Code (VS Code) software. Eclipse Theia is an extensible platform to develop multi-language Cloud and Desktop Integrated Development Environments (IDEs) with state-of-the-art web technologies that enable developers, organizations, and vendors to create new, extensible developer experiences. Early contributors and adopters span a broad variety of industries and applications, and include companies like ARM, Arduino, EclipseSource, Ericsson, Gitpod, Google Cloud, IBM, Red Hat, SAP, and TypeFox.</p>

<p>&ldquo;We are thrilled to see Eclipse Theia deliver on its promise of providing a production-ready, vendor-neutral, and open source framework for creating custom and white-labeled developer products,&rdquo; said Mike Milinkovich, executive director of the Eclipse Foundation. &ldquo;Visual Studio Code is one of the world&rsquo;s most popular development environments. Not only does Theia allow developers to install and reuse VS Code extensions, it provides an extensible and adaptable platform that can be tailored to specific use cases, which is a huge benefit for any organization that wants to deliver a modern and professional development experience. Congratulations to all the Theia committers and contributors on achieving this milestone.&rdquo;</p>

<p>Started as a project by Ericsson and TypeFox in 2016, the Theia project has become an integral part of enterprise cloud solutions around the world. Its momentum and distribution became significant enough that last year, the project approached the Eclipse Foundation as a potential host and vendor-neutral foundation that could further guide the project&rsquo;s growth. Today, Theia is one of the Eclipse projects encompassed by the <a href="https://ecdtools.eclipse.org/">Eclipse Cloud Development Tools Working Group</a>&nbsp; (ECD WG), an industry collaboration focused on delivering development tools for and in the cloud.</p>

<p>The most significant differences between Eclipse Theia and VS Code are:</p>

<ul>
	<li>Theia&#39;s architecture is more modular and allows for significantly more customizations</li>
	<li>Theia is designed from the ground to run on both Desktop and Cloud</li>
	<li>Theia is developed under the community-driven and vendor-neutral governance of the Eclipse Foundation.</li>
</ul>

<p>Eclipse Theia is designed to work as a native desktop application as well as in the context of a browser and a remote server. To support both situations with a single source, Theia runs in two separate processes. Those processes are called frontend and backend respectively, and they communicate through JSON-RPC messages over WebSockets or REST APIs over HTTP. In the case of Electron, the backend, as well as the frontend, run locally, while in a remote context the backend would run on a remote host.</p>

<p>Both the frontend and backend processes have their dependency injection (DI) container to which extensions can contribute. Similar to VS Code&rsquo;s online marketplace for code extensions, Eclipse Theia 1.0 also has a marketplace that is available today and, in the spirit of true open source community, allows for even non-VS Code applications to use these extensions.</p>

<p>The Eclipse Foundation has a proven track record of enabling developer-focused open source software collaboration and innovation earned over more than 15 years. Home to critical cloud native open source projects, including Jakarta EE, Eclipse Che, and more, the Foundation&rsquo;s more than 375 collaborative projects have resulted in over 195 million lines of code &mdash; a $10 billion shared investment.</p>

<p>To get involved with the Eclipse Theia Project and begin contributing, please visit <a href="https://theia-ide.org/">https://theia-ide.org/</a>.</p>

<p>For more information about the Eclipse Cloud Development Tools Working Group, view the <a href="https://www.eclipse.org/org/workinggroups/eclipse_cloud_development_charter.php">Charter</a> and <a href="https://www.eclipse.org/org/workinggroups/wgpa/eclipse_cloud_development_tools_participation_agreement.pdf">ECD Working Group Participation Agreement (WGPA)</a>, or email membership@eclipse.org. You can also join the <a href="https://accounts.eclipse.org/mailing-list/ecd-tools-wg">ECD Tools mailing list</a>.</p>

<h2>Quotes from Eclipse Theia Project Adopters and Contributors</h2>

<h3>Arduino</h3>

<p>&ldquo;As one of the world&rsquo;s largest open source ecosystems for hardware and software, we fully support extending vendor-neutral governance to every aspect of software development,&rdquo; said Luca Cipriani, CTO of Arduino. &ldquo;Eclipse Theia is another important step in that direction. Our community has been eagerly advocating for functionality for some time.&rdquo;</p>

<h3>EclipseSource</h3>

<p>&ldquo;We actively support an increasing number of customers in adopting Eclipse Theia and in migrating their existing tool chains to the cloud,&rdquo; said Dr. Jonas Helming, principal software architect and general manager of EclipseSource. &ldquo;The 1.0 release formally reflects what is already a fact; Theia is a mature, stable, and powerful platform for building cloud-based IDEs and tools.&rdquo;</p>

<h3>Ericsson&nbsp;&nbsp;</h3>

<p>&ldquo;Eclipse Theia is an extensible open source platform for creating and maintaining custom IDE and IDE-like applications. A huge benefit of Theia is that it allows developers to start with a desktop application and easily transition it to run in the cloud without having to rewrite it,&rdquo; said Gunnar Nilsson, Ericsson&rsquo;s open source strategist. &ldquo;Having a truly open source solution is why we helped to start this project more than three years ago. We&rsquo;re very pleased that the Eclipse Foundation has helped us guide this first official release and look forward to many more in the years to come.&rdquo;</p>

<h3>Gitpod</h3>

<p>&ldquo;As one of the creators for the Eclipse Theia project, this first official release is the realization of years of work from a fantastic community,&rdquo; said Sven Efftinge, co-founder of both Gitpod and TypeFox and current CEO of Gitpod. &ldquo;Finally we have a true open source alternative for desktop and cloud IDEs that rivals the best the industry has to offer. We firmly believe this is just the beginning for Eclipse Theia.&rdquo;</p>

<h3>Red Hat&nbsp;</h3>

<p>&quot;At Red Hat we believe that developer teams need great tools to succeed in the new open hybrid cloud world,&rdquo; said Thomas M&auml;der, Red Hat&rsquo;s principal software engineer and committer to the Theia project. &ldquo;Eclipse Che is one of the fastest growing open source cloud IDEs and for version 7, we replaced our own IDE code with Eclipse Theia. The results have been great. We have a more modern IDE that is compatible with Visual Studio Code extensions and a vibrant, vendor-neutral community. Red Hat sees a lot of potential in Eclipse Theia and is committed to grow the editor as part of the community.&rdquo;</p>

<h3>SAP</h3>

<p>&ldquo;SAP has been providing cloud-based development tools based on Eclipse open source projects since 2014,&rdquo; said Michael Wintergerst, SVP, SAP Cloud Platform Core at SAP. &ldquo;SAP Business Application Studio, based on Eclipse Theia 1.0, provides our customers with an efficient business application development environment for their Intelligent Enterprise for both public and private cloud. We see our customers benefiting from the rich set of add-on tools that will be available in the free, vendor-neutral extensions marketplace.&rdquo;</p>

<h2>About the Eclipse Foundation</h2>

<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and commercially focused environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 375 open source projects, including runtimes, tools, and frameworks for a wide range of technology domains such as IoT, edge computing, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 300 members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter <a href="https://www.globenewswire.com/Tracker?data=r-dKnkzEx8J2jm68aMwsXND3DPYOl6p60tYwoWOkQRpJjLP_SKHpU_opBz5bi7JLO7Y2btHRJBvdyqqcTBtrLg==">@EclipseFdn</a>, <a href="https://www.globenewswire.com/Tracker?data=S12nqbeRNKuTWwBUeNUnEC9gFrlX34gxCEhP2Uu-2KuWYmqkXbIScweQ1ykqwa2QtAHlvDYJT8GyKpicKaq5uLxRrTZwh2HI7nQfCd01lmEj3UXrlRWmU3VLtBxzjMlq">LinkedIn</a>, or visit <a href="https://www.globenewswire.com/Tracker?data=EsgUi61-f7vRM-pEF4XYjO2jYG6DqnLbsSPKrHZwkTiaAkCbjz8HlE68IUVSML_CiBt9S9b00GJWRIs3EmTELQ==">eclipse.org</a>.</p>

<p>Third-party trademarks mentioned are the property of their respective owners.</p>

<p><strong>Media Contact:</strong><br />
Nichols Communications for the Eclipse Foundation, Inc.<br />
Jay Nichols<br />
jay@nicholscomm.com<br />
+1 408-772-1551</p>

  	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);