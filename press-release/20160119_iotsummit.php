<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Announcing the IoT Summit 2016";
  $pageKeywords = "eclipse, iot, virginia, reston, march, eclipsecon";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  		<p><i>An opportunity to learn and discover open source IoT technology</i></p>
  <p><b>Ottawa, Canada - January 19, 2016</b> - The Eclipse Internet of Things (IoT) community, an Eclipse Foundation working group, is pleased to announce the 
  		<a href="https://www.eclipsecon.org/na2016/iot-summit">IoT Summit 2016</a> to be held March 8-9, 2016 in Reston, Virginia. The event is co-located with 
  		<a href="https://www.eclipsecon.org/na2016/">EclipseCon 2016</a>.</p>
  		
		<p><a href="http://iot.eclipse.org/">Eclipse IoT</a> is the leading open source community building implementations of IoT protocols, runtimes, and device frameworks. The IoT Summit will be focused 
  		on the open source and open standards available to organizations building IoT solutions. Participants will be learn about new key initiatives from the Eclipse 
  		IoT community, Apache Spark, LoRaWAN, Microelectromechanical systems (MEMS) and other technology relevant for IoT solutions.</p>
  		
		<p>The IoT Summit will feature IoT experts from leading IoT companies including, Analog Devices, Bosch, Eclipse Foundation, Eurotech, IS2T, IBM, Marvell Semiconductor, 
  		Red Hat, The Things Network and Verisign. Attendees will have an opportunity to learn first-hand from these IoT technology leaders.</p>
  		
		<p>The IoT is driving major changes for all companies and industries. One of key change are the technologies required to build IoT solutions, including hardware, 
  		networking, messaging, data processing, security, application development, cloud computing and more. Attend the IoT Summit to explore the different aspects of IoT 
  		technology and understand how to get started building IoT solutions.</p>
  		
		<p>Registration is now open for the IoT Summit. Schedule and registration details are available here: 
  		<a href="https://www.eclipsecon.org/na2016/iot-summit">www.eclipsecon.org/na2016/iot-summit</a></p>
		  		
  		
  	<h3>About the Eclipse Foundation</h3>
	<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, 
  		deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and 
  		individuals extend, complement, and support the Eclipse Platform.</p>

  	<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at 
  		<a href="http://www.eclipse.org/">www.eclipse.org</a>.</p>
  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
  				<li><a href="https://www.eclipsecon.org/na2016/iot-summit">IoT Summit</a></li>
  				<li><a href="http://iot.eclipse.org/">Eclipse IoT</a></li>
  				<li><a href="https://www.eclipsecon.org/na2016/">EclipseCon 2016</a></li>
  				<li><a href="https://www.eclipsecon.org/na2016/registration">Registration</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
