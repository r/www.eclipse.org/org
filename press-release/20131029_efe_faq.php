<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse Foundation Europe FAQ";
	$pageKeywords	= "eclipse, eclipse foundation, subsidiary, europe, faq";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

	<p><b>Q: What was announced?</b><br>The Eclipse Foundation, Inc. has announced the creation of a wholly owned subsidiary in Germany named Eclipse Foundation Europe GmbH.</p>

	<p><b>Q: Why is the Eclipse Foundation creating a European subsidiary?</b><br>The Eclipse community has been rapidly growing in Europe, and a significant portion of our operations are focused there. Examples include two annual conferences (EclipseCon Europe and EclipseCon France), many Eclipse Days and Democamps, and currently a total of three staff. Creating Eclipse Foundation Europe (EFE) will help make those operations more efficient. Examples include being able to hire our staff as employees rather than contractors, being able to do banking in euros more easily, and being able to create a small office.</p>

 	<p><b>Q: What will the EFE do?</b><br>EFE and its staff will be focused on supporting the needs of members, committers, projects, and commercial ecosystem based in Europe.</p>

	<p><b>Q: Why was this created in Germany?</b><br>The short answer is because that's where Ralph Mueller lives. Ralph will be the Managing Director of the Eclipse Foundation Europe, and will be responsible for its operations. As those who are familiar with the Eclipse community in Europe know, Ralph has been leading our staff presence there for many years.</p>

	<p><b>Q: Is the Eclipse Foundation Europe a German nonprofit corporation?</b><br>Strictly speaking, no. The EFE is incorporated as a GmbH, which is the typical form of for-profit incorporation in Germany. But that said we have no intention of changing the business model of the Eclipse Foundation and seeking profits from our operations. The EFE will be effectively operated as a not-for-profit, within the tax and regulatory requirements of Germany.</p>

	<p>We looked at many options as we worked towards establishing the EFE, including a Verein and a charitable corporation. The GmbH approach was the most effective approach for our needs.</p>

	<p><b>Q: We are an Eclipse Foundation Member based in Europe. Should we move our membership to the EFE?</b><br>No. All memberships will continue to be held directly with the Eclipse Foundation, Inc. in the USA.</p>

	<p><b>Q: Can I pay my Eclipse membership fees in Euros?</b><br>No. All memberships will continue to be held directly with the Eclipse Foundation, Inc. in the USA, in US Dollars.</p>

	<p><b>Q: Will the EFE start hosting projects in Europe?</b><br>No. All Eclipse projects will continue to be hosted in our main forge on servers located in Canada. We may re-examine this in the future based on community interest.</p>

	<p><b>Q: How does EFE intends to participate to EU funded projects?</b><br>EFE will apply to participate to EU funded projects like ITEA2 or Artemis projects to help implement open innovation and disseminate projects results. The European Commission has been supporting Open Source for a long time, and many EU funded projects produce open source results. Many EU funded projects are also using Eclipse technologies. We think that by working with the project teams from the beginning, EFE will help create sustainable open source ecosystems that last after the end of the funded projects.</p>

	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/org/press-release/20131029_efe.php">EFE Press Release</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

