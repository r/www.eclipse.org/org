<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "EclipseCon 2014 Keynotes Speakers Announced";
	$pageKeywords	= "eclipse, eclipsecon, keynotes, 2014";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – November 27, 2013</b> – The Eclipse Foundation is pleased to announce the keynote speakers for the upcoming EclipseCon 2014 conference. The annual conference takes place March 17-20 at the Hyatt Regency San Francisco Airport.</p>

		<p>This year the <a href="https://www.eclipsecon.org/na2014/keynote-speakers">keynote speakers</a> touch upon topics important to the future of the software industry and the future of Eclipse.</p>
			<ul>
				<li><b>Catarina Mota</b>, co-founder of Open Materials, will look at the innovation happening in the open hardware and smart materials community and how it will impact the future Internet of Things industry.</li>
				<li><b>Hans-Jürgen Kugler</b>, chief scientist at Kugler Maag, will present “Will the New Industrial Revolution Lead to a Controlled Society or a Creative Society.” Mr. Kugler will discuss the forces that led to the industrial revolution and compare these to the current forces in today’s changing software industry.</li>
				<li><b>Mike Milinkovich</b>, executive director of the Eclipse Foundation, will look ahead to explore what is possible for the Eclipse community in the next 10 years.</li>
			</ul>
				<br/>
		<p>Registration is <a href="https://www.eclipsecon.org/na2014/registration">now open</a> for EclipseCon 2014. Attendees who register before December 31, 2013 will received a $700 discount off the full registration prices. Companies are also invited to participate as EclipseCon sponsors. A detailed sponsorship prospectus is <a href="https://www.eclipsecon.org/na2014/prospectus">now available</a>.</p>


	<h2>About the Eclipse Foundation</h2>

		<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>

		<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="http://eclipse.org/">www.eclipse.org</a>.</p>
				

	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipsecon.org/na2014">EclipseCon 2014</a></li>
				<li><a href="https://www.eclipsecon.org/na2014/keynote-speakers">Keynote Speakers</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

