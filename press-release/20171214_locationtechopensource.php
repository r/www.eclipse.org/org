<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Open Source Community Accelerates Big Data Analytics for Geospatial Solutions";
$pageKeywords	= "eclipse, locationtech, gepspatial, open source, big data, project";
$pageAuthor		= "Ian Skerrett";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
		# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<h4><em><span>LocationTech open source community announces new project releases that provide core technology for geospatial big data analytic solutions</span></em></h4>

<p><strong>Ottawa, Canada - December 14, 2017</strong> - LocationTech, an Eclipse Foundation Working Group and a community that builds software for geospatial technology, is pleased to announce the release of five open source projects that provide core technology used to build geospatial big data analytics solutions. These project releases reflect the growing commitment of the LocationTech community to be a leading source of innovation for the geospatial industry.</p>

<p>LocationTech is an open source community that provides technology for the $500 billion worldwide geospatial industry. For instance, LocationTech projects can be used to efficiently process satellite images, analyze maps for the agriculture industry, visualize smart city sensor data, and in many other geospatial use cases. The community is a collaboration among key geospatial organizations, including Boundless, Red Hat, Radiant Solutions, IBM, and Oracle. The LocationTech community has grown to include nine open source projects, 18 member organizations, and over 100 developers.</p>

<p>&ldquo;Geospatial big data analytics technology is becoming more and more important across all industries, such as agriculture, transportation, and government,&rdquo; explains Mike Milinkovich, executive director of the Eclipse Foundation. &ldquo;LocationTech is delivering on the promise of providing key technology for companies that enable large-scale analytics of geospatial data. Having an open source community, like LocationTech, that accelerates adoption and innovation of geospatial technology will have a significant impact on the entire industry.&rdquo;</p>

<p>The vision of the LocationTech community is to be the leading provider of core technology for geospatial big data analytics. The five projects being released reflect the growing investment towards achieving this vision. The new project releases include the following:</p>
<a href="https://locationtech.github.io/geowave/">GeoWave</a> is a software library that connects the scalability of distributed computing frameworks and key-value stores with modern geospatial software to store, retrieve, and analyze massive geospatial datasets. GeoWave takes multidimensional data, such as spatial or spatial-temporal, and indexes it into a key-value store such as Apache Accumulo or Apache HBase. These distributed storage technologies, in addition to complementary distributing processing frameworks such as Apache Hadoop and Apache Spark, have proven capabilities to unlock the potential of massive datasets across a variety of domains.<br />
<br />
<a href="http://geogig.org/">GeoGig</a> 1.2 is a tool for geospatial data versioning. It enables users to leverage versioning of their geospatial data and to enable replication and synchronization workflows, in addition to supporting end-to-end data management workflows. The new GeoGig 1.2 release improves the collaborative version workflow by improving cloning and push/pull performance and provides an updated Web API to align with the latest version of GeoServer.<br />
<br />
<a href="https://www.locationtech.org/projects/technology.geotrellis">GeoTrellis</a> 1.2 is a geographic data processing Scala library designed to work with large geospatial raster datasets. The tool provides developers with a set of utilities to help create useful, high performing web services that load and manipulate raster data (data normally used to represent satellite or aerial images). The new release includes a number of optimizations and new features including distributed computation support for viewshed and Euclidean distance through Apache Spark

<p><a href="http://www.geomesa.org/">GeoMesa</a> 1.3.5 is a distributed, spatio-temporal database built on a number of distributed cloud data storage systems, including Apache Accumulo, Apache HBase, Apache Cassandra, and Apache Kafka. The suite of tools brings spatial-temporal data, real-time IoT, and sensor workloads to the cloud. GeoMesa&rsquo;s novel indexing schema enables efficient queries resulting in rapid access to large data stores for any client application.</p>
<a href="https://github.com/locationtech/jts">Java Topology Suite</a> (JTS) 1.15 is a Java library for vector geometry providing spatial data types, spatial relationships and spatial operations. JTS is an established open source project that recently moved to the LocationTech community. New technical features for JTS 1.15 include K-Nearest Neighbor search for STR-Tree, improved handling of Quadtree queries, support for GeometryCollection, and a new JTSTestRunner command-line application. This initial LocationTech release the project is changing from LGPL to a dual license of Eclipse Distribution License (EDL) / Eclipse Public License (EPL) . This license change opens up JTS to a wider range of organizations and applications.

<p>&ldquo;LocationTech is becoming the critical nexus for organizations looking to develop and deploy geospatial Big Data solutions,&rdquo; says Eddie Pickle, Managing Director of Open Source Programs at Radiant Solutions.</p>

<p>&quot;The latest release of GeoGig to LocationTech represents a huge leap forward. &nbsp;Not only does it support versioning workflows for traditional geospatial data, but it is now optimized for spatio-temporal analysis of big data and streaming datasets from IoT sensors,&rdquo; &nbsp;says Anthony Calamito, Chief Geospatial Officer and Vice President of Products</p>

<p>Additional information about the five project releases and the other LocationTech projects is available at <a href="http://www.locationtech.org/">www.locationtech.org/</a>.</p>

<p>The LocationTech Working Group is also organizing the annual FOSS4G NA conference on May 14-16, 2018 in St. Louis, Missouri, followed by a Community Day on May 17. Members of the LocationTech community will be speaking and showcasing the open source projects at this conference.</p>

<h3>About the Eclipse Foundation</h3>

<p>The Eclipse Foundation is a not-for-profit organization that supports a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects with the Eclipse Foundation, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. Eclipse Foundation Working Groups allow organizations to collaborate on building innovative new technology to meet specific industry needs.  More information is available at eclipse.org.</p>

  </div>
	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.locationtech.org/list-of-projects">LocationTech Projects</a></li>
                <li><a target="_blank" href="https://2018.foss4g-na.org/">FOSS4G NA 2018</a></li>
				<li><a target="_blank" href="https://www.locationtech.org/">LocationTech</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

