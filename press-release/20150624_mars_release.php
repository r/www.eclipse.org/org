<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Ships Tenth Annual Release Train";
	$pageKeywords	= "eclipse, eclipsecon, release, mars";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
				
			
			<p><i>Mars release now available for download</i></p>					
			<p><b>Ottawa, Canada - June 24, 2015</b> - The Eclipse Foundation is proud to announce the availability of the Mars release, 
				the annual Eclipse release train. This is the tenth year the Eclipse community has shipped a coordinated release of 
				multiple Eclipse projects. The Mars release represents the work of 79 different open source projects, 65 million lines 
				of code and involved 380 Eclipse committers.</p>
			<p>Ten years of shipping coordinated release trains, which were all on time to the day, is a great accomplishment by the Eclipse community," 
				explains Mike Milinkovich, Executive Director of the Eclipse Foundation. "This predictable release schedule has been a huge benefit to 
				our community and ecosystem, and has encouraged widespread adoption of Eclipse technology."</p>
				
			<h2>Key Highlights of Mars Release</h2> 
				<ul>
					<li>Java developers can take advantage of new features in the Eclipse IDE, including new Quick Fixes for Java 8, a hierarchical presentation 
				for nested projects in the Project Explorer, the ability to customize perspectives, and speed improvements for text search.</li> 
					<li>The <a href="https://wiki.eclipse.org/Linux_Tools_Project/Docker_Tooling/User_Guide">Linux Tools</a> project now includes integrated tools that allow Eclipse users to build and manage Docker containers/images.</li> 
					<li>A new project, called <a href="https://projects.eclipse.org/projects/tools.oomph">Oomph</a>, makes it significantly easier to install 
				an Eclipse IDE and to provision a project-specific Eclipse workspace. Oomph also makes it possible to record and share user preference settings across workspaces.</li> 
					<li>Eclipse users will now be able to benefit from first-class Gradle support inside the Eclipse IDE. The new project, <a href="https://projects.eclipse.org/projects/tools.buildship">Buildship</a>, makes it 
				possible for developers to set up, configure, and initiate Gradle builds from Eclipse.</li> 
					<li>Improved <a href="https://www.eclipse.org/m2e/">Maven support</a> makes it easier for developers to use Maven from Eclipse. The new release includes support for Maven 3.3.3, 
				improved Maven archetypes integration, enhanced auto-completion in the pom editor, and experimental support for automatic configuration updates.</li> 
					<li><a href="https://projects.eclipse.org/projects/modeling.sirius">Sirius 3.0</a> introduces new usability features to help users create diagrams, significant performance gains for large models, and 
				improvements to the query language that makes it easier to write and validate expressions.</li> 
					<li>The <a href="https://projects.eclipse.org/projects/technology.jubula">Jubula</a> project has released a Client API that allows developers to create test cases in Java. This allows Java developers to 
				develop and maintain test cases in Eclipse and store those test cases in code repositories, such as Git.</li> 
					<li>In an effort to improve the quality of all Eclipse projects, a new <a href="https://dev.eclipse.org/recommenders/community/confess/#/about">automated error reporting feature</a> has been added to the Eclipse 
				packages. If an error occurs in Eclipse, the user will be prompted to automatically send an error report to the appropriate Eclipse project team.</li> 
					<li>An early access version of the <a href="http://marketplace.eclipse.org/content/eclipse-java%E2%84%A2-9-support-beta-mars">Java<sup>TM</sup> 9 support</a> in Eclipse is available on the Eclipse Marketplace. This will allow 
				Eclipse users to use Java 9 in the Eclipse IDE.</li> 
				</ul>						
														
			<p>The Mars release is the tenth year the Eclipse community has shipped a release train. The full list of release trains includes:</p>
				<ul>	
					<li>Mars, June 24, 2015</li>
					<li>Luna, June 25, 2014</li>
					<li>Kepler, June 26, 2013</li>
					<li>Juno, June 27, 2012</li>
					<li>Indigo, June 22, 2011</li>
					<li>Helios, June 23, 2010</li>
					<li>Galileo, June 24, 2009</li>
					<li>Ganymede, June 25, 2008</li>
					<li>Europa, June 27, 2007</li>
					<li>Callisto, June 26, 2006</li>
				</ul>

			<p>The Mars release is available for download today. More information about the Mars release train is available at <a href="https://www.eclipse.org/mars/">eclipse.org/mars</a></p>
				
			<h3>About the Eclipse Foundation</h3>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and 
				managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, 
				and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at 
				<a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
						
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipse.org/mars/">Mars Release</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

