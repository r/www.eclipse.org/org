<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Luna Release Train Now Available";
	$pageKeywords	= "eclipse, foundation, luna, release, 2014";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – June 25, 2014</b> - The Eclipse Foundation is pleased to announce the availability of the Luna release, 
				the annual release train from the Eclipse community. This year 76 projects are participating in the release that includes 
				61 million lines of code and was developed by over 340 Eclipse committers. This is the ninth year the Eclipse community has planned, 
				developed, and delivered a coordinated release that allows users and adopters to update their Eclipse installations at one time.</p>
				
		<p>"Shipping an annual release train on a predictable schedule is an amazing accomplishment for any open source community,” said Mike Milinkovich, 
				executive director of the Eclipse Foundation. “Congratulations to all the Eclipse committers that make it possible. It is the hard work of 
				these developers that make the annual release train a reality. The benefit to the wider community has been that Eclipse is a predictable 
				and reliable supplier of open source technology to the greater software industry."</p>
				
		<p>Highlights from the Luna release include the following:</p>
			<ul>
				<li><b>Java 8 Support for Eclipse Projects:</b> Eclipse Luna includes official support for Java™ 8 in the Java development tools, Plug-in Development 
				Tools, Object Teams, Eclipse Communication Framework, Maven integration, Xtext, and Xtend. The Eclipse compiler includes language enhancements, 
				search and refactoring, Quick Assist and Clean Up to migrate anonymous classes to lambda expressions and back, and new formatter options for lambdas.</li>
				<li><b>Support for OSGi R6:</b> Eclipse Equinox now supports the newly published OSGi R6 specification. In addition, Eclipse ECF Remote Service/Remote 
				Service Admin standard has been enhanced to use Java 8's CompleteableFuture for asynchronous remote services.</li>
				<li><b>New Eclipse Paho 1.0 release:</b> The Paho 1.0 release provides client libraries, utilities, and test material for the MQTT and MQTT-SN messaging 
				protocols.  MQTT and MQTT-SN are designed for existing, new, and emerging solutions for Machine-to-Machine (M2M) and Internet of Things (IoT).  
				Paho includes client libraries in Java, C/C++, Python, and JavaScript for desktop, embedded, and mobile devices.</li>
				<li><b>Workbench UI Improvements:</b>  A number of UI improvements to the Eclipse Workbench have been included in Luna, including a new dark theme, 
				split editors, line numbers on by default, reduced whitespace in default presentation, and ability to hide the "quick access" bar.</li>
				<li><b>Updated PHP Development Tools Package:</b> The PHP Development Tools come with support for PHP 5.5 and improved performance in the PHP editor. 
				The "Eclipse for PHP Developers" package on the Eclipse download site provides an easier way to start developing PHP applications.</li>
				<li><b>Crowd-sourced API Recommendations:</b>  Eclipse Code Recommenders integrates the Snipmatch code snippet search engine and adds the ability 
				to easily contribute new snippets to a shared repository of API recommendations.</li>
			</ul>
		<p>There are also eight new projects participating in the Luna release, including EMF Client Platform, EMFStore, Sirius, BPMN2 Modeler Project, Business Process Model and Notation (BPMN2), Paho, QVTd (QVT Declarative), and XWT.</p>
		<p>More information and downloads are available from <a href="https://www.eclipse.org/luna/">https://www.eclipse.org/luna/</a>.</p>
				
				
</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipse.org/luna/">Luna Release</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

