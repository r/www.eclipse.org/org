<?php
/**
 * ************************************************************************
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which accompanies
 * this distribution, and is available at http://eclipse.org/legal/epl-2.0
 * ************************************************************************
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

// Begin: page-specific settings. Change these.
$pageTitle = "Winners of Open IoT Challenge 4.0 Demonstrate How Open Source Accelerates Innovation";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse iot, open iot challenge, iot, internet of things, open iot solutions");

// Custom Metatags
$Theme->setMetaDescription("Eclipse IoT is pleased to announce the winners of the fourth annual Open IoT Challenge!");
$Theme->setMetaImage("https://www.eclipse.org/org/workinggroups/assets/images/wg_iot.png");
$Theme->setMetaImageHeight('122');
$Theme->setMetaImageWidth('140');

include ("_projectCommon.php");

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML
<div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>

  		<p><b>Ottawa, Canada – April 4, 2018</b> – <a target="_blank" href="https://iot.eclipse.org">Eclipse IoT</a>, an Eclipse Foundation Working Group, is pleased to announce the winners of the fourth annual <a target="_blank" href="https://iot.eclipse.org/open-iot-challenge">Open IoT Challenge</a>! Each year, this developer challenge is organized to promote the use of open source and open standards in Internet of Things (IoT) solutions. This year the winners were selected by a panel of judges comprised of representatives from Bitreactive, CONTACT Software, Eurotech, Intel, Red Hat and Sierra Wireless.</p>

<p>The top 3 winning teams of the Open IoT Challenge 4.0 are:</p>

<h3>1st place: <a target="_blank" href="https://www.espinr.es/post/171893735073/activioty-timekeeping-report">Activ-IoTy</a></h3>
<p>Activ-IoTy Timekeeping is a modular low-cost solution for timekeeping and athletics competition management. It is based on standard technologies such as RFID, JSON-LD, and MQTT that enable to easily acquire, publish, and share information related to athletics events. Using Eclipse Mosquitto, Eclipse Paho, and many other open source projects, the Activ-IoTy team has built a fully functional IoT solution and the judging panel was particularly impressed by the thorough testing efforts of the team in real-life environments.</p>

<h3>2nd place: <a target="_blank" href="https://trusted827.wordpress.com/2018/03/15/final-report/">Trusting IoT</a></h3>
<p>Trusting IoT provides a trusted indoor positioning system for nursing homes that can be used to guarantee appropriate care of the elderly. The solution uses Eclipse Kura and Bluetooth low-energy beacons to implement the indoor positioning system. The IOTA distributed ledger is used as a way to store a permanent and unalterable history of the positions of the residents at any point in time.</p>

<h3>3rd place: <a target="_blank" href="http://dev.bruscino.eu/2018/03/smart-solar-water-heating-solution.html">Smart Solar Water Heating</a></h3>
<p>Smart Solar Water Heating uses IoT and AI technologies to improve the efficiency of passive solar water heating systems. This project leverages open standards such as OMA LWM2M MQTT, several open source projects (including Eclipse Wakaama, Eclipse Leshan, Eclipse Kura) for telemetry and control, as well as other open source projects (including Enmasse and Apache Spark deployed using the OpenShift Origin platform) to enable device connectivity and to help predict the availability of hot water.</p>


<h2>Special Acknowledgement</h2>
<p>There were 78 entering teams competing to build the best solution, 15 of which submitted a <a target="_blank" href="https://iot.eclipse.org/open-iot-challenge-entries/">final report</a> for their project. The judging panel would like to acknowledge the outstanding quality of this year’s entries. From <a target="_blank" href="https://medium.com/@bilal.alsaeedi/alya-smart-mirror-openiotchallenge-final-report-a8cc8e95885">smart mirrors</a>, to <a target="_blank" href="http://dronov.net/2018/03/15/open-iot-challenge-4-final.html">cloud-connected oscilloscopes</a>, to <a target="_blank" href="https://demirago.wixsite.com/poulpyproject/open-iot-challenge-4-0">connected aquariums</a> helping understand marine life, this year’s finalists have demonstrated a perfect understanding of IoT business cases and a very strong technical expertise of the open source IoT projects they used.</p>

<p>The Open IoT Developer Challenge is an annual challenge organized by the Eclipse IoT Working Group, in collaboration with the sponsors Bitreactive, CONTACT Software, Eurotech, Intel, Red Hat and Sierra Wireless. Participants are given three months to create an IoT solution using open source software and open standards. The three winners are selected by a panel of judges, who evaluate the participating solutions based on the applicability of a solution to a specific industry, innovation, completeness, use of open source and open standards, and communication with the larger community.</p>

<p>More details about the Challenge and the finalists are available at <a target="_blank" href="https://iot.eclipse.org/open-iot-challenge">https://iot.eclipse.org/open-iot-challenge</a>.</p>

<h2>About Eclipse IoT and Eclipse Foundation</h2>
<p>The <a target="_blank" href="https://iot.eclipse.org">Eclipse IoT Working Group</a> is a collaborative working group hosted by the Eclipse Foundation, that is working to create open source software for IoT solutions. Eclipse IoT is made up of over 40 member companies and 30 open source projects.</p>
<p>Eclipse is a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects in the Eclipse community, ranging from tools for software developers, geospatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tools for scientific research, and much more. The Eclipse Foundation is a not-for-profit foundation that is the steward of the Eclipse community. More information is available at <a target="_blank" href="http://www.eclipse.org/">eclipse.org</a>.</p>


      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
  				<li><a href="http://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge</a></li>
  				<li><a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
