<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "La sortie simultanée Eclipse annuelle prête au téléchargement";
	$pageKeywords	= "eclipse, foundation, luna, sortie simultanée, 2014";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – 25 juin, 2014</b> - La fondation Eclipse est heureuse d’annoncer l’arrivée de Luna : 
				la nouvelle sortie simultanée d’Eclipse. Cette année, 76 projets ont participé, avec 340 committers 
				impliqués et 61 millions de ligne de code contribuées. C’est la neuvième année que la communauté Eclipse 
				planifie, développe et livre une sortie simultanée de ses projets majeurs pour permettre aux utilisateurs 
				de mettre à jour leur installation Eclipse en une seule fois.</p>

		<p>« Mener une sortie simultanée avec un agenda prévisible est un accomplissement exceptionnel pour n’importe quel 
				communauté Open Source » explique Mike Milinkovich, Directeur Exécutif de la Fondation Eclipse. « Félicitations 
				à tous les committers Eclipse qui rendent cela possible. C’est un travail dur pour ces développeurs qui font de 
				cette sortie simultanée annuelle une réalité. Eclipse est un fournisseur de technologies Open Source fiables, 
				prévisibles et offertes à l’ensemble de industrie logicielle et cela profite ainsi à toute la communauté. »</p>

		<p>Voici quelques-unes des principales nouveautés pour Luna :</p>	
			<ul>
				<li><b>Support Java 8 pour les projets Eclipse</b> : Luna ajoute le support officiel pour Java 8 dans les Java Development 
				Tools, Plug-in Development Tools, Object Teams, Eclipse Communication Framework, Maven integration, Xtext et Xtend. 
				Le compilateur Eclipse inclut les améliorations du langage, la recherche et le refactoring, Quick Assist et Clean 
				Up pour migrer les classes anonymes vers des lambda expressions et inversement, et de nouvelles options de formatage pour les lambdas.</li>
				<li><b>Support de OSGi R6</b> : Eclipse Equinox supporte maintenant la nouvelle version de la spécification OSGi R6. De plus, le 
				standard Eclipse ECF Remote Service/Remote Service Admin a été amélioré pour l’utilisation de CompleteableFuture de Java 8 pour les services distants asynchrones.</li>
				<li><b>Eclipse Paho 1.0</b> : Paho 1.0 fournit des librairies clientes, des services et des tests matériels pour les protocoles de message MQTT et MQTT-SN. 
				MQTT et MQTT-SN sont conçus pour les solutions existantes, nouvelles et émergentes pour le Machine-to-Machine (M2M) et l’Internet of Things (IoT). 
				Paho inclut des librairies clientes en Java, C/C++, Python et JavaScript pour les systèmes desktop, embarqués et mobiles.</li>
				<li><b>Amélioration de l’interface utilisateur</b> : Un nombre important d’améliorations de l’UI pour le Workbench Eclipse ont été 
				ajoutées pour Luna, incluant un thème sombre, des éditeurs scindés, l’affichage des numéros de ligne par défaut, la réduction 
				des espaces et la possibilité de cacher la barre quick access.</li>
				<li><b>Mise à jour du package PHP Development Tools</b> : Les outils de développement PHP sous Eclipse intègrent maintenant le support 
				pour PHP 5.5 et l’éditeur PHP bénéficient de meilleures performances. Le package « Eclipse for PHP Developers » permet ainsi de 
				démarrer plus simplement des applications PHP.</li>
				<li><b>Recommendations d’API participatives</b> : Eclipse Code Recommenders intègre le moteur de recherche de code snippet Snipmatch et 
				ajoute la possibilité de facilement contribuer de nouveaux snippets dans un référentiel partagé de recommendations d’API.</li>
			</ul>
		<p>Il y a aussi 8 nouveaux projets qui participant à la sortie à Luna, incluant EMF Client Platform, EMFStore, Sirius, BPMN2 Modeler Project, 
				Business Process Model and Notation (BPMN2), Paho, QVTd (QVT Declarative) et XWT.</p>

		<p>Tous les projets qui participent à la sortie Luna sont dès maintenant disponibles au téléchargement sur : <a href="https://www.eclipse.org/luna/">https://www.eclipse.org/luna/</a>. 
				En complément, douze packages basés sur des profils développeurs différents rendent plus facile le téléchargement de cette nouvelle version.</p>

		<p><b>A propos de la fondation Eclipse</b></p>
		<p>Eclipse est une communauté Open Source dont les projets sont centrés sur la construction d’une plateforme de développement ouverte composée de 
				frameworks extensibles, outils et runtimes pour construire, déployer, et gérer le cycle de vie logiciel. Un large et dynamique écosystème 
				d’éditeurs logiciels majeurs, de start-ups innovantes, d’instituts de recherche, et d’individuels étendent, complètent et supportent la plateforme Eclipse.</p>
		<p>La fondation Eclipse est une organisation à but non lucratif, financée par ses membres et qui hébergent les projets Eclipse. Le détail complet pour Eclipse et la 
				fondation Eclipse est disponible sur <a href="http://www.eclipse.org/">www.eclipse.org</a>.</p>
				
				
</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipse.org/luna/">Luna Release</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

