<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Announcing Eclipse IoT Day San Jose 2017";
	$pageKeywords	= "eclipse, iot, iot day, san jose";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

			<p>The Eclipse IoT Working Group is pleased to announce the Eclipse IoT Day San Jose 2017. The one day event will take place on March 20 at the San Jose Convention Center, in co-location with <a href="https://www.eclipseconverge.org/na2017/">Eclipse Converge</a> and <a target="_blank" href="https://devoxx.us/">Devoxx US</a>.</p>

			<p>The focus of the event is open source and open standards for building IoT solutions. Attendees will learn about the software needed to build devices, gateways, and IoT cloud platforms.</p> 
			
			<p>The agenda is packed with great speakers from companies including, Bosch SI, Deutsche Telekom, Eurotech, Intel, IOTRACKS, MicroEJ, Red Hat and Samsung. They will be covering a wide range of IoT topics, from Industry 4.0 and fog computing for devices at the edge, to smart homes and more!</p> 
			
			<p>Registration for the event is open. Register or get more details at <a href="https://iot.eclipse.org/eclipse-iot-day-san-jose/">https://iot.eclipse.org/eclipse-iot-day-san-jose/</a></p>
				
				<p align="center"><a href="https://iot.eclipse.org/eclipse-iot-day-san-jose/"><img class="img-responsive" src="/org/images/iotday.png" alt="iot day san jose"/></a>
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://iot.eclipse.org/eclipse-iot-day-san-jose/">Eclipse IoT Day | San Jose 2017</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

