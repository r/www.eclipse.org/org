<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Eclipse Foundation supports EU funded Brain-IoT Project";
$pageKeywords	= "eclipse, EU, IoT, funding, project, open source";
$pageAuthor		= "Philippe Krief";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
				
<h4><em><span>Eclipse Foundation Europe Selected to Provide Open Source Community Building Expertise for EU funded IoT Research Project</span></em></h4>
				
<p>Zwingenberg, Germany &mdash; A European consortium of twelve partners from France, Germany, Italy, Spain and the UK, won approval to begin work on Brain-IoT, a &euro;5 million research project funded by the EU Framework Programme for Research and Innovation Horizon 2020.</p>
				
<p>In line with the optimistic forecasts released over the last few years, Internet of Things (IoT) products and services are deployed more and more in mass-market and professional usage scenarios, becoming a reality in our day-to-day lives. Commercial and pilot deployments world-wide are progressively demonstrating the value of IoT solutions in real conditions, but also raising some concerns in terms of dependability, security, privacy and safety constraints.</p>
				
<p>In this context, Brain-IoT aims at establishing a framework and methodology that supports smart autonomous and cooperative behaviors of populations of heterogeneous IoT platforms that are also closely interacting with Cyber-Physical systems (CPS). Brain-IoT will employ highly dynamic federations of heterogeneous IoT platforms, mechanisms enforcing privacy and data ownership policies as well as open semantic models enabling interoperable operations and exchange of data and control features. Brain-IoT will also offer model-based tools easing the development of innovative, tightly integrated IoT and CPS solutions.</p>
				
<p>The viability of the proposed approaches is demonstrated in two futuristic usage scenarios: Service Robotics and Critical Water Infrastructure Management. As well, viability will be demonstrated through collaboration with on-going, large-scale, pilot IoT initiatives.</p>
				
<p>The BRAIN-IoT Consortium is composed of twelve partners from 5 European countries and involves research centres/academia and R&amp;D teams from several large, medium and small industrial companies. The &nbsp;Brain-IoT project consortium is coordinated by Istituto Superiore Mario Boella (ISMB)- Italy and includes Commissariat a l&rsquo;Energie Atomique et aux Energie Alternatives (CEA) - France, Universit&eacute; Grenoble Alpes - France, Paremus Limited - United Kingdom, Siotic - Spain, STMicroelectronics - France, Siemens - Germany, Eclipse Foundation Europe - Germany, Institut De l&rsquo;Audiovisuel et des Telecommunications en Europe (iDATE) -France, Cassidian Cybersecurity (AIRBUS) - France, Robotnik Automation &ndash; Spain, and Empresa Municipal de Aguas de la Coruna - Spain.</p>
				
<p>&ldquo;The Eclipse Foundation Europe is pleased to participate in this Research Project,&rdquo; says Mike Milinkovich, Executive Director of the Eclipse Foundation. &nbsp;&ldquo;Our experience with building technology communities and specifically the Eclipse IoT open source community gives us a head start in contributing to Brain-IoT.&rdquo; The Eclipse Foundation will provide expertise and resources to accelerate technology adoption and community building for the BRAIN-IoT technology in the Internet of Things (IoT) industry. The organization will be responsible for hosting the open source elements of the BRAIN-IoT project, mentoring the community on open source best practices, supervising the training material development, and hosting events to promote the project community.</p>

<p>More information about the BRAIN-IoT project is available at <a href="http://brain-iot.eu">http://brain-iot.eu</a></p>
<h3>About the Eclipse Foundation</h3>
				
<p>The Eclipse Foundation is a not-for-profit organization that supports a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects with the Eclipse Foundation, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. Eclipse Foundation Working Groups allow organizations to collaborate on building innovative new technology to meet specific industry needs.</p>
				
  </div>
<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipse.org/org/research/">Research at the Eclipse Foundation</a></li>
                <li><a target="_blank" href="https://www.eclipse.org/org/workinggroups/">Eclipse Working Groups</a></li>
				<li><a target="_blank" href="https://www.eclipse.org/org/">About Us</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);