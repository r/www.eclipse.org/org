<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "PolarSys Introduces Five New Open Source Solutions for Embedded System Development";
	$pageKeywords	= "eclipse, eclipsecon, polarsys, solutions";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
			<p><i>Eclipse Working Group aims to improve tools for embedded system development through open innovation</i></p>
			<p><b>Ottawa, Canada and Embedded World, Nuremberg, Germany – February 23, 2015</b> - PolarSys, a working group of the Eclipse Foundation, is pleased to announce five new 
				industry solutions for embedded system development. The new solutions target the key aspects of embedded system development, including Systems and Software Engineering, 
				Software Development, and Verification and Validation. The first <a target="_blank" href="http://polarsys.org/solutions">PolarSys solutions will be available for download in March, 2015</a>.</p> 
		
			<p>PolarSys is a collaboration among some of the world’s largest systems engineering companies, including Airbus, Ericsson, CEA List, and Thales.  This group is dedicated 
				to creating an environment of open innovation that will create better tools and methods for systems engineering. It is expected that the resulting solutions will be better 
				suited to the requirements of modern engineering teams and will provide more flexible extensibility and customization that makes it easier to adopt the tools to the methods 
				and processes of software engineering teams.</p>
			
			<p>The new PolarSys solutions include the following:</p>
				<ul>	
					<li><b>Capella</b> is a model-based engineering solution that includes a graphical modeling workbench that helps engineers build better architectures. Capella provides the tools
				 necessary for engineering-wide collaboration through shared reference architectures and the ability to understand and define complex architectures.</li> 
					<li><b>Papyrus</b> is the PolarSys solution for SysML and UML modeling. It relies on the underlying Eclipse platform and on other Polarsys and Eclipse-based offerings to fulfill 
				lifecycle integration needs, such as a C/C++ IDE, source control (e.g. Git), and reporting. Through its integrated support for UML and SysML modeling, Papyrus provides the basis
				 for the adoption and use of Model Driven Engineering (MDE) and Model Based Systems Engineering (MBSE). In addition, partners have already built industrial tools based on Papyrus
				 to support their domains and their customers.</li> 
					<li><b>Titan</b> is a test automation toolset supporting the complete chain of test development, test execution, and results analysis.  Titan provides a full-featured TTCN-3 environment
				 and a standard test language, which makes it well suited for grey-box and black-box testing of components, systems, and models.</li> 
					<li><b>C/C++ for Embedded Linux</b> is based on the very popular Eclipse CDT IDE that is used extensively by embedded software engineering teams. CDT runs on most variants of Linux and 
				integrates with GCC, Clang, and GDB and can also be use on non-Linux embedded systems.</li> 
					<li><b>Trace Compass</b> is a powerful tool for the analysis and visualization of log files and system traces. It allows engineers and technical support personnel to diagnose problems, 
				tune for performance, and improve comprehension of system behavior.</li> 
				</ul>				
			<p>All of the PolarSys solutions are based on technology and tools that have been deployed by large-scale systems engineering teams. All the solutions are open source and are provided 
				free of charge under the Eclipse Public License (EPL).</p>
			<p>"The innovation required to advance systems engineering tools needs to be led by the key system engineering companies," explains Dominique Toupin, Chair of the PolarSys Working Group 
				and Product Manager at Ericsson. "This is why we believe the PolarSys Working Group is a perfect environment for open innovation that can include many different companies participating
				 in the development of future tools to systems engineers."</p>
			<p>The Eclipse Foundation and the PolarSys Working Group will be demonstrating the PolarSys solutions at the Embedded World conference on February 24-26 in Nuremberg, Germany. The Eclipse 
				Foundation booth will be in Hall 4, booth 160. Representatives of the Eclipse Foundation will also be presenting at the Embedded World conference: Gaël Blondelle will present "How to
				 Test Complex Systems" on February 25 at 1:30pm and Benjamin Cabé will present “Building the Internet of Things with Eclipse IoT Stack and Java” on February 26.</p>   
				
		<h3>About the PolarSys Working Group</h3>
			<p>PolarSys is an industrial working group dedicated to open source tools for embedded systems development. It addresses the full range of systems engineering activities, including modeling, 
				requirements engineering, simulation, code and debugging, and testing and verification.</p> 
			<p>PolarSys is led by large organizations such as Airbus, Atomic Energy and Alternative Energies Commission (CEA), Ericsson, and Thales.</p> 
		
		<h3>About the Eclipse Foundation</h3>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and 
				managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, 
				and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
												
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://polarsys.org/">PolarSys Website</a></li>
				<li><a target="_blank" href="http://polarsys.org/solutions">PolarSys Solutions</a></li>		
				<li><a href="https://www.eclipse.org/org/press-release/20150223_polarsys_solutions_de.php">Deutsche Version</a></li>			
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

