<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Eclipse IoT Announces Fourth Edition of the Open IoT Challenge";
$pageKeywords	= "eclipse, internet of things, iot, challenge, open source";
$pageAuthor		= "Roxanne Joncas";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
		# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p><b>Ottawa, Canada – September 28, 2017</b> - The Eclipse Internet of Things (IoT) Working Group, hosted at the Eclipse Foundation, is pleased to announce the annual <a href="http://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge</a>. The fourth edition of this challenge encourages IoT enthusiasts and developers to build innovative solutions for the Internet of Things using open standards and open source technology. The previous edition of the Challenge resulted in a great solution to track sleep and improve the life of people suffering with mood disorders.</p> 
<p>This year’s participants will once again be supported with hardware development kits, receive technical support, and the winners will receive special prizes. This year the Challenge is <a href="http://iot.eclipse.org/open-iot-challenge/sponsors#sponsors">sponsored</a> by Bitreactive, CONTACT Software, Eurotech, Red Hat, and Sierra Wireless.</p>

<p>The <a href="http://iot.eclipse.org/">Eclipse IoT Working Group</a> is a community of 27 IoT open source projects and 34 participating companies. The goal of Eclipse IoT is to provide the technology needed to build IoT Devices, Gateways, and Cloud Platforms.</p>

<p>The Open IoT Challenge encourages participants to build IoT solutions using any or all of the following technologies:</p>

<ul>
    <li>IoT open standards, such as MQTT, CoAP, Lightweight M2M, MQTT, OneM2M and OPC UA.</li>
    <li>Eclipse IoT projects, such as Californium, Kura, Leshan, Milo, Mosquitto, Paho, SmartHome, Unide, Vorto and others listed <a href="http://iot.eclipse.org/projects">here</a>.</li>
    <li>Other open source technology, such as Cloud Foundry, OpenShift, Apache Spark or Apache Camel.</li>
    <li>Commercial hardware and services that interact with open source software and open standards for IoT.</li>
</ul>

<p>Participants are required to <a href="http://iot.eclipse.org/open-iot-challenge/">submit a proposal</a> for their solution before November 13, 2017. A panel of judges will select a shortlist of participants that will receive hardware development kits. All participants will then have until March 15, 2018 to build their IoT solutions. The winners will be announced the end of March 2018.</p>

<p>Additional information about the Challenge is available at <a href="http://iot.eclipse.org/open-iot-challenge/">http://iot.eclipse.org/open-iot-challenge</a>. Information about the the previous Open IoT Challenge, including the winners, is available <a href="https://iot.eclipse.org/open-iot-challenge/third-edition/">here</a>.</p>

<p align=center"><a href="http://iot.eclipse.org/open-iot-challenge/"><img class="img-responsive" src="/org/press-release/images/challenge4_deadline.png"></a>

  </div>
	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge 4.0</a></li>
				<li><a target="_blank" href="https://iot.eclipse.org/open-iot-challenge/third-edition/">Open IoT Challenge 3.0</a></li>
                <li><a target="_blank" href="http://iot.eclipse.org">Eclipse IoT</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

