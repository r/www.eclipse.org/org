<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "La fondation Eclipse crée une filiale en Europe pour soutenir le développement de son écosystème Open Source";
	$pageKeywords	= "eclipse, eclipse fondation, filiale, europe";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

<p><b>Zwingenber, Allemagne – le 29 octobre, 2013</b> – La fondation Eclipse a annoncé la création d’une filiale européenne, Eclipse Foundation Europe GmbH (EFE), établie à Zwingenberg (Hessen) en Allemagne. EFE a été créée pour soutenir encore mieux l’importante communauté open source Eclipse qui est en pleine croissance en Europe.</p>
<p>Eclipse est une communauté open source mondial avec plus de 250 projets, plus de 1000 contributeurs et plus de 200 organisations membres qui soutiennent les opérations de la fondation, et plus de 8 millions de développeurs qui utilisent Eclipse. Un quart de la communauté d’utilisateurs Eclipse habitent en Europe et la moitié des organisations membres sont établies en Europe.</p>
<p>La nouvelle EFE a recruté une équipe en Europe pour soutenir la communauté Eclipse Européenne. L'équipe aidera les partenaires qui participent aux projets de recherche européens tels que ITEA,  fournira des services de développement et de dissémination en tant que partenaire de certains projets et participera aussi aux différents réseaux européens comme Bitkom Allemagne pour promouvoir l’innovation ouverte, les business models open source et la plateforme Eclipse.</p>
<p>Ralph Mueller a été nommé le Directeur de la fondation Eclipse en Europe. M. Mueller a une expérience de 30 ans dans l’industrie des technologies de l’information. Il a précédemment travaillé chez IBM, Object Technology International et Siemens-Nixdorf; récemment, il était Directeur de l’écosystème Européen pour la fondation Eclipse.</p>
<p>« L’Europe est le leader pour l’adoption de technologies open source, plus spécifiquement Eclipse » explique M. Mueller. « La création de l'Eclipse Foundation Europe nous permettra d’accélérer nos efforts de promotion pour les Working Groups Eclipse  et ainsi favoriser la collaboration entre industriels. »</p>
<p>Plus d’informations sur l’Eclipse Foundation Europe dans les <a href="http://eclipse.org/org/press-release/20131029_efe_faq.php">FAQ</a> (en anglais).</p>

<h2>À propos d’Eclipse</h2>
<p>Eclipse est une communauté open source dont les projets se concentrent sur la création d’une plateforme ouverte de développement basée sur des frameworks extensibles, des outils et des runtimes qui permettent de créer, déployer et gérer les logiciels durant leur cycle de vie. Un écosystème large et dynamique, composés de grands éditeurs, de PME innovantes, d’universités, d’instituts de recherche et d’individus, étend, enrichit et maintient la plateforme Eclipse.</p>
<p>La fondation Eclipse est une organisation à but non lucratif, supportée par ses membres, qui héberge les projets Eclipse. Pour plus de détails au sujet d’Eclipse et de la fondation Eclipse, voir <a href="http://eclipse.org/">www.eclipse.org</a>.</p>
					

	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/org/press-release/20131029_efe_faq.php">FAQ</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

