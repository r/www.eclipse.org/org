<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "New Industry 4.0 Open Testbed Addresses Performance Monitoring and Management in Manufacturing";
$pageKeywords	= "eclipse, industry 4.0, testbed, open source, smart manufacturing";
$pageAuthor		= "Roxanne Joncas";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
		# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p><i>Bosch Software Innovations, CONTACT Software, Eurotech, fortiss GmbH, and InfluxData Collaborate to Demonstrate Product Performance Management</i></p>

<p><b>Ottawa, Canada - October 19, 2017</b> - The Eclipse IoT Working Group, an Eclipse Foundation industry collaboration, has released the second Open IoT Testbed to address performance management and monitoring of factory equipment used in the manufacturing industry. The new <a target="_blank" href="https://iot.eclipse.org/testbeds/production-performance-management/">Production Performance Management (PPM) Testbed</a> demonstrates how performance information about factory equipment can be consistently communicated to an IoT Cloud Platform using different IoT gateways and different PLCs. Eclipse IoT member companies participating in the testbed include Bosch Software Innovations, CONTACT Software, Eurotech, fortiss GmbH, and InfluxData.</p>

<p>Key challenges for the manufacturing industry are to improve capacity, quality and flexibility, while lowering costs. To overcome these challenges, factories look to improve optimization of factory equipment, referred to as Production Performance Management (PPM). However, the heterogenous nature of the equipment used in the manufacturing industry sometimes makes it difficult to get an overall perspective. The equipment used in factories is often based on proprietary software that uses proprietary protocols, and it’s often difficult to update to more modern protocols. This environment makes it challenging to create solutions that monitor equipment across entire factory floors, and across different factories. The Eclipse Production Performance Management Testbed aims to showcase how consistent monitoring across all factory equipment will lead to improved factory equipment optimization.</p>

<p>The Eclipse IoT Open Testbeds are collaborations between vendors and open source communities to demonstrate and test commercial and open source components needed to create specific industry solutions. Each testbed will deliver a running solution and make its source code available under an open source license. Previously announced is an <a target="_blank" href="https://iot.eclipse.org/testbeds/asset-tracking/">Asset Tracking Testbed</a> that demonstrates and tests how assets with various sensors can be tracked via different IoT gateways. The gateways report data in real time to an IoT cloud platform, or use the events to automate control of various systems.</p>

<p>The PPM Testbed is based on a new industry protocol called Production Performance Management Protocol (PPMP) which is part of the Eclipse Unide project. PPMP is an open standard that helps process and aggregate production performance metrics, making the world of OT easily consumable by existing IT systems and processes. The testbed features two IoT gateways: Eclipse Kura for general purpose hardware and Eclipse 4diac for PLC hardware. These gateways generate PPMP messages and send them to an IoT Cloud service that runs InfluxDB. As part of the testbed, real machine telemetry data samples are made available to test and demonstrate the testbed. The source code for all the components of the testbed is also available under an open source license, so anyone can download and install the PPM testbed.</p>

<p>The debut of the PPM Testbed will be at <a target="_blank" href="https://www.eclipsecon.org/europe2017/">EclipseCon Europe 2017</a> on October 24-26 in Ludwigsburg, Germany. Attendees will be able to see the PPM Testbed at the Eclipse IoT WG meeting on October 23. More information about the Eclipse Open IoT Testbeds can be found at <a target="_blank" href="https://iot.eclipse.org/testbeds">https://iot.eclipse.org/testbeds</a>. Companies interested in showcasing their support of IoT open source and open standards are welcome to participate in the PPM testbed and in any new testbeds.</p>

<p>The Eclipse IoT Working Group has also released a white paper, <a target="_blank" href="https://iot.eclipse.org/white-paper-industry-40">Open Source Software for Industry 4.0</a>, that documents the software challenges and requirements for successful Industry 4.0 implementations. In addition, the white paper discusses how open source software, and in particular technology from the Eclipse IoT projects, can be used to address some of the software requirements for Industry 4.0 solutions.</p> 

<h2>Quotes</h2>

<p><b>Bosch Software Innovations</b><br>
"The Production Performance Management Testbed shows how simple performance monitoring can be quickly implemented using open standards and open source software, like Eclipse Unide. This collaboration shows how different vendors can work together to solve Industry 4.0 solutions."<br>
- Dr. Nils-H. Schmidt, Portfolio Management Industrial IoT, Bosch Software Innovations</p>

<p><b>CONTACT Software</b><br>
“CONTACT is committed to the outstanding work of the Eclipse Open Source community. The PPM Open IoT testbed and our implementation of the PPMP protocol are excellent examples, stimulating future editions of CONTACT’s Elements for IoT framework.”<br>
- Frank Patz-Brockmann, Director Software-Development, CONTACT Software</p>

<p><b>Eurotech</b><br>
“We are excited to support the new Eclipse IoT Open Testbeds for Industry 4.0 with our partners. We continue to believe that testbeds are an important initiative in showcasing how open source components provide solid building blocks to accelerate the development and the deployment of IoT solutions and applications.”<br> 
- Marco Carrer, CTO, Eurotech Group</p>

<p><b>fortiss GmbH</b><br>
“With the Production Performance Management Testbed we can show how production performance data can easily be generated within Eclipse 4diac PLCs programs reducing the effort of integrating monitoring functionalities in PLC programs.”<br>
- Dr. Alois Zoitl, Head of Competence Field Industrie 4.0, fortiss GmbH</p>

<p><b>InfluxData</b><br>
"InfluxData is pleased to be contributing to the Eclipse IoT Working Group’s Open IoT Testbed. As the leader in time-series event monitoring, InfluxData is excited to see the Open IoT Testbed’s continued progress towards IoT monitoring solutions which utilize time series data to increase efficiencies and lower costs.”<br>
- David G. Simmons, Senior Developer Evangelist, InfluxData</p>

<h3>About the Eclipse Foundation</h3>

<p>The Eclipse Foundation is a not-for-profit organization that supports a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects with the Eclipse Foundation, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. Eclipse Foundation Working Groups allow organizations to collaborate on building innovative new technology to meet specific industry needs.  More information is available at eclipse.org.</p>

  </div>
	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://iot.eclipse.org/white-paper-industry-40">Open Source Software for Industry 4.0</a></li>
                <li><a target="_blank" href="https://iot.eclipse.org/testbeds">Open IoT Testbeds</a></li>
			<li><a target="_blank" href="https://iot.eclipse.org/">Eclipse IoT</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

