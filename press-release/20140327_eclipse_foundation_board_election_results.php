<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "2014 Eclipse Foundation Board Election Results";
	$pageKeywords	= "eclipse, foundation, board election, election results, committer member";
	$pageAuthor		= "Christopher Guindon";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – March 27, 2014</b> - The Eclipse Foundation is pleased to announce the
				results of the 2014 <a href="//www.eclipse.org/org/elections/">Board of Directors elections</a>.</p>

		<p style="margin-bottom:0">The elected Committer Member representatives are:</p>
<ul style="padding-bottom:15px;">
    <li>John Arthorne</li>
    <li>Ed Merks</li>
</ul>

<p style="margin-bottom:0">The elected Sustaining Member representatives are:</p>
<ul style="padding-bottom:15px;">
   <li>Max Rydahl Andersen (Red Hat)</li>
    <li>Mik Kersten (Tasktop)</li>
</ul>

<p>The elected directors will be serving on the Board from April 1, 2014 to March 31, 2015.</p>

<p>The Eclipse Foundation would like to thank Werner Keil for running for the position of Committer representative.
We would also like to thank and recognize the many contributions of the outgoing directors:
Chris Aniszczyk, Christian Dupuis (Pivotal), and Hans Kamutzki (MicroDoc).
Their presence on the Board of Directors was of great value to the Eclipse community.</p>

</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="//www.eclipse.org/org/elections/">Board of Directors elections</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

