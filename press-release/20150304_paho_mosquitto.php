<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "New Release of Eclipse Paho and Eclipse Mosquitto Continue Momentum of the Growing Open Source IoT Community and MQTT";
	$pageKeywords	= "eclipse, eclipsecon, paho, mosquitto, iot, mqtt";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
			<p><b>Ottawa, Canada – March 4, 2015</b> – The Eclipse Foundation is pleased to announce the release of Eclipse Paho 1.1 and Eclipse Mosquitto 1.4, two open source projects that implement the client 
				and broker for the OASIS MQTT standard. Both project releases continue the momentum enjoyed by the Eclipse IoT open source community and the adoption of the MQTT standard by IoT solution providers.</p>
			
			<p>Eclipse IoT is an open source community focused on building open source technology for IoT developers. The community has 15 different open source projects that implement popular IoT standards, 
				such as MQTT, Lightweight M2M, and CoAP, and IoT frameworks that developers can use to accelerate development of IoT solutions.</p>
				  
			<p>MQTT is a popular IoT messaging protocol that has been widely adopted by IoT solution providers. It is a lightweight publish/subscribe messaging transport that is ideally suited for connecting 
				small devices to the Internet. It requires a small footprint on the client, minimal bandwidth for the messages, and adapts well to unreliable network connections. MQTT 3.1.1 was ratified 
				as an OASIS standard in November, 2014.</p>
			
			<p><b>Paho 1.1</b> implements the MQTT client specification. New to the 1.1 release are</p>
				<ul>	
					<li>Support for Microsoft .NET, WinRT and Android clients</li>
					<li>C and C++ libraries for embedded clients</li>
					<li>Updated versions of the Java, Python, and JavaScript clients to conform to the MQTT 3.1.1 standard</li>
				</ul>
			<p>Paho 1.1 is now <a href="https://www.eclipse.org/paho/#downloads">available for download</a>.
				
			<p><b>Mosquitto 1.4</b> implements the MQTT broker specification. New to the 1.4 release are</p>
				<ul>
					<li>Easier integration with web sites via support for WebSockets</li>
					<li>Improved security by providing more flexible support for TLS v1.2, 1.1 and 1.0 plus support for ECDHE-ECDSA family ciphers</li>
					<li>Better interoperability between MQTT brokers via improved bridge support, including wildcard TLS certificates and conformance to MQTT 3.1.1</li>
				</ul>
			<p>Mosquitto 1.4 is <a href="https://www.eclipse.org/mosquitto/download/#source">now available for download</a>.</p>
				
			<p>"In the last year we have seen tremendous interest in the Eclipse IoT community, and in particular Paho and Mosquitto," explains Mike Milinkovich, Executive Director of the Eclipse Foundation. 
				"Forty developers contributed to the new Paho and Mosquitto releases, demonstrating incredible interest for these projects and MQTT in general."</p>
			<p>Paho and Mosquitto are being used by companies and enterprises to add support for MQTT in IoT solutions. The projects’ open source licenses allows for royalty-free use of the technology.  
				Here are some companies using Paho and Mosquitto:</p>
			
			<p><b>Bluewind</b><br>
			<p>At Bluewind we decided to use Mosquitto and Paho MQTT tools and libraries to build a new coffee machine for Illy, both for its innovative cloud features and for internal interprocess 
				communication. Mosquitto and Paho leads to having a very fast and stable software design while giving peace of mind on security, for a product that will be constantly connected and 
				supervised via the Internet.</p>
			<p><b>dc-square</b><br>
			Eclipse Paho is the client library of choice of most of our HiveMQ MQTT broker customers. The Java and C clients are of high quality and are rock stable. The 1.1 release pushes the quality 
				of the libraries even further.</p>
			<p><b>logi.cals GmbH</b><br>
			logi.cals GmbH decided to use MQTT to connect different target systems of our Soft-PLC (logi.RTS). As our engineering platform (logi.CAD3) is based on the Eclipse framework, we decided to 
				use the Eclipse Paho framework for our developments. Eclipse Paho and the Mosquitto MQTT-broker enabled us to jump-start the development of innovative products for both the IoT world as 
				well as for the industrial sector. The easy to use API, the large user base and the comprehensive documentation make it an unbeatable solution for a huge amount of embedded applications. 
				Thank you and keep up the good work!</p>
			<p><b>SSV Software Systems GmbH</b><br>
			In our Real Time Data Channel (RTDC) solution that connects embedded industrial sensors with the IoT, we use the Eclipse Paho libraries and the Eclipse MQTT broker Mosquitto to provide a 
				high quality, highly secured IoT-platform. Several industrial customers use RTDC to monitor their factory equipment in real time. The Paho libraries and the Mosquitto broker helped us 
				to bring these solutions to the market.</p>
							
		<h3>About the Eclipse Foundation</h3>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and 
				managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, 
				and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at 
				<a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
												
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipse.org/paho/#downloads">Paho 1.1 Download</a></li>
				<li><a href="https://www.eclipse.org/mosquitto/download/#source">Mosquitto 1.4 Download</a></li>	
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

