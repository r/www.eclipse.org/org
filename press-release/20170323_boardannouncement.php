<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Foundation Announces 2017 Board Member Election Results";
	$pageKeywords	= "eclipse, board, directors";
	$pageAuthor		= "Kat Hirsch";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>


<p>The Eclipse Foundation announced the results of the Sustaining Member and Committer elections for representatives to the Board of Directors.
Congratulations to Tracy Miranda, Torkild Resheim, and Gunnar Wagenknecht as new Sustaining representatives. Chris Aniszczyk and Ed Merks will be returning as Committer representatives, along with the newly elected Mickael Istria. We're looking forward to working with you on the Eclipse Foundation Board of Directors, effective April 1st. </p>

<p>The Eclipse Foundation would also like to recognize the contributions of Chris Holmes, Dani Megert,  and Matthias Zimmerman who are leaving the Eclipse Board. The Eclipse community owes them a great deal for their many hours of effort on the Eclipse Board.</p>

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://eclipse.org/org/elections/">Election Home</a></li>
				<li><a target="_blank" href="https://eclipse.org/org/elections/keydates.php">Key Dates</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

