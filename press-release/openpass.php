<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "New Proposal for OpenPASS Working Group";
	$pageKeywords	= "eclipse, OpenPass, Working Group, Automotive";
	$pageAuthor		= "Kat Hirsch";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

			<p>A proposal has been made to establish the <a href="https://www.eclipse.org/org/workinggroups/openpasswg_charter.php">Eclipse OpenPASS Working Group</a>. The goal of the Eclipse OpenPASS Working Group will be to support the development of advanced driver assistance systems, also referred to as autonomous driving systems, providing tools and adapters for standardized openly-available and vendor-neutral platforms for simulation of traffic scenarios. Volkswagen Group of America, Inc., BMW Group, Daimler AG, ITK Engineering AG and others have all expressed interest in participating in this collaborative working group.</p>

<p>The <a target="_blank" href="https://www.eclipse.org/org/workinggroups/openpasswg_charter.php">proposed charter</a> for the working group is now available for review. If you have comments or feedback on the proposed charter please send them to openpass-wg@eclipse.org.</p>

<p>All members of the Eclipse Foundation are invited to join and participate in this working group. If you are interested please send an e-mail to the mailing list, openpass-wg@eclipse.org. Based on the feedback and level of interest received, the working group will be officially created in July 2016.</p>
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://dev.eclipse.org/mailman/listinfo/openpass-wg">Eclipse OpenPASS Mailing List</a></li>
		    <li><a target="_blank" href="https://www.eclipse.org/org/workinggroups/openpasswg_charter.php">Eclipse OpenPASS Charter</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

