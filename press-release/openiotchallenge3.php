<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse IoT Announces Third Edition of the Open IoT Challenge";
  $pageKeywords = "eclipse, eclipsescience, internet of things, iot, challenge";
  $pageAuthor   = "Kat Hirsch";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    		<p><strong>Ottawa, Canada – October 31, 2016</strong> - The Eclipse Internet of Things (IoT) Working Group, hosted at the Eclipse Foundation, is pleased to announce the <a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge</a>. The third edition of this challenge encourages IoT enthusiasts and developers to build innovative solutions for the Internet of Things using open standards and open source technology. The previous edition of the Challenge resulted in a solution for remote patient monitoring in rural areas, a simulator of connected things and gateways, an IoT robot controller as well as a railway system, and other great solutions. This year’s participants will once again be supported with hardware development kits, receive technical support, and the winners will receive special prizes. This year the Challenge is <a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/sponsors#sponsors">sponsored</a> by bitreactive, Bosch Software Innovations, Eurotech, MicroEJ, and Red Hat.</p>

			<p>The <a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT Working Group</a> is a community of 26 IoT open source projects and over 28 participating companies. The goal of Eclipse IoT is to provide a set of open source IoT technology that is used by the IoT industry to build open, commercial and enterprise IoT solutions.</p>

			<p>The Open IoT Challenge encourages participants to build IoT solutions using any or all of the following technologies:</p>

    		<ul>
				<li>IoT open standards, such as MQTT, CoAP, Lightweight M2M, MQTT, OneM2M and OPC-UA.</li>
				<li>Eclipse IoT projects, such as Californium, Edje, Kura, Leshan, Milo, Mosquitto, Paho, SmartHome, Vorto and others listed <a target="_blank" href="http://iot.eclipse.org/projects">here</a>.</li>
				<li>Other open source technology, such as Cloud Foundry, OpenShift, Apache Spark or Apache Camel.</li>
				<li>Commercial hardware and services that interact with open source software and open standards for IoT.</li>
			</ul>
			
			<p>Participants are required to <a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/">submit a proposal</a> for their solution before November 25, 2016. A panel of judges will select a shortlist of participants that will receive hardware development kits. All participants will then have until the end of February to build their IoT solutions. The winners will be announced the beginning of March.</p>
			
			<p>Additional information about the Challenge is available at <a target="_blank" href="http://iot.eclipse.org/open-iot-challenge">http://iot.eclipse.org/open-iot-challenge</a>. Information about the the previous Open IoT Challenge, including the winners, is available <a target="_blank" href="https://www.eclipse.org/org/press-release/20160310_iotchallenge_winners2016.php">here</a>.</p> 

    		<a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/"><img class="img-responsive" src="/community/eclipse_newsletter/2016/october/images/iotchallenge.png" alt="open iot challenge"></a>
    		
  </div>

  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
    		<li><a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge 3.0</a></li>
    		<li><a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/second-edition/">Submit your Entry</a></li>
    		<li><a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a></li>
    		<li><a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/first-edition/">Challenge First Edition</a></li>
    		<li><a target="_blank" href="http://iot.eclipse.org/open-iot-challenge/second-edition/">Challenge Second Edition</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
