<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Register for the Eclipse Fall 2013 Training Series";
	$pageKeywords	= "eclipse, training, series, fall";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>



<p>The Eclipse member companies are pleased to announce the <a href="http://www.eclipse.org/community/training/classes.php">Fall 2013 Training Series</a>. These training classes are an excellent opportunity for software developers and architects to learn more about Eclipse Rich Client Platform (RCP), Eclipse4 RCP, Eclipse BIRT, Building OSGI/RCP applications with Maven/Tycho and Modeling Technologies. Eclipse experts will lead the sessions, providing practical experience through classroom instruction and hands-on labs. Virtual and on-site classes in <b>English</b>, <b>French</b>, <b>German</b>, <b>Dutch</b>, <b>Spanish</b>, and <b>Italian</b> have been scheduled in several countries from October 21 to November 30, 2013.</p>

<p>See the <a href="http://www.eclipse.org/community/training/classes.php">schedule</a> for a complete list of courses and course descriptions.</p>

<p>Eclipse members participating in the training series are <a target="_blank" href="http://www.actuate.com/home/">Actuate</a>, <a target="_blank" href="http://www.obeo.fr/">Obeo</a>, <a target="_blank" href="http://www.opcoach.com/">OPCoach</a>, <a target="_blank" href="http://eclipsesource.com/en/services/training/">EclipseSource</a>, <a target="_blank" href="http://industrial-tsi.com/">Industrial TSI</a>, and <a target="_blank" href="http://www.rcp-vision.com/?lang=en">RCP Vision</a>, and Committer Member Lars Vogel.</p>

<p>Take advantage of this excellent learning opportunity and register for an Eclipse training class today!</p>

	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://www.eclipse.org/community/training/classes.php">Schedule</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

