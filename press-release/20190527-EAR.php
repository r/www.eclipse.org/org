<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Statement by the Eclipse Foundation on Huawei Entity List Ruling";
$pageKeywords	= "eclipse, Eclipse Foundation, huawei, entry list ruling, EAR, open source, bulletin";
$pageAuthor		= "Mike Milinkovich";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
				
			<p><Restrictions on exports and reexports to parties named on Entity List specifically apply to activities and transactions subject to the Export Administration Regulation (EAR). [1] Open source publicly available encryption software source code, as classified by the US Department of Commerce, Bureau of Industry and Security (BIS) effective September 20, 2016, is "publicly available" and "published" and is not "subject to the EAR." [2]</p> 
			<p>Open Source projects involving encryption software source code are still required to send a notice of the URL to BIS and NSA to satisfy the "publicly available" notice requirement in EAR &sect; 742.15(b).</p>
			<p>The Eclipse Foundation provides regular updates to the relevant authorities to ensure that our project's notices are up to date and are maintained in the future.[3]</p>
			<p>Open source software, collaboration on open source development, attending open telephonic or in person meetings, and providing sponsorship funds are all activities that are not subject to the EAR and therefore should have no impact on our communities.</p>
			<p>For more information, visit our <a href="https://protect-us.mimecast.com/s/yOcwCQW57whxLLV8tMHlyf?domain=eclipse.org">Legal FAQ</a>, particularly the <a href="https://www.eclipse.org/legal/legalfaq.php#h.qu58e71q3dbc">section on cryptography</a>. </p>
			<p>References:</p>
			<ul>
				<li>[1] <a href="https://www.bis.doc.gov/index.php/documents/regulations-docs/2394-huawei-and-affiliates-entity-list-rule/file">https://www.bis.doc.gov/index.php/documents/regulations-docs/2394-huawei-and-affiliates-entity-list-rule/file</a></li>
				<li>[2] 81 Fed. Reg. 64656, 64668 (September 20, 2016). See also, <a href="https://www.bis.doc.gov/index.php/policy-guidance/encryption/223-new-encryption">https://www.bis.doc.gov/index.php/policy-guidance/encryption/223-new-encryption</a></li>
				<li>[3] <a href=" https://www.eclipse.org/legal/legalfaq.php#h.qu58e71q3dbc"> https://www.eclipse.org/legal/legalfaq.php#h.qu58e71q3dbc</a></li>
			</ul>
<h3>About the Eclipse Foundation</h3>
				
<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and commercially focused environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 350 open source projects, including runtimes, tools and frameworks for a wide range of technology domains such as IoT, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 275 corporate members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter @EclipseFdn, LinkedIn or visit eclipse.org.</p>
				
  	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);