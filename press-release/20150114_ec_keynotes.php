<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "EclipseCon 2015 Keynotes Announced";
	$pageKeywords	= "eclipse, eclipsecon, foss4g";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
		
			<p><b>Ottawa, Canada - January 14, 2015</b> - The Eclipse Foundation is pleased to announce the keynote speakers for the <a target="_blank" href="https://www.eclipsecon.org/na2015/">EclipseCon 2015</a> conference, planned for March 9-12 at the San Francisco Airport Hyatt Regency Hotel.</p> 
			<p>The three <a target="_blank" href="https://www.eclipsecon.org/na2015/keynotes">keynote speakers</a> will cover topics relevant to software development and future industry trends.</p> 
				<ul>
				<li><b>Denise Jacobs</b>, author and speaker about creativity, will explore options to create Infinite Possibilities that help people create a clear path to a future of meaningful work.</li>
				<li><b>Jesse Andrews</b>, VP Product Engineering at Planet Labs, will present the future of satellite technology, specifically very small satellites called cube satellites, and their ability to greatly increase the world’s access to satellite imagery and the valuable information derived from it.</li>
				<li><b>Mark Reinhold</b>, Chief Architect, Oracle Java Platform Group, is a key technical leader for the future Java 9 and Java 10 releases. In this keynote presentation, he will present the key features of the upcoming Java 9 release and offer insights on the future Java 10 release.</li>
				</ul>
			<p>EclipseCon 2015 is the annual meeting of the Eclipse North American community. The topics for this year’s conference include cloud development, developing with the Eclipse Platform, Eclipse modeling, and innovations with software development languages and tools. The conference also includes special <a target="_blank" href="https://www.eclipsecon.org/na2015/themedays">theme days</a> on the Internet of Things, C/C++ Development, Xtext, Science, and the PolarSys Working Group.</p>   
			<p>For the first time, EclipseCon will be co-located with <a target="_blank" href="https://2015.foss4g-na.org/">FOSS4G NA</a>, an event dedicated to open source geospatial technology. Attendees at EclipseCon will have full access to all of the FOSS4G sessions.</p> 
			<p>Registration for EclipseCon is now open. Attendees who <a target="_blank" href="https://www.eclipsecon.org/na2015/registration">register</a> by February 2, 2015 will receive a $500 discount off the full registration price. Companies are also invited to participate as EclipseCon sponsors. A detailed sponsorship prospectus is now available.</p> 
			<br>
			<h4>About the Eclipse Foundation</h5>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p> 
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="https://www.eclipse.org/home/index.php">www.eclipse.org</a>.</p> 
							
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipsecon.org/na2015/">EclipseCon 2015</a></li>
				<li><a href="https://www.eclipsecon.org/na2015/keynotes">Keynote Speakers</a></li>
				<li><a href="https://2015.foss4g-na.org/">FOSS4G NA</a></li>
				<li><a href="https://www.eclipsecon.org/na2015/themedays">Theme Days</a></li>
				<li><a href="https://www.eclipsecon.org/na2015/registration">Register Now</a></li>				
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

