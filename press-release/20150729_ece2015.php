<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Keynote Speakers Announced for the 10th EclipseCon Europe Conference";
	$pageKeywords	= "eclipse, eclipsecon, europe, 2015, germany";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
				
			
			<p><b>Ottawa, Canada - July 29, 2015</b> - The Eclipse Foundation is pleased to announce the keynote speakers for <a target="_blank" href="https://www.eclipsecon.org/europe2015/">EclipseCon Europe</a>. Planned for November 3-5, 2015, the 
				conference will take place at the Forum am Schlosspark in Ludwigsburg, Germany.</p> 

			<p>This year will mark the tenth consecutive year of this popular event dedicated to Eclipse software developers and its member companies. At every EclipseCon Europe hundreds of developers, researchers, and companies gather to learn, 
				network, and innovate on new software.</p>
			
			<p>The keynote addresses will be given by top researchers and industry leaders.</p>
			<ul>
				<li><b>Sabine Hauert</b>, lecturer from Bristol Robotics Laboratory, specializes in designing swarms of nanobots for biomedical applications. She will explore how swarm technology research is being used to revolutionize the 
				detection and treatment of cancer tumors.</li>
				<li><b>Michael Würtenberger</b>, managing director of BMW Car IT, will share thoughts about cars as mobile devices in the Internet of Things (IoT), and how the technological capabilities of a mature industry changes the way 
				we think about and develop new products and services.</li>
				<li><b>Stefan Ferber</b>, vice president for portfolio strategy at Bosch, will discuss Bosch’s IoT strategy and the importance of adopting open standards and open source technology from consortia like the OSGi Alliance and 
				Eclipse Foundation.</li>
			</ul>
			
			<p>EclipseCon Europe is a three day event co-located with the <a target="_blank" href="http://www.osgi.org/CommunityEvent2015/HomePage">OSGi Alliance Community Event 2015</a>. The topics for this year’s conference include Eclipse technology, introduction to the Eclipse ecosystem, OSGi, web technology and 
				cloud development, industry solutions, science applications, and more. In addition to the regular program, the conference also hosts two theme days:</p>
			<ul>
				<li><b>IoT Day</b> explores the exciting community that is building open source technology for the Internet of Things (IoT) industry</li>
				<li><b>Project Quality Day</b> focuses on the methods and tools that assure the quality of projects</li>
			</ul>
			
			<p>The <a target="_blank" href="https://www.eclipsecon.org/europe2015/cfp">call for papers</a> submission deadline is July 31. The program will be chosen by an independent program committee that is focused on selecting high quality sessions. The full program will be announced in August.</p>
			
			<p>Registration is now open and sponsorship opportunities are available. For more information visit <a target="_blank" href="https://www.eclipsecon.org/europe2015/">www.eclipsecon.org</a>.</p> 
				
			<h3>About the Eclipse Foundation</h3>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, 
				tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, 
				innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse 
				and the Eclipse Foundation are available at <a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
						
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipsecon.org/europe2015/">EclipseCon Europe</a></li>
				<li><a target="_blank" href="http://www.osgi.org/CommunityEvent2015/HomePage">OSGi Alliance Community Event 2015</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/europe2015/keynotes">Keynote Speakers</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/europe2015/cfp">Call for papers</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

