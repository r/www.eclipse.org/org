<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Eclipse Foundation Announces 2018 Board Member Election Results";
$pageKeywords	= "eclipse, board members, election";
$pageAuthor		= "Mike Milinkovich";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
		# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p><strong>Ottawa, Canada - March  07, 2018</strong>- Today we are please to announce the results of the Eclipse Foundation Sustaining Member and Committer Member elections for representatives to our Board of Directors. Congratulations to Tracy Miranda, and Torkild Ulvoy Resheim  who were returned to the Board by acclamation as Sustaining representatives. Dani Megert and Ed Merks will be returning as Committer representatives. We're looking forward to working with you on the Eclipse Foundation Board of Directors, effective April 1st.</p>
<p>The Eclipse Foundation would also like to recognize the contributions of Chris Anisczcyk, Mickael Istria, and Gunnar Wagenknecht who are leaving the Eclipse Board. The Eclipse community owes them a great deal for their many hours of effort on the Eclipse Board.</p>

<h3>About the Eclipse Foundation</h3>

<p>The Eclipse Foundation is a not-for-profit organization that supports a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects with the Eclipse Foundation, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. Eclipse Foundation Working Groups allow organizations to collaborate on building innovative new technology to meet specific industry needs.  More information is available at eclipse.org.</p>

  </div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

