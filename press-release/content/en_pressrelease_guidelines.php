<?php

/**
 * Copyright (c) 2005, 2018, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Denis Roy (Eclipse Foundation) - Initial implementation
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <style>
    h3 {
      line-height: 1.3em
    }
  </style>
  <h1><?php print $pageTitle; ?></h1>
  <ul>
    <li><a href="#introduction">Introduction</a></li>
    <li><a href="#goals">Goals of the Guidelines</a></li>
    <li><a href="#press-release-triggers">Events That May Trigger a Press
        Release</a></li>
    <li><a href="#requesting-a-press-release">Requesting a Press Release</a></li>
    <li><a href="#general-guidelines">General Guidelines for Press
        Releases Issued by the Eclipse Foundation</a></li>
  </ul>
  <h2 id="introduction">Introduction</h2>
  <p>The Eclipse Foundation has been very successful in developing strong
    press relationships resulting in community-related news being
    frequently reported. As the number of projects, working groups, and
    members grow, media interest in the Eclipse ecosystem is also growing.
    Therefore, to maintain our strong relationship with the news media and
    to support the public relations efforts of our member organizations
    and projects, we have developed a set of guidelines on the issuance of
    press releases from and involving the Eclipse Foundation.</p>
  <p>
    Please refer to the following guidelines when preparing a press
    release that incorporates Eclipse Foundation names or trademarks or
    involves the Eclipse Foundation in any way. For press release
    approval, or further questions, please contact the Foundation&rsquo;s
    marketing team at <a href="mailto:pr@eclipse-foundation.org">pr@eclipse-foundation.org</a>.
  </p>
  <h2 id="goals">Goals of the Guidelines</h2>
  <ul>
    <li>Maintain the positive image of the Eclipse Foundation with the
      journalists, editors, influencers, and publications that cover the
      Foundation.</li>
    <li>Ensure the Eclipse Foundation is viewed positively in the greater
      software industry.</li>
    <li>Support member&rsquo;s desires to publicize their involvement with the
      Eclipse Foundation and their participation in the Eclipse community.</li>
    <li>Ensure a level of consistency and professionalism regarding public
      relations efforts around the Eclipse Foundation.</li>
    <li>Maintain a level of momentum around the Eclipse Foundation through
      regular news updates that demonstrates the activity and vitality of
      the Eclipse community.</li>
  </ul>
  <h2 id="press-release-triggers">Events That May Trigger a Press Release</h2>
  <p>There are different events that may trigger a press release related
    to the Eclipse Foundation. This section provides the guidelines of
    what the Foundation involvement may be in those press releases.</p>
  <h3>Company Joins the Eclipse Foundation as a Member</h3>
  <p>When a company joins the Eclipse Foundation, the company is
    encouraged to issue their own press release announcing their
    membership in the Foundation. The Eclipse Foundation will support this
    press release by providing a supporting quote. The actual release is
    issued by the member company. The Foundation will link to the new
    member press release from the eclipse.org website.</p>
  <h3>Company Joins the Eclipse Foundation as a Strategic Member</h3>
  <p>When a company joins the Eclipse Foundation as a Strategic Member
    and hence becomes a member of the Board of Directors, the Eclipse
    Foundation would like to do a joint press release with the company.
    The decision to do a joint release is with the member company. If a
    joint release is desired, the Eclipse Foundation will participate in
    the press outreach activities. If the company would like to issue
    their own release, the Eclipse Foundation would be willing to provide
    a supporting quote. The Foundation will link to the new member press
    release from the eclipse.org website.</p>
  <h3>Company Releases a Commercial Product Based on/Integrated with
    Eclipse-Governed Technologies or a Product Certified Compatible with
    an Eclipse Foundation Specification</h3>
  <p>If a member company issues a press release about a product that is
    based on or is integrated with Eclipse-governed technologies or is
    certified compatible with an Eclipse Foundation specification, the
    Eclipse Foundation will support this press release by providing a
    supporting quote. The actual release is issued by the member company.
    The Foundation will link to the new product press release from the
    eclipse.org website.</p>
  <h3>Company Proposes a New Eclipse Project</h3>
  <p>When a company proposes a new Eclipse project, they may wish to
    issue a press release. The Eclipse Foundation will support this press
    release with a supporting quote. It is important that the press
    release positions the news of the project as a &lsquo;project proposal&rsquo;.
    This is to respect the Eclipse development process that requires a
    30-day period of feedback before a project can be considered for
    creation. The Eclipse Foundation will link to the new member press
    release from the eclipse.org website.</p>
  <h3>New Project is Created at or Transitioned to the Eclipse Foundation</h3>
  <p>When a new project is created, or an incubator project is graduated,
    the Eclipse Foundation and the company that initially proposed the
    project may decide to issue a press release. This would be a joint
    release between the company and the Eclipse Foundation. The main
    message of the release needs to reflect the Eclipse
    Foundation-specific news and should not contain a strong corporate
    message. If appropriate, the Eclipse Foundation will participate in
    the press outreach activities. The press release would be posted on
    the eclipse.org web site.</p>
  <h3>New Working Group is Created at or Transitioned to the Eclipse
    Foundation</h3>
  <p>When a new working group is created, the Eclipse Foundation may
    decide to issue a press release in collaboration with the working
    group. The messaging of the release needs to reflect the working
    group&rsquo;s vision, scope, priorities, and related Eclipse projects. If
    appropriate, the Eclipse Foundation and media spokespersons for the
    founding member organizations would participate in the press outreach
    activities. The press release would be posted on the eclipse.org
    website.</p>
  <h3>Project Oriented Press Releases</h3>
  <p>
    Press releases to announce news about a project, (e.g. a new version or
    significant milestone such as industry adoption news) are issued by the Eclipse
    Foundation. All projects are invited to propose potential press releases. The
    Project/PMC Lead is required to approve any project-specific press release and
    act as a spokesperson for the release. To encourage a vendor neutral community,
    companies are encouraged to not issue press releases regarding a specific
    Eclipse project.
  </p>
  <p>The Eclipse Foundation will not issue press releases announcing new
    versions of a technology or incubation projects. In general,
    technology and incubation projects are not intended for production
    use; therefore the promotion of these projects should be focused
    within the existing Eclipse community. New releases of technology and
    incubation projects can be promoted via the elipse.org website and
    forums.</p>
  <h3>Foundation News Releases</h3>
  <p>The Eclipse Foundation may from time to time issue press releases to
    support certain events or strategies. For example, press releases
    announcing EclipseCon, participation in a trade show or momentum in
    the Eclipse community.</p>


  <h2 id="requesting-a-press-release">Requesting a Press Release</h2>
  <p>
    Projects, working groups, and member companies can request the
    issuance of a press release by completing the Eclipse Foundation <a href="https://forms.gle/Lcpv2L212cdhanfn7" target="_blank">Press
      Release Request Form</a>. Please use this form to submit requests for
    the Eclipse Foundation to produce press releases, announcements,
    blogs, quotes, or other publicity about your Eclipse community-related
    news. Once submitted, a member of the Foundation&rsquo;s Marketing team will
    be in touch to confirm your request and take the next steps.
  </p>
  <h2 id="general-guidelines">General Guidelines for Press Releases
    Issued by the Eclipse Foundation</h2>
  <ul class="margin-bottom-50">
    <li>In general, the Eclipse Foundation tries not to overstate the
      capabilities of our projects and avoid obvious hyperbole-filled
      statements. Ours is an open source community that has a strong
      developer focus. Over-hyped statements are generally viewed
      negatively by the community.</li>
    <li>Usage of Eclipse Foundation names, marks and logos must adhere to
      the <a href="/legal/logo_guidelines.php">Eclipse Foundation Trademark Usage Guidelines</a>.</li>
    <li>The Eclipse Foundation avoids any statements that may be viewed as
      competitive to a member company.</li>
    <li>A company leading an Eclipse Foundation open source project should
      not claim ownership of an open source project. For instance
      statements such as <em>ABC Company&rsquo;s Eclipse &lt;favourite
        project&gt; has announced…</em> should be avoided. The Eclipse
      Foundation is a vendor-neutral open source community, so ownership
      claims go against this philosophy. Reinforcing a company&rsquo;s leadership
      in a project via a supporting quote is fine.
    </li>
    <li>Whenever possible, a press release should promote the fact that
      the Eclipse project is being adopted by a wide variety of commercial
      products and stress the open source nature of the project. This is to
      reinforce the message that Eclipse projects and technologies are
      adopted broadly for building commercial products.</li>
    <li>Press releases announcing future capabilities or new project
      versions that will be available more than 30 days after the press
      release should be avoided. The philosophy is to only promote what is
      available to the community today.</li>
    <li>
        Mike Milinkovich, Executive Director of the Eclipse Foundation, serves
        as the primary spokesperson and is responsible for all quotes
        attributed to the Eclipse Foundation in press communications. In
        circumstances when the Executive Director is not available, or with the
        prior approval of the Executive Director, alternative spokespersons may
        address the press. This approach ensures consistent messaging and
        upholds our commitment to transparent communication. Alternative
        spokespeople for the Eclipse Foundation include Thabang Mashologu, Vice
        President of Marketing and Community Programs; Gaël Blondelle, Chief
        Membership Officer; Michael Plagge, VP of Ecosystem Development; and
        Mikael Barbero, Head of Security.
    </li>
    <li>
      For project-specific press releases, the Project/PMC Lead may act as
      the spokesperson and provide quotes attributed in the press releases.
    </li>
    <li>All press releases from the Eclipse Foundation must be approved by
      the VP of Marketing and Community Programs and the Executive
      Director of the Eclipse Foundation.</li>
    <li>
      <p>The &lsquo;boilerplate&rsquo; description of the Eclipse Foundation to be used
        in press releases is as follows:</p>
      <blockquote>
        <p>
          <strong>About the Eclipse Foundation</strong>
        </p>
        <p>
          <em>
            The Eclipse Foundation provides our global community of individuals and organisations with a business-friendly environment for
            open source software collaboration and innovation. We host the <a href="https://eclipseide.org/">Eclipse IDE</a>, <a href="https://adoptium.net/">Adoptium</a>,
            <a href="https://sdv.eclipse.org/">Software Defined Vehicle</a>, <a href="https://jakarta.ee/">Jakarta EE</a>, and over 410+ open source
            <a href="https://www.eclipse.org/projects/">projects</a>, including runtimes, tools, specifications, and frameworks for cloud and edge applications,
            IoT, AI, automotive, systems engineering, open processor designs, and many others. Headquartered in Brussels, Belgium, the Eclipse Foundation is an international
            non-profit association supported by over 360 <a href="https://www.eclipse.org/membership/">members</a>. To learn more, follow us on social media
            <a href="https://x.com/EclipseFdn">@eclipsefdn</a>, <a href="https://www.linkedin.com/company/eclipse-foundation/">LinkedIn</a> or visit <a href="https://www.eclipse.org/">eclipse.org</a>.
          </em>
        </p>
      </blockquote>
    </li>
    <li>It is expected that as the Eclipse community grows and matures,
      what is considered newsworthy about Eclipse Foundation will evolve.
      Therefore, these guidelines will evolve and be updated from time to
      time. It is also expected that certain events may require the Eclipse
      Foundation to issue a press release that is not covered by or
      contradict these guidelines. In that case, the Executive Director of
      the Eclipse Foundation will use his discretion and best judgment for
      issuing the press release.</li>
  </ul>
</div>
