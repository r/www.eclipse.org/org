<?php
/**
 * Copyright (c) 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<h1><?php print $pageTitle; ?></h1>
<p><em>Expanded polyglot capabilities based on Language Server Protocol (LSP) plugins among major milestones in Eclipse Foundation’s thirteenth annual release train</em></p>

<p>OTTAWA, June 27, 2018 (GLOBE NEWSWIRE) -- The Eclipse Foundation, the platform for open collaboration and innovation,
today announced the Photon release of the Eclipse IDE. This marks the thirteenth annual simultaneous
release of Eclipse projects. There are 85 projects in the Eclipse Photon simultaneous release,
consisting of over 73 million lines of code, with contribution by 620 developers, 246 of
whom are Eclipse committers. As one of the world’s most popular desktop development
environments, the Eclipse IDE is downloaded over 2 million times per month, and is
the critical development environment for more than 4 million active users.</p>

<p>The simultaneous release delivers native Eclipse IDE experiences for Rust and C# through Language Server based plugins.
The Language Server Protocol (LSP) ecosystem delivers editing support for popular and emerging programming languages.
Combined with the move to a quarterly rolling release cadence, the LSP focus demonstrates a commitment to
keeping pace with evolving developer and commercial needs.</p>

<p>“With new language adoption happening at a faster rate, LSP based plugins will take the Eclipse IDE’s proven extensibility to the next level,”
said Mike Milinkovich, Executive Director of the Eclipse Foundation.
“Congratulations to all of the committers, projects, and Foundation staff that made the Eclipse Photon simultaneous release possible.”</p>

<p>Key Highlights of the Eclipse Photon Release:</p>

<ul>
<li>Support for building, debugging, running and packaging Rust applications with full Eclipse IDE user experience.</li>
<li>C# editing and debug capabilities, including syntax coloring, autocomplete suggestions, code diagnostics, and code navigation tools.</li>
<li>Support for building Java™ 10 and Java EE™ 8 based applications out of the box.</li>
<li>Dark theme improvements in text colors, background color, popup dialogs, mark occurrences, and more.</li>
<li>For more information on the release’s features and projects, explore the <a href="https://www.eclipse.org/photon/noteworthy/"></a>Eclipse IDE, Photon Edition New and Noteworthy</a> summary.</li>
</ul>

<p>To promote the Eclipse Photon release, the Eclipse Foundation has also produced an expert-led webinar series on projects
within the simultaneous release.
For more information please visit <a href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/events/">https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/events/</a>.</p>

<p>The Eclipse Photon release is available for download today. More information and downloads are available at <a href="http://www.eclipse.org/photon">http://www.eclipse.org/photon</a>.</p>
<br/>
<p><strong>About The Eclipse Foundation</strong><br/>
The Eclipse Foundation provides a global community of individuals and organizations with a mature, scalable and
commercially-focused environment for open source software collaboration and innovation.
 The Foundation is home to the Eclipse IDE, Jakarta EE and over 350
 open source projects, including runtimes, tools and frameworks for
 a wide range of technology domains such as IoT, automotive, geospatial,
 systems engineering and many others. The Eclipse Foundation is a not-for-profit
 organization supported by over 275 corporate members, including industry
 leaders who value open source as a key enabler for business strategy.
 To learn more, follow us on <a href="https://twitter.com/EclipseFdn">Twitter @EclipseFdn</a>, <a href="https://www.linkedin.com/company/eclipse-foundation">LinkedIn</a> or visit <a href="https://www.eclipse.org/">eclipse.org</a>.</p>

<p>Editorial Contact<br/>
Lonn Johnston<br/>
+1 650.219.7764<br/>
lonn@flak42.com</p>