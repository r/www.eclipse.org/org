<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Potential adware plugin on Eclipse Marketplace - Eclipse Class Decompiler";
	$pageKeywords	= "eclipse, eclipse marketplace";
	$pageAuthor		= "Kat Hirsch";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p>It has been brought to the attention of the Eclipse Foundation that a listing on Eclipse Marketplace, called Eclipse Class Decompiler, contains binary code that could be used for adware and could potentially download files to Eclipse workspaces.
The listing has been removed from Eclipse Marketplace and the Eclipse Foundation strongly suggests any Eclipse user to uninstall this plugin from their workspace.</p>

<p>The initial report demonstrating how this plugin could be used for adware was reported in a <a target="_blank" href="https://0x10f8.wordpress.com/2017/08/07/reverse-engineering-an-eclipse-plugin/">blog</a> by a community member.
The findings of this blog post have been confirmed by Eclipse Foundation staff.
It is clear that this plugin includes code that is not appropriate behavior for the Eclipse users and community.</p>

<p>To remove this plugin, please do the following:</p>

<ol>
  <li>Go to Help > About Eclipse > Installation Details</li>
  <li>Select the feature(s) belonging to the entry</li>
  <li>Press "Uninstall"</li>
  <li>Follow the wizard </li>
</ol>

<p>If you have any questions about this bulletin, please send an email to <a href="mailto:emo@eclipse-foundation.org.">EMO</a>.</p>

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://marketplace.eclipse.org/">Eclipse Marketplace</a></li>
				<li><a target="_blank" href="https://eclipse.org">Eclipse Homepage</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

