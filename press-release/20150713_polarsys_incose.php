<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "PolarSys Open Source Community Participates in the 25th INCOSE Symposium";
	$pageKeywords	= "eclipse, eclipsecon, polarsys, incose";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
				
			
			<p><i>Goal is to demonstrate and promote use of open source solutions for systems engineering.</i></p>

			<p><b>Ottawa, Canada – July 13, 2015</b> – The PolarSys open source community, an Eclipse Foundation Working Group, focused on creating open source 
				solutions for embedded development and systems engineering will be exhibiting and speaking at the upcoming INCOSE Symposium in Seattle, WA. 
				PolarSys and member companies will be showcasing selected open source solutions that target systems engineering. The full range of PolarSys 
				solutions includes systems and software engineering, software development, and verification and validation. INCOSE is the annual conference 
				of the International Council of Systems Engineering.</p>
				
			<p>PolarSys is a collaboration among some of the world’s largest systems engineering companies, including Airbus, Ericsson, CEA List, and Thales. 
				This group is dedicated to creating an environment of open innovation that will create better tools and methods for systems engineering.</p> 
			<p>The objective of PolarSys’s participation at INCOSE is to educate and encourage other organizations to use open source solutions for systems 
				engineering. Experts on open source and systems engineering tools will be on hand to discuss opportunities for using PolarSys solutions on 
				large, industrial projects.</p> 
			<p>At the PolarSys exhibit at INCOSE (booth B6-B7), several PolarSys members, including CEA List, Obeo, Thales, and Zeligsoft, will 
				showcase the following PolarSys systems engineering solutions:</p>
				<ul>	
					<li><a target="_blank" href="http://www.polarsys.org/capella/">Capella</a> is a graphical modeling workbench for model-based systems engineering (MBSE) providing rich methodological guidance based 
				on the Arcadia method for architectural design. Capella is extensible through specialty engineering viewpoints. It provides a wide spectrum 
				of integrated tools improving productivity, ensuring scalability and enforcing model consistency.</li>
					<li><a target="_blank" href="https://eclipse.org/papyrus/">Papyrus</a> is the PolarSys solution for SysML and UML modeling that features extension and customization capabilities for full 
				integration with the development lifecyle. Papyrus demos will cover Papyrus SysML and Papyrus for Real Time, a complete software modeling 
				and development environment for cyber-physical systems.</li>
				</ul>
			<p>A number of individuals involved in the PolarSys community will be speaking at INCOSE, including:</p>
				<ul>	
					<li>Jean-Luc Voirin, Stephane Bonnet, Veronique Normand, Daniel Exertier (Thales) speaking about ‘From initial investigations up 
				to large-scale rollout of an MBSE method and its supporting workbench: the Thales experience’</li>
					<li>Sebastien Gerard, Nataliya Yakymets (CEA List) speaking about ‘Enabling System and Safety Co-Engineering based on standards.’</li>
					<li>Stephane Bonnet, Jean-Luc Voirin, Veronique Normand, Daniel Exertier (Thales) speaking about ‘Implementing the MBSE Cultural Change: 
				Organization, Coaching and Lessons Learned’</li>
				</ul>
				<p>More information about PolarSys is available at <a target="_blank" href="https://www.polarsys.org/">https://www.polarsys.org/</a>. 
				
			<h3>About the Eclipse Foundation</h3>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, 
				tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, 
				innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse 
				and the Eclipse Foundation are available at <a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
						
				
			<h3>Contact Information</h3>
			<p>Ian Skerrett<br>
			Eclipse Foundation<br>
			Tel: 613-224-9461<br>
			Email: ian.skerrett@eclipse.org</p>
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.polarsys.org/">Polarsys</a></li>
				<li><a target="_blank" href="http://www.polarsys.org/capella/">Capella</a></li>
				<li><a target="_blank" href="https://eclipse.org/papyrus/">Papyrus</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

