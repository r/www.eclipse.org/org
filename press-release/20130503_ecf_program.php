<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "EclipseCon France Program and Keynotes Announced";
	$pageKeywords	= "eclipsecon, france, program, keynotes";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
				<p><strong>Ottawa, Canada - May 6, 2013</strong> - The Eclipse Foundation is pleased to announce the first <a href="http://eclipsecon.org/france2013/">EclipseCon France</a>. 
				After successful Eclipse Day events in Toulouse, and thanks to a strong and vibrant Eclipse community in France, the Foundation has added this new event to the EclipseCon family. 
				Planned for June 5-6 in Toulouse, this technical conference focuses on the latest innovations in both the Eclipse community and the software industry.</p>

				<p>The EclipseCon France <a href="http://eclipsecon.org/france2013/program/session-schedule">program</a> has been announced. 
				It includes 35 talks and workshops over two days, and features sessions on</p>
					<ul>
						<li>Eclipse 4</li>
						<li>Orion</li>
						<li>OSGi</li>
						<li>M2M Applications</li>
						<li>Eclipse Industry Working Groups like Polarsys and Long Term Support</li>
						<li>And much more!</li>
					</ul>
				<h4>Keynote Speakers</h4>
				<p>We are also pleased to announce the keynote speakers:</p>
					<ul>
						<li>Dominique Sciamma, Strate Collège Designers, on Design : Making the World Simple, Fair, and Beautiful</li>
						<li>Hans-Jürgen Kugler, KUGLER MAAG CIE GmbH, on The Humanist Perspective of Industry 4.0</li>
					</ul>
				<p>Registration is <a href="http://eclipsecon.org/france2013/registration">now open</a>. Register now and join us in Toulouse.</p>
				
	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://www.eclipsecon.org/france2013/keynotes">Keynotes</a></li>
				<li><a href="http://eclipsecon.org/france2013/registration">Registration Page</a></li>			
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

