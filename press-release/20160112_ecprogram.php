<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "EclipseCon 2016 Keynotes and Program Announced";
  $pageKeywords = "eclipse, eclipsecon, virginia, reston, march, iot";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  <p><b>Ottawa, Canada - January 12, 2016</b> - The Eclipse Foundation is pleased to announce the <a href="https://www.eclipsecon.org/na2016/keynotes">keynote speakers</a>
  		and the program for the EclipseCon 2016 conference, planned for March  7-10 at the Hyatt Regency in Reston, Virginia. The three keynote speakers will cover 
  		topics relevant to software development and future industry trends.</p> 
  		
  		<ul>
			<li><b>Tyler Jewell</b>, President and CEO of Codenvy Inc., will lead off the conference by addressing the evolution of IDEs and presenting his vision of 
  		future development environments. Tyler will demonstrate a next generation Eclipse IDE, and show how major ecosystem vendors are establishing this vision.</li>   
			<li><b>Chris Rasmussen</b>, a GeoINT Pathfinder PM and Public OSS Lead at the National Geospacial-Intelligence Agency (NGA), will speak about the major transition 
  		that federal organizations such as his have undergone with respect to open source, and will provide insights into what this change in strategy means for 
  		those interacting with them.</li> 
			<li><b>Samantha Chiu</b>, a member of Women Who Code, will describe the unique challenges and significant positive outcomes that she and her colleagues experienced 
  		in working to introduce programming skills to women living in austere environments, including her work in Afghanistan and Ghana.</li> 
  		</ul>
  		
  	<p>EclipseCon is the annual North American gathering of the Eclipse community. The conference <a href="https://www.eclipsecon.org/na2016/conference/schedule/session/2016-03-07">program</a> 
  		has been published and topics include Eclipse Platform/RCP/Runtimes, Languages and Tools, Web/Mobile/Cloud Development, Modeling, Embedded, Science, Java 9, IDEs and more.</p> 

  	<p>This year, EclipseCon is also co-located with the <a href="https://www.eclipsecon.org/na2016/iot-summit">IoT Summit</a>, hosted by the Eclipse Internet of Things (IoT) Working Group 
  		on March 8-9. Eclipse IoT is the leading open source community building Internet of Things protocols, runtimes, and device frameworks. It’s a great chance to learn about new 
  		key initiatives from the Eclipse IoT community and about Eclipse IoT projects, as well as other open source projects used to implement IoT solutions.</p>
  		
	<p><a href="https://www.eclipsecon.org/na2016/registration">Registration</a> for EclipseCon is now open. Attendees who register by February 1, 2016 will receive a $250 discount 
  		off the full registration price. Companies are also invited to participate as EclipseCon NA sponsors. A detailed 
  		<a href="https://www.eclipsecon.org/na2016/prospectus">sponsorship prospectus</a> is available.</p>

  	<p>“EclipseCon 2016 attendees will enjoy a program that provides a rich exposure to technologies from the Eclipse community used by millions of developers around the world. 
  		It will be an opportunity to learn and share ideas with the leaders of our community,” said Mike Milinkovich, Eclipse Foundation’s Executive Director.  
  		“In particular, the keynotes scheduled for this year will address areas of importance to our broad community, and to challenge the status quo.”</p>
  		
  	<h3>About the Eclipse Foundation</h3>
	<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, 
  		deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and 
  		individuals extend, complement, and support the Eclipse Platform.</p>

  	<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at 
  		<a href="http://www.eclipse.org/">www.eclipse.org</a>.</p>
  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipsecon.org/na2016">EclipseCon 2016</a></li>	
  				<li><a href="https://www.eclipsecon.org/na2016/keynotes">Kenote Speakers</a></li>
  				<li><a href="https://www.eclipsecon.org/na2016/conference/schedule/session/2016-03-07">Program</a></li>
  				<li><a href="https://www.eclipsecon.org/na2016/iot-summit">IoT Summit</a></li>
  				<li><a href="https://www.eclipsecon.org/na2016/registration">Register Now</a></li>
  				<li><a href="https://www.eclipsecon.org/na2016/prospectus">Sponsorship</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
