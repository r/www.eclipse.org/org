<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include ("_projectCommon.php"); // All on the same line to unclutter the user's
// desktop'

//
// Begin: page-specific settings. Change these.
$pageTitle = "The Eclipse Foundation Releases IoT Commercial Adoption Survey Results";
$pageKeywords = "eclipse, Eclipse Foundation";
$pageAuthor = "Eclipse Foundation";

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p><em>&ndash; Leading open source community finds IoT adoption real and growing with results detailing spend, target markets, and relevant applications&nbsp; &ndash; </em></p>

<p><strong>OTTAWA &ndash; MARCH 10, 2020</strong> &ndash; The <a href="http://www.eclipse.org">Eclipse Foundation</a>, one of the world&rsquo;s largest open source foundations focused on the Internet of Things (IoT), today announced the release of its first annual IoT Commercial Adoption survey. One of the first of its kind, this survey&rsquo;s objective was to gain a better understanding of the IoT industry landscape by identifying the requirements, priorities, and challenges faced by organizations that are deploying and using commercial IoT solutions, including those based on open source technologies. This survey is distinct and separate from the IoT Developer Survey, the industry&rsquo;s most influential survey from the development front lines, which the Eclipse Foundation has conducted for the last six years.</p>

<p>&ldquo;The Internet of Things is clearly one of the major technology trends today and a ubiquitous buzzword,&rdquo; said Mike Milinkovich, executive director of the Eclipse Foundation. &ldquo;This survey, which we hope will be the first of an annual tradition, seeks to provide real insights into what organizations are doing with the IoT right now and their plans for production deployments.&rdquo;</p>

<p>From October 7 to December 2, 2019, 366 individuals from a broad set of industries and organizations participated in an online survey. Five of the top conclusions drawn from the survey data include:</p>

<ul>
	<li>IoT is real and adoption is growing, if slower than the hype would indicate. Just under 40% are deploying IoT solutions today and another 22% plan to start deploying IoT within the next 2 years.</li>
	<li>Caution rules in the IoT market, with 30% of organizations planning to spend less than $100K in the next year.&nbsp;</li>
	<li>IoT investment is on the rise, with 40% of organizations planning to increase their IoT spending in the next fiscal year.&nbsp;</li>
	<li>Open source pervades IoT with 60% of companies factoring open source into their IoT deployment plans.</li>
	<li>Hybrid clouds lead the way for IoT deployments. Overall, AWS, Azure, and GCP are the leading cloud platforms for IoT implementations.&nbsp;</li>
</ul>

<p>The survey data also includes details on IoT adoption by industry, top concerns by commercial IoT adopters, and breakdowns by organizational role. To find out more, interested parties can download the entire report and its ground-breaking insights <a href="http://bit.ly/2019IoTsurvey">here</a>.</p>

<p>The Eclipse Foundation has a proven track record of enabling developer-focused open source software collaboration and innovation earned over more than 15 years. The Foundation&rsquo;s IoT community alone represents one of the largest open source collaborations in the world spanning 43 projects from more than 40 members and over 4 million lines of code produced. Eclipse IoT projects have been adopted by world-leading companies across verticals to deliver commercial IoT solutions and services.</p>

<p>To get involved with the Eclipse IoT Working Group and contribute to Eclipse IoT projects, please visit <a href="https://iot.eclipse.org/">https://iot.eclipse.org/</a>. As an added benefit of membership, Eclipse IoT Working Group members receive exclusive access to detailed industry research findings, including the annual IoT Developer Survey, the findings from which are leveraged by the entire industry.</p>

<p>The 2020 IoT Developer Survey is coming soon. If you are interested in contributing questions or providing feedback, join the working group mailing list <a href="https://accounts.eclipse.org/mailing-list/iot-wg">here</a>.</p>

<h2>Quotes from Eclipse IoT Working Group Members</h2>

<h3>Bosch</h3>

<p>&ldquo;This new survey aligns with our own insights into the IoT industry and how organizations are embracing open IoT platforms and commercial offerings based on open source,&rdquo; said Anita Bunk, Bosch.IO&rsquo;s open source advocate and head of Marketing, Associate and Technical Communications. &ldquo;We are seeing growing interest in our Bosch IoT Suite offering that is built upon Eclipse IoT open source. Our customers benefit from the open and transparent development that accelerates their ability to deliver real business outcomes.&rdquo;</p>

<h3>Eurotech&nbsp;</h3>

<p>&ldquo;The insights provided by this survey are exactly the kind of information vendors, solution providers, and other ecosystem stakeholders need to plan their next steps in this burgeoning market,&rdquo; said Giuseppe Surace, Chief Product and Marketing Officer for Eurotech. &ldquo;Based on open source, our Everyware IoT portfolio of building blocks, including our Everyware Software framework ESF and Everyware Cloud EC, addresses needs from the edge to the cloud, so any market intelligence on how companies are looking to consume end-to-end IoT solutions is extremely useful.&rdquo;</p>

<h3>Red Hat&nbsp;</h3>

<p>&ldquo;This survey is one of the first to truly tap into what industry leaders are actually doing about IoT right now,&rdquo; said Deborah Bryant, senior director, Open Source Program Office for Red Hat. &ldquo;The survey results highlight the important role of open source software in helping companies achieve their goals. This should be a wake-up call for any organization that has yet to evaluate solutions based on open standards and open source technologies as part of their IoT plans.&rdquo;</p>

<h2>About the Eclipse Foundation</h2>

<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and commercially focused environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 375 open source projects, including runtimes, tools, and frameworks for a wide range of technology domains such as IoT, edge computing, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 275 members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter <a href="https://www.globenewswire.com/Tracker?data=r-dKnkzEx8J2jm68aMwsXND3DPYOl6p60tYwoWOkQRpJjLP_SKHpU_opBz5bi7JLO7Y2btHRJBvdyqqcTBtrLg==">@EclipseFdn</a>, <a href="https://www.globenewswire.com/Tracker?data=S12nqbeRNKuTWwBUeNUnEC9gFrlX34gxCEhP2Uu-2KuWYmqkXbIScweQ1ykqwa2QtAHlvDYJT8GyKpicKaq5uLxRrTZwh2HI7nQfCd01lmEj3UXrlRWmU3VLtBxzjMlq">LinkedIn</a>, or visit <a href="https://www.globenewswire.com/Tracker?data=EsgUi61-f7vRM-pEF4XYjO2jYG6DqnLbsSPKrHZwkTiaAkCbjz8HlE68IUVSML_CiBt9S9b00GJWRIs3EmTELQ==">eclipse.org</a>.</p>

<p>Third-party trademarks mentioned are the property of their respective owners.</p>

<h2>Media Contact:</h2>

<p>Nichols Communications for the Eclipse Foundation, Inc.<br />
Jay Nichols<br />
jay@nicholscomm.com<br />
+1 408-772-1551</p>


  	</div>
</div>

EOHTML;

// Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);