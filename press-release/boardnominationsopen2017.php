<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "2017 Elections: Nominations Open";
	$pageKeywords	= "eclipse, board, elections";
	$pageAuthor		= "Kat Hirsch";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

			<p>Nominations for the 2017 Eclipse Foundation <a target="_blank" href="https://www.eclipse.org/org/foundation/directors.php">Board</a> <a target="_blank" href="https://www.eclipse.org/org/elections/">Election</a> is now open for Committer and Sustaining Member representatives.
		    The terms of office are from April 1, 2017 to March 31, 2018.</p>

		    <p>The positions of Sustaining Members and Committer representatives are a vitally important part of the Eclipse Foundation's governance. We encourage everyone to consider participating.</p>

		    <p>To nominate an individual, please send an email to elections@eclipse.org. Self-nominations are accepted.</p>

		   <p><b>Key Dates:</p></b>
		    <ul><li> January 10, 2017: Nominations open. To nominate someone, simply send an email to elections@eclipse.org.</li>
		    <li> January 31, 2017: Nominations close</li>
		    <li> February 3, 2017: Deadline for nominees to return their content to elections@eclipse.org</li>
		    <li> February 7, 2017: List of nominees published on www.eclipse.org</li>
		    <li> February 8, 2017: Individual pages ready for candidate review</li>
		    <li> February 13, 2017: Nominees' personal pages made available on www.eclipse.org</li>
		    <li> February 21, 2017: Voting begins</li>
		    <li> March 10, 2017: Voting ends at 3pm Eastern time</li>
		    <li> March 20, 2017: New representatives announced at Eclipse Converge in San Jose, CA</li>
		    </ul>

		    </div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipse.org/org/foundation/directors.php">Board of Directors</a></li>
				<li><a target="_blank" href="https://www.eclipse.org/org/elections/">Elections</a></li>
		    <li><a target="_blank" href="https://www.eclipse.org/org/elections/keydates.php">Key Dates</a></li>

			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

