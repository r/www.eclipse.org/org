<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse IoT Announce Winners of Open IoT Developer Challenge";
  $pageKeywords = "eclipse, open iot challenge, iot";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  		
  		<p><b>Ottawa, Canada – March 10, 2016</b> – The Eclipse Foundation is pleased to announce the winners of the <a href="http://iot.eclipse.org/open-iot-challenge/">Open IoT Developer 
  		Challenge</a>, an annual challenge to promote the use of open source and open standards in IoT solutions. The winners of the Challenge are:</p>
			<ul>
			<li><b>1st place</b>: <a target="_blank" href="http://iotvaidya.blogspot.in/">IoT Vaidya</a>, an IoT solution for remote 
  			patient monitoring in rural areas. IoT Vaidya allows for a patient 
  			located in a rural area to self-attach open hardware sensors and monitors that send information to a doctor in a different location.</li>
			<li><b>2nd place</b>: <a target="_blank" href="http://k33g.github.io/">Atta</a>, a simulator of connected things and gateways. It provides a way to describe IoT simulation scenarios using a 
  			domain-specific language, and to execute the simulations. It supports the open IoT protocols MQTT and CoAP.</li>
			<li><b>3rd place</b>: <a target="_blank" href="http://modes3.tumblr.com/">MoDeS</a> (Model-Based Demonstrator of Smart and Safe Systems) has 
  			implemented an IoT robot controller as well as a railway system. The goal was to demonstrate how modeling, verification, 
  			and validation techniques can be used for IoT.</li>
  			</ul>
		
		<p>More details about all the participants in the Open IoT Developer Challenge are available at 
  		<a target="_blank" href="http://openiotchallenge.tumblr.com">http://openiotchallenge.tumblr.com</a></p>
		
		<p>The Open IoT Developer Challenge is an annual challenge organized by the Eclipse IoT Working Group and sponsored by bitreactive, 
  		Eurotech, MicroEJ, Red Hat, and Zolertia. Participants are given three months to create an IoT solution using open source software 
  		and open standards. The winners were selected by a panel of judges, who evaluate the participating solutions based on innovation, 
  		completeness, use of open source and open standards, and communication with the larger community.</p>
		
		<p>The <a href="http://iot.eclipse.org/">Eclipse IoT Working Group</a> is an open source community building software frameworks and runtimes required for 
  		creating IoT solutions. Eclipse IoT includes over 20 open source projects and 30 member organizations that are collaborating 
  		on a common vision of building IoT solutions based on open source and open standards.</p>
		  		
		<h2>About the Eclipse Foundation</h2>
		<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, 
  		tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, 
  		innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse open source technology.</p>
		<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the 
  		Eclipse Foundation are available at <a href="http://www.eclipse.org">http://www.eclipse.org</a>.</p>

  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
  				<li><a href="http://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge</a></li>
  				<li><a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
