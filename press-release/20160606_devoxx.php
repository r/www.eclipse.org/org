<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Devoxx US Announced for March 21-23, 2017 in San Jose, CA";
  $pageKeywords = "eclipse, devoxx";
  $pageAuthor   = "Eric Poirier";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>

    <p><em>Inaugural US edition of the largest vendor-independent developer conference</em></p>

    <p><strong>London, UK - June 8, 2016</strong> - The largest vendor-independent developer conference, <a href="https://www.devoxx.com/" target="_blank">Devoxx</a>, is pleased to announce the first ever USA edition, Devoxx US. This inaugural event is scheduled for March 21-23, 2017 at the San Jose Convention Center. IBM joins Devoxx US as the exclusive Diamond sponsor.</p>

    <p>The Devoxx network of developer events was launched in 2001. It has quickly become the world's largest vendor-neutral developer conference in the world. The annual Devoxx conferences welcome over 11,000 attendees, from over 50 countries, at five different events (Belgium, UK, Morocco, France, Poland, and soon the United States). It is the only conference designed “from developers, for developers,” gathering the best and brightest from the international developer community.</p>

    <p>Stephan Janssen, Devoxx Founder, will lead the <a href="https://devoxx.us/" target="_blank">Devoxx US</a> program committee, and will be responsible for selecting the technical program. The Eclipse Foundation will be responsible for overall operation and production of Devoxx US. The event is expected to attract more than 1000 software developers and over 30 sponsors.</p>

    <p>Devoxx US will follow the successful Devoxx formula, and will provide attendees with access to over 200 technical sessions by expert speakers on topics that are relevant to today’s developers, covering emerging technologies & architectures and latest development trends.</p>

    <p>“Designed by developers, and envisioned as a place for the entire developer community to come together as one, Devoxx strives to provide the best tech conference setting to network, hack, be inspired and learn. We are excited about expanding our reach, bringing this same respected experience to the United States,” says Stephan Janssen.</p>

    <p>In addition to IBM’s role as Diamond sponsor, Atlassian, Codenvy, Couchbase, JetBrains, Vaadin, and the Eclipse Foundation will be founding sponsors for Devoxx US. The event will be a great opportunity for sponsors to reach the most influential developers in North America.  For more information regarding sponsorship packages, please contact <a href="mailto:sponsors@devoxx.us">sponsors@devoxx.us</a>.</p>

    <p>“We are thrilled to be part of the team to bring Devoxx to North America. The opportunity to attend Devoxx in Europe has long been the envy of US-based developers. Now they will have the opportunity to experience Devoxx close to home. Devoxx is an incredibly successful conference platform that brings together the best and brightest developers to learn and share,” says Mike Milinkovich, Executive Director of the Eclipse Foundation.</p>

    <p>Registration for Devoxx US is now open. The registration price is $995.  The first 100 attendees to register will be eligible to receive a Devoxx US collector’s item T-shirt.<br>
    Register at: <a href="https://devoxx.us">https://devoxx.us</a>.</p>

    <p>The Call for papers will open in September 2016.</p>

    <p>Follow us on twitter <a href="https://twitter.com/DevoxxUS">@DevoxxUS</a> for the latest conference news!</p>

    <p>For more information, contact <a href="mailto:info@devoxx.us">info@devoxx.us</a>  or see <a href="https://devoxx.us">https://devoxx.us</a></p>

  </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
          <li><a target="_blank" href="https://devoxx.us/">Devoxx US</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
