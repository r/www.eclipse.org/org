<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse Foundation Europe Selected to Provide Open Source and Technology Dissemination Expertise for EU H2020 IoT Research Project";
  $pageKeywords = "eclipse, agile, iot";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  		
  		<p><b>Zwingenberg, Germany – March 15, 2016</b> – The Eclipse Foundation Europe GmbH (EFE) has been selected to participate in the Adaptive 
  		Gateways for DIverse MuLtiple Environments (AGILE) project, a project funded by the European Commission – H2020 ICT30 Program. EFE will 
  		provide expertise and resources to accelerate the adoption and dissemination of the AGILE technology within the Internet of Things (IoT) 
  		industry. The organization will be responsible for hosting the AGILE open source project, mentoring the AGILE community on open source best 
  		practices, developing training material and hosting events to promote the AGILE community.</p> 
		
  		<p>AGILE is a project funded by the European Commission – H2020 ICT30 Program. The total funding for the project is 6.857.550€ over three years 
  		and includes a consortium of 17 organizations, including CREATE-NET (Italy), iMinds VZW (Belgium), Eclipse Foundation Europe GmbH (Germany), 
  		ATOS Spain SA (Spain), Canonical Group Limited (United Kingdom), Resin.io (United Kingdom), Jolocom UG (Germany), Universität Passau 
  		(Germany), Sky-Watch (Denmark), BioAssist SA (Greece), MOBISTAR SA (Belgium), Startupbootcamp (Spain), Technische Universität Graz (Austria), 
  		INRIA (France), EUROTECH SPA (Italy), Libelium (Spain), and IoTango S.A. (United States).</p> 

  		<p>The EU ICT30-2015 programme is the European programme for “Internet of Things and Platforms for Connected Smart Objects”. ICT 30-2015 
  		cuts across several technological areas (smart systems integration, cyber-physical systems, smart networks, big data) and brings together 
  		different generic ICT technologies and their stakeholder constituencies to develop technological platforms that will have a strong influence 
  		on the way in which we live and work.</p>

  		<p>AGILE aims to develop the software and hardware required to build modular gateway solutions for managing the devices and data in IoT 
  		solutions. It will support the local management of devices, data, app development, and runtime, and include security features that allow 
  		users to share data in a trusted way. The AGILE technology will be based on existing open source projects and create new open source technology to complete 
  		the solution. The AGILE project will also feature the integration of an IoT apps Marketplace to help facilitate a diverse ecosystem of extensions and plugins. 
  		Finally, AGILE will be used in five diverse Pilots in various domains (QuantifiedSelf, Smart Retails, Open Air monitoring using Drones, etc.) and be supported 
  		by an IoT Testbed, featuring more than 2500 sensors, to facilitate testing of IoT solutions.</p>
  		
		<p>The AGILE project will leverage the skills, expertise, and technology of the Eclipse Foundation to create a sustainable and diverse community supporting 
  		the AGILE technology. The goal of AGILE is to create an ecosystem of large commercial technology companies, small start-ups, research institutions and individuals 
  		that 1) creates innovative solutions and extensions using the AGILE technology and 2) reinvest in the ongoing development of the AGILE technology. The Eclipse 
  		Foundation is a leader in the open source community and specifications in open source IoT technology. The Eclipse Foundation hosts over 270 different open source 
  		projects and has 230 members. The Eclipse IoT working group specializes in open source technology for IoT and hosts over 20 different open source projects that 
  		deliver technology for IoT standards (such as MQTT, CoAP, and LWM2M) and IoT frameworks that include IoT gateways, home automation, and SCADA systems. The core of 
  		the AGILE software stack will be based on the Eclipse Kura project.</p>
  		
  		<p>More information about the AGILE project is available at <a target="_blank" href="http://agile-iot.eu">http://agile-iot.eu</a>. More information about the 
  		Eclipse IoT community is available at <a href="http://iot.eclipse.org/">http://iot.eclipse.org/</a>.</p>
  		
		  		
		<h2>About the Eclipse Foundation</h2>
		<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, 
  		tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, 
  		innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse open source technology.</p>
		<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the 
  		Eclipse Foundation are available at <a href="http://www.eclipse.org">http://www.eclipse.org</a>.</p>

  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
  				<li><a target="_blank" href="http://agile-iot.eu">AGILE Project</a></li>
  				<li><a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
