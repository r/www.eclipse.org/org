<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Eclipse Foundation Supports EU Funded SmartCLIDE Project";
$pageKeywords	= "SmartCLIDE, Eclipse Foundation";
$pageAuthor		= "Mike Milinkovich";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
				
<p>Zwingenberg, Germany &mdash; March 26, 2020 &mdash; A European consortium of eleven partners from Germany, Greece, Luxembourg, Portugal, Spain, and the United Kingdom has announced the launch of the SmartCLIDE project, a &euro;4.9 million research project funded by the European Union&rsquo;s Horizon 2020 research and innovation program. The Eclipse Foundation, a leading global open source software foundation and the largest open source organization in Europe, will support SmartCLIDE with services, including project communications, community building, IP management, and licensing.</p>

<p>In January 2020, the SmartCLIDE ( <strong>C</strong>loud, deep-<strong>L</strong>earning, <strong>I</strong>DE, <strong>D</strong>iscovery and programming-by-<strong>E</strong>xample) project was created under the leadership of the Bremen Institute for Applied Systems Technology ATB. The project proposes a new smart cloud native development environment based on the coding-by-demonstration principle and its goal is to find new ways to boost the adoption of cloud and Big Data solutions in small and medium-sized enterprises and public sector organizations. SmartCLIDE provides support for cloud services creators on different levels of abstraction at all stages of full-stack data-centered services and enables the self-discovery of IaaS and Saas services with the ultimate aim of providing a tool for empowering non-technical staff to deploy new services.</p>

<p>The project entails a strong cooperation between eleven research partners, the CERTH (Centre for Research and Technology Hellas) and The Air research institutes, software companies including Contact Software (a Solutions member of the Eclipse Foundation), Kairos DS, Intrasoft International, and universities such as the University of Macedonia. Together with security-by-design experts from The Open Group, research members and collaborators are implementing a universal reference architecture based on microservices.</p>

<p>The architecture includes tools for classification and context-related configurations of software modules, automatic testing, and distribution of solutions, as well as providing generic interfaces to leading cloud service providers. The SmartCLIDE solution builds on a behavior-driven development (BDD) approach, which enables the users&rsquo; engagement in the software development process at an early stage and in an agile manner. In addition, a deep learning engine analyzes the application usage by means of runtime monitoring. This AI component will help software developers in the future to redesign their customer solutions to fit perfec tly and to detect and eliminate bugs at a faster rate.</p>

<p>&quot;The cloud is the motor of digitization, but many companies are still hesitant to use it,&quot; says Stefan Gregorzik, Business Development Manager at CONTACT Software. &quot;SmartCLIDE should make it possible to combine high security requirements with easy system integration and a good user experience, so that cloud solutions are widely accepted&quot;.</p>

<p>&ldquo;Growing market demand for data-intensive cloud applications is driving the need for a new generation of cloud development tools like SmartCLIDE,&rdquo; said Mike Milinkovich, executive director of the Eclipse Foundation. &ldquo;We are thrilled to support the growth of a vibrant ecosystem around this innovative technology.&rdquo;</p>

<p>The Eclipse Foundation will support project communications in the worldwide open source community and the central components of the new cloud IDE will be published under the open source <a href="https://www.eclipse.org/legal/epl-2.0/">Eclipse Public License (EPL) 2.0</a> open source, the Apache Software License (ASL) 2.0, or an open source license compatible with the EPL 2.0. This means that software developers and business users will be able to view, freely utilize, and further develop the later project results.</p>

<p>More information about the SmartCLIDE project is available at <a href="http://smartclide.eu">http://smartclide.eu</a>.</p>

<h2>About the Eclipse Foundation</h2>

<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 375 open source projects , including runtimes, tools, and frameworks for a wide range of technology domains such as IoT, edge computing, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 300 members , including industry leaders who value open source as a key enabler for business strategy.</p>

<p>To learn more, follow us on Twitter <a href="https://twitter.com/eclipsefdn">@EclipseFdn</a>, <a href="https://www.linkedin.com/company/eclipse-foundation/">LinkedIn</a> or visit <a href="http://eclipse.org">eclipse.org</a>.</p>

  	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);