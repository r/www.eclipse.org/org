<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse IoT Announces Winners of Open IoT Challenge 3.0";
  $pageKeywords = "eclipse, internet of things, iot, challenge";
  $pageAuthor   = "Kat Hirsch";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    		<p><strong>Ottawa, Canada – March 16, 2017</strong> - The Eclipse IoT community, an Eclipse Foundation Working Group, is pleased to announce the winners of the third <a target="_blank" href="https://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge</a>, a developer challenge to promote the use of open source and open standards in Internet of Things (IoT) solutions. The winners were selected by a panel of judges comprised of representatives from Bitreactive, Bosch SI, Eurotech, MicroEJ, and Red Hat.</p>
    		
	<p>The winners of the this year’s Challenge are:</p>

    		<p align="center"><a target="_blank" href="https://iot.eclipse.org/open-iot-challenge/"><img class="img-responsive" src="/org/press-release/images/challenge3_winners_sm.png"/></a></p>
    		
<p><b>1st place: <a target="_blank" href="https://medium.com/inmoodforlife/in-the-mood-for-life-open-iot-challenge-final-report-a0c19482118c#.dmcxrn7b5">InMoodforLife</a></b><br>
An application to analyze and monitor sleep patterns of people affected by bipolar disorder to improve the therapeutic approach, react, and adapt the treatment faster. The solution was built using an off-the-shelf sleep monitor, Raspberry Pi, Eclipse Vert.x, and Warp 10. The team continues to develop the solution and their roadmap includes device management integration using Eclipse hawkBit and Eclipse Hono. The InMoodforLife team also plans to work with psychiatrists to validate the sleep architecture analysis of the project.</p>

<p><b>2nd place: <a target="_blank" href="http://krishi-iot.blogspot.de/2017/02/krishi-iot-final-report.html">krishi IoT</a></b><br>
A solution to help farmers execute agricultural operations in a smarter and efficient way. It includes smart sensors, a gateway device with GSM-based Internet connectivity, a mobile app, and a web app (powered by IBM Bluemix and CloudFoundry). krishi IoT devices retrieve the sensor parameters (such as temperature, humidity, soil moisture, crop images, etc), and relays the information to local krishi IoT gateway. The solutions use many Eclipse projects including Vorto, Paho, Mosquitto, Kura, Kapua, and Hono and well as IBM Bluemix, Cloudfoundry, IBM Bluemix, and Bitreactive’s Reactive Blocks.</p>

<p><b>3rd place: <a target="_blank" href="https://eneristics.wordpress.com/2017/02/27/rhds-final-report-what-we-accomplished/">RHDS</a></b><br>
Residential Home Diagnostics System (RHDS) is smart energy IoT solution that performs home diagnostics for residents. The solution uses three Eclipse projects: Kura, Leshan, and Wakaama as well as open protocols such as MQTT, CoAP, LWM2M, among other technology.</p>

<p><b>Special Acknowledgement</b></p>
<p>The judging panel would also like to acknowledge the submission from <a target="_blank" href="https://agrinode.blogspot.de/2017/02/final-report-for-open-iot-challenge-30.html">AgriNode</a>. The team developed a Wireless Sensor Network system consisting of a gateway and sensor nodes. The system was built using Arduino, Raspberry Pi, and open source IoT software (Eclipse Kura, Reactive Blocks).</p>

<p>The Open IoT Developer Challenge is an annual challenge organized by the Eclipse IoT Working Group, in collaboration with the sponsors Bitreactive, Bosch SI, Eurotech, MicroEJ, and Red Hat. Participants are given three months to create an IoT solution using open source software and open standards. The three winners are selected by a panel of judges, who evaluate the participating solutions based on the applicability of a solution to a specific industry, innovation, completeness, use of open source and open standards, and communication with the larger community.</p>

    	<p align="center"><a target="_blank" href="https://iot.eclipse.org/open-iot-challenge/"><img class="img-responsive" src="/org/press-release/images/challenge3_sponsors.png"/></a></p>
    		
    		
<p>More details about the Challenge and the finalist is available <a target="_blank" href="https://iot.eclipse.org/open-iot-challenge/">here</a>.</p>

    		<h2>About Eclipse IoT and Eclipse Foundation</h2>
<p>The <a target="_blank" href="https://iot.eclipse.org/">Eclipse IoT Working Group</a> is a collaborative working group hosted by the Eclipse Foundation, that is working to create open source software for IoT solutions. Eclipse IoT is made up of over 30 member companies and 28 open source projects.</p>

    		<p>Eclipse is a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects in the Eclipse community, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tools for scientific research, and much more. The Eclipse Foundation is a not-for-profit foundation that is the steward of the Eclipse community. More information is available at <a href="https://www.eclipse.org/">eclipse.org</a>.</p>
    		
  </div>

  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
    		<li><a target="_blank" href="https://iot.eclipse.org/open-iot-challenge/">Open IoT Challenge 3.0 Winners</a></li>
    		<li><a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
