<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include ("_projectCommon.php"); // All on the same line to unclutter the user's desktop'

//
                                                                                                                                                                                                                                                                                                                                                                                                    // Begin: page-specific settings. Change these.
$pageTitle = "Eclipse Foundation Updates its Terms of Use and Privacy Policy";
$pageKeywords = "eclipse, privacy policy, terms ";
$pageAuthor = "Katharine Hirsch";

// Add page-specific Nav bars here
// Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
// $Nav->addCustomNav("My Link", "mypage.php", "_self");
// $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

// End: page-specific settings
//

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>


			<p>The Eclipse Foundation has posted new versions of its <a href="http://www.eclipse.org/legal/termsofuse.php">Website Terms of Use</a>
		    and <a href="http://www.eclipse.org/legal/privacy.php">Privacy Policy</a>. Our motivations for updating these documents are pretty simple:</p>

    <ol><li>The Eclipse Foundation has started offering services which collect and store user data on our servers.
		For example, the Automated Error Reporting Initiative will store stack traces from users who have encountered errors and wish to report them.
		    Our previous versions of these documents were constructed under the assumption that what people were sending us was code, documentation, patches, and the like.
		    As a result, we have added words to the effect that the data a user sends is open, unencrypted, and publicly available on the Creative Commons CC0 1.0 license.
		    From a privacy perspective we made it clear that the data in AERI and the User Storage Service is public. So just like you don't put passwords in your GitHub repository, don't store private data with Eclipse.</li>

		  <li>As a matter of clean up, we finally removed all references to the Common Public License from our terms of use. The Eclipse Foundation switch
		    from the CPL to the Eclipse Public License about a decade ago, so it was time.</li></ol>

<p>If you have any questions or concerns with this changes, please contact <a href="mailto:license@eclipse.org">license@eclipse.org</a></p>
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://www.eclipse.org/legal/termsofuse.php">Website Terms of Use</a></li>
				<li><a target="_blank" href="http://www.eclipse.org/legal/privacy.php">Privacy Policy</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;

// Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php

/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

