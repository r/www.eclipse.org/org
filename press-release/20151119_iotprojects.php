<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "New Releases of Eclipse IoT Projects Advance IoT Open Source Technology";
  $pageKeywords = "eclipse, iot, kura, concierge, californium";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  <p><b>Ottawa, Canada – November 19, 2015</b> – The Eclipse Internet of Things (IoT) Working Group, an Eclipse Foundation Working Group, 
  		is pleased to announce the new releases of three IoT open source projects, including Eclipse Kura 1.3, Eclipse Californium 1.0, 
  		and Eclipse Concierge 5.0. These projects, together with the entire Eclipse IoT ecosystem, provide open source IoT technology 
  		for developers to build IoT solutions.</p>
  		
  	<ul>
  		<li><b>Eclipse Concierge 5.0</b> is an implementation of the OSGi framework that is ideally suited for very small IoT devices. 
  		Concierge 5.0 implements the OSGi 5.0 specification and has a footprint of only 250KB. Compared to implementations of other 
  		OSGi frameworks, Concierge has a 2x faster startup time, has better runtime performance, and is easier to deploy and manage. 
  		Concierge can run on IoT hardware, like Beaglebone or Raspberry Pi, using Java 8 Compact Profile. Concierge 5.0 is available 
  		for <a href="https://projects.eclipse.org/projects/iot.concierge/downloads">download now</a>.</li>
  		
  		<li><b>Eclipse Californium 1.0</b> is the first stable release offering a Java implementation of the Constrained Application Protocol 
  		(CoAP) standardized by the IETF in RFC 7252. Californium also includes an implementation of the DTLS 1.2 security standard. 
  		Californium 1.0 can be <a href="https://www.eclipse.org/californium/">downloaded now</a>.</li>
  		
  		<li><b>Eclipse Kura</b> implements a common set of services required by IoT Gateways, including I/O access, data services, 
  		network configuration, and remote management. The new 1.3 release of Kura improves the software update feature by 
  		increasing the process efficiency and reliability on large files. The Kura web UI has been improved with new security 
  		and bundle management features. Kura 1.3 can now run on the Java 8 Compact Profile, resulting in both a faster startup 
  		and a smaller footprint.  Kura 1.3 is now available for <a href="https://www.eclipse.org/kura/downloads.php">download</a>.</li>
  	</ul>
  <p>The Eclipse IoT community consists of 20 different open source projects, over 1.6 million lines of code, and includes contributions 
  		from over 140 developers and participation from 30 organizations. The goal of the community is to provide the open source technology 
  		developers need to build IoT solutions.</p>
  		
	<p>In conjunction with these releases, the Eclipse IoT Working Group is also organizing an Open IoT Challenge to encourage developers to 
  		build IoT solutions using open source technology and open standards. Developers can apply to participate by November 23 and will have 
  		until the end of February to complete their solution. A panel of judges will select the best solutions that will be awarded prizes.  
  		More details are available at <a href="http://iot.eclipse.org/open-iot-challenge/">http://iot.eclipse.org/open-iot-challenge/</a></p>
  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://iot.eclipse.org/">Eclipse IoT</a></li>	
  				<li><a href="http://iot.eclipse.org/open-iot-challenge">Open IoT Challenge</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
