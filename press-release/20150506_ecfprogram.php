<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "EclipseCon France 2015 - Program and Keynote Announced";
	$pageKeywords	= "eclipse, eclipsecon, france, program";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
				
			
			<p>We're pleased to announce the program and keynote for <a target="_blank" href="https://www.eclipsecon.org/france2015/">EclipseCon France 2015</a>, planned for June 24-25 in Toulouse. 
				EclipseCon France is dedicated to Eclipse Working Groups (such as PolarSys, Internet of Things, and Science) and is designed to create opportunities 
				for the French and entire European Eclipse community to learn, explore, share, and collaborate on the latest ideas and information about Eclipse 
				technologies, the Eclipse Working Groups, and Eclipse member companies. We will be hosting an <a target="_blank" 
				href="https://www.eclipsecon.org/france2015/unconference">Unconference</a> again this year, June 22-23 at ENSEEIHT.</p>
			<p>The EclipseCon France <a target="_blank" href="https://www.eclipsecon.org/france2015/conference/schedule/session/2015-06-24">program</a> includes 
				36 talks and 8 workshops over two days, and features sessions on
				<ul>
					<li>Eclipse in the Industry</li>
					<li>IoT and Embedded</li>
					<li>Science</li>
					<li>Modern Technologies and Methodologies</li>
					<li>Eclipse Technologies and Community</li>
					<li>and much more!</li>
				</ul>
			<h3>Keynote Speaker</h3>
			<p>We are especially pleased to announce this year's keynote speaker:</p>
			
			<img src="/org/images/raftery.jpg" width="45%"/>
			<p><b>Tom Raftery, RedMonk</b></p> 
			<p><a target="_blank" href="https://www.eclipsecon.org/france2015/session/smarter-grids-convergence-internet-things-and-energy">Smarter Grids: The Convergence of the Internet of Things and Energy</a></p>
			
			<h3>Register Now</h3>	
			<p>Registration is now open and the early registration deadline ends May 26. <a target="_blank" href="https://www.eclipsecon.org/france2015/registration">Register now</a> and join us in Toulouse!</p>									
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2015/conference/schedule/session/2015-06-24">Program</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2015/keynote">Keynote</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2015/unconference">Unconference</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2015/registration">Registration</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

