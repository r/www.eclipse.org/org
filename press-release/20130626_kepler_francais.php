<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "La sortie simultanée Eclipse annuelle prête au téléchargement";
	$pageKeywords	= "eclipse, kepler, open source, release train";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

			<p><i>Eclipse Kepler se compose de 71 projets et plus de 58 millions de lignes de code</i></p>

			<p><b>Ottawa, Canada – 26 juin 2013</b> – La fondation Eclipse est heureuse d’annoncer la sortie simultanée annuelle Eclipse. Chaque année, la communauté Eclipse coordonne une sortie majeure de ses projets Open Source fin juin. Elle permet aux utilisateurs des technologies Eclipse de mettre à jour, en une fois, les nouvelles versions de leurs projets.</p>
			<p>Eclipse Kepler met en avant 71 équipes de projets Open Source, composées de 420 développeurs provenant de 54 organisations différentes et avec tout cela plus de 58 millions de lignes de code. Cette sortie simultanée démontre que les processus de développement Open Source sont particulièrement efficaces pour le développement distribué à grande échelle.</p>
			<p>« La sortie simultanée Eclipse est très importante pour le succès et la croissance de tout l’écosystème Eclipse » explique Mike Milinkovich, Directeur Exécutif de la fondation Eclipse. « Les utilisateurs des technologies Eclipse savent qu’ils peuvent s’appuyer sur un agenda prévisible annuel de sortie, ainsi les organisations sont confiantes dans l’utilisation de la technologie Eclipse pour leur plateforme de développement logiciel. Eclipse est une parfaite illustration de la révolution suscitée par l'Open Source dans l'industrie logicielle. »</p>
			<p>Voici quelques-unes des principales nouveautés pour Kepler :</p>
			<p><b>Support de Java EE7</b> – La version 3.5 du projet <a href="http://www.eclipse.org/webtools/">Eclipse Web Tools (WTP)</a> apporte le support de Java EE 7, lui-même récemment publié, incluant le support pour JPA 2.1, JSF 2.2, JAX-RS 2.0, Servlet 3.1, EJB 3.2, Connector 1.7, App Client 7.0 et EAR 7.0. Les assistants de création, les aides au contenu et de validation ont été mis à jour. Ainsi les développeurs Java peuvent facilement créer, débugger et déployer des applications compatibles Java EE 7.</p>
			<p><b>Nouvelle suite pour la gestion des processus métier</b> – La sortie d’<a href="http://www.eclipse.org/stardust/">Eclipse Stardust 1.0</a> apporte un moteur et des outils de gestion de processus métier. Eclipse Stardust inclut un environnement de modélisation qui permet de créer et de débugger les modèles de workflow, un moteur de processus pour exécuter des applications BPM, un portail web pour les exécutions basées sur un navigateur et la surveillance des processus métier et un composant de reporting basé sur BIRT pour la surveillance d’exécution et le reporting des applications BPM.</p>
			<p><b>Extensibilité et utilisation d’un IDE basé sur le web</b> – <a href="http://www.eclipse.org/orion/">Orion 3.0</a> apporte des améliorations sur l’utilisation et l’extension de l’IDE web Orion. Il peut maintenant être facilement installé comme un fichier WAR dans un serveur d’applications Java,  permettant de déployer vers des services cloud plus facilement. L’utilisation d’Orion a également été améliorée pour inclure la navigation de fichiers directement dans l’éditeur, de nouveaux raccourcis (vi et Emacs), sauvegarde automatique / chargement automatique et un nouveau style visuel.</p>
			<p><b>Nouveau support pour le Big Data</b> – <a href="http://www.eclipse.org/birt/phoenix/">Eclipse BIRT 4.3</a> introduit le support des bases de données MongoDB et Cassandra et permet une intégration simplifiée pour les capacités de visualisation BIRT dans des applications Big Data. C’est un ajout au support existant d’Hadoop fourni par BIRT.</p>
			<p><b>Meilleure intégration pour la revue de code</b> – Grâce à <a href="http://www.eclipse.org/mylyn/">Mylyn 3.9</a>, il est désormais beaucoup plus facile de réaliser des revues de code dans Eclipse. La nouvelle vue navigateur intégrée avec Gerrit démontre la vue structurée de tous les fichiers et commentaires d’une revue.</p>
			<p><b>Amélioration de l’intégration avec Maven pour les développeurs JavaEE</b> – Nouveau support pour l’<a href="http://www.eclipse.org/m2e-wtp/">intégration Maven avec le projet Eclipse Web Tools (WTP)</a> fournissant un ensemble de connecteurs qui ajoute le support Maven pour les projets Eclipse Java EE, incluant les projets war, ejb, et rar.</p>
			<br/>
			<p>Tous les projets qui participent à la sortie Kepler sont dès maintenant disponibles au <a href="http://eclipse.org/kepler/">téléchargement</a>. En complément, douze packages basés sur des profils développeurs différents permettent le téléchargement de cette nouvelle version facilement.</p>

			<p><b>A propos de la fondation Eclipse</b></p>
			<p>Eclipse est une communauté Open Source dont les projets sont centrés sur la construction d’une plateforme de développement ouverte composée de frameworks extensibles, outils et runtimes pour construire, déployer, et gérer le cycle de vie logiciel. Un écosystème large et dynamique d’éditeurs logiciels majeurs, de start-ups innovantes, d’instituts de recherche, et d’individuels étendent, complètent et supportent la plateforme Eclipse.</p>
			<p>La fondation Eclipse est une organisation à but non lucratif, financée par ses membres,  qui hébergent les projets Eclipse. Le détail complet d’Eclipse et de la fondation Eclipse est disponible sur <A href="http://www.eclipse.org/">www.eclipse.org</a>.</p>
			
	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/kepler/">Kepler</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

