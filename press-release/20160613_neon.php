<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Neon Release Page";
  $pageKeywords = "eclipse, neon";
  $pageAuthor   = "Eric Poirier";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>

    <p>The Eclipse Neon release train is scheduled for June 22nd, 2016. The Eclipse Foundation has organized a Neon webinar series comprised of 7 episodes, in support of the release.</p>

    <p>The Neon webinar series will commence on June 15th, 2016. Each webinar will be led by project leads and experts in an effort to highlight new features and promote discussion around the release.</p>
      <div class="table-responsive">
     <table class="table table-striped">
        <thead>
          <tr>
            <th>Date</th>
            <th>Topic</th>
            <th>Presenter(s)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>June 15, 2016 at 8:30am EST (add European time)</td>
            <td>Eclipse Neon and Beyond - The JDT Perspective</td>
            <td>
              <ul>
                <li>Manoj Palat (Committer in the Eclipse JDT Core Project)</li>
              </ul></td>
          </tr>
          <tr>
            <td>June 16, 2016 at 9:00am EST</td>
            <td>JSDT 2.0</td>
            <td>
              <ul>
                <li>Gorkem Ercan (RedHat)</li>
                <li>Ilya Buziuk (RedHat)</li>
              </ul>
            </td>
          </tr>
        <tr>
            <td>June 20, 2016 at 9:00am EST</td>
            <td>The Eclipse User Storage Service (USS): Creating a Better User Experience for Developers</td>
            <td>
              <ul>
                <li>Antoine Thomas (Eclipse)</li>
                <li>Christopher Guindon (Eclipse)</li>
              </ul>
            </td>
          </tr>
        <tr>
            <td>June 21, 2016 at 11:00am EST</td>
            <td>Eclipse Tools for Cloud Foundry Overview</td>
            <td>
              <ul>
                <li>Nieraj Singh (Pivotal Software)</li>
                <li>Elson Yuen (WebSphere Application Server Tools)</li>
              </ul>
            </td>
          </tr>
        <tr>
            <td>June 22, 2016 at 9:00am EST</td>
            <td>What's New for PHP Developers in Eclipse Neon?</td>
            <td>
              <ul>
                <li>Kaloyan Raev (Committer in the PDT, DLTK and other Eclipse Projects)</li>
              </ul>
            </td>
          </tr>
        <tr>
            <td>June 23, 2016, at 9:00am EST</td>
            <td>Docker Tooling for Eclipse Neon</td>
            <td>
              <ul>
                <li>Xavier Coulon (RedHat)</li>
              </ul>
            </td>
          </tr>
        <tr>
            <td>June 24, 2016 at 9:00am EST</td>
            <td>Automated Error Reporting (AERI) now available for any Eclipse Plugin</td>
            <td>
              <ul>
                <li>Marcel Bruch (Eclipse)</li>
              </ul>
            </td>
          </tr>
        </tbody>
        </table>
        </div>
        <p>The Neon web page is available at <a href="https://www.eclipse.org/neon">https://www.eclipse.org/neon</a>. Downloads will not be available until June 22nd, 2016.</p>
        <p>Please register for the Neon webinar series at <a href="https://www.eclipse.org/community/webinars">https://www.eclipse.org/community/webinars</a>, to join the discussion and help spread the word.</p>
  </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
          <li><a href="https://www.eclipse.org/neon">Neon page</a></li>
        <li><a href="https://www.eclipse.org/community/webinars">Webinars</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
