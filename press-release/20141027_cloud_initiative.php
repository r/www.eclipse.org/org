<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Announces Cloud Development Industry Initiative";
	$pageKeywords	= "eclipse, foundation, cloud";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

			<p><i>Codenvy, IBM, Pivotal and SAP Lead New Eclipse Cloud Development Top-level Project</i></p>
		
			<p><b>Ottawa, Canada - October 27, 2014</b> - The Eclipse Foundation is pleased to announce a new industry initiative to create the technologies, platforms, and tools necessary to enable the delivery of highly integrated cloud development and cloud developer environments. This new initiative will establish an open source community dedicated to cloud development tools, with over 65 developers and 8 companies participating by providing committers and by investing resources. Codenvy, IBM, Pivotal and SAP will staff the steering committee of the new <a href="https://www.eclipse.org/ecd/">Eclipse Cloud Development top-level project</a> (ECD).</p> 
		
			<p>Cloud computing has changed how applications are deployed and consumed, but developers have struggled with a disparate blend of desktop and cloud services that don’t work together or share the same standards or protocols. Developers require an open, modern, and integrated set of technologies that can be used to provision cloud development platforms, create cloud developer environments, and support the interaction of clients (such as IDEs) with distributed services. The ECD initiative will deliver services and tools for cloud orchestration of IDEs, build systems, continuous integration, code assistants, and debugging.</p>
						
			<p>The Eclipse Cloud Development top-level project encompasses three areas of work:</p>
				<ul>	
					<li>Industry-Oriented Developer Environments. Providing industry-oriented solutions based upon ECD building blocks.</li>
					<li>Platforms for Hosting & Connecting Developer Environments. Creating the technologies to host, scale, integrate, and manage developer and development environments.</li>
					<li>Tools to Extend Platforms. Creating the tools to build, extend and adopt these platforms for new development scenarios.</li>
				</ul>			

			<p>ECD will bring together existing and new cloud based projects to establish common building blocks, protocols, and APIs to facilitate interoperability. The initial projects hosted by ECD include</p>
				<ul>	
					<li><a href="http://www.eclipse.org/orion/">Eclipse Orion</a> - a cloud IDE with services for JavaScript and dynamic languages</li>
					<li><a href="http://www.eclipse.org/flux">Eclipse Flux</a> - a messaging bus that enables interoperability between desktop and cloud development tools and between development micro-services</li>
					<li><a href="https://projects.eclipse.org/projects/technology.che">Eclipse Che</a> - an extensible platform for SaaS developer environments that provisions, shares, and scales projects</li>
					<li><a href="https://projects.eclipse.org/proposals/dirigible">Eclipse Dirigible</a> – a proposed project for a cloud IDE that supports a full development lifecycle of on-demand applications leveraging in-system programming models and rapid application development techniques</li>
				</ul>	
					
			<p>"Software development in the cloud is definitely a big part of the future for developer tools and services," explains Mike Milinkovich, executive director of the Eclipse Foundation. “This is why we are excited to bring together companies like SAP, Pivotal, IBM and Codenvy to collaborate on creating the building blocks to enable the next generation of software development in the cloud. This builds on the vision laid out three years ago when the Orion project was introduced at Eclipse by IBM.” </p>
	
			<h3>Supporting Quotes</h3>

				<p><b>Codenvy</b><br/>
				"We are excited to see our contribution become Eclipse Che and to help drive the Eclipse Cloud Development initiative. The ECD project will standardize how developers code, build, run and deploy in the cloud.  Codenvy will be contributing 35 people to the advancement of ECD IP and ecosystem. The strong showing of industry support for ECD indicates that the era of cloud development has arrived."</p>
				
				<p><b>IBM</b><br/>
				"As a longtime supporter of Eclipse, we are pleased the Eclipse Cloud Development initiative will continue to raise the visibility of Eclipse Orion. The Orion Web IDE is a key part of our DevOps Services for Bluemix - IBM's cloud platform for development," said IBM Vice President of Open Technology and Cloud Performance Solutions, Angel Diaz.  "As the Eclipse initiative evolves, the tech and development communities will continue to benefit from these additional cloud development resources."</p>
				
				<p><b>Red Hat</b><br/>
				"Red Hat has been a supporter and contributor to the Eclipse IDE project for many years, providing support for key open source technologies, including Hibernate, Arquillian, WildFly, Drools, and Apache Camel. As developers look to cloud-based platforms such as JBoss xPaaS and OpenShift by Red Hat we are excited to see the launch of the Eclipse Cloud Development Project and are committed to ensuring that open source technology continues to define and dominate the software development landscape as we enter the era of open, hybrid cloud."<br/>
				-- Rich Sharples, Red Hat</p>
				
				<p><b>SAP</b><br/>
				"SAP made strategic investments in the cloud infrastructure and services over the last years to enable enterprises for their transition to the Cloud. The holistic and intuitive development experience in this new environment is a key factor, which can attract people to start immediately building their next generation cloud solutions. Eclipse Foundation is the perfect place for a collaboration with the other leaders, where we can continue and further extend the good tradition so far related to our on-premise tools. That is why we decided to join this new initiative as well, and together lay down the foundation of a new standard for cloud development."<br/>
				-- Radoslav Nikolov, managing director, SAP Labs Bulgaria</p>
				
				<p><b>Tasktop</b><br/>
				"It's very promising to see a new top-level project dedicated to cloud development at Eclipse," said Mik Kersten, Tasktop CEO and creator of the Eclipse Mylyn project. "Eclipse provides a proven ground for connecting corporate and community contributions, and it's at that intersection that some of the most interesting tool innovation happens."</p>
				
				<p><b>WSO2</b><br/>
				"Eclipse Che provides a significant advancement in supporting today’s connected world by enabling unprecedented flexibility and collaboration for software developers, whether they are accessing the IDE from their desktops, corporate servers, or the cloud,” said Dr. Sanjiva Weerawarana, WSO2 founder and CEO. “We are excited to extend our support for Eclipse’s open source technology as a new member of the Eclipse Foundation, contributing technology to run Eclipse Che as a desktop application and collaborating with Codenvy on cloud-enabled plugins for the Eclipse Che IDE platform."</p>
				
				<p><b>eXo</b><br/>
				"By moving to cloud based developer environments, Eclipse is changing the way we approach developments. The process of setting up environments has always been painful and time consuming. These setups are often heavy... Now, Eclipse will set us up with a ready-to-code / ready-to-test environment in just a click. This also means that onboarding new contributors will be very easy as it will just need one click for them to have the environment ready."<br/>
				-- Frederic Drouet, Head of Engineering at eX</p>	

				<p><b>Kin Lane, API Evangelist</b><br/>
				Every aspect of application development is moving into the clouds making things more modular and scalable and it is good to see efforts out there like CodeEnvy, and their work with the Eclipse Foundation to include APIs in the IDE, as it evolves into the cloud. Now developers will be able to have exactly the tools they need, in a modular, containerized, and customizable environment that runs in the cloud, and doesn't just outperform the desktop experience, but truly scales as developers now expect throughout the application development lifecycle.</p>
		</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipse.org/ecd/">Eclipse Cloud Development top-level project</a></li>
				<li><a href="http://mmilinkov.wordpress.com/2014/10/27/eclipse-cloud-development-the-faq/">Eclipse Cloud Development: The FAQ</a></li>
				<li><a href="http://www.eclipse.org/orion/">Eclipse Orion</a></li>
				<li><a href="https://projects.eclipse.org/projects/technology.che">Eclipse Che</a></li>
				<li><a href="http://www.eclipse.org/flux">Eclipse Flux</a></li>
				<li><a target="_blank" href="https://projects.eclipse.org/proposals/dirigible">Eclipse Dirigible</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

