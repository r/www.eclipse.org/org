<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "New Eclipse IoT project releases to accelerate IoT solution development";
  $pageKeywords = "eclipse, iot";
  $pageAuthor   = "Eric Poirier";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>

    <p>OTTAWA, Canada – June 16, 2016 – <a href="http://iot.eclipse.org/">The Eclipse Internet of Things (IoT)
        Working Group</a>, hosted at the Eclipse Foundation, is pleased to announce
        <strong>new releases for four open source IoT projects</strong>: <a href="https://www.eclipse.org/kura/">Eclipse Kura 2.0</a>,
        Eclipse Paho 1.2, Eclipse SmartHome 0.8 and Eclipse OM2M 1.0. These
        projects are helping developers to rapidly create new IoT solutions
        based on open source and open standards.</p>

    <p>Eclipse IoT is an open source community that provides the core technologies
        required by developers to build IoT solutions. The community is composed
        of over <strong>200 active contributors</strong> working on <strong>24 different projects</strong>. These
        projects are made up of over 2 million lines of code and have been
        <strong>downloaded over 500,000 times</strong>. The Eclipse IoT Working Group includes
        <strong>30 member companies</strong> that collaborate to provide software building blocks
        in the form of open source implementations of the standards, services,
        and frameworks that enable an Open Internet of Things.</p>

        <h3>Eclipse SmartHome 0.8</h3>
        <p>Eclipse SmartHome is a framework that allows building smart home
        solutions that have a strong focus on heterogeneous environments.It
        is designed to run on embedded devices, such as Raspberry Pi,
        BeagleBone Black or Intel Edison.</p>

        <p>This new Eclipse SmartHome 0.8 release includes:</p>
        <ul>
          <li><strong>New REST API</strong>, including Server-sent events (SSE) support, that exposes the functionality of Eclipse SmartHome to clients for easy interaction. SmartHome’s “Paper UI” is an administration interface that makes use of this new API.</li>
          <li><strong>Additional bindings</strong> to support many new devices, like Sonos speakers, LIFX bulbs, Belkin WeMo devices, digitalSTROM systems, and others.</li>
          <li><strong>OSGi EnOcean Base Driver</strong> contributed by Orange Labs.</li>
          <li><strong>New rule engine</strong>, a very flexible and powerful framework that supports templates for beginners, JavaScript for automation rules and graphical rule editors, contributed by ProSyst.</li>
        </ul>

        <p>Find out more about <a href="https://www.eclipse.org/smarthome/">Eclipse SmartHome 0.8</a>.
        The 0.8 release is now available for <a href="https://www.eclipse.org/smarthome/documentation/community/downloads.html">download</a>.</p>

        <h3>Eclipse Paho 1.2 </h3>
        <p>Eclipse Paho provides open-source client implementations of the MQTT and MQTT-SN messaging protocols. The new Paho 1.2 release includes updates to existing Java, Python, JavaScript, C, .Net, Android and Embedded C/C++ client libraries. The improvements include:</p>
        <ul>
        <li><strong>Automatic reconnect & offline buffering</strong> functionality for the C, Java and Android Clients.</li>
        <li><strong>WebSocket support</strong> for the Java and Python Clients.</li>
        <li><strong>New Go Client</strong>, a component for Windows, Mac OS X, Linux and FreeBSD.</li>
        </ul>

        <p>Eclipse Paho 1.2 is now <a href="https://www.eclipse.org/paho/downloads.php">available</a>.</p>

        <h3>Eclipse Kura 2.0</h3>

        <p>Eclipse Kura is a framework for building IoT gateways. The latest release, Kura 2.0, brings many new features and enhancements, including: </p>

        <ul>
        <li><strong>New responsive user interface</strong> (UI) based on Bootstrap that simplifies Kura gateway management from mobile devices. </li>
        <li><strong>Support for multiple cloud connections</strong>, enabling scenarios where a single gateway can be simultaneously connected to multiple cloud platforms including Eurotech Everyware Cloud, Amazon AWS IoT, Microsoft Azure IoT and IBM IoT Foundation.</li>
        <li><strong>Improved onboarding process for developers</strong> with new tools and code samples to ease the creation of Kura applications and drag-and-drop deployment of Kura apps available from the Eclipse Marketplace.</li>
        <li>Tighter integration with <strong>Apache Camel</strong> to enable declarative message routing within the business logic of the Eclipse Kura applications.</li>
        </ul>

        <p>Eclipse Kura 2.0 will be available later in June.</p>

        <h3>Eclipse OM2M 1.0</h3>

        <p>Eclipse OM2M is an open source implementation of the oneM2M standard. It provides a set of horizontal IoT services that enable the development of IoT solutions independently of the underlying network. </p>

        <p>The OM2M 1.0 implementation of oneM2M provides the following key features:</p>

        <ul>
        <li><strong>Modular platform architecture</strong>, based on OSGi making it highly extensible</li>
        <li><strong>Lightweight REST API</strong> exposed through multiple communication bindings including HTTP and CoAP protocols and supporting various content formats such as XML and JSON.</li>
        <li><strong>Flexible data storage</strong> based on an abstract persistence layer supporting embedded & server databases, in-memory mode, SQL & NoSQL models.</li>
        </ul>

        <p>OM2M implements the following key concepts of the oneM2M specification:</p>

        <ul>
        <li><strong>Dedicated Common Service Entity</strong> (CSE) for Infrastructure node (IN), Middle Node (MN), and Application Service Node (ASN).</li>
        <li><strong>Common Service Function</strong> (CSF) including: Registration, Application and Service Management, Discovery, Data Management and Repository, Subscription and Notification, Group Management, Security, etc.</li>
        </ul>

        <p>Find out more about <a href="https://www.eclipse.org/om2m/">Eclipse OM2M</a>. The 1.0 release will be available later this June.</p>

        <h3>New Eclipse IoT Project Proposal</h3>

        <p>Eclipse Kapua is a new <a href="https://projects.eclipse.org/proposals/eclipse-kapua">open source project proposal</a>
        from Eurotech to create a modular integration platform
        for IoT devices and smart sensors, that aims to bridge
        Operation Technology with Information Technology. Eclipse
        Kapua focuses on managing edge IoT nodes including their
        connectivity, configuration, and application life cycle.
        It also allows to aggregate real-time data streams from the
        edge, either archiving them or routing them towards enterprise
        IT systems and applications. Interested parties should get
        in touch with the project leadership on the <a href="https://projects.eclipse.org/proposals/eclipse-kapua">project proposal page</a>.</p>

        <p>More information about the Eclipse IoT community is available at <a href="http://iot.eclipse.org/">iot.eclipse.org</a>.</p>
        </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
          <li><a href="http://iot.eclipse.org">Eclipse IoT</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
