<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Keynotes for Devoxx US Announced";
	$pageKeywords	= "devoxx, conference, eclipse";
	$pageAuthor		= "Kat Hirsch";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

		 <p><i>Industry experts will present topics about identity, deep learning, and design.</i></p>

		    <p><b>Ottawa, Canada - January 31, 2017</b>- The world’s largest vendor-independent developer conference, Devoxx, is pleased to announce the keynote speakers for Devoxx US 2017.
		    Devoxx US is part of the Devoxx network of conferences that attracts over 11,000 attendees, from over 50 countries, at separate events in Belgium, UK, Morocco, France, Poland, and the United States.
		    This is the inaugural year for Devoxx US, scheduled for March 21-23 in San Jose, CA.</p>

		 <p>The Devoxx US keynote speakers are leading experts on topics that are relevant to forward-thinking software developers.
		    The speakers and topics include the following:</p>

		    <ul>
		    <li><b>Janelle Klein</b> from Open Mastery will present <a target="_blank" href="http://cfp.devoxx.us/2017/talk/NAN-8167/What_is_%22Identity%22%3F">“What Is Identity"</a>, which explores using software as a metaphor to understand the human brain.
		    This keynote will introduce a new language of metaphorical shapes to describe the “Architecture of Identity,” and will examine some of the key dysfunctions in our organizations through this lens.</li>
		    <li><b>Brian Kane’s</b> keynote, titled <a target="_blank" href="http://cfp.devoxx.us/2017/talk/NHF-2672/Personality_Is_the_New_Ringtone:_Experiments_in_AI_design">“Personality is the New Ringtone: Experiments in AI Design"</a>, will discuss how artificial intelligence is
		    changing the way we think about design, and how we identify and address the new challenges of designing intelligent and connected devices and experiences for people.
		    Brian is also the developer behind the viral Billy Bass Alexa hack.</li>
		    <li><b>Olivier Gevaert</b>, assistant professor of medicine at Stanford University, will talk about how deep learning technology is being applied to state-of-the-art cancer treatments.
		    Olivier’s research makes extensive use of TensorFlow, an open source library for machine learning.</li>
		    <li><b>The Java Posse Reunion</b> will be the closing keynote for Devoxx US.
		    The popular <a target="_blank" href="http://javaposse.com/">Java Posse</a>, featuring Dick Wall, Chet Haase, Tor Norbye, Joe Nuxoll, and Carl Quinn, will be back together to highlight the latest Java trends in their typical community style.</li>
		    </ul>

		    <p>Devoxx US is for software developers who want to be immersed in the latest trends and technology in software development.
		    The conference features over 170 speakers and over 200 technical sessions.</p>

		    <p>The session tracks include Methodology & Architecture, Java Language, Cloud, Containers & Infrastructure, Server Side Java, Programming Languages, Big Data, Modern Web, Future<Devoxx>, and Mobile & Embedded.
		    The sessions were selected by an independent committee of developers to create a program in the Devoxx tradition of “From Developers For Developers.” </p>

		    <p><a target="_blank" href="https://devoxx.us/registration/">Registration</a> for Devoxx US is now open. Explore the <a target="_blank" href="http://cfp.devoxx.us/2017/index.html">Program</a>.</p>
		    <p>Devoxx is produced by the Eclipse Foundation. Stephan Janssen, founder of Devoxx, is the Devoxx US program chair.</p>

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.devoxx.us">Devoxx US</a></li>
				<li><a target="_blank" href="https://devoxx.us/registration">Registration</a></li>
				<li><a target="_blank" href="http://cfp.devoxx.us/2017/index.html">Program</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

