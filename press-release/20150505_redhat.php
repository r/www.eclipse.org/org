<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Foundation Announces Red Hat as a Strategic Member";
	$pageKeywords	= "eclipse, eclipsecon, member, redhat";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
				
			
			<p><i>Company reaffirms its commitment to Eclipse open source tools and the new Eclipse Internet of Things open source community</i></p>

			<p><b>Ottawa, Canada - May 5, 2015</b> - The Eclipse Foundation is pleased to welcome <a target="_blank" href="http://www.redhat.com/en">Red Hat</a> as a strategic member of the Eclipse Foundation. Red Hat has been a long-time solution member 
				of the Eclipse Foundation and actively involved in the Eclipse open source community. As a new strategic member, Red Hat will take a seat on the Board of Directors of the Eclipse Foundation, 
				strengthening its support of the Foundation.</p>
			<p>Red Hat is an active member of the Eclipse open source community. Red Hat employees participate in 27 Eclipse Projects, including as project leaders for Vert.x, m2e-wtp, THyM and Linux Tools.</p> 
			<p>Red Hat delivers Eclipse-based solutions to their developer communities, including:</p>
				<ul>		
					<li>Red Hat JBoss Developer Studio, built on the popular Eclipse-based developer tool JBoss Tools, allows Eclipse Java users to develop applications for Red Hat JBoss Middleware, such as Red Hat 
					JBoss Enterprise Application Platform and JBoss Fuse.</li> 
					<li>Red Hat Developer Toolset, based on Eclipse Linux Tools and CDT, allows C/C++ developers to quickly build Red Hat and Fedora based solutions.</li>
				</ul>
				
			<p>Red Hat also plans to join the Eclipse Internet of Things (IoT) Working Group, an open source community for the Internet of Things. Red Hat's participation in the Eclipse IoT community will 
				focus on enabling enterprise middleware for IoT solutions.</p>
			<p>"Red Hat is passionate about supporting open source communities. It is core to our business and strategy," explains Mike Piech, general manager, Middleware, Red Hat. "We believe providing 
				world-class developer tools is essential for our success, so it is natural for Red Hat to increase its commitment to the Eclipse community and participate in this leading open source developer tools platform."</p>
			<p>"Red Hat is a well-respected leader in the open source community. We believe having Red Hat increase their level of commitment to Eclipse is a great endorsement of the Eclipse tools platform, 
				our IoT technology, and the Eclipse community," states Mike Milinkovich, executive director of the Eclipse Foundation.</p>
			<p>More information about Red Hat's involvement in the Eclipse community can be found at <a href="http://jboss.org/eclipse">http://jboss.org/eclipse</a></p>
								
							
		<h3>About the Eclipse Foundation</h3>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and 
				managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, 
				and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at 
				<a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
			<p><i>Red Hat and JBoss are trademarks of Red Hat, Inc., registered in the U.S. and other countries.</i></p>									
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://www.redhat.com/en">Red Hat</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

