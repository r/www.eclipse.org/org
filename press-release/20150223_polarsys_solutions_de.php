<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "PolarSys stellt 5 neue Open Source Lösungen für Systems Engineering vor";
	$pageKeywords	= "eclipse, eclipsecon, polarsys";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
			<p><i>Zielsetzung der Working Group ist die Verbesserung des Entwicklungsprozesses für
eingebettete Systeme durch die Anwendung von “open innovation” Konzepten</i></p>
			<p>Die Eclipse Foundation und PolarSys, ein Industrie Konsortium (“Working Group”) innerhalb der Eclipse Foundation,
			liefert nun 5 neue Lösungen für die Entwicklung von eingebetteten Systemen. Diese neuen Lösungen zielen auf
			die Bereiche Systems- und Software Engineering, Software Entwicklung, Verifikation und Validierung ab. <a target="_blank" href="http://polarsys.org/solutions">Diese
			Lösungen werden ab März 2015 zum Download bereit stehen</a>.</p>
			<p>PolarSys ist ein Industriekonsortium innerhalb der Eclipse Foundation und hat einige der größten System Engineering
			Unternehmen der Welt als Mitglieder. So gehören z.B. Airbus, Ericsson, CEA LIST und Thales dem Konsortium an.
			Zielsetzung der Working Group ist die Verbesserung des Entwicklungsprozesses für eingebettete Systeme durch die
			Anwendung von “open innovation” Konzepten. Die Erwartung ist, dass die entwickelten Lösungen besser geeignet
			sind, die Anforderungen modernen Software Engineerings zu erfüllen. Dies wird durch bessere Erweiterbarkeit und
			Anpassbarkeit erreicht.</p>

			<p>Die neuen PolarSys Lösungen sind:</p>
				<ul>
					<li><b>Capella</b>, ein modell-basiertes graphisches Werkzeug zur Unterstützung des Architektur Designs. Capella enthält
					alle nötigen Werkzeuge zur Zusammenarbeit an gemeinsamen Architekturen zwischen den Ingenieursdisziplinen.
					<li><b>Papyrus</b>, die PolarSys Lösung zur Bearbeitung von SysML und UML Modellen. Es basiert auf der robusten Eclipse
					Plattform sowie anderen PolarSys und Eclipse basierten Lösungen zur Einbindung einer C/C++ Entwicklungsumgebung,
					Source Code Verwaltungssystemen wie z.B. git sowie Reporting Komponenten.<br>
					Durch seine integrierte Unterstützung für SysML und UML ist Papyrus hervorragend geeignet für das
					Model-Driven-Engineering (MDE) und das Model-Based-Systems-Engineering (MBSE). Um Papyrus herum
					gibt es bereits ein Ökosystem, das Papyrus-basierte Produkte und Support anbietet.</li>
					<li><b>Titan</b>, ein Toolset für die Testautomatisierung. Es unterstützt die Entwicklung und Ausführung von Tests
					sowie die Analyse der Resultate. Titan ist eine komplette Umgebung für TTCN-3 (Testing and Test Control Notation)
					und somit hervorragend geeignet für “ Grey-Box” und “Black-Box” Tests von Komponenten, Systemen und
					Modellen.</li>
					<li><b>C/C++ für Linux</b>, basiert auf der bekannten Eclipse CDT Entwicklungsumgebung, die in der Embedded
					Systems Entwicklung weit verbreitet ist. CDT läßt sich mit sehr vielen Linux Distributionen einsetzen und bietet
					Integrationen mit Werkzeugen wie GCC, Clang, GDB. CDT arbeitet auch auf vielen nicht-Linux basierten
					eingebetteten Systemen.</li>
					<li><b>Trace Compass</b>, ein mächtiges Werkzeug für Analyse und Visualisierung von großen Log und Trace
					Dateien. Es wird eingesetzt von Ingenieuren und technischem Personal zur Diagnose von Problemen sowie
					zur Performance Verbesserung und besserem Verständnis.</li>
				</ul>
				
			<p>Alle PolarSys Lösungen basieren auf Technologien und Werkzeugen, die bereits in großen Entwicklungsteams
				zum Einsatz kommen. Alle Lösungen werden als Open Source Software unter der Eclipse Public License (EPL)
				bereitgestellt.</p>
			<p>„Die Entwicklung von modernen und innovativen Werkzeugen für das System Engineering muss von den innovativen
				System Engineering Unternehmen gesteuert werden.„Sagt Dominique Toupin, Sprecher der PolarSys Working
				Group und Produkt Manager bei Ericsson. “Wir glauben, dass die PolarSys Working Group den geeigneten
				Rahmen bietet, in dem die viele Unternehmen zusammen die Werkzeuge für das Systems Engineering der Zukunft
				entwickeln.“</p>
			<p>Vertreter des PolarSys Konsortiums werden die neuen Lösungen auf der Embedded World Konferenz in Nürnberg
				vom 24. – 26. Februar in Halle 4, Stand 160 vorstellen. Vertreter der Eclipse Foundation werden ebenfalls
				vertreten sein: 1) Gael Blondell wird am 25. Februar, 13:30 den Vortrag „How to test complex systems” halten,
				Benjamin Cabe wird über „Building the Internet of Things with the Eclipse IoT stack and Java“ am 26. Februar sprechen.</p>
				
		<h3>Über die PolarSys Working Group</h3>
				<p>PolarSys ist ein Industriekonsortium innerhalb der Eclipse Foundation (Eclipse Working Group), die sich die Bereitstellung
				von Open Source Werkzeugen für die Entwicklung von eingebetteten System zur Aufgabe gemacht hat.
				Das Spektrum der PolarSys Werkzeuge soll alle Systems Engineering Aktivitäten wie Anforderungsmanagment,
				Modellierung, Simulation, Codierung sowie Test, Verifikation und Validierung abdecken.</p>
				<p>PolarSys wird von den folgenden Unternehmen gesteuert: Airbus, Atomic Energy und Alternative Energies
				Commission (CEA LIST), Ericsson und Thales. Weitere Informationen finden Sie auf <a target="_blank" 
				href="http://polarsys.org/">www.polarsys.org</a>.</p>
		<h3>Über die Eclipse Foundation</h3>
				<p>Eclipse ist eine Open Source-Community, deren Projekte sich auf die Entwicklung einer offenen Entwicklungsplattform
				konzentrieren, bestehend aus erweiterbaren Frameworks, Tools und Laufzeitumgebungen für die
				Entwicklung, Verteilung und Verwaltung von Software über ihren gesamten Lebenszyklus hinweg.</p>
				<p>Es handelt sich dabei um ein großes, lebhaftes Umfeld aus maßgeblichen Technologieanbietern, innovativen Startup-
				Unternehmen, Universitäten, Forschungsinstitutionen und Einzelpersonen, die die Eclipse-Plattform erweitern,
				ergänzen und unterstützen.</p>
				<p>Die Eclipse Foundation ist eine Non-Profit-Organisation und wird von ihren Mitgliedern unterstützt. Weitere
				Informationen über Eclipse und die Eclipse Foundation finden Sie auf <a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
				
			<p><b>Kontaktinfo:</b><br>
				Ralph Müller<br>
				Eclipse Foundation Europe GmbH<br>
				ralph.mueller@eclipse.org<br>
				Tel.: +49 6251 860 6413</p>
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://polarsys.org/">PolarSys Website</a></li>	
				<li><a target="_blank" href="http://polarsys.org/solutions">PolarSys Solutions</a></li>	
				<li><a href="https://www.eclipse.org/org/press-release/20150223_polarsys_solutions.php">English Version</a></li>	
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

