<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Eclipse Science Advances Open Source Technology for Scientific Research";
$pageKeywords	= "eclipse, science, open source, eclipse foundation";
$pageAuthor		= "Ian Skerrett";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
		# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p>Ottawa, Canada - October 18, 2017 - The Eclipse Science Working Group, a working group of the Eclipse Foundation, today announced the new releases of five open source projects used by the scientific research community to advance and simplify the software used by science projects, like genomic, astrophysics, nuclear simulations, etc. The new project releases are part of an annual release train that is managed by the Science WG.</p>

<p>The Eclipse Science Working Group is a collaboration of national laboratories, universities, industry and individuals, including Oak Ridge National Lab, Diamond Light Source, Itema, Kichwa Coders and others. The participants work together to advance scientific software development through an open source community. In total the Eclipse Science WG consists of 11 open source projects, 15 member organizations, 32 open source developers and over 1.5 MLOC that are focused on creating open source software for scientific applications.</p>

<p>This is the second year the Science WG has coordinated a release train. This year the new project releases include:</p>

<ol>
<li><a href="https://wiki.eclipse.org/Triquetrum/Overview" target="_blank">Eclipse Triquetrum</a>, an open platform for managing and executing scientific workflows. The goal of Triquetrum is to support a wide range of use cases, ranging from automated processes based on predefined models, to replaying ad-hoc research workflows recorded from a user's actions in a scientific workbench UI.  The new release includes additional Ptolemy actors and support of actor-oriented classes. </li>
<li><a href="https://www.eclipse.org/january/" target="_blank">Eclipse January</a>, is a set of libraries for handling scientific numerical data in Java. Eclipse January is inspired by the Python library, NumPy. The new Eclipse January release now support units of measurement metadata and a new set of APIs that makes it easier for users to get started.</li>
<li><a href="https://projects.eclipse.org/projects/science.eavp" target="_blank">Eclipse EAVP</a>, is a scientific visualization framework that includes a service based architecture that supports such diverse capabilities as mesh editing, 2D plotting, and 3D geometry editing. The new release of Eclipse EAVP integrates Eclipse January data for geometry, dsl and form data structures and consolidates 2D plotting series selection UI.</li>
<li><a href="https://www.eclipse.org/ease/" target="_blank">Eclipse EASE</a>,  is a scripting environment for the Eclipse IDE. The new release integrates the Py4J engine so now users are able to run and debug python code on a native python installation. Eclipse EASE also now includes new UnitTest framework which allows developers to write scripted tests in JUnit style.</li>
<li><a href="https://projects.eclipse.org/projects/science.texlipse" target="_blank">Eclipse TeXlipse</a>, provides an Eclipse extension to support LaTex projects, so that document preparation can be incorporated into the normal Eclipse development activities. General LaTeX users will also find the advanced editing and automatic document generation features of TeXclipse provide a compelling alternative to other LaTeX environments. TeXclipse was an existing project that has recently moved to the Eclipse Foundation. This will be the first release focuses on ensuring the project follows the Eclipse Foundation IP policies, the project source code has been moved to Eclipse Foundation infrastructure and a new installers was created to simplify installation.</li>
</ol>
<p>The new releases are available today from the project web site. More details about the Eclipse Science Working Group is available at https://science.eclipse.org/</p>

<h3>About the Eclipse Foundation</h3>

<p>The Eclipse Foundation is a not-for-profit organization that supports a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects with the Eclipse Foundation, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. Eclipse Foundation Working Groups allow organizations to collaborate on building innovative new technology to meet specific industry needs.  More information is available at eclipse.org.</p>

  </div>
	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://science.eclipse.org/news#node-426">Eclipse Science Simultaneous Release</a></li>
                <li><a target="_blank" href="https://twitter.com/EclipseScience">Follow Eclipse Science on Twitter</a></li>
				<li><a target="_blank" href="https://science.eclipse.org/">Eclipse Science</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

