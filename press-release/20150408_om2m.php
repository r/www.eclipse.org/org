<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Announces First Release of Eclipse OM2M Project";
	$pageKeywords	= "eclipse, eclipsecon, om2m";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>
				
			
			<p><i>Open source project focuses on implementing oneM2M Standard</i></p>
				
			<p><b>Ottawa, Canada – April 8, 2015</b> – The Eclipse Foundation and the Eclipse IoT Working Group announce the first release of the <a href="https://www.eclipse.org/om2m/">Eclipse OM2M</a> open source project. OM2M, led by developers from LAAS-CNRS, implements the ETSI SmartM2M standard and plans to migrate to the new oneM2M standard.</p>
			<p>OM2M is a standardized service platform that implements critical service capabilities required for M2M and IoT applications. It allows these services to be implemented independently of the network and the underlying hardware environment, making it easy for M2M and IoT developers to develop applications that integrate different types of devices and networking protocols.</p>
			<p>OM2M 0.8 implements the ETSI SmartM2M standard. Later this year, OM2M will migrate the implementation to the global oneM2M standard. oneM2M is a global standards organization involving over 200 companies, including the key regional ICT standards bodies ARIB, ATIS, CCSA, ETSI, TIA, TTA and TTC, and the leading industry consortia Broadband Forum, Continua, HGI, Next Generation M2M Consortium and the OMA. oneM2M released its first specifications in January 2015.</p>
			<p>A key issue for IoT is interoperability between different devices that use different protocols. This is particularly important for industries such as eHealth, Industrial Automation, and Home. OM2M 0.8 supports out-of-the-box integration and protocol interoperability with HTTP and CoAP enabled devices. OM2M’s extensible framework allows for easy integration with devices using protocols such as Zigbee, Zwave, 6LoWPAN, Modbus, and more.</p> 
			<p>OM2M is a platform composed of a set of Java and OSGi services making it very easy to extend and customize.  The services are exposed by a RESTful API, providing primitive procedures for machine authentication, resources discovery, applications registration, containers management, synchronous and asynchronous communications, access rights authorization, group management, and re-targeting.</p>
			<p>"We are very pleased to see the Eclipse OM2M project move to implement the new oneM2M standard," explains Omar Elloumi, chair of oneM2M Technical Plenary. "oneM2M has been very well received by the ICT industry and the availability of high quality open source implementations will increase adoption and deployment of oneM2M."</p>
			<p>"Our goal for OM2M is to make it the industry reference implementation of oneM2M," describes Mahdi Ben Alaya, co-leader of the OM2M project and PhD student from LAAS-CNRS. "We believe the combination of open standards and open source implementations accelerate adoption of key technology for M2M and IoT applications. Our expectation is that oneM2M and OM2M will make it easier to facilitate the deployment of industry solutions across heterogeneous networks and devices."</p> 
			<p>OM2M 0.8 is available for <a href="https://www.eclipse.org/om2m/">download</a> today. The technology is available royalty-free under the Eclipse Public License (EPL). Detailed documentation is available and feedback and contributions are encouraged on the project's forums.</p>
							
							
		<h3>About the Eclipse Foundation</h3>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and 
				managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, 
				and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at 
				<a href="https://www.eclipse.org/">www.eclipse.org</a>.</p>
												
				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipse.org/om2m/">Download OM2M</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

