<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse Foundation Announces Ericsson as a Strategic Member";
  $pageKeywords = "eclipse, ericsson, member";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  		<p><b>February 1, 2016</b> &mdash; The Eclipse Foundation is pleased to announce that 
  		<a target="_blank" href="http://www.ericsson.com/">Ericsson</a> has become a strategic member of the 
  		Eclipse Foundation. Ericsson has been an active member in the Eclipse community, leading a number of open source projects and industry working groups. 
  		As a strategic member they will take a seat on the Eclipse Foundation Board of Directors.</p> 
		
  		<img align="left" src="/org/images/ericsson_logo.png"><p>Ericsson is a world leader in the rapidly changing environment of communications technology – providing equipment, software, and services to enable 
  		transformation through mobility. Forty percent of global mobile traffic runs through networks supplied by Ericsson and more than 1 billion 
  		subscribers around the world rely on networks managed by Ericsson. Ericsson’s leadership in technology and services has been a driving force 
  		behind the expansion and improvement of connectivity worldwide.</p>  
		
  		<p>Ericsson makes extensive use of the Eclipse IDE and Eclipse modeling tools within their internal development teams. Thousands of users rely on 
  		Eclipse-based tools and PolarSys Solutions to build Ericsson products.</p> 
		
  		<p>Ericsson’s involvement in the Eclipse community includes:</p> 
  		
  		<ul>
			<li>Leading four Eclipse projects - CDT, EGerrit, Titan, and Trace Compass - that provide important tools for embedded developers and system engineers</li> 
			<li>Leading two Eclipse working groups – Leading the Polarsys Working Group, an industry collaboration to create open source tools for embedded software
  		developers and founding member of the Papyrus Industry Consortium, an industry collaboration to create an ecosystem around the Papyrus modeling platform</li>
			<li>Contributing  10 million lines of code to various Eclipse projects</li>
			<li>Providing significant funding to tool suppliers and researchers to improve the Eclipse platform</li>
		</ul>
  		
  		<p>"The Eclipse Foundation is now an organization where end-user companies can drive advanced tool technology. We have been very successful at Eclipse creating 
  		open source communities that help advanced the innovation and quality of tools Ericsson requires to build innovative solutions," explains Dominique Toupin, 
  		manager of Engineering and IT tools at Ericsson. "By becoming a strategic member, we hope to influence the Eclipse Foundation and encourage other end-user 
  		organizations to participate in a vibrant ecosystem where end-user companies, tool suppliers, and research work together to create significant open innovations."
		
  		<p>“Open source provides an ideal mechanism for end-user organizations to have direct influence on how critical technology is developed. Ericsson has been a 
  		leader in creating successful open source communities that deliver technology used by system engineering and embedded development teams,” explains Mike 
  		Milinkovich, executive director of the Eclipse Foundation. “As software becomes more critical for all industries, we believe open source and Eclipse 
  		working groups provides a significant opportunity for organizations to have a direct impact on creating innovative software platforms. Ericsson is a 
  		great example of how a large organization can use open source to meet their software requirements.” 
		
		<p>Ericsson joins 13 other companies that are strategic members of the Eclipse Foundation, including Bosch, CA, CEA List, Codenvy, Google, IBM, Innoopract, itemis AG, 
  		Obeo, OpenText, Oracle, Red Hat, and SAP.</p>
  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
  				<li><a href="http://www.eclipse.org/membership/exploreMembership.php#allmembers">Eclipse Members</a></li>
  				<li><a target="_blank" href="http://www.ericsson.com/">Ericsson</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
