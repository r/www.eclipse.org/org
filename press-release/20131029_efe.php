<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse Foundation Creates European Subsidiary to Support Growing Open Source Community";
	$pageKeywords	= "eclipse, eclipse foundation, subsidiary, europe";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

<p><b>Zwingenberg, Germany – October 29, 2013</b> -- The Eclipse Foundation has announced the creation of a European subsidiary, Eclipse Foundation Europe GmbH (EFE), located in Zwingenberg (Hessen), Germany. EFE has been established to help even better support the large and growing Eclipse open source community in Europe.</p>
<p>Eclipse is a global open source community with over 250 projects, more than 1000 committers, over 200 member organizations supporting the operation of the Foundation, and with an estimated 8 million developers using Eclipse. One-quarter of the Eclipse user community is located in Europe and half of the member companies are based in Europe.</p> 
<p>The new EFE has local staff members to support the European community. Staff will support members in EU-funded projects such as ITEA2, provide appropriate development and dissemination services as a project partner, and participate in European-based associations such as the German Bitkom to create awareness of open innovation, open source business models, and the Eclipse platform.</p>
<p>Ralph Mueller has been appointed the Managing Director of Eclipse Foundation Europe. Mr. Mueller is a 30-year veteran in the IT industry. He has previously worked at IBM, Object Technology International, and Siemens-Nixdorf; most recently he has been the Director of European Ecosystem at the Eclipse Foundation.</p>
<p>“Europe has been a leader in the adoption of open source technology, and in particular Eclipse,” explains Mr. Mueller. “The creation of Eclipse Foundation Europe will allow us to accelerate our push to promote the Eclipse Working Groups as a way to foster industry collaboration.”</p>
<p>More information about Eclipse Foundation Europe is available in this <a href="http://eclipse.org/org/press-release/20131029_efe_faq.php">FAQ</a>.</p>

<h2>About Eclipse</h2>
<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>
<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="http://eclipse.org/">www.eclipse.org</a>.</p>
				

	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/org/press-release/20131029_efe_faq.php">FAQ</a></li>
				<li><a href="http://eclipse.org/org/press-release/20131029_efe_fr.php">French</a>
				<li><a href="http://eclipse.org/org/press-release/20131029_efe_de.php">German</a>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

