<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Annual Eclipse Release Train Ready for Download";
	$pageKeywords	= "eclipse, kepler, open source, release train";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

<p><i>Eclipse Kepler features 71 projects and over 58 million lines of code</i></p>

			<p><b>Ottawa, Canada – June 26, 2013</b> – The Eclipse Foundation is pleased to announce the availability of the annual Eclipse release train. Each year, the Eclipse community of open source projects coordinates a major release at the end of June. The coordinated release allows users and adopters of Eclipse technology to update to new versions of Eclipse projects all at one time.</p>
			<p>The Kepler release features 71 different open source project teams involving 420 developers from 54 different organizations and over 58 million lines of code. The coordinated release demonstrates that open source development processes are very effective for large-scale distributed development.</p>
			<p>“The Eclipse release trains are very important to the success and growth of the entire Eclipse ecosystem,” explains Mike Milinkovich, executive director of the Eclipse Foundation. “Users and adopters of Eclipse technology know they can rely upon a predictable annual release schedule, so organizations are confident about using Eclipse technology as their software development platform. Eclipse is a great example of how open source software has changed the software industry for the better.”</p>
			<p>Highlights of the Kepler release include the following:</p>
			<p><b>Support for Java EE 7</b> – The <a href="http://www.eclipse.org/webtools/">Eclipse Web Tools Project (WTP) 3.5</a> release adds support for the recently released Java EE 7, including support for JPA 2.1, JSF 2.2, JAX-RS 2.0, Servlet 3.1, EJB 3.2, Connector 1.7, App Client 7.0 and EAR 7.0. Eclipse WTP project and code generation wizards, content assist and validation have been updated so Java developers can easily create, debug and deploy Java EE 7 compliant applications.</p>
			<p><b>New Business Process Management Suite</b> – The <a href="http://www.eclipse.org/stardust/">Eclipse Stardust 1.0</a> release provides a business process management engine and tools. Eclipse Stardust includes a modeling environment to create and debug workflow models, a process engine to execute BPM applications, a web portal for browser-based execution and monitoring of business processes and an Eclipse BIRT-based reporting component for runtime monitoring and reporting BPM applications.</p>
			<p><b>Scalability and Usability of Web-based IDE</b> – <a href="http://www.eclipse.org/orion/">Orion 3.0</a> release features improvements to the usability and scalability of the Orion web-based IDE. Orion can now be easily installed as a WAR file to a Java application server, making easier to deploy to cloud services.   Orion usability has also been improved to include direct file navigation in the editor, new key bindings (vi and Emacs), auto-save/auto load and a new look and feel.</p>
			<p><b>New Support for Big Data</b> – <a href="http://www.eclipse.org/birt/phoenix/">Eclipse BIRT 4.3</a> introduces support for the popular MongoDB and Cassandra databases, and allow for easy integration of the BIRT visualization capabilities into big data applications. This new support is in addition to the existing Hadoop support provided by BIRT.</p>
			<p><b>Better Integration for Code Reviews</b> – <a href="http://www.eclipse.org/mylyn/">Mylyn 3.9</a> now makes it a lot easier to conduct code review within Eclipse. A new navigator view that is integrated with Gerrit shows the structured view of all the files and comments of a review. </p>
			<p><b>Improved Integration with Maven for Java EE Developers</b> – New support for <a href="http://www.eclipse.org/m2e-wtp/">Maven integration with the Eclipse Web Tools Project (WTP)</a> provides a set of connectors that add Maven support for Java EE related Eclipse projects, including war, ejb, ear and rar projects.</p>
			<br/>
			<p>All of the projects participating in the Kepler release train are now available for <a href="http://eclipse.org/kepler/">download</a>. Twelve packages, based on different developer profiles, make it easy to download the new release.</p>

			<p><b>About the Eclipse Foundation</b></p>
			<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>
			<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="http://www.eclipse.org/">www.eclipse.org</a>.</p>			


	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/kepler/">Kepler</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

