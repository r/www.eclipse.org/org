<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Day India 2015";
	$pageKeywords	= "eclipse, day, india, bangalore";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

				<p>The Eclipse Foundation is pleased to announce <a target="_blank" href="http://eclipsedayindia.mybluemix.net/">Eclipse Day India 2015</a>. 
				Eclipse Days are events designed to create opportunities for the local Eclipse community to explore, share, and collaborate on the latest 
				ideas and information about Eclipse and its members. This one-day event consists of technical talks, demos, posters, and is intertwined with 
				great networking opportunities.</p>
				
			<h2>Event Details</h2>	
				<p><b>Date:</b> Saturday, August 29, 2015</p>
				<p><b>Venue:</b> Robert Bosch Engineering and Business Solutions<br>                                                                                  
                123 Industrial Layout, Hosur road, Koramangala<br>                                                              
                Bangalore, Karnataka - 560 095</p>         
				
				<p>Eclipse Day India 2015 will feature keynote from Mike Milinkovich, Eclipse Foundation, and will provide a glimpse into the Internet of Things (IoT)
				 and the industry needs.</p>
				
			<h2>Special Guests</h2>
				<ul>
				    <li><b>Mike Milinkovich</b> has been involved in the software industry for over thirty years, doing everything from software engineering, 
				to product management to IP licensing. He has been the Executive Director of the Eclipse Foundation since 2004. In that role he is responsible 
				for supporting both the Eclipse open-source community and its commercial ecosystem. Prior to joining Eclipse, Mike was a vice president in Oracle's 
				development group. Other stops along the way include WebGain, The Object People, IBM, Object Technology International (OTI) and Nortel. Mike sits 
				on the Board of the Open Source Initiative (OSI), on the Executive Committee of the Java Community Process (JCP), and is an observer and past 
				member of the Board of OpenJDK. Mike spends many of his evenings playing with IoT gadgets, and getting Eclipse software running on them.</li>
				    <li><b>Daniel Megert</b> leads the Platform and the JDT subprojects and represents the project in the Eclipse Planning Council, 
				and is also a member of the Eclipse Architecture Council. Dani worked at OTI and now works at IBM Research GmbH. He is one of the 
				initial Eclipse Project committers. His interests include user interface and API design, editors, software quality, and performance.</li>
				    <li><b>Srikanth Sankaran</b> is currently working in Java Compiler group at Oracle. Prior to this, he was a Lead/Architect at IBM 
				and for the past 5 years has worked on various components of JDT/Core, most notably on the Java compiler,code assist, Java model and 
				DOM/AST projects. Srikanth has 20+ years of experience mostly in R&D organizations producing Compilers & Language tools with 
				contributions in the areas of C/C++ compilers, debuggers, profilers, class browsers, IDEs, memory analysis tools, static analysis 
				tools and the like.</li>
					<li><b>Stephan Hermann</b> received his Ph.D. at Technische Universit&auml;t Berlin in 2002. Around that time he started developing the concepts 
				of Object Teams, the language OT/J and its tools; he is the lead of the Eclipse Object Teams Project. He is also a committer on JDT/Core and 
				JDT/UI where his pet project is improving the null pointer analysis. In 2010 he joined GK Software, where he promotes a model driven approach 
				and develops in-house tools to support this approach.</li>
				</ul>
				
			<h2>Registration</h2>
				<p>Eclipse day India 2015 is a free event, and all are welcome!</p> 
				<a class="btn btn-warning" target="_blank" href="http://eclipsedayindia.mybluemix.net/">Register here</a>		
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://eclipsedayindia.mybluemix.net/">Eclipse Day India 2015</a></li>	
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

