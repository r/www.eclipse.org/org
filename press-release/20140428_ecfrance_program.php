<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "EclipseCon France 2014 - Program and Keynotes Announced";
	$pageKeywords	= "eclipse, foundation, eclipsecon, france, keynote, program, 2014";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – April 28, 2014</b> - The Eclipse Foundation is pleased to announce the program and keynotes for the second <a target="_blank" href="https://www.eclipsecon.org/france2014/">EclipseCon France</a>. 
				Planned for June 18-19 in Toulouse, this technical conference focuses on the latest innovations in the Eclipse community, Eclipse working groups 
				and the software industry. This year, we will also be hosting an Unconference on June 17.</p>

		<p>The EclipseCon France <a target="_blank" href="https://www.eclipsecon.org/france2014/eclipsecon/program/sessions/accepted">program</a> has been announced. It includes 31 talks and 8 workshops over two days, and features sessions on</p>
			<ul>
				<li>Eclipse in the Industry</li>
				<li>Mobile and Web Development</li>
				<li>IoT and Embedded</li>
				<li>Tools in Action</li>
				<li>And much more!</li>
			</ul>	

	<h2>Keynote Speakers</h2>
		<p>We are also pleased to announce the keynote speakers:</p>
			<ul>
				<li><b>Gaëtan Séverac</b>, Naïo Technologies - <a target="_blank" href="https://www.eclipsecon.org/france2014/session/how-i-accidentally-created-business">How I Accidentally Created a Business</a></li>
				<li><b>Manfred Broy</b>, Technische Universität München - <a target="_blank" href="https://www.eclipsecon.org/france2014/session/cyber-physical-systems-road-connected-assistance-and-global-services">Cyber-Physical Systems - A Road to Connected Assistance and Global Services</a></li>
			</ul>

		<p>Registration is now open. <a target="_blank" href="https://www.eclipsecon.org/france2014/registration">Register now</a> and join us in Toulouse!</p>
				

</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2014/">EclipseCon France</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2014/unconference">Unconference</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2014/keynote-speakers">Keynote Speakers</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2014/eclipsecon/program/sessions/accepted">Program</a></li>
				<li><a target="_blank" href="https://www.eclipsecon.org/france2014/registration">Registration</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

