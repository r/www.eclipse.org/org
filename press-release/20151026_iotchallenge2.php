<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse IoT Announces the Second Edition of the Open IoT Challenge";
  $pageKeywords = "eclipse, iot challenge";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  <p><b>Ottawa, Canada - October 26, 2015</b> - The Eclipse Internet of Things (IoT) Working Group, hosted at the Eclipse Foundation, is pleased to announce the 
  		<a href="http://iot.eclipse.org/open-iot-challenge">Open IoT Challenge</a>. 
  		The second edition of this challenge encourages IoT enthusiasts and developers to build innovative solutions for the Internet of Things using open standards and open source technology. 
  		The previous edition of the Challenge resulted in innovative solutions implementing a connected car monitoring system, home automation, greenhouse automation, and others. Once again, participants 
  		will be supported with hardware development kits, technical support, and prizes for the winners. The Challenge is sponsored by bitreactive, Eurotech, IS2T, Red Hat, and Zolertia.</p>

		<p>The <a href="http://iot.eclipse.org/">Eclipse IoT Working Group</a> is a community of twenty IoT open source projects and over thirty participating companies. The goal of Eclipse IoT is to provide a set of open 
  		source IoT technology that is used by the IoT industry to build open, commercial and enterprise IoT solutions.</p> 
		
		<p>The Open IoT Challenge encourages participants to build IoT solutions using any or all of the following technologies:
		<ul>
  		<li>IoT open standards, such as MQTT, CoAP, Lightweight M2M, DNS-SEC, OneM2M</li>
		<li>Eclipse IoT projects, such as Eclipse Kura, Leshan, SmartHome, Mosquitto, Paho, and others listed at <a href="http://iot.eclipse.org/">iot.eclipse.org</a></li>
		<li>Other open source projects</li>
		<li>Commercial hardware and services that interact with open-source software and open standards for IoT</li>
  		</ul>
		
		<p>Participants are required to submit a proposal for their solution before November 23. A panel of judges will select a shortlist of participants that will receive hardware 
  		development kits. All participants will then have until the end of February to build their IoT solutions. The winners will be announced the beginning of March.</p>
		
		<p>Additional information about the Challenge is available at <a href="http://iot.eclipse.org/open-iot-challenge">http://iot.eclipse.org/open-iot-challenge</a>. Information about 
  		the the previous Open IoT Challenge, including the winners, is available <a href="http://iot.eclipse.org/open-iot-challenge/first-edition">here</a>. 

  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://iot.eclipse.org/open-iot-challenge">Open IoT Challenge 2.0</a></li>
				<li><a href="http://blog.benjamin-cabe.com/2015/04/08/announcing-the-open-iot-challenge-winners">Winners of First Open IoT Challenge</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
