<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include ("_projectCommon.php"); // All on the same line to unclutter the user's
                                // desktop'

//
// Begin: page-specific settings. Change these.
$pageTitle = "The Eclipse Foundation Launches The Eclipse Cloud Development Tools Working Group for Cloud Native Software";
$pageKeywords = "eclipse, Eclipse Foundation";
$pageAuthor = "Mike Milinkovich";

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

			<blockquote><em>&ndash; New Working Group includes founding members&nbsp;<br />
Broadcom, IBM, Red Hat, SAP, and others &ndash;</em></blockquote>

<p><strong>OTTAWA &ndash; October 8, 2019 </strong>&ndash; The Eclipse Foundation today announced the launch of the Eclipse Cloud Development Tools Working Group (ECD WG), a vendor-neutral open source collaboration that will focus on development tools for and in the cloud. The ECD WG will drive the evolution and broad adoption of emerging standards for cloud-based developer tools, including language support, extensions, marketplaces, and developer workspace definition. Founding members of the ECD WG include Broadcom, EclipseSource, Ericsson, IBM, Intel, Red Hat, SAP, Software AG, and Typefox among many others.&nbsp;</p>

<p>&ldquo;Our own survey of developers has shown that cloud native applications are of critical importance for today&rsquo;s enterprises,&rdquo; said Mike Milinkovich, executive director of the Eclipse Foundation. &ldquo;More than 80 percent of the developers surveyed have plans to create cloud native applications within the next 12-18 months. It is this demand that drove the formation of this working group.&rdquo;</p>

<p>The ECD WG encompasses a broad portfolio of open source cloud development projects including <a href="/che/">Eclipse Che</a>, <a href="https://www.theia-ide.org/">Eclipse Theia</a>, <a href="/codewind/">Eclipse CodeWind</a>, <a href="https://www.dirigible.io/">Eclipse Dirigible</a>, <a href="https://projects.eclipse.org/projects/ecd.sprotty">Eclipse Sprotty</a>, and many more. The ECD WG will work to accelerate the adoption of Cloud Integrated Development Environment (IDE) and container-based workspace management through the adoption of standards, the engagement with third party developer tool providers, and through the promotion of Eclipse Foundation projects to cloud developers.&nbsp;&nbsp;</p>

<p>In addition, the group will explore the impacts and optimizations for running developer tools in a cloud environment, including scale out / scale-to-zero execution of compute-intensive tasks like CI and testing. A very timely example of these efforts includes the recent release of Eclipse Che 7, the world&rsquo;s first Kubernetes-native IDE, on September 17, 2019. Che 7 is specifically designed for creating cloud native enterprise applications. Interested parties can read more about this release <a href="/che/">here</a>.&nbsp;&nbsp;</p>

<p>Home to the Eclipse Desktop IDE, the Eclipse Foundation has a proven track record of enabling developer-focused open source software collaboration and innovation earned over 15 years. As one of the world&rsquo;s most popular desktop development environments, the Eclipse IDE is downloaded over 2 million times per month, and is a critical development environment for more than 4 million active users. Overall, the Foundation&rsquo;s more than 370 collaborative projects have resulted in over 195 million lines of code &mdash; a $10 billion shared investment.</p>

<p>To learn more about getting involved with the ECD WG, view the <a href="/org/workinggroups/eclipse_cloud_development_charter.php">Charter</a> and <a href="/org/workinggroups/wgpa/eclipse_cloud_development_tools_participation_agreement.pdf">ECD Working Group Participation Agreement (wgpa)</a>, or email us at membership@eclipse.org. You can also join the <a href="https://accounts.eclipse.org/mailing-list/ecd-tools-wg">ECD Tools</a> mailing list.&nbsp;</p>

<h2>Quotes from Eclipse Cloud Development Tools Working Group Members&nbsp;</h2>

<h3>EclipseSource</h3>

<p>&ldquo;As a service provider for the design and implementation of domain-specific tools, modeling tools, and IDEs, EclipseSource is very excited to actively participate in the ECDT working group,&rdquo; said Dr. Jonas Helming, principal software architect / general manager of EclipseSource Munich. &ldquo;In our customer projects around the globe, we can observe an increasing demand for web-based and cloud-based tooling. Quite a number of interesting technologies have already been emerged within the Eclipse ecosystem to support this use case, e.g. Eclipse Theia, Eclipse Che or the Graphical Language Server Protocol (GSLP). Coordinating these efforts in the scope of the working group will help contributors, service providers, and adopters to get the most benefit out of great open source technologies. We believe the Eclipse ecosystem has the potential to be a core ecosystem for web-based tooling in the upcoming decade as it is for desktop-based tools.&rdquo;</p>

<h3>IBM</h3>

<p>&ldquo;Application development is evolving rapidly to take advantage of the innovation in the multicloud ecosystem. Developer tools are critical in accelerating adoption of new technology and enabling enterprise developers in the modernization journey to the cloud,&rdquo; said Danny Mace, vice president, Hybrid Cloud App Platform, IBM. &ldquo;We are proud to join the Eclipse Cloud Development Working Group working to drive the evolution and broad adoption of cloud development tools. Working at Eclipse will expand tools communities for containerized applications on Kubernetes and offer developers the tools needed for cloud-based scenarios. Developers will benefit, as will our customers.&rdquo;&nbsp;</p>

<h3>Red Hat</h3>

<p>&ldquo;Developers today are building applications for the hybrid cloud. They are focused on the speed of collaboration and development in order to out-flank their competitors. However, the hybrid cloud operating environment can be complex, with Kubernetes, containers, microservices and service mesh&rsquo;s adding a host of new considerations to application design and implementation,&rdquo; says Brad Micklea, VP of Developer Tools and Advocacy at Red Hat. &ldquo;Red Hat is working with the Eclipse Cloud Development Working Group to provide new open source alternatives to these technology stacks while giving developers the tools they need to get their software to market faster.&rdquo;</p>

<h3>SAP</h3>

<p>&ldquo;For the past 5 years SAP has provided cloud development tools, while aligning with the latest technologies, standards, and open source trends,&rdquo; said Michael Wintergerst, senior vice president, head of SAP Cloud Platform Core. &ldquo;As a strategic member of the Eclipse Foundation, SAP actively participates in Eclipse Cloud Development projects. This participation includes an involvement in the Eclipse Che, Dirigible,Theia, and Orion projects. As a next step, the proposed Eclipse Cloud Development Tools Working Group aims to create a stimulating environment to collaborate on cloud development tools. The working group will increase the synergy between all relevant parties by creating shared concepts and standards, and foster further collaboration in an open environment.&rdquo;</p>

<h3>TypeFox</h3>

<p>&ldquo;At TypeFox&rsquo;s, we build software tools and IDEs for experts,&rdquo; says Dr. Jan K&ouml;hnlein, co-founder TypeFox. &rdquo;As a response to the growing demand to run these tools in the Cloud, we joined forces with Ericsson and initiated the open-source projects Theia, a framework for cloud-based or rich-client IDEs, and Sprotty for web-based diagramming. Moving these projects under the umbrella of the Eclipse Foundation allowed other big players to join in a common endeavor to build the solid foundation for the next generation of web-based tools. The Eclipse Cloud Development Tools Working Group stands for the commitment to keep these frameworks industrial strength, truly open-source and vendor neutral.&rdquo;</p>

<h2>About The Eclipse Foundation</h2>

<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable and commercially-focused environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 375 open source projects, including runtimes, tools, and frameworks for a wide range of technology domains such as IoT, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 275 members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter <a href="https://www.globenewswire.com/Tracker?data=r-dKnkzEx8J2jm68aMwsXND3DPYOl6p60tYwoWOkQRpJjLP_SKHpU_opBz5bi7JLO7Y2btHRJBvdyqqcTBtrLg==">@EclipseFdn</a>, <a href="https://www.globenewswire.com/Tracker?data=S12nqbeRNKuTWwBUeNUnEC9gFrlX34gxCEhP2Uu-2KuWYmqkXbIScweQ1ykqwa2QtAHlvDYJT8GyKpicKaq5uLxRrTZwh2HI7nQfCd01lmEj3UXrlRWmU3VLtBxzjMlq">LinkedIn</a> or visit <a href="https://www.globenewswire.com/Tracker?data=EsgUi61-f7vRM-pEF4XYjO2jYG6DqnLbsSPKrHZwkTiaAkCbjz8HlE68IUVSML_CiBt9S9b00GJWRIs3EmTELQ==">eclipse.org</a>.</p>

<p>Third-party trademarks mentioned are the property of their respective owners.&nbsp;</p>

<h2>Media Contact:</h2>

<p>Nichols Communications for the Eclipse Foundation, Inc.<br />
Jay Nichols<br />
jay@nicholscomm.com<br />
+1 408-772-1551</p>
  	</div>
</div>

EOHTML;

// Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);