<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse Simplifies Development of Internet of Things (IoT) Solutions with Open IoT Stack for Java";
	$pageKeywords	= "eclipse, foundation, iot, stack, java";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

	<p><b>The News</b></p>
	<p>The Eclipse IoT community is making it easier for Java developers to connect and manage devices in an IoT solution by delivering an 
				Open IoT Stack for Java.  Based on open source and open standards, the Open IoT Stack for Java simplifies IoT development 
				by allowing developers to re-use a core set of frameworks and services in their IoT solutions.</p>
				
	<p><b>Why It Matters</b></p>
		<ul>
			<li>The Internet of Things is a tremendous opportunity for organizations and developers to create new solutions that connect devices to the Internet.</li> 
			<li>The complexity of creating IoT solutions makes it difficult for developers to deliver new innovative solutions. The current state requires developers to create proprietary and closed solutions that are locked into a particular vendor, don’t easily interoperate, and are slow to deliver.</li> 
			<li>There are a number of emerging IoT standards (MQTT, CoAP, Lightweight M2M) that will make it possible for devices to connect and interoperate. Open source implementations of these standards will make it easier for developers to adopt these standards.</li> 
			<li>There are 9 million Java developers that will bridge the world between enterprise and embedded development. Providing open source Java frameworks and services will make it easier for Java to become a language for IoT.</li> 
			<li>Similar to the way the Internet evolved to run on an open source foundation (Apache, Linux, etc), the IoT’s success will hinge on the establishment of open source technologies at its core that everyone can use to enable connectivity and interoperability.</li> 
		</ul>							
				
	<p><b>The Details</b></p>
		<ul>
			<li>The Eclipse Open IoT Stack for Java is a set of Java frameworks and OSGi services that make it easy to connect and manage IoT solutions. The Open IoT Stack for Java includes support for 1) popular IoT standards: OASIS MQTT, IETF CoAP and OMA Lightweight M2M (LWM2M), and 2) a set of services for building IoT Gateways.</li> 
			<li>A new version of Eclipse Kura 1.0 will be released at JavaOne. Eclipse Kura 1.0 provides a set of Java and OSGi service for building IoT Gateways.</li> 
			<li>A new project called Eclipse Leshan will be announced at JavaOne to support LWM2M. Leshan will provide a Java implementation of the LWM2M server.</li> 
			<li>In addition to the core Open IoT Stack, a set of industrial frameworks are available to accelerate the process of creating home automation and SCADA factory automation solutions.</li> 
			<li>The Open IoT Stack for Java is supported by a large community of companies, universities and research institutions, including:  2lemetry, Actuate, Bitreactive, Cisco, Deutsche Telekom, DC-Square, Eurotech, ibh Systems, IBM, LAAS-CNRS, openHAB, Ubuntu, Sierra Wireless, and 2lemetry.</li> 
		</ul>	

	<p><b>Open IoT Stack for Java Details</b></p> 
		<p>The Open IoT Stack for Java is a set of open source Java frameworks and OSGi services. It includes the following Eclipse IoT projects:</p>
			<ul>
				<li>Paho provides a Java implementation of the MQTT client and Moquette provides a Java MQTT broker.</li>
				<li>Californium provides a Java implementation of CoAP, including DTLS for IoT security.</li>
				<li>Leshan will provide a Java implementation of the Lightweight M2M standard for device management.</li>
				<li>Kura is a set of OSGi services for building IoT Gateways. It includes services for device management, application management, cloud connectivity and network configuration.</li>
				<li>Eclipse Smarthome is a set of Java and OSGi services for building smart home and assisted living solutions.</li>
				<li>Eclipse SCADA is a set of Java and OSGi services that implements many of the services required for a SCADA industrial automation system, including data acquisition, monitoring, data and event archival, visualization and value processing.</li>
				<li>Eclipse OM2M is an implementation of the ETSI M2M standard.  It provides a horizontal Service Capability Layer (SCL) that can be deployed in an M2M network, a gateway, or a device.</li>
			</ul>
				
		<b>More information</b>
			<p>Website: <a href="http://iot.eclipse.org/java/">iot.eclipse.org/java</a>
			<p>Demo: Eclipse Foundation will be demoing the Open Stack for Java at JavaOne booth #5615</p>
							
		<h3>Quotes from Community</h3>
			<p><b>Oracle</b><br/>
			At Oracle, we are working hard to make Java the number one platform for the Internet of Things. The Eclipse Foundation’s Open IoT Stack for Java is a fantastic and welcome example of Java developers building another innovative and diverse ecosystem, this time for IoT solutions.<br/>
			-	Benjamin Wesson, vice president, product development, Oracle</p>
			
			<p><b>IBM</b><br/>
			As the Internet of Things evolves, developers need greater access to technologies and standards to deliver innovative products and services to market. Our involvement in the Eclipse Paho project, alongside the Oasis standardization effort for MQTT, will accelerate the development of Internet of Things solutions across industries. IBM MessageSight and IBM Internet of Things Foundation beta are built on MQTT, enabling the open source community, and promoting flexibility and choice for clients.<br/> 
			-	Michael Curry, VP, IBM</p>
			
			<p><b>Eurotech</b><br/>
			Even before co-founding the Eclipse Foundation M2M (now IoT) Working Group, Eurotech embraced the open source philosophy. We have invested our development resources into creating and donating the Kura project to the community while also delivering it as a fully supported commercial offering through the Everyware Software Framework. We appreciate the innovations and contributions of the talented and generous Eclipse community and look forward to exciting enhancements and revolutionary projects in the coming days, months and years.<br/>
			-	Marco Carrer, VP of Software Development for Eurotech</p>
			
			<p><b>Deutsche Telekom</b><br/>
			Telekom has been an Eclipse Foundation solution member since July this year. We are convinced that we will take the development of our QIVICON Smart Home solution further forward with the support of the large developer community and its outstanding know-how. To do so we are integrating the Eclipse Smart Home project into our core platform. As we would like to promote Eclipse's work at the same time, we have not only joined the IoT working group but are also contributing to the Eclipse Smart Home project by actively participating in the open source project's development.<br/>
			-	Holger Knöpke, Head of Connected Home, Deutsche Telekom</p>
			
			<p><b>Actuate</b><br/>
			Internet of Things offers numerous opportunities for developers to create new solutions using data from devices connected to the Internet. The challenge is to establish a set of standards to make effective use of the many different sources of data. Eclipse’s new Open IoT Stack for Java will make it easier for the over 3.5 million BIRT developers to access, visualize and analyze IoT data to deliver actionable information to enhance customer experience and improve operations.<br/>
			-	Nobby Akiha, senior vice president of marketing at Actuate Corporation.</p>
			
			<p><b>dc-square</b><br/>
			The open IoT stack from Eclipse helps our HiveMQ customers to build their MQTT client applications on proven and rock stable solutions like Eclipse Paho and Eclipse Kura. This gives them the advantage to start from day one with their applications instead of implementing low-level wire protocols themselves.<br/>
			-	Dominik Obermaier, CTO, dc-square GmbH</p>
			
			<p><b>IBH Systems</b><br/>
			IBH SYSTEMS uses Eclipse SCADA as the strategic platform to realize ours customers’ projects. It can play to it strengths especially in environments where the customer wants to have full control over the software and be able to customize it in any way. Additionally our approach to modularization means that a developer only needs to use what he actually requires. This in turn allows other open source, or even commercial projects to reuse our high quality implementations of industrial protocols, like Modbus or IEC 60870-5-104, without the need of integrating the whole Eclipse SCADA stack.<br/>
			-	Jens Reimann, IBH Systems</p>
			
			<p><b>Bitreactive</b><br/>
			The Open IoT Stack for Java is an ideal platform to enable interoperability and reduce fragmentation. Our tool, Reactive Blocks supports application development with Eclipse IoT technologies like MQTT, Kura and CoAP since they provide proven and standardized solutions.<br/>
			-	Jone Rasmussen, General Manager of Bitreactive</p>
				
			<p><b>Canonical</b><br/>
			Ubuntu is collaborating with the Eclipse foundation on making sure developers can easily use any of their IoT solutions both on the cloud and the embedded space.<br/>
			-	Maarten Ectors, IoT Strategy Director of Canonical</p>
				
			<p><b>2lemetry</b><br/>
			2lemetry is excited by all the new developments that the Eclipse Open IoT Stack is making. It allows for quicker adoption of some of the best emerging technologies in IoT to date. <br/>
			-	Chris Chiappone, CTO of 2lemetry</p>
													
					
</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="http://iot.eclipse.org/java/">Open IoT Stack for Java</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

