<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "RFP Now Open for Eclipse IDE Enhancements Funded by Friends of Eclipse Donations";
	$pageKeywords	= "eclipse, friends, development project, proposals";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

			<p>The Eclipse Foundation is pleased to announce that we are initiating our request for proposals for new development projects targeted directly at enhancing and improving the 
				Eclipse IDE/Platform. This is the first such request for proposals based on our new <a href="https://www.eclipse.org/contribute/dev_program.php">Friends-Enabled Eclipse 
				IDE/Platform Enhancements Program</a>, or FEEP.</p> 
			
			<p>The funding for these FEEP projects has come exclusively from donations made by individuals to <a href="https://www.eclipse.org/donate/">Friends of Eclipse</a>, 
				and it is part of our commitment to use 100% of the funds raised through donations to enhance and improve the Eclipse IDE/Platform.</p>   
			
			<p>We are inviting proposals from the broader Eclipse community, and hope that many of our members and committers will participate.</p> 
			
			<p>The list of projects being funded is available at <a href="https://projects.eclipse.org/development-efforts">www.eclipse.org/development-efforts</a> 
			
			<p>The deadline for submitting proposals for this first cycle is Tuesday, November 10.  We expect to award contracts to the winning bidders the week of November 16, 
				with development expected to begin shortly thereafter. We are also committed to posting a list of the winning bidders, along with the contract value, at the end of the bid process.</p> 
			
			<p>Our thanks to everyone who has donated to the Friends of Eclipse, and hope that you will continue to support our ongoing process of improvement and enhancement.</p>  

				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="https://www.eclipse.org/contribute/dev_program.php">FEEP Program</a></li>
				<li><a href="https://www.eclipse.org/donate/">Friends of Eclipse</a></li>	
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

