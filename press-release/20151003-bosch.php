<?php
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ("_projectCommon.php"); // All on the same line to unclutter the user's desktop'

//
                                // Begin: page-specific settings. Change these.
$pageTitle = "Eclipse Foundation announces Bosch as a strategic member";
$pageKeywords = "eclipse, eclipsecon, europe, 2015, germany, bosch";
$pageAuthor = "Eric Poirier";

// Add page-specific Nav bars here
// Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
// $Nav->addCustomNav("My Link", "mypage.php", "_self");
// $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

// End: page-specific settings
//

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>

  <p>Ludwigsburg, Germany – November 3, 2015 – The Eclipse Foundation is pleased to announce that Bosch has become a strategic member of the Eclipse Foundation. Bosch has been a long-term solutions member of the Eclipse Foundation and is actively participating in the Eclipse Automotive Working Group and the Eclipse Internet of Things (IoT) Working Group. As a new strategic member, Bosch will now have a representative on the Eclipse Board of Directors and will be leading a number of open source projects at Eclipse. More than 10,000 Bosch engineers use the Eclipse Integrated Development Environment (IDE).</p>
<p>Bosch is a €49 billion global industrial corporation that provides technology and services for industries such as Mobility Solutions, Industrial Technology, Consumer Goods, and Energy and Building Technology. The company is one of the largest suppliers of automotive components and is a leader in the emerging industrial IoT industry.</p>
<p>Bosch’s involvement in the Eclipse community includes</p>
<ul>
      <li>Participating in the Eclipse IoT and Eclipse Automotive Working Groups.</li>
<li>Leading the Eclipse Vorto project, an IoT tool that allows for the creation and management of information models that describe IoT devices. These information models can be used to easily connect devices to a variety of IoT platforms.</li>
<li>Leading the Eclipse APP4MC, a tool chain infrastructure for embedded multi- and many-core software development used in the automotive industry. The technology is based on the standard developed in the European ITEA research projects AMALTHEA and its successor AMALTHEA4public.</li>
</ul>
      <p>“No company can realize the IoT on its own. It is very important for Bosch to engage in business ecosystems and open source communities,” explains Stefan Ferber, Vice President for Engineering at Bosch Software Innovations and Bosch’s representative on the Eclipse Board of Directors. “Within the Eclipse Community, through the contribution of many IoT developers, tools and standards are created on an open platform that many companies can benefit from for their IoT applications.”</p>
<p>“As software becomes strategic for industrial companies, Bosch is becoming a leader in adopting open source strategies for their solutions,” explains Mike Milinkovich, executive director of the Eclipse Foundation. “Bosch understands that the keys to success in the software industry are widely adopted open source platforms that can be used to create both developer ecosystems and commercial solutions. We are thrilled to have their leadership in creating IoT and Automotive platforms at Eclipse.”</p>
<p>Bosch joins 12 other companies that are strategic members of the Eclipse Foundation, including Actuate, CA, CEA List, Codenvy, Google, IBM, Innoopract, itemis, Obeo, Oracle, Red Hat, and SAP.</p>

EOHTML;

// Generate the web page
$App->generatePage ( $theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html );