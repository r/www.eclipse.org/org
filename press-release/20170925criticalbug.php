<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Special Notice for Eclipse IDE Users on macOS 10.13 in non-English mode";
$pageKeywords	= "eclipse, bug, critical bug eclipse, macOS 10.13";
$pageAuthor		= "Ian Skerrett";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
		# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<h2>Problem summary:</h2>
    <p>When Eclipse is launched on macOS 10.13 and user's primary language is not set to English, all the menu items in the main menubar are disabled. However, Context menus are not disabled.<br />Please see Bug <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=520176">520176</a>.</p>
    <p><strong>Fix:</strong>
        <br />A fix has been identified for the problem and it;ll be available in Eclipse Oxygen 4.7.1a and later.
    </p>
    <p><strong>Workarounds for existing users:</strong>
        <br />1.&nbsp;Modify Info.plist:
        <br />-Right click on Eclipse.app<br />-Select Show Package Contents
        <br />-Open Contents folder
        <br />-Open Info.plist file
        <br />&nbsp; &nbsp; -If opened with XCode, remove the entry for &lsquo;Localizations&rsquo;
        <br />&nbsp; &nbsp; -If open with a Text Editor, remove the complete entry for &lsquo;CFBundleLocalizations&rsquo;
    </p>
    <p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;key&gt;CFBundleLocalizations&lt;/key&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;array&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;ar&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;cs&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;da&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;el&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;en&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;es&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;de&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;fi&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;fr&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;hu&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;it&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;iw&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;ja&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;ko&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;nl&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;no&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;pl&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;pt_BR&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;pt&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;ru&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;sv&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;tr&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;zh_HK&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;zh_TW&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;string&gt;zh&lt;/string&gt;
        <br />&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&lt;/array&gt;
        <br />-save the Info.plist file<br />-Launch Eclipse.app
    </p>
    <p>2. Launch eclipse with -nl en option
        <br />Using this option will force Eclipse to launch in English even if language pack for the user's language is installed.
        <br />- From the Terminal, go to Eclipse.app/Contents/MacOS
        <br />- Run ./eclipse -nl en
    </p>
    <p>3. Modify eclipse.ini to launch eclipse with English language. Using this option will force Eclipse to launch in English even if language pack for the user's language is installed.
    </p>
    <p>-Right click on Eclipse.app
        <br />-Select Show Package Contents
        <br />-Open Contents folder
        <br />-Open Eclipse folder
        <br />-Open eclipse.ini file
        <br />-Add -Duser.language=en to the end of the file.
        <br />-Save and launch eclipse
    </p>
  </div>
	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://marketplace.eclipse.org/">Eclipse Marketplace</a></li>
				<li><a target="_blank" href="https://eclipse.org">Eclipse Homepage</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

