<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include ("_projectCommon.php"); // All on the same line to unclutter the user's
                                // desktop'

//
// Begin: page-specific settings. Change these.
$pageTitle = "Payara Server is Jakarta EE 8 Compatible";
$pageKeywords = "eclipse, Eclipse Foundation";
$pageAuthor = "Mike Milinkovich";

// Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
				<p>9<sup>th</sup> of October 2019</p>
			<p><em>Payara Server 5.193.1 is Fully Certified as an Open Source Jakarta EE 8 Compatible Implementation</em></p>

<p>Malvern, UK, and Ottawa, 9<sup>th</sup> of October 2019. Payara Services and the Eclipse Foundation, Inc. today announced their application server, Payara Server, has passed the nearly 50,000 open sourced licensed Jakarta EE 8 Technology Compatibility Kits (TCKs) for the Full Platform and is now a Jakarta EE 8 compatible implementation. Payara Server 5.193.1 is available for download <a href="https://www.payara.fish/software/downloads/">here</a>.</p>

<p>&ldquo;The Payara team is extremely proud to be among the first few companies to achieve Jakarta EE 8 Full Platform Compatibility, starting with Payara Platform 5.193.1,&rdquo; says Steve Millidge, founder of Payara Services. &ldquo;This is a significant milestone for Payara and the team has done a huge amount of work to get this done. I think this is a great adoption story for Jakarta EE in general as Payara Server is not a Java EE 8 implementation and this shows that Jakarta EE is an open standard and can bring in new organisations and implementations.&rdquo;</p>

<p>Jakarta EE is the open, vendor-neutral successor of Java EE, and the start of a new age for enterprise Java innovation including cloud-native technologies and microservices. Moving Java EE to a community-driven open source model enables faster evolution, more frequent releases, and the development of modern applications. Payara Server was unable to achieve Java EE certification but not only has the application server become Jakarta EE compatible due to the community-driven process, but the Payara team have also participated in the process of moving Java EE to Jakarta EE as project management committee members, committers, and members of the working group.</p>

<p>&ldquo;Congratulations to Payara for being one of the first to deliver on the promise of Jakarta EE&#39;s open specifications and branding process,&rdquo; says Mike Milinkovich, executive director for the Eclipse Foundation. &ldquo;It is important to note that Payara is a new vendor to this ecosystem, as they were never a Java EE licensee. It is great to see Jakarta EE&#39;s open community processes bring in new organizations and implementations. We fully expect many more organizations to follow in Payara&#39;s pioneering footsteps.&rdquo;</p>

<p>For more information about Payara Server and other Payara Platform solutions, including commercial support and consultancy services, explore <a href="http://www.payara.fish/">Payara.fish</a> or contact Payara at &lt;email&gt; or &lt;phone&gt;</p>

<h2>About Payara Services</h2>

<p>Payara&nbsp;Services is&nbsp;a dedicated team of professionals devoted to open&nbsp;source, Java, our customers, and the community.</p>

<p>We&#39;re&nbsp;major contributors to the development and engineering effort of the&nbsp;Payara&nbsp;Server Open Source Project and the&nbsp;Payara&nbsp;Foundation.&nbsp;Our global Support Engineers deliver 24/7 production, development and migration support directly to customers worldwide.&nbsp;&nbsp;</p>

<p>As Solutions Members of the Eclipse Foundation&nbsp;and members of the Project Management Committee,&nbsp;we&#39;re&nbsp;helping&nbsp;shape the future of the industry&nbsp;to meet&nbsp;our customers&#39; needs&nbsp;through&nbsp;our&nbsp;direct contributions to Jakarta EE&nbsp;and&nbsp;Eclipse&nbsp;MicroProfile&reg;&nbsp;.&nbsp;</p>

<p>Unlike other companies creating&nbsp;products&nbsp;without&nbsp;support,&nbsp;Payara&nbsp;Services is committed to the continuous development and support of our open source software to ensure the highest quality solutions.&nbsp;Discover more at <a href="http://www.payara.fish/">Payara.fish</a>. Follow us on Twitter @Payara_Fish.</p>

<h2>About The Eclipse Foundation</h2>

<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable and commercially-focused environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 375 open source projects, including runtimes, tools, and frameworks for a wide range of technology domains such as IoT, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 275 members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter <a href="https://www.globenewswire.com/Tracker?data=r-dKnkzEx8J2jm68aMwsXND3DPYOl6p60tYwoWOkQRpJjLP_SKHpU_opBz5bi7JLO7Y2btHRJBvdyqqcTBtrLg=="><strong>@EclipseFdn</strong></a> <a href="https://www.globenewswire.com/Tracker?data=S12nqbeRNKuTWwBUeNUnEC9gFrlX34gxCEhP2Uu-2KuWYmqkXbIScweQ1ykqwa2QtAHlvDYJT8GyKpicKaq5uLxRrTZwh2HI7nQfCd01lmEj3UXrlRWmU3VLtBxzjMlq"><strong>LinkedIn</strong></a> or visit <a href="https://www.globenewswire.com/Tracker?data=EsgUi61-f7vRM-pEF4XYjO2jYG6DqnLbsSPKrHZwkTiaAkCbjz8HlE68IUVSML_CiBt9S9b00GJWRIs3EmTELQ=="><strong>eclipse.org</strong></a>.</p>

<h2>Contacts</h2>

<p><strong>Stephen Millidge</strong><br />
Founder, Payara Services<br />
US:&nbsp;+1 415 523 0175 |UK: +44 207 754 0481</p>

<p><strong>Dominika Tasarz-Sochacka</strong>&nbsp;<br />
Marketing Manager, Payara Services<br />
US:&nbsp;+1 415 523 0175 |UK: +44 207 754 0481<br />
Email:&nbsp;<a href="mailto:dominika.tasarz@payara.fish">dominika.tasarz@payara.fish</a></p>

<p><strong>Jay Nichols</strong><br />
Nichols Communications for the Eclipse Foundation, Inc.<br />
US: +1 408-772-1551<br />
jay@nicholscomm.com</p>

  	</div>
</div>

EOHTML;

// Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);