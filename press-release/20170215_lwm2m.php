<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "Eclipse IoT Announces Support for OMA LightweightM2M 1.0 Device Management Standard";
	$pageKeywords	= "iot, eclipse iot, internet of things, lightweightm2m, m2m, machine to machine";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

		<p><b>Ottawa, Canada - February 15, 2017</b> - The Eclipse IoT Working Group, a collaborative working group of the Eclipse Foundation, is pleased to announce the Eclipse Leshan open source project will support the newly ratified Open Mobile Alliance (OMA) <a target="_blank" href="http://www.openmobilealliance.org/wp/Overviews/lightweightm2m_overview.html">LightweightM2M (LwM2M) 1.0</a> device management standard. LwM2M has become an important Internet of Things (IoT) standard for managing low power and lightweight devices that are common for IoT solutions.</p>
		
		<p><a href="https://eclipse.org/leshan">Eclipse Leshan</a> is a Java based implementation of the LwM2M standard. It provides a LwM2M server implementation that can be incorporated into any IoT platform or solution. Eclipse Leshan includes some of the core IoT functionality required for device management, including access control, device connectivity, firmware update, location, and connectivity statistics.</p>
		
		<p>“A key strategy of the Eclipse IoT community has been to provide open source implementations of IoT standards,” explains Mike Milinkovich, Executive Director of the Eclipse Foundation. “Therefore, we are thrilled about our close collaboration with the OMA to provide an open source implementation of LwM2M. Eclipse Leshan’s production quality code will be available soon after the specification has been ratified.”</p>
		
		<p>Eclipse Leshan has been in development since 2014. The project code has been downloaded over 30,000 times. The project is led by developers from Sierra Wireless and includes contributions from over 25 developers. Companies active in the Leshan community include Sierra Wireless, Bosch Software Innovations, and many others.</p>
		
		<p>A new milestone release of Eclipse Leshan 1.0, supporting the LwM2M 1.0 specification, was made available this week.</p>
		
		<p>OMA’s LwM2M standard is a powerful and yet simple technology that fulfills remote device management requirements for the IoT,” said Julien Vermillard, Principal Engineer, Sierra Wireless. “Sierra Wireless started the open source “Leshan project” with the Eclipse Foundation as a way to promote interoperability, and our AirVantage IoT platform is compatible with Leshan. We believe the LwM2M 1.0 release will provide a stable foundation for the standard and support the growing Leshan and LwM2M communities.”</p>
		
		<p><a target="_blank" href="https://github.com/eclipse/wakaama">Eclipse Wakaama</a> will also support the LwM2M 1.0 specification. Eclipse Wakaama is a C language implementation of the LwM2M specification.</p>
		
		<h3>About Eclipse IoT and Eclipse Foundation</h3>
		
		<p>The <a href="https://iot.eclipse.org/">Eclipse IoT Working Group</a>, is a collaborative working group hosted by the Eclipse Foundation, that is working to create open source software for IoT solutions. Eclipse IoT is made up of over 30 member companies and 28 open source source projects.</p> 
		
		<p>Eclipse is a community for individuals and organizations who wish to collaborate on open source software. There are over 300 open source projects in the Eclipse community, ranging from tools for software developers, geo-spatial technology, system engineering and embedded development tools, frameworks for IoT solutions, tool for scientific research, and much more. The Eclipse Foundation is a not-for-profit foundation that is the steward of the Eclipse community. More information is available at <a href="http://www.eclipse.org/">eclipse.org</a>.</p>
		
		



				
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://iot.eclipse.org/">Eclipse IoT</a></li>
				<li><a target="_blank" href="https://eclipse.org/leshan">Eclipse Leshan</a></li>
				<li><a target="_blank" href="https://github.com/eclipse/wakaama">Eclipse Wakaama</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

