<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "MQTT Test Day Demonstrates Successful Interoperability for the Internet of Things";
	$pageKeywords	= "eclipse, foundation, iot, mqtt, test, day, internet of things";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<p><b>Fifteen Products and Organizations Participate in First Interop Testing Event</b></p>
<br>

		<p><b>Ottawa, Canada – April 8, 2014</b> - The Eclipse Foundation and the Eclipse IoT Working Group hosted the first 
				MQTT Interop Test Day to demonstrate the success of MQTT as a standard for the Internet of Things.  A total 
				of 15 MQTT-based products participated in the testing event, demonstrating the interoperability of the MQTT 
				standard for IoT.  The list of participants includes ClearBlade, Sierra Wireless, HiveMQ, WSO2, Xively, 
				2lemetry, Red Hat JBoss A-MQ,  Litmus Automation, Eurotech, Software AG Universal Messaging and IBM MessageSight.</p>

		<p>The Internet of Things (IoT) is a key trend in the technology industry that is projected to be a $19 trillion industry. 
				IoT is described as the connection and communication of different sensor and actuating devices via the Internet with 
				the people, places and existing enterprise systems. For IoT to be successful, a key requirement is interoperability 
				between different solutions, devices, and enterprise server applications.  MQTT is an emerging standard that aims to 
				provide a messaging protocol for IoT interoperability.</p>

		<p>MQTT was initially developed by IBM and Eurotech. In November 2012, IBM announced it would contribute the client implementation 
				of MQTT to the Eclipse Paho open source project.  In March 2013, IBM, Eurotech, and others began the standardization process 
				of MQTT within the OASIS standards group. It is expected that the standardization process will be completed by September 2014, 
				resulting in the publication of the MQTT 3.1.1 specification.</p> 
		<p>The MQTT Interop Test day was hosted by the Eclipse IoT Working Group, an open source community dedicated to the development of 
				open source IoT technology. Today, Eclipse IoT hosts 14 different open source projects focused on IoT, including Eclipse Paho 
				that provides MQTT client implementations and Eclipse Mosquito that provides an MQTT broker implementation.</p> 

		<p>At the Test Day, products were matched with other products and put through a predefined set of tests.  The tests measured the 
				conformance to the draft MQTT specification and the degree of interoperability with other products. Overall, more than 50% of 
				the test pairs were considered successful. At this stage of the standardization process, this demonstrates a good level of 
				interoperability between MQTT implementations and points to the ease of creating interoperable IoT solutions based on MQTT. 
				It is expected that once the final OASIS specification is approved in the Fall 2014 the level of interoperability will significantly improve.</p>

		<p>A detailed report, including a full list of participants and test plans, is <a href="http://iot.eclipse.org/documents/2014-04-08-MQTT-Interop-test-day-report.html">now available</a>. A future MQTT Test Day will be planned for the Fall 2014.</p>
				
	<h3>Quotes from Participants</h3>

		<p><b>2lementry</b><br>
		"As a company that relies on interoperability with MQTT, the Eclipse Interop Test Day provided a great way to validate our services with other clients,"  
				said Chris Chiappone, CTO of 2lemetry. "Eclipse also did a great job creating a conformance test suite prior to the event, which helped us 
				successfully pre-test client and server compliance."</p>
				
		<p><b>Xively</b><br>
		"We believe that standardization and interoperability are critical to accelerating innovation in today's early-stage Internet of Things market," 
				said Mario Finocchiaro, Director of Xively Business Development for LogMeIn.  "The inaugural MQTT interoperability event proved to be a great 
				opportunity for collaboration between emerging IoT leaders, while showing promise for encouraging and empowering broader commercial IoT adoption.  
				Congratulations to Eclipse for bringing it all together."</p>
				
		<p><b>ClearBlade</b><br>
		"The MQTT Interop Day was extremely valuable in bringing together both specification and implementation leaders, and reconfirming our objectives as a 
				unified community.  The ClearBlade enterprise platform was built on the foundation and belief that strong open standards and interoperation create 
				enterprise value, and that is achieved with this level of industry collaboration."</p>

		<p><b>Red Hat</b><br>
		"The Internet-of-Things (IoT) will require open standards to make it a reality. That is why Red Hat JBoss A-MQ, an open source enterprise message broker, 
				supports open protocol standards such as AMQP 1.0 and MQTT. Red Hat has demonstrated its commitment to the MQTT standard by successfully taking part 
				in the MQTT Interoperability Test Day at EclipseCon this year. JBoss A-MQ supports MQTT across TCP/TLS and WebSockets, and allows for an MQTT client 
				to interoperate with other clients connected to JBoss A-MQ, regardless of the protocol that client is using."<br>
				- Rob Davies, director of software engineering, Red Hat</p>

		<p><b>Eurotech</b><br>
		"It has been really exciting to see so many companies investing in high quality MQTT implementations. The interoperability test has been a huge success and 
				is a testament to how quickly the community is growing around this IoT protocol. We're looking forward to completing the MQTT standardization process 
				within the OASIS standards body and continuing the comprehensive activities within the IoT Working Group of the Eclipse Foundation. With Eurotech there 
				at the very beginning of the protocol definition, it’s gratifying to see acceptance building so quickly now as MQTT helps to enable the Internet of Things."<br>
				- Wes Johnson, Director, Software for Eurotech</p>
				
		<p><b>IBM</b><br>
		"The mix of MQTT technologies represented, ranging from open source to commercial, and the successes achieved at the event demonstrate the vibrancy of the MQTT 
				ecosystem and means we will be well positioned as we move toward a ratified standard.  Interoperability of technology in this space is vital for the Internet 
				of Things ecosystem."<br>  
				- Dave Locke, IBM senior inventor and co-lead of the Eclipse Paho project</p>
						
				

</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://iot.eclipse.org/documents/2014-04-08-MQTT-Interop-test-day-report.html">MQTT Interop Test Day Report</a></li>
				<li><a href="http://iot.eclipse.org/">Eclipse IoT</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

