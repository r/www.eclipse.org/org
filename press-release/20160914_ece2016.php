<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Keynotes, Tracks and Sponsors Announced for the 11th Annual EclipseCon Europe Conference";
  $pageKeywords = "eclipse, eclipsecon, ece2016";
  $pageAuthor   = "Eric Poirier";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
  <div id="midcolumn">
    <h1>$pageTitle</h1>
    <p><strong>Ottawa, Canada – September 8, 2016</strong> - The Eclipse Foundation is pleased to announce
        <a href="https://www.eclipsecon.org/europe2016">EclipseCon Europe 2016</a>. Taking place on October 25-27, 2016, the conference will
        be held at the Forum am Schlosspark in Ludwigsburg, Germany.</p>
    <p>This year will mark the eleventh consecutive year of the popular event
        dedicated to Eclipse software developers and its member companies.
        EclipseCon Europe is a three-day event, co-located with the <a href="https://www.osgi.org/2016-osgi-community-event/">OSGi Alliance Community Event 2016</a>,
        designed to create opportunities for the Eclipse community to learn, explore, share and collaborate on the latest ideas and information about Eclipse and its member companies.</p>

    <p>The keynote addresses will be given by top researchers and industry leaders.</p>
    <ul>
      <li><a href="https://www.eclipsecon.org/europe2016/session/shuttle-case">Stephen Carver, Cranfield University – The Shuttle Case</a>
        <ul>
          <li>Mr. Carver, Senior Lecturer at Cranfield University, will discuss the Space Shuttle disasters, the inevitability of those failures, and the critical importance of communication and leadership in large-scale projects and programs.</li>
        </ul>
      </li>
      <li><a href="https://www.eclipsecon.org/europe2016/session/oh-no-visual-studio-code-yet-another-development-tool">Dr. Erich Gamma, Distinguished Engineer at Microsoft – Oh No! Visual Code – Yet Another Development Tool</a>
        <ul>
          <li>Dr. Gamma is one of the original leaders of the Eclipse project. He will talk about his journey to create the new Visual Code tool, and how the architecture enabled the sharing of language services among Eclipse, Visual Studio Code and other development tools.</li>
        </ul>
      </li>
      <li><a href="https://www.eclipsecon.org/europe2016/session/osgijava-industrial-iot-more-solid-trend-%E2%80%94-essential-scale-world-internet-connected-things">Robert Andres, Eurotech and Walter Hofmann, Hitachi - OSGi/Java in Industrial: IoT</a>
        <ul>
          <li>During this presentation, Mr. Andres and Mr. Hofmann will discuss how OSGi and Java are being used to create modular software building blocks that make it possible to create IoT solutions based on open standards and open source.</li>
        </ul>
      </li>
      <li><a href="https://www.eclipsecon.org/europe2016/session/observation-gravitational-waves-binary-black-hole-mergers-dawn-new-astronomy">Prof. Dr. Benno Willke, Max Planck Institute for Gravitational Physics - Observation of Gravitational Waves from Binary Black Hole Mergers</a>
        <ul>
          <li>In the presentation, Dr. Willke will explore the future of gravitational wave astronomy, and the recent advancements in gravitational wave technology and related methodologies. </li>
        </ul>
      </li>
    </ul>
    <p>The conference program consists of eight tutorials and 105 technical sessions. The major themes include Tools, IDE & DevOps, Java 9, Eclipse Technology, Web & Cloud Development & Operations, Industry Solutions, Science, and an Introduction to the Eclipse Ecosystem. </p>

        <p>In addition to the regular program, EclipseCon Europe includes special theme days and events:</p>

        <ul>
          <li><strong>IoT Day</strong> explores the exciting community that is building open source technology for the Internet of Things (IoT) industry;</li>
          <li><strong>Project Quality Day</strong> focuses on the methods and tools that assure the quality of projects;</li>
          <li><strong>CDT Summit</strong>  (part of the Unconference) is an informal gathering of CDT (Eclipse C++ Development Tooling Project) users and extenders;</li>
          <li><strong>IoT Playground</strong> showcases IoT technology and gadgets, with an opportunity for engagement and experimentation.</li>
        </ul>

        <p>The week also includes the <a href="https://www.eclipsecon.org/europe2016/unconference">Unconference</a> on Monday, October 24.
        The Unconference provides the type of networking activities and casual
        interaction that is ideal for Eclipse <strong>Working Groups</strong> and similar
        initiatives that bring many different people and organizations together.</p>

        <p>The Dual/Elite sponsor for EclipseCon Europe is The Bosch Group,
        and the Premium sponsors are Codenvy, Thingworx, and SAP. EclipseCon
        Europe could not exist without the contribution and support of these
        companies and organizations.</p>

        <p>Registration is now open for the event. Please visit the <a href="https://www.eclipsecon.org/europe2016/registration">registration page</a>.</p>

        <p>For information on how to become a sponsor, please visit the EclipseCon Europe 2016 <a href="https://www.eclipsecon.org/europe2016/prospectus">sponsor prospectus page</a>.</p>

        <h3>About the Eclipse Foundation</h3>

        <p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>

        <p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="https://www.eclipse.org">www.eclipse.org</a>.</p>
  </div>

  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
          <li><a href="https://www.eclipse.org/neon">Neon Release</a></li>
      </ul>
    </div>
  </div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
