<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse Foundation Celebrates 10th Anniversary";
	$pageKeywords	= "eclipse, foundation, anniversary, 10";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

		<p><b>Ottawa, Canada – February 3, 2014</b> – The Eclipse Foundation today marked its tenth birthday. Originally created as a consortium when IBM released the Eclipse Platform into open source in 2001, the Eclipse Foundation was formed as an independent, not-for-profit, and vendor-neutral organization and announced on February 2, 2004. Since that time, the Eclipse Foundation has grown from 19 projects and 50 members to 247 projects and 205 members. Eclipse's collaborative governance model has led to the creation of a number of collaborative working groups in industries and technologies such as aerospace, automotive, geospatial and the Internet of Things.</p>

		<p>Originally focused on providing an extensible platform for building desktop software development tools, the Eclipse community has grown to cover a wide range of technologies, including rich client platforms, modeling, web-based development tools, Java server runtimes, and frameworks, protocols and tools for the Internet of Things.</p>
	
		<p>Over the past ten years, the Eclipse community has had a significant impact on the technology industry.</p>
			<ul>

				<li>Eclipse substantially changed the Java IDE and software development tools industry, becoming the dominant Java IDE and ALM tool platform for software developers.</li>
				<li>Eclipse has become the defacto standard for C and C++ IDE in the embedded and silicon vendor industry.</li>
				<li>Eclipse was the first --- and perhaps the only --- open source organization that has demonstrated the ability to ship annual releases on time to the day each and every year for the past ten years.</li>
				<li>Eclipse was the first open source organization to demonstrate that market competitors such as IBM, BEA, Borland, Sybase, Oracle, SAP, Google, etc. could collaborate successfully in governing an open source community.</li>
				<li>Eclipse has established a set of best practices for open source IP management that has led to pervasive use of Eclipse open source technology in commercial products. Eclipse has scanned over 2,000 third party open source libraries to assure their IP cleanliness.</li>
			</ul>
				
		<p>"Having begun its life as a bold experiment in corporate open source contribution, the Eclipse Foundation has over the past decade evolved from a focused Java development project to a diverse community supporting a wide array of languages and needs," said Stephen O'Grady, Principal Analyst at RedMonk. "From browser based development to the Internet of Things, Eclipse is always willing to rethink its role and adapt itself to the fast changing industry around it."</p>
				
		<p>"Ten years is a long time in the technology industry" commented Mike Milinkovich, executive director of the Eclipse Foundation. "All of us who are part of the Eclipse community are very proud of what we have built, and look forward to even more successes in the future. The Eclipse community is growing quickly in exciting new areas such as web-based development (Orion), tools for safety-critical software engineering, geospatial technologies, and protocols, frameworks and tools for the internet of things. The breadth of technology being worked on at Eclipse is breathtaking."</p> 


		<h2>About the Eclipse Foundation</h2>

		<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and individuals extend, complement, and support the Eclipse Platform.</p>

		<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at <a href="http://eclipse.org/">www.eclipse.org</a>.</p>		
	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/">eclipse.org</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

