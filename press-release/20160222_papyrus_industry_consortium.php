<?php                                                             require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App  = new App();  $Nav  = new Nav();  $Menu   = new Menu();   include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


  #
  # Begin: page-specific settings.  Change these.
  $pageTitle    = "Eclipse Foundation Launches Papyrus Industry Consortium Focused on Modeling Tools for Embedded Development";
  $pageKeywords = "eclipse, papyrus, modeling, polarsys, embedded development";
  $pageAuthor   = "Roxanne Joncas";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
  # $Nav->addCustomNav("My Link", "mypage.php", "_self");
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML
  <div id="maincontent">
	<div id="midcolumn">
  <h1>$pageTitle</h1>
  		
  		<p><b>Embedded World, Germany &mdash; February 23, 2016</b> &mdash; The Eclipse Foundation is pleased to announce the creation of the Papyrus 
  		Industry Consortium, a collaboration to create a model-based engineering platform and workbench based on the domain specific and 
  		modeling capabilities of the Eclipse Papyrus family of products. The consortium partners will jointly fund the development of the 
  		Papyrus platform and tools, consolidate and prioritize requirements from consortium partners and across application domains, and 
  		promote Papyrus as an open source solution for MBE. The founding members of the consortium include Adocus, 
  		Airbus Helicopters, Airbus Defence & Space, Atos, CEA List, Combitech/Saab, EclipseSource, Ericsson, 
  		Flanders make, Fraunhofer Fokus, OneFact, and Zeligsoft.</p>
  		
		<p>The Papyrus Industry Consortium (IC) aims to deliver an industrial-grade open source MBE solution for companies developing software 
  		intensive systems ranging from embedded software development, enterprise software, Internet of Things (IoT) to cyber-physical systems. 
  		As the mantra ‘software is eating the world’ indicates, many of these companies are becoming more reliant on software development for 
  		their customer solutions. Better software design tools, like Papyrus, will enable these organizations to design, develop and deliver 
  		software-based solutions in a more efficient manner.</p>

  		<p>“Ericsson has over a thousand software engineers who require advanced modeling tools to create the software systems for our commercial 
  		solutions”, explains Francis Bordeleau, Product Manager Software Development at Ericsson. “Therefore, the existence of both an 
  		industrial-grade modeling tool and a vibrant and growing modeling tools community is a critical part of our long-term software 
  		development strategy. With the establishment of the Papyrus IC, we strongly believe that Papyrus will become the dominant modeling 
  		tools solution that will provide the required innovation for a larger adoption of model-based engineering.”</p>

  		<p>Papyrus is an extensible platform and tool suite that supports existing modeling languages such as UML 2.5 and SysML 1.4, and the 
  		creation of industry-specific and domain-specific modeling languages, allowing end-users, suppliers, and academics to customize 
  		Papyrus to their specific needs. Through its various specializations, Papyrus also enables development of capabilities regarding 
  		different development aspects, including model-based simulation, model-based testing, safety analysis, and architecture exploration.</p>

  		<p>“The Papyrus IC is a great example of end-user organizations using open source to direct the development of an industry solution 
  		to meet their specific needs,” explains Mike Milinkovich, executive director of the Eclipse Foundation. “We expect the Papyrus IC 
  		will jointly fund $5 million/year of research and development towards building a model-based engineering solution that meets the 
  		needs of large organizations that depend on modeling tools for their software development and design. We believe this type of 
  		open source collaboration is the future of software development for many types of industries.”</p> 

  		<p>More information about the Papyrus tools and the Papyrus IC is available at 
  		<a href="https://eclipse.org/papyrus/">https://eclipse.org/papyrus/</a> and 
  		<a target="_blank" href="https://www.polarsys.org/ic/papyrus">https://www.polarsys.org/ic/papyrus</a>. The Eclipse 
  		Foundation and the Papyrus IC will be demonstrating the Papyrus platform at the Embedded World 
  		Conference in Nuremberg, Germany, on March 23-25 in Hall 4, Booth 160.</p>
  		
  		
  	<h3>About the Eclipse Foundation</h3>
	<p>Eclipse is an open source community whose projects are focused on building an open development platform comprised of extensible frameworks, tools, and runtimes for building, 
  		deploying, and managing software across the lifecycle. A large, vibrant ecosystem of major technology vendors, innovative start-ups, universities, research institutions, and 
  		individuals extend, complement, and support the Eclipse Platform.</p>

  	<p>The Eclipse Foundation is a not-for-profit, member supported corporation that hosts the Eclipse projects. Full details of Eclipse and the Eclipse Foundation are available at 
  		<a href="http://www.eclipse.org/">www.eclipse.org</a>.</p>
  		
      </div>

   <!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
  				<li><a href="https://eclipse.org/papyrus/">Papyrus</a></li>
  				<li><a target="_blank" href="https://www.polarsys.org/ic/papyrus">Papyrus IC</a></li>
			</ul>
		</div>
	</div>
</div>
      </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>
