<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


#
# Begin: page-specific settings.  Change these.
$pageTitle 		= "Eclipse Foundation Announces Jakarta EE Committee Election Results";
$pageKeywords	= "eclipse, Jakarta EE, election, Jakarta EE Committee, election results, Eclipse Foundation";
$pageAuthor		= "Thabang Mashologu";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
# $Nav->addCustomNav("My Link", "mypage.php", "_self");
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
				
<p><strong>Ottawa, Canada - July 31, 2018 </strong> -Today we are pleased to announce the results of the Participant and Committer Member elections for representatives to the Jakarta EE Working Group Steering Committee, Specification Committee, and Marketing & Brand Committee. These committees provide governance for the evolution and adoption of Jakarta EE, the community-led platform created from the contribution of Java EE technologies and hosted by the Eclipse Foundation.</p>
				
<p>Congratulations to the following Committee Members:</p>
	<ul>
		<li>Specification Committee - Participant: Alex Theedom (LJC)</li>
        <li>Specification Committee - Committer Member: Werner Keil</li>
		<li>Marketing & Brand Committee - Participant: Simon Maple (LJC)</li>
		<li>Marketing & Brand Committee - Committer Member: Ivar Grimstad</li>
        <li>Steering Committee - Participant: Martijn Verburg (LJC)</li>
		<li>Steering Committee - Committer Member: Ivar Grimstad</li>
	</ul>
<p>We are looking forward to working with you all on the Jakarta EE Working Group committees.</p>
<p>For more information about the Jakarta EE Working Group governance and processes, please visit the Jakarta EE <a href="https://www.eclipse.org/org/workinggroups/jakarta_ee_charter.php" target="_blank">Working Group Charter</a> page. To learn more about Jakarta EE, please visit jakarta.ee.</p>				

<h3>About the Eclipse Foundation</h3>
				
<p>The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and commercially focused environment for open source software collaboration and innovation. The Foundation is home to the Eclipse IDE, Jakarta EE, and over 350 open source projects, including runtimes, tools and frameworks for a wide range of technology domains such as IoT, automotive, geospatial, systems engineering, and many others. The Eclipse Foundation is a not-for-profit organization supported by over 275 corporate members, including industry leaders who value open source as a key enabler for business strategy. To learn more, follow us on Twitter @EclipseFdn, LinkedIn or visit eclipse.org.</p>
				
  </div>
	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://jakarta.ee/">Jakarta EE</a></li>
                <li><a target="_blank" href="https://jakarta.ee/members/">Jakarta EE Members</a></li>
				<li><a target="_blank" href="https://jakarta.ee/connect/">Jakarta EE Connect</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);