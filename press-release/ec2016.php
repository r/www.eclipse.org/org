<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "EclipseCon returns to D.C. for 2016";
	$pageKeywords	= "eclipse, eclipsecon, march";
	$pageAuthor		= "Roxanne Joncas";

	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

			
			<p>The Eclipse Foundation is pleased to announce that <a href="https://www.eclipsecon.org/na2016/">EclipseCon 2016</a>, its preeminent North American conference, is returning to Reston, Virginia, just outside Washington, D.C.</p>
				
			<p>The annual Eclipse North American conference will be held March 7-10, 2016 at the <a target="_blank" href="http://reston.hyatt.com/en/hotel/home.html">Hyatt Regency Reston</a>, located just minutes from Dulles Airport in the D.C. area.  This location was very popular with attendees in 2012, and proved to be an easy location for European and East Coast residents to participate in this always popular showcase event for the Eclipse community.</p> 

			<p>Mikael Barbero, who recently joined the Eclipse Foundation from Obeo, will serve as the Program Chair, and is bringing a few new topics and talk formats to the conference.  The conference Call for Papers will go out in early October - details will be posted soon.</p>

			<p>Be sure to save the dates - we look forward to seeing everyone in Reston next March!</p>
				
				<a href="https://www.eclipsecon.org/na2016/"><img src="/org/images/eclipsecon_logoslim.png" class="img-responsive"/></a><br/>
				</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a target="_blank" href="https://www.eclipsecon.org/na2015/">EclipseCon 2015</a></li>	
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

