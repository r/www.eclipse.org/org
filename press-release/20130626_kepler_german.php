<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include("_projectCommon.php");    # All on the same line to unclutter the user's desktop'

	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Das jährliche integrierte Eclipse Release steht bereit";
	$pageKeywords	= "eclipse, kepler, open source, release train";
	$pageAuthor		= "Roxanne Joncas";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank)
	# $Nav->addCustomNav("My Link", "mypage.php", "_self");
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank");

	# End: page-specific settings
	#
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<style>
		.paddedlist li {	padding-bottom:7px;	}
		#midcolumn ul ul{padding-bottom:0px;}
	</style>
	
<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
<br>

<p><i>Das Eclipse Kepler Release besteht aus 71 Projekten und mehr als 58 Millionen Code Zeilen</i></p>

			<p><b>Ottawa, Kanada</b> - Die Eclipse Foundation teilt die Verfügbarkeit des jährlichen integrierten Releases („Release Train“) mit. Wie auch schon in den vergangenen Jahren hat die Open Source Community auch 2013 wieder sein Hauptrelease pünktlich zum Ende Juni verfügbar machen können. Dieses Release erlaubt es den Benutzern von Eclipse Technologien, dieses entweder  so zu verwenden oder in ihre Anwenungen und Produkte zu integrieren.</p>
			<p>Das Kepler Release besteht aus 71 unterschiedlichen Projekten, die von 420 Entwicklern aus 54 verschiedenen Organisationen betreut wurden. Insgesamt besteht das Release aus mehr als 58 Millionen Code Zeilen. Dieses Release dokumentiert erneut, dass Open Source Prozesse sehr effektiv sind bei der Entwicklung von grossen Software Systemen.</p>
			<p>Mike Milinkovich, Executive Director der Eclipse Foundation, erläutert: „Das jährliche integrierte Eclipse Release ist sehr wichtig für den Erfolg und das Wachstum unseres Eco Systems. Die Benutzer und kommerziellen Erweiterer  können sich darauf verlassen, jedes Jahr zur selben Zeit unsere Lieferung zu erhalten und sind somit in der Lage, auch ihrerseits eine verlässliche Planung aufzustellen. Eclipse ist ein hervorragendes Beispiel dafür, wie offene Technologien einen höchst positiven Einfluss auf die Industrie haben können!"</p>
			<p>Die Zusammenfassung der Highlights des Kepler Releases: </p>
			<p><b>Support für Java EE 7</b> - Das <a href="http://www.eclipse.org/webtools/">Eclipse Web Tools Project (WTP) 3.5</a> stellt Support für das kürzlich freigegebene JAVA EE 7 bereit. Es beinhaltet auch Unterstützungen für JPA 2.1, JSF 2.2, JAX-RS 2.0, Servlet 3.1, EJB 3.2, Connector 1.7, App Client 7.0 und EAR 7.0.Eclipse WTP project wizards,  code generation wizards und Validierung wurden auf den neuesten Stand gebracht, so dass Entwickler nun einfach Java EE 7 Applikationen entwickeln, debuggen und in Betrieb nehmen können.</p>
			<p><b>Neue Business Process Management Suite</b> – Das <a href="http://www.eclipse.org/stardust/">Eclipse Stardust</a> Projekt liegt im Kepler Release in der Version 1.0 vor. Stardust liefert eine Process Management Engine und die zur Konfiguration und Entwicklung nötigen Werkzeuge. Ebenso enthält es eine Modellierungsumgebung zur Erstellung, Debuggen  und Ausführung von BPM Applikationen, ein Web Portal für Browser-basierte Ausführung und Monitoring sowie eine Eclipse BIRT basierte Komponente für das Lauzeit orientierte Monitoring und Reporting.</p>
			<p><b>Skalierbarkeit und  Benutzbarkeit der Web-basierten IDE</b> – <a href="http://www.eclipse.org/orion/">Orion 3.0</a> hat seine Skalierbarkeit und Benutzbarkeit im neuen Release weiter verbessern können. Orion kann nun einfach in einer WAR Datei installiert werden und ist somit besser in Cloud Umgebungen betreibbar. Im neuen Release verfügt Orion über bessere Datei Navigation im Editor, neue Shortcuts für vi und Emacs Kompatibilität, <i>auto/save und auto/load</i> Funktionen sowie ein verbessertes <i>Look and Feel</i></p>.
			<p><b>Support für Big Data</b> – Das <a href="http://www.eclipse.org/birt/phoenix/">Eclipse BIRT 4.3</a> Release verfügt nun über eine Anbindung an die beliebten Datenbanken MongoDB und Cassandra. Hiermit erschliessen sich die Visualisierungsfunktionen von BIRT für die Entwickler von Big Data Applikationen. Diese neuen Funktionen erweitern die bereits bestehende Anbindung an die Hadoop Technologie.</p>
			<p><b>Bessere Integration für Code Reviews</b> – <a href="http://www.eclipse.org/mylyn/">Mylyn 3.9</a> ermöglicht nun eine vereinfachte Vorgehensweise für Code Reviews. Eine neue Navigationsansicht, die mit dem Werkzeug Gerrit integriert ist, erlaubt eine strukturierte Ansicht auf alle Dateien und Kommentare in einem Review.</p>
			<p><b>Verbesserte Integration mit Maven für Java EE Developers</b> – Unterstützung für die Integration mit dem Eclipse Web Tools Project bringt eine Sammlung von Konnektoren. Diese erlauben <a href="http://www.eclipse.org/m2e-wtp/">Maven Integration</a> für Java EE Projekte in Eclipse, einschliesslich WAR, EIB, EAR und RAR Projekte.</p>
			<br/>
			<p>Alle diese Porjekte sind Bsstandteil des integrierten Kepler Releases und stehen jetzt als <a href="http://www.eclipse.org/kepler/">Download</a> zur Verfügung. 12 verschiedene Packages für verschiedene Nutzungsprofile machen es einfach, Kepler herunterzuladen und zu benutzen.  </p>

			<p><b>Über die Eclipse Foundation</b></p>
			<p>Die Eclipse Foundation ist eine auf Mitgliedschaften basierende „non-for-profit“-Organisation, die das Hosting und Management der Eclipse Projekte betreibt.</p>
			<p>Weitere Informationen über die Eclipse Foundation stehen unter <a href="http://www.eclipse.org/">http://www.eclipse.org</a> bereit.</p>			


	</div>

	<!-- remove the entire <div> tag to omit the right column!  -->
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="http://eclipse.org/kepler/">Kepler</a></li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
<?php
/*
 * Created on 20-Jan-2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
?>

