<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div class="container margin-bottom-20">
  <div class="row section-highlights">
    <div class="featured-highlights-item match-height-item-by-row col-sm-8">
      <img width="60" src="assets/images/top-level-wg/vendor-neutral-governance.svg" alt="vendor neutral governance icon">
      <h3>Vendor-Neutral Governance</h3>
      <p>Working Groups allow for individuals and organizations to collaborate under a vendor neutral governance model</p>
      <p>
        <a href="about.php#wg-neutral">Learn more</a>
      </p>
    </div>
    <div class="featured-highlights-item match-height-item-by-row col-sm-8">
      <img width="60" src="assets/images/top-level-wg/ecosystem-development-marketing.svg" alt="vendor neutral governance icon">
      <h3>Ecosystem Development and Marketing</h3>
      <p>Eclipse Foundation staff help build a community for collaboration through marketing and community programs</p>
      <p>
        <a href="about.php#wg-eco">Learn more</a>
      </p>
    </div>
    <div class="featured-highlights-item match-height-item-by-row col-sm-8">
      <img width="60" src="assets/images/top-level-wg/collaborative-management.svg" alt="vendor neutral governance icon">
      <h3>Collaborative Management</h3>
      <p>Working groups coordinate the efforts of open source projects by providing a shared vision and roadmap</p>
      <p>
        <a href="about.php#wg-collaborative">Learn more</a>
      </p>
    </div>
  </div>
  <hr>
  <div class="row section-highlights">
    <div class="featured-highlights-item match-height-item-by-row col-sm-8">
      <img width="60" src="assets/images/top-level-wg/specification-development.svg" alt="vendor neutral governance icon">
      <h3>Specification Development</h3>
      <p>Eclipse Working Groups use the proven Eclipse Specification Development processes that provides a framework for the development of specifications in open source</p>
      <p>
        <a href="about.php#wg-spec">Learn more</a>
      </p>
    </div>
    <div class="featured-highlights-item match-height-item-by-row col-sm-8">
      <img width="60" src="assets/images/top-level-wg/branding.svg" alt="vendor neutral governance icon">
      <h3>Branding and Compatibility</h3>
      <p>Creating branding and compatibility programs to build a trusted ecosystem of implementers and consumers</p>
      <p>
        <a href="about.php#wg-branding">Learn more</a>
      </p>
    </div>
    <div class="featured-highlights-item match-height-item-by-row col-sm-8">
      <img width="60" src="assets/images/top-level-wg/research.svg" alt="vendor neutral governance icon">
      <h3>Research@Eclipse</h3>
      <p>The Eclipse Foundation participates in many government funded industry research projects</p>
      <p>
        <a href="/org/research">Learn more</a>
      </p>
    </div>
  </div>
</div>

