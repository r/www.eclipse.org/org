<?php

/**
 * Copyright (c) 2019, 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 *  Zhou Fang (Eclipse Foundation) - Initial implementation
 *  Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 1.1 - Revision history at end of document</p>
    <h2>Vision and Scope</h2>
    <p>
      Hardware has evolved dramatically during the last 30 years. Many of the core assumptions applied to
      operating systems to manage the increasing hardware complexity and diversity have not.
    </p>
    <p>
      Oniro envisions an evolution in the operating system front that will foster more modular platforms covering a
      significantly wider range and number of connected devices, while granting developers a simpler and more
      curated development experience, enabling them to provide consumers a significantly better user experience.
    </p>
    <p>
      The Oniro Working Group&#39;s mission is to foster an ecosystem of organizations that supports the community
      in the production and evolution of the Oniro operating system and platform. Oniro is a new commercially
      oriented, modular, and multikernel open source software platform. Its ecosystem will be developed in an
      environment where collaboration is promoted via the core Eclipse Foundation principles of
      vendor-neutrality, transparency, and openness.
    </p>
    <p>
      The Oniro Distribution shall be an Oniro compatible implementation, which
      shall be deemed OpenHarmony compatible implementation, primarily intended
      for the European market and shall be promoted as OpenHarmony compatible
      in the marketplace.
    </p>
    <p>The Oniro Working Group’s mission is accomplished through two strategic goals:</p>
    <ol>
      <li>Support the Oniro developer community in the creation and evolution of the software platform.</li>
      <li>Drive the commercial success of this software platform. </li>
    </ol>
    <p>The Oniro Working Group will support the developer community focusing on, at least, the following actions: </p>
    <ul>
      <li>
        Establish and drive a funding model that enables this working group and its community to operate
        on a sustainable basis.
      </li>
      <li>
        Design and execute a branding, communication, and promotion strategy raising awareness about
        Oniro outputs and outcomes, consolidating the Oniro brand as a reference in its field.
      </li>
      <li>
        Provide expertise to support and lead the Oniro ecosystem and developer community in legal,
        intellectual property, and software license topics.
      </li>
      <li>
        Provide expertise and support in other non-technical activities that the developer&apos;s community might
        require to accomplish their goals.
      </li>
    </ul>
    <p>
      The Oniro Working Group will drive the commercial success of the software platform focusing on, at least,
      the following actions:
    </p>
    <ul>
      <li>
        Create an ecosystem and develop programs to encourage interaction and
        overlap between the Oniro ecosystem and the OpenHarmony ecosystem to
        further strengthen both projects.
      </li>
      <li>
        Develop and execute marketing and developer advocacy programs to support the worldwide
        adoption of Oniro.
      </li>
      <li>
        Design, create and validate specifications to assure attractiveness for
        European markets that bring transparency and standardization to the
        software platform as well as the products based on or derived from it.
      </li>
      <li>
        In alignment with OpenAtom's similar programs, define a compatibility
        program that consolidates a variety of products based on the Oniro
        project outputs. Such compatibility program will leverage the
        cooperation agreement between Eclipse Foundation and OpenAtom
        Foundation to further enable Oniro compatible products to make further
        claims of being OpenHarmony compatible.
      </li>
      <li>
        Define and manage a product roadmap that brings together the Oniro developer&apos;s community and
        the platform consumers&apos; expectations and needs, helping the adoption of Oniro technologies by the
        Working Group Members as well as the wider industry.
      </li>
    </ul>

    <h2>Governance and Precedence</h2>
    <h2>Applicable Documents</h2>
    <p>
      The following governance documents are applicable to this charter, each of which can be found on the
      <a href="/org/documents/">Eclipse Foundation Governance Documents</a> page or
      the <a href="/legal/">Eclipse Foundation Legal Resources</a> page:
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Working Group Process</li>
      <li>Eclipse Working Group Operations Guide</li>
      <li>Eclipse Membership Agreement</li>
      <li>Eclipse Intellectual Property Policy</li>
      <li>Eclipse Antitrust Policy</li>
      <li>Eclipse Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership Agreement, including the requirement
      set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse
      Foundation, including but not limited to the Intellectual Property and Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this working group’s charter and the Eclipse
      Foundation Bylaws, Membership Agreement, Development Process, Specification Process, Working Group
      Process or any policies of the Eclipse Foundation, the terms of the respective Eclipse Foundation Bylaws,
      Membership Agreement, process or policy shall take precedence.
    </p>

    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a
      <a href="/membership/#tab-levels">Contributing Member</a>
      of the Eclipse Foundation, have executed the Oniro Working Group Participation Agreement and adhere to
      the requirements set forth in this Charter to participate.
    </p>
    <p>
      The participation fees associated with each of these membership classes are shown in the Annual
      Participation Fees section. These are annual fees, and are established by the Oniro Steering Committee, and
      will be updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any Working Group
      participation fees, and are decided as described in the Eclipse Foundation Bylaws and detailed in the Eclipse
      Foundation Membership Agreement.
    </p>
    <p>
      There are 6 classes of Oniro working group membership &mdash; Strategic, Gold,
      Silver, Committer, Supporter and Guest.
    </p>
    <h3>Classes of Membership</h3>
    <h4>Strategic Members</h4>
    <p>
      Strategic Members are organizations that view these Working Group
      standards and technologies as strategic to their organization and are
      investing significant resources to sustain and shape the activities of
      this working group. These organizations want to drive the development and
      evolution of the Oniro technology ecosystem. Strategic Members of this
      working group must be at least a Contributing Member of the Eclipse
      Foundation, and have a minimum of three developers participating on its
      projects, and made at least # accepted commit or pull request to a
      related working group project within the last # months.
    </p>
    <h4>Gold Members</h4>
    <p>
      Gold Members are typically organizations that deliver products or services based upon related standards, specifications and technologies, or view Oniro working group&apos;s standards and technologies as strategic to their organization. These organizations want to participate in the development and direction of an open ecosystem related to this working group, and to participate actively in the definition, evolution, and promotion of the specifications and technologies. Gold Members of this working group must be at least a Contributing Member of the Eclipse Foundation.
    </p>
    <h4>Silver Members</h4>
    <p>
      Silver Members are typically organizations that deliver products or services based upon related
      standards, specifications and technologies, or view Oniro working group’s standards and technologies as
      strategic to their organization. These organizations want to participate in the development and direction of
      an open ecosystem related to this working group. Silver Members of this working group must be at
      least a Contributing Member of the Eclipse Foundation.
    </p>
    <h4>Committer Members</h4>
    <p>
      Committers are individuals who, through a process of meritocracy defined by the Eclipse Development
      Process, are able to contribute and commit code to the Eclipse Foundation projects included in the scope of
      this working group. Committers may become members of the working group by virtue of working for a
      member organization, or may choose to complete the membership-at-large process independently if they
      are not, along with completing this working group’s Participation Agreement once defined. For further
      explanation and details, see the
      <a href="/membership/become_a_member/committer.php">Eclipse Committer Membership</a> page.
    </p>
    <h4>Supporting Members</h4>
    <p>
      Supporting Members are: (a) organizations that want to participate in an
      open ecosystem related to this working group and wish to show their
      initial support for it, and (b) organizations which are at least
      Contributing members of the Eclipse Foundation. Organizations may be
      Supporting Members for a maximum period of 1 year only, after which time
      they are expected to change their membership to a different level.
      Supporting Members may be invited to participate in committee meetings at
      the invitation of the respective committee, but under no circumstances do
      Supporting Members have voting rights in any working group body.
      Supporting Members may not benefit from or participate in any Oniro
      compatibility branding program. Further, Supporting Members are not
      considered Participant Members as that term is defined in the Eclipse
      Foundation Specification Process. Supporting Members are required to
      execute the Working Group's Participation Agreement.
    </p>
    <h4>Guest Members</h4>
    <p>Guest Members are organizations which are Associate members of the Eclipse Foundation. Typical guests
      include R&D partners, universities, academic research centers, etc. Guests may be invited to participate in
      committee meetings at the invitation of the respective committee, but under no circumstances do Guest
      members have voting rights. Guest members are required to execute the Working Group&apos;s Participation
      Agreement.
    </p>
    <h3>Membership Summary</h3>
    <table class="table table-bordered">
      <tbody class="text-center">
        <tr>
          <td> </td>
          <td>
            Strategic Member
          </td>
          <td>
            Gold Member
          </td>
          <td>
            Silver Member
          </td>
          <td>
            Committer Member
          </td>
          <td>
            Supporting Member
          </td>
          <td>
            Guest Member
          </td>
        </tr>
        <tr>
          <td class="text-left">
            Member of the Steering Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
          <td>
            N/A
          </td>
        </tr>
        <tr>
          <td class="text-left">
            Member of the Marketing Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
          <td>
            N/A
          </td>
          <td>
            Invitation Only
          </td>
        </tr>
        <tr>
          <td class="text-left">
            Member of the Specification Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected &#42;
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
          <td>
            Invitation Only
          </td>
        </tr>
      </tbody>
    </table>
    <p>
      &#42; Gold Members will elect as a class one voting representative to the Specification Committee. In addition, all Gold Members, other than the elected Gold Member representative, may appoint an observer (i.e., non-voting) to the Specification Committee.
    </p>

    <h4>Special Interest Groups</h4>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within
      the Working Group with a focus to collaborate around a particular topic
      or domain of direct interest to the working group. SIGs are designed to
      drive the objectives of a subset of the Members of the Working Group in
      helping them achieve a dedicated set of goals and objectives. The scope
      of the SIG must be consistent with the scope of the Working Group
      Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each
      SIG may be either temporary or a permanent structure within the working
      group. SIGs can be disbanded at any time by self selection presenting
      reasons to and seek approval from the Steering Committee. Steering
      Committees may disband a SIG at any time for being inactive or non
      compliant with the Working Group's Charter, or by request of the SIG
      itself. SIGs operate as a non-governing Body of the Working Group. There
      are no additional annual fees to Members for participation in a SIG.
    </p>

    <h4>Sponsorship</h4>
    <p>
      Sponsors are companies or individuals who provide money or services to the
      working group on an ad hoc basis to support the activities of the Working Group
      and its managed projects. Money or services provided by sponsors are used as
      set forth in the working group annual budget. The working group is free to
      determine whether and how those contributions are recognized. Under no
      condition are sponsorship monies refunded.
    </p>
    <p>
      Sponsors need not be members of the Eclipse Foundation or of the Working
      Group.
    </p>

    <h2>Governance</h2>
    <p>This Oniro Working Group is designed as:</p>
    <ul>
      <li>
        A vendor-neutral, without borders, member driven organization, that is open, respectful and
        inclusive to all;
      </li>
      <li>
        A means to support the organization and sustainability of a community of developers that design, produce and maintain the different Oniro technologies as well as a user ecosystem, through an open source development model;
      </li>
      <li>
        A means to foster a vibrant and sustainable ecosystem of different research and commercial organizations from a wide variety of industries, like consumer electronics, IoT device makers, system integrators, board vendors, independent software vendors, etc. to ensure the sustainability of the community effort as well as the commercial success of the Oniro technologies.
      </li>
    </ul>
    <p>
      In order to implement these principles, the following governance bodies have been defined (each a &#34;Body&#34;):
    </p>
    <ul>
      <li>The Steering Committee</li>
      <li>The Marketing and Brand Committee</li>
      <li>The Specification Committee</li>
    </ul>

    <h3>Governing Bodies</h3>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>
        Define and manage which Eclipse Foundation projects are included within the scope of this working
        group.
      </li>
      <li>Ensure the consistency of logo usage and other marketing materials.</li>
      <li>Define and manage the roadmap.</li>
      <li>Review and approve this charter.</li>
      <li>
        Review and approve brand positioning, communication strategy, and any trademark policy referred
        to it by the Marketing and Brand Committee.
      </li>
      <li>Define the annual fees for all classes of the working group members.</li>
      <li>Establish an annual program plan.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Review on a regular basis the new member pipeline.</li>
      <li>
        Approve (if any) customizations to the Eclipse Foundation Specification Process (EFSP) recommended by the
        Specification Committee.
      </li>
      <li>
        Approve the creation of subcommittees and define the purpose, scope, and membership of each
        such subcommittee.
      </li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>
        Each Strategic Member of the working group is entitled to a seat on the Steering Committee.
      </li>
      <li>
        Two seats are allocated to Gold Members via election. The Gold Member seats are allocated following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
      </li>
      <li>
        One seat is allocated to Silver Members via election. The Silver Member seat is allocated following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
      </li>
      <li>
        One seat is allocated to Committer Members via election. The Committer Member seat is allocated
        following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
      </li>
      <li>
        The Committee elects a chair of the Steering Committee. This chair is elected among the members of
        the Committee. They will serve for a 12 month period or until their successor is elected and
        qualified, or as otherwise provided for in this Charter.
      </li>
      <li>
        The OpenAtom Foundation is entitled to a non-voting seat on the
        Steering Committee. This seat does not count towards quorum.
      </li>
    </ul>
    <h4>Meeting Management</h4>
    <ul>
      <li>The Steering Committee meets at least quarterly.</li>
      <li>
        As prescribed in the Eclipse Foundation Working Group Process, all meetings related to the working
        group will follow a prepared agenda and minutes are distributed two weeks after the meeting and
        approved at the next meeting at the latest, and shall in general conform to the Eclipse Foundation
        Antitrust Policy.
      </li>
    </ul>
    <h3>Marketing and Brand Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Marketing and Brand Committee members are required to:</p>
    <ul>
      <li>Define strategic marketing and communication goals and objectives for the working group.</li>
      <li>
        Coordinate and support the implementation of developer and end-user outreach programs.
      </li>
      <li>
        Raise issues for discussion and provide feedback to the Foundation on the execution of marketing,
        brand, and communications activities for the working group.
      </li>
      <li>
        Assist the Eclipse Foundation in the working group’s participation in conferences and events related
        to the working group.
      </li>
      <li>
        Define and communicate the trademark policy and brand positioning, if applicable, and refer to it for
        approval by the Steering Committee.
      </li>
      <li>
        Collaborate with other working group members, or relevant external stakeholders to develop
        co-marketing strategies, amplify messaging on social channels, and share best practices.
      </li>
      <li>
        Coordinate marketing and ecosystem development actions in cooperation
        with OpenHarmony.
      </li>
    </ul>
    <p>
      Committee members are expected to be leaders in communicating key messaging on behalf of the working
      group, and to play a leadership role in driving the overall success of the working group marketing efforts. In
      particular, members are encouraged to engage their respective marketing teams and personnel to amplify
      and support the achievement of the working group’s goals and objectives.
    </p>
    <h4>Composition</h4>
    <ul>
      <li>
        Each Strategic Member of the working group is entitled to a seat on the Marketing and Brand Committee.
      </li>
      <li>
        One seat is allocated to Gold Members via election. The Gold Member seat is allocated following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
      </li>
      <li>
        One seat is allocated to Silver Members via election. The Silver Member seat is allocated following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
      </li>
      <li>
        The Committee elects a chair of the Steering Committee. This chair is elected among the members of
        the Committee. They will serve for a 12 month period or until their successor is elected and
        qualified, or as otherwise provided for in this Charter.
      </li>
      <li>
        The OpenAtom Foundation is entitled to a non-voting seat on the
        Marketing and Brand Committee. This seat does not count towards
        quorum.
      </li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Marketing and Brand Committee meets at least twice a year.</p>
    <h3>Specification Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Specification Committee members are required to:</p>
    <ul>
      <li>
        Ensure that all specification projects operate in an open, transparent, and vendor-neutral fashion in
        compliance with the specification process.
      </li>
      <li>
        Ensure that Oniro specifications that are inherited from OpenHarmony
        are appropriate and consistent with the objectives of this charter.
      </li>
      <li>
        Apply and govern the specification projects in the scope of this working group according to the
        approved specification process.
      </li>
      <li>
        Approve specifications for adoption by the community.
      </li>
      <li>
        Work with the related Project Management Committee (PMC) to ensure that the EFSP is complied
        with by all related working group specification projects.
      </li>
      <li>
        Define (if any) customizations to the EFSP. The EFSP with the approved customizations is the
        specification process to be used by all specifications related to this working group, and refer for
        approval by the Steering Committee.
      </li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>
        Each Strategic Member of the working group is entitled to a seat on the Specification Committee.
      </li>
      <li>
        One seat is allocated to Gold Members via election. The Gold Member seat is allocated
        following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
        In addition, each Gold Member of the working group, other than the elected Gold Member representative, is entitled to appoint an observer (non-voting) to the Specification Committee.
      </li>
      <li>
        One seat is allocated to Silver Members via election. The Silver Member seat is allocated following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
      </li>
      <li>
        One seat is allocated to Committer Members via election. The Committer Member seat is allocated
        following the Eclipse &#34;Single Transferable Vote&#34;, as defined in the Eclipse Foundation Bylaws.
      </li>
      <li>
        Any additional individuals as designated from time to time by the
        Executive Director.
      </li>
      <li>
        The Committee elects a chair of the Steering Committee. This chair is elected among the members of
        the Committee. They will serve for a 12 month period or until their successor is elected and
        qualified, or as otherwise provided for in this Charter.
      </li>
      <li>
        The OpenAtom Foundation is entitled to a non-voting seat on the
        Specification Committee. This seat does not count towards quorum.
      </li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Specification Committee meets at least once a quarter.</p>

    <h2>Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this working group, unless otherwise specified. For
      all matters related to membership action, including without limitation: meetings, quorum, voting, vacancy, resignation or removal, the respective terms set forth in the Eclipse
      Foundation Bylaws apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member organization they are representing
      at any time by providing written notice to the Steering Committee. In the event a Body member is
      unavailable to attend or participate in a meeting of the Body, they may be represented by another Body
      member by providing written proxy to the Body’s mailing list in advance. As per the Eclipse Foundation Bylaws, a
      representative shall be immediately removed from the Body upon the termination of the membership of
      such representative’s Member organization.
    </p>
    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body are
      determined by a simple majority of the representatives represented at a committee
      meeting at which a quorum is present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors
      approve a specification license; (ii) approving specifications for
      adoption; (iii) modifying the working group charter; (iv) approving or
      changing the name of the working group; and (v) approving changes to
      annual Member contribution requirements; any such actions must be
      approved by no less than two-thirds (2/3) of the representatives
      represented at a committee meeting at which a quorum is present.
    </p>
    <h3>Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee, Specification Committee, and the Marketing
      Committee.</p>
    <p>
      All representatives shall hold office until their respective successors are appointed or elected, as applicable.
      There shall be no prohibition on re-election or re-designation of any representative following the completion
      of that representative&#39;s term of office.
    </p>
    <h4>Appointed Members</h4>
    <p>
      Representatives appointed on behalf of their Member organization shall serve in such capacity on committees until the earlier of their removal by their respective appointing Member organization or as otherwise provided for in this Charter.
    </p>
    <h4>Elected Representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve for a 12 month period,
      or until their respective successors are elected and qualified, or as otherwise provided for in this Charter.
      Procedures governing elections of Representatives may be established pursuant to resolutions of the
      Steering Committee provided that such resolutions are not inconsistent with any provision of this Charter.
    </p>
    <h3>Meetings Management</h3>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all
      meetings related to the working group will follow a prepared agenda and
      minutes are distributed two weeks after the meeting and approved at the
      next meeting at the latest, and shall in general conform to the Eclipse
      Foundation Antitrust Policy.
    </p>
    <h4>Meeting Frequency</h4>
    <p>
      Each governing body meets at least twice a year. All meetings may be held at any place that has been
      designated from time-to-time by resolution of the corresponding Body. All meetings may be held remotely
      using phone calls, video calls, or any other means as designated from time-to-time by resolution of the
      corresponding Body.
    </p>
    <h4>Place of Meetings</h4>
    <p>
      All meetings may be held at any place that has been designated from time-to-time by resolution of the
      corresponding body. All meetings may be held remotely using phone calls, video calls, or any other means as
      designated from time-to-time by resolution of the corresponding body.
    </p>
    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been provided to
      each of the representatives at least fifteen (15) calendar days prior to such meeting, which
      notice will identify all potential actions to be undertaken by the Body at the Body meeting. No representative
      will be intentionally excluded from Body meetings and all representatives shall receive notice of the meeting
      as specified above; however, Body meetings need not be delayed or rescheduled merely because one or
      more of the representatives cannot attend or participate so long as at least a quorum of the Body is
      represented at the Body meeting.
    </p>
    <h4>Actions</h4>
    <p>
      The body may undertake an action only if it was identified in a body meeting notice or otherwise identified
      in a notice of special meeting.
    </p>
    <h4>Invitations</h4>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right to vote.</p>

    <h2>Working Group Annual Participation Fees Schedule A</h2>
    <p>
      The following Participation Fees have been established by the Oniro Steering Committee. These fees are in addition to each Member&#39;s
      <a href="/membership/#tab-fees">Membership Fees in the Eclipse Foundation</a>.
    </p>
    <h3>Oniro Strategic Member Annual Participation Fees</h3>
    <p>Strategic members are required to execute the Oniro Working Group Participation Agreement.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="75%">Corporate Revenue</th>
          <th class="text-center" width="25%">Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €1 billion</td>
          <td class="text-center">€300 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €500 million but less than or equal to €1 billion
          </td>
          <td class="text-center">€200 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €100 million but less than or equal to €500
            million
          </td>
          <td class="text-center">€100 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €10 million but less than or equal to €100
            million
          </td>
          <td class="text-center">€50 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td class="text-center">€25 000</td>
        </tr>
      </tbody>
    </table>
    <h3>Oniro Gold Member Annual Participation Fees</h3>
    <p>Gold members are required to execute the Oniro Working Group Participation Agreement.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="75%">Corporate Revenue</th>
          <th class="text-center" width="25%">Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €1 billion</td>
          <td class="text-center">€100 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €500 million but less than or equal to €1 billion
          </td>
          <td class="text-center">€75 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €100 million but less than or equal to €500
            million
          </td>
          <td class="text-center">€50 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €10 million but less than or equal to €100
            million
          </td>
          <td class="text-center">€25 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td class="text-center">€15 000</td>
        </tr>
      </tbody>
    </table>
    <h3>Oniro Silver Member Annual Participation Fees</h3>
    <p>
      Silver members are required to execute the Oniro Working Group Participation Agreement.
    </p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="75%">Corporate Revenue</th>
          <th class="text-center" width="25%">Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €1 billion</td>
          <td class="text-center">€30 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €500 million but less than or equal to €1 billion
          </td>
          <td class="text-center">€25 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €100 million but less than or equal to €500
            million
          </td>
          <td class="text-center">€10 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €10 million but less than or equal to €100
            million
          </td>
          <td class="text-center">€5 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td class="text-center">€2 500</td>
        </tr>
      </tbody>
    </table>
    <h3>Oniro Supporter Member Annual Participation Fees</h3>
    <p>
      Supporter members pay no annual fees, but are required to execute the
      Oniro Working Group Participation Agreement
    </p>
    <h3>Oniro Guest Member Annual Participation Fees</h3>
    <p>
      Guest members pay no annual fees, but are required to execute the Oniro Working Group Participation Agreement.
    </p>
    <h3>Oniro Committer Member Annual Participation Fees</h3>
    <p>
      Committer members pay no annual fees, but are required to execute the Open Working Group Participation Agreement.
    </p>

    <p class="margin-top-20"><strong>Charter History</strong></p>
    <ul>
      <li>v0.1 - Initial draft</li>
      <li>v0.2 - Refinements based on feedback of initial draft</li>
      <li>v0.3 - Errata updates</li>
      <li>v0.4 - Modified to expand to 5 classes of Membership</li>
      <li>v0.5 - Amended Steering Committee Composition</li>
      <li>v1.0 - Ratified by Steering Committee and approved by Executive Director</li>
      <li>
        v1.1 - Amended to support the OpenAtom Foundation Agreement, added
        Supporter Member Class of Membership and other standard revisions
      </li>
    </ul>
  </div>
</div>
