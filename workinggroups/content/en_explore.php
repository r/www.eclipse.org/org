<?php

/**
 * Copyright (c) 2019, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Zhou Fang (Eclipse Foundation)
 * Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<nav>
  <p>
    <b>Jump to Section</b>
  </p>
  <ul>
    <li role="presentation">
      <a href="#working-groups">Working Groups</a>
    </li>
    <li role="presentation">
      <a href="#interest-groups">Interest Groups</a>
    </li>
  </ul>
</nav>

<section id="working-groups">
  <h2>Working Groups</h2>
  <div class="eclipsefdn-wgs-list">
    <div class="text-center">
      <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom-20"></i>
      <span class="sr-only">Loading...</span>
    </div>
  </div>
</section>

<section id="interest-groups">
  <h2>Interest Groups</h2>
  <div class="row display-flex flex-wrap">
    <div id="ig-jakarta-future" class="col-xs-24 col-sm-12 margin-bottom-20 wg-item wg-active-item">
      <div class="bordered-box bordered-box-light flex-column">
        <div class="box-header vertical-align flex-column-mobile padding-left-10">
          <div class="collaboration-card-header col-xs-24 col-sm-16 vertical-align justify-left-desktop">
            <a href="https://projects.eclipse.org/interest-groups/jakarta-ee-future-directions">
              <img style="height:40px;" src="/org/workinggroups/assets/images/ig-jakarta-ee-future-directions.png" alt="Jakarta EE Future Directions">
            </a>
          </div>
          <div class="col-xs-24 col-sm-8 fw-700 vertical-align">
            Status: Active
          </div>
        </div>
        <div class="flex-grow flex-column padding-30">
          <div class="flex-grow">
            <p>
              Java will continue to be a mission critical language for organizations of all sizes well into the future.
              This interest group is intended to bring together people from inside and outside of Java related working groups,
              influencers, and other interested parties. It will take on the role of ideation, research, and presentation of recommendations,
              working closely with Jakarta EE and MicroProfile working groups to ensure the ideas are scoped, prioritized, and understood.
              Recommendations from this interest group are non-binding on the working groups and are intended to provide insights and ideas that working groups do not have time to consider.
              <a href="https://projects.eclipse.org/interest-groups/jakarta-ee-future-directions">Learn more</a>
            </p>
          </div>
          <div class="row">
            <div class="col-sm-24 col-md-offset-15 col-md-9 margin-top-10 padding-5">
              <div class="btn-row">
                <a href="https://accounts.eclipse.org/contact/membership" class="btn btn-lg btn-neutral btn-block text-wrap">Not an Eclipse Member?</a>
                <a href="/collaborations/interest-groups/contact/" class="btn btn-lg btn-primary btn-block text-wrap">Eclipse Member ready to join now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="ig-models-privacy-engineering" class="col-xs-24 col-sm-12 margin-bottom-20 wg-item wg-active-item">
      <div class="bordered-box bordered-box-light flex-column">
        <div class="box-header vertical-align flex-column-mobile padding-left-10">
          <div class="collaboration-card-header col-xs-24 col-sm-16 vertical-align justify-left-desktop">
            <a href="https://projects.eclipse.org/interest-groups/models-privacy-engineering">
              <img style="height:40px;" src="/org/workinggroups/assets/images/ig-models-for-privacy-engineering.png" alt="Models for Privacy Engineering">
            </a>
          </div>
          <div class="col-xs-24 col-sm-8 fw-700 vertical-align">
            Status: Active
          </div>
        </div>
        <div class="flex-grow flex-column padding-30">
          <div class="flex-grow">
            <p>
              Sharing practices for privacy engineering based on models.
              This interest group focuses on the sharing of practices for privacy engineering
              based on the use of models. <a href="https://projects.eclipse.org/interest-groups/models-privacy-engineering">Learn more</a>
            </p>
          </div>
          <div class="row">
            <div class="col-sm-24 col-md-offset-15 col-md-9 margin-top-10 padding-5">
              <div class="btn-row">
                <a href="https://accounts.eclipse.org/contact/membership" class="btn btn-lg btn-neutral btn-block text-wrap">Not an Eclipse Member?</a>
                <a href="/collaborations/interest-groups/contact/" class="btn btn-lg btn-primary btn-block text-wrap">Eclipse Member ready to join now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="ig-openmobility" class="col-xs-24 col-sm-12 margin-bottom-20 wg-item wg-active-item">
      <div class="bordered-box bordered-box-light flex-column">
        <div class="box-header vertical-align flex-column-mobile padding-left-10">
          <div class="col-xs-24 col-sm-16 vertical-align justify-left-desktop">
            <a href="https://openmobility.eclipse.org/">
              <img style="height:40px;" src="https://www.eclipse.org/org/workinggroups/assets/images/wg-openmobility.svg" alt="openMobility Interest Group's logo">
            </a>
          </div>
          <div class="col-xs-24 col-sm-8 fw-700 vertical-align">
            Status: Active
          </div>
        </div>
        <div class="flex-grow flex-column padding-30">
          <div class="flex-grow">
            The openMobility Interest Group drives the evolution and broad adoption of mobility modelling and simulation technologies.
            <a href="https://openmobility.eclipse.org/">Learn more</a>
          </div>
          <div class="row">
            <div class="col-sm-24 col-md-offset-15 col-md-9 margin-top-10 padding-5">
              <div class="btn-row">
                <a href="https://accounts.eclipse.org/contact/membership" class="btn btn-lg btn-neutral btn-block text-wrap">Not an Eclipse Member?</a>
                <a href="/collaborations/interest-groups/contact/" class="btn btn-lg btn-primary btn-block text-wrap">Eclipse Member ready to join now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<style>
  @media all and (-ms-high-contrast:none) {
    div.bordered-box {
      height: auto;
    }
  }

  .collaboration-card-header {
    min-height: 8rem;
  }
</style>
