<?php
/**
 * Copyright (c) 2017, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>November, 2020</p>
    <h2>Definitions</h2>
    <p>The following terms are used in this document.</p>
    <ul>
      <li>Research Consortium (RC) is a specific Eclipse Working Group dedicated to the support and promotion of a research platform, excluding the industrialization of this platform.</li>
      <li>Technology Readiness Level (<a href="https://en.wikipedia.org/wiki/Technology_readiness_level" target="_blank">TRL</a>) is a scale used to describe the maturity of a technology. It was originally defined by NASA in the 70’s and is widely adopted in the embedded systems domain.</li>
    </ul>
    <h2>The GEMOC Initiative</h2>
    <p><a href="http://gemoc.org" target="_blank">The GEMOC initiative</a> is an international initiative to develop, coordinate, and disseminate Research &amp; Transfer efforts on the use of modeling languages in the context of the globalization of modeling languages. As we use the term, the globalization of modeling languages entails the use of multiple modeling languages to support the coordinated development of the diverse aspects of a system. The GEMOC Initiative has developed techniques, frameworks, and environments to facilitate the creation, integration, and automated processing of heterogeneous modeling languages.</p>
    <p>GEMOC focuses on three design and validation issues common in complex software-intensive systems:</p>
    <ul>
      <li>Consideration of various concerns: Multiple stakeholders are involved in the design process, each with a specific domain expertise. Stakeholders express their perspective in their own language, which then must be composed for global analysis and execution.</li>
      <li>Integration of heterogeneous parts: Complex systems integrate different devices specialized for different applications designed to deliver a global service. Therefore, communication and synchronization must be modeled to compose heterogeneous parts and characterize the emerging behavior.</li>
      <li>Fostering evolution and openness: It is not possible to establish an exhaustive, finite list of domain languages, communication and timing models. Thus, tools and environments must be open and allow for the evolution or creation of languages and models.</li>
    </ul>
    <h2>Vision/Mission of the GEMOC Research Consortium</h2>
    <h3>WHY create a Research Consortium (RC)?</h3>
    <p>The community of the GEMOC Initiative had reached a level of maturity that requires an organization (the consortium) to:</p>
    <ul>
      <li>Sustain the GEMOC studio as a research platform</li>
      <li>Support experiments with new solutions, demonstrators and pilot projects by academia and industry</li>
      <li>Foster relationships between academia and industry</li>
      <li>Foster industrial transfer and innovation</li>
    </ul>
    <h3>WHAT does the GEMOC (RC) do?</h3>
    <ul>
      <li>Host the GEMOC studio research platform and ensure the constant availability of the research platform through adequate integration testing and continuous integration</li>
      <li>Coordinate and operate the co-funding of the GEMOC research platform for sustainability</li>
      <li>Plan and coordinate evolutionary initiatives among the stakeholders through the use of mailing lists, forums and bug trackers</li>
      <li>Share the benefits of pilot projects</li>
      <li>Foster exchange between academia and industry, as well as collaboration on research projects</li>
    </ul>
    <h3>WHO participates in the GEMOC RC?</h3>
    <p>Researchers and practitioners, both from academia and industry, who want to collaborate on the topic of the globalization of modeling languages are the main target group. Joining the GEMOC initiative allows them to share, discuss, exchange ideas and collaborate with other members, taking advantage of the research framework proposed by the research consortium as well as infrastructure and collaboration tools. These tools include mailing lists, model repositories, an integrated studio, continuous integration, etc.</p>
    <h3>HOW does the GEMOC RC accomplish its goals?</h3>
    <p>By fostering and leveraging collaboration among members and ensuring the availability of the GEMOC Studio as a research platform, the GEMOC RC lays the groundwork for the fulfillment of its mission.</p>
    <h2>Technical Scope</h2>
    <p>The technical scope of the GEMOC Research Consortium (RC) is focused on <a href="https://hal.inria.fr/hal-00994551" target="_blank">the globalization of modeling languages</a>. The more detailed scope of the research topics addressed by the RC are proposed by the Steering Committee and approved by the advisory board of the GEMOC initiative.</p>
    <p>Projects hosted in the context of the GEMOC Research Consortium must have a TRL level below or equal to 5. This means that the GEMOC RC hosts advanced research prototypes that should be used only for inconsequential (play) examples, academic use cases, demonstrators, and industrial pilot projects. The GEMOC RC explicitly excludes the objective of increasing the maturity level of these projects to make them ready for operational deployment (TRL&gt;=6). If members of the RC need to industrialize a technology hosted in the GEMOC RC, they are invited to fork the project and host it at Eclipse, PolarSys, or one of the PolarSys Industry Consortia.</p>
    <p>The default IP due diligence level for GEMOC RC projects is the Type A as described by the <a href="https://eclipse.org/org/documents/Eclipse_IP_Policy.pdf" target="_blank">Eclipse Foundation, Inc. IP Policy</a> document. In the context of the GEMOC RC, projects are published under the Eclipse Public License.</p>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents and Processes</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Working Group Participation Agreement</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
    <p>In the event of any conflict between the terms set forth in the GEMOC RC Charter and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, processes, or policies shall take precedence.</p>
    <p>The GEMOC RC will apply the Eclipse Foundation processes where applicable.</p>
    <h3>GEMOC Research Consortium Community</h3>
    <p>Projects hosted by the Eclipse Foundation in the context of the GEMOC Research Consortium apply the Eclipse development process and best practices of openness, transparency and meritocracy. As such, these projects are open to participation by developers or contributors based on their merits. They can submit contributions to the project that will be considered for inclusion by the project committers. Committer status is acquired by contributors after election by existing committers as described in the <a href="https://www.eclipse.org/projects/handbook/#elections-committer" target="_blank">Eclipse Development Process</a>.</p>
    <h3>GEMOC Research Consortium Supporting Members</h3>
    <p>Supporting Members are academic and industrial partners that financially support the GEMOC Research Consortium in order to sustain the GEMOC studio as a research platform. Additionally, supporting members benefit from:</p>
    <ul>
      <li>Privileged collaboration and partnerships with the GEMOC community, and access to state of the art documents and software components</li>
      <li>Publicity and value creation through the GEMOC initiative and the GEMOC RC</li>
    </ul>
    <p>Any partner can become a supporting member of the GEMOC RC, either through a specific Eclipse membership (for Eclipse members) or a donation (for non-members). The minimum annual fee to become a supporting member is 5000€ for industrial partners, and 2000€ for academic or non-profit partners. Multi-year donations are possible.</p>
    <h3>GEMOC Research Consortium Steering Committee</h3>
    <p>The responsibilities of the Steering Committee are as follows:</p>
    <ul>
      <li>Coordination and integration of the related Eclipse projects</li>
      <li>Coordination of the GEMOC studio contributors</li>
      <li>Community coordination and management</li>
      <li>Management of the Eclipse GEMOC Research Consortium</li>
    </ul>
    <p>The Steering Committee also manages the budget of the GEMOC RC. The expected expenses include, but are not limited to:</p>
    <ul>
      <li>GEMOC studio maintenance: consulting with Eclipse platform experts to sustain the availability and usability of the GEMOC studio as a research platform. It includes release engineering, continuous integration, documentation and API/architecture management</li>
      <li>Funding of outreach activities (travel, event sponsorships, demo materials) for specific conferences and events</li>
    </ul>
    <p>The Steering Committee is composed of members elected by existing members (co-optation) based on meritocracy. They are selected from the community or the supporting members of the GEMOC RC. The Steering Committee is initially composed of founding members of the GEMOC initiative and main contributors to the GEMOC studio.</p>
    <p>A member of the Steering Committee shall be deemed to be in good standing, and thus eligible to vote on issues coming before the Steering Committee, if the member has attended (in person or telephonically) a minimum of three (3) of the last four (4) meetings (if there have been at least four meetings), unless such absence has been approved by the other members of the Steering Committee. The term of a Steering Committee member lasts as long as the member is active and in good standing. A member of the Steering Committee shall be removed after a vote of 2/3 of the other members if the member is not in good standing. The Steering Committee should then co-opt another member.</p>
    <p>Appointed representatives on the Body may be replaced by the Member organization they are representing at any time by providing written notice to the Steering Committee. In the event a Body member is unavailable to attend or participate in a meeting of the Body, they may be represented by another Body member by providing written proxy to the Body’s mailing list in advance. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the Body upon the termination of the membership of such representative’s Member organization.</p>
    <p>GEMOC RC members or any other interested parties can be invited to the meetings but have no vote.</p>
    <h2>Services</h2>
    <h3>Infrastructure</h3>
    <p>The GEMOC RC uses the normal infrastructure provided to Eclipse projects, including, among others, mailing lists, forums, bug trackers, source code repositories, continuous integration servers, build servers, and web sites.</p>
    <h3>Budget management</h3>
    <p>The Eclipse Foundation manages contracts (additional memberships and donations) for the GEMOC RC and collects a management fee of 15% of the contracted amounts.</p>
    <h3>Community building</h3>
    <p>The Eclipse Foundation will provide access to its network of developers to facilitate the community building and the dissemination of the GEMOC innovations. The EF will use its communication channels such as its Newsletter, Twitter, or LinkedIn channels to promote key GEMOC events and encourage its members to join and participate in the GEMOC research consortium.</p>
    <h2>Miscellaneous</h2>
    <h3>Update of the charter</h3>
    <p>This charter should be updated by the Steering Committee, after a positive vote of 2/3 of the Steering Committee members.</p>

    <p><strong>Version History</strong></p>
    <ul>
      <li>Nov. 2020: Updates in support of the Eclipse Foundation corporate restructuring</li>
    </ul>
  </div>
</div>