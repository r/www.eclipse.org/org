<?php
/**
 * Copyright (c) 2011, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <h2>Goals and Vision</h2>
    <p>Location based analysis was once the exclusive domain of specialists and a peripheral
      activity for many organizations. As technology advanced, its use shifted by adding spatial
      capabilities to mainstream consumer technology and enterprise IT.</p>
    <p>Trends such as the decreased cost and increased computing power of devices, often portable,
      have significantly increased the rate of data generation and expectations that the data be
      easily consumable anywhere at any time. Expectations have shifted from asynchronous processing
      of data to real time.</p>
    <p>These represent new demands for technology. The LocationTech Industry Working Group aims at
      answering these needs by:</p>
    <ul>
      <li>Providing a set of industry-friendly open source tools, frameworks, and components</li>
      <li>Providing collaborative means to make open innovation easier</li>
      <li>Fostering exchanges between academics, industrial partners, and community</li>
      <li>Operating software repositories, build chains, test facilities, etc.</li>
      <li>Managing the quality and maturity of tools and components from early research prototypes
        through obsolescence</li>
      <li>Ensuring open innovation through the sharing of the research, development and maintenance
        efforts as far as possible</li>
      <li>Organizing sustainable commercial services and ecosystems around those components</li>
    </ul>
    <p>Recognizing projects maturity and company know-how and commitment through a branding process
      available only to member organizations.</p>
    <h2>
      <a name="Core_domains"></a>Core domains
    </h2>
    <p>This group focuses on software, data formats, data exchange protocols, frameworks,
      techniques, and tools to fulfil its goal and vision, including:</p>
    <ul>
      <li>Desktop, Mobile, and Web Mapping</li>
      <li>Model driven design</li>
      <li>Data capture, exchange, and processing</li>
    </ul>
    <p>Aggregating and relating data from diverse sources</p>
    <p>Other domain specific techniques and tools may be proposed for acceptance by the Steering
      Committee.</p>
    <h2>
      <a name="Component_Management"></a>Component Management
    </h2>
    <p>A number of good technologies answering some of our needs already exist in open source. The
      group does not intend to re-develop existing components. Specific issues like durability,
      interoperability, and intellectual property stewardship are not always taken into account and
      the group will strive to influence these areas and re-implement only as a last resort. There
      are two kinds of projects are therefore supported in this group:</p>
    <dl>
      <dt>Hosted Projects</dt>
      <dd>The technical artefacts are hosted in our facilities.</dd>
      <dt>External Projects</dt>
      <dd>The technical artefacts are hosted elsewhere.</dd>
    </dl>
    <h2>
      <a name="Governance_and_Precedence"></a>Governance and Precedence
    </h2>
    <p>
      <strong>Applicable Documents</strong>
    </p>
    <ul>
      <li><a href="http://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf">Eclipse
          Bylaws</a></li>
      <li><a href="http://www.eclipse.org/org/industry-workgroups/industry_wg_process.php">Industry
          Working Group Process</a></li>
      <li><a
        href="http://www.eclipse.org/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202010_01_05%20Final.pdf"
      >Eclipse Membership Agreement</a></li>
      <li><a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
          Development Process</a></li>
    </ul>
    <p>All Members must be parties to the Eclipse Membership agreement, including the requirement
      set forth in Section 2.2 to follow the Bylaws and then-current policies of the Eclipse
      Foundation. In the event of any conflict between the terms set forth in this Working Group's
      Charter and the Eclipse Foundation Bylaws, Membership Agreement, Eclipse Development Process,
      Eclipse Industry Working Group Process, or any policies of the Eclipse Foundation, the terms
      of the Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take
      precedence.</p>
    <h2>
      <a name="IP_Management"></a>IP Management
    </h2>
    <p>The Intellectual Property Policy of the Eclipse Foundation will apply to all activities in
      this group. The group will follow the Eclipse Foundation's IP due diligence process in order
      to provide clean open source software released under licenses approved by the group and the
      Eclipse Foundation Board of Directors. Approved licenses for this group include EPL, MIT, and
      BSD. This list may be amended from time to time by the group and the Eclipse Foundation Board
      of Directors. The EPL license is the recommended license for projects in this group.</p>
    <h2>
      <a name="Development_Process"></a>Development Process
    </h2>
    <p>The Eclipse Foundation Development Process will apply to all open source projects hosted by
      this group. In particular, the project lifecycle model and review process will be followed by
      the open source projects hosted by this group.</p>
    <h2>
      <a name="Membership"></a>Membership
    </h2>
    <p>
      An entity must be at least a <a
        href="http://www.eclipse.org/membership/become_a_member/membershipTypes.php#solutions"
      >Solutions Member</a> of the Eclipse Foundation, have executed the IWG Participation
      Agreement, and adhere to the requirements set forth in this Charter to participate. The
      Eclipse Solution Member fees appear in the tables below for convenience only: they are decided
      as described in the <a
        href="http://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf"
      >Eclipse bylaws</a> and detailed in the <a
        href="http://www.eclipse.org/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202010_01_05%20Final.pdf"
      >Eclipse membership agreement</a>.
    </p>
    <h3>
      <a name="Classes_of_membership"></a>Classes of membership
    </h3>
    <h4>
      <a name="Strategic_members"></a>Strategic members
    </h4>
    <p>Strategic Members are organizations that view location technology as strategic to their
      organization and are investing resources to sustain and shape the activities of this group.</p>
    <h5>
      <a name="LocationTech_Strategic_Member_Fees"></a>LocationTech Strategic Member Fees
    </h5>
    <table class="table table-striped">
      <tr>
        <th>
          <p>Revenue</p>
        </th>
        <th>
          <p>Eclipse SolutionMembership</p>
        </th>
        <th>
          <p>LocationTech StrategicMembership</p>
        </th>
        <th>
          <p>Total</p>
        </th>
      </tr>
      <tr>
        <td>
          <p>&gt;$250 million</p>
        </td>
        <td>
          <p>$20,000</p>
        </td>
        <td>
          <p>$30,000</p>
        </td>
        <td>
          <p>$50,000</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&gt;$100 million &lt;= $250 million</p>
        </td>
        <td>
          <p>$15,000</p>
        </td>
        <td>
          <p>$25,000</p>
        </td>
        <td>
          <p>$40,000</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&gt;$50 million &lt;= $100 million</p>
        </td>
        <td>
          <p>$10,000</p>
        </td>
        <td>
          <p>$20,000</p>
        </td>
        <td>
          <p>$30,000</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&gt;$10 million &lt;= $50 million</p>
        </td>
        <td>
          <p>$7,500</p>
        </td>
        <td>
          <p>$15,000</p>
        </td>
        <td>
          <p>$22,500</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&lt;$10 million</p>
        </td>
        <td>
          <p>$5,000</p>
        </td>
        <td>
          <p>$10,000</p>
        </td>
        <td>
          <p>$15,000</p>
        </td>
      </tr>
    </table>
    <h4>
      <a name="Participant_members"></a>Participant members
    </h4>
    <p>Participant Members are organizations that view location technology as an important part of
      their organization's activities. These organizations want to participate in the development of
      the location technology ecosystem.</p>
    <h5>
      <a name="LocationTech_Participant_Member_Fees"></a> LocationTech Participant Member Fees
    </h5>
    <table class="table table-striped">
      <tr>
        <th>
          <p>Revenue</p>
        </th>
        <th>
          <p>Eclipse SolutionMembership</p>
        </th>
        <th>
          <p>LocationTech ParticipantMembership</p>
        </th>
        <th>
          <p>Total</p>
        </th>
      </tr>
      <tr>
        <td>
          <p>&gt;$250 million</p>
        </td>
        <td>
          <p>$20,000</p>
        </td>
        <td>
          <p>$10,000</p>
        </td>
        <td>
          <p>$30,000</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&gt;$100 million &lt;= $250 million</p>
        </td>
        <td>
          <p>$15,000</p>
        </td>
        <td>
          <p>$7,500</p>
        </td>
        <td>
          <p>$22,500</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&gt;$50 million &lt;= $100 million</p>
        </td>
        <td>
          <p>$10,000</p>
        </td>
        <td>
          <p>$3,500</p>
        </td>
        <td>
          <p>$13,500</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&gt;$10 million &lt;= $50 million</p>
        </td>
        <td>
          <p>$7,500</p>
        </td>
        <td>
          <p>$2,250</p>
        </td>
        <td>
          <p>$9,750</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&lt;$10 million</p>
        </td>
        <td>
          <p>$5,000</p>
        </td>
        <td>
          <p>$1,000</p>
        </td>
        <td>
          <p>$6,000</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>&lt;$1 million &amp; &lt; 10 employees</p>
        </td>
        <td>
          <p>$1,500</p>
        </td>
        <td>
          <p>$500</p>
        </td>
        <td>
          <p>$2,000</p>
        </td>
      </tr>
    </table>
    <h4>
      <a name="Guests"></a>Guests
    </h4>
    <p>Guests are organizations who have been invited for one year by the Steering Committee to
      participate in some aspects of the activities of the group. Typical guests includes R&amp;D
      partners, academic entities and potential future full fledged members who want to have an
      inner look before deciding of their strategy. Even if guests can be invited to some meetings,
      they have no right to vote. Invitations may be renewed by the Steering Committee. Guests will
      be required to sign a participation agreement.</p>
    <h4>
      <a name="Committers"></a>Committers
    </h4>
    <p>
      Committer Members are individuals who through a process of meritocracy defined by the Eclipse
      Development Process are able to contribute and commit code to software projects. Committers
      may be members by virtue of working for a member organization, or may choose to complete the
      membership process independently if they are not. For further explanation and details, see the
      <A HREF="http://www.eclipse.org/membership/become_a_member/committer.php">Eclipse Committer
        Membership</a> page.
    </p>
    <h3>
      <a name="Membership_Summary"></a>Membership Summary
    </h3>
    <table class="table table-striped">
      <tr>
        <th></th>
        <th>
          <p>Strategic Member</p>
        </th>
        <th>
          <p>Participant Member</p>
        </th>
        <th>
          <p>Committer</p>
        </th>
        <th>
          <p>Guest</p>
        </th>
      </tr>
      <tr>
        <td>
          <p>Member of the Steering Committee</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>Elected</p>
        </td>
        <td>
          <p>Elected</p>
        </td>
        <td>
          <p>Invited</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Member of the Architecture Committee</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>Elected</p>
        </td>
        <td>
          <p>Elected</p>
        </td>
        <td>
          <p>Invited</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Member of the Marketing Committee</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>Elected</p>
        </td>
        <td>
          <p>Elected</p>
        </td>
        <td>
          <p>Invited</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Access to the open collaboration infrastructure</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>X</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>IP Due diligence</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>X</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Write Access to open source code repositories</p>
        </td>
        <td>
          <p>-</p>
        </td>
        <td>
          <p>-</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>-</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Access to the LTS Build Infrastructure</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>-</p>
        </td>
        <td>
          <p>-</p>
        </td>
        <td>
          <p>-</p>
        </td>
      </tr>
      <tr>
        <td>
          <p>Access to LTS binary releases</p>
        </td>
        <td>
          <p>X</p>
        </td>
        <td>
          <p>-</p>
        </td>
        <td>
          <p>-</p>
        </td>
        <td>
          <p>-</p>
        </td>
      </tr>
    </table>
    <p>
      Services are detailed in section <a href="http://wiki.eclipse.org/Location/Charter#Services">Services</a>
    </p>
    <p>All matters related to Membership in the Eclipse Foundation and this group will be governed
      by the Eclipse Foundation Bylaws, Membership Agreement and Eclipse Industry Working Group
      Process. These matters include, without limitation delinquency, payment of dues, termination,
      resignation, reinstatement, assignment, and the distribution of assets upon dissolution.</p>
    <p>Members who resign, or otherwise terminate their membership in the group lose their rights to
      access and use any private assets and data of the group after the date of the termination.</p>
    <p>The private data of terminated Members, such test cases or custom build chains shall be
      archived and the archive sent back to their copyright holders under an open source license
      specified by the Steering Committee. The private data of Members terminated can be removed
      from storages forty five (45) days after the Termination. Nevertheless, the Members terminated
      can request to the Steering Committee to store for two (2) years their archived private data.
      This request shall be sent within thirty (30) days after the Termination and can be accepted
      or not, on the sole discretion of the Steering Committee.</p>
    <p>The survival of any licenses to group private assets acquired by a Member during the period
      of Membership shall be as specified in the license.</p>
    <h2>
      <a name="Services"></a>Services
    </h2>
    <h3>
      <a name="Open_source_collaboration_infrastructure"></a> Open source collaboration
      infrastructure
    </h3>
    <p>This group leverages open source collaboration infrastructure managed by the Eclipse
      Foundation. As such, source code repositories, Bugzilla, wikis, forums, project mailing lists,
      and other services provided as the Open Source collaboration infrastructure are publicly
      visible. Committers have write access to this infrastructure, and as such have the rights and
      obligations as set forth in the Eclipse Development Process, and the various Eclipse Committer
      Agreements. The catalog of components hosted by this group is part of this collaboration
      infrastructure.</p>
    <h3>
      <a name="Hosting_custom_builds_on_the_group.27s_infrastructure"></a> Hosting custom builds on
      the group's infrastructure
    </h3>
    <p>The capability of using the group's test and build infrastructure in order to create
      member-specific bundles exists. These bundles can be private to the member who defines and
      uses them.</p>
    <h3>
      <a name="IP_due_diligence"></a>IP due diligence
    </h3>
    <p>IP due diligence is necessary to check that the committers have the right to open-source the
      code they contribute.</p>
    <p>It is also necessary to check that the different integrated components have compatible
      licenses. We allow not only EPL licensed components, but also MIT &amp; BSD licensed
      components.</p>
    <h2>
      <a name="Governance"></a>Governance
    </h2>
    <p>This group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the following governance bodies have been defined
      (each a &quot;Body&quot;):</p>
    <ul>
      <li>The Steering Committee</li>
      <li>The General Assembly</li>
      <li>The Architecture Committee</li>
      <li>The Marketing Committee</li>
      <li>Project Management Committees</li>
    </ul>
    <h3>
      <a name="Common_Dispositions"></a>Common Dispositions
    </h3>
    <p>The dispositions below apply to all governance bodies for this group, unless otherwise
      specified. For all matters related to membership action, including without limitation:
      meetings, quorum, voting, electronic voting action without meeting, vacancy, resignation or
      removal, the terms set forth in Section 6 of the Eclipse Foundation Bylaws apply.</p>
    <h4>
      <a name="Good_Standing"></a>Good Standing
    </h4>
    <p>A representative shall be deemed to be in Good Standing, and thus eligible to vote on issues
      coming before the Body he participates to, if the representative has attended (in person or
      telephonically) a minimum of three (3) of the last four (4) Body meetings (if there have been
      at least four meetings). Appointed representatives on the Body may be replaced by the Member
      organization they are representing at any time by providing written notice to the Steering
      Committee. In the event a Body member is unavailable to attend or participate in a meeting of
      the Body, he or she may send a representative and may vote by proxy, which shall be included
      in determining whether the representative is in Good Standing. As per the Eclipse Foundation
      Bylaws, a representative shall be immediately removed from the Body upon the termination of
      the membership of such representative&rsquo;s Member organization.</p>
    <h4>
      <a name="Voting"></a>Voting
    </h4>
    <h5>
      <a name="Super_Majority"></a>Super Majority
    </h5>
    <p>For actions (i) requesting that the Eclipse Foundation Board of Directors approve an
      additional distribution license for projects; (ii) amending the terms of the group's
      Participation agreement; (iii) approving or changing the name of the group; (iv) approving
      changes to annual Member contribution requirements; any such actions must be approved by no
      less than two-thirds (2/3) of the representatives in Good Standing represented at a Steering
      Committee meeting at which a quorum is present.</p>
    <h4>
      <a name="Term_and_Dates_of_elections"></a>Term and Dates of elections
    </h4>
    <p>This section only applies to the Steering Committee, Architecture Committee, and the
      Marketing Committee.</p>
    <p>All representatives shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or re-designation of any
      representative following the completion of that representative&rsquo;s term of office.</p>
    <h5>
      <a name="Strategic_Members"></a>Strategic Members
    </h5>
    <p>Strategic Members Representatives shall serve in such capacity on committees until the
      earlier of their removal by their respective appointing Member organization or as otherwise
      provided for in this Charter.</p>
    <h5>
      <a name="Elected_representatives"></a>Elected representatives
    </h5>
    <p>Elected representatives shall each serve one-year terms and shall be elected to serve from
      April 1 to March 31 of each calendar year, or until their respective successors are elected
      and qualified, or as otherwise provided for in this Charter. Procedures governing elections of
      Representatives may be established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this Charter.</p>
    <h4>
      <a name="Meetings_Management"></a>Meetings Management
    </h4>
    <h5>
      <a name="Place_of_meetings"></a>Place of meetings
    </h5>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding Body. All meetings may be held remotely using phone calls,
      video calls or any other mean as designated from time-to-time by resolution of the
      corresponding Body.</p>
    <h5>
      <a name="Regular_meetings"></a>Regular meetings
    </h5>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representative in Good Standing at least thirty (30) calendar days
      prior to such meeting, which notice will identify all potential actions to be undertaken by
      the Body at the Body meeting. No representative will be intentionally excluded from Body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the Body is
      represented at the Body meeting. Electronic voting shall be permitted in conjunction with any
      and all meetings of the Body the subject matter of which requires a vote of the Body to be
      delayed until each such representative in attendance thereat has conferred with his or her
      respective Member organization as set forth in Section <a
        href="http://wiki.eclipse.org/Location/Charter#Voting"
      >Voting</a> above.
    </p>
    <h5>
      <a name="Actions"></a>Actions
    </h5>
    <p>The Body may undertake an action only if it was identified in a Body Meeting notice or
      otherwise identified in a notice of special meeting.</p>
    <h4>
      <a name="Invitations"></a>Invitations
    </h4>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right
      of vote.</p>
    <h3>
      <a name="Steering_Committee"></a>Steering Committee
    </h3>
    <h4>
      <a name="Powers_and_Duties"></a>Powers and Duties
    </h4>
    <p>Steering committee members are required to:</p>
    <ul>
      <li>Define the strategy of the group</li>
      <li>Define the global roadmap</li>
      <li>Discuss and amend the charter and the participation agreement</li>
      <li>Define the budget and fees each year</li>
      <li>Invite guest members</li>
    </ul>
    <h4>
      <a name="Composition"></a>Composition
    </h4>
    <ul>
      <li>Each strategic member of the group has a seat on the Steering Committee.</li>
      <li>At least one seat is allocated to Participant Members. An additional seat on the Committee
        shall be allocated to the Participant Members for every additional five (5) seats beyond one
        (1) allocated to Strategic Members. Participant Member seats are allocated following the
        Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. An additional seat on the Committee
        shall be allocated to the Comitter Members for every additional five (5) seats beyond one
        (1) allocated to Strategic Members. Committer Member seats are allocated following the
        Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</li>
      <li>The Steering Committee elects among its members a chair who will represent the IWG. They
        will serve from April 1 to March 31 of each calendar year, or until their respective
        successors are elected and qualified, or as otherwise provided for in this Charter.</li>
    </ul>
    <h4>
      <a name="Meeting_Management"></a>Meeting Management
    </h4>
    <p>The Steering Committee meets at least twice a year.</p>
    <h3>
      <a name="General_Assembly"></a>General Assembly
    </h3>
    <h4>
      <a name="Powers_and_Duties_2"></a>Powers and Duties
    </h4>
    <ul>
      <li>Approve changing the name of the group.</li>
    </ul>
    <h4>
      <a name="Composition_2"></a>Composition
    </h4>
    <p>Each Strategic and Participant Member of the IWG has a seat on the General Assembly.</p>
    <h4>
      <a name="Meeting_Management_2"></a>Meeting Management
    </h4>
    <p>The General Assembly meets at least once a year.</p>
    <h3>
      <a name="Architecture_Committee"></a>Architecture Committee
    </h3>
    <h4>
      <a name="Powers_and_Duties_3"></a>Powers and Duties
    </h4>
    <p>Architecture Committee members are required to:</p>
    <ul>
      <li>Ensure the technical consistency of projects</li>
      <li>Ensure that projects achieve objectives</li>
      <li>Recommend technologies</li>
      <li>Establish technical guidelines</li>
      <li>The Architecture Committee validates new project proposals</li>
    </ul>
    <h4>
      <a name="Composition_3"></a>Composition
    </h4>
    <ul>
      <li>Each strategic member of the group has a seat on the Committee.</li>
      <li>At least one seat is allocated to Participant Members. An additional seat on the Committee
        shall be allocated to the Participant Members for every additional five (5) seats beyond one
        (1) allocated to Strategic Members. Participant Member seats are allocated following the
        Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. An additional seat on the Committee
        shall be allocated to the Comitter Members for every additional five (5) seats beyond one
        (1) allocated to Strategic Members. Committer Member seats are allocated following the
        Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected
        among the members of the Committee. They will serves from April 1 to March 31 of each
        calendar year, or until their successor is elected and qualified, or as otherwise provided
        for in this Charter.</li>
    </ul>
    <h4>
      <a name="Meeting_Management_3"></a>Meeting Management
    </h4>
    <p>The Architecture committee meets at least twice a year.</p>
    <h3>
      <a name="The_Marketing_Committee"></a>The Marketing Committee
    </h3>
    <h4>
      <a name="Powers_and_Duties_4"></a>Powers and Duties
    </h4>
    <p>The Marketing Committee members are required to:</p>
    <ul>
      <li>Ensure the consistency of logo usage and other marketing materials</li>
      <li>Define &amp; Follow maketing and communication activities</li>
      <li>Ensure consistency of annual conferences</li>
      <li>Ensure consistency of regional conferences</li>
    </ul>
    <h4>
      <a name="Composition_4"></a>Composition
    </h4>
    <ul>
      <li>Each strategic member of the group has a seat on the Committee.</li>
      <li>At least one seat is allocated to Participant Members. An additional seat on the Committee
        shall be allocated to the Participant Members for every additional five (5) seats beyond one
        (1) allocated to Strategic Members. Participant Member seats are allocated following the
        Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. An additional seat on the Committee
        shall be allocated to the Comitter Members for every additional five (5) seats beyond one
        (1) allocated to Strategic Members. Committer Member seats are allocated following the
        Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected
        among the members of the Committee. They will serves from April 1 to March 31 of each
        calendar year, or until their successor is elected and qualified, or as otherwise provided
        for in this Charter.</li>
    </ul>
    <h4>
      <a name="Meeting_Management_4"></a>Meeting Management
    </h4>
    <p>Marketing Committee meets at least twice a year.</p>
    <h3>
      <a name="Project_Management_Committees"></a>Project Management Committees
    </h3>
    <p>Any Project Management Committee (PMC) established by the group and the Eclipse Foundation
      Board of Directors shall be governed by the Eclipse Development Process.</p>
  </div>
</div>