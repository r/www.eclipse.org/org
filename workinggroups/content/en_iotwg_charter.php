<?php

/**
 * Copyright (c) 2011, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *   Zachary Sabourin (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 2.1 Revision History at End</p>
    <h2>Mission Statement</h2>
    <p>
      The mission of Eclipse IoT is to provide a forum for individuals and organizations to build and promote open source software, open standards and open collaboration models needed to create a scalable, enterprise-ready and open Internet of Things.
    </p>

    <h2>Vision and Scope</h2>
    <p>
      The main objective of the Eclipse IoT Working group is to encourage, develop, and promote open source solutions that will be able to compete successfully in challenging and fragmented markets of IoT ecosystems all around the globe. For this the working group aims for:
    </p>
    <ul>
      <li>
        Creating a vendor-neutral member-driven community of open source projects that fosters inclusion and collaboration.
      </li>
      <li>
        Reflecting the achievements of the community through strong and distinctive brands such as Eclipse IoT and Edge Native.
      </li>
      <li>
        Striving to support a wide range of embedded platforms, programming models, connection types, and communication protocols.
      </li>
      <li>
        Providing a vivid platform to collaborate on widely accepted IoT and Edge Computing architectural guidelines, that
      </li>
      <ul>
        <li>
          Promote open standard communication protocols to deal with IoT requirements and constraints such as; power, CPU, cost, connection availability, and bandwidth.
        </li>
        <li>
          Help to balance the right level of coupling between applications, systems and communication interfaces.
        </li>
        <li>
          Bridge the gap between the Information Technology and Operational Technology ecosystems.
        </li>
        <li>
          Deliver a unified vision, architecture blueprints, and code for the seamless development and operation of edge native applications.
        </li>
      </ul>

      <li>
        Fostering Open Source IoT development solutions, like development environments and development boards, that
      </li>
      <ul>
        <li>
          Integrate well with open source Enterprise and Web development tools and environments.
        </li>
        <li>
          Assert modular, state-of-the-art application architecture as well as the appropriate reuse of software components (e.g. drivers, communication protocols).
        </li>
        <li>
          Support developers who need to integrate IoT, Enterprise, and Web application systems. e.g. hardware and infrastructure costs, no relevant software engineering environment, proprietary interfaces, numerous and complex programming models.
        </li>
      </ul>

      <li>
        Facilitating open source support for IoT-oriented middleware, including IoT integration with established middleware solutions.
      </li>
    </ul>
    <p>
      To achieve these goals, the Eclipse IoT working group will pursue initiatives related to the following domains:
    </p>
    <ul>
      <li>
        Development tools, including simulators and emulators
      </li>
      <li>
        Reference architectures
      </li>
      <li>
        Programming models and frameworks
      </li>
      <li>
        Open and standard communication protocols
      </li>
      <li>
        Open APIs
      </li>
    </ul>

    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>
      The following governance documents are applicable to this charter, each of which can be found on the <a href="/org/documents">Eclipse Foundation Governance Documents</a> page or the <a href="/legal">Eclipse Foundation Legal Resources</a> page:
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this working group’s charter and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Specification Process, Working Group Process or any policies of the Eclipse Foundation, the terms of the respective Eclipse Foundation Bylaws, Membership Agreement, process or policy shall take precedence.
    </p>

    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a <a href="/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation, have executed the Eclipse IoT working group participation agreement and adhere to the requirements set forth in this Charter to participate.
    </p>
    <p>
      The participation fees associated with each of these membership classes are shown in the Annual Participation Fees section. These are annual fees, and are established by the Eclipse IoT Steering Committee, and will be updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any Working Group membership fees, and are decided as described in the Eclipse Foundation Bylaws and detailed in the Eclipse Foundation Membership Agreement.
    </p>
    <p>
      There are four classes of Eclipse IoT working group membership - Leader, Innovator, Supporter and Guest.
    </p>

    <h3>Classes of Membership</h3>
    <h4>Leader Members</h4>
    <p>
      Leader Members are organizations that view this Working Group's standards and technologies as strategic to their organization and are investing significant resources to sustain and shape the activities of this working group. Leader Members of this working group and must be at least a Contributing Member of the Eclipse Foundation, and are expected to have a minimum of two developers participating in its projects.
    </p>
    <h4>Innovator Members</h4>
    <p>
      Innovator Members are organizations that view Eclipse IoT technologies as important to their corporate and product strategy. They are typically organizations that deliver products or services based upon related standards, specifications and technologies, or view this working group’s standards and technologies as strategic to their organization. These organizations want to participate in the development and direction of an open ecosystem related to this working group. Innovator Members of this working group must be at least a Contributing Member of the Eclipse Foundation.
    </p>
    <h4>Supporter Members</h4>
    <p>
      Supporter Members are organizations that want to participate in an open ecosystem related to this working group and wish to show their support for it. Supporter Members of this working group must be at least a Contributing Member of the Eclipse Foundation.
    </p>
    <h4>Guest Members</h4>
    <p>
      Guest Members are organizations which are Associate members of the Eclipse Foundation. Typical guests include R&D partners, universities, academic research centers, etc. Guests may be invited to participate in committee meetings at the invitation of the respective committee, but under no circumstances do Guest members have voting rights. Guest members are required to execute the working group’s Participation Agreement.
    </p>

    <h2>Membership Summary</h2>
    <table class="table table-stripped text-center">
      <thead>
        <tr>
          <th class='text-center'>Committee Representation</th>
          <th class='text-center'>Leader</th>
          <th class='text-center'>Innovator</th>
          <th class='text-center'>Supporter</th>
          <th class='text-center'>Guest</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Steering Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>N/A</td>
          <td>N/A</td>
        </tr>
        <tr>
          <td>Marketing and Brand Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>N/A</td>
          <td>N/A</td>
        </tr>
      </tbody>
    </table>

    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within the Working Group with a focus
      to collaborate around a particular topic or domain of direct interest to the working group. SIGs are
      designed to drive the objectives of a subset of the Members of the Working Group in helping them
      achieve a dedicated set of goals and objectives. The scope of the SIG must be consistent with the scope
      of the Working Group Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each SIG may be either temporary or
      a permanent structure within the working group. SIGs can be disbanded at any time by self selection
      presenting reasons to and seek approval from the Steering Committee. Steering Committees may
      disband a SIG at any time for being inactive or non compliant with the Working Group's Charter, or by
      request of the SIG itself. SIGs operate as a non-governing Body of the Working Group. There are no
      additional annual fees to Members for participation in a SIG.
    </p>

    <h2>Sponsorship</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to the working group on an ad
      hoc basis to support the activities of the Working Group and its managed projects. Money or services
      provided by sponsors are used as set forth in the working group annual budget. The working group is
      free to determine whether and how those contributions are recognized. Under no condition are
      sponsorship monies refunded.

    </p>
    <p>Sponsors need not be members of the Eclipse Foundation or of the Working Group.</p>

    <h2>Governance</h2>
    <p>This Eclipse IoT working group is designed as:</p>
    <ul>
      <li>a vendor-neutral, member-driven organization, </li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and developers define the roadmap collaboratively.</li>
    </ul>

    <h2>Governing Bodies</h2>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this working group.</li>
      <li>Ensure the consistency of logo usage and other marketing materials.</li>
      <li>Define and manage the technical roadmap.</li>
      <li>Review and approve this charter.</li>
      <li>Review and approve any trademark policy referred to it by the Marketing and Brand Committee.</li>
      <li>Define the annual fees for all classes of the working group members. </li>
      <li>Establish an annual program plan.</li>
      <li>Approve the annual budget based upon funds received through working group fees.</li>
      <li>Approve the creation of subcommittees and define the purpose, scope, and membership of each such subcommittee.</li>
      <li>Approve the creation and retirement of Special Interest Groups (SIGs).</li>
    </ul>
    <h4>Composition</h4>
    <p>Each Leader Member of the working group has a seat on the Steering Committee.</p>
    <p>
      One seat is allocated to Innovator Members. Innovator Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.
    </p>
    <p>
      The Committee elects a chair of the Steering Committee. This chair is elected among the members of the Committee. They will serve for a 12 month period, or until their successor is elected and qualified, or as otherwise provided for in this Charter. There is no limit on the number of terms the chair may serve.
    </p>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>

    <h3>Marketing and Brand Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Marketing and Brand Committee members are required to:</p>
    <ul>
      <li>Define strategic marketing priorities, goals, and objectives for the working group.</li>
      <li>Coordinate the implementation of developer and end-user outreach programs.</li>
      <li>Raise issues for discussion and provide feedback to the Foundation on the execution of marketing, brand, and communications activities for the working group.</li>
      <li>Provide requirements to the Eclipse Foundation for conferences and events related to the working group.</li>
      <li>Define the trademark policy, if applicable, and refer to it for approval by the Steering Committee.</li>
      <li>Collaborate with other working group members to develop co-marketing strategies, amplify messaging on social channels, and share best practices.</li>
    </ul>
    <p>
      Committee members are expected to be leaders in communicating key messaging on behalf of the working group, and to play a leadership role in driving the overall success of the working group marketing efforts. In particular, members are encouraged to engage their respective marketing teams and personnel to amplify and support the achievement of the working group’s goals and objectives.
    </p>
    <h4>Composition</h4>
    <ul>
      <li>Each Leader Member of the working group has a seat on the Marketing Committee.</li>
      <li>One seat is allocated to Innovator Members. Innovator Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Bylaws.</li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected among the members of the Committee. They will serve for a 12 month period, or until their successor is elected and qualified, or as otherwise provided for in this Charter. There is no limit on the number of terms the chair may serve. </li>
    </ul>

    <h4>Meeting Management</h4>
    <p>The Marketing and Brand Committee meets at least twice a year.</p>

    <h3>Common Dispositions</h3>
    <p>
      The dispositions below apply to all governance bodies for this working group, unless otherwise specified. For all matters related to membership action, including without limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy, resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member organization they are representing at any time by providing written notice to the Steering Committee. In the event a Body member is unavailable to attend or participate in a meeting of the Body, they may be represented by another Body member by providing written proxy to the Body’s mailing list in advance. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the Body upon the termination of the membership of such representative’s Member organization.
    </p>

    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body are determined by a simple majority of the representatives in Good Standing represented at a committee meeting at which a quorum is present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors approve a specification license; (ii) approving specifications for adoption; (iii) approving or changing the name of the working group; and (iv) approving changes to annual Member contribution requirements; any such actions must be approved by no less than two-thirds (2/3) of the representatives in Good Standing represented at a committee meeting at which a quorum is present.
    </p>

    <h3>Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee and the Marketing Committee.</p>
    <p>
      All representatives shall hold office until their respective successors are appointed or elected, as applicable. There shall be no prohibition on re-election or re-designation of any representative following the completion of that representative's term of office.
    </p>
    <h4>Leader Members</h4>
    <p>
      Leader Members Representatives shall serve in such capacity on committees until the earlier of their removal by their respective appointing Member organization or as otherwise provided for in this Charter.
    </p>
    <h4>Elected Representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve for a 12 month period, or until their respective successors are elected and qualified, or as otherwise provided for in this Charter. Procedures governing elections of Representatives may be established pursuant to resolutions of the Steering Committee provided that such resolutions are not inconsistent with any provision of this Charter.
    </p>

    <h3>Meetings Management</h3>
    <h4>Meeting Frequency</h4>
    <p>
      Each governing body meets at least twice a year. All meetings may be held at any place that has been designated from time-to-time by resolution of the corresponding Body. All meetings may be held remotely using phone calls, video calls, or any other means as designated from time-to-time by resolution of the corresponding Body.
    </p>
    <h4>Place of Meetings</h4>
    <p>
      All meetings may be held at any place that has been designated from time-to-time by resolution of the corresponding body. All meetings may be held remotely using phone calls, video calls, or any other means as designated from time-to-time by resolution of the corresponding body.
    </p>
    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been provided to each of the representatives in Good Standing at least fifteen (15) calendar days prior to such meeting, which notice will identify all potential actions to be undertaken by the Body at the Body meeting. No representative will be intentionally excluded from Body meetings and all representatives shall receive notice of the meeting as specified above; however, Body meetings need not be delayed or rescheduled merely because one or more of the representatives cannot attend or participate so long as at least a quorum of the Body is represented at the Body meeting. Electronic voting shall be permitted in conjunction with any and all meetings of the Body the subject matter of which requires a vote of the Body to be delayed until each such representative in attendance thereat has conferred with his or her respective Member organization as set forth in Section Voting above.
    </p>
    <h4>Actions</h4>
    <p>The body may undertake an action only if it was identified in a body meeting notice or otherwise identified in a notice of special meeting.</p>
    <h4>Invitations</h4>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right to vote.</p>
    <h4>Working Group Fees</h4>
    <p>
      The Steering Committee defines a fee structure and approves an annual budget based on resource requirements necessary to achieve the objectives of the group. Members agree to pay the annual fees as established by the Steering Committee.
    </p>
    <p>
      Established fees cannot be charged retroactively and any fee structure change will be communicated to all members via the mailing list and will be charged the next time a member is invoiced.
    </p>

    <h2>Working Group Annual Participation Fees Schedule A</h2>
    <p>The following fees have been established by the Eclipse IoT Steering Committee. These fees are in addition to each participant’s membership fees in the Eclipse Foundation.</p>
    <h3>Eclipse IOT Leader Member Annual Participation Fees</h3>
    <p>Leader members are required to execute theEclipse IoT Working Group Participation Agreement.</p>
    <table class="table table-stripped">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €1 billion</td>
          <td>€50 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €250 million but less than or equal to €1 billion</td>
          <td>€30 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €50 million but less than or equal to €250 million</td>
          <td>€15 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €10 million but less than or equal to €50 million</td>
          <td>€10 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td>€5 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than €1 million and < 10 employees</td>
          <td>€3 000</td>
        </tr>
      </tbody>
    </table>
    <h3>Eclipse IoT Innovator Member Annual Participation Fees</h3>
    <p>Innovator members are required to execute the Eclipse IoT Working Group Participation Agreement.</p>
    <table class="table table-stripped">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €1 billion</td>
          <td>€25 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €250 million but less than or equal to €1 billion</td>
          <td>€15 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €50 million but less than or equal to €250 million</td>
          <td>€10 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €10 million but less than or equal to €50 million</td>
          <td>€5 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td>€2 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than €1 million and < 10 employees</td>
          <td>€1 000</td>
        </tr>
      </tbody>
    </table>
    <h3>Eclipse IoT Supporter Member Annual Participation Fees</h3>
    <p>Supporter Members pay no annual fees, but are required to execute the Eclipse IoT Working Group Participation Agreement.</p>
    <h3>Eclipse IoT Guest Member Annual Participation Fees</h3>
    <p>Guest members pay no annual fees, but are required to execute the Eclipse IoT Working Group Participation Agreement.</p>

    <br>

    <p><strong>Charter History</strong></p>
    <ul>
      <li>V1.9 November, 2020 Updated in support of the Eclipse Foundation corporate restructuring.</li>
      <li>
        V2.0 March 19, 2021 Completely restructured following the new working group charter template. Renamed existing membership levels. Added Innovator level.
        <ul>
          <li>Approved by the Steering Committee, June 21, 2021</li>
          <li>Approved by Executive Director, June 22, 2021</li>
        </ul>
      </li>
      <li>V2.1 June, 2023 General refinements to align with Working Group Process updates</li>
    </ul>
  </div>
</div>
