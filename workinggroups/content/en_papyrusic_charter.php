<?php
/**
 * Copyright (c) 2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>
      <strong>October 17, 2015</strong>
    </p>
    <h2>Definitions</h2>
    <p>A list of definitions of terms used in this document.</p>
    <ul>
      <li><b>Model Based Engineering (MBE)</b>: We use the term <i>Model Based Engineering</i> (and
        the acronym MBE) as a generic term to refer to model-based or model-driven development. In
        this document, this term could be replaced by other terms used in the industry such as Model
        Driven Development (MDD), Model Driven Architecture (MDA), and Model Based System
        Engineering (MBSE).</li>
      <li><b>Papyrus Industry Consortium</b> is referred to as <i>Papyrus IC</i> in this document</li>
      <li><b>Papyrus Toolsuite</b>: We use the term <i>Papyrus Toolsuite</i> to refer to the
        solution composed of Papyrus and related open source components.</li>
      <li><b>Suppliers</b>: We use the term <i>Supplier</i> to refer to any organization providing
        commercial offerings related to the technologies developed in the scope of this consortium,
        including proprietary products, professional services, training, and mentoring.</li>
    </ul>
    <h2>Vision/Mission</h2>
    <ul type="disc">
      <li>WHY is it needed
        <ul>
          <li>To fulfill the overall MBE vision, the industry needs an open source alternative to
            existing proprietary modeling tools to eliminate vendor lock-in, provide the ability to
            develop new tool features and integrations as required, and ensure long-term
            availability</li>
          <li>To develop the advanced MBE tool offerings end-users need, suppliers need access to a
            customizable, extensible, and open tool platform</li>
          <li>To develop new MBE capabilities, and enable innovations and technology transfer,
            Research/Academia needs access to an open industrial platform that can be used for both
            research and teaching purposes</li>
        </ul>
      </li>
      <li>WHAT it does
        <ul>
          <li>Develop a customizable and extensible industrial-grade open source MBE tool suite
            based on the Papyrus/Eclipse platform, other key open source technologies, and leading
            industry standards</li>
          <li>Develop a MBE methods and processes body of knowledge comprising of agreed best
            practices and use cases guiding the usage of this tool suite.</li>
        </ul>
      </li>
      <li>WHO it does it for
        <ul>
          <li>or companies developing software-based systems, from Enterprise Software to Internet
            of Things (IoT) and Cyber-Physical Systems (CPS)</li>
        </ul>
      </li>
      <li>HOW it does what it does
        <ul>
          <li>By fostering and leveraging collaborations between members of a composed of end-users
            suppliers, and research/academia</li>
        </ul>
      </li>
    </ul>
    <h2>Overall Goals</h2>
    <ul type="disc">
      <li>Development of industrial-grade open source solution</li>
      <li style="list-style: none">
        <ul>
          <li>Plan and coordinate the development of an industrial-grade Papyrus Toolsuite to ensure
            both the availability of key capabilities, and the evolution and long-term availability
            of a complete MBE solution</li>
          <li>Capture, consolidate, and manage requirements from the different consortium members
            and application domains</li>
          <li>Define development priorities based on consortium members requirements/needs</li>
          <li>To meet this objective, a number of different aspects must be addressed, including
            QA/testing, customization, packaging, distribution, training, commercial support, and
            continuous evolution/integration</li>
        </ul>
      </li>
      <li>Joint development financing</li>
      <li style="list-style: none">
        <ul>
          <li>Coordinate investments from consortium members in the different aspects of the overall
            MBE solution to reduce development time, risks, and cost, and maximize ROI</li>
        </ul>
      </li>
      <li>Knowledge sharing</li>
      <li style="list-style: none">
        <ul>
          <li>Share MBE knowledge, expertise, and experiences among consortium members to enable
            broad adoption of the Papyrus Toolsuite and MBE in general</li>
          <li>Establish a MBE methods and processes body of knowledge to help improving development
            productivity and product quality</li>
        </ul>
      </li>
      <li>Promotion of open source solution</li>
      <li style="list-style: none">
        <ul>
          <li>Promote the use of the Papyrus Toolsuite as a leading industrial MBE solution</li>
        </ul>
      </li>
      <li>Development of the community</li>
      <li style="list-style: none">
        <ul>
          <li>Work together on the development of a vibrant community of end-users, suppliers, and
            research/academia focused on the development, promotion, and broad adoption of the
            Papyrus Toolsuite</li>
        </ul>
      </li>
      <li>Standardization</li>
      <li style="list-style: none">
        <ul>
          <li>Take leadership position in standardization initiatives to ensure that the open source
            solution is based on open industry standards</li>
          <li>Leverage existing standards developed by lead industry standard organizations,
            including OMG, ISO, and INCOSE</li>
          <li>Develop close relationship with those lead industry standard organizations to be in a
            position to influence the development of the required standards</li>
        </ul>
      </li>
      <li>Collaboration on research projects</li>
      <li style="list-style: none">
        <ul>
          <li>Define research priorities to ensure mid/long-term evolution of the open source
            modeling solution</li>
          <li>Foster collaborations between consortium members to drive strategic research projects
            to allow key evolutions</li>
        </ul>
      </li>
      <li>Contribution to MBE education and training</li>
      <li style="list-style: none">
        <ul>
          <li>Work in collaboration with academic partners and universities to help establishing
            high-quality education programs in MBE</li>
          <li>Encourage consortium members to directly contribute to the training of both
            undergraduate and graduate students</li>
        </ul>
      </li>
    </ul>
    <h2>Technical Scope Initial draft</h2>
    <p>It is very important to mention that the technical scope of the consortium will be defined
      and approved by the Steering Committee.</p>
    <p>Also, the technical scope will be periodically reviewed and updated by the Steering Committee
      based on the needs of the consortium members and the level of available resources.</p>
    <h2>Modeling Aspects</h2>
    <p>The scope of the Papyrus IC will include the following key modeling aspects</p>
    <ul>
      <li>System modeling</li>
      <li>Software modeling</li>
      <li>Architecture modeling</li>
      <li>Functional modeling</li>
      <li>Information/data modeling</li>
      <li>Business process modeling</li>
      <li>Requirements modeling</li>
    </ul>
    <p>Modeling will be based on use of standards (UML and SysML) and Domain Specific Modeling
      Languages (DSML) defined as extensions of standard languages, including UML-RT, RT-UML, and
      xtUML.</p>
    <h2>Membership</h2>
    <h3>Main elements of Membership Classes</h3>
    <ul type="disc">
      <li>Membership commitment is on an annual basis</li>
      <li style="list-style: none">
        <ul>
          <li>Membership is checked annually and can be terminated, suspended or changed to a
            different membership class by the Papyrus IC Steering Committee if the member fails to
            deliver membership fees or previously committed results.</li>
        </ul>
      </li>
      <li>Eclipse/Polarsys Requirements</li>
      <li style="list-style: none">
        <ul>
          <li>Be at least Eclipse Solution members</li>
          <li>Adhere to the Eclipse bylaws and processes</li>
          <li>Adhere to the Polarsys charter</li>
        </ul>
      </li>
      <li>Membership Fees</li>
      <li style="list-style: none">
        <ul>
          <li>The Papyrus IC is established as a Special Interest Group (SIG) of the Eclipse
            Polarsys WG</li>
          <li style="list-style: none">
            <ul>
              <li>As such, all Papyrus IC members are also members of Polarsys without any
                additional fees</li>
            </ul>
          </li>
          <li>Each Papyrus IC member must pay both Eclipse Membership fees and Polarsys WG
            Membership fees as defined below</li>
          <li style="list-style: none">
            <ul style="">
              <li>Papyrus IC members must at least be <i>Eclipse Solution members</i>. Eclipse
                membership fee details can be found on the <a
                href="/membership/become_a_member/membershipTypes.php"
              >Eclipse website</a>
              </li>
              <li>Papyrus IC members must pay Polarsys membership as defined in the <a
                href="/org/workinggroups/polarsys_charter.php"
              >Polarsys charter</a> with the following equivalence
              </li>
              <li style="list-style: none">
                <ul>
                  <li>Papyrus IC User Lead Member -Polarsys Steering Committee fees</li>
                  <li>Papyrus IC User Participant Member - Polarsys Participant Member fees</li>
                  <li>Papyrus IC Supplier Lead Member - Polarsys Participant Member fees</li>
                  <li>Papyrus IC Supplier Participant Member - Polarsys Participant Member fees</li>
                  <li>Papyrus Research/Academia Member Polarsys Guest member fees</li>
                  <li>Papyrus IC Observer Member - Polarsys Guest member fees</li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li>Resource allocation</li>
      <li style="list-style: none">
        <ul>
          <li>Number of days per year that a member company is committing to different aspects of
            the consortium activities as required by the different committees</li>
          <li>In the calculation of the overall member contribution, these resources will be
            accounted for at USD 1,200 per day</li>
        </ul>
      </li>
      <li>Technical contributions</li>
      <li style="list-style: none">
        <ul>
          <li>Commitment to make a given level of technical contributions to the development of
            specific aspects of the Papyrus Toolsuite</li>
          <li>Technical Contributions can take the form of</li>
          <li style="list-style: none">
            <ul>
              <li>Financial commitment to invest an amount of money in the development of specific
                technical aspects</li>
              <li style="list-style: none">
                <ul>
                  <li>In this case, the technical development is carried out by an external supplier
                  </li>
                </ul>
              </li>
              <li>SW engineers (e.g. developers, architects) dedicated to the development of
                specific technical aspects</li>
              <li style="list-style: none">
                <ul>
                  <li>Companies providing internal resources to work on development of open source
                    technologies approved by the Steering Committee can get recognized contributions
                    towards Technical Contributions</li>
                  <li>In the calculation of the technical contribution, these resources will be
                    accounted for at the following rate</li>
                  <li style="list-style: none">
                    <ul>
                      <li>Developer: USD 650 per day</li>
                      <li>Architect or Senior Developer: USD 750 per day</li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>Proprietary technology components contributed to the consortium as new open source
                components</li>
              <li style="list-style: none">
                <ul>
                  <li>In this case, the Steering Committee is responsible for getting an evaluation
                    of the transferred technology</li>
                </ul>
              </li>
            </ul>
          </li>
          <li>In all cases, the contributions need to be approved by the Steering Committee</li>
          <li>A list of examples of acceptable contributions is provided in section <i>Examples of
              Acceptable Contributions</i>
          </li>
        </ul>
      </li>
    </ul>
    <h2>Membership Classes</h2>
    <ul type="disc">
      <li>User Lead Member
        <p>User Lead members want to influence the development of the Papyrus Toolsuite and be an
          integral part of the group of organizations that govern the development of the open source
          MBE solution. They invest an essential amount of resources to sustain the Papyrus IC
          activities and contribute to the development of the open source technologies.</p>
        <p>Typical User Lead members are organizations that use MBE solutions at the core of their
          development process and who consider MBE solutions as a strategic asset.</p>
      </li>
      <li style="list-style: none">
        <ul>
          <li>Membership fees: Eclipse membership + Polarsys Steering Committee membership</li>
          <li>Resource allocation: minimum 30 person-days</li>
          <li>Technical contributions: minimum 100K USD</li>
          <li>Total contribution: USD 175K to 200K (depending on the membership fees)</li>
        </ul>
      </li>
      <li>Supplier Lead Member
        <p>Supplier Lead members want to influence the development of the Papyrus Toolsuite and be
          an integral part of the group of organizations that govern the development of the open
          source MBE solution. They invest an essential amount of resources to sustain the Papyrus
          IC activities and contribute to the development of the open source technologies. The main
          contributions from Supplier Lead members come in the form of technical contributions to
          the development of the different open source components.</p>
        <p>Typical Lead Supplier members are organizations that provide commercial offerings
          (products and/or services) based on the Papyrus Toolsuite and who consider these
          technologies as strategic to their business.</p>
        <ul>
          <li>Membership fees: Eclipse membership + Polarsys Participant membership</li>
          <li>Resource allocation minimum 30 person-days</li>
          <li>Technical contributions: minimum 100K USD</li>
          <li>Total contribution: USD 175K to 200K (depending on the membership fees)</li>
        </ul>
      </li>
      <li>Participant Member</li>
      <li style="list-style: none">
        <p>Participant members want to participate in the development of the Papyrus ecosystem. They
          contribute to the development and adoption of the Papyrus Toolsuite through different
          types of contributions.</p>
        <p>The Participant Member class includes any organization, user or supplier, small or large,
          that view the Papyrus Toolsuite as an important part of their corporate and product
          strategy.</p>
        <ul>
          <li>Membership fees: Eclipse membership + Polarsys Participant membership</li>
          <li>Resource allocation: minimum 10 person-days</li>
          <li>Technical contributions: minimum 10K USD</li>
          <li>Total contribution: USD 25K to 50K (depending on the membership fees)</li>
        </ul>
      </li>
      <li>Research/academia Member
        <p>Research/academia members are universities and research organizations that want to
          participate in the development of the overall MBE solution based on Papyrus by addressing
          different research aspects, use Papyrus for teaching/training purposes, and contribute to
          the overall development of the ecosystem.</p>
        <ul>
          <li>Membership fees: None Polarsys Guest membership status</li>
          <li>Resource allocation: 8 person-days</li>
          <li>Technical contributions: None</li>
          <li>Note</li>
          <li style="list-style: none">
            <ul>
              <li>Each research/academia member must be invited/sponsored by a User Lead or Supplier
                Lead member and approved by the Steering committee</li>
              <li>Research/academia membership is renewable on an annual basis</li>
            </ul>
          </li>
        </ul>
      </li>
      <li>Observer Member
        <p>Observer members are organizations who have been invited for one year by the Steering
          Committee of the Papyrus IC to participate to the activities of the consortium.</p>
        <p>Typical Observer members include RD partners, potential future full-fledged members who
          want to have a closer look before deciding on their strategy, and organizations that canÕt
          commit the required level of resources but can still contribute to the activities of the
          Papyrus IC in different ways.</p>
        <ul>
          <li>Annual fees: None Polarsys Guest membership status</li>
          <li>Resource allocation: 8 person-days</li>
          <li>Technical contributions: None</li>
          <li>Notes</li>
          <li style="list-style: none">
            <ul>
              <li>An Observer member must be invited/sponsored by a User Lead or Supplier Lead
                member and approved by the Steering committee</li>
              <li>Observer status is renewable on an annual basis</li>
              <li>While they don't have voting rights, Observer members can still collaborate with
                consortium members on technical developments (e.g. co-funding of development of
                specific open source components) and research projects, and contribute to the
                definition of the overall strategy and promotion of the Papyrus Toolsuite</li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
    <h2>Rights and Benefits for the Different Membership Levels</h2>
    <p>The table below identifies the rights and benefits of the members regarding the different
      committees of the Papyrus IC.</p>
    <ul>
      <li>"X" indicates that every member of that membership class has a seat on the corresponding
        committee</li>
      <li>"Elected" indicates that a number of seats on the corresponding committee will be
        allocated for that membership class based on the following rule
        <ul>
          <li>At least one seat will be allocated</li>
          <li>One additional seat will be added for every additional five (5) Lead member seats (the
            sum of User Lead and Supplier Lead member seats) beyond one (1)</li>
          <li>Participant member seats are allocated following the Eclipse single transferable vote,
            as defined in the Eclipse Bylaws.</li>
        </ul>
      </li>
      <li>"-" indicates that a member of that membership class has no seat on the corresponding
        committee</li>
    </ul>
    <p>A list of additional member benefits will be defined by the Steering Committee once
      established.</p>
    <!-- Membership Summary Table -->
    <p>
      <strong>Papyrus IC membership rights regarding committee participation</strong>
    </p>
    <table class="table table-striped">
      <tr>
        <th></th>
        <th><strong>User Lead Member</strong></th>
        <th><strong>Supplier Lead Member</strong></th>
        <th><strong>Participant Member</strong></th>
        <th><strong>Research/Academia Member</strong></th>
        <th><strong>Observer Member</strong></th>
      </tr>
      <tr>
        <td>General Assembly</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
      </tr>
      <tr>
        <td>Steering Committee</td>
        <td>X</td>
        <td>X</td>
        <td>Elected</td>
        <td>Elected</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Product Management Committee</td>
        <td>X</td>
        <td>X</td>
        <td>Elected</td>
        <td>-</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Architecture Committee</td>
        <td>X</td>
        <td>X</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Research/Academia Committee</td>
        <td>X</td>
        <td>X</td>
        <td>Elected</td>
        <td>Elected</td>
        <td>-</td>
      </tr>
    </table>
    <h2>Committees</h2>
    <p>Committee members and chairman will be elected and will serve from April 1 to March 31 of
      each calendar year, or until their respective successors are elected and qualified, or as
      otherwise provided for in this charter.</p>
    <h2>Steering Committee</h2>
    <p>The consortium is led by the steering committee that is composed of representatives of the
      different membership classes as defined in Table 1.</p>
    <ul>
      <li>Composition
        <ul>
          <li>Chairman: elected representative from the User Lead member companies</li>
          <li>Participants: see Table 1</li>
        </ul>
      </li>
      <li>Responsibilities
        <ul>
          <li>Define the overall strategic directions of the consortium</li>
          <li>Define and manage consortium budget</li>
          <li>Define and maintain technical/product scope, roadmap and development priorities</li>
          <li>Approve technical contributions of the consortium members</li>
        </ul>
      </li>
    </ul>
    <h2>Product Management Committee</h2>
    <p>The Product Management Committee reports to the Steering Committee</p>
    <ul>
      <li>Composition
        <ul>
          <li>Chairman: elected representative from the User Lead member companies</li>
          <li>Participants: see Table 1</li>
        </ul>
      </li>
      <li>Responsibilities
        <ul>
          <li>Define and maintain technical/product roadmap and development priorities</li>
          <li>Ensure quality of the different open source components</li>
          <li>Oversee the definition and delivery of product packages</li>
          <li>Define and implement product marketing strategy</li>
          <li>Define and implement the overall strategy for the development of the Papyrus ecosystem
          </li>
        </ul>
      </li>
    </ul>
    <h2>Architectural Committee</h2>
    <p>The Architectural Committee reports to the Steering Committee</p>
    <ul>
      <li>Composition
        <ul>
          <li>Chairman: elected representative from the User Lead member companies</li>
          <li>Participants: see Table 1</li>
        </ul>
      </li>
      <li>Responsibilities
        <ul>
          <li>Define and maintain the overall product architecture</li>
          <li>Ensure the consistency and integrity of the architecture and its different components
          </li>
          <li>Analyze and provide recommendations on potential tool integrations</li>
        </ul>
      </li>
    </ul>
    <h2>Research/Academia Committee</h2>
    <p>The Research/Academia Committee reports to the Steering Committee</p>
    <ul>
      <li>Composition
        <ul>
          <li>Chairman: elected representative from the User Lead member companies</li>
          <li>Participants: see Table 1</li>
        </ul>
      </li>
      <li>Responsibilities
        <ul>
          <li>Identify key research directions</li>
          <li>Interact with research groups involved in consortium activities</li>
          <li>Interact with funding agencies to promote research on Papyrus open source platform</li>
          <li>Participate to key research events (conferences, workshops, and commercial events) to
            promote Papyrus as a lead MBE platform for research projects</li>
          <li>Interaction with universities to help establishing high-quality training and education
            programs both at the undergraduate and graduate level</li>
          <li>Gather the training material exploited by the academics, related to the Papyrus
            Toolsuite</li>
          <li>Maintain an information set on advanced research, demos, and prototypes around the
            Papyrus Toolsuite (papers, press, videos, SW, etc.</li>
        </ul>
      </li>
    </ul>
    <h2>Examples of Acceptable Contributions</h2>
    <p>
      This section provides a list of examples of acceptable contributions towards Papyrus IC
      membership levels. <strong>In all cases, it is important to mention that the Steering
        Committee is responsible for approving the contributions and that all contributions must be
        explicitly approved.</strong>
    </p>
    <ul>
      <li>Monetary contributions
        <ul>
          <li>Funding of development projects in which they have a particular interest</li>
        </ul>
      </li>
      <li>Project contributions
        <ul>
          <li>Committers working on Eclipse projects&gt;</li>
          <li>Technical contributions done by non-committers to Eclipse projects</li>
        </ul>
      </li>
      <li>Customization and DSML
        <ul>
          <li>Development of new DSML for Papyrus</li>
        </ul>
      </li>
      <li>Integration
        <ul>
          <li>Investigate the potential integration of Papyrus IC solutions with other
            tools/solutions used in the industry</li>
          <li>Develop new integrations for Papyrus IC solutions</li>
        </ul>
      </li>
      <li>Testing
        <ul>
          <li>Testing of the different releases and the registration of new bugs</li>
          <li>Testing of bug fixes</li>
          <li>Contributions to the establishment of testing frameworks for the different Eclipse
            projects, including development of test cases for the different Eclipse projects</li>
        </ul>
      </li>
      <li>Product Management
        <ul>
          <li>Contributions to the creation and evolution of the Papyrus IC roadmap</li>
          <li>Management of specific development projects</li>
          <li>Contributions to the development of product requirements and project plan</li>
          <li>Contributions to the documentation of Papyrus IC solutions</li>
          <li>Development of open source tutorials for Papyrus IC solutions</li>
          <li>Creation of marketing materials (logos, icons, datasheets, posters, etc.) for Papyrus
            IC solutions</li>
        </ul>
      </li>
      <li>Support
        <ul>
          <li>Participation in open forums and mailing lists for Papyrus IC solutions</li>
        </ul>
      </li>
      <li>Promotion of the Papyrus IC solutions
        <ul>
          <li>Participation to industry events (e.g. conferences and workshops) to promote Papyrus
            IC solutions</li>
          <li>Presentation of Papyrus IC tutorials/training in public forums</li>
          <li>Writing white papers, data sheets, and experience reports for Papyrus IC solutions</li>
          <li>Evangelism and community development activities for the Papyrus IC</li>
        </ul>
      </li>
      <li>Research and scientific contributions - the goal to accept items like these would be to
        involve the scientific community and to encourage transition from science to practical
        application
        <ul>
          <li>Research projects on improving Papyrus</li>
          <li>Research papers on Papyrus and related technologies</li>
          <li>Case studies on Papyrus and related technologies (lead or participation)</li>
          <li>Scientific workshops and conferences (Participation, Organization, PC)</li>
        </ul>
      </li>
      <li>Participations to industry organizations - Such activities will help to promote the usage
        of Papyrus as an industrial tool as well as highlight how the tool can and is used in
        industry, and increase and enhance the visibility of Papyrus amongst potential users, and
        help promoting the achievements of the user community
        <ul>
          <li>Collaborative work on Industry specifications and standards that benefit, enhance, and
            promotes a Papyrus-based Model-Based -System Engineering, -Software Engineering, and/or
            -Software Development
            <ul>
              <li>E.g. contribution to OMG (UML, SysML,etc), ISO (standardization of specifications)
              </li>
            </ul>
          </li>
          <li>Participation and contribution to working groups promoting open source modeling,
            especially Papyrus, and other approaches to MBS
            <ul>
              <li>E.g. participation/contribution to INCOSE working groups such as model
                interoperability, SEBOK, and efforts aligned with INCOSE vision for MBSE</li>
            </ul>
          </li>
          <li>Participation in industry conferences and workshops to promote and demonstrate usage
            of Papyrus
            <ul>
              <li>E.g. INCOSE Symposium, Workshop, and Chapter events; EmbeddedWorld.</li>
            </ul>
          </li>
          <li>Collaboration with industries in the creation of papers, case studies, and experience
            reports where Papyrus solutions are used in an industrial context</li>
        </ul>
      </li>
      <li style="list-style: none">Contributions to MBE education and training
        <ul>
          <li>Contributions to the definition or improvement of MBE education and training programs
          </li>
          <li>Supervision or co-supervision of undergraduate and graduate students in fields of
            interest to the Papyrus IC</li>
        </ul>
      </li>
      <li>Other activities
        <ul>
          <li>Other activities/contributions as defined by the Papyrus IC Steering Committee</li>
        </ul>
      </li>
    </ul>
  </div>
</div>