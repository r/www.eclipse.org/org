<?php

/**
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">

    <h1><?php print $pageTitle; ?></h1>
    <p>Draft - Not Yet Approved</p>
    <p>Version 0.4 - Revision history at end of document</p>

    <h2>Vision and Scope</h2>
    <p>
      New government regulations on the software industry will impact open
      source communities. The community should support their successful
      implementation. This working group is intended to bring together key
      stakeholders from industry, small and medium enterprise (SME), research,
      and open source foundations to work with government in forging
      specifications that will enable industry to continue to leverage open
      source through the software supply chain to meet those regulatory
      requirements, and in turn will enable the open source projects to better
      meet industry's needs in this regard.
    </p>
    <p>
      Working within the Eclipse Foundation framework for developing
      Specifications, the working group will:
    </p>
    <ul>
      <li>
        Define software development processes - either current or adapted -
        which meet the regulatory compliance standards set forth by regulatory
        authorities. This is achieved through:
        <ul>
          <li>
            Identifying best practices, process and tools which may already
            exist within the open source ecosystem
          </li>
          <li>
            Identifying gaps and resources that may fill those gaps
          </li>
        </ul>
      </li>
      <li>
        Identify and assess relevant regulatory requirements applicable to
        software development, including but not limited to cybersecurity, AI,
        and industry-specific directives.
      </li>
      <li>
        Establish Special Interest Groups (SIGs) for domain-specific work (i.e.
        Cyber and AI).
      </li>
      <li>
        Compile guidelines and best practices for integrating compliance
        considerations into all stages of the software development lifecycle,
        from planning and design to deployment and maintenance.
      </li>
      <li>
        Collaborate with legal experts, industry stakeholders, regulatory
        bodies, and government officials to ensure alignment with current and
        evolving regulatory frameworks.
      </li>
      <li>
        Provide a forum where feedback can be garnered from government with
        respect to the specifications being developed, and that all interested
        stakeholders can meet when and how appropriate to better ensure the
        specifications meet the objectives established by government.
      </li>
      <li>
        Foster transparency and accountability within the open source community
        by promoting adherence to regulatory standards.
      </li>
      <li>
        Identify gaps in educational resources and training materials to
        support open source developers in understanding and implementing
        compliant development practices.
      </li>
      <li>
        With staff support, monitor regulatory updates and emerging trends to
        continuously refine and update the established processes and
        guidelines.
      </li>
      <li>
        Promote the adoption of the resulting specifications to various
        relevant standardisation bodies.
      </li>
      <li>
        Through its member organisations, promote the adoption of compliant
        software development practices across industry through advocacy,
        outreach, and knowledge-sharing initiatives.
      </li>
    </ul>

    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>
      The following governance documents are applicable to this Charter, each
      of which can be found on the <a href="/org/documents/">Eclipse Foundation Governance Documents</a>
      page or the <a href="/legal/">Eclipse Foundation Legal Resources</a> page:
    </p>
    <ul>
      <li>
        Eclipse Foundation Bylaws
      </li>
      <li>
        Eclipse Foundation Working Group Process
      </li>
      <li>
        Eclipse Foundation Working Group Operations Guide
      </li>
      <li>
        Eclipse Foundation Code of Conduct
      </li>
      <li>
        Eclipse Foundation Communication Channel Guidelines
      </li>
      <li>
        Eclipse Foundation Membership Agreement
      </li>
      <li>
        Eclipse Foundation Intellectual Property Policy
      </li>
      <li>
        Eclipse Foundation Antitrust Policy
      </li>
      <li>
        Eclipse Foundation Development Process
      </li>
      <li>
        Eclipse Foundation Trademark Usage Guidelines
      </li>
      <li>
        Eclipse Foundation Specification Process
      </li>
      <li>
        Eclipse Foundation Specification License
      </li>
      <li>
        Eclipse Foundation Technology Compatibility Kit Licence
      </li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership
      Agreement, including the requirement set forth in Section 2.2 to abide by
      and adhere to the Bylaws and then-current policies of the Eclipse
      Foundation, including but not limited to the Intellectual Property and
      Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this Working
      Group's Charter and the Eclipse Foundation Bylaws, Membership Agreement,
      Development Process, Specification Process, Working Group Process or any
      policies of the Eclipse Foundation, the terms of the respective Eclipse
      Foundation Bylaws, Membership Agreement, process or policy shall take
      precedence.
    </p>

    <h2>Membership</h2>
    <p>
      With the exception of Foundation and Guest members as described below, an
      entity must be at least a <a href="/membership/#tab-levels">Contributing Member</a>
      of the Eclipse Foundation, have executed the Open Regulatory Compliance
      Working Group Participation Agreement once defined and adhere to the
      requirements set forth in this Charter to participate.
    </p>
    <p>
      The participation fees associated with each of these membership classes
      are shown in the Annual Participation Fees section. These are annual fees
      and are established by the Open Regulatory Compliance Steering Committee
      and will be updated in this Charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are
      separate from any working group membership fees and are decided as
      described in the Eclipse Foundation Bylaws and detailed in the Eclipse
      Foundation Membership Agreement.
    </p>
    <p>
      There are four classes of Open Regulatory Compliance Working Group
      membership - Strategic, Participant, Foundation, and Guest.
    </p>

    <h2>Classes of Membership</h2>
    <h3>Strategic Members</h3>
    <p>
      Strategic Members are organisations that view this Working Group's
      standards, specifications and technologies as strategic to their
      organisation and are investing significant resources to sustain and shape
      the activities of this working group. Strategic Members of this working
      group must be at least a Contributing Member of the Eclipse Foundation.
      Strategic members are expected to have at least one person from their
      organisation participate in at least one of the specifications under the
      purview of the working group.
    </p>
    <h3>Participant Members</h3>
    <p>
      Participant Members are typically organisations that deliver products or
      services based upon related standards, specifications and technologies,
      or view this working group’s standards and technologies as strategic to
      their organisation. These organisations want to participate in the
      development and direction of an open ecosystem related to this working
      group. Participant Members of this Working Group must be at least a
      Contributing Member of the Eclipse Foundation. Participant members to
      have at least one person from their organisation are expected to
      participate in at least one of the specifications under the purview of
      the working group.
    </p>
    <h3>Foundation Members</h3>
    <p>
      Foundation Members are organisations that are recognised not-for-profits in
      their country of registration and host open source software projects made
      available under an OSI-approved licence(s). Foundation Membership of this
      working group is open to any classes of Membership with the Eclipse
      Foundation. Foundation Members are required to execute the Working Group’s
      Participation Agreement.
    </p>
    <h3>Guest Members</h3>
    <p>
      Guest Members are organisations which are Associate members of the
      Eclipse Foundation. Typical guests include R&amp;D partners, universities,
      academic research centres, etc. Guests may be invited to participate in
      committee meetings at the invitation of the respective committee, but
      under no circumstances do Guest members have voting rights. Guest members
      are required to execute the Working Group’s Participation Agreement.
    </p>

    <h2>Membership Summary</h2>
    <table class="table">
      <thead>
        <tr>
          <th>Committee Representation</th>
          <th>Strategic Member</th>
          <th>Participant Member</th>
          <th>Foundation Member</th>
          <th>Guest Member</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Steering Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected*</td>
          <td>N/A</td>
        </tr>
        <tr>
          <td>Specification Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected*</td>
          <td>N/A</td>
        </tr>
      </tbody>
    </table>
    <p>
      <small>
        *Elected representatives of the Foundation Membership Class that hold
        Associate Membership with the Eclipse Foundation, will formally be
        appointed in their individual capacity by the Executive Director as per
        the powers granted in the Eclipse Foundation Working Group Process.
      </small>
    </p>

    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within
      the Working Group with a focus to collaborate around a particular topic
      or domain of direct interest to the working group. SIGs are designed to
      drive the objectives of a subset of the Members of the working group in
      helping them achieve a dedicated set of goals and objectives. The scope
      of the SIG must be consistent with the scope of the Working Group
      Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each
      SIG may be either temporary or a permanent structure within the working
      group. SIGs can be disbanded at any time by self-selection presenting
      reasons to and seeking approval from the Steering Committee. Steering
      Committees may disband a SIG at any time for being inactive or
      non-compliant with the Working Group's Charter, or by request of the SIG
      itself. SIGs operate as a non-governing Body of the Working Group. There
      are no additional annual fees to Members for participation in a SIG.
    </p>

    <h2>Sponsorship</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to
      the working group on an ad hoc basis to support the activities of the
      working Group and its managed projects. Money or services provided by
      sponsors are used as set forth in the working group annual budget. The
      working group is free to determine whether and how those contributions
      are recognised. Under no condition are sponsorship monies refunded.
    </p>
    <p>
      Sponsors need not be members of the Eclipse Foundation or of the Working
      Group.
    </p>

    <h2>Governance</h2>
    <p>
      This Open Regulatory Compliance Working Group is designed as a
      vendor-neutral, member-driven organisation meant to foster a vibrant and
      sustainable ecosystem of diverse stakeholders. The governance model
      enables each specification to be defined in a collaborative and
      deliberative manner.
    </p>

    <h2>Governing Bodies</h2>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>
      Steering Committee members are required to:
    </p>
    <ul>
      <li>
        Define and manage the strategy of the working group.
      </li>
      <li>
        Define and manage which Eclipse Foundation projects are included within
        the scope of this working group.
      </li>
      <li>
        Define and manage the roadmap of specifications for coverage and
        alignment.
      </li>
      <li>
        Review and approve this Charter.
      </li>
      <li>
        Define and approve the trademark policy for this working group.
      </li>
      <li>
        Define the annual fees for all classes of the working group members.
      </li>
      <li>
        Establish the annual program plan.
      </li>
      <li>
        Approve the annual budget based upon funds received through fees.
      </li>
      <li>
        Approve the creation of subcommittees and define the purpose, scope,
        and membership of each such subcommittee.
      </li>
      <li>
        Approve the creation and retirement of Special Interest Groups (SIGs).
      </li>
      <li>
        Identify opportunities for the specifications under the purview of the
        working group to be used or adopted by standardisation bodies.
      </li>
    </ul>
    <h4>Composition</h4>
    <p>
      Each Strategic Member of the working group is entitled to a seat on the
      Steering Committee.
    </p>
    <p>
      One seat is allocated to Participant Members via election. The
      Participant Member seat is allocated following the Eclipse "Single
      Transferable Vote", as defined in the Eclipse Foundation Bylaws.
    </p>
    <p>
      Minimum of three (3) seats are allocated to representatives of the
      Foundation Members via election. In addition, an additional seat on the
      Committee shall be allocated to the Foundation Members for every additional
      five (5) seats beyond one (1) allocated to Strategic Members via
      election. Foundation Member seats are allocated following the Eclipse
      "Single Transferable Vote", as defined in the Eclipse Bylaws. Elected
      representatives of the Foundation Membership Class that hold Associate
      Membership with the Eclipse Foundation, will formally be appointed in
      their individual capacity by the Executive Director as per the powers
      granted the Executive Director in the Eclipse Foundation Working Group
      Process.
    </p>
    <p>
      Additional individuals may be added from time to time by the Executive
      Director.
    </p>
    <p>
      The Committee elects a chair of the Steering Committee. This chair is
      elected among the members of the Committee. They will serve for a
      12-month period or until their successor is elected and qualified, or as
      otherwise provided for in this Charter. There is no limit on the number
      of terms the chair may serve.
    </p>
    <h4>Meeting Management</h4>
    <p>
      The Steering Committee meets at least twice a year.
    </p>

    <h3>Specification Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Specification Committee members are required to:</p>
    <ul>
      <li>
        Ensure that all specification projects operate in an open, transparent,
        and vendor-neutral fashion in compliance with the specification
        process.
      </li>
      <li>
        Apply and govern the specification projects in the scope of this
        working group according to the Eclipse Foundation Specification Process
        (EFSP) version 2.0 or later. While permitted by the EFSP, the working
        group will not make any customizations to the EFSP version 2.0 and will
        conform to it without modification.
      </li>
      <li>
        Approve specifications for adoption by the community.
      </li>
      <li>
        Work with the related Project Management Committee (PMC) to ensure that
        the EFSP is complied with by all related working group specification
        projects.
      </li>
      <li>
        All specification projects in the purview of this working group will
        use the Implementation Patent Licence as that term is defined in the
        EFSP.
      </li>
    </ul>
    <h4>Composition</h4>
    <p>
      Each Strategic Member of the working group is entitled to have a seat on
      the Specification Committee.
    </p>
    <p>
      One seat is allocated to Participant Members via election. The
      Participant Member seat is allocated following the Eclipse "Single
      Transferable Vote", as defined in the Eclipse Foundation Bylaws.
    </p>
    <p>
      Minimum of three (3) seats are allocated to representatives of the
      Foundation Members via election. In addition, an additional seat on the
      Committee shall be allocated to the Foundation Members for every additional
      five (5) seats beyond one (1) allocated to Strategic Members via
      election. Foundation Member seats are allocated following the Eclipse
      "Single Transferable Vote", as defined in the Eclipse Bylaws. Elected
      representatives of the Foundation Membership Class that hold Associate
      Membership with the Eclipse Foundation, will formally be appointed in
      their individual capacity by the Executive Director as per the powers
      granted the Executive Director in the Eclipse Foundation Working Group
      Process.
    </p>
    <p>
      One seat is allocated to a representative designated by the Project
      Management Committee (PMC) responsible for the governance of projects
      within the purview of the working group.
    </p>
    <p>
      Any additional individuals as designated from time to time by the
      Executive Director.
    </p>
    <p>
      The Committee elects a chair who reports to the Steering Committee. This
      chair is elected among the members of the Committee. They will serve for
      a 12-month period or until their successor is elected and qualified, or
      as otherwise provided for in this Charter. There is no limit on the
      number of terms the chair may serve.
    </p>

    <h3>Public Sector Engagement Forum</h3>
    <p>
      Soliciting regular feedback and comments from relevant representatives of
      governmental bodies and public sector entities is an explicit objective
      of the working group to ensure alignment between the specifications and
      the underlying regulatory binding instruments they are intended to
      address. In the context of this forum, government officials are not
      required to be members of the Eclipse Foundation (which is generally a
      requirement for participation in any Eclipse Foundation working group)
      and instead are considered “Observers”.
    </p>
    <p>
      The working group will organise or facilitate briefings and other means
      of engagement including mailing lists or discussion forums with Observers
      to solicit their feedback and advice on particular subject areas related
      to this working group to the members of the working group.
    </p>

    <h2>Common Disposition</h2>
    <p>
      The dispositions below apply to all governance bodies for this working
      group unless otherwise specified. For all matters related to membership
      action, including without limitation: meetings, quorum, voting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse
      Foundation Bylaws apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member
      organisation they are representing at any time by providing written
      notice to the Steering Committee. In the event a Body member is
      unavailable to attend or participate in a meeting of the Body, they may
      be represented by another Body member by providing written proxy to the
      Body’s mailing list in advance. As per the Eclipse Foundation Bylaws, a
      representative shall be immediately removed from the Body upon the
      termination of the membership of such representative’s Member
      organisation. Appointed individuals cannot be replaced by any other
      representative.
    </p>
    <p>
      All individuals appointed by the Executive Director to any of the
      governing bodies must enter into a separate Open Regulatory Compliance
      Individual Working Group Participation Agreement, which amongst other
      things will require them to agree to adhere to the terms of this Charter,
      and notably the Eclipse Foundation Antitrust Policy and Eclipse Community
      Code of Conduct.
    </p>
    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Accepting the actions specified below for which a Super Majority is
      required, votes of the Body are determined by a simple majority of the
      representatives represented at a committee meeting at which a quorum is
      present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions (i) approving specifications for adoption; (ii) modifying the
      working group charter; (iii) approving or changing the name of the
      working group; and (iv) approving changes to annual Member contribution
      requirements; any such actions must be approved by no less than
      two-thirds (2/3) of the representatives represented at a committee
      meeting at which a quorum is present.
    </p>

    <h3>Term and Dates of Elections</h3>
    <p>
      This section only applies to the Steering Committee and Specification Committee.
    </p>
    <p>
      All representatives shall hold office until their respective successors
      are appointed or elected, as applicable. There shall be no prohibition on
      re-election or re-designation of any representative following the
      completion of that representative's term of office with the exception of
      Appointed Individuals. Appointed Individuals shall hold office for a
      12-month period renewable.
    </p>
    <h4>Strategic Members</h4>
    <p>
      Strategic Members Representatives shall serve in such capacity on
      committees until the earlier of their removal by their respective
      appointing Member organisation or as otherwise provided for in this
      Charter.
    </p>
    <h4>Elected Representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be
      elected to serve for a 12- month term or until their respective
      successors are elected and qualified, or as otherwise provided for in
      this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this
      Charter.
    </p>

    <h3>Meetings Management</h3>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all
      meetings related to the working group will follow a prepared agenda and
      minutes are distributed two weeks after the meeting and approved at the
      next meeting at the latest, and shall in general conform to the Eclipse
      Foundation Antitrust Policy.
    </p>
    <h4>Meeting Frequency</h4>
    <p>
      Each governing Body meets at least twice a year. All meetings may be held
      at any place designated from time to time by resolution of the
      corresponding Body. All meetings may be held remotely using phone calls,
      video calls, or any other means as designated from time to time by
      resolution of the corresponding Body.
    </p>
    <h4>Place of Meetings</h4>
    <p>
      All meetings may be held at any place that has been designated from time
      to time by resolution of the corresponding Body. All meetings may be held
      remotely using phone calls, video calls, or any other means as designated
      from time-to-time by resolution of the corresponding Body.
    </p>
    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice
      of same has been provided to each of the representatives at least fifteen
      (15) calendar days prior to such meeting, which notice will identify all
      potential actions to be undertaken by the Body at the Body meeting. No
      representative will be intentionally excluded from Body meetings and all
      representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because
      one or more of the representatives cannot attend or participate so long
      as at least a quorum of the Body is represented at the Body meeting.
    </p>
    <h4>Actions</h4>
    <p>
      The Body may undertake an action only if it was identified in a Body
      meeting notice or otherwise identified in a notice of special meeting.
    </p>
    <h4>Invitations</h4>
    <p>
      The Body may invite any member to any of its meetings. These invited
      attendees have no right to vote.
    </p>

    <h2>Working Group Participation Fees</h2>
    <p>
      The initial Steering Committee will be tasked with establishing the fee
      structure. The fees are intended to provide sufficient funding to achieve
      the objectives of the working group. Members agree to pay the annual
      fees as established by the Steering Committee and as agreed to by each
      participating member in their respective Working Group Participation
      Agreement.
    </p>
    <p>
      No Participation Fees are charged in 2024. All Members who join prior to
      January 1, 2025, agree to begin paying the full annual fees associated
      with their participation effective January 2025 and each January
      thereafter. All Members who join the working group in 2025 and beyond
      shall pay the full annual fees effective the month they join the working
      group and each anniversary month thereafter.
    </p>
    <p>
      Any change to the annual fees will be communicated to all Members via the
      mailing list and will be reflected in their next annual fees payment.
      Established fees cannot be changed retroactively.
    </p>

    <h2>Working Group Annual Participation Fees Schedule A</h2>
    <p>
      The following fees have been established by the Open Regulatory
      Compliance Working Group Steering Committee. These fees are in addition
      to each participant’s membership fees in the Eclipse Foundation.
    </p>
    <h3>Open Regulatory Compliance Strategic Member Annual Participation Fees</h3>
    <p>
      Strategic members are required to execute the Open Regulatory Compliance
      Working Group Participation Agreement.
    </p>
    <table class="table">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;1 billion</td>
          <td>&euro;50 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;100 million but less
            than or equal to &euro;1 billion
          </td>
          <td>&euro;35 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;50 million but less than or equal to &euro;100 million
          </td>
          <td>&euro;20 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to &euro;50 million</td>
          <td>&euro;10 000</td>
        </tr>
      </tbody>
    </table>

    <h3>Open Regulatory Compliance Participant Member Annual Participation Fees</h3>
    <p>
      Strategic members are required to execute the Open Regulatory Compliance
      Working Group Participation Agreement.
    </p>
    <table class="table">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;1 billion</td>
          <td>&euro;20 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;100 million but less than or equal to &euro;1 billion</td>
          <td>&euro;10 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;50 million but less than or equal to &euro;100 million</td>
          <td>&euro;5 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than &euro;50 million</td>
          <td>&euro;0</td>
        </tr>
      </tbody>
    </table>
    <h3>Open Regulatory Compliance Foundation Member Annual Participation Fees</h3>
    <p>
      Foundation members pay no annual fees but are required to execute the Open
      Regulatory Compliance Working Group Participation Agreement.
    </p>
    <h3>Open Regulatory Compliance Guest Member Annual Participation Fees</h3>
    <p>
      Guest members pay no annual fees but are required to execute the Open
      Regulatory Compliance Working Group Participation Agreement.
    </p>

    <p class="margin-top-20"><strong>Charter Version History</strong></p>
    <ul>
      <li>
        v0.1 proposed draft - 21 March 2024
      </li>
      <li>
        v0.2 updated proposed draft - 7 May 2024
      </li>
      <li>
        v0.3 rename Steward member to Foundation member - 2 June 2024
      </li>
      <li>
        v0.4 updated strategic and participant member revenue tier to &euro;50 million from &euro;10 million - 11 September 2024
      </li>
    </ul>
  </div>
</div>
