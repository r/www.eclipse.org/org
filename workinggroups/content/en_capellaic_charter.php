<?php
/**
 * Copyright (c) 2017, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <div class="alert alert-danger">
      <strong><?php print $pageTitle;?> HAS BEEN TERMINATED</strong>
    </div>
    <p>June 12, 2017</p>
    <h1>Definitions</h1>
    <p>A list of definitions of terms used in this document.</p>
    <ul>
      <li>Arcadia refers to the MBSE method supported by Capella.</li>
      <li>The Eclipse Capella Industry Consortium is referred to as the Capella IC in this document</li>
      <li>Capella Project (&ldquo;Capella&rdquo;) refers to the Polarsys project hosted at
        https://www.polarsys.org/capella/.</li>
      <li>Capella refers to the Polarsys Solution consisting of a ready-to-use and well-tested
        package with the Capella Project and its open source dependencies, as described at
        https://www.polarsys.org/solutions/capella.<sup><a href="#cmnt1">[a]</a></sup><sup><a
          href="#cmnt2"
        >[b]</a></sup>
      </li>
      <li>Capella Add-ons refers to the plug-ins which extends the scope and feature set of Capella,
        including connectors, and which are not included in Capella itself.</li>
      <li>Capella Studio refers to the solution consisting of Capella and its SDK which is used to
        extend Capella.</li>
      <li>Model Based Systems Engineering (MBSE) refers to model-based systems design and
        architecture.</li>
      <li>Suppliers refers to any organization providing commercial software related offerings,
        including proprietary products, professional services, training, and mentoring, related to
        the technologies within the scope of this consortium.</li>
    </ul>
    <h1>Vision/Mission</h1>
    <p>To fulfill the overall MBSE vision, industrial users needs a mature, full-featured,
      sustainable solution which can be widely distributed as an open source alternative to existing
      proprietary modeling tools.</p>
    <p>The Capella tools, in association with the Arcadia method, can meet this need.</p>
    <p>The Capella ecosystem will include companies developing complex systems and software,
      suppliers providing offers on top of Capella and academics researching MBSE themes. A place to
      foster collaboration, share investments, exchange best practises, and to leverage Open
      Innovation will ensure a vibrant ecosystem that will enable the co-development of new MBSE
      capabilities.</p>
    <p>The Capella IC aims to host the Capella Ecosystem stakeholders in a vendor neutral way,
      organized by an open governance model.</p>
    <h1>Overall Goals</h1>
    <p>The main goal of the Capella IC is to make Arcadia the leading system engineering method and
      Capella the leading MBSE tool. Other key goals are in the following thematic areas:</p>
    <h2>Knowledge sharing</h2>
    <ul>
      <li>Share MBSE experiences and case studies among consortium members to enable broad adoption
        of Capella and Arcadia in operational projects.</li>
      <li>Establish a technology watch service to help members leverage innovation in Capella and
        its ecosystem.</li>
      <li>Keep up-to-date with new and noteworthy, future improvements, success stories, and user
        feedback.</li>
      <li>Organize private exchange workshops between members</li>
    </ul>
    <h2>Joint development financing</h2>
    <ul>
      <li>Coordinate investments from consortium members in the different aspects of the overall
        MBSE solution to reduce development time, risk, and costs, and to maximize ROI</li>
    </ul>
    <h2>Product Management</h2>
    <ul>
      <li>Capture, consolidate, and manage requirements from the different consortium members and
        application domains</li>
      <li>Define development priorities based on consortium members requirements/needs</li>
      <li>Discuss the roadmap among users and contributors</li>
      <li>Operate a transparent process supported by tools.</li>
    </ul>
    <h2>Promotion</h2>
    <ul>
      <li>Promote the use of Capella as a leading industrial MBSE solution</li>
      <li>Provide materials to explain the value of Capella and Arcadia to executives and managers</li>
    </ul>
    <h2>Community Development</h2>
    <ul>
      <li>Provide a focal point for organizing collaboration and creating partnerships among
        suppliers, end-users, and research/academia</li>
    </ul>
    <h2>Technical Scope/Projects</h2>
    <p>The initial areas of work for the Capella IC include:</p>
    <ul>
      <li>Capella</li>
      <li>Capella Studio</li>
      <li>Underlying technologies and plug-ins used by Capella and Capella Studio</li>
      <li>Capella Add-ons</li>
    </ul>
    <p>Additional areas of work may be defined in the future by the Steering Committee of the IC.</p>
    <h1>Modeling Processes</h1>
    <p>The scope of the Capella IC includes the following key modeling processes:</p>
    <ul>
      <li>Definition of the Problem - Customer Operational Needs Analysis</li>
      <li>Formalization of system requirements - System Needs Analysis</li>
      <li>Development of System Architectural Design - Logical Architecture (Notional Solution)</li>
      <li>Development of System Architecture - Physical Architecture</li>
      <li>Formalize Component Requirements - Contracts for Development and IVVQ (Integration
        Validation Verification Qualification)</li>
      <li>Co-Engineering, Sub-Contracting and Multi-Level Engineering</li>
      <li>Adaptation of Arcadia to support new dedicated Engineering Domains and to improve
        integration capabilities with other tools</li>
    </ul>
    <h1>Membership</h1>
    <h2>Summary of Membership Classes and Membership Fees</h2>
    <p>Membership is based on a one year renewable commitment. As Capella IC is a neutral and
      transparent consortium, the rules for joining are clearly defined by the Working Group
      participation agreement and the Eclipse membership agreement.</p>
    <p>As the Capella IC is a sub-group of the Polarsys Working Group, and as Polarsys itself is a
      working group of the Eclipse Foundation, there are two additional membership requirements for
      joining the Capella IC:</p>
    <ul>
      <li>Eclipse Solution membership as a minimum requirement, in compliance with the Eclipse
        bylaws and processes</li>
      <li>Polarsys membership, in compliance with the Polarsys charter</li>
    </ul>
    <p>All Capella IC members must pay both Eclipse Membership fees and Polarsys WG Membership fees.
      No additional fees are mandatory to join the Capella IC.</p>
    <p>Each year, Polarsys members are entitled to allocate a pre-determined portion of their
      membership fees to ICs and Common Activities. To join the Capella IC, a minimum 20% of this
      pre-determined portion, must be allocated to the Capella IC.</p>
    <p>The fees to join the Capella IC are currently USD 0. Fee changes are at the discretion of the
      Capella Steering Committee.</p>
    <h3>Membership Classes</h3>
    <p>The membership classes of the Capella IC are designed to reflect the varied interests of the
      members. The membership class of each Capella IC member is confirmed once per year.</p>
    <h4>Driver Member</h4>
    <p>Driver members want to influence the development of Capella and be an integral part of the
      group of organizations that govern the development of the open source MBSE solution. They
      invest a substantial amount of resources to sustain the Capella IC activities and contribute
      to the development of the open source technologies.</p>
    <p>Typical Drivers members are organizations that use MBSE solutions at the core of their
      development process and who consider Capella as a strategic asset.</p>
    <p>Each Driver member must document annually a minimum investment in the Capella IC activities
      such as allocation of internal developers, features outsourcing, maintenance and support
      contribution, marketing. These activities do not include investments for internal deployments
      within the members&rsquo; teams or for private customizations. The minimum annual amount is
      $50 000. The Steering committee is responsible for the analysis and approval of the annual
      investment documentation.</p>
    <h4>Participant Member</h4>
    <p>Participant members participate in the development of the Capella ecosystem. They contribute
      to the development and adoption of Capella according to their roles, such as the development
      of viewpoints for tools providers, participation at conferences, or providing requirements or
      user experiences for end-users.</p>
    <h4>Research/Academia Member</h4>
    <p>Research/academia members are Universities and research organizations that want to
      participate in the development of the overall MBSE solution based on Capella. Their
      contributions take the form of addressing various relevant research topics, using Capella for
      teaching/training purposes, and contributing to the overall development of the ecosystem.</p>
    <h1>Committees</h1>
    <p>The Capella IC has 3 standing committees: the Steering Committee, the Product Management
      Committee and the Marketing Committee.</p>
    <p>Committee members and Chairpersons are elected and serve for one year from April 1 to March
      31 of the following year, or until their respective successors are elected or nominated, or as
      otherwise provided for in this charter.</p>
    <h2>Steering Committee</h2>
    <p>The Capella IC is led by the Steering Committee.</p>
    <h3>Composition</h3>
    <ul>
      <li>Each Driver Member has one seat on the Steering Committee.</li>
      <li>Participant Members elect one representative.</li>
      <li>The Chair of the Marketing Committee and Product Management Committee each have one seat.</li>
      <li>The Chair is elected by a simple majority of Steering Committee members.</li>
    </ul>
    <h3>Responsibilities of the Steering Committee</h3>
    <ul>
      <li>Define the overall strategic directions of the consortium.</li>
      <li>Define and manage the consortium budget.</li>
      <li>Define and amend the charter.</li>
      <li>Define and implement the overall strategy for the development of the Capella ecosystem.</li>
    </ul>
    <h2>Product Management Committee</h2>
    <p>The Product Management Committee reports to the Steering Committee.</p>
    <h3>Composition</h3>
    <ul>
      <li>Each member has one seat.</li>
      <li>The Chairperson is elected by a simple majority of the Product Management Committee
        members.</li>
    </ul>
    <h3>Responsibilities of the Product Management Committee</h3>
    <ul>
      <li>Capture, consolidate, and manage requirements from members.Define development priorities
        based on consortium members&rsquo; requirements/needs.</li>
      <li>Define and maintain the technical/product roadmap and development priorities.</li>
      <li>Ensure the consistency and integrity of the architecture and its components.</li>
      <li>Interact with research groups involved in consortium activities.</li>
    </ul>
    <h2>Marketing Committee</h2>
    <p>The Marketing Committee reports to the Steering Committee.</p>
    <h3>Composition</h3>
    <ul>
      <li>Each member has one seat.</li>
      <li>The Chairperson is elected by a simple majority of the Marketing Committee members.</li>
    </ul>
    <h3>Responsibilities of the Marketing Committee</h3>
    <ul>
      <li>Define a marketing strategy to promote Capella.</li>
      <li>Create marketing materials.</li>
      <li>Organize communication on Capella.</li>
      <li>Organize events.</li>
      <li>Interact with the Polarsys Marketing Committee.</li>
    </ul>
  </div>
</div>