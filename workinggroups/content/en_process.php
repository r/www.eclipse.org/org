<?php
/**
 * Copyright (c) 2008, 2021 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Wayne Beaton, Sharon Corbett
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>

<?php 

/*
 * The Eclipse Foundation Working Group Process source is in GitHub:
 * 
 * https://github.com/EclipseFdn/EFWGP
 * 
 * The source is in Asciidoc; the HTML is generated using the provided
 * Maven pom.xml file. Deployment is unsophisticated: I manually copy the
 * generated "process.html" file into this directory.
 * 
 * As of the authoring of this comment, we're building from the the "2.1" tag.
 */

include dirname(__FILE__) . '/process.html'; ?>