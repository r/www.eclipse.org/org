<?php
/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
	<p>v1.2 - History at end of document.</p>
	<h2>Vision and Scope</h2>
	<p>The MicroProfile Working Group will continue driving the
		evolution and broad adoption of technologies derived from or related
		to the MicroProfile project. MicroProfile is an open source, vendor
		neutral, collaborative, initiative that creates technical
		specifications, APIs, and technology compatibility kits (TCKs) for
		Java runtimes that enables the development, deployment, and management
		of cloud-native microservices. The MicroProfile community owns the
		responsibility to collaborate on and release open source
		specifications, and to promote, evangelize and market MicroProfile in
		the marketplace.</p>
	<p>The MicroProfile community values the following principles:</p>
	<ul>
		<li><strong>Limited processes.</strong> MicroProfile abides by
			the Eclipse Development Process and the Eclipse Foundation
			Specification Process. Any additional processes specific to
			MicroProfile are created only when necessary.</li>
		<li><strong>Experiment and innovate.</strong> MicroProfile as a
			community provides an industry proving ground to incubate and
			experiment with well-established problems needing cross-platform
			APIs, gather user feedback, adapt and iterate at a fast pace.</li>
		<li><strong>No backwards compatibility guarantee.</strong> Major
			versions of a specification developed within MicroProfile may break
			backwards compatibility.</li>
		<li><strong>Implementation first.</strong> MicroProfile
			specifications are released only after an implementation has been
			created and both the specification and implementation has had
			sufficient time for community review.</li>
		<li><strong>Encourage brand adoption.</strong> Define guidelines
			that would allow usage of the MicroProfile brand without charge in a
			way that is consistent with the Eclipse Foundation Trademark
			Guidelines.</li>
		<li><strong>Openness.</strong> Transparency, inclusiveness and
			eliminating barriers to participate are highly-valued principles.
			Public meetings and lists are preferred. Lists are favored for key
			decisions. Specifications have been managed in a way that provides
			open access to all MicroProfile committers.</li>
		<li><strong>Low barrier to entry.</strong> It is
			MicroProfile&rsquo;s intent to operate a low-cost Working Group.
			Budget will be evaluated annually and as membership changes for
			opportunities to maintain low fees and costs.</li>
	</ul>
	<p>Additionally, the working group will:</p>
	<ul>
		<li>Promote the MicroProfile brand and its value in the
			marketplace.</li>
		<li>Ensure the consistency of logo usage and other marketing
			materials.</li>
		<li>Manage the use of the Eclipse Foundation Specification
			Process to formalize the specifications that are defined within the
			scope of this working group.</li>
		<li>Leverage Eclipse Foundation-defined licensing and
			intellectual property flows that encourage community participation,
			protect community members, and encourage usage.</li>
		<li>Manage the overall technical strategies and its related open
			source projects.</li>
	</ul>
	<h2>Governance and Precedence</h2>
	<h3>Applicable Documents</h3>
	<p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
	<ul>
	  <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
	  <li>Eclipse Foundation Specification Process</li>
	  <li>Eclipse Foundation Technology Compatibility Kit License</li>
	  <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li> 
	</ul>
	<p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
	<p>In the event of any conflict between the terms set forth in this
		working group&#39;s charter and the Eclipse Foundation Bylaws,
		Membership Agreement, Development Process, or Working
		Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership
		Agreement, process, or policy shall take precedence.</p>
	<h3>Membership</h3>
	<p>With the exception of Guest members as described below, an
		entity must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation,
		have executed the MicroProfile Participation Agreement,
		and adhere to the requirements set forth in this Charter to
		participate.</p>
	<p>There are three classes of MicroProfile working group membership
		- Corporate, Committer, and Guest. Each of these classes is described
		in detail below.</p>
	<p>The participation fees associated with each of these membership
		classes are shown in the tables in Schedule A.</p>
	<p>The fees associated with membership in the Eclipse Foundation
		are separate from any working group membership fees, and are decided
		as described in the Eclipse Foundation Bylaws and detailed in the Eclipse Foundation Membership Agreement.</p>
	<h2>Classes of MicroProfile Working Group Membership</h2>
	<h3>Corporate members</h3>
	<p>Corporate Members are organizations that view open technical
		communication standards and technologies as strategic to their
		organization and are investing significant resources to sustain and
		shape the activities of the MicroProfile Working Group.</p>
	<p>Corporate Members of the MicroProfile Working Group must be at
		least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation.</p>
	<h3>Committer Members</h3>
	<p>Committer Members are individuals who through a process of
		meritocracy defined by the Eclipse Development Process are able to
		contribute and commit to the Eclipse Foundation projects included in
		the scope of this Working Group. Committers may be members by virtue
		of working for a member organization, or may choose to complete the
		membership process independently if they are not. For further
		explanation and details, see the <a href="http://www.eclipse.org/membership/become_a_member/committer.php">Eclipse Committer Membership page</a>.</p>
	<h3>Guest Members</h3>
	<p>Guest Members are organizations which are <a href="https://www.eclipse.org/membership/become_a_member/membershipTypes.php#associate">Associate members</a> of
		the Eclipse Foundation who have been invited for one year, renewable,
		by the Steering Committee to participate in particular aspects of the
		activities of the Working Group. Typical guests include Java user
		groups, R&amp;D partners, universities, academic research centers,
		etc.</p>
	<p>Guests may be invited to participate in committee meetings at
		the invitation of the respective committee, but under no circumstances
		do Guest members have voting rights. Guest members are required to
		execute the MicroProfile Participation Agreement.</p>
	<h2>Membership Summary</h2>
	<table class="table">
	   <thead>
	       <tr>
	           <td></td>
               <th>Corporate Member</th>
               <th>Committer Member</th>
               <th>Guest Member</th>
           </tr>
        </thead>
        <tbody>
            <tr>
                <td>Member of the Steering Committee *</td>
                <td>Appointed</td>
                <td>Elected</td>
                <td>Invitation Only</td>
            </tr>
        </tbody>
	</table>
	<p><em>*MicroProfile WG Steering Committee and Specification Committee
		are combined into a single committee called Steering Committee. See
		the Governance section.</em></p>
	<p>All matters related to Membership in the Eclipse Foundation and
		the MicroProfile Working Group will be governed by the Eclipse
		Foundation Bylaws, Membership Agreement and Working Group
		Process. These matters include, without limitation, delinquency,
		payment of dues, termination, resignation, reinstatement, and
		assignment.</p>
	<h2>Governance</h2>
	<p>This MicroProfile working group is designed as:</p>
	<ul>
		<li>a member driven organization,</li>
		<li>a means to foster a vibrant and sustainable ecosystem of
			components and service providers,</li>
		<li>a means to organize the community of each project or
			component so that users and developers define the roadmap
			collaboratively.</li>
	</ul>
	<p>In order to implement these principles, the following governance
		bodies have been defined (each a &quot;Body&quot;):</p>
	<ul>
		<li>Steering Committee that includes the responsibilities of the
			Specification Committee as defined in the Eclipse Foundation
			Specification Process.</li>
	</ul>
	<h2>Steering Committee</h2>
	<h3>Powers and Duties</h3>
	<p>Steering Committee members are required to:</p>
	<ul>
		<li>Define and manage the strategy of the working group.</li>
		<li>The Steering Committee will take on the role and fulfill the
			obligations of the Specification Committee as defined/required under
			the Eclipse Foundation Specification Process.</li>
		<li>Define and manage which Eclipse Foundation projects are
			included within the scope of this working group. This will require
			adoption of the specification process by these projects.</li>
		<li>Define and manage the roadmaps.</li>
		<li>Define and approve this charter.</li>
		<li>Define, review and approve the trademark policy to ensure
			compatibility of independent implementations of specifications.</li>
		<li>Define the annual fees for all classes of
			working group membership.</li>
		<li>Approve the annual budget based upon funds received through fees.</li>
		<li>Invite Guest members to participate in the working group.</li>
		<li>Ensure that all specification projects operate in an open,
			transparent, and vendor-neutral fashion in compliance with the
			specification process.</li>
		<li>Approve specifications for adoption by the community.</li>
	</ul>
	<h3>Composition</h3>
	<ul>
		<li>Corporate Members. Each Corporate Member of the Working Group
			has one seat on the Steering Committee.</li>
		<li>Committer Representative. At least one Committer
			Representative is elected by the MicroProfile project committers
			themselves. Procedures governing elections of Representatives may be
			established pursuant to resolutions of the Steering Committee
			provided that such resolutions are not inconsistent with any
			provision of this Charter or Eclipse Foundation Bylaws.</li>
	</ul>
	<h2>Common Dispositions</h2>
	<p>The dispositions below apply to all governance bodies for this
		working group, unless otherwise specified. For all matters related to
		membership action, including without limitation: meetings, quorum,
		voting, electronic voting action without meeting, vacancy, resignation
		or removal, the respective terms set forth in the Eclipse Foundation
		Bylaws apply.</p>
	<h3>Good Standing</h3>
	<p>A representative shall be deemed to be in Good Standing, and
		thus eligible to vote on issues coming before the Body they
		participate in, if the representative has attended a minimum of three
		(3) of the last four (4) Body meetings (if there have been at least
		four meetings). Appointed representatives on the Body may be replaced
		by the Member organization they are representing at any time by
		providing public notice to the Steering Committee. In the event a Body
		member is unavailable to attend or participate in a meeting of the
		Body, they may be represented by another Body member by providing written proxy to the Body’s mailing list in advance, which shall be included in determining whether the representative is in Good Standing. As per the Eclipse
		Foundation Bylaws, a representative shall be immediately removed from
		the Body upon the termination of the membership of such
		representative&rsquo;s Member organization.</p>
	<h3 id="dispositions-voting">Voting</h3>
	<h4>Super Majority</h4>
	<p>For actions (i) requesting that the Eclipse Foundation Board of
		Directors approve a specification license; (ii) approving or changing the
		name of the working group; and (iii) approving changes to annual Member
		contribution requirements; any such actions must be approved by no
		less than two-thirds (2/3) of the representatives in Good Standing
		represented at a committee meeting at which a quorum is present.</p>
	<h3>Term and Dates of Elections</h3>
	<p>Steering Committee representatives shall hold office until their
		respective successors are appointed. There shall be no prohibition on
		re-designation of any representative following the completion of that
		representative&rsquo;s term of office.</p>
	<p>Elected representatives shall each serve one-year terms and
		shall be elected to serve from October 1 to September 30 of each
		calendar year, or until their respective successors are elected and
		qualified, or as otherwise provided for in this Charter.</p>
	<h4>Corporate Members</h4>
	<p>Corporate Members representatives shall serve in such capacity
		on committees until the earlier of their removal by their respective
		appointing Member organization or as otherwise provided for in this
		Charter.</p>
	<h3>Meetings Management</h3>
	<h4>Place of meetings</h4>
	<p>All meetings may be held at any place that has been designated
		from time-to-time by resolution of the corresponding Body. All
		meetings may be held remotely using phone calls, video calls or any
		other means as designated from time-to-time by resolution of the
		corresponding Body.</p>
	<p>Meeting schedule: At least 1 meeting per quarter.</p>
	<h4>Regular meetings</h4>
	<p>No Body meeting will be deemed to have been validly held unless
		a notice of the same has been provided to each of the representatives
		in Good Standing at least fifteen (15) calendar days prior to such
		meeting. No representative will be intentionally excluded from Body
		meetings and all representatives shall receive notice of the meeting
		as specified above; however, Body meetings need not be delayed or
		rescheduled merely because one or more of the representatives cannot
		attend or participate so long as at least a quorum of the Body is
		represented at the Body meeting. Public meetings must be held at least
		every 90 calendar days. Electronic voting shall be permitted in
		conjunction with any and all meetings of the Body the subject matter
		of which requires a vote of the Body to be delayed until each such
		representative in attendance thereat has conferred with his or her
		respective Member organization as set forth in section
		<a href="#dispositions-voting">Voting</a> above.</p>
	<h4>Actions</h4>
	<p>The Body may undertake an action only if it was identified in a
		Body Meeting notice or otherwise identified in a notice of special
		meeting.</p>
	<h3>Invitations</h3>
	<p>The Body may invite any Eclipse Foundation Member to any of its
		meetings. These invited attendees have no right of vote.</p>
	<h2>Schedule A - MicroProfile Working Group Fees</h2>
	<p>The following fees have been established by the MicroProfile
		Steering Committee. These fees are in addition to each participant&#39;s membership fees in the
		Eclipse Foundation.</p>
	<p>Corporate members are required to execute the MicroProfile Working Group
		Participation Agreement, and to pay the fees shown below.</p>
	<table class="table">
		<thead>
			<tr>
				<th>Corporate Revenue</th>
				<th class="text-center">Annual Fees</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Annual Corporate Revenues greater than $1 billion</td>
				<td class="text-center">$10,000</td>
			</tr>
			<tr>
				<td>Annual Corporate Revenues greater than $100 million but less than or equal to $1 billion</td>
				<td class="text-center">$8,000</td>
			</tr>
			<tr>
				<td>Annual Corporate Revenues greater than $10 million but less than or equal to $100 million</td>
				<td class="text-center">$6,000</td>
			</tr>
			<tr>
				<td>Annual Corporate Revenues less than or equal to $10 million</td>
				<td class="text-center">$3,000</td>
			</tr>
			<tr>
				<td>Annual Corporate Revenues less than $1 million and &lt; 10 employees</td>
				<td class="text-center">$1,500</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<h3>MicroProfile Committer Member Annual Participation Fees</h3>
	<p>Committer members pay no annual fees, but are required to
		execute the MicroProfile Working Group Participation Agreement.</p>
	<h3>MicroProfile Guest Member Annual Participation Fees</h3>
	<p>Guest members pay no annual fees, but are required to execute
		the MicroProfile Working Group Participation Agreement.</p>
	<h2>Charter Version History</h2>
	<ul>
		<li>V0.1 created April 06, 2020</li>
		<li>V0.2 started upon v0.1 delivery to the MP Forum, thread <a href="https://groups.google.com/forum/#!topic/microprofile/G5WZvF0wZuY">HERE</a>.</li>
		<li>V0.3 5/12 decided after the Community Hangout</li>
		<li>V0.4 Added Working Group Fees June 23rd</li>
		<li>V0.5 Added openness and budget verbiage. Frozen for
			membership review. Jul 14, 2020</li>
		<li>V.0.6 Aug 4th committer adjustments</li>
		<li>V.0.7 Aug 18th committer adjustments</li>
		<li>V1.0 Sept 1st submitted as Final to the Foundation</li>
		<li>V1.0 October 20th Ratified by Steering Committee</li>
		<li>V1.1 Nov 24th Updates in support of the Eclipse Foundation corporate restructuring</li>
		<li>V1.2 Nov 16/21 Updated Working Group Fees as per Steering Committee and Executive Director approval</li>
	</ul>
  </div>
</div>