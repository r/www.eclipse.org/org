<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <h2 id="mission">Mission</h2>
    <p>
      The mission of the openGENESIS Working Group is to provide methods
      and tools for the assessment of artificial intelligence (AI) used for
      autonomous driving. AI algorithms should be proved to be safe and
      roadworthy before deployed to public road. <br /> The working group
      openGENESIS drives and supports the investigations of a verifiable,
      understandable and not least certifiable AI. The working group will
      supply public and regulation authorities with necessary approaches to
      deal with the challenges of approval and certification of AI.
    </p>
    <p>openGENESIS provides a collaboration platform for knowledge
      exchange among members. The members can cooperate with each other
      efficiently and provide the results in an open access domain.
      openGENESIS enables research with direct access to practice and
      safety relevant challenges.</p>
    <h2 id="scopeandobjectives">Scope and Objectives</h2>
    <p>The highly challenging task to enable assessing AI for the safety
      critical case of autonomous driving will be addressed by global
      collaboration among industry, research and regulators. To equip this
      collaboration with the necessary framework, the openGENESIS working
      group will coordinate and support the activities on the following
      topics:</p>
    <ul>
      <li>Develop necessary methods, tools and metrics for verification and
        validation of AI</li>
      <li>Support the evolution of functional safety approaches for AI</li>
      <li>Establish an open accessible platform for AI training and
        validation data sets</li>
      <li>Drive the aspects and discussion of reproducibility and
        explainability of AI</li>
      <li>Encourage and support academic research efforts within the scope
        of openGENESIS</li>
      <li>Support a global collaboration and drive a common understanding
        of quality of AI</li>
      <li>Maintain the scope as well as the objectives of the openGENESIS
        working group</li>
      <li>Coordinate the development of related Eclipse Foundation projects
        towards an openGENESIS objective.</li>
      <li>Define and manage the specifications for interfaces and functions
        for the working group.</li>
    </ul>
    <h2 id="governanceandprecedence">Governance and Precedence</h2>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <h3 id="applicabledocuments">Applicable Documents</h3>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li> 
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership
      Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse
      Foundation, including but not limited to the Intellectual Property
      and Antitrust Policies.</p>
    <p>In the event of any conflict between the terms set forth in this
      working group's charter and the Eclipse Foundation Bylaws, Membership
      Agreement, Development Process, Working
      Group Process, or any policies of the Eclipse Foundation, the terms
      of the Eclipse Foundation Bylaws, Membership Agreement, process, or
      policy shall take precedence.</p>
    <h2 id="membership">Membership</h2>
    <p>In order to participate in the openGENESIS working group, an
      entity must have executed the openGENESIS Participation Agreement, and adhere to the requirements set forth in this
      Charter.</p>
    <p>The openGENESIS working group is open at any time to all new
      members who meet these conditions.</p>
    <p>There are different classes of openGENESIS working group membership -
      Driver Member, Development Member and Guest Member. Each of these classes is
      described in detail below.</p>
    <h3 id="classesofopengenesismembership">Classes of openGENESIS
      Membership</h2>
    <h3 id="drivermembers">Driver Members</h3>
    <p>Driver Members are organizations that view the openGENESIS
      activities as strategic to their organization. These members will
      invest resources to sustain and shape the activities of this working
      group as well as maintain the scope of openGENESIS. Driver Members
      include users and strategic contributors related to the technologies
      and results provided by the openGENESIS working group. Driver members
      will lead the operation of the working group, support the
      "openGENESIS Spotlight Projects" ("GSLP") and will act as first
      contact of the working group. An openGENESIS WG Driver Member
      must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation.</p>
    <h3 id="developmentmembers">Development Members</h3>
    <p>Development Members are organizations that are interested to
      benefit from the openGENESIS community. They will lead or participate
      in "openGENESIS Spotlight Projects" ("GSLP") and investigate the
      technical questions. They will propose and execute the GSLPs
      according to the processes defined by the openGENESIS working group.
      Development members will invest efforts to the openGENESIS Spotlight
      Projects ("GSLP") as defined in the proposal. An openGENESIS WG
      Development Member must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse
      Foundation.</p>
    <h3 id="guestmembers">Guest Members</h3>
    <p>Guest members are organizations who have been invited for one year
      by the Steering Committee of openGENESIS WG. Typical Guest Members
      include academic entities or non-profit organizations, who want to
      have a closer look before deciding on their strategy. Guest Members
      are allowed to participate in the openGENESIS WG within the scope of
      their invitation. Guest Members are not allowed to become a Steering
      Committee Member and they have no right to vote concerning the matters
      of the working group. Invitations may be renewed by the Steering
      Committee. Guest Members are required to sign the participation
      agreement. An openGENESIS WG Guest Member must be at least an Associate
      Member of the Eclipse Foundation.</p>
    <h2 id="governance">Governance</h2>
    <p>This openGENESIS working group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem through the
        creation and execution of openGENESIS Spotlight Projects ("GSLP"),</li>
      <li>a means to organize the community of each project or component so
        that users and developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the "Steering Committee" as
      the sole governance body for the working group has been defined.</p>
    <h2 id="steeringcommittee">Steering Committee</h2>
    <h3 id="powersandduties">Powers and Duties</h3>
    <p>The Steering Committee is required to:</p>
    <ul>
      <li>Define and maintain the scope and objectives of the working group</li>
      <li>Define and maintain the roadmap of the working group</li>
      <li>Define and maintain the life-cycle definition of the openGENESIS
        Spotlight Projects ("GSLP")</li>
      <li>Approve proposals to become GSLPs</li>
      <li>Monitor existing GSLPs</li>
      <li>Define the annual fees for all classes of working group membership</li>
      <li>Approve the annual budget based upon funds received through fees</li>
      <li>Contribute to the acquisition of new working group members by actively engaging in community development and evangelism</li>
      <li>Define services to be executed from Eclipse Foundation Management
        Organization ("EMO") and allocate funding for such services</li>
      <li>Review this charter regularly</li>
    </ul>
    <h3 id="composition">Composition</h3>
    <ul>
      <li>Each Driver Member of the working group has one seat on the
        Steering Committee.</li>
      <li>One seat is allocated for Development Members. An additional seat
        is allocated for each additional 5 Driver Members beyond the first
        Driver Member.</li>
    </ul>
    <h2 id="commondispositions">Common Dispositions</h2>
    <p>The dispositions below apply to all governance bodies for this
      working group, unless otherwise specified. For all matters related to
      membership action, including without limitation: meetings, quorum,
      voting, electronic voting action without meeting, vacancy,
      resignation or removal, the respective terms set forth in the
      Eclipse Foundation Bylaws apply.</p>
    <h3 id="voting">Voting</h3>
    <p>For actions (i) approving specifications for adoption; (ii) approving or changing the name of the working group; and (iii) approving changes to annual Member contribution requirements; any
      such actions must be approved by no less than two-thirds (2/3) of the
      representatives present at a steering committee meeting.</p>
    <h3 id="termanddatesofelections">Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee. All
      representatives shall hold office until their respective successors
      are appointed or elected, as applicable. There shall be no
      prohibition on re-election or re-designation of any representative
      following the completion of that representative's term of office.</p>
    <p>Elected representatives shall each serve one-year terms and shall
      be elected to serve from January 1 to December 31 of each calendar
      year, or until their respective successors are elected and qualified.</p>
    <h3 id="meetingsmanagement">Meetings Management</h3>
    <p>All meetings may be held at any place that has been designated by
      resolution of the corresponding body. All meetings may be held
      remotely using phone calls, video calls or any other means as
      designated by resolution of the corresponding body.</p>
    <h3 id="infrastructureandservices">Infrastructure and Services</h2>
    <p>The openGENESIS Working Group uses the normal infrastructure
      provided to Eclipse projects, including, among others, mailing lists,
      forums, bug trackers, source code repositories, continuous
      integration servers, build servers, and web sites. Additional
      services may be requested by the Steering Committee of the working
      group on demand. Cost for such services shall be covered by
      membership fees as set forth in this working group charter.</p>
    <h3 id="communitybuilding">Community Building</h3>
    <p>The Eclipse Foundation will provide access to its network of
      developers to facilitate the community building and the dissemination
      of the openGENESIS innovations. The Eclipse Foundation will use its
      communication channels such as its Newsletter, Twitter, or LinkedIn
      channels to promote key openGENESIS events and encourage its members
      to join and participate in the openGENESIS working group.</p>
    <h3 id="membershipfeestructure">Membership Fee Structure</h3>
    <p>The following table lists the annual openGENESIS working group fees
      payable to the Eclipse Foundation.</p>
      <table class="table">
      <tr>
        <th>Annual Turnover</th>
        <th>WG Fees Driver Member<sup><a href="#sup1">1</a></sup> </th>
        <th>WG Fees Development Member</th>
        <th>WG Fees Guest Members<sup><a href="#sup2">2</a></sup> </th>
      </tr>
      <tr>
        <td>&gt; $250 million </td>
        <td>TBD</td>
        <td>TBD</td>
        <td>€ 0</td>
      </tr>
      <tr>
        <td>&gt; $100 Million &lt;= $250 million</td>
        <td>TBD</td>
        <td>TBD</td>
        <td>€ 0</td>
      </tr>
      <tr>
        <td>&gt; $50 million &lt;= $100 million</td>
        <td>TBD</td>
        <td>TBD</td>
        <td>€ 0</td>
      </tr>
      <tr>
        <td>&gt; $10 million &lt;= $50 million</td>
        <td>TBD</td>
        <td>TBD</td>
        <td>€ 0</td>
      </tr>
      <tr>
        <td>&lt; $10 million </td>
        <td>TBD</td>
        <td>TBD</td>
        <td>€ 0</td>
      </tr>
      <tr>
        <td>&lt; $1 million, &lt; 10 employees </td>
        <td>TBD</td>
        <td>TBD</td>
        <td>€ 0</td>
      </tr>
      </table>
    <p>
      <sup id="sup1">1</sup> Alternative for research facilities only: annual fees can be provided in form of annual participation services of 150 person-days/year. (A minimum qualification of master degree or higher is required for a person-day.) TBD
    </p>
    <p>
      <sup id="sup2">2</sup> Guest Members pay no fees
    </p>

    <p class="margin-top-20"><strong>Version History</strong></p>
    <ul>
      <li>November 2020 - Updates in support of the Eclipse Foundation corporate restructuring</li>
    </ul>
  </div>
</div>