<?php

/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 2.0 Revision history at end of document.</p>
    <h2>Vision and Scope</h2>
    <p>The Eclipse IDE Working Group is formed to ensure the continued sustainability, integrity, evolution and adoption of the Eclipse IDE suite of products and related technologies. In particular, it is formed to provide governance, guidance, and funding for the communities that support the delivery of the Eclipse Foundation&rsquo;s flagship &ldquo;Eclipse IDE&rdquo; products.</p>

    <p>The Working Group will:</p>
    <ul>
      <li>Oversee development of the release plan;</li>
      <li>Provide governance, resources, and funding for those aspects of the development of the Eclipse IDE packages that are required for the simultaneous release, as well as for resources directly related to IDE packages;</li>
      <li>Set the criteria by which final products are &ldquo;official&rdquo; and define the rules by which these decisions are made;</li>
      <li>Provide the framework under which the production of all intermediate products required in the creation of the Eclipse IDE downloads is carried out;</li>
      <li>Act in coordination with EMO in administering the services hosted;</li>
      <li>Assist in the fostering of the growth and evolution of the ecosystem.</li>
    </ul>
    <p>The Eclipse IDE Working Group promotes the &quot;Eclipse IDE&quot; brand and its value in the marketplace, provides vendor-neutral marketing and other services to the Eclipse IDE ecosystem, leverages Eclipse Foundation licensing and intellectual property flows that encourage community participation, provides governance for the wholistic technical and business strategies for related open source projects; and establishes and drives a funding model that enables this working group and its community to operate on a sustainable basis.</p>

    <h3>Projects and Committers</h3>
    <p>The Eclipse IDE Working Group considers all Eclipse open source projects that participate in the Eclipse IDE simultaneous release within its purview. Additional projects may be added by a decision of the Steering Committee. All developers who hold current committer status on the Eclipse IDE Working Group projects are considered to be committers of the Eclipse IDE Working Group and are eligible to become Committer Members of this Working Group, as defined below.</p>

    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <ul>
      <li><a href="/org/documents/eclipse-foundation-be-bylaws-en.pdf">Eclipse Foundation Bylaws</a></li>
      <li><a href="/org/workinggroups/process.php">Eclipse Foundation Working Group Process</a></li>
      <li><a href="/org/workinggroups/operations.php">Eclipse Foundation Working Group Operations Guide</a></li>
      <li><a href="/org/documents/Community_Code_of_Conduct.php">Eclipse Foundation Code of Conduct</a></li>
      <li><a href="/org/documents/communication-channel-guidelines/">Eclipse Foundation Communication Channel Guidelines</a></li>
      <li><a href="/org/documents/eclipse-foundation-membership-agreement.pdf">Eclipse Foundation Membership Agreement</a></li>
      <li><a href="/org/documents/Eclipse_IP_Policy.pdf">Eclipse Foundation Intellectual Property Policy</a></li>
      <li><a href="/org/documents/Eclipse_Antitrust_Policy.pdf">Eclipse Foundation Antitrust Policy</a></li>
      <li><a href="/projects/dev_process/development_process.php">Eclipse Foundation Development Process</a></li>
      <li><a href="https://www.eclipse.org/legal/logo_guidelines.php">Eclipse Foundation Trademark Usage Guidelines</a></li>
    </ul>
    <p>All members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to follow the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
    <p>In the event of any conflict between the terms set forth in this Working Group&#39;s Charter and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>

    <h2>Membership</h2>
    <p>With the exception of Guest members as described below, an entity must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation, have executed the Eclipse IDE Working Group Participation Agreement once defined and adhere to the requirements set forth in this Charter to participate.</p>
    <p>The participation fees associated with each of these membership classes are shown in the Annual Participation Fees section. These are annual fees, and are established by the Steering Committee, and will be updated in this charter document accordingly.</p>
    <p>The fees associated with membership in the Eclipse Foundation are separate from, and in addition to, any Working Group membership fees, and are decided as described in the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Bylaws</a> and detailed in the <a href="https://www.eclipse.org/org/documents/eclipse-foundation-membership-agreement.pdf">Eclipse Foundation Membership Agreement</a>.</p>
    <p>
      There are five classes of Eclipse IDE Working Group membership -
      Platinum, Strategic, Supporter, Committer, and Guest.
    </p>

    <h2>Classes of Membership</h2>

    <h3>Platinum Members</h3>
    <p>
      Platinum Members are organizations that view the Eclipse IDE Working
      Group's vision, scope, and technologies as mission critical to their
      organization's future, and are investing significant resources to sustain
      and define the core activities that are the responsibility of the working
      group.
    </p>
    <p>
      In particular, Platinum Members are interested in the long term planning
      and evolution of the Eclipse IDE Working Group.
    </p>
    <p>
      Platinum Members may participate in all areas of the working group and
      its committees.
    </p>
    <p>
      Platinum Members of the Eclipse IDE Working Group must be at least a
      <a href="/membership/#tab-levels">Contributing Member</a>
      of the Eclipse Foundation and are encouraged to have contributors
      participating in the working group's initiatives or related projects.
    </p>
    <p>
      Platinum Members of the Eclipse IDE working group are required to commit
      to a minimum of three (3) years of participation as either a Platinum or
      Strategic member.
    </p>

    <h3>Strategic Members</h3>
    <p>Strategic Members are organizations that view the Eclipse IDE Working Group&rsquo;s vision, scope, and technologies as critical to their organization&rsquo;s future, and are investing significant resources to sustain and define the core activities that are the responsibility of the working group.</p>
    <p>Strategic Members may participate in all areas of the working group and its committees.</p>
    <p>
      Strategic Members of the Eclipse IDE Working Group must be at least a
      <a href="/membership/#tab-levels">Contributing Member</a>
      of the Eclipse Foundation and are encouraged to have contributors
      participating in the working group&rsquo;s initiatives or related projects.
    </p>
    <p>
      Strategic Members of the Eclipse IDE working group are required to commit
      to a minimum of three (3) years of participation as either a Platinum or
      Strategic member.
    </p>

    <h3>Supporter Members</h3>
    <p>Supporter Members are typically organizations that view the Eclipse IDE Working Group&rsquo;s vision, scope, and technologies as critical parts of their organization&#39;s business operations. These organizations want to influence the direction and support the development of an ecosystem through the Eclipse IDE Working Group.</p>
    <p>
      Supporter Members are eligible to participate in all areas of the working
      group and its committees. Supporter Members may be elected to the
      Steering Committee, and may request to participate in subcommittees, the
      decision for which is taken by the Steering Committee.
    </p>
    <p>Supporter Members of the Eclipse IDE Working Group must be at least a <a href="/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation.</p>

    <h3>Committer Members</h3>
    <p>Committers are individuals who through a process of meritocracy defined by the Eclipse Development Process are able to contribute and commit code to the Eclipse Foundation projects included in the scope of this working group. For clarity, see the Projects and Committer section above.</p>
    <p>Committers who work for an organization who is a member of this working group are Committer Members. Additionally, Committers may choose to complete the Individual Committer Member process of the Eclipse Foundation, along with completing this working group&rsquo;s Participation Agreement. For further explanation and details, see the <a href="https://www.eclipse.org/membership/become_a_member/committer.php">Eclipse Committer Membership</a> page.</p>

    <h3>Guest Members</h3>
    <p>Guest Members are organizations which are Associate members of the Eclipse Foundation who have been invited for one year, renewable, by the Steering Committee to participate in particular aspects of the activities of the Working Group. Typical guests include R&D partners, universities, academic research centers, etc. Guests may be invited to participate in committee meetings at the invitation of the respective committee, but under no circumstances do Guest members have voting rights. Guest members are required to execute the working group’s Participation Agreement.</p>

    <p>All matters related to Membership in the Eclipse Foundation and this working group will be governed by the Eclipse Foundation Bylaws, Membership Agreement, and Working Group Process. These matters include, without limitation, delinquency, payment of dues, termination, resignation, reinstatement, and assignment.</p>

    <h3>Sponsorship</h3>
    <p>Sponsors are companies or individuals who provide money or services to the working group on an ad hoc basis to support the activities of the Eclipse IDE Working Group and its managed projects.</p>
    <p>Money or services provided by sponsors are used as set forth in the working group annual budget. The working group is free to determine whether and how those contributions are recognized. Under no condition are sponsorship monies refunded.</p>
    <p>Sponsors need not be members of the Eclipse Foundation or of the Eclipse IDE Working Group.</p>

    <h3>Special Interest Groups</h3>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within
      the Working Group with a focus to collaborate around a particular topic
      or domain of direct interest to the working group. SIGs are designed to
      drive the objectives of a subset of the Members of the Working Group in
      helping them achieve a dedicated set of goals and objectives. The scope
      of the SIG must be consistent with the scope of the Working Group
      Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each
      SIG may be either temporary or a permanent structure within the working
      group. SIGs can be disbanded at any time by self selection presenting
      reasons to and seek approval from the Steering Committee. Steering
      Committees may disband a SIG at any time for being inactive or
      non-compliant with the Working Group's Charter, or by request of the SIG
      itself. SIGs operate as a non-governing Body of the Working Group. There
      are no additional annual fees to Members for participation in a SIG.
    </p>

    <h2>Governing Bodies</h2>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>
        Define and manage the strategy of the working group;
      </li>
      <li>
        Review and approve this charter;
      </li>
      <li>
        Establish an annual program plan;
      </li>
      <li>
        Determine how available funds will be allocated, what work will be
        outsourced, under what terms, the acceptance criteria, and if the
        criteria have been met;
      </li>
      <li>
        Define the annual fees for all classes of working group membership;
      </li>
      <li>
        Approve the annual budget based upon funds received through fees and
        sponsorship;
      </li>
      <li>
        Approve the creation of subcommittees and define the purpose, scope,
        and membership of each such subcommittee; and
      </li>
      <li>
        Approve the creation and retirement of Special Interest Groups (SIGs).
      </li>
    </ul>

    <h4>Composition</h4>
    <p>
      Each Platinum and Strategic Member of the working group has a seat on the
      Steering Committee.
    </p>
    <p>At least one seat each is allocated to Supporter and Committer Members. Supporter and Committer Member seats are allocated following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</p>
    <p>The Planning Council chair or their designate is eligible to a seat on the Steering Committee.</p>
    <p>
      The Committee elects a chair of the Steering Committee. This chair is
      elected among the members of the Committee. They will serve for a 12
      month period or until their successor is elected and qualified, or as
      otherwise provided for in this Charter.
    </p>

    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least quarterly.</p>

    <h3>Planning Council</h3>
    <p>The Planning Council is responsible for establishing a roadmap and coordinated release plan that balances the many competing requirements. The release plan describes the themes and priorities that focus these releases, and orchestrates the dependencies among project plans.</p>
    <h4>Powers and Duties</h4>
    <p>The Planning Council is responsible for managing the Eclipse IDE Simultaneous Release, cross-project planning, facilitating the mitigation of architectural issues and user interface conflicts, and all other coordination and integration issues. The Planning Council discharges its responsibility via collaborative evaluation, prioritization, and compromise.</p>
    <p>The Planning Council has the following responsibilities:</p>
    <ul>
      <li>Planning and requirements gathering and management of the Eclipse IDE and SimRel;</li>
      <li>Setting the names and dates of Eclipse IDE product releases;</li>
      <li>Setting, evolving, and enforcing the policies and procedures for participation of Eclipse open source projects that contribute content to the Eclipse IDE Simultaneous Release;</li>
      <li>Setting, evolving, and enforcing policies and procedures for the creation and distribution of Eclipse IDE products, along with all intermediate products;</li>
      <li>Setting, evolving, and enforcing policies and procedures governing supporting products (e.g., marketplaces, statistics gathering and data collection services);</li>
      <li>Establishing practices to mitigate risk and ensure overall quality of the processes and resulting products;</li>
      <li>Providing technical guidance and oversight for the working group’s projects;</li>
      <li>Establishing developer communication channels; and</li>
      <li>Establishing user support channels.</li>
    </ul>
    <h4>Composition</h4>
    <p>Members of the Planning Council are generally senior technical experts.</p>
    <p>The Planning Council is composed of:</p>
    <ul>
      <li>One representative designated by the Project Management Committee (PMC) for every top-level project for which at least one subproject is a simultaneous release participant;</li>
      <li>Two representatives for each Platinum Member;</li>
      <li>One representative designated for each Strategic Member; and</li>
      <li>Additional representatives as approved by the Steering Committee.</li>
    </ul>
    <p>
      The Planning Council elects a chair of the Planning Council. This chair
      is elected among the members of the Council. They will serve for a 12
      month period or until their successor is elected and qualified, or as
      otherwise provided for in this Charter.
    </p>
    <h4>Meeting Management</h4>
    <p>The Planning Council meets monthly.</p>

    <h2>Membership Representation Summary</h2>
    <div>
      <table class="table table-stripped">
        <thead>
          <tr>
            <th>Committee Representation</th>
            <th class="text-center">
              Platinum Member
            </th>
            <th class="text-center">
              Strategic Member
            </th>
            <th class="text-center">
              Supporter Member
            </th>
            <th class="text-center">
              Committer Member
            </th>
            <th class="text-center">
              Guest Member
            </th>
            <th class="text-center">
              PMC Representative
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Steering Committee Member
            </td>
            <td class="text-center">
              Appointed
            </td>
            <td class="text-center">
              Appointed
            </td>
            <td class="text-center">
              Elected
            </td>
            <td class="text-center">
              Elected
            </td>
            <td class="text-center">
              N/A
            </td>
            <td class="text-center">
              N/A
            </td>
          </tr>
          <tr>
            <td>
              Planning Council
            </td>
            <td class="text-center">
              Appointed (2 representatives)
            </td>
            <td class="text-center">
              Appointed
            </td>
            <td class="text-center">
              N/A
            </td>
            <td class="text-center">
              N/A
            </td>
            <td class="text-center">
              N/A
            </td>
            <td class="text-center">
              Appointed
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <h3>Common Dispositions</h3>
    <p>
      The dispositions below apply to all governance bodies for this working
      group, unless otherwise specified. For all matters related to membership
      action, including without limitation: meetings, quorum, voting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse
      Foundation Bylaws apply.
    </p>

    <p>
      Appointed representatives on the Body may be replaced by the Member
      organization they are representing at any time by providing written
      notice to the Steering Committee. In the event a Body member is
      unavailable to attend or participate in a meeting of the Body, they may
      be represented by another Body member by providing written proxy to the
      Body’s mailing list in advance. As per the Eclipse Foundation Bylaws, a
      representative shall be immediately removed from the Body upon the
      termination of the membership of such representative's Member
      organization.
    </p>

    <h4>Voting</h4>
    <h5>Simple Majority</h5>
    <p>
      Excepting the actions specified below for which a Super Majority is
      required, votes of the Body are determined by a simple majority of the
      representatives represented at a committee meeting at which a quorum is
      present.
    </p>

    <h5>Super Majority</h5>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors
      approve a specification license; (ii) approving specifications for
      adoption; (iii) modifying the working group charter; (iv) approving or
      changing the name of the working group; and (v) approving changes to
      annual Member contribution requirements; any such actions must be
      approved by no less than two-thirds (2/3) of the representatives
      represented at a committee meeting at which a quorum is present.
    </p>

    <h3>Meeting Management</h3>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all
      meetings related to the working group will follow a prepared agenda and
      minutes are distributed two weeks after the meeting and approved at the
      next meeting at the latest, and shall in general conform to the 
      <a href="/org/documents/Eclipse_Antitrust_Policy.pdf">Eclipse Foundation Antitrust Policy</a>.
    </p>

    <h4>Place of Meetings</h4>
    <p>All meetings may be held at any place that has been designated from time-to-time by resolution of the corresponding body. All meetings may be held remotely using phone calls, video calls, or any other means as designated from time-to-time by resolution of the corresponding body.</p>

    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice
      of same has been provided to each of the representatives at least fifteen
      (15) calendar days prior to such meeting, which notice will identify all
      potential actions to be undertaken by the Body at the Body meeting. No
      representative will be intentionally excluded from Body meetings and all
      representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because
      one or more of the representatives cannot attend or participate so long
      as at least a quorum of the Body is represented at the Body meeting.
    </p>

    <h4>Actions</h4>
    <p>The body may undertake an action only if it was identified in a body meeting notice or otherwise identified in a notice of special meeting.</p>

    <h4>Invitations</h4>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right of vote.</p>

    <h4>Term and Dates of Elections</h4>
    <p>All representatives shall hold office until their respective successors are appointed or elected, as applicable. There shall be no prohibition on re-election or re-designation of any representative following the completion of that representative's term of office.</p>

    <h5>Platinum and Strategic Members</h5>
    <p>
      Platinum and Strategic Members Representatives shall serve in such
      capacity on committees until the earlier of their removal by their
      respective appointing Member organization or as otherwise provided for in
      this Charter.
    </p>

    <h5>Elected Representatives</h5>
    <p>
      Elected representatives shall each serve one-year terms and shall be
      elected to serve for a 12 month period, or until their respective
      successors are elected and qualified, or as otherwise provided for in
      this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this
      Charter.
    </p>

    <h2>Working Group Participation Fees</h2>
    <p>
      The following fees have been established by the Eclipse IDE WG Steering
      Committee. These fees are in addition to each participant's membership
      fees in the Eclipse Foundation.
    </p>
    <p>
      Established fees cannot be changed retroactively and any fee structure
      change will be communicated to all members via the mailing list and will
      be charged the next time a member is invoiced.
    </p>

    <h3>Eclipse IDE Platinum Member Annual Participation Fees</h3>
    <p>
      Platinum members are required to execute the Eclipse IDE WG Participation
      Agreement.
    </p>
    <p>
      Platinum members are required to commit to a minimum of three (3) years
      of participation as either a Platinum or Strategic member.
    </p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th>
            <p>Corporate Revenue</p>
          </th>
          <th>
            <p>Annual Fees</p>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <p>Annual Corporate Revenues greater than €1 billion</p>
          </td>
          <td class="text-center">
            <p>€150 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>
              Annual Corporate Revenues greater than €500 million but less than
              or equal to €1 billion
            </p>
          </td>
          <td class="text-center">
            <p>€100 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>
              Annual Corporate Revenues greater than €100 million but less than
              or equal to €500 million
            </p>
          </td>
          <td class="text-center">
            <p>€75 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>
              Annual Corporate Revenues greater than €10 million but less than
              or equal to €100 million
            </p>
          </td>
          <td class="text-center">
            <p>€37 500</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Annual Corporate Revenues less than or equal to €10 million</p>
          </td>
          <td class="text-center">
            <p>€7 500</p>
          </td>
        </tr>
      </tbody>
    </table>

    <h3>Eclipse IDE Strategic Member Annual Participation Fees</h3>
    <p>
      Strategic members are required to execute the Eclipse IDE Participation
      Agreement.
    </p>
    <p>
      Strategic members are required to commit to a minimum of three (3) years
      of participation as either a Platinum or Strategic member.
    </p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th>
            <p>Corporate Revenue</p>
          </th>
          <th>
            <p>Annual Fees</p>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <p>Annual Corporate Revenues greater than €1 billion</p>
          </td>
          <td class="text-center">
            <p>€30 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Annual Corporate Revenues greater than €500 million but less than or equal to €1 billion</p>
          </td>
          <td class="text-center">
            <p>€20 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Annual Corporate Revenues greater than €100 million but less than or equal to €500 million</p>
          </td>
          <td class="text-center">
            <p>€15 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Annual Corporate Revenues greater than €10 million but less than or equal to €100 million</p>
          </td>
          <td class="text-center">
            <p>€7 500</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Annual Corporate Revenues less than or equal to €10 million</p>
          </td>
          <td class="text-center">
            <p>€1 500</p>
          </td>
        </tr>
      </tbody>
    </table>

    <h3>Eclipse IDE Supporter Member Annual Participation Fees</h3>
    <p>Supporter members are required to execute the Eclipse IDE Participation Agreement.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th>
            <p>Corporate Revenue</p>
          </th>
          <th>
            <p>Annual Fees</p>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <p>Annual Corporate Revenues greater than €1 billion</p>
          </td>
          <td class="text-center">
            <p>€10 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Annual Corporate Revenues less than or equal to €1 billion but greater than €10 million</p>
          </td>
          <td class="text-center">
            <p>€5 000</p>
          </td>
        </tr>
        <tr>
          <td>
            <p>Annual Corporate Revenues less than or equal to €10 million</p>
          </td>
          <td class="text-center">
            <p>€500</p>
          </td>
        </tr>
      </tbody>
    </table>

    <h3>Eclipse IDE Committer and Guest Member Annual Participation Fees</h3>
    <p>Committer and Guest members pay no annual fees, but are required to execute the Eclipse IDE Participation Agreement.</p>

    <hr />
    <p>Charter Version History</p>
    <ul>
      <li>v0.1 created October, 2020</li>
      <li>v0.2 created February, 2021</li>
      <li>v0.3 created April, 2021 (establish and clarify membership fees, other rights and requirements)</li>
      <li>v1.0 created May, 2021 (approved by steering committee)</li>
      <li>
        v2.0 created July, 2023 (introduction of Platinum Membership class and
        standard refinements)
      </li>
    </ul>
  </div>
</div>
