<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Mike Milinkovich (Eclipse Foundation)
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version v1.7 Revision history is at end of document.</p>
    <h2>Vision and Scope</h2>
    <p>The Jakarta EE working group will drive the evolution and broad adoption of technologies
      derived from or related to the Eclipse Enterprise for Java (EE4J) project. Eclipse Enterprise
      for Java (EE4J) is an open source initiative to create technical specifications,
      implementations of those APIs, and technology compatibility kits for Java runtimes that enable
      development, deployment, and management of server-side and cloud-native applications. EE4J is
      based on the Java™ Platform, Enterprise Edition (Java EE) standards, and uses Java EE 8 as the
      baseline for creating new specifications and implementations.</p>
    <p>Eclipse Foundation projects will provide technical implementations of API specifications and
      TCKs. The working group will:</p>
    <ul>
      <li>Promote the "Jakarta EE" brand and its value in the marketplace.</li>
      <li>Provide vendor neutral marketing and other services to the Jakarta EE ecosystem.</li>
      <li>Define and manage a specification process to formalize the specifications that are defined
        within the scope of this working group.</li>
      <li>Define compatibility rules and a compatibility and branding process for implementations of
        these specifications to ensure application portability.</li>
      <li>Leverage Eclipse Foundation defined licensing and intellectual property flows that encourage community participation,
        protect community members, and encourage usage.</li>
      <li>Manage the overall technical and business strategies for EE4J and related projects.</li>
      <li>Establish and drive a funding model that enables this working group and its community to
        operate on a sustainable basis.</li>
    </ul>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>      
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
      <li>Jakarta EE Trademark Guidelines</li>
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the
      requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the
      Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>

    <p>All Members must further be parties to the Jakarta EE Working Group Participation Agreement.</p>
    <p>In the event of any conflict between the terms set forth in this working group's charter and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>
    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a <a
        href="/membership/become_a_member/membershipTypes.php#contributing"
      >Contributing Member</a> of the Eclipse Foundation, have executed the Jakarta EE Working Group Participation
      Agreement, and adhere to the requirements set forth in this Charter to participate.
    </p>
    <p>There are five classes of Jakarta EE working group membership - Strategic, Enterprise,
      Participant, Committer, and Guest. Each of these classes is described in detail below.</p>
    <p>The participation fees associated with each of these membership classes is shown in the
      tables in Schedule A. These are annual fees, and are established by the Jakarta EE Steering
      Committee, and will be updated in this charter document accordingly.</p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any working
      group membership fees, and are decided as described in the <a
        href="https://www.eclipse.org/org/documents/eclipse-foundation-be-bylaws-en.pdf"
      >Eclipse Foundation Bylaws</a> and detailed in the <a
        href="https://www.eclipse.org/org/documents/eclipse-foundation-membership-agreement.pdf"
      >Eclipse Foundation Membership Agreement</a>.
    </p>
    <h2>Classes of Jakarta EE membership</h2>
    <h3>Strategic members</h3>
    <p>Strategic Members are organizations that view enterprise Java technology as strategic to
      their organization and are investing significant resources to sustain and shape the activities
      of this working group.</p>
    <p>Strategic Members of the Jakarta EE working group must be at least a Contributing Member of the
      Eclipse Foundation, and have a minimum of 4 developers participating on EE4J projects.</p>
    <h3>Enterprise members</h3>
    <p>Enterprise Members are typically organizations that view enterprise Java technology as a
      critical part of their organization's business operations. These organizations want to support
      the development and evolution of the enterprise Java technology ecosystem.</p>
    <p>Enterprise Members of the Jakarta EE working group must be at least Contributing Members of the
      Eclipse Foundation.</p>
    <h3>Participant members</h3>
    <p>Participant Members are typically organizations that deliver products or services based on
      enterprise Java technology. These organizations want to participate in the development of the
      enterprise Java technology ecosystem.</p>
    <p>Participant Members of the Jakarta EE working group must be at least Contributing Members of the
      Eclipse Foundation.</p>
    <h3>Committer members</h3>
    <p>
      Committer Members are individuals who through a process of meritocracy defined by the Eclipse
      Development Process are able to contribute and commit code to the Eclipse Foundation projects
      included in the scope of this working group. Committers may be members by virtue of working
      for a member organization, or may choose to complete the membership process independently if
      they are not. For further explanation and details, see the <a
        href="http://www.eclipse.org/membership/become_a_member/committer.php"
      >Eclipse Foundation Committer Membership</a> page.
    </p>
    <h3>Guest members</h3>
    <p>Guest Members are organizations which are Associate members of the Eclipse Foundation that wish to participate in
      particular aspects of the activities of the Working Group. Typical guests include JUGs, R&D
      partners, universities, academic research centers, etc. Guests may be invited to participate
      in committee meetings at the invitation of the respective committee, but under no
      circumstances do Guest members have voting rights. Guest members are required to execute the
      Jakarta EE Working Group Participation Agreement.</p>
    <h2>Membership Summary</h2>
    <table class="table">
      <thead>
        <tr>
          <th></th>
          <th>Strategic Member</th>
          <th>Enterprise Member</th>
          <th>Participant Member</th>
          <th>Committer Member</th>
          <th>Guest Member</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Member of the Steering Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Invitation Only</td>
        </tr>
        <tr>
          <td>Member of the Specification Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Invitation Only</td>
        </tr>
        <tr>
          <td>Member of the Marketing Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Invitation Only</td>
        </tr>
      </tbody>
    </table>
    <p>All matters related to Membership in the Eclipse Foundation and this Jakarta EE working group
      will be governed by the Eclipse Foundation Bylaws, Membership Agreement and Working
      Group Process. These matters include, without limitation, delinquency, payment of dues,
      termination, resignation, reinstatement, and assignment.</p>
    <h2>Governance</h2>
    <p>This Jakarta EE working group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a welcoming and professional environment,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the following governance bodies have been defined
      (each a "Body"):</p>
    <ul>
      <li>The Steering Committee</li>
      <li>The Specification Committee</li>
      <li>The Marketing and Brand Committee</li>
    </ul>
    <h2>Steering Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this
        working group. This will require acceptance of the specification process by these projects.</li>
      <li>Define and manage a process for projects outside of the Eclipse Foundation to be included
        in the Jakarta EE platform.</li>
      <li>Define and manage the roadmaps.</li>
      <li>Review and approve this charter.</li>
      <li>Review and approve the specification process.</li>
      <li>Review and approve the trademark policy to ensure compatibility of independent
        implementations of specifications.</li>
      <li>Define the annual fees for all classes of working group membership.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Steering Committee.</li>
      <li>At least two seats are allocated to Enterprise Members. Enterprise Member seats are
        allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation
        Bylaws.</li>
      <li>At least one seat is allocated to Participant Members. Participant Member seats are
        allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation
        Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. Committer Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
    </ul>
    <h3>Meeting Management</h3>
    <p>The Steering Committee meets at least twice a year.</p>
    <h2>Specification Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Specification Committee members are required to:</p>
    <ul>
      <li>Define the specification process to be used by all Jakarta EE specifications, and refer to
        it for approval by the Steering Committee.</li>
      <li>Ensure that all specification expert groups operate in an open, transparent, and
        vendor-neutral fashion in compliance with the specification process.</li>
      <li>Approve specifications for adoption by the community.</li>
      <li>Approve profiles which define collections of specifications which meet a particular market
        requirement.</li>
      <li>Work with the Eclipse Enterprise for Java Project Management Committee (EE4J PMC) to
        ensure that the specification process is complied with by all specification projects.</li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Specification Committee.</li>
      <li>At least two seats are allocated to Enterprise Members. Enterprise Member seats are
        allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>At least one seat is allocated to Participant Members. Participant Member seats are
        allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. Committer Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>At least one seat is allocated to a representative of the EE4J PMC. The EE4J PMC may
        decide how to select its representative.</li>
      <li>Guest members that have been invited by the Jakarta EE Steering Committee as observers.
        Guest members have no voting rights.</li>
      <li>Any additional individuals as designated from time to time by the Executive Director.</li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected
        among the members of the Committee. They will serve for a 12 month period, or until their successor is elected and qualified, or as otherwise provided
        for in this Charter.</li>
    </ul>
    <h3>Meeting Management</h3>
    <p>The Specification Committee meets at least once per quarter.</p>
    <h2>Marketing and Brand Committee</h2>
    <h3>Powers and Duties</h3>
    <p>The Marketing and Brand Committee members are required to:</p>
    <ul>
      <li>Define the trademark policy to be used by all Jakarta EE specifications, and refer to it
        for approval by the Steering Committee.</li>
      <li>Ensure the consistency of logo usage and other marketing materials</li>
      <li>Define and implement marketing and communication activities for the working group</li>
      <li>Define and implement developer outreach programs</li>
      <li>Provide requirements to the Eclipse Foundation for conferences and events related to
        Jakarta EE</li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Marketing Committee.</li>
      <li>At least two seats are allocated to Enterprise Members. Enterprise Member seats are
        allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>At least one seat is allocated to Participant Members. Participant Member seats are
        allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. Committer Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>Guest members that have been invited by the Jakarta EE Steering Committee as observers.
        Guest members have no voting rights.</li>
      <li>Any additional individuals as designated from time to time by the Executive Director.</li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected
        among the members of the Committee. They will serve for a 12 month period, or until their successor is elected 
        and qualified, or as otherwise provided for in this Charter.</li>
    </ul>
    <h3>Meeting Management</h3>
    <p>Marketing Committee meets at least once per quarter.</p>
    <h2>Common Dispositions</h2>
    <p>The dispositions below apply to all governance bodies for this working group, unless
      otherwise specified. For all matters related to membership action, including without
      limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws
      apply.</p>
    <p>Appointed representatives on the Body may be replaced by the Member
      organization they are representing at any time by providing written notice to the Steering
      Committee. In the event a Body member is unavailable to attend or participate in a meeting of
      the Body, they may send a representative and may vote by proxy, or they may be represented by 
      another Body member by providing written proxy to the Body’s mailing list in advance. As per the Eclipse Foundation
      Bylaws, a representative shall be immediately removed from the Body upon the termination of
      the membership of such representative’s Member organization.</p>
    <h3 id="voting">Voting</h3>
    <h4>Super Majority</h4>
    <p>For actions (i) requesting that the Eclipse Foundation Board of Directors approve a
      specification license; (ii) approving specifications for adoption; (iii) approving or changing the name of the
      working group; and (iv) approving changes to annual Member contribution requirements; any such
      actions must be approved by no less than two-thirds (2/3) of the representatives represented 
      at a committee meeting at which a quorum is present.</p>
    <h3>Term and Dates of elections</h3>
    <p>All representatives shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or re-designation of any
      representative following the completion of that representative’s term of office.</p>
    <h4>Strategic Members</h4>
    <p>Strategic Members Representatives shall serve in such capacity on committees until the
      earlier of their removal by their respective appointing Member organization or as otherwise
      provided for in this Charter.</p>
    <h4>Elected representatives</h4>
    <p>Elected representatives shall each serve one-year terms and shall be elected to serve for a 12 month period, 
      or until their respective successors are elected
      and qualified, or as otherwise provided for in this Charter. Procedures governing elections of
      Representatives may be established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this Charter.</p>
    <h3>Meetings Management</h3>
    <h4>Place of meetings</h4>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding Body. All meetings may be held remotely using phone calls,
      video calls or any other means as designated from time-to-time by resolution of the
      corresponding Body.</p>
    <h4>Regular meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representatives at least fifteen (15) calendar days
      prior to such meeting, which notice will identify all potential actions to be undertaken by
      the Body at the Body meeting. No representative will be intentionally excluded from Body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the Body is
      represented at the Body meeting. Electronic voting shall be permitted in conjunction with any
      and all meetings of the Body the subject matter of which requires a vote of the Body to be
      delayed until each such representative in attendance thereat has conferred with his or her
      respective Member organization as set forth in Section <a href="#voting">Voting</a> above.
    </p>
    <h4>Actions</h4>
    <p>The Body may undertake an action only if it was identified in a Body Meeting notice or
      otherwise identified in a notice of special meeting.</p>
    <h3>Invitations</h3>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right
      of vote.</p>
    <br> 
    <p>
      <strong>Charter Version History</strong>
    </p>
    <p>
      v1.0 created February 5, 2018<br> 
      Revised v1.1 February 27, 2018 - Added Guest membership<br>
      Revised v1.2 March 6, 2018 - Changed name from EE.next to Jakarta EE<br> 
      Revised v1.3 May 8, 2018 - Changed name of Influencer Member to Enterprise Member<br> 
      Revised v1.4 September 11, 2018 - Defined the annual membership fees; altered the Eclipse Foundation 
      membership level requirements to allow Solutions members of the Foundation to become Strategic, 
      Enterprise or Solutions members of the working group<br>
      Revised v1.5 November 24, 2020 - Updates in support of the Eclipse Foundation corporate restructuring<br>
      Revised v1.6 September 14, 2021 - Updates to support removal of strategic member 3 year commitment, 
      Enterprise Requirements Committee and other administrative adjustments<br>
      Revised v1.7 February 13, 2024 - Update to Schedule A to formalize that
      Java User Groups who are Contributing members of the Eclipse Foundation
      may act as participant members with no additional fee.
    </p>
    <h2>Jakarta EE Working Group Annual Participation Fees Schedule A</h2>
    <p>The following fees have been established by the Jakarta EE Steering Committee.</p>
    <h3>Jakarta EE Strategic Member Annual Participation Fees</h3>
    <table class="table">
      <tr>
        <th>Corporate Revenue</th>
        <th>Annual Fees</th>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $1 billion</td>
        <td>$300,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $500 million but less than or equal to $1 billion
        </td>
        <td>$200,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $100 million but less than or equal to $500
          million</td>
        <td>$100,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $10 million but less than or equal to $100
          million</td>
        <td>$50,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues less than or equal to $10 million</td>
        <td>$25,000</td>
      </tr>
    </table>
    <h3>Jakarta EE Enterprise Member Annual Participation Fees</h3>
    <table class="table">
      <tr>
        <th>Corporate Revenue</th>
        <th>Annual Fees</th>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $1 billion</td>
        <td>$50,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $500 million but less than or equal to $1 billion
        </td>
        <td>$35,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $100 million but less than or equal to $500
          million</td>
        <td>$20,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $10 million but less than or equal to $100
          million</td>
        <td>$10,000</td>
      </tr>
    </table>
    <h3>Jakarta EE Participant Member Annual Participation Fees</h3>
    <table class="table">
      <tr>
        <th>Corporate Revenue</th>
        <th>Annual Fees</th>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $1 billion</td>
        <td>$20,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $500 million but less than or equal to $1 billion
        </td>
        <td>$15,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $100 million but less than or equal to $500
          million</td>
        <td>$10,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues greater than $10 million but less than or equal to $100
          million</td>
        <td>$7,500</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues less than or equal to $10 million</td>
        <td>$5,000</td>
      </tr>
      <tr>
        <td>Annual Corporate Revenues less than $1 million and < 10 employees, and Java User Groups</td>
        <td>$0</td>
      </tr>
    </table>
    <h3>Jakarta EE Committer Member Annual Participation Fees</h3>
    <p>Committer members pay no annual fees, but are required to execute the Jakarta EE
      Participation Agreement.</p>
    <h3>Jakarta EE Guest Member Annual Participation Fees</h3>
    <p>Guest members pay no annual fees, but are required to execute the Jakarta EE Participation
      Agreement.</p>
  </div>
</div>
