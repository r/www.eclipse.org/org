<?php

/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 1.4 - Revision history is at end of document.</p>
    <h2>Vision and Scope</h2>
    <p>The OSGi Working Group will drive the evolution and broad adoption of software technologies
      derived from or related to the OSGi Specification Project. The OSGi Specification Project is an
      open source initiative to create software specifications, implementations of those specifications, and
      Technology Compatibility Kits (TCKs) for those specifications that enable development, deployment,
      and management of embedded, server-side and cloud-native applications. The OSGi Specification
      Project is based on Specifications, Reference Implementations and Compliance Tests of the former
      OSGi Alliance and uses those materials as the baseline for creating new specifications,
      implementations and TCKs, or revising existing OSGi Alliance specifications, reference
      implementations and compliance tests.</p>
    <p>The working group will:</p>
    <ul>
      <li>Provide vendor neutral services to the OSGi ecosystem.</li>
      <li>Leverage the Eclipse Foundation Specification Process to formalize the specifications that are defined within the scope of this working group.</li>
      <li>Define compatibility rules and a compatibility and branding process for implementations of these specifications to ensure application portability.</li>
      <li>Leverage Eclipse Foundation licensing and intellectual property flows that encourage community participation, protect community members, and encourage usage.</li>
      <li>Manage the overall technical and business strategies for the OSGi Working Group and related projects.</li>
      <li>Establish and drive a funding model that enables this working group and its community to operate on a sustainable basis.</li>
      <li>Promote the OSGI brand and its value in the marketplace.</li>
    </ul>

    <h2>Governance and Precedence</h2>
    <p><strong>Applicable Documents</strong></p>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
    <p>In the event of any conflict between the terms set forth in this working group's charter and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>

    <h2>Membership</h2>
    <p>
      Strategic and Participant working group members must be at least a <a href="/membership/#tab-levels"><strong>Contributing Member</strong></a> of the Eclipse Foundation. All working group members must have executed the OSGi Working Group Participation Agreement, and adhere to the requirements set forth in this Charter to participate.
    </p>
    <p>
      The participation fees associated with each of these membership classes is shown in the tables in Schedule A. These are annual fees, and are established by the OSGi Working Group Steering Committee, and will be updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any working group membership fees, and are decided as described in the <a href="/org/documents/eclipse_foundation-bylaws.pdf">Eclipse Foundation Bylaws</a> and detailed in the <a href="/org/documents/eclipse_membership_agreement.pdf">Eclipse Foundation Membership Agreement</a>.
    </p>
    <p>There are four classes of OSGi working group membership - Strategic, Participant, Committer, and Guest. Each of these classes is described in detail below.</p>

    <h3>Classes of OSGi Working Group Membership</h3>
    <h4>Strategic Members</h4>
    <p>Strategic Members are organizations that view OSGi technology as strategic to their organization and are investing significant resources to sustain and shape the activities of this working group.</p>
    <p>Strategic Members of the OSGi working group must be at least a Contributing Member of the Eclipse Foundation, and have a minimum of 1 developer participating on the OSGi Specification Project.</p>
    <h4>Participant Members</h4>
    <p>Participant Members are typically organizations that deliver products or services based on OSGi technology. These organizations want to participate in the development of the OSGi technology ecosystem.</p>
    <p>Participant Members of the OSGi working group must be at least Contributing Members of the Eclipse Foundation.</p>
    <h4>Committer Members</h4>
    <p>Committer Members are individuals who through a process of meritocracy defined by the Eclipse Foundation Development Process are able to contribute and commit materials to the Eclipse Foundation projects included in the scope of this working group. Committers may be members by virtue of working for a member organization, or may choose to complete the membership process independently if they are not. For further explanation and details, see the <a href="/membership/become_a_member/committer.php">Eclipse Foundation Committer Membership</a> page.</p>
    <h4>Guest Members</h4>
    <p>
      Guest Members are organizations which are Associate members of the Eclipse Foundation. Typical guests include JUGs, R&D partners, universities, academic research centers, etc. Guests may be invited to participate in committee meetings at the invitation of the respective committee, but under no circumstances do Guest members have voting rights. Guest members are required to execute the OSGi Working Group Participation Agreement.
    </p>
    <h3>Membership Summary</h3>
    <table class="table table-bordered">
      <tbody>
        <tr>
          <td> </td>
          <td>
            Strategic Member
          </td>
          <td>
            Participant Member
          </td>
          <td>
            Committer Member
          </td>
          <td>
            Guest Member
          </td>
        </tr>
        <tr>
          <td>
            Member of the Steering Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
        </tr>
      </tbody>
    </table>

    <h2>Special Interest Groups</h2>
    <p>A Special Interest Group (SIG) is a lightweight structure formed within the Working Group with a focus to collaborate around a particular topic or domain of direct interest to the working group. SIGs are designed to drive the objectives of a subset of the Members of the Working Group in helping them achieve a dedicated set of goals and objectives. The scope of the SIG must be consistent with the scope of the Working Group Charter.</p>
    <p>The creation of a SIG requires approval of the Steering Committee. Each SIG may be either temporary or a permanent structure within the working group. SIGs can be disbanded at any time by self selection presenting reasons to and seek approval from the Steering Committee. Steering Committees may disband a SIG at any time for being inactive or non compliant with the Working Group's Charter, or by request of the SIG itself. SIGs operate as a non-governing Body of the Working Group. There are no additional annual fees to Members for participation in a SIG.</p>

    <h2>Sponsorship</h2>
    <p>Sponsors are companies or individuals who provide money or services to the working group on an ad hoc basis to support the activities of the Working Group and its managed projects. Money or services provided by sponsors are used as set forth in the working group annual budget. The working group is free to determine whether and how those contributions are recognized. Under no condition are sponsorship monies refunded.</p>
    <p>Sponsors need not be members of the Eclipse Foundation or of the Working Group.</p>

    <h2>Governance</h2>
    <p>This OSGi Working Group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and developers define the roadmap collaboratively.</li>
    </ul>

    <h2>Governing Bodies</h2>
    <p>In order to implement these principles, the following governance bodies have been defined (each a "Body"):</p>
    <ul>
      <li>The Steering Committee. For greater clarity, the Steering Committee shall also fulfill the responsibilities of the Specification Committee as those responsibilities are defined in the Eclipse Foundation Specification Process.</li>
    </ul>
    </h2>

    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this working group. This will require acceptance of the specification process by these projects.</li>
      <li>Define and manage a process for projects outside of the Eclipse Foundation to be included in the OSGi Working Group Materials.</li>
      <li>Define and manage the roadmaps.</li>
      <li>Review and approve this charter.</li>
      <li>Define, review and approve the trademark policy to be used by all OSGi Specification Project specifications and to ensure compatibility of independent implementations of specifications.</li>
      <li>Define the annual fees for all classes of working group membership.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Ensure the consistency of logo usage.</li>
      <li>Approve (if any) customizations to the Eclipse Foundation Specification Process (EFSP).</li>
      <li>Define the specification process to be used by all OSGi Specification Project specifications.</li>
      <li>Ensure that all specification expert groups operate in an open, transparent, and vendor-neutral fashion in compliance with the specification process.</li>
      <li>Approve specifications for adoption by the community.</li>
      <li>Approve profiles which define collections of specifications which meet a particular market requirement.</li>
      <li>Define (if any) customizations to the EFSP. The EFSP with the approved customizations is the specification process to be used by all specifications related to this working group.</li>
      <li>Approve the creation of subcommittees and define the purpose, scope, and membership of each such subcommittee.</li>
      <li>Approve the creation and retirement of Special Interest Groups (SIGs).</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Steering Committee.</li>
      <li>Two seats are allocated to Participant Members. Additional seats may be added by a super-majority resolution of the Steering Committee. Participant Member seats are elected following the Eclipse Foundation "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>One seat is allocated to Committer Members. Additional seats may be added by a super-majority resolution of the Steering Committee. Committer Member seats are elected following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>Guest members may be invited as observers. Guest members have no voting rights.</li>
      <li>Any additional individuals as designated from time to time by the Eclipse Foundation Executive Director.</li>
      <li>The Committee elects a chair of the Steering Committee. This chair is elected among the members of the Committee. They will serve for a 12 month period or until their successor is elected and qualified, or as otherwise provided for in this Charter. There is no limit on the number of terms the chair may serve.</li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>

    <h3>Common Dispositions</h3>
    <p>The dispositions below apply to all governance bodies for this working group, unless otherwise specified. For all matters related to membership action, including without limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy, resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws apply.</p>
    <p>Appointed representatives on the Body may be replaced by the Member organization they are representing at any time by providing written notice to the Steering Committee. In the event a Body member is unavailable to attend or participate in a meeting of the Body, they may be represented by
      another Body member by providing written proxy to the Body’s mailing list in advance. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the Body upon the termination of the membership of such representative’s Member organization.</p>
    <h4>Voting</h4>
    <p><strong>Simple Majority</strong></p>
    <p>Excepting the actions specified below for which a Super Majority is required, votes of the Body are determined by a simple majority of the representatives represented at a committee meeting at which a quorum is present.</p>


    <p><strong>Super Majority</strong></p>
    <p>For actions (i) requesting that the Eclipse Foundation Board of Directors approve a specification license;
      (ii) approving specifications for adoption; (iii) modifying the working group charter; (iv) approving or changing the name of the working group; and (v) approving changes to annual Member contribution requirements; any such actions must be approved by no less than two-thirds (2/3) of the representatives at a committee meeting at which a quorum is present.</p>

    <h4>Term and Dates of Elections</h4>
    <p>This section only applies to the Steering Committee</p>
    <p>All representatives shall hold office until their respective successors are appointed or elected, as
      applicable. There shall be no prohibition on re-election or re-designation of any representative following
      the completion of that representative’s term of office.</p>
    <p>
      <strong>Strategic Members</strong>
    </p>
    <p>Strategic Members Representatives shall serve in such capacity on committees until the earlier of their
      removal by their respective appointing Member organization or as otherwise provided for in this Charter.</p>
    <p>
      <strong>Elected Representatives</strong>
    </p>
    <p>Elected representatives shall each serve one-year terms and shall be elected to serve for a 12 month term, or until their respective successors are elected and qualified, or as
      otherwise provided for in this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided that such resolutions are not
      inconsistent with any provision of this Charter.</p>
    <h4>Meetings Management</h4>
    <p>As prescribed in the Eclipse Foundation Working Group Process, all meetings related to the working group will follow a prepared agenda and minutes are distributed two weeks after the meeting and approved at the next meeting at the latest, and shall in general conform to the Eclipse Foundation Antitrust Policy.</p>

    <p><strong>Meeting Frequency</strong></p>
    <p>Each governing body meets at least twice a year. All meetings may be held at any place that has been designated from time-to-time by resolution of the corresponding Body. All meetings may be held remotely using phone calls, video calls, or any other means as designated from time-to-time by resolution of the corresponding Body.</p>

    <p><strong>Place of Meetings</strong></p>
    <p>All meetings may be held at any place that has been designated from time-to-time by resolution of the
      corresponding Body. All meetings may be held remotely using phone calls, video calls or any other
      means as designated from time-to-time by resolution of the corresponding Body.</p>
    <p>
      <strong>Regular Meetings</strong>
    </p>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been provided to
      each of the representatives at least fifteen (15) calendar days prior to such meeting,
      which notice will identify all potential actions to be undertaken by the Body at the Body meeting. No
      representative will be intentionally excluded from Body meetings and all representatives shall receive
      notice of the meeting as specified above; however, Body meetings need not be delayed or rescheduled
      merely because one or more of the representatives cannot attend or participate so long as at least a
      quorum of the Body is represented at the Body meeting. Electronic voting shall be permitted in
      conjunction with any and all meetings of the Body the subject matter of which requires a vote of the Body
      to be delayed until each such representative in attendance thereat has conferred with his or her
      respective Member organization as set forth in Section <strong>Voting</strong> above.
    </p>
    <p>
      <strong>Actions</strong>
    </p>
    <p>The Body may undertake an action only if it was identified in a Body Meeting notice or otherwise
      identified in a notice of special meeting.</p>
    <h4>Invitations</h4>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right of vote.</p>

    <h3>Working Group Participation Fees</h3>
    <p>Established fees cannot be charged retroactively and any fee structure change will be communicated to all members via the mailing list and will be charged the next time a member is invoiced.</p>

    <h3>OSGi Working Group Annual Participation Fees Schedule A</h3>
    <p>The following fees have been established by the OSGi Working Group Steering Committee.</p>

    <h3>OSGi Working Group Strategic Member Annual Participation Fees</h3>
    <p>Strategic members are required to execute the OSGi Working Group Participation Agreement, and to begin paying the fees shown below effective January 1, 2021.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="55%">Corporate Revenue</th>
          <th class="text-center" width="20%">Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than $100 million</td>
          <td class="text-center">$15,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to $100 million</td>
          <td class="text-center">$3,500</td>
        </tr>
      </tbody>
    </table>

    <h3>OSGi Working Group Participant Member Annual Participation Fees</h3>
    <p>Participant members are required to execute the OSGi Working Group Participation Agreement, and to begin
      paying the fees shown below effective January 1, 2021.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="55%">Corporate Revenue</th>
          <th class="text-center" width="20%">Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than $10 million, or has 10 or more employees</td>
          <td class="text-center">$5,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to $10 million, and less than 10 employees</td>
          <td class="text-center">$1,500</td>
        </tr>
      </tbody>
    </table>

    <h3>OSGi Working Group Committer Member Annual Participation Fees</h3>
    <p>Committer members pay no annual fees, but are required to execute the OSGi Working Group Participation Agreement.</p>

    <h3>OSGi Working Group Guest Member Annual Participation Fees</h3>
    <p>Guest members pay no annual fees, but are required to execute the OSGi Working Group Participation Agreement.</p>

    <p class="margin-top-20"><strong>OSGi Working Group Charter Version History</strong></p>
    <ul>
      <li>v0.90 Initial draft created May 5, 2020</li>
      <li>v0.91 May 7, 2020, Eliminated Enterprise class membership; Eliminated Marketing Committee and moved relevant duties to the Steering Committee; Changed “compliance tests” to “TCKs” where appropriate; Minor corrections to fees tables.</li>
      <li>v0.92 May 12, 2020, Revised utilizing the feedback provided on V0.91 by OSGi Board and Officers</li>
      <li>v0.93 October, 2020, Revised by Eclipse Foundation</li>
      <li>v0.94 November 6, 2020, Dan Bandera changes to Participant Member fee, and minimum number of Participant Member seats.</li>
      <li>v1.0 - December 10, 2020, Charter ratified by OSGi Steering Committee</li>
      <li>v1.1 - February 4, 2021, Updated in support of the Eclipse Foundation corporate restructuring</li>
      <li>v1.2 - August 16, 2021, Align committee seats as per approval of Steering Committee, remove references to OSGi PMC in both Specification Powers and Duties and Composition</li>
      <li>v1.3 - July, 2022, Add Steering Committee Chair to Steering Committee composition and other standard refinements</li>
      <li>v1.4 - March, 2024, Steering Committee assumes the responsibility for the Specification Committee and removal of strategic three year commitment</li>
    </ul>
  </div>
</div>