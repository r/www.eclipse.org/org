<?php

/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Draft Version 0.3 - Not Yet Approved</p>
    <p>Revision History at End</p>
    <h2>Vision and Scope</h2>
    <p>
      The OpenHW Europe Working Group (EWG) is a Working Group of the Eclipse Foundation AISBL. The
      OpenHW EWG is a joint initiative of the Eclipse Foundation AISBL and the OpenHW Group, a Canadian
      non-profit with a close partnership with the Eclipse Foundation. As such, Members wishing to contribute
      to the initiatives of the OpenHW EWG are required to be members of both Eclipse Foundation and
      OpenHW Group. Further details are described below.
    </p>
    <p>
      The overarching goal of the EWG is to address Europe-focused initiatives complementing the work
      program of the OpenHW Group. Those initiatives may address strategic or market analysis, technical
      requirements, and digital sovereignty. By working collaboratively, members of Eclipse Foundation and
      OpenHW Group can pursue those initiatives and have a positive impact with strategic open hardware
      and related open software projects.
    </p>
    <p>The specific tasks of EWG, outlined in more detail below, are:</p>
    <ul>
      <li>To ensure the establishment and operation of a European-located code repository for the
        projects related to OpenHW Group;
      </li>
      <li>To participate in European Projects relevant to European-centric open hardware and related
        open software initiatives;
      </li>
      <li>To gather European-centric requirements and strategic/technology needs for an open hardware
        ecosystem and where appropriate to incubate projects arising from that discussion under the
        OpenHW Group top level project.
      </li>
      <li>To carry out European marketing initiatives in support of the EWG’s initiatives. </li>
    </ul>
    <p>
      The EWG will work closely with the OpenHW Group’s Technical Working Group. In addition, to advance
      the objective of carrying out marketing initiatives, it is expected the EWG will work closely with the
      OpenHW Group’s Marketing Working Group. Both of these groups are defined in the OpenHW Group’s
      Bylaws. The EWG may recommend that projects be initiated by the OpenHW Group by developing and
      presenting project concept proposals to the OpenHW Group Technical Working Group.
    </p>

    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>
      The following governance documents are applicable to this charter, each of which can be found on the
      <a href="/org/documents/">Eclipse Foundation Governance Documents</a> page or
      the <a href="/legal/">Eclipse Foundation Legal Resources</a>.
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership Agreement, including the
      requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of
      the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this Working Group’s Charter and the Eclipse
      Foundation Bylaws, Memberships Agreement, Development Process, Specification Process, Working
      Group Process or any policies of the Eclipse Foundation, the terms of the respective Eclipse Foundation
      Bylaws, Membership Agreement, process or policy shall take precedence.
    </p>
    <p>
      In addition, all Members must be a party to the OpenHW Group Membership Agreement and are
      expected to adhere to the OpenHW Group Bylaws and then-current policies. More details are available
      at <a href="https://www.openhwgroup.org/membership/">https://www.openhwgroup.org/membership/</a>.
    </p>

    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a
      <a href="/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation
      (formerly Solutions Member), and an OpenHW Group Supporter, Silver, Gold, or Platinum Member.
      An entity must have executed the OpenHW EWG Working Group Participation Agreement and adhere to
      the requirements set forth in this Charter to participate.
    </p>
    <p>
      The participation fees, if any, associated with each of these membership classes are shown in the Annual
      Participation Fees section. These are annual fees, and are established by the OpenHW Europe Steering
      Committee, and will be updated in this Charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any Working Group
      membership fees, and are described in the Eclipse Foundation Bylaws and detailed in the Eclipse
      Foundation Membership Agreement. The fees associated with membership in the OpenHW Group are
      separate from any Working Group membership fees, and are described in the OpenHW Membership
      Agreement.
    </p>
    <p>There are two classes of EWG membership - Participant and Guest.</p>
    <h2>Classes of Membership</h2>
    <h3>Participant Members</h3>
    <p>
      Participant Members are typically organizations that deliver products or services based upon related
      standards, specifications and technologies, or view this Working Group’s technologies and related
      initiatives as strategic to their organization. These organizations want to participate in the development
      and direction of an open ecosystem related to this Working Group. Participant Members of this Working
      Group must be at least a Contributing Member of the Eclipse Foundation and an OpenHW Group
      Supporter, Silver, Gold, or Platinum member.
    </p>
    <h3>Guest Members</h3>
    <p>
      Guest Members are organizations which are Associate members of the Eclipse Foundation and must be a
      Supporter, Silver, Gold or Platinum member of OpenHW Group. Typical guests include R&D partners,
      universities, academic research centers, etc. Guests may be invited to participate in committee meetings
      at the invitation of the respective committee, but under no circumstances do Guest members have
      voting rights. Guest members are required to execute the Working Group’s Participation Agreement.
    </p>

    <h2>Governance</h2>
    <p>The OpenHW EWG is designed as:</p>
    <ul>
      <li>a vendor-neutral, member-driven organization</li>
      <li>a means to organize European-focused activities of the OpenHW Group ecosystem</li>
    </ul>
    <p>
      In addition to meeting all Eclipse Foundation governance requirements, the EWG governance model is
      intended to align closely with the activities and initiatives of the OpenHW Group.
    </p>

    <h2>Governing Bodies</h2>
    <h2>Steering Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li> Ensure the strategy of the Working Group is defined and managed.</li>
      <li>Ensure the Eclipse projects included within the scope of the Working Group are defined and
        managed.
      </li>
      <li> Ensure the research initiatives of the Working Group are identified and approved prior to being
        pursued.
      </li>
      <li>
        Work in conjunction with the OpenHW Technical Working Group and other OpenHW Working
        Groups as appropriate, to ensure visibility, alignment, and coordination with those Working
        Groups.
      </li>
      <li>
        Working in conjunction with the OpenHW Marketing Working Group, establish and manage a
        marketing plan.
      </li>
      <li>Review and approve this charter.</li>
      <li>Define the annual fees for all classes of the Working Group members.</li>
      <li>Establish an annual program plan.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
    </ul>
    <h3>Composition</h3>
    <p>
      A minimum of two and up to five representatives shall be elected from among the Participant Members.
      Participant Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in the
      Eclipse Foundation Bylaws.
    </p>
    <p>
      The Steering Committee elects a chair of the Steering Committee. This chair is elected among the
      members of the Committee. The chair serves from September 1 to August 31 of each calendar year, or
      until their successor is elected and qualified, or as otherwise provided for in this Charter. There is no
      limit on the number of terms the chair may serve.
    </p>
    <h3>Meeting Management</h3>
    <p>The Steering Committee meets at least twice a year.</p>

    <h2>Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this Working Group, unless otherwise
      specified. For all matters related to membership action, including without limitation: meetings, quorum,
      voting, electronic voting action without meeting, vacancy, resignation or removal, the respective terms
      set forth in the Eclipse Foundation Bylaws apply.
    </p>
    <h3>Good Standing</h3>
    <p>
      A representative shall be deemed to be in Good Standing, and thus eligible to vote on issues coming
      before the Body they participate in, if the representative has attended (in person or telephonically) a
      minimum of three (3) of the last four (4) Body meetings (if there have been at least four meetings).
      Appointed representatives on the Body may be replaced by the Member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a Body
      member is unavailable to attend or participate in a meeting of the Body, they may be represented by
      another Body member by providing written proxy to the Body’s mailing list in advance which shall be
      included in determining whether the representative is in Good Standing. As per the Eclipse Foundation
      Bylaws, a representative shall be immediately removed from the Body upon the termination of the
      Eclipse Foundation membership of such representative’s Member organization.
    </p>
    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body are
      determined by a simple majority of the representatives in Good Standing represented at a committee
      meeting at which a quorum is present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions (i) approving or changing the name of the Working Group; and (ii) approving changes to
      annual Member contribution requirements; any such actions must be approved by no less than
      two-thirds (2/3) of the representatives in Good Standing represented at a committee meeting at which a
      quorum is present.
    </p>
    <h3>Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee.</p>
    <p>
      All representatives shall hold office until their respective successors are appointed or elected, as
      applicable. There shall be no prohibition on re-election or re-designation of any representative following
      the completion of that representative's term of office.
    </p>
    <h4>Elected representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve from September 1
      to August 31 of each calendar year, or until their respective successors are elected and qualified, or as
      otherwise provided for in this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided that such resolutions are not
      inconsistent with any provision of this Charter.
    </p>
    <h3>Meetings Management</h3>
    <h4>Meeting Frequency</h4>
    <p>
      Each governing body meets at least twice a year. All meetings may be held at any place that has been
      designated from time-to-time by resolution of the corresponding Body. All meetings may be held
      remotely using phone calls, video calls, or any other means as designated from time-to-time by
      resolution of the corresponding Body.
    </p>
    <h4>Place of Meetings</h4>
    <p>
      All meetings may be held at any place that has been designated from time-to-time by resolution of the
      corresponding body. All meetings may be held remotely using phone calls, video calls, or any other
      means as designated from time-to-time by resolution of the corresponding body.
    </p>
    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been provided to
      each of the representatives in Good Standing at least fifteen (15) calendar days prior to such meeting,
      which will identify all potential actions to be undertaken by the Body at the Body meeting. No
      representative will be intentionally excluded from Body meetings and all representatives shall receive
      notice of the meeting as specified above; however, Body meetings need not be delayed or rescheduled
      merely because one or more of the representatives cannot attend or participate so long as at least a
      quorum of the Body is represented at the Body meeting.
    </p>
    <h4>Actions</h4>
    <p>
      The body may undertake an action only if it was identified in a body meeting notice or otherwise
      identified in a notice of special meeting.
    </p>
    <h4>Invitations</h4>
    <p>The Body may invite any Member to any of its meetings. These invited attendees have no right to vote.</p>

    <h2>Working Group Fees</h2>
    <p><strong>Currently, there are no fees associated with participation in this Working Group.</strong></p>

    <p class="margin-top-20"><strong>Charter History</strong></p>
    <ul>
      <li>v0.2 Eclipse Foundation proposed draft</li>
      <li>v0.3 Eclipse Foundation review</li>
    </ul>
  </div>
</div>