<?php

/**
 * Copyright (c) 2022 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Zhou Fang (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p><strong>Version 1.0</strong> January 21, 2022</p>
    <h2>Objective:</h2>
    <p>
      At the Eclipse Foundation, most open source projects progress using developers who are employed
      by members, self-employed, or volunteers. In some instances Working Groups may, in exceptional
      circumstances, choose to allocate a portion of their budget to fund development efforts in support
      of the projects within their purview. Working Groups with applicable budgets are able to take
      advantage of an initiative to spend funds to contract development efforts to advance the working
      group and/or its associated projects.
    </p>
    <p>
      Such funding is over and above the more typical way projects and initiatives get advanced. Examples
      of such development efforts include project process enhancements or updates, addressing
      outstanding issues or other inhibitors preventing use or adoption of the project(s) in their current
      form, or addressing issues related to the working group hosting a version of its own project(s) (e.g.,
      the Open VSX initiative within Eclipse Cloud Development Tools working group), etc.
    </p>
    <p>
      In order to carry out such development efforts, the Eclipse Foundation will manage the development
      effort on behalf of the working group. The Foundation will, in turn, seek to engage with capable
      service providers to see that these development efforts get carried out.
    </p>
    <h3>About this Document:</h3>
    <p>
      This document is intended to provide guidance to working groups on how the Eclipse Foundation will
      carry out this management and to identify the various roles and responsibilities associated with
      these types of efforts. This document is neither exhaustive nor binding but rather describes the
      general process; specific roles, responsibilities and tasks which will be decided upon in each
      individual case.
    </p>

    <h2>Summary:</h2>
    <p>
      The Eclipse Foundation (EF) will engage with the Working Group&apos;s Steering Committee, or their
      delegate if so designated (for example, the Planning Council in the Eclipse IDE Working Group) as
      the key stakeholders in the community to determine the highest priority development effort requests
      to be addressed, convert these priorities into actionable development tasks, and then engage with
      qualified resources to carry out these tasks.
    </p>
    <p>
      The guiding principles of the process are:
    </p>
    <ul>
      <li>
        To adhere to the principles of transparency and openness,
      </li>
      <li>
        To complement the existing community development efforts,
      </li>
      <li>
        To encourage a “multiplying effect” where community participation is amplified by this
        funding program&apos;s undertakings,
      </li>
      <li>
        To ensure the funds allocated fit within the overall working group program plan and budget.
      </li>
    </ul>

    <h2>Governance:</h2>
    <p>
      Arranging for the implementation of development efforts and initiatives will be managed exclusively
      by the Eclipse Foundation on behalf of the working group. The EF commits to being transparent with
      regard to with whom it has chosen to engage and a summary of the expenditures made on behalf of
      the working group. Having said this, the EF recognizes and supports that pricing for services is
      considered confidential by many vendors, and the EF will work to respect that confidentiality while
      still ensuring working group members are aware of the total value being delivered.
    </p>
    <p>
      The Foundation will designate an individual to manage the various tasks on its behalf (the “EF Rep”).
      It is expected that the Steering Committee or other committee will name one to three delegates to
      collaborate with the EF Rep. The delegate(s) can come from within the Steering Committee or
      another working group body, or their designate if more appropriate. Together, the delegates and the
      EF Rep form a small ad hoc team (the “Team”).
    </p>

    <h2>Budget:</h2>
    <p>
      The amount of funds available for development will vary, and will be subject to the available Working
      Group funds as allocated in the working group budget.
    </p>

    <h2>Phases:</h2>
    <p>
      To create a process that is easier to track, budget for, and measure success, development will be
      done through a series of phases:
    </p>
    <h3>1. Identify the Prioritized Development Efforts</h3>
    <p>
      The Steering Committee or their delegate(s) will provide a prioritized backlog of enhancements that
      are to be implemented. These will typically be in the form of open issues or bugs in the community,
      and sufficient in detail to allow for potential vendors or committers to understand the desired
      outcomes and to sufficiently investigate the effort involved in carrying out the development.
    </p>
    <p>
      From this input and interaction, the Team will capture this list as a set of desired development
      efforts in a form that will allow vendors and/or committers to put forward proposals for carrying out
      the development efforts. The Team will also identify the relative allocated budget to assign to each
      of the development efforts.
    </p>
    <p>
      The description of each Development Effort is expected to include, as a minimum:
    </p>
    <ul>
      <li>task name and Gitlab Issue id,</li>
      <li>time estimate, if possible,</li>
      <li>expected deliverable(s)/outcome(s),</li>
      <li>identify any dependencies on external resources including Eclipse IT infrastructure updates,</li>
      <li>means of demonstration and/or verification of completion.</li>
    </ul>
    <h3>2. Bid Solicitation and Contract Process</h3>
    <p>
      The EF Rep will decide, with consultation with the Team, the most effective way to solicit bids. This
      may include an open bidding process, though that is not a requirement if it is felt doing so is unlikely
      to lead to competitive bids. If an open bidding process is chosen, it is expected to be approximately
      2 weeks in duration to enable potential contractors to interact with the EF Rep to seek additional
      information regarding the development efforts.
    </p>
    <p>
      Regardless of process, the Eclipse Foundation may solicit bids from specific committers, Members,
      and/or contractors should it feel particular expertise or experience is required to be successful. In
      addition, it is expected that the Team will suggest potential committers and/or contractors they
      believe have particular expertise or experience as well. Any bids submitted by
      contractors/committers may be noted as confidential bids to protect the submitter&apos;s pricing strategy.
    </p>
    <p>Typically, each development contract will include the following:</p>
    <ul>
      <li>Description of the approach to address the Development Effort,</li>
      <li>Identification of who will perform the work (i.e., named developers),</li>
      <li>The proposed timeframe for the development effort,</li>
      <li>Any dependencies/caveats that exist that must be addressed,</li>
      <li>Proposed outcome for the work, including how to demonstrate the work has been
        completed,</li>
      <li>Explicit list of deliverables, including any interim deliverables,</li>
      <li>A proposed price, including whether the bid is being made on a fixed price or time and
        materials basis.</li>
    </ul>
    <p>
      Potential contractors may request that any bids or negotiations with EF be treated as confidential for
      competitive reasons, and EF will work to accommodate such requests to the extent possible.
    </p>
    <p>
      However, all contractors must agree to EF disclosing the name and total value of any contracted
      Development Effort.
    </p>
    <p>
      The output of this phase is a documented and agreed Statement of Work.
    </p>
    <h3>3. Award of Contract(s)</h3>
    <p>
      In evaluating proposals from vendors, the EF will base its decisions on the following criteria:
    </p>
    <ul>
      <li>Price and timeliness,</li>
      <li>Plan for proposed development and deliverables,</li>
      <li>
        Skillset of proposed developers, with preference given to committers in the relevant area,
      </li>
      <li>
        Bidder&apos;s relationship with EF, with preference given to either Contributing Members with
        committers, or self-employed committers with relevant expertise,
      </li>
      <li>
        Any additional relevant elements in the bid, including delivery date, whether fixed price vs.
        time and materials basis, etc.
      </li>
    </ul>
    <p>
      Contractors will be expected to execute a Services Agreement and Statement of Work (SOW) with
      the Eclipse Foundation for each winning bid.
    </p>
    <h3>4. Sign-Off on Completed Statement of Work</h3>
    <p>
      To assist the EF in managing the individual development efforts, the Team will advise EF Rep on the
      satisfactory completion of each development effort. For each Development Effort, the contractor is
      expected to provide:
    </p>
    <ul>
      <li>Open source code. In most cases, it will be expected that the acceptance criteria for code is
        that it has been committed by the relevant Eclipse project. Other scenarios may be possible
        in exceptional circumstances,
      </li>
      <li>Test harness(es) for code,</li>
      <li>
        Documentation, including appropriate documentation related to Intellectual Property. This
        includes updates to the relevant public bug(s)/issue(s),
      </li>
      <li>
        Any other appropriate deliverables, including those to which the contractor committed in the
        original SOW.
      </li>
    </ul>
  </div>
</div>