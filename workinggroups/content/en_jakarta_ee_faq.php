<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <h2 class="h3">What is the purpose of a working group?</h2>
    <p>An Eclipse Foundation working group is a special-purpose consortia of Eclipse Members
      interested in supporting a technology domain. They are intended to complement the activities of
      a collection of Eclipse Foundation open source projects. Open source projects are excellent for
      many things, but they typically do not do a great job with activities such as marketing, branding,
      specification and compliance processes, and the like.
    </p>

    <h2 class="h3">How do I join the working group?</h2>
    <p>Joining is straightforward.  All members may subscribe to the
    <a href="https://accounts.eclipse.org/mailing-list/jakarta.ee-wg">Jakarta.EE-wg@eclipse.org</a>
    working group mailing list.  To join the working group, your company representative
    can simply send an email to this list declaring your commitment to participate.
    There are no fees in 2018 to participate. </p>
    <p>Committer members are also welcome to join.  More information regarding committer
    membership, see the <a href="https://www.eclipse.org/membership/become_a_member/committer.php">Eclipse Committer Membership page.</a></p>

    <h2 class="h3">Who are the current members of this working group?</h2>
    <p>The current members are listed <a href="https://jakarta.ee/membership/members/">here</a>.</p>

    <h2 class="h3">What is the role of the PMC versus the working group or the working group Steering
      Committee?
    </h2>
    <p>Eclipse Foundation projects are self-governing meritocracies that set their own technical
      agendas and plans. The Project Management Committee for an Eclipse top-level project
      oversees the day-to-day activities of its projects through activities such as reviewing and
      approving plans, accepting new projects, approving releases, managing committer elections,
      and the like.
    </p>
    <p>Working groups and their steering committees are intended to complement the work happening
      in the open source projects with activities that lead to greater adoption, market presence, and
      momentum. Specifically the role of the working group is to foster the creation and growth of the
      ecosystem that surrounds the projects.
    </p>
    <p>Working groups do not direct the activities of the projects or their PMC. They are intended to be
      peer organizations that work in close collaboration with one another.
    </p>
    <h2 class="h3">Who defines and manages technical direction?</h2>
    <p>The projects manage their technical direction. The PMC may elect to coordinate the activities of
      multiple projects to facilitate the release of software platforms, for example.
    </p>
    <p>Because the creation of roadmaps and long term release plans can require market analysis,
      requirements gathering, and resource commitments from member companies, the working
      group may sponsor complementary activities to generate these plans. However, ultimately it is
      up to the projects to agree to implement these plans or roadmaps. The best way for a working
      group to influence the direction of the open source projects is to ensure that they have adequate
      resources. This can take the form of developer contributions, or under the <a href="/org/workinggroups/mfi_program.php">Member Funded
      Initiatives programs</a>, working groups can pool funds to contract developers to implement the
      features they desire.
    </p>
    <h2 class="h3">Why are there so many levels of membership?</h2>
    <p>Because the Java EE ecosystem is a big place, and we want to ensure that there are roles for
      all of the players in it. We see the roles of the various member classes to roughly align as
      follows:
    </p>
    <ul>
      <li>Strategic members are the vendors that deliver Java EE implementations. As such they
        are typically putting in the largest number of contributors, and are leading many of the
        projects.
      </li>
      <li>Enterprise members are the large enterprises that rely upon Java EE today for their
        mission critical application infrastructure, and who are looking to Jakarta EE to deliver the
        next generation of cloud native Java. They have made strategic investments in this
        technology, have a massive skills investment in their developers, and want to protect
        these investments as well as influence the future of this technology.
      </li>
      <li>Participant members are the companies that offer complementary products and services
        within the Java EE ecosystem. Examples include ISVs which build products on Java EE,
        or system integrators that use these technologies in delivering solutions to their
        customers.
      </li>
      <li>Committer members are comprised of the committers working on the various EE4J
        projects who are also members of the Eclipse Foundation. While the <a href="/org/documents/">Eclipse bylaws</a>
        define the criteria for committers to be considered members, in essence any committer
        members are either a) a committer who is an employee of an Jakarta EE member company
        or b) any other committer who has explicitly chosen to join as a member. Giving
        Committer members a role in the working group governance process mimics the
        governance structure of the Eclipse Foundation itself, where giving committers an
        explicit voice has been invaluable.
      </li>
    </ul>
    <h2 class="h3">What makes this different from the Java Community Process (JCP)?</h2>
    <p>The Jakarta EE working group will be the successor organization to the JCP for the family of
      technologies formerly known as Java EE. It has several features that make it a worthy
      successor to the JCP:
    </p>
    <ol>
      <li>It is vendor neutral. The JCP was owned and operated first by Sun and later by Oracle.
        Jakarta EE is designed to be inclusive and diverse, with no organization having any special
        roles or rights.
      </li>
      <li>It has open intellectual property flows. At the JCP, all IP flowed to the Spec Lead, which
        was typically Oracle. We are still working out the exact details, but the IP rights with
        Jakarta EE and EE4J will certainly not be controlled by any for-profit entity.
      </li>
      <li>It is more agile. This is an opportunity to define a 21st century workflow for creating
        rapidly evolving Java-based technologies. We will be merging the best practices from
        open source with what we have learned from over 15 years of JCP experience.
      </li>
    </ol>
    <h2 class="h3">Is the WG steering committee roughly equivalent to the JCP Executive Committee?</h2>
    <p>No, not really. The JCP EC always had two mixed roles: as a technical body overseeing the
      specification process, and as an ecosystem governance body promoting Java ME, SE, and EE.
      In Jakarta EE the Steering Committee will be the overall ecosystem governance body. The Jakarta EE
      Specification Committee will focus solely on the development and smooth operation of the
      technical specification process.
    </p>
    <h2 class="h3">Does a project have to be approved as a spec before it can start?</h2>
    <p>That is actually a decision which will be made by the EE4J PMC, not the working group.
      However, it is a goal of the people and organizations working on creating this working group that
      the Java EE community move to more of a code-first culture. We anticipate and hope that the
      EE4J PMC will embrace the incubation of technologies under its banner. Once a technology has
      been successfully implemented and adopted by at least some in the industry, it can then
      propose that a specification be created for it.
    </p>
    <h2 class="h3">In addition to the Steering Committee, what other committees exist?</h2>
    <p>There are four committees comprising the Jakarta EE governance structure - the Steering
      Committee, the Specification Committee, the Marketing and Brand Committee, and the
      Enterprise Requirements Committee. A summary of the make-up of each of the committees is
      in the table below.
    </p>
    <table class="table">
      <thead>
        <tr>
          <th></th>
          <th>Strategic Member</th>
          <th>Enterprise Member</th>
          <th>Participant Member</th>
          <th>Committer Member</th>
          <th>Guest Member</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Member of the Steering Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>N/A</td>
        </tr>
        <tr>
          <td>Member of the Specification Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Invitation Only</td>
        </tr>
        <tr>
          <td>Member of the Marketing Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>Invitation Only</td>
        </tr>
        <tr>
          <td>Member of the Enterprise Requirements Committee</td>
          <td>Appointed</td>
          <td>Appointed</td>
          <td>N/A</td>
          <td>N/A</td>
          <td>N/A</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
