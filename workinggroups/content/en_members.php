<?php

/**
 * Copyright (c) 2021 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available
 * at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

$working_groups = array(
  'eclipse-ide' => 'Eclipse® IDE Working Group',
  'microprofile' => 'MicroProfile® Working Group',
  'osgi' => 'OSGi Working Group',
  'adoptium' => 'Adoptium Working Group',
);

if (empty($_GET['wg']) || empty($working_groups[$_GET['wg']])) {
  header("HTTP/1.1 404 Not Found");
  exit();
} else { ?>
  <h1><? print $App->checkPlain($working_groups[$_GET['wg']]); ?> Members</h1>
  <div class="eclipsefdn-members margin-bottom-20 margin-top-20">
    <div class="row">
      <div class="eclipsefdn-members-list" data-ml-wg="<?php print $App->checkPlain($_GET['wg']); ?>">
        <div class="text-center">
          <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom-20"></i>
          <span class="sr-only">Loading...</span>
        </div>
      </div>
    </div>
  </div>
<?php } ?>