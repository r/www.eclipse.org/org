<?php

/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <div class="alert alert-danger">
      <strong><?php print $pageTitle;?> has been archived.</strong>
    </div>
    <p>Draft - Not Yet Approved</p>
    <p>Version 0.9 - Revision History at end of document</p>
    <h2>Vision and Scope</h2>
    <p>
      The Eclipse AI, Cloud & Edge (AICE) Working Group, hereafter referred to as AICE WG, manages and
      operates an open lab (the “AICE OpenLab”) that provides a set of resources to promote the
      advancement, implementation, and verification of open source software for AI, Cloud, and Edge
      computing. The AICE WG and the associated AICE OpenLab does this by:
    </p>
    <ul>
      <li>Fostering open and neutral collaboration amongst members for the adoption of open source
        technologies</li>
      <li>Defining, publishing and promoting reference architectures, blueprints and distributions of open
        source software that have been verified for industry AI, Cloud, and Edge standards,
        requirements, and use cases</li>
      <li>Developing and providing open source verification test suites, test tools, calibrated datasets and
        hosted test infrastructure for industry AI, Cloud, and Edge standards, requirements and use cases</li>
      <li>Ensuring that key requirements regarding privacy, security and ethics are integrated into all the
        OpenLab activities</li>
      <li>Partnering with industry organizations to assemble and verify open source software for their
        standards, requirements, and use cases</li>
      <li>Promoting the AICE OpenLab in the marketplace and engaging with the larger open source
        community</li>
      <li>Managing and funding the lab infrastructure resources to support this work</li>
    </ul>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>
      The following governance documents are applicable to this charter, each of which can be found on the
      <a href="/org/documents/">Eclipse Foundation Governance Documents</a> page or the
      <a href="/legal/">Eclipse Foundation Legal Resources</a> page:
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership Agreement, including the
      requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of
      the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this working group’s charter and the Eclipse
      Foundation Bylaws, Membership Agreement, Development Process, Specification Process, Working
      Group Process or any policies of the Eclipse Foundation, the terms of the respective Eclipse Foundation
      Bylaws, Membership Agreement, process or policy shall take precedence.
    </p>
    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a
      <a href="/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation, have executed the
      AICE Working Group participation agreement once defined and adhere to the requirements set forth in this
      Charter to participate.
    </p>
    <p>
      The participation fees associated with each of these membership classes are shown in the Annual
      Participation Fees section. These are annual fees, and are established by the AICE Steering
      Committee, and will be updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any Working Group
      membership fees, and are decided as described in the Eclipse Foundation Bylaws and detailed in the
      Eclipse Foundation Membership Agreement.
    </p>
    <p>There are three (3) classes of AICE Working Group membership - Strategic, Participant and Guest.</p>
    <h2>Classes of Membership</h2>
    <h3>Strategic Members</h3>
    <p>Strategic Members are organizations that view the AICE OpenLab as strategic to their organization and
      are investing significant resources to sustain and shape the activities of this working group. Strategic
      Members of this working group must be at least a Contributing Member of the Eclipse Foundation and
      commit to strategic membership for a three (3) year period.
    </p>
    <p>
      Strategic Members want to influence the definition and further development of the relevant best
      practices being adopted and promoted by AICE OpenLab blueprints and reference architectures, as well
      as the management of the AICE OpenLab resources. They are members of the Steering Committee and
      invest an essential amount of resources to sustain and shape the Working Group activities.
    </p>
    <h3>Participant Members</h3>
    <p>
      Participant Members consider interoperability and best practice adoption related to AI, Cloud & Edge a
      relevant addition to their business. Participant Members are invested in the relevant best practices
      being adopted and promoted by AICE OpenLab projects as well as requiring access to the resources of
      the AICE OpenLab.
    </p>
    <p>
      Participant Members want to participate in the development and direction of an open ecosystem related
      to this working group. Participant Members of this working group must be at least a Contributing
      Member of the Eclipse Foundation.
    </p>
    <h3>Guest Members</h3>
    <p>
      Guest Members are organizations which are Associate members of the Eclipse Foundation. Typical guests
      include Associations for various industry verticals, Open Source Software Communities, SDOs, R&D
      partners, universities, academic research centers, etc.
    </p>
    <p>
      Guest members cannot use the AICE OpenLab resources allocated to Special Interest Groups (as defined below) to fulfill
      their own requirements and use cases. However they may support Strategic and Participant members in
      the application of their own open specifications, open source projects or specific domain artifacts within
      the domain of the Special Interest Groups.
    </p>
    <p>
      Guests may be invited to participate in committee or Special Interest Group meetings
      at the invitation of the respective committee or SIG, but under no circumstances do Guest members
      have voting rights. Guest members are required to execute the working group’s Participation Agreement.
    </p>
    <p>Guest membership is free of charge.</p>
    <h2>Sponsorship of the AICE Working Group</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to the working group on an ad
      hoc basis to support the activities of the AICE Working Group and its managed projects.
      Money or services provided by sponsors are used as set forth in the working group annual budget. The
      working group is free to determine whether and how those contributions are recognized. Under no
      condition are sponsorship monies refunded.
    </p>
    <p>
      Sponsors need not be members of the Eclipse Foundation or of the AICE Working Group.
    </p>
    <h2>Governance</h2>
    <p>This AICE Working Group is designed as:</p>
    <ul>
      <li> a vendor-neutral, member-driven organization</li>
      <li> a means to foster a vibrant and sustainable ecosystem of SIGs</li>
      <li> a means to manage collaboration on an infrastructure for experiments and tests</li>
    </ul>
    <p>
      In order to implement these principles, the following governance bodies have been defined (each a
      "Body"):
    </p>
    <ul>
      <li> The Steering Committee</li>
      <li> The OpenLab Operations and Architecture Committee</li>
      <li> The Privacy, Security and Ethics Committee</li>
    </ul>
    <h2>Governing Bodies</h2>
    <p>The Steering Committee oversees the overall governance of the AICE OpenLab.</p>
    <h2>Steering Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group</li>
      <li>Review and approve requests for the creation, modification or termination of SIGs</li>
      <li>Manage AICE OpenLab resource allocations to SIGs and resolve cross SIG AICE OpenLab resource
        allocation disputes referred to it by the AICE Open Lab Operations and Architecture Committee
      </li>
      <li>Mediate cases that have been brought to it from the Privacy, Security and Ethics Committee with
        the relevant SIGs
      </li>
      <li>Manage and approve sponsorship contributions applications</li>
      <li>Review and approve SIG processes, policies, open source projects and tools used by the SIGs
        within the AICE OpenLab and which may be included in the released AICE OpenLab blueprints
        and reference architectures
      </li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this
        working group
      </li>
      <li>Establish an annual program plan</li>
      <li>Review and approve this charter</li>
      <li>Define the annual fees for all classes of the working group members</li>
      <li>Approve the annual budget based upon funds received through fees and sponsorship</li>
    </ul>
    <h3>Composition</h3>
    <p>Each Strategic Member of the working group has a seat on the Steering Committee.</p>
    <p>One seat is allocated to Participant Members. Participant Member seats are allocated following the
      Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.
    </p>
    <p>
      The Committee elects a chair of the Steering Committee. This chair is elected among the members of the
      Committee. They will serve for a 12 month period or until their successor is elected and qualified, or as
      otherwise provided for in this Charter. There is no limit on the number of terms the chair may serve.
    </p>
    <h3>Meeting Management</h3>
    <p>The Steering Committee meets at least quarterly.</p>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all meetings related to the working
      group will follow a prepared agenda and minutes are distributed two weeks after the meeting and
      approved at the next meeting at the latest, and shall in general conform to the Eclipse Foundation
      AntiTrust Policy.
    </p>
    <h2>OpenLab Operations and Architecture Committee</h2>
    <p>
      The OpenLab Operations and Architecture Committee oversees the technical design and operations of
      the AICE OpenLab resources.
    </p>
    <h3>Powers and Duties</h3>
    <p>OpenLab Operations and Architecture Committee members are required to:</p>
    <ul>
      <li>Define and manage a policy to govern Allocation and Consumption of OpenLab resources by
        Special Interest Groups
      </li>
      <li>Give priority to applications driven by Members</li>
      <li>Ensure that the design and build of the lab meets the needs of the OpenLab members and SIGs</li>
      <li>Establish targets for uptime and security of the resources within the AICE OpenLab</li>
      <li>Oversee the management the AICE OpenLab Administration tasks</li>
      <li>Technical Steering of the AICE OpenLab in terms of best practices applied to lab assets</li>
    </ul>
    <h3>Composition</h3>
    <p>Members of the OpenLab Operations and Architecture Committee are generally senior technical experts
      with the skills to design, build, and operate the lab.
    </p>
    <p>
      Each Strategic Member of the working group has a seat on the OpenLab Operations and Architecture
      Committee.
    </p>
    <p>
      Three seats are allocated to Participant Members. Participant Member seats are allocated following the
      Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws.
    </p>
    <p>
      Guest members may be invited by the Steering Committee as observers. Guest members have no voting
      rights.
    </p>
    <p>
      Other individuals may be invited by the Steering Committee to participate in the OpenLab Operations
      and Architecture Committee with no voting rights.
    </p>
    <p>Any additional individuals as designated from time to time by the Executive Director.</p>
    <p>
      The Committee elects a chair who reports to the Steering Committee. This chair is elected among the
      members of the Committee. They will serve for a 12 month period or until their successor is elected and
      qualified, or as otherwise provided for in this Charter. There is no limit on the number of terms the chair
      may serve.
    </p>
    <h3>Meeting Management</h3>
    <p>The OpenLab Operations and Architecture Committee meets at least quarterly.</p>
    <p>All meetings will follow a prepared agenda and minutes are distributed two weeks after the meeting and
      approved at the next meeting at the latest.
    </p>
    <h2>Privacy, Security and Ethics Committee</h2>
    <p>
      The OpenLab Privacy, Security and Ethics Committee oversees the privacy, security and ethical issues of
      the AICE OpenLab data usage and artefacts production.
    </p>
    <h3>Powers and Duties</h3>
    <p>Privacy, Security and Ethics Committee members are required to:</p>
    <ul>
      <li>
        Review the processes, open source projects and tools that the AICE Working Group SIGs should
        use, leverage and provide guidance to SIGs to enhance the privacy, security and ethics applied to
        all data that resides or passes through the AICE OpenLab.
      </li>
      <li>
        Provide guidance for SIGs to implement increased security where necessary and in the case of
        disputes refer to the Steering Committee for mediation.
      </li>
      <li>
        Provide guidance to better ensure all blueprints and reference architectures produced by SIGs
        have high standards for privacy, security and ethics.
      </li>
    </ul>
    <h3>Composition</h3>
    <p>
      Members of the OpenLab Privacy, Security and Ethics Committee are generally senior experts with the
      skills and experience to appropriately and effectively meet the Committee’s purpose.
    </p>
    <p>Each Strategic Member of the working group has a seat on the Privacy, Security and Ethics Committee.</p>
    <p>
      One seat is allocated to Participant Members. Participant Member seats are allocated following the
      Eclipse "Single Transferable Vote", as defined in the Eclipse Bylaws.
    </p>
    <p>
      Guest members that have been invited by the Steering Committee as observers. Guest members have no
      voting rights.
    </p>
    <p>
      Individuals can be invited by the Steering Committee to participate in the OpenLab Operations and
      Architecture Committee with no voting rights.
    </p>
    <p>Any additional individuals as designated from time to time by the Executive Director.</p>
    <p>
      The Committee elects a chair who reports to the Steering Committee. This chair is elected among the
      members of the Committee. They will serve for a 12 month period or until their successor is elected and
      qualified, or as otherwise provided for in this Charter. There is no limit on the number of terms the chair
      may serve.
    </p>
    <h3>Meeting Management</h3>
    <p>The Privacy, Security and Ethics Committee meets at least quarterly.</p>
    <p>
      All meetings will follow a prepared agenda and minutes are distributed two weeks after the meeting and
      approved at the next meeting at the latest.
    </p>
    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a community formed within the AICE Working Group with a focus to
      produce reference architectures, blueprints and distributions of open source software for a specific
      vertical. SIGs may be formed by Member organizations requesting allocation of AICE OpenLab resources
      to fulfil a specific purpose on the approval of the AICE Working Group Steering Committee. Only SIGs can
      utilize the AICE OpenLab infrastructure. SIGs provide the meeting point for Research, OSS Communities,
      Industry Associations and Standards Development Organizations (SDO), to utilize AICE OpenLab
      resources to foster improved technology adoption, compatibility and interoperability. SIGs own the
      lifecycle of the blueprints, reference architectures and use cases they will test, from definition to
      publicizing results.
    </p>
    <h2>Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this working group, unless otherwise specified.
      For all matters related to membership action, including without limitation: meetings, quorum, voting,
      electronic voting action without meeting, vacancy, resignation or removal, the respective terms set forth
      in the Eclipse Foundation Bylaws apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a Body
      member is unavailable to attend or participate in a meeting of the Body, they may be represented by
      another Body member by providing a written proxy to the Body’s mailing list in advance. As per the
      Eclipse Foundation Bylaws, a representative shall be immediately removed from the Body upon the
      termination of the membership of such representative’s Member organization.
    </p>
    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body are
      determined by a simple majority of the representatives represented at a committee meeting at which a
      quorum is present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors approve a specification license;
      (ii) approving specifications for adoption; (iii) approving or changing the name of the working group; and
      (iv) approving changes to annual Member contribution requirements; any such actions must be
      approved by no less than two-thirds (2/3) of the representatives represented at a committee meeting at
      which a quorum is present.
    </p>
    <h3>Term and Dates of Elections</h3>
    <p>
      This section only applies to the Steering Committee, OpenLab Operations and Architecture Committee,
      and the Privacy, Security and Ethics Committee.
    </p>
    <p>
      All representatives shall hold office until their respective successors are appointed or elected, as
      applicable. There shall be no prohibition on re-election or re-designation of any representative following
      the completion of that representative's term of office.
    </p>
    <h4>Strategic Members</h4>
    <p>
      Strategic Members Representatives shall serve in such capacity on committees until the earlier of their
      removal by their respective appointing Member organization or as otherwise provided for in this Charter.
    </p>
    <h4>Elected representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve for a 12 month
      period, or until their respective successors are elected and qualified, or as otherwise provided for in this
      Charter. Procedures governing elections of Representatives may be established pursuant to resolutions
      of the Steering Committee provided that such resolutions are not inconsistent with any provision of this
      Charter.
    </p>
    <h2>Membership and Committees Summary</h2>
    <table class="table table-stripped">
      <thead>
        <tr>
          <th>Committee Representation</th>
          <th class="text-center">
            Strategic Member
          </th>
          <th class="text-center">
            Participant Member
          </th>
          <th class="text-center">
            Guest Member
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            Member of the Steering Committee
          </td>
          <td class="text-center">
            Appointed
          </td>
          <td class="text-center">
            Elected
          </td>
          <td class="text-center">
            N/A
          </td>
        </tr>
        <tr>
          <td>
            Member of the Openlab
            Operations and
            Architecture Committee
          </td>
          <td class="text-center">
            Appointed
          </td>
          <td class="text-center">
            Elected
          </td>
          <td class="text-center">
            N/A
          </td>
        </tr>
        <tr>
          <td>
            Member of the Privacy,
            Security and Ethics
            Committee
          </td>
          <td class="text-center">
            Appointed
          </td>
          <td class="text-center">
            Elected
          </td>
          <td class="text-center">
            N/A
          </td>
        </tr>
      </tbody>
    </table>
    <h2>Meetings Management</h2>
    <h3>Meeting Frequency</h3>
    <p>
      Each governing body meets at least twice a year. All meetings may be held at any place that has been
      designated from time-to-time by resolution of the corresponding Body. All meetings may be held
      remotely using phone calls, video calls, or any other means as designated from time-to-time by
      resolution of the corresponding Body.
    </p>
    <h3>Place of Meetings</h3>
    <p>
      All meetings may be held at any place that has been designated from time-to-time by resolution of the
      corresponding body. All meetings may be held remotely using phone calls, video calls, or any other
      means as designated from time-to-time by resolution of the corresponding body.
    </p>
    <h3>Regular Meetings</h3>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of the same has been
      provided to each of the representatives at least fifteen (15) calendar days prior to such meeting, which
      notice will identify all potential actions to be undertaken by the Body at the Body meeting. No
      representative will be intentionally excluded from Body meetings and all representatives shall receive
      notice of the meeting as specified above; however, Body meetings need not be delayed or rescheduled
      merely because one or more of the representatives cannot attend or participate so long as at least a
      quorum of the Body is represented at the Body meeting.
    </p>
    <h3>Actions</h3>
    <p>
      The body may undertake an action only if it was identified in a body meeting notice or otherwise
      identified in a notice of special meeting.
    </p>
    <h3>Decisions</h3>
    <p>
      Decisions shall be taken by simple majority vote, unless specified otherwise. The body has a quorum if all
      representatives have properly been invited. Decisions shall be reported in writing. Guests do not have
      any voting rights.
    </p>
    <h3>Invitations</h3>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right to vote.</p>
    <h2>Working Group Fees</h2>
    <p>
      In 2021 the fees will be €0. The initial Steering Committee will be tasked with defining a fee structure
      and budget starting with calendar year 2022, and following years based on resource requirements
      necessary to achieve the objectives of the group. Members agree to pay the annual fees as established
      by the Steering Committee.
    </p>
    <p>
      If fees are not established for a full calendar year, they will be effective one month following the
      resolution of established fees and will be prorated for the year. All members are expected to pay the
      prorated fees for the associated calendar year. Beginning with the next calendar year, fees will be
      assessed on an annual basis.
    </p>
    <p>
      Established fees cannot be charged retroactively and any fee structure change will be communicated to
      all members via the mailing list and will be charged the next time a member is invoiced.
    </p>
    <h3>Working Group Annual Participation Fees Schedule A</h3>
    <p>
      The following fees have been established by the AICE Steering Committee. These fees are in
      addition to each participant’s membership fees in the Eclipse Foundation.
    </p>
    <h3>AICE Working Group Strategic Member Annual Participation Fees</h3>
    <p>
      Strategic members are required to execute the AICE Working Group Participation Agreement and to
      begin paying fees effective January 1, 2022.
    </p>
    <p>Strategic members are required to commit to three (3) years of membership.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="75%">Corporate Revenue</th>
          <th class="text-center" width="25%">Strategic</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €250 million</td>
          <td class="text-center">€60 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €100 million but less than or
            equal to €250 million
          </td>
          <td class="text-center">€35 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €50 million but less than or
            equal to €100 million
          </td>
          <td class="text-center">€20 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €10 million but less than or
            equal to €50 million
          </td>
          <td class="text-center">€10 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td class="text-center">€5 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than €1 million with less than 10 employees</td>
          <td class="text-center">€5 000</td>
        </tr>
        <tr>
          <td>Govt, Govt agencies, Research Organizations, NGOs, etc.</td>
          <td class="text-center">€5 000</td>
        </tr>
        <tr>
          <td>Academic, Publishing Organizations, User Groups, etc.</td>
          <td class="text-center">€5 000</td>
        </tr>
      </tbody>
    </table>
    <h3>AICE Working Group Participant Member Annual Participation Fees</h3>
    <p>
      Participant members are required to execute the AICE Working Group Participation Agreement and to
      begin paying fees effective January 1, 2022.
    </p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="75%">Corporate Revenue</th>
          <th class="text-center" width="25%">Strategic</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €250 million</td>
          <td class="text-center">€20 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €100 million but less than or
            equal to €250 million
          </td>
          <td class="text-center">€15 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €50 million but less than or
            equal to €100 million
          </td>
          <td class="text-center">€10 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €10 million but less than or
            equal to €50 million
          </td>
          <td class="text-center">€5 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td class="text-center">€1 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than €1 million with less than 10 employees</td>
          <td class="text-center">€1 000</td>
        </tr>
        <tr>
          <td>Govt, Govt agencies, Research Organizations, NGOs, etc.</td>
          <td class="text-center">€1 000</td>
        </tr>
        <tr>
          <td>Academic, Publishing Organizations, User Groups, etc.</td>
          <td class="text-center">€1 000</td>
        </tr>
      </tbody>
    </table>
    <h3>AICE Working Group Guest Member Annual Participation Fees</h3>
    <p>Guest members pay no annual fees, but are required to execute the AICE Working Group Participation Agreement.</p>

    <p class="margin-top-20"><strong>Charter History</strong></p>
    <ul>
      <li>V0.9 Eclipse Foundation Review</li>
      <li>V0.8 Adjustments</li>
      <li>V0.7 Clarification of rights and requirements (Draft)</li>
      <li>V0.6 Rewording, add the notion of SIG, draft created April 21, 2021</li>
      <li>V0.5 Initial draft created March 23, 2021</li>
    </ul>
  </div>
</div>
