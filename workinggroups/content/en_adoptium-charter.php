<?php
/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 1.1 - Revision History at end of document</p>
    <h2>Vision and Scope</h2>
    <p>
      The Adoptium working group’s scope is to advance, promote, and support
      the dissemination of technologies derived from or related to the Eclipse
      Adoptium Top-Level Project. The Eclipse Adoptium project produces
      high-quality runtimes and associated technology for use across the Java
      ecosystem. The working group’s vision is to meet the needs of both the
      Eclipse community and broader runtime users by facilitating a
      comprehensive set of technologies around runtimes for Java and Java
      Virtual Machine-based applications that operate alongside existing
      standards, infrastructures, and cloud platforms.
    </p>
    <p>
      The working group will:
    </p>
    <ul>
      <li>
        Establish Adoptium as the industry's trusted source for high-quality
        enterprise-ready Java runtimes.
      </li>
      <li>
        Promote the "Adoptium" brand and its value in the marketplace.
      </li>
      <li>
        Provide vendor-neutral marketing and other services to the Adoptium
        ecosystem.
      </li>
      <li>
        Define quality criteria and a quality branding process for
        enterprise-grade runtime technology.
      </li>
      <li>
        Leverage licensing and intellectual property flows that encourage
        community participation, protect community members, and encourage broad
        usage.
      </li>
      <li>
        Manage the overall technology roadmap and business strategies for
        Eclipse Adoptium and related projects.
      </li>
      <li>
        Establish and drive a funding model that enables this working group and
        its community to operate on a sustainable basis.
      </li>
    </ul>

    <h2>Governance and Precedence</h2>
    <p>
      The following governance documents are applicable to this charter, each
      of which can be found on the Eclipse Foundation Governance Documents page
      or the Eclipse Foundation Legal Resources page.
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li> 
    </ul>
    <p>
      All Members of the working group must be parties to the Eclipse
      Foundation Membership Agreement, including the requirement set forth in
      Section 2.2 to adhere to and abide by the Bylaws and then-current
      policies of the Eclipse Foundation, including but not limited to the
      Intellectual Property and Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this working
      group's charter and the Eclipse Foundation Bylaws, Membership Agreement,
      Development Process, Working Group Process, or any policies of the
      Eclipse Foundation, the terms of the Eclipse Foundation Bylaws,
      Membership Agreement, process, or policy shall take precedence.
    </p>

    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be
      at least a <a href="/membership/#tab-levels">Contributing Member</a> 
      of the Eclipse Foundation, have executed the Adoptium Working Group
      Participation Agreement and adhere to the requirements set forth in this
      Charter to participate.
    </p>
    <p>
      There are five classes of Adoptium working group membership - Strategic,
      Enterprise, Participant, Committer, and Guest. Each of these classes is
      described in detail below.
    </p>
    <p>
      The participation fees associated with each of these membership classes
      are shown in the tables in Schedule A. These are annual fees, and are
      established by the Adoptium working group Steering Committee, and will be
      updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are
      separate from any working group membership fees and are decided as
      described in the <a href="/org/documents/eclipse-foundation-be-bylaws-en.pdf">Eclipse Bylaws</a> 
      and detailed in the 
      <a href="/org/documents/eclipse-foundation-membership-agreement.pdf">Eclipse Membership Agreement</a>.
    </p>

    <h2>Classes of Adoptium Working Group Membership</h2>
    <h3>Strategic Members</h3>
    <p>
      Strategic Members are organisations that view Adoptium working group
      managed technology as critical to their organisation’s future, and are
      investing significant resources to sustain and define the core activities
      that are the responsibility of the working group.
    </p>
    <p>
      Strategic Members may participate in all areas of the working group and
      its committees. Such committees and subcommittees may include the quality
      assurance subcommittee, infrastructure subcommittee, branding and
      marketing subcommittee, and others as deemed necessary by the working
      group Steering Committee.
    </p>
    <p>
      Strategic Members of the Adoptium working group must be at least a
      Contributing Member of the Eclipse Foundation and are encouraged to have
      contributors participating in Adoptium projects and other Adoptium
      initiatives.
    </p>

    <h3>Enterprise Members</h3>
    <p>
      Enterprise Members are typically organisations that view the Adoptium
      working group managed technology as a critical part of their
      organisation's business operations. These organisations want to influence
      the direction and support the development of a runtime technology
      ecosystem through Eclipse Adoptium.
    </p>
    <p>
      Enterprise Members are eligible to, though not required to, participate
      in all areas of the working group and its committees. Enterprise Members
      may be elected to the Steering Committee and may participate in
      subcommittees at such an Enterprise member’s request.
    </p>
    <p>
      Enterprise Members of the Adoptium working group must be at least
      Contributing Members of the Eclipse Foundation.
    </p>

    <h3>Participant Members</h3>
    <p>
      Participant Members are typically organisations that deliver products or
      services based on Adoptium technology. These organisations want to
      participate in the evolution of the Eclipse Adoptium ecosystem to ensure
      it continues to meet their needs.
    </p>
    <p>
      Participant Members may be elected to the Steering Committee and are
      eligible to participate in all subcommittees at the invitation of the
      respective subcommittee.
    </p>
    <p>
      Participant Members of the Adoptium working group must be at least
      Contributing Members of the Eclipse Foundation.
    </p>

    <h3>Committer Members</h3>
    <p>
      Committer Members are individuals who through a process of meritocracy defined by the Eclipse
      Development Process are able to contribute and commit code to the Eclipse Foundation projects included in
      the scope of this working group. Committers may be members by virtue of working for a member organisation
      or may choose to complete the membership process independently if they are not. For further explanation
      and details, see the <a href="/membership/become-a-member/committer/">Eclipse Foundation Committer Membership</a> 
      page.
    </p>
    <p>
      Committer Members may be elected to the Steering Committee and are
      eligible to participate in all subcommittees at the invitation of the
      respective subcommittee.
    </p>
    
    <h3>Guest Members</h3>
    <p>
      Guest Members are organisations that are Associate members of the Eclipse
      Foundation who have been invited for one year, renewable, by the Steering
      Committee to participate in particular aspects of the activities of the
      working group. Typical guests include Java User Groups, Research and
      Development partners, universities, academic research centres, etc.
    </p>
    <p>
      Guests may be invited to participate in committee meetings at the
      invitation of the respective committee, but under no circumstances do
      Guest members have voting rights. Guest members are required to execute
      the Adoptium Working Group Participation Agreement.
    </p>
    
    <h2>Membership Summary</h2>
    <table class="table table-bordered text-center">
      <thead>
        <tr>
          <th></th>
          <th>
            Strategic Member
          </th>
          <th>
            Enterprise Member
          </th>
          <th>
            Participant Member
          </th>
          <th>
           Committer Member
          </th>
          <th>
           Guest Member
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="text-left" scope="row">
            Eclipse Foundation Membership
          </th>
          <td>
            At least a Contributing Member organisation
          </td>
          <td>
            At least a Contributing Member organisation
          </td>
          <td>
            At least a Contributing Member organisation
          </td>
          <td>
            At least Individual Committer Member
          </td>
          <td>
            Associate Member organisation
          </td>
        </tr>
        <tr>
          <th class="text-left" scope="row">
          Membership Commitment
          </th>
          <td>
            1 year, renewable
          </td>
          <td>
          1 year, renewable
          </td>
          <td>
          1 year, renewable
          </td>
          <td>
          1 year, renewable
          </td>
          <td aria-label="Not Applicable">
            N/A
          </td>
        </tr>
        <tr>
          <th class="text-left" scope="row">
          Member of the Steering Committee
          </th>
          <td>
          Appointed by the member organisation
          </td>
          <td>
          Elected
          </td>
          <td>
          Elected
          </td>
          <td>
          Elected
          </td>
          <td>
          N/A
          </td>
        </tr>
        <tr>
          <th class="text-left" scope="row">
          Member of subcommittees
          </th>
          <td>
          Upon request of member
          </td>
          <td>
          Upon request of member
          </td>
          <td>
          By invite of subcommittee
          </td>
          <td>
          By invite of subcommittee
          </td>
          <td>
          N/A
          </td>
        </tr>
        <tr>
          <th class="text-left" scope="row">
          Description on Website
          </th>
          <td>
          ✓
          </td>
          <td>
          </td>
          <td>
          </td>
          <td>
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <th class="text-left" scope="row">
            Logo on Website
          </th>
          <td>
            ✓
          </td>
          <td aria-label="Yes">
            ✓
          </td>
          <td aria-label="Yes">
            ✓
          </td>
          <td aria-label="Yes">
          </td>
          <td>
            ✓
          </td>
        </tr>
      </tbody>
    </table>
    
    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within
      the working group with a focus to collaborate around a particular topic
      or domain of direct interest to the working group. SIGs are designed to
      drive the objectives of a subset of the Members of the working group in
      helping them achieve a dedicated set of goals and objectives. The scope
      of the SIG must be consistent with the scope of the Working Group
      Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each
      SIG may be either temporary or a permanent structure within the working
      group. SIGs can be disbanded at any time by self selection presenting
      reasons to and seeking approval from the Steering Committee. Steering
      Committees may disband a SIG at any time for being inactive or
      non-compliant with the working group's charter, or by request of the SIG
      itself. SIGs operate as a non-governing Body of the working group. There
      are no additional annual fees to Members for participation in a SIG.
    </p>

    <h2>Sponsorship of the Adoptium Working Group</h2>
    <h3>Sponsors</h3>
    <p>
      Sponsors are companies or individuals who provide money or services to
      the working group on an ad hoc basis to support the activities of the
      Adoptium working group and its managed projects.
    </p>
    <p>
      Money or services provided by sponsors are used as set forth in the
      working group annual budget. The working group is free to determine
      whether and how those contributions are recognized. Under no condition
      are sponsorship monies refunded.
    </p>
    <p>
      Sponsors need not be members of the Eclipse Foundation or of the Adoptium
      working group.
    </p>

    <h2>Governance</h2>
    <p>This Adoptium working group is designed as:</p>
    <ul>
      <li>
        a vendor-neutral, member-driven organisation,
      </li>
	    <li>
        a means to foster a vibrant and sustainable ecosystem of components and
        service providers,
      </li>
	    <li>
        a means to organise the community of each project or component so that
        users and developers define the roadmap collaboratively.
      </li>
    </ul>

    <h2>Steering Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Steering Committee members are entitled to:</p>
    <ul>
      <li>
        Define and manage the strategy of the working group.
      </li>
      <li>
        Define and manage which Eclipse Foundation projects are included within
        the scope of this working group.
      </li>
      <li>
        Define and manage a process for projects outside of the Eclipse
        Foundation to be included in the Eclipse Adoptium Project.
      </li>
      <li>
        Define and manage non-technical roadmaps.
      </li>
      <li>
        Review and approve changes to the working group charter.
      </li>
      <li>
        Review and approve the trademark policy and any subsequent changes.
      </li>
      <li>
        Ensure the consistency of logo usage and other marketing materials.
      </li>
      <li>
        Establish the annual program plan.
      </li>
      <li>
        Define the annual fees for all classes of the working group membership.
      </li>
      <li>
        Approve the annual budget based upon funds received through fees and
        sponsorship.
      </li>
      <li>
        Create subcommittees and define the purpose, scope, and membership of
        each such subcommittee. Examples of such subcommittees include quality
        assurance, infrastructure, and branding and marketing.
      </li>
      <li>
        Recognize the contributions of members, participants, and sponsors.
      </li>
      <li>
        Approve the creation and retirement of sub committees and Special
        Interest Groups (SIGs).
      </li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>No single Eclipse Foundation Member organisation may occupy more than 50% of the Steering Committee membership through any means including appointment or elections.</li>
	    <li>Each Strategic Member of the working group may occupy a seat on the Steering Committee as appointed by the Eclipse Foundation Member organisation holding such Strategic Membership.</li>
	    <li>The Steering Committee may determine from time to time the total number of seats to be occupied via election by Enterprise, Participant, and Committer Members.</li>
	    <li>All elected seats are allocated following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.</li>
	    <li>At least one seat is allocated to Committer Members via election. This seat remains empty if there are no candidates.</li>
	    <li>
        The Committee elects a chair of the Steering Committee. This chair is
        elected among the members of the Steering Committee. They will serve
        for a 12-month period or until their successor is elected and qualified
        or as otherwise provided for in this Charter. There are no restrictions
        on the number of terms a Chairperson may serve.
      </li>
    </ul>
    <h3>Meeting Management</h3>
    <ul>
      <li>The Steering Committee meets at least quarterly.</li>
      <li>All meetings will follow a prepared agenda and are minuted.</li>
    </ul>

    <h2>Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this working
      group, unless otherwise specified. For all matters related to membership
      action, including without limitation: meetings, quorum, voting,
      electronic voting action without a meeting, vacancy, resignation, or
      removal, the respective terms set forth in the Eclipse Foundation Bylaws
      apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member
      organisation they are representing at any time by providing written
      notice to the Steering Committee. In the event a Body member is
      unavailable to attend or participate in a meeting of the Body, they may
      be represented by another Body member by providing written proxy to the
      Body’s mailing list in advance. As per the Eclipse Foundation Bylaws, a
      representative shall be immediately removed from the Body upon the
      termination of the membership of such representative’s Member
      organisation.
    </p>

    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Accepting the actions specified below for which a Super Majority is
      required, votes of the Body are determined by a simple majority of the
      representatives represented at a committee meeting at which a quorum is
      present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions:
    </p>
    <ol style="list-style-type: lower-roman;">
      <li>
        requesting that the Eclipse Foundation Board of Directors approve a
        specification license,
      </li>
      <li>
        approving specifications for adoption, 
      </li>
      <li>
        modifying the working group charter;
      </li>
      <li>
        approving or changing the name of the working group; and 
      </li>
      <li>
        approving changes to annual Member contribution requirements;
      </li>
    </ol>
    <p>
      any such actions must be approved by no less than two-thirds (&frac23;)
      of the representatives in represented at a committee meeting at which a
      quorum is present.
    </p>

    <h3>Term and Dates of Elections</h3>
    <p>
    This section only applies to the Steering Committee.
    </p>
    <p>All Member representatives shall hold office until their respective successors are appointed or elected, as applicable. There shall be no prohibition on re-election or re-designation of any representative following the completion of that representative’s term of office.</p>

    <h4>Strategic Members</h4>
    <p>
      Strategic Members Representatives shall serve in such capacity on
      committees until the earlier of their removal by their respective
      appointing Member organisation or as otherwise provided for in this
      Charter.
    </p>
    <h4>Elected representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be
      elected to serve for a 12-month term, or until their respective
      successors are elected and qualified, or as otherwise provided for in
      this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this
      Charter.
    </p>

    <h3>Meetings Management</h3>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all
      meetings related to the Working Group will follow a prepared agenda and
      minutes are distributed two weeks after the meeting and approved at the
      next meeting at the latest, and shall in general conform to the Eclipse
      Foundation Antitrust Policy.
    </p>
    <h4>Meeting Frequency</h4>
    <p>
      Each governing Body meets at least twice a year. All meetings may be held
      at any place that has been designated from time to time by resolution of
      the corresponding Body. All meetings may be held remotely using phone
      calls, video calls, or any other means as designated from time to time by
      resolution of the corresponding Body.
    </p>
    <h4>Place of Meetings</h4>
    <p>
      All meetings may be held at any place that has been designated from
      time-to-time by resolution of the corresponding Body. All meetings may be
      held remotely using phone calls, video calls, or any other means as
      designated from time-to-time by resolution of the corresponding Body.
    </p>
    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice
      of same has been provided to each of the representatives at least fifteen
      (15) calendar days prior to such meeting, which notice will identify all
      potential actions to be undertaken by the Body at the Body meeting. No
      representative will be intentionally excluded from Body meetings and all
      representatives shall receive notice of the meeting; however, Body
      meetings need not be delayed or rescheduled merely because one or more of
      the representatives cannot attend or participate so long as at least a
      quorum of the Body is represented at the Body meeting.
    </p>
    <h4>Actions</h4>
    <p>
      The Body may undertake an action only if it was identified in a Body
      Meeting notice or otherwise identified in a notice of the special
      meeting.
    </p>
    <h4>Invitations</h4>
    <p>
      The Body may invite any member to any of its meetings. These invited
      attendees have no right to vote.
    </p>

    <h2>Adoptium Working Group Annual Participation Fees Schedule A</h2>
    <p>
      The following fees have been established by the Adoptium Steering
      Committee. These fees are in addition to each participant’s membership
      fees in the Eclipse Foundation.
    </p>

    <h3>Adoptium Strategic Member Annual Participation Fees</h3>
    <p>
      Strategic members are required to execute the Adoptium Participation
      Agreement.
    </p>
    <p>
      Strategic members of the working group are strongly encouraged and
      anticipated to support the working group’s project(s) in order for the
      working group to achieve its annual objectives as outlined in the working
      group’s program plan.  Such support could include full-time project
      contributors, financial sponsorships, in-kind sponsorship, marketing and
      communication support, among other initiatives.
    </p>

    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th>
            Corporate Revenue
          </th>
          <th>
            Annual Fees - USD
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            Annual Corporate Revenues greater than $1 billion
          </td>
          <td class="text-center">
            $50,000
          </td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than $500 million but less than
            or equal to $1 billion
          </td>
          <td class="text-center">
            $40,000
          </td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than $100 million but less than or equal to $500 million
          </td>
          <td class="text-center">
            $30,000
          </td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than $10 million but less than or
            equal to $100 million
          </td>
          <td class="text-center">
            $20,000
          </td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues less than or equal to $10 million
          </td>
          <td class="text-center">
            <p>$10,000</p>
          </td>
        </tr>
    </tbody>
    </table>

    <h3>Adoptium Enterprise Member Annual Participation Fees</h3>
    <p>Enterprise members are required to execute the Adoptium Participation Agreement.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="55%">Corporate Revenue</th>
          <th class="text-center" width="20%">Annual Fees - USD</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than $1 billion</td>
          <td class="text-center">$32,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $500 million but less than or equal to $1 billion</td>
          <td class="text-center">$24,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $100 million but less than or equal to $500 million</td>
          <td class="text-center">$18,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $10 million but less than or equal to $100 million</td>
          <td class="text-center">$12,000</td>
        </tr>
      </tbody>
    </table>

    <h3>Adoptium Participant Member Annual Participation Fees</h3>
    <p>Participant members are required to execute the Adoptium Participation Agreement.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="55%">Corporate Revenue</th>
          <th class="text-center" width="20%">Annual Fees - USD</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than $1 billion</td>
          <td class="text-center">$15,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $500 million but less than or equal to $1 billion</td>
          <td class="text-center">$12,500</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $100 million but less than or equal to $500 million</td>
          <td class="text-center">$10,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $10 million but less than or equal to $100 million</td>
          <td class="text-center">$7,500</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to $10 million</td>
          <td class="text-center">$5,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than $1 million and < 10 employees</td>
          <td class="text-center">$0</td>
        </tr>
        <tr>
          <td>Govt, Govt agencies, Research organisations, NGOs, etc.</td>
          <td class="text-center">$0</td>
        </tr>
        <tr>
          <td>Academic, Publishing organisations, User Groups, etc.</td>
          <td class="text-center">$0</td>
        </tr>
      </tbody>
    </table>

    <h3>Adoptium Committer Member Annual Participation Fees</h3>
    <p>Committer members pay no annual fees but are required to execute the Adoptium Individual Participation Agreement.</p>
    <h3>Adoptium Guest Member Annual Participation Fees</h3>
    <p>Guest members pay no annual fees but are required to execute the Adoptium Participation Agreement.</p>

    <p class="margin-top-20"><strong>Charter Version History</strong></p>
    <ul>
      <li>0.5 Eclipse Foundation Review</li>
	    <li>0.6 Update in support of the Eclipse Foundation corporate restructuring</li>
	    <li>0.7 Refine the Strategic member contributor obligation to vary by corporate revenue</li>
	    <li>0.8 Changes following exec director review</li>
      <li>1.0 Ratified by Steering Committee</li>
      <li>1.1 Removed Strategic member three-year commitment, FTEs and other standard refinements</li>
    </ul>
  </div>
</div>
