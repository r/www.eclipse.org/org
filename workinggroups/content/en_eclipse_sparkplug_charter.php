<?php

/**
 * Copyright (c) 2019 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 2.5 Revision History at the end</p>
    <h2>Vision and Scope</h2>
    <p><img src="assets/images/wg_sparkplug-logo.png" align="left" class="padding-right-10">

      The <strong> Sparkplug Working Group</strong> will drive the evolution and broad
      adoption of the Sparkplug protocol and related technologies that enable the creation
      of open, interoperable, Industrial IoT (IIoT) solutions.
    </p>
    <p>In particular, the Working Group will encourage the definition of technical specifications
      and associated implementations that rationalize access to industrial data, improve the
      interoperability and scalability of IIoT solutions, and provide an overall framework for
      supporting Industry 4.0.</p>
    <p>The Working Group will:</p>
    <ul>
      <li>Promote the &quot;Sparkplug&quot; brand and its value in the marketplace.</li>
      <li>Provide vendor neutral marketing and other services to the Sparkplug ecosystem.</li>
      <li>Leverage the Eclipse Foundation Specification Process to formalize the specifications that are defined
        within the scope of this working group.</li>
      <li>Define compatibility rules and a compatibility and branding process for implementations of
        these specifications to ensure application portability.</li>
      <li>Leverage Eclipse-defined licensing and intellectual property flows that encourage community participation,
        protect community members, and encourage usage.</li>
      <li>Manage the overall technical and business strategies related to its open source projects.</li>
      <li>Establish and drive a funding model that enables this working group and its community to
        operate on a sustainable basis.</li>
      <li>Manage the overall technical and business strategies for Eclipse Tahu and related projects.</li>
    </ul>

    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found
      on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents</a> page or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>
      All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.
    </p>
    <p>In the event of any conflict between the terms set forth in this working group&#39;s charter
      and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>
    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a <a href="/membership/become_a_member/membershipTypes.php#contributing">Contributing Member</a> of the Eclipse Foundation, have executed the Sparkplug Working Group
      Participation Agreement, and adhere to the requirements set forth in this Charter
      to participate.
    </p>
    <p>There are four classes of Sparkplug working group membership - Strategic,
      Participant, Committer and Guest. Each of these classes is described in detail below.</p>
    <p>The participation fees associated with each of these membership classes is shown in the
      tables in Schedule A. These are annual fees, and are established by the Sparkplug
      Steering Committee, and will be updated in this charter document accordingly.</p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any working
      group membership fees, and are decided as described in the <a href="/org/documents/eclipse_foundation-bylaws.pdf">Eclipse Foundation Bylaws</a> and detailed in the <a href="/org/documents/eclipse_membership_agreement.pdf">Eclipse Foundation Membership Agreement</a>.
    </p>
    <h2>Classes of Sparkplug Membership</h2>
    <h3>Strategic Members</h3>
    <p>Strategic Members are organizations that view IIoT standards and technologies as strategic to
      their organization and are investing significant resources to sustain and shape the activities
      of this working group.</p>
    <p>Strategic Members of the Sparkplug working group must be at least a Contributing Member
      of the Eclipse Foundation, and have a minimum of 3 developers participating on
      Sparkplug projects, and made at least one accepted commit or pull request to a Sparkplug
      project within the last 12months.</p>
    <h3>Participant Members</h3>
    <p>Participant Members are typically organizations that deliver products or services based on
      open IIoT standards and technologies, or view IIoT standards and technologies as strategic to
      their organization. These organizations want to participate in the development and direction
      of an open IIoT ecosystem.</p>
    <p>Participant Members of the Sparkplug Working Group must be at least Contributing Members
      of the Eclipse Foundation.</p>
    <h3>Committer Members</h3>
    <p>
      Committers are individuals who through a process of meritocracy defined by the Eclipse
      Development Process are able to contribute and commit code to the Eclipse Foundation
      projects included in the scope of this working group. Committers may become members of the
      working group by virtue of working for a member organization, or may choose to complete the
      membership-at-large process independently if they are not, along with completing this working
      group&apos;s Participation Agreement. For further explanation and details, see the
      Eclipse Committer Membership page.
    </p>
    <h3>Guest Members</h3>
    <p>Guest Members are organizations which are Associate members of the Eclipse Foundation who
      have been invited for one year, renewable, by the Steering Committee to participate in
      particular aspects of the activities of the Working Group. Typical guests include R&amp;D
      partners, universities, academic research centers, etc. Guests may be invited to participate
      in committee meetings at the invitation of the respective committee, but under no
      circumstances do Guest members have voting rights. Guest members are required to execute the
      Sparkplug Working Group Participation Agreement.</p>
    <h2>Membership Summary</h2>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th>Strategic Member</th>
          <th>Participant Member</th>
          <th>Committer Member</th>
          <th>Guest Member</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Member of the Steering Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>Elected</td>
          <td>N/A</td>
        </tr>
      </tbody>
    </table>
    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within the Working Group with a
      focus to collaborate around a particular topic or domain of direct interest to the working group.
      SIGs are designed to drive the objectives of a subset of the Members of the Working Group in
      helping them achieve a dedicated set of goals and objectives. The scope of the SIG must be
      consistent with the scope of the Working Group Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each SIG may be either
      temporary or a permanent structure within the working group. SIGs can be disbanded at any
      time by self selection presenting reasons to and seek approval from the Steering Committee.
      Steering Committees may disband a SIG at any time for being inactive or non compliant with the
      Working Group&apos;s Charter, or by request of the SIG itself. SIGs operate as a non-governing
      Body of the Working Group. There are no additional annual fees to Members for participation in
      a SIG.
    </p>
    <h2>Sponsorship</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to the working group on
      an ad hoc basis to support the activities of the Working Group and its managed projects. Money
      or services provided by sponsors are used as set forth in the working group annual budget. The
      working group is free to determine whether and how those contributions are recognized. Under
      no condition are sponsorship monies refunded.
    </p>
    <p>
      Sponsors need not be members of the Eclipse Foundation or of the Working Group.
    </p>
    <h2>Governance</h2>
    <p>This Sparkplug Working Group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.</li>
    </ul>
    <h2>Governing Bodies</h2>
    <p>In order to implement these principles, the Steering Committee has been defined (also referred to as the &quot;Body&quot; below) as described below:</p>
    <h2>Steering Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this
        working group. This will require acceptance of the specification process by these projects.</li>
      <li>Define and manage the roadmaps.</li>
      <li>Review and approve this charter.</li>
      <li>Define and approve the specification process to be used by the Sparkplug specifications.</li>
      <li>Ensure that all Specification Projects operate in an open, transparent, and vendor-neutral
        fashion in compliance with the Specification Process.</li>
      <li>Approve specifications for adoption by the community.</li>
      <li>Work with the Eclipse IoT Project Management Committee (IoT PMC) to ensure that the
        specification process is complied with by all Sparkplug specification projects.</li>
      <li>Review and approve the trademark policy to ensure compatibility of independent
        implementations of specifications.</li>
      <li>Define the trademark policy and Trademark Usage Guidelines to be used by the Sparkplug
        specifications.</li>
      <li>Ensure the consistency of logo usage and other marketing materials.</li>
      <li>Establish an annual program plan.</li>
      <li>Define the annual fees for all classes of the working group membership.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Approve (if any) customizations to the Eclipse Foundation Specification Process recommended by the Specification Committee.</li>
      <li>Approve the creation of subcommittees and define the purpose, scope, and membership
        of each such subcommittee.</li>
      <li>Approve the creation and retirement of Special Interest Groups (SIGs).</li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Steering Committee.</li>
      <li>One seat is allocated to Participant Members. Participant Member seats are allocated
        following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse
        Foundation Bylaws.</li>
      <li>
        One seat is allocated to Committer Members via election. The Committer Member seat
        is allocated following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse
        Foundation Bylaws.
      </li>
      <li>
        The Committee elects a chair who reports to the Steering Committee. This chair is
        elected among the members of the Committee. They will serve for a 12 month period or
        until their successor is elected and qualified, or as otherwise provided for in this Charter.
        There is no limit on the number of terms the chair may serve.
      </li>
    </ul>
    <h3>Meeting Management</h3>
    <p>The Steering Committee meets at least twice a year.</p>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all meetings related to the
      working group will follow a prepared agenda and minutes are distributed two weeks after the
      meeting and approved at the next meeting at the latest, and shall in general conform to the
      Eclipse Foundation Antitrust Policy.
    </p>
    <h2>Specification Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Specification Committee members are required to:</p>
    <ul>
      <li>Define the specification process to be used by all Sparkplug specifications, and refer to it for approval by the Steering Committee.</li>
      <li>Ensure that all specification expert groups operate in an open, transparent, and vendor-neutral fashion in compliance with the specification process.</li>
      <li>Approve specifications for adoption by the community. </li>
      <li>Approve profiles which define collections of specifications which meet a particular market requirement.</li>
      <li>Work with the Eclipse IoT Project Management Committee (Eclipse IoT PMC) to ensure that the specification process is complied with by all Sparkplug working group specification projects.</li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Specification Committee.</li>
      <li>One seat is allocated to Participant Members. Participant Member seats are allocated following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Foundation Bylaws.</li>
      <li>
        One seat is allocated to Committer Members via election. The Committer Member seat is
        allocated following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse
        Foundation Bylaws.
      </li>
      <li>Guest members that have been invited by the Sparkplug Steering Committee as observers. Guest members have no voting rights.</li>
      <li>Any additional individuals as designated from time to time by the Executive Director.</li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected among the members of the Committee. They will serve for a 12 month period, or until their successor is elected and qualified, or as otherwise provided for in this Charter.</li>
    </ul>
    <h3>Meeting Management</h3>
    <p>The Specification Committee meets at least once per quarter.</p>
    <h2>Common Dispositions</h2>
    <p>The dispositions below apply to all governance bodies for this working group, unless
      otherwise specified. For all matters related to membership action, including without
      limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws
      apply.</p>
    <p>
      Appointed representatives on the Body may be replaced by the Member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a
      Body member is unavailable to attend or participate in a meeting of the Body, they may be
      represented by another Body member by providing written proxy to the Body&apos;s mailing list in
      advance. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed
      from the Body upon the termination of the membership of such representative&apos;s Member
      organization.
    </p>
    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body
      are determined by a simple majority of the representatives represented at a committee meeting
      at which a quorum is present.
    </p>
    <h4>Super Majority</h4>
    <p>For actions (i) requesting that the Eclipse Foundation Board of Directors approve a
      specification license; (ii) approving specifications for adoption; (iii) approving or changing the name of the
      working group; and (iv) approving changes to annual Member contribution requirements; any such
      actions must be approved by no less than two-thirds (2/3) of the representatives represented at a committee
      meeting at which a quorum is present.</p>
    <h3>Term and Dates of Elections</h3>
    <p>
      This section only applies to the Steering Committee, Specification Committee, and the
      Marketing Committee.
    </p>
    <p>All representatives shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or re-designation of any
      representative following the completion of that representative&rsquo;s term of office.</p>
    <h4>Strategic Members</h4>
    <p>Strategic Members Representatives shall serve in such capacity on committees until the
      earlier of their removal by their respective appointing Member organization or as otherwise
      provided for in this Charter.</p>
    <h4>Elected Representatives</h4>
    <p>Elected representatives shall each serve one-year terms and shall be elected to serve for a 12
      month term, or until their respective successors are elected
      and qualified, or as otherwise provided for in this Charter. Procedures governing elections of
      Representatives may be established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this Charter.</p>
    <h3>Meetings Management</h3>
    <h4>Meeting Frequency</h4>
    <p>
      Each governing body meets at least twice a year. All meetings may be held at any place that
      has been designated from time-to-time by resolution of the corresponding Body. All meetings
      may be held remotely using phone calls, video calls, or any other means as designated from
      time-to-time by resolution of the corresponding Body.
    </p>
    <h4>Place of Meetings</h4>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding Body. All meetings may be held remotely using phone calls,
      video calls or any other means as designated from time-to-time by resolution of the
      corresponding Body.</p>
    <h4>Regular Meetings</h4>
    <p>No Body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representatives at least fifteen (15) calendar days
      prior to such meeting, which notice will identify all potential actions to be undertaken by
      the Body at the Body meeting. No representative will be intentionally excluded from Body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the Body is
      represented at the Body meeting. Electronic voting shall be permitted in conjunction with any
      and all meetings of the Body the subject matter of which requires a vote of the Body to be
      delayed until each such representative in attendance thereat has conferred with his or her
      respective Member organization as set forth in Section Voting above.</p>
    <h4>Actions</h4>
    <p>The Body may undertake an action only if it was identified in a Body Meeting notice or
      otherwise identified in a notice of special meeting.</p>
    <h3>Invitations</h3>
    <p>The Body may invite any member to any of its meetings. These invited attendees have no right
      of vote.</p>
    <h3>Sparkplug Working Group Annual Participation Fees Schedule A</h3>
    <p>The following fees have been established by the Sparkplug Steering Committee. These fees are
      in addition to each participant&rsquo;s membership fees in the Eclipse Foundation.</p>
    <h4>Sparkplug Strategic Member Annual Participation Fees</h4>
    <p>Strategic members are required to execute the Sparkplug Working Group Participation Agreement.</p>
    <p>Strategic members are required to commit to three (3) years of membership.</p>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than $1 billion</td>
          <td>$20,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $500 million but less than or equal to $1
            billion</td>
          <td>$15,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $100 million but less than or equal to $500
            million</td>
          <td>$10,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $10 million but less than or equal to $100
            million</td>
          <td>$5,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to $10 million</td>
          <td>$1,500</td>
        </tr>
      </tbody>
    </table>
    <h4>Sparkplug Participant Member Annual Participation Fees</h4>
    <p>Participant members are required to execute the Sparkplug Working Group Participation Agreement.</p>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than $1 billion</td>
          <td>$10,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to $1 billion but greater than $10
            million</td>
          <td>$5,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to $10 million</td>
          <td>$500</td>
        </tr>
      </tbody>
    </table>
    <h4>Sparkplug Committer Member Annual Participation Fees</h4>
    <p>
      Committer Members pay no annual fees, but are required to execute the Sparking Working
      Group Participation Agreement.
    </p>
    <h4>Sparkplug Guest Member Annual Participation Fees</h4>
    <p>Guest members pay no annual fees, but are required to execute the Sparkplug Working Group Participation
      Agreement.</p>
    <hr />
    <p>Charter Version History</p>
    <ul>
      <li>v1.0 created Nov 27, 2018.</li>
      <li>v2.0 created Dec 5, 2018</li>
      <li>v2.1 created Dec 14, 2018</li>
      <li>v2.2 created Feb 13, 2019</li>
      <li>v2.3 created Feb 26, 2020
        <ul>
          <li>Approved by the Steering Committee March 19, 2020</li>
        </ul>
      </li>
      <li>v2.4 created Nov, 2020
        <ul>
          <li>Updates in support of the Eclipse Foundation corporate restructuring</li>
        </ul>
      </li>
      <li>
        v2.5 created Jan 2022
        <ul>
          <li>Add Committer Member as Membership Class and other standard refinements</li>
        </ul>
      </li>
    </ul>
  </div>
</div>