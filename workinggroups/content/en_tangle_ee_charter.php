<?php
/**
 * Copyright (c) 2018-2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Mike Milinkovich (Eclipse Foundation)
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 1.1 Revision history at the end.</p>
    <h2>Vision and Scope</h2>
    <p>The Tangle EE Working Group will drive the evolution and broad adoption of the
      Tangle protocol and related technologies that enable the creation of a highly-scalable,
      open-source, permissionless, secure, peer-to-peer distributed ledger technology enabling the
      machine economy.</p>
    <p>In particular, the Working Group will encourage the definition of technical
      specifications and associated open source implementations.</p>
    <p>The Working Group will:</p>
    <ul>
      <li>Promote the &quot;Tangle EE&quot; brand and its value in the marketplace.</li>
      <li>Provide vendor neutral marketing and other services to the Tangle EE ecosystem.</li>
      <li>Leverage Eclipse-defined licensing and intellectual property flows that encourage community participation,
        protect community members, and encourage usage.</li>
      <li>Leverage the Eclipse Foundation Specification Process to formalize specifications that are defined
        within the scope of this working group.</li>
      <li>Manage the overall technical and business strategies of its related open source projects.</li>
      <li>Establish and drive a funding model that enables this working group and its community to
        operate on a sustainable basis.</li>
    </ul>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents</a> page or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li> 
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
    <p>In the event of any conflict between the terms set forth in this Working
      Group&#39;s Charter and the Eclipse Foundation Bylaws, Membership Agreement, Eclipse
      Development Process, Eclipse Industry Working Group Process, or any policies of the Eclipse
      Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or
      policy shall take precedence.</p>
    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a <a
        href="/membership/become_a_member/membershipTypes.php#contributing"
      >Contributing Member</a> of the Eclipse Foundation, have executed the Tangle EE Participation
      Agreement, and adhere to the requirements set forth in this Charter to
      participate.
    </p>
    <p>There are three classes of Tangle EE working group membership - Strategic,
      Participant, and Guest. Each of these classes is described in detail below.</p>
    <p>The participation fees associated with each of these membership classes is shown in
      the tables in Schedule A. These are annual fees, and are established by the Tangle EE Steering
      Committee, and will be updated in this charter document accordingly.</p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any working
      group membership fees, and are decided as described in the <a
        href="/org/documents/eclipse_foundation-bylaws.pdf"
      >Eclipse Foundation Bylaws</a> and detailed in the <a
        href="/org/documents/eclipse_membership_agreement.pdf"
      >Eclipse Foundation Membership Agreement</a>.
    </p>
    <h2>Classes of Tangle EE Membership</h2>
    <h3>Strategic members</h3>
    <p>Strategic Members are organizations that view IIoT standards and technologies as
      strategic to their organization and are investing significant resources to sustain and shape
      the activities of this working group.</p>
    <p>Strategic Members of the Tangle EE working group must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation, and have a minimum of 3 developers participating on Tangle EE projects, and made at least one accepted commit or pull request to a Tangle EE project within the last 12 months.</p>
    <h3>Participant members</h3>
    <p>Participant Members are typically organizations that deliver products or services
      based on open IIoT standards, specifications and technologies, or view IIoT standards and
      technologies as strategic to their organization. These organizations want to participate in
      the development and direction of an open IIoT ecosystem.</p>
    <p>Participant Members of the Tangle EE working group must be at least Contributing
      Members of the Eclipse Foundation.</p>
    <h3>Guest members</h3>
    <p>Guest Members are organizations which are Associate members of the Eclipse
      Foundation who have been invited for one year, renewable, by the Steering Committee to
      participate in particular aspects of the activities of the Working Group. Typical guests
      include R&amp;D partners, universities, academic research centers, etc. Guests may be invited
      to participate in committee meetings at the invitation of the respective committee, but under
      no circumstances do Guest members have voting rights. Guest members are required to execute
      the Tangle EE Working Group Participation Agreement.&nbsp;</p>
    <h3>Membership Summary</h3>
    <div>
      <table class="table table-stripped">
        <thead>
          <tr>
            <th>&nbsp;</th>
            <th class="text-center">
              Strategic Member
            </th>
            <th class="text-center">
              Participant Member
            </th>
            <th class="text-center">
              Guest Member
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Member of the Steering Committee
            </td>
            <td class="text-center">
              Appointed
            </td>
            <td class="text-center">
              Elected
            </td>
            <td class="text-center">
              N/A
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <h2>Governing Bodies</h2>
    <p>This Tangle EE working group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the Steering Committee has been defined
      (also referred to as the &quot;Body&quot; below) as described below:</p>
    <h2>Steering Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this
        working group. This will require acceptance of the specification process by these projects.</li>
      <li>Define and manage the project roadmaps.</li>
      <li>Review and approve this charter.</li>
      <li>Define and approve the specification process to be used by the Tangle EE specifications.</li>
      <li>Ensure that all Specification Projects operate in an open, transparent, and vendor-neutral
        fashion in compliance with the Specification Process.</li>
      <li>Approve specifications for adoption by the community.</li>
      <li>Work with the Eclipse IoT Project Management Committee (IoT PMC) to ensure that the
        specification process is complied with by all Tangle EE specification projects.</li>
      <li>Review and approve the trademark policy to ensure compatibility of independent
        implementations of specifications.</li>
      <li>Define the trademark policy and Trademark Usage Guidelines to be used by the Tangle EE
        specifications.</li>
      <li>Ensure the consistency of logo usage and other marketing materials.</li>
      <li>Define the annual fees for all classes of working group membership.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Invite Guest members to participate in the working group.</li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Steering Committee.</li>
      <li>Two seats are allocated to Participant Members. Participant Member seats are allocated
        following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Foundation
        Bylaws.</li>
    </ul>
    <h3>Meeting Management</h3>
    <p>The Steering Committee meets at least twice a year.</p>
    <h2>Common Dispositions</h2>
    <p>The dispositions below apply to all governance bodies for this working group,
      unless otherwise specified. For all matters related to membership action, including without
      limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws
      apply.</p>
    <h3>Good Standing</h3>
    <p>A representative shall be deemed to be in Good Standing, and thus eligible to vote
      on issues coming before the Body they participate in, if the representative has attended (in
      person or telephonically) a minimum of three (3) of the last four (4) Body meetings (if there
      have been at least four meetings). Appointed representatives on the Body may be replaced by
      the Member organization they are representing at any time by providing written notice to the
      Steering Committee. In the event a Body member is unavailable to attend or participate in a
      meeting of the Body, they may be represented by another Body member by providing written proxy to the Body’s mailing list in advance, which shall be included in determining whether the representative is in Good Standing. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the Body upon the termination of the membership of such representative&rsquo;s Member organization.</p>
    <h3>Voting</h3>
    <p>For actions (i) requesting that the Eclipse Foundation Board of Directors approve a
      specification license; (ii) approving specifications for adoption; (iii) approving or changing the name of the
      working group; and (iv) approving changes to annual Member contribution requirements; any such
      actions must be approved by no less than two-thirds (2/3) of the representatives in Good
      Standing represented at a committee meeting at which a quorum is present.</p>
    <h3>Term and Dates of elections</h3>
    <p>All representatives shall hold office until their respective successors are
      appointed or elected, as applicable. There shall be no prohibition on re-election or
      re-designation of any representative following the completion of that representative&rsquo;s
      term of office.</p>
    <h4>Strategic Members</h4>
    <p>Strategic Members Representatives shall serve in such capacity on committees until
      the earlier of their removal by their respective appointing Member organization or as
      otherwise provided for in this Charter.</p>
    <h4>Elected representatives</h4>
    <p>Elected representatives shall each serve one-year terms and shall be elected to
      serve from April 1 to March 31 of each calendar year, or until their respective successors are
      elected and qualified, or as otherwise provided for in this Charter. Procedures governing
      elections of Representatives may be established pursuant to resolutions of the Steering
      Committee provided that such resolutions are not inconsistent with any provision of this
      Charter.</p>
    <h3>Meetings Management</h3>
    <h4>Place of meetings</h4>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding Body. All meetings may be held remotely using phone calls,
      video calls or any other means as designated from time-to-time by resolution of the
      corresponding Body.</p>
    <h4>Regular meetings</h4>
    <p>No Body meeting will be deemed to have been validly held unless a notice of same
      has been provided to each of the representatives in Good Standing at least fifteen (15)
      calendar days prior to such meeting, which notice will identify all potential actions to be
      undertaken by the Body at the Body meeting. No representative will be intentionally excluded
      from Body meetings and all representatives shall receive notice of the meeting as specified
      above; however, Body meetings need not be delayed or rescheduled merely because one or more of
      the representatives cannot attend or participate so long as at least a quorum of the Body is
      represented at the Body meeting. Electronic voting shall be permitted in conjunction with any
      and all meetings of the Body the subject matter of which requires a vote of the Body to be
      delayed until each such representative in attendance thereat has conferred with his or her
      respective Member organization as set forth in Section Voting above.</p>
    <h4>Actions</h4>
    <p>The Body may undertake an action only if it was identified in a Body Meeting notice
      or otherwise identified in a notice of special meeting.</p>
    <h3>Invitations</h3>
    <p>The Body may invite any member to any of its meetings. These invited attendees have
      no right of vote.</p>
    <h3>Tangle EE Working Group Annual Participation Fees Schedule A</h3>
    <p>One of the initial tasks of the governing body of the group will define and approve
      a fee structure and budget which will allow the group to execute on its objectives in 2020 and
      beyond. The following fees are a basis for such discussion and would apply in 2020 if no other
      decision is approved by the end of the year.</p>
    <h4>Tangle EE Strategic Member Annual Participation Fees</h4>
    <p>Strategic members are required to execute the Tangle EE Participation Agreement.</p>
    <p>Strategic members are required to commit to three (3) years of membership.</p>
    <div>
      <table class="table table-stripped">
        <thead>
          <tr>
            <th>
              Corporate Revenue
            </th>
            <th class="text-right">
              Annual Fees
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Annual Corporate Revenues greater than $1 billion
            </td>
            <td class="text-right">
              $20,000
            </td>
          </tr>
          <tr>
            <td>
              Annual Corporate Revenues greater than $500 million but less than or
                equal to $1 billion
            </td>
            <td class="text-right">
              $15,000
            </td>
          </tr>
          <tr>
            <td>
              Annual Corporate Revenues greater than $100 million but less than or
                equal to $500 million
            </td>
            <td class="text-right">
              $10,000
            </td>
          </tr>
          <tr>
            <td>
              Annual Corporate Revenues greater than $10 million but less than or equal
                to $100 million
            </td>
            <td class="text-right">
              $5,000
            </td>
          </tr>
          <tr>
            <td>
              Annual Corporate Revenues less than or equal to $10 million
            </td>
            <td class="text-right">
              $1,500
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <h4>Tangle EE Participant Member Annual Participation Fees</h4>
    <p>Participant members are required to execute the Tangle EE Participation Agreement.</p>
    <div>
      <table class="table table-stripped">
        <thead>
          <tr>
            <th>
              Corporate Revenue
            </th>
            <th class="text-right">
              Annual Fees
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Annual Corporate Revenues greater than $1 billion
            </td>
            <td class="text-right">
              $10,000
            </td>
          </tr>
          <tr>
            <td>
              Annual Corporate Revenues less than or equal to $1 billion but greater
                than $10 million
            </td>
            <td class="text-right">
              $5,000
            </td>
          </tr>
          <tr>
            <td>
              Annual Corporate Revenues less than or equal to $10 million
            </td>
            <td class="text-right">
              $500
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <h4>Tangle EE Guest Member Annual Participation Fees</h4>
    <p>Guest members pay no annual fees, but are required to execute the Tangle EE
      Participation Agreement.</p>
    <hr />
    <p>Charter Version History</p>
    <ul>
      <li>v0.1 created Mar 6, 2019.</li>
      <li>v0.1.1 - typos fixed April 30, 2019.</li>
      <li>v0.1.2 - mention that there is no fee in 2019</li>
      <li>v0.1.3 - remove 2019 fee info (obsolete) Jan, 2020</li>
      <li>v1.0 - Approved by Tangle EE Steering Committee May 22, 2020</li>
      <li>v1.1 - Updates in support of the Eclipse Foundation corporate restructuring</li>
    </ul>
  </div>
</div>