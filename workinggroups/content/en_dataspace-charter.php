<?php

/**
 * Copyright (c) 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Olivier goulet (Eclipse Foundation) - Initial Implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="maincontent">
    <div id="midcolumn">
        <h1><?php print $pageTitle; ?></h1>
        <p>Version 1.0 - Revision history at end of document</p>

        <h2 id="vision-and-scope">Vision and Scope</h2>
        <p>
            The mission of the Eclipse Dataspace Working Group is to provide a
            forum for individuals and organizations to build and promote
            open-source software, specifications, and open collaboration models
            needed to create scalable, modular, extensible, industry-ready
            open-source components based on open standards for dataspaces.
        </p>
        <p>
            The primary objective of the Eclipse Dataspace Working Group is to
            encourage, develop, and promote open-source solutions that will
            enable the development, as well as the participation in dataspaces
            for organizations of all types and sizes. It does not favor a
            specific industry or type of organization and is fully dedicated to
            enable global adoption of dataspace technologies to foster the
            creation and operation of trusted data sharing ecosystems.
        </p>
        <p>
            The working group will have a strong focus on participating in
            standards development, implementation and onboarding of existing
            open-source projects and providing guidance to associated projects
            on alignment with the overarching goal of supporting a broad
            ecosystem of interoperable dataspaces.
        </p>
        <p>
            To this end, the working group aims to foster a component driven
            model that supports project collections in 3 distinct groups:
        </p>

        <h3>Dataspace Core & Protocols:</h3>
        <p>
            DCP focuses on the core protocol specifications and their
            standardization. It also provides alignment between the protocol
            specifications and OSS projects implementing mandatory dataspace
            functionality.
        </p>

        <h3>Dataspace Data Planes & Components</h3>
        <p>
            DDPC focuses on alignment between projects that implement data
            planes, which are essential components for dataspaces as well as
            additional optional components which enable advanced dataspace
            scenarios. These includes components which are not essential to
            create a viable dataspace but add capabilities that increase the
            business value of dataspaces.
        </p>

        <h3>Dataspace Authority & Management</h3>
        <p>
            DAM focuses on alignment of tools and workflows to enable the
            implementation of dataspaces. Its associated projects will support
            Dataspace Authorities to manage their dataspaces. This includes
            policy management, member management, and also starter kits for
            dataspace authorities.
        </p>
        <p>
            Overall the 3 groups of open-source projects are aiming to create an
            ecosystem of projects covering diverse aspects of dataspace
            solutions. Implementations are non-exclusive and
            competing/overlapping projects can exist. Especially in the group of
            Dataspace Data Planes & Components multiple overlapping projects are
            expected to emerge and co-exists from the beginning. Even Dataspace
            Core Components can have diverging implementations. Protocols are
            going to be the unifying aspect between projects, providing minimum
            viable interoperability.
        </p>

        <h2 id="mission">Mission of the Working Group</h2>
        <ul>
            <li>
                Foster open source implementations of needed components for
                dataspaces (build a core first).
            </li>
            <li>
                Define, document, standardize and promote protocols that
                Dataspace-related open source projects may choose to utilize in
                their development.
            </li>
            <li>
                Define, document, and promote processes that Dataspace-related
                open source projects may choose to utilize in their development.
            </li>
            <li>
                Define rules and a branding process for projects which
                participate in the Dataspace Working Group to inform the market
                of their relation to dataspaces and their process maturity.
            </li>
            <li>
                Define compatibility rules and a compatibility and branding
                process for implementations of these components to ensure
                interoperability.
            </li>
            <li>
                Enable reference distributions and foster integrations in
                various developer toolchains.
            </li>
            <li>
                Drive the funding model that enables this working group and its
                community to operate on a sustainable basis.
            </li>
            <li>
                Promote the Eclipse Dataspace brand, along with any additional
                brands as approved by the Steering Committee and the Eclipse
                Foundation, and their value in the marketplace.
            </li>
            <li>
                Provide vendor neutral marketing and other services to the
                Dataspace ecosystem.
            </li>
            <li>
                Communicate/Liaise with other open-source and specification
                communities.
            </li>
            <li>
                Leverage Eclipse-defined licensing and intellectual property
                flows that encourage community participation, protect community
                members, and encourage usage.
            </li>
            <li>
                Manage the overall technical and business strategies related to
                its open source projects.
            </li>
        </ul>

        <h2 id="governance-and-precedence">Governance and Precedence</h2>

        <h3>Applicable Documents</h3>
        <p>
            The following governance documents are applicable to this charter,
						each of which can be found on the <a href="/org/documents/">Eclipse Foundation Governance Documents</a> 
						page or the <a href="/legal/">Eclipse Foundation Legal Resources</a> 
						page:
        </p>
        <ul>
            <li>Eclipse Foundation Bylaws</li>
            <li>Eclipse Foundation Working Group Process</li>
            <li>Eclipse Foundation Working Group Operations Guide</li>
            <li>Eclipse Foundation Code of Conduct</li>
            <li>Eclipse Foundation Communication Channel Guidelines</li>
            <li>Eclipse Foundation Membership Agreement</li>
            <li>Eclipse Foundation Intellectual Property Policy</li>
            <li>Eclipse Foundation Antitrust Policy</li>
            <li>Eclipse Foundation Development Process</li>
            <li>Eclipse Foundation Trademark Usage Guidelines</li>
            <li>Eclipse Foundation Specification Process</li>
            <li>Eclipse Foundation Specification License</li>
            <li>Eclipse Foundation Technology Compatibility kit license</li>
        </ul>
        <p>
            All Members must be parties to the Eclipse Foundation Membership
            Agreement, including the requirement set forth in Section 2.2 to
            abide by and adhere to the Bylaws and then-current policies of the
            Eclipse Foundation, including but not limited to the Intellectual
            Property and Antitrust Policies.
        </p>
        <p>
            In the event of any conflict between the terms set forth in this
            working group's charter and the Eclipse Foundation Bylaws,
            Membership Agreement, Development Process, Specification Process,
            Working Group Process or any policies of the Eclipse Foundation, the
            terms of the respective Eclipse Foundation Bylaws, Membership
            Agreement, process or policy shall take precedence.
        </p>

        <h3>Membership</h3>
        <p>
						With the exception of Guest members as described below, an entity
						must be at least a <a href="/membership/#tab-levels">Contributing Member</a> 
						of the Eclipse Foundation, have executed the Eclipse Dataspace
						Working Group Participation Agreement, and adhere to the requirements 
						set forth in this Charter to participate.
        </p>
        <p>
            The participation fees associated with each of these membership
            classes are shown in the Annual Participation Fees section. These
            are annual fees, and are established by the Eclipse Dataspace
            Working Group Steering Committee, and will be updated in this
            charter document accordingly.
        </p>
        <p>
            The fees associated with membership in the Eclipse Foundation are
            separate from any Working Group membership fees, and are decided as
            described in the Eclipse Foundation Bylaws and detailed in the
            Eclipse Foundation Membership Agreement.
        </p>
        <p>
            There are five classes of the Eclipse Dataspace Working Group
            membership - Strategic, Participant, Committer, Supporter and Guest.
        </p>

        <h3>Classes of Membership</h3>

        <h4>Strategic Members</h4>
        <p>
            Strategic Members are organizations that view this Working Group
            standards, specifications and technologies as strategic to their
            organization and are investing significant resources to sustain and
            shape the activities of this working group. Strategic Members of the
            working group are required to commit to strategic membership for a
            three (3) year period. Strategic Members of this working group must
            be at least a Contributing Member of the Eclipse Foundation.
        </p>

        <h4>Participant Members</h4>
        <p>
            Participant Members are typically organizations that deliver
            products or services based upon related standards, specifications
            and technologies, or view this working group's standards and
            technologies as strategic to their organization. These organizations
            want to participate in the development and direction of an open
            ecosystem related to this working group. Participant Members of this
            working group must be at least a Contributing Member of the Eclipse
            Foundation.
        </p>

        <h4>Committer Members</h4>
        <p>
            Committers are individuals who, through a process of meritocracy
            defined by the Eclipse Foundation Development Process, are able to
            contribute and commit code to the Eclipse Foundation projects
            included in the scope of this working group. Committers may become
            members of the working group by virtue of working for a member
            organization, or may choose to complete the membership-at-large
            process independently if they are not, along with completing this
            working group's Participation Agreement once defined. For further
            explanation and details, see the <a href="/membership/become_a_member/committer.php">Eclipse Committer Membership page</a>.
        </p>

        <h4>Supporting Members</h4>
        <p>
            Supporting Members are: (a) organizations that want to participate
            in an open ecosystem related to this working group and wish to show
            their initial support for it, and (b) organizations which are at
            least Contributing members of the Eclipse Foundation. Organizations
            may be Supporting Members for a maximum period of 1 year only, after
            which time they are expected to change their membership to a
            different level. Supporting Members may be invited to participate in
            committee meetings at the invitation of the respective committee,
            but under no circumstances do Supporting Members have voting rights
            in any working group body. Further, Supporting Members are not
            considered Participant Members as that term is defined in the
            Eclipse Foundation Specification Process. Supporting Members are
            required to execute the Working Group's Participation Agreement.
        </p>

        <h4>Guest Members</h4>
        <p>
            Guest Members are organizations which are Associate members of the
            Eclipse Foundation. Typical guests include R&D partners,
            universities, academic research centers, etc. Guests may be invited
            to participate in committee meetings at the invitation of the
            respective committee, but under no circumstances do Guest members
            have voting rights. Guest members are required to execute the
            Working Group's Participation Agreement.
        </p>

        <h4>Membership Summary</h4>
        <table class="table">
            <thead>
                <tr>
                    <th>Committee Representation</th>
                    <th>Strategic Member</th>
                    <th>Participant Member</th>
                    <th>Committer Member</th>
                    <th>Supporter Member</th>
                    <th>Guest Member</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Steering Committee</td>
                    <td>Appointed</td>
                    <td>Elected</td>
                    <td>Elected</td>
                    <td>N/A</td>
                    <td>N/A</td>
                </tr>
                <tr>
                    <td>Marketing and Brand Committee</td>
                    <td>Appointed</td>
                    <td>Elected</td>
                    <td>Elected</td>
                    <td>N/A</td>
                    <td>N/A</td>
                </tr>
                <tr>
                    <td>Specification Committee</td>
                    <td>Appointed</td>
                    <td>Elected</td>
                    <td>Elected</td>
                    <td>N/A</td>
                    <td>N/A</td>
                </tr>
                <tr>
                    <td>Technical Advisory Committee</td>
                    <td>Appointed</td>
                    <td>Elected</td>
                    <td>Elected</td>
                    <td>N/A</td>
                    <td>N/A</td>
                </tr>
            </tbody>
        </table>

        <h2 id="governance">Governance</h2>
        <p>This Eclipse Dataspace Working Group is designed as:</p>
        <ul>
            <li>
                a vendor-neutral, member-, and contribution driven organization
            </li>
            <li>
                a means to foster a vibrant and sustainable ecosystem of
                components and service providers
            </li>
            <li>
                a means to organize the community of each project or component
                so that users and developers define the roadmap collaboratively.
            </li>
        </ul>

        <h2 id="governing-bodies">Governing Bodies</h2>

        <h3 class="governing-bodies-steering-committee">Steering Committee</h3>
        <h4>Powers and Duties</h4>
        <p>Steering Committee members are required to:</p>
        <ul>
            <li>
                Define and manage the strategy of the working group.
            </li>
            <li>
                Define and manage which Eclipse Foundation projects are included
                within the scope of this working group.
            </li>
            <li>
                Ensure the consistency of logo usage and other marketing
                materials.
            </li>
            <li>
                Define and manage the technical roadmap.
            </li>
            <li>
                Review and approve this charter.
            </li>
            <li>
                Review and approve any trademark policy referred to it by the
                Marketing and Brand Committee.
            </li>
            <li>
                Define the annual fees for all classes of the working group
                members.
            </li>
            <li>
                Establish an annual program plan.
            </li>
            <li>
                Approve the annual budget based upon funds received through
                fees.
            </li>
            <li>
                Approve (if any) customizations to the Eclipse Foundation
                Specification Process recommended by the Specification
                Committee.
            </li>
            <li>
                Approve the creation of subcommittees and define the purpose,
                scope, and membership of each such subcommittee.
            </li>
            <li>
                Approve the creation and retirement of Special Interest Groups
                (SIGs).
            </li>
        </ul>

        <h3 id="governing-bodies-composition">Composition</h3>
        <p>
            Each Strategic Member of the working group is entitled to a seat on
            the Steering Committee.
        </p>
        <p>
            Participant Members shall be entitled to at least one (1) seat on
            the Steering Committee. In addition, an additional seat on the
            Steering Committee shall be allocated to the Participant Members for
            every additional five (5) seats beyond one (1) allocated to
            Strategic Members via election. Participant Member seats are
            allocated following the Eclipse "Single Transferable Vote", as
            defined in the Eclipse Foundation Bylaws.
        </p>
        <p>
            One seat is allocated to Committer Members via election. The
            Committer Member seat is allocated following the Eclipse "Single
            Transferable Vote", as defined in the Eclipse Bylaws.
        </p>
        <p>
            The Committee elects a chair of the Steering Committee. This chair
            is elected among the members of the Committee. They will serve for a
            12 month period or until their successor is elected and qualified,
            or as otherwise provided for in this Charter. There is no limit on
            the number of terms the chair may serve.
        </p>

        <h3 id="governing-bodies-meeting-management">Meeting Management</h3>
        <p>
            The Steering Committee meets at least twice a year.
        </p>

        <h3 id="governing-bodies-marketing-and-brand-committee">Marketing and Brand Committee</h3>
        <h4>Powers and Duties</h4>
        <p>
            The Marketing and Brand Committee members are required to:
        </p>
        <ul>
            <li>
                Define strategic marketing priorities, goals, and objectives for
                the working group.
            </li>
            <li>
                Coordinate and support the implementation of developer and
                end-user outreach programs.
            </li>
            <li>
                Raise issues for discussion and provide feedback to the
                Foundation on the execution of marketing, brand, and
                communications activities for the working group.
            </li>
            <li>
                Assist the Eclipse Foundation in the working group's
                participation in conferences and events related to the working
                group.
            </li>
            <li>
                Define the trademark policy, if applicable, and refer to it for
                approval by the Steering Committee.
            </li>
            <li>
                Collaborate with other working group members to develop
                co-marketing strategies, amplify messaging on social channels,
                and share best practices.
            </li>
        </ul>
        <p>
            Committee members are expected to be leaders in communicating key
            messaging on behalf of the working group, and to play a leadership
            role in driving the overall success of the working group marketing
            efforts. In particular, members are encouraged to engage their
            respective marketing teams and personnel to amplify and support the
            achievement of the working group's goals and objectives.
        </p>

        <h4>Composition</h4>
        <p>
            Each Strategic Member of the working group is entitled to a seat on
            the Marketing Committee.
        </p>
        <p>
            Participant Members shall be entitled to at least one (1) seat on
            the Marketing Committee. In addition, an additional seat on the
            Marketing Committee shall be allocated to the Participant Members
            for every additional five (5) seats beyond one (1) allocated to
            Strategic Members via election. Participant Member seats are
            allocated following the Eclipse "Single Transferable Vote", as
            defined in the Eclipse Bylaws.
        </p>
        <p>
            One seat is allocated to Committer Members via election. The
            Committer Member seat is allocated following the Eclipse "Single
            Transferable Vote", as defined in the Eclipse Bylaws.
        </p>
        <p>
            The Committee elects a chair who reports to the Steering Committee.
            This chair is elected among the members of the Committee. They will
            serve for a 12 month period or until their successor is elected and
            qualified, or as otherwise provided for in this Charter. There is no
            limit on the number of terms the chair may serve.
        </p>

        <h4>Meeting Management</h4>
        <p>
            The Marketing and Brand Committee meets at least twice a year.
        </p>

        <h3 id="governing-bodies-specification-committee">Specification Committee</h3>
        <h4>Powers and Duties</h4>
        <p>
            Specification Committee members are required to:
        </p>
        <ul>
            <li>
                Ensure that all specification projects operate in an open,
                transparent, and vendor-neutral fashion in compliance with the
                specification process.
            </li>
            <li>
                Apply and govern the specification projects in the scope of this
                working group according to the approved specification process.
            </li>
            <li>
                Approve specifications for adoption by the community.
            </li>
            <li>
                Work with the related Project Management Committee (PMC) to
                ensure that the EFSP is complied with by all related working
                group specification projects.
            </li>
            <li>
                Review and approve the trademark policy to ensure compatibility
                of independent implementations of specifications.
            </li>
            <li>
                Define (if any) customizations to the EFSP. The EFSP with the
                approved customizations is the specification process to be used
                by all specifications related to this working group, and refer
                for approval by the Steering Committee.
            </li>
        </ul>
        <h4>Composition</h4>
        <p>
            Each Strategic Member of the working group is entitled to have a
            seat on the Specification Committee.
        </p>
        <p>
            Participant Members shall be entitled to at least one (1) seat on
            the Specification Committee. In addition, an additional seat on the
            Specification Committee shall be allocated to the Participant
            Members for every additional five (5) seats beyond one (1) allocated
            to Strategic Members via election. Participant Member seats are
            allocated following the Eclipse "Single Transferable Vote", as
            defined in the Eclipse Bylaws.
        </p>
        <p>
            One seat is allocated to Committer Members via election. The
            Committer Member seat is allocated following the Eclipse "Single
            Transferable Vote", as defined in the Eclipse Bylaws.
        </p>
        <p>
            Any additional individuals as designated from time to time by the
            Executive Director.
        </p>
        <p>
            The Committee elects a chair who reports to the Steering Committee.
            This chair is elected among the members of the Committee. They will
            serve for a 12 month period or until their successor is elected and
            qualified, or as otherwise provided for in this Charter. There is no
            limit on the number of terms the chair may serve.
        </p>
        <h4>Meeting Management</h4>
        <p>The Specification Committee meets at least once a quarter.</p>

        <h3 id="governing-bodies-technical-advisory-committee">Technical Advisory Committee</h3>
        <h4>Powers and Duties</h4>
        <p>Technical Advisory Committee members are required to:</p>
        <ul>
            <li>
                Recommend to the Steering Committee which Eclipse Foundation
                open-source projects should be included within the purview of
                the Eclipse Dataspace Working Group.
            </li>
            <li>
                Establish, evolve, and enforce policies and processes that
                augment the Eclipse Foundation Development Process and the
                Eclipse Foundation Specification Process to support dataspace
                requirements for the Eclipse Foundation open source projects
                that choose to contribute content to the Eclipse
                Dataspace-branded releases. Where appropriate, process
                definition work may be referred to the Specification Committee
                to be developed under the Eclipse Foundation Specification
                process.
            </li>
            <li>
                Establish a roadmap at least annually for review and approval by
                the Steering Committee.
            </li>
            <li>
                Establish a roadmap process which will solicit input from and
                build rough consensus with stakeholders including Projects and
                Project Management Committees.
            </li>
            <li>
                Establish a coordinated release plan that balances the many
                competing requirements. The release plan describes the themes
                and priorities that focus these releases, and orchestrates the
                dependencies among project plans.
            </li>
            <li>
                Coordinate the distribution release process and cross-project
                planning, facilitate the mitigation of architectural issues and
                interface conflicts, and generally address all other
                coordination and integration issues.
            </li>
            <li>
                Set, evolve, and enforce engineering policies and procedures for
                the creation and distribution of Eclipse Dataspace-branded
                releases, along with all intermediate releases.
            </li>
        </ul>
        <p>
            The Technical Advisory Committee discharges its responsibility via
            collaborative evaluation, prioritization, and compromise. The
            Technical Advisory Committee will establish a set of principles to
            guide its deliberations which will be approved by the Steering
            Committee and the Executive Director.
        </p>
        <h4>Composition</h4>
        <p>
            Each Strategic Member of the working group is entitled to have a
            seat on the Technical Advisory Committee.
        </p>
        <p>
            Participant Members shall be entitled to at least one (1) seat on
            the Technical Advisory Committee. In addition, an additional seat on
            the Technical Advisory Committee shall be allocated to the
            Participant Members for every additional five (5) seats beyond one
            (1) allocated to Strategic Members via election. Participant Member
            seats are allocated following the Eclipse "Single Transferable
            Vote", as defined in the Eclipse Bylaws.
        </p>
        <p>
            One seat is allocated to Committer Members via election. The
            Committer Member seat is allocated following the Eclipse "Single
            Transferable Vote", as defined in the Eclipse Bylaws.
        </p>
        <p>
            One seat is allocated for a representative of each Project
            Management Committee (PMC) responsible for the governance of one or
            more projects within the purview of the Eclipse Dataspace Working
            Group.
        </p>
        <p>
            Any additional individuals as designated from time to time by the
            Executive Director.
        </p>
        <p>
            The Committee elects a chair who reports to the Steering Committee.
            This chair is elected among the members of the Committee. They will
            serve for a 12 month period or until their successor is elected and
            qualified, or as otherwise provided for in this Charter. There is no
            limit on the number of terms the chair may serve.
        </p>
        <h4>Meeting Management</h4>
        <p>
            The Technical Advisory Committee meets at least once a quarter.
        </p>

        <h3 id="governing-bodies-sigs">Special Interest Groups</h3>
        <p>
            A Special Interest Group (SIG) is a lightweight structure formed
            within the Working Group with a focus to collaborate around a
            particular topic or domain of direct interest to the working group.
            SIGs are designed to drive the objectives of a subset of the Members
            of the Working Group in helping them achieve a dedicated set of
            goals and objectives. The scope of the SIG must be consistent with
            the scope of the Working Group Charter.
        </p>
        <p>
            The creation of a SIG requires approval of the Steering Committee.
            Each SIG may be either temporary or a permanent structure within the
            working group. SIGs can be disbanded at any time by self-selection
            presenting reasons to and seek approval from the Steering Committee.
            Steering Committees may disband a SIG at any time for being inactive
            or non-compliant with the Working Group's Charter, or by request of
            the SIG itself. SIGs operate as a non-governing Body of the Working
            Group. There are no additional annual fees to Members for
            participation in a SIG.
        </p>

        <h2 id="sponsorship">Sponsorship</h2>
        <p>
            Sponsors are companies or individuals who provide money or services
            to the working group on an ad hoc basis to support the activities of
            the Working Group and its managed projects. Money or services
            provided by sponsors are used as set forth in the working group
            annual budget. The working group is free to determine whether and
            how those contributions are recognized. Under no condition are
            sponsorship monies refunded.
        </p>
        <p>
            Sponsors need not be members of the Eclipse Foundation or of the
            Working Group.
        </p>

        <h2 id="common-dispositions">Common Dispositions</h2>
        <p>
            The dispositions below apply to all governance bodies for this
            working group, unless otherwise specified. For all matters related
            to membership action, including without limitation: meetings,
            quorum, voting, vacancy, resignation or removal, the respective
            terms set forth in the Eclipse Foundation Bylaws apply.
        </p>
        <p>
            Appointed representatives on the Body may be replaced by the Member
            organization they are representing at any time by providing written
            notice to the Steering Committee. In the event a Body member is
            unavailable to attend or participate in a meeting of the Body, they
            may be represented by another Body member by providing written proxy
            to the Body's mailing list in advance. As per the Eclipse Foundation
            Bylaws, a representative shall be immediately removed from the Body
            upon the termination of the membership of such representative's
            Member organization.
        </p>

        <h2 id="voting">Voting</h2>
        <h3 id="voting-simple-majority">Simple Majority</h3>
        <p>
            Excepting the actions specified below for which a Super Majority is
            required, votes of the Body are determined by a simple majority of
            the representatives represented at a committee meeting at which a
            quorum is present.
        </p>
        <h3 id="voting-super-majority">Super Majority</h3>
        <p>
            For actions (i) requesting that the Eclipse Foundation Board of
            Directors approve a specification license; (ii) approving
            specifications for adoption; (iii) modifying the working group
            charter; (iv) approving or changing the name of the working group;
            and (v) approving changes to annual Member contribution
            requirements; any such actions must be approved by no less than
            two-thirds (2/3) of the representatives represented at a committee
            meeting at which a quorum is present.
        </p>
        <h3 id="voting-term-and-dates-of-elections">Term and Dates of Elections</h3>
        <p>This section applies to all committees.</p>
        <p>
            All representatives shall hold office until their respective
            successors are appointed or elected, as applicable. There shall be
            no prohibition on re-election or re-designation of any
            representative following the completion of that representative's
            term of office.
        </p>
        <h3 id="voting-strategic-members">Strategic Members</h3>
        <p>
            Strategic Members Representatives shall serve in such capacity on
            committees until the earlier of their removal by their respective
            appointing Member organization or as otherwise provided for in this
            Charter.
        </p>
        <h3 id="voting-elected-representatives">Elected Representatives</h3>
        <p>
            Elected representatives shall each serve one-year terms and shall be
            elected to serve for a 12 month term or until their respective
            successors are elected and qualified, or as otherwise provided for
            in this Charter. Procedures governing elections of Representatives
            may be established pursuant to resolutions of the Steering Committee
            provided that such resolutions are not inconsistent with any
            provision of this Charter.
        </p>
        <h3 id="voting-meetings-management">Meetings Management</h3>
        <p>
            As prescribed in the Eclipse Foundation Working Group Process, all
            meetings related to the working group will follow a prepared agenda
            and minutes are distributed two weeks after the meeting and approved
            at the next meeting at the latest, and shall in general conform to
            the Eclipse Foundation Antitrust Policy.
        </p>
        <h4>Meeting Frequency</h4>
        <p>
            All meetings may be held at any place that has been designated from
            time-to-time by resolution of the corresponding Body. All meetings
            may be held remotely using phone calls, video calls, or any other
            means as designated from time-to-time by resolution of the
            corresponding Body.
        </p>
        <h4>Place of Meetings</h4>
        <p>
            All meetings may be held at any place that has been designated from
            time-to-time by resolution of the corresponding Body. All meetings
            may be held remotely using phone calls, video calls, or any other
            means as designated from time-to-time by resolution of the
            corresponding Body.
        </p>
        <h4>Regular Meetings</h4>
        <p>
            No Body meeting will be deemed to have been validly held unless a
            notice of same has been provided to each of the representatives at
            least fifteen (15) calendar days prior to such meeting, which notice
            will identify all potential actions to be undertaken by the Body at
            the Body meeting. No representative will be intentionally excluded
            from Body meetings and all representatives shall receive notice of
            the meeting as specified above; however, Body meetings need not be
            delayed or rescheduled merely because one or more of the
            representatives cannot attend or participate so long as at least a
            quorum of the Body is represented at the Body meeting.
        </p>
        <h4>Actions</h4>
        <p>
            The Body may undertake an action only if it was identified in a Body
            meeting notice or otherwise identified in a notice of special
            meeting.
        </p>
        <h4>Invitations</h4>
        <p>
            The Body may invite any member to any of its meetings. These invited
            attendees have no right to vote.
        </p>
        <h2>Working Group Participation Fees</h4>
        <p>
            The initial Steering Committee will be tasked with defining a fee
            structure and budget for the current and following years based on
            resource requirements necessary to achieve the objectives of the
            group. Members agree to pay the annual fees as established by the
            Steering Committee and as agreed to by each participating member in
            their respective Working Group Participation Agreement.
        </p>
        <p>
            No Participation Fees are charged in 2023. All Members who join
            prior to January 1, 2024, agree to begin paying the full annual fees
            associated with their participation effective January 2024 and each
            January thereafter. All Members who join the working group in 2024
            or beyond shall pay the full annual fees effective the month they
            join the working group, and each anniversary month thereafter.
        </p>
        <p>
            Established fees cannot be changed retroactively. Any change to the
            annual fees will be communicated to all Members via the mailing list
            and will be reflected in their next annual fees payment.
        </p>
        <h2>Working Group Annual Participation Fees Schedule A</h2>
        <p>
            The following fees have been established by the Eclipse Dataspace
            Steering Committee. These fees are in addition to each participant's
            membership fees in the Eclipse Foundation.
        </p>
        <h3>Eclipse Dataspace Strategic Member Annual Participation Fees</h3>
        <p>
            Strategic members are required to execute the Eclipse Dataspace
            Working Group Participation Agreement.
        </p>
        <p>
            Strategic members are required to commit to three (3) years
            membership.
        </p>
        <table class="table">
            <thead>
                <tr>
                    <th>Member Organizations</th>
                    <th>Annual Fees</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Commercial</td>
                    <td>&euro;50 000</td>
                </tr>
                <tr>
                    <td>Academic, Non-profit, Public Sector</td>
                    <td>&euro;15 000</td>
                </tr>
            </tbody>
        </table>
        <h3>Eclipse Dataspace Participant Member Annual Participation Fees</h3>
        <p>
            Participant members are required to execute the Eclipse Dataspace
            Working Group Participation Agreement.
        </p>
        <table class="table">
            <thead>
                <tr>
                    <th>Member Organizations</th>
                    <th>Annual Fees</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Commercial</td>
                    <td>&euro;20 000</td>
                </tr>
                <tr>
                    <td>Academic, Non-profit, Public Sector</td>
                    <td>&euro;10 000</td>
                </tr>
            </tbody>
        </table>
        <h3>Eclipse Dataspace Committer Member Annual Participation Fees</h3>
        <p class="margin-bottom-20">
            Committer members pay no annual fees, but are required to execute
            the Eclipse Dataspace Working Group Participation Agreement.
        </p>
        <h3>Eclipse Dataspace Supporter Member Annual Participation Fees</h3>
        <p>
            Supporter members are required to execute the Eclipse Dataspace
            Working Group Participation Agreement.
        </p>
        <table class="table">
            <thead>
                <tr>
                    <th>Member Organizations</th>
                    <th>Annual Fees</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Commercial</td>
                    <td>&euro;5 000</td>
                </tr>
                <tr>
                    <td>Academic, Non-profit, Public Sector</td>
                    <td>&euro;1 000</td>
                </tr>
            </tbody>
        </table>
        <h3>Eclipse Dataspace Guest Member Annual Participation Fees</h3>
        <p>
            Guest members pay no annual fees, but are required to execute the
            Eclipse Dataspace Working Group Participation Agreement.
        </p>

        <p class="margin-top-20"><strong>Charter Version History</strong></p>
        <ul>
            <li>v0.1 created March 30th, 2023</li>
            <li>v0.2 created May 8th, 2023</li>
            <li>Prefinal created July 12th, 2023</li>
						<li>Draft v0.3 created July 17th, 2023 - Eclipse Foundation Review</li>	
						<li>v1.0 created November 3rd, 2023 - approved by Steering Committee</li>	
        </ul>
    </div>
</div>
