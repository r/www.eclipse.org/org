<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle;?></h1>
     <div class="alert alert-danger">
      <strong><?php print $pageTitle;?> has been archived.</strong>
    </div>
    <p>February 25, 2018 Christophe Loetz (Compex Systemhaus GmbH)<br/>
      Revised March 10, 2018 Ralph Mueller (Eclipse Foundation)<br/>
      Revised March 16, 2018 Christophe Loetz (Compex Systemhaus GmbH)<br/>
      Revised April 04, 2018 Christophe Loetz (for final release as community review draft)
    </p>
    <h2>Scope, Goals and Vision</h2>
    <p>The OS.bea Working Group will drive the evolution and broad adoption of technologies of the Open Standard Business Platform (OSBP). </p>
    <p>The working group will: </p>
    <ul>
      <li>Manage the overall technical and business strategies for OSBP driven projects.</li>
      <li>Define and manage a specification process to formalize the specifications that are defined within the scope of this working group. </li>
      <li>Define compatibility rules and branding process for implementations of these specifications to ensure application portability. </li>
      <li>Promote the "OS.bea" brand and its value in the marketplace.</li>
      <li>Provide vendor-neutral marketing and other services to the OS.bea ecosystem. </li>
      <li>Establish and drive a funding model that enables this working group and its community to operate on a sustainable basis. </li>
    </ul>
    <p>The OS.bea Working Group (OS.bea WG) wants to foster and support an open and innovative
      eco-system providing tools and systems, and adapters for standardized, openly-available
      and vendor-neutral OSB Platform as described above.
    </p>
    <p>In particular, the OS.bea WG will </p>
    <ul>
      <li>Help to support OS.bea related Eclipse projects that develop the related software components </li>
      <li>Provide the resources for managing the quality and the maturity of these components throughout the life-cycle </li>
      <li>Ensure open innovation through the sharing of the research, development, and maintenance efforts as far as possible </li>
      <li>Foster exchanges between academics, standardization organizations, industry partners and community </li>
    </ul>
    <h2>Definitions</h2>
    <p><strong>OS.bea</strong><br>
      stands for "Open Standard Business and Enterprise Application".
    </p>
    <p><strong>Bodies</strong><br>
      Gremia, etc.
    </p>
    <p><strong>Eclipse Members</strong><br>
      Eclipse Members have signed the Eclipse Membership Agreement and have signed the Eclipse Foundation Membership Agreement and are in good standing. For a complete overview over the Eclipse Membership process and the current Eclipse members refer to the <a href="https://eclipse.org">Eclipse Home Page.</a>
    </p>
    <p><strong>OS.bea WG Members</strong><br>
      OS.bea WG members have signed the OS.bea WG participation agreement and have fulfilled the requirements as stated in the WG participation agreement exhibit.
    </p>
    <p><strong>OS.bea WG deliverables and results</strong><br>
      The services and goods (software, documents) to be delivered hereunder are referred to in this document as OS.bea WG deliverables / results.
    </p>
    <p><strong>OS.bea related Eclipse projects</strong><br>
      The Eclipse Foundation provides a framework for defining, driving and executing projects. "OS.bea related Eclipse projects" means Eclipse projects that are of interest for the OS.bea WG.
    </p>
    <p><strong>OS.bea WG participation agreement</strong><br>
      Eclipse members have to sign OS.bea WG participation agreement to participate in the OS.bea WG.
    </p>
    <p><strong>OS.bea Software</strong><br>
      OS.bea Software is software using OSBP technology and its related frameworks, projects and technologies as appropriate.
    </p>
    <p><strong>Project Lead</strong><br>
      A natural person that leads an OS.bea related Eclipse project. The Project Lead may be an employee of a WG member.
    </p>
    <p><strong>Open Standard Business Platform (OSBP)</strong><br>
      Stands for the Eclipse Open Standard Business Platform (OSBP) project.
    </p>
    <h2>Governance</h2>
    <p>The OS.bea working group is designed as:</p>
    <ul>
      <li>a working group driven by its strategic members,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of users, contributors and service providers,</li>
      <li>a means to organize the community of each project or component so that strategic users and developers define the roadmap collaboratively. </li>
    </ul>
    <p>In order to implement these principles, the following governance bodies have been defined (each a "Body"): </p>
    <ul>
      <li>The Steering Committee</li>
      <li>The Specification Committee</li>
      <li>The Marketing and Brand Committee</li>
    </ul>
    <h2>Membership</h2>
    <p>An entity must have executed the OS.bea Participation Agreement once defined, and adhere to the requirements set forth in this Charter to participate. Every entity must be at least an Eclipse Solution member. </p>
    <p>There are three classes of OS.bea membership - Strategic, Participant, and Committer. Each of these classes is described in detail below. </p>
    <p>The fees associated with each of these OS.bea membership classes is also shown below. </p>
    <p>In addition, all matters related to Membership in the Eclipse Foundation and this OS.bea working group will be governed by the Eclipse Foundation Bylaws, Membership Agreement and Eclipse Working Group Process. These matters include, without limitation, delinquency, payment of dues, termination, resignation, reinstatement, and assignment. </p>
    <TABLE class="table">
      <TR>
        <TH style="text-align: center;"><strong>Member of</strong></TH>
        <TH style="text-align: center; background-color: #d3d3d3"><strong>Strategic Consumer Member</strong></TH>
        <TH style="text-align: center; background-color: #d3d3d3"><strong>Strategic Developer Member</strong></TH>
        <TH style="text-align: center; background-color: #d3d3d3"><strong>Participant Member</strong></TH>
      </TR>
      <TR>
        <TD style="text-align: left; background-color: #d3d3d3">Steering Committee</TD>
        <TD style="text-align: center;">Appointed</TD>
        <TD style="text-align: center;">Appointed</TD>
        <TD style="text-align: center;">Elected</TD>
      </TR>
      <TR>
        <TD style="text-align: left; background-color: #d3d3d3">Specification Committee</TD>
        <TD style="text-align: center;">Appointed</TD>
        <TD style="text-align: center;">Appointed</TD>
        <TD style="text-align: center;">Elected</TD>
      </TR>
      <TR>
        <TD style="text-align: left; background-color: #d3d3d3">Marketing Committee</TD>
        <TD style="text-align: center;">Appointed</TD>
        <TD style="text-align: center;">Appointed</TD>
        <TD style="text-align: center;">Elected</TD>
      </TR>
    </TABLE>
    <p>Members who resign, or otherwise terminate their membership in the working group, lose their rights to access and use any private assets and data of the working group after the effective date of the termination.</p>
    <h3>Strategic Consumer Member</h3>
    <p>Strategic Consumer Members want to influence the definition and further development of the OS.bea brand and technology as well as all its deliverables. </p>
    <p>They are members of the Steering Committee and invest an essential amount of resources to sustain the WG activities. Typically  Strategic Consumer members include users of the technologies and results provided by  the OS.bea Working Group. Most of them use OS.bea technology at large.</p>
    <h3>Strategic Developer Member</h3>
    <p>Strategic Developer Members consider the OS.bea technology stack and the related business opportunities essential to the success of the company. Typically they invest heavily in definition and development of the OS.bea related technology.</p>
    <h3>Participant Members</h3>
    <p>Participant Members consider the OS.bea technology a relevant addition to their business. They are either users of the related technology and service offerings or they are interested in adoption, extension and service provisioning for the OS.bea technology stack. </p>
    <h3>Guest Members</h3>
    <p>Guests are organizations who have been invited for one year by the OS.bea Steering Committee to participate in particular aspects of the activities of the Working Group. Typically guests includes R&D partners, Universities, academic research centers, and potential full-fledged members who want to have a closer look before deciding on their participation strategy. Guest membership is free of charge and invitations may be renewed once by the Steering Committee. Guests are required to sign an Eclipse and OS.bea Membership Agreement, but when participating in meetings of a OS.bea body, they have no voting rights.</p>
    <h2>Bodies</h2>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group</li>
      <li>Define and execute the marketing strategy for the working group</li>
      <li>Define and manage the technical roadmap</li>
      <li>Review and execute the specification process</li>
      <li>Review and approve this charter</li>
      <li>Define the budget and fees for all classes of working group membership each year.</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Strategic (Consumer and Developer) Member of the working group has a seat on the Steering Committee.</li>
      <li>"Transferable Vote", as defined in the Eclipse Bylaws. </li>
      <li>At least one seat is allocated to Participant Members. Participant Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Bylaws. </li>
    </ul>
    <h3>Specification Committee</h3>
    <p>Will be defined in Powers and Duties as one of the first tasks</p>
    <h4>Powers and Duties</h4>
    <p>Specification Committee Members are required to:</p>
    <ul>
      <li>Define the specification process to be used by the OS.bea WG, and refer to it for approval by the Steering Committee. </li>
      <li>Ensure that all specification expert groups operate in an open, transparent, and vendor-neutral fashion in compliance with the specification process. </li>
      <li>Approve specifications for adoption by the community.</li>
      <li>Approve profiles which define collections of specifications which meet a particular market requirement. </li>
      <li>Work with the OSBP to ensure that the specification process is complied with by all specification projects. </li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Specification Committee.</li>
      <li>At least one seat is allocated to Participant Members. Participant Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Bylaws. </li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected among the members of the Committee. They will serve from April 1 to March 31 of each calendar year, or until their successor is elected and qualified, or as otherwise provided for in this Charter. </li>
    </ul>
    <h3>Marketing and Brand Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Marketing and Brand Committee members are required to:</p>
    <ul>
      <li>Define and implement marketing and communication activities for the working group</li>
      <li>Define and implement developer and end user outreach programs</li>
      <li>Provide requirements to the Eclipse Foundation for conferences and events related to the OS.bea WG</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Marketing Committee.</li>
      <li>At least one seat is allocated to Participant Members. Participant Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Bylaws. </li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected among the members of the Committee. They will serve from April 1 to March 31 of each calendar year, or until their successor is elected and qualified, or as otherwise provided for in this Charter. </li>
    </ul>
    <h2>Meeting Management </h2>
    <h3>Common Dispositions</h3>
    <p>For all matters related to membership action, including without limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy, resignation or removal, the terms set forth in Section 6 of the Eclipse Foundation Bylaws apply. </p>
    <h3>Meeting frequency</h3>
    <p>All bodies meet in person at least twice a year. </p>
    <h3>Invitation and quorum</h3>
    <p>No Body meeting will be deemed to have been validly held unless a notice of same has been provided to each of the representative in Good Standing at least thirty (30) calendar days prior to such meeting, which notice will identify all potential actions to be undertaken by the Body at the Body meeting. No representative will be intentionally excluded from Body meetings and all representatives shall receive notice of the meeting as specified above; however, Body meetings need not be delayed or rescheduled merely because one or more of the representatives cannot attend or participate so long as at least a quorum of the Body (as defined in the Common Dispositions section) is represented at the Body meeting. Electronic voting shall be permitted in conjunction with any and all meetings of the Body, the subject matter of which requires a vote of the Body to be delayed until each such representative in attendance thereat has conferred with his or her respective Member organization as set forth in Section Voting above.</p>
    <h3>Place of Meetings </h3>
    <p>All meetings may be held at any place that has been designated from time-to-time by resolution of the corresponding body. All meetings may be held remotely using phone calls, video calls, or any other means as designated from time-to-time by resolution of the corresponding body. </p>
    <h3>Actions </h3>
    <p>The body may undertake an action only if it was identified in a body meeting notice or otherwise identified in a notice of special meeting. </p>
    <h3>Invitations </h3>
    <p>The body may invite any OS.bea member to any of its meetings. These invited attendees have no right of vote. </p>
    <h3>Decisions </h3>
    <p>Decisions shall be taken by simple majority vote, unless specified otherwise. The body has a quorum if all representatives have properly been invited. Decisions shall be reported in writing. Guests do not have any voting rights. </p>
    <h3>Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee, Specification Committee, and the Marketing Committee.</p>
    <p>All representatives shall hold office until their respective successors are appointed or elected, as applicable. There shall be no prohibition on re-election or re-designation of any representative following the completion of that representative's term of office. </p>
    <h4>Strategic Members</h4>
    <p>Strategic Members Representatives shall serve in such capacity on committees until the earlier of their removal by their respective appointing Member organization or as otherwise provided for in this Charter. </p>
    <h4>Elected representatives</h4>
    <p>Elected representatives shall each serve one-year terms and shall be elected to serve from April 1 to March 31 of each calendar year, or until their respective successors are elected and qualified, or as otherwise provided for in this Charter. Procedures governing elections of Representatives may be established pursuant to resolutions of the Steering Committee provided that such resolutions are not inconsistent with any provision of this Charter. </p>
    <h3>Applicable Documents</h3>
    <ul>
      <li><a href="/org/documents/eclipse_foundation-bylaws.pdf">Eclipse Bylaws</a></li>
      <li><a href="/org/workinggroups/industry_wg_process.php">Industry Working Group Process</a></li>
      <li><a href="/org/documents/Eclipse%20MEMBERSHIP%20AGMT%202010_01_05%20Final.pdf">Eclipse Membership Agreement</a></li>
      <li><a href="/org/documents/Eclipse_IP_Policy.pdf">Eclipse Intellectual Property Policy</a></li>
      <li><a href="/org/documents/Eclipse_Antitrust_Policy.pdf">Eclipse Anti-Trust Policy</a></li>
      <li><a href="/projects/dev_process/development_process.php">Eclipse Development Process</a></li>
    </ul>
    <p>All Members must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to follow the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Anti-Trust Policies.</p>
    <p>In the event of any conflict between the terms set forth in this Working Group's Charter and the Eclipse Foundation Bylaws, Membership Agreement, Eclipse Development Process, Eclipse Industry Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence. </p>
    <h3>OS.bea Working Group Fees</h3>
    <p>In year one the fees will be $ 0.--</p>
  </div>
</div>