<?php
/**
 * Copyright (c) 2016, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>
      November 2020. Version v1.1 Revision history is at end of document
    </p>
    <p>This Working Group Charter is entered into and made part of the openPASS Working Group (WG)
      Participation Agreement by the undersigned Working Group Members.</p>
    <h2>Definitions</h2>
    <p>
      <strong>ADAS</strong><br> stands for "Advanced Driver Assistance System". Any vehicle function
      that assists the driver in his driving tasks.
    </p>
    <p>
      <strong>Automatic driving</strong><br> Any vehicle function that automatically executes
      driving tasks.
    </p>
    <p>
      <strong>Bodies</strong><br> Gremia, Working Groups etc.
    </p>
    <p>
      <strong>Eclipse Members</strong><br> Eclipse Members have signed the Eclipse Membership
      Agreement and have paid their membership fees. For a complete overview over the Eclipse
      Membership process and the current Eclipse members refer to the <a href="https://eclipse.org/">Eclipse
        Home Page</a>.
    </p>
    <p>
      <strong>openPASS WG Members</strong><br> openPASS WG members have signed the openPASS WG
      participation agreement and have fulfilled the requirements as stated in the WG exhibit.
    </p>
    <p>
      <strong>openPASS WG deliverables and results</strong><br> The services and goods (software,
      documents) to be delivered hereunder are referred to in this document as openPASS WG
      deliverables / results.
    </p>
    <p>
      <strong>openPASS related Eclipse projects</strong><br> The Eclipse foundation provides a
      framework for defining, driving and executing projects. "openPASS related Eclipse projects"
      means Eclipse projects that are of interest for the openPASS WG.
    </p>
    <p>
      <strong>Project Manager</strong><br> A natural person that leads an openPASS related Eclipse
      project. The Project Manager may be an employee of a WG member. In this case, the Project
      Manager has a seat on the Architecture Committee and the Quality Committee (see below).
    </p>
    <p>
      <strong>openPASS</strong><br> Short form of "Open Platform for Assessment of Safety Systems".
    </p>
    <p>
      <strong>openPASS WG participation agreement</strong><br> Eclipse members have to sign openPASS
      WG participation agreement to participate in the openPASS WG.
    </p>
    <p>
      <strong>openPASS WG Operational Rules</strong><br> The operational rules define the details
      for the collaboration for the WG members. They are defined and published by corresponding
      bodies of the openPASS WG.
    </p>
    <p>
      <strong>Requirements package</strong><br> The services and goods (software, documents) to be
      delivered hereunder are referred to in this document as openPASS deliverables / results (see
      above). The corresponding specification provided by the client is referred to as "Requirements
      Package"".
    </p>
    <p>
      <strong>Result package</strong><br> See openPASS deliverables / results
    </p>
    <p>
      <strong>Service package</strong><br> See openPASS deliverables / results
    </p>
    <h2>Goals and Vision</h2>
    <p>The rise of advanced driver assistance systems and partially automated driving functions
      leads to the need of virtual simulation to assess these systems and their effects. This
      especially refers, but is not limited, to safety effects in traffic. There are various methods
      and tools for prospective evaluation of safety systems with respect to traffic safety.
      Implementing the methodology by creating and maintaining the SIM@openPASS platform will
      support reliability and transparency of results obtained by simulation. The growing number,
      complexity, and variety of those vehicle functions make simulation an essential part in
      research, development, testing, public rating, and homologation and is thus, directly or
      indirectly, required by all stakeholders in vehicle safety, such as manufacturers, suppliers,
      insurance companies, legislators, consumer advocates, academia, and other.</p>
    <p>Every result obtained by the use of any openPASS related Eclipse project result shall clearly
      identify the Version of the openPASS related project it was calculated with.</p>
    <p>The openPASS working group is the driving force behind related development of core frameworks
      and modules. The openPASS WG shall endeavor to make sure that openPASS related Eclipse
      projects are in line with external important developments. The goal is a broad availability of
      different modules.</p>
    <p>
      <strong>Innovation</strong>: Product development is driven by a constant innovative progress
      in vehicle system development which cover not only the product features but also the
      development of methods and tools. Furthermore, there are constant proceedings from scientific
      research focusing on road and vehicle safety to be considered. Assessment tools for virtual
      simulation must ensure to cope with innovative methods and environments.
    </p>
    <p>
      <strong>Architecture integrity and Modularity</strong>: According to the distributed software
      development, exchange of simulation modules is a crucial issue for collaboration. Compliance
      with software architecture is a key requirement. It is necessary to use open and standardized
      interfaces.
    </p>
    <p>
      <strong>Long Term Availability</strong>: Simulation environment and modules are for that
      reason precious for the manufacturer and have to be stored carefully throughout the whole
      product lifecycle and beyond. Simulation results can thus be recreated at a later point in
      time.
    </p>
    <p>
      <strong>Legal requirements</strong>: Legal and regulatory requirements are changing from time
      to time and from country to country for the automotive industry. Changes should be reflected
      in the development of the SIM@openPASS platform.
    </p>
    <h2>Scope and Core Domains</h2>
    <p>The openPASS Working group (openPASS WG) wants to foster and support an open and innovative
      eco-system providing tools and systems, and adapters for standardized, openly-available and
      vendor-neutral platform for simulation of traffic scenarios as described above.</p>
    <p>In particular, the openPASS WG will</p>
    <ul>
      <li>Define requirements for the development of the SIM@openPASS platform and its related
        applications</li>
      <li>Help to support openPASS related Eclipse projects that provide the necessary software
        components</li>
      <li>Provide the resources for managing the quality and the maturity of these components
        throughout the life-cycle</li>
      <li>Ensure open innovation through the sharing of the research, development, and maintenance
        efforts as far as possible</li>
      <li>Foster exchanges between academics, standardization organizations, industry partners and
        community</li>
      <li>Provide and maintain methods and best practices for standardized and vendor independent
        simulation platforms</li>
    </ul>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents and Processes</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
	    <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Public License (EPL)</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li> 
    </ul>
    <p>All members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies. </p>
    <p>In the event of any conflict between the terms set forth in this working group’s charter and the Eclipse Foundation Bylaws, Membership Agreement, Eclipse Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the respective Eclipse Foundation Bylaws, Membership Agreement, processes, or policies shall take precedence.</p>
    <p>The openPASS WG will apply the Eclipse Foundation processes where applicable.</p>

    <h3>Collaboration</h3>
    <p>Collaboration of the members is crucial for the success of a vital openPASS WG. Each member
      individually commits itself to cooperation applying principles of openness, transparency and
      meritocracy. Every member will fulfill its commitment to best knowledge and conscience.</p>
    <p>The collaboration duties of the openPASS WG include, but are not limited to</p>
    <ul>
      <li>Annual plan containing themes, outreach</li>
      <li>Definition of requirement for result packages and services packages</li>
      <li>Valuation of the defined packages with respect to estimated man power units, content and
        quality for their delivery</li>
      <li>Assignment of requirement packages and / or service modules to the WG members according to
        their openPASS WG membership fees.</li>
      <li>Acceptance of the corresponding services or results delivered by the openPASS WG members.
      </li>
      <li>Monitoring the quality of the deliverables</li>
    </ul>
    <p>Each WG member bears sole responsibility for its own contributions to software, work results
      and any other products created or published in connection with the WG, in relation to all
      other WG members.</p>
    <p>The WG members will provide reasonable assistance to each other in the event that a third
      party makes claims or initiates legal proceedings against a member. Where a WG member is
      liable towards a third party for the contribution of another WG member, the WG member's right
      to take recourse against this other WG member without limitation shall remain unaffected.</p>
    <p>Each WG member may, at its sole discretion, designate third parties, including affiliates, to
      act on the WG member's behalf pertaining to the WG. A WG member may in particular provide
      contributions to software and work results by or through third parties and entrust third
      parties to assume functions within the WG the WG member&rsquo;s behalf (e.g. within a WG
      Committee). All acts of a third party are acts on behalf of the WG member it has been
      designated by and are deemed acts of this WG member.</p>
    <p>Where contributions of a WG member are published (e.g. software or work results), they shall
      be published under the name of the Eclipse Foundation, the respective contributing WG member
      or, if applicable under the name of the respective contributing third party designated by this
      WG member, and the WG members shall clarify who the copyright holder of any such contributions
      are. All software, work results, or any other products shall be published by openPASS related
      Eclipse projects, rather than on behalf of or under the name of the WG as such.</p>
    <p>No WG member is authorized to act with regard to third parties as an agent or representative
      of other WG members or the WG. No member shall create the appearance of such authorization.</p>
    <h3>Working Group Participation</h3>
    <p>In order to participate in the openPASS WG, an entity must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation, have executed the openPASS WG Participation Agreement, and adhere to the requirements set forth in this charter. The Eclipse Contributing Member fees are determined as described in the Eclipse Foundation Bylaws and detailed in the Eclipse Foundation Membership Agreement.</p>
    <p>The openPASS WG is open at any time to all new members who meet these conditions.</p>
    <h4>Classes of Membership</h4>
    <p>The membership classes of the openPASS WG are established to reflect the different interest
      situations of the members. The membership class has to be declared by the potential member in
      his openPASS WG participation agreement. The membership class of each openPASS WG member is
      checked once a year.</p>
    <h5>Driver Members</h5>
    <p>Driver Members want to influence the definition and further development of openPASS and all
      its deliverables. They are members of the Steering Committee and invest an essential amount of
      resources to sustain the WG activities. Typical Driver members include industry users of the
      technologies and results provided by openPASS WG. Most of them simulate, develop, and test
      vehicle functions with possible influence on traffic safety as one part of their core
      business.</p>
    <h5>User Members</h5>
    <p>User Members use the technologies and results provided by the openPASS WG. They want to keep
      track of the openPASS development but do not want to influence in an essential way. Most User
      Members simulate, develop, and test vehicle function with possible influence on traffic safety
      as one part of their core business.</p>
    <h5>Service Provider Members</h5>
    <p>Service Provider Members view development of virtual simulation tools as an important part of
      their corporate and product strategy and offer services for deployment, development or
      maintenance of openPASS relevant components or systems.</p>
    <h5>Guests</h5>
    <p>Guests are organizations who have been invited for one year by the Steering Committee of
      openPASS WG to participate in some aspects of the activities of the Working Group. Typical
      Guests include all stakeholders in vehicle safety and beyond, such as R&amp;D partners,
      academic entities, legislators, consumer protection advocates, insurance companies, and
      potential future full-fledged members who want to have a closer look before deciding on their
      strategy. When Guests are invited to an openPASS WG body for meetings, they have no right to
      vote. Invitations may be renewed by the Steering Committee. Guests are required to sign the
      participation agreement.</p>
    <p>Guests need to be at least Eclipse Associate Members.</p>
    <h4>openPASS WG Participation Fees</h4>
    <p>To operate properly, the openPASS WG will request additional services from the Eclipse
      Management Organization (EMO) or execute certain tasks with resources from the WG
      participants. Participation fees are due in addition to the Eclipse membership fees and are
      outlined in the Membership Fee Structure table below.</p>
    <p>On an annual basis, participation fees will be decided by the Steering Committee and this Charter will be updated accordingly to allow for the execution of these services. Results for employee service days can be delivered as</p>
    <ul>
      <li>result packages or</li>
      <li>service packages.</li>
    </ul>
    <p>The corresponding packages or services have to be offered in advance to and accepted by the
      steering committee. Participation in the working group is free for Guest members.</p>
    <p>If there are no open membership fees left, voluntary donations of man power units by the
      openPASS WG members are welcome as well. Otherwise the requirements packages won't be
      processed. The execution or delivery of the assigned packages are fulfilled independently by
      the respective member under its full responsibility and on its own expenses.</p>
    <h4>Case of Violation</h4>
    <p>Membership is checked annually by the Steering Committee and can be terminated, suspended or
      changed to a different membership class by the openPASS WG steering committee if the member
      fails to deliver participation fees or previously committed results or otherwise violates
      applicable governance documents. Those decisions have to be taken by unanimous vote with
      exception of the affected member.</p>
    <h4>Termination</h4>
    <p>On observing a 4 (four) weeks period of notice each member shall be entitled to terminate its
      participation by giving written notice to the Steering Committee. Delivered contributions are
      not refundable. After termination the respective member is not in charge of any further
      deliveries.</p>
    <h2>Services</h2>
    <h3>Collaboration Infrastructure</h3>
    <p>The openPASS WG leverages the standard Eclipse open source collaboration infrastructure. As
      such, source code repositories, Bugzilla, wikis, forums, project mailing lists, and other
      services provided as the open source collaboration infrastructure are publicly visible.
      Committers of openPASS related Eclipse projects have write access to this infrastructure, and
      as such have the rights and obligations as set forth in the Eclipse Development Process and
      the various Eclipse committer agreements.</p>
    <p>All openPASS WG deliverables and results are published to the Eclipse open source
      infrastructure. The standard license shall be the Eclipse Public License (EPL). Exceptions to
      this rule need to be proposed by the Steering Committee and approved by the board of directors
      of the Eclipse Foundation.</p>
    <h3>Requirements Management</h3>
    <p>The requirements on openPASS packages will be collected, consolidated and assigned to
      packages, which can be handled by Eclipse projects. This task is fulfilled by the requirements
      management service of the openPASS WG based on the Eclipse collaboration infrastructure
      services.</p>
    <h3>Quality Assurance (QA)</h3>
    <p>openPASS packages have to be tested with respect to their functional, non-functional
      requirements and their seamless interoperability with the openPASS integration environment.
      This environment is maintained by the openPASS QA service and represents a superset of all
      components available to the users. The QA reports are the basis for the acceptance of the
      contributed packages by the Steering committee.</p>
    <h3>Architecture Compliance</h3>
    <p>openPASS concepts and components have to be evaluated with respect to their architectural
      compliance with the SIM@openPASS software architecture. The openPASS WG provides such a
      service.</p>
    <h3>Marketing and Branding</h3>
    <p>
      One major success factor for the openPASS WG is the adoption of openPASS technology and a
      flourishing ecosystem for many. Good marketing and outreach activities are one of the keys to
      achieve this goal.<br> Creating and protecting a good brand aims at rewarding the skills and
      investment of service providers. Having the right to use the brand recognizes that service
      providers are able to extend or provide quality services to the openPASS ecosystem.&nbsp;
    </p>
    <h3>Governance</h3>
    <p>The following governance bodies are defined:</p>
    <ul>
      <li>The Steering Committee</li>
      <li>The General Assembly</li>
      <li>The Architecture Committee</li>
      <li>The Quality Committee</li>
    </ul>
    <h3>Common Dispositions</h3>
    <p>The dispositions below apply to all openPASS bodies, unless otherwise specified. For all
      matters related to membership action, including without limitation meetings, quorum, voting,
      electronic voting action without meeting, vacancy, resignation, or removal, the respective terms set
      forth in the Eclipse Foundation Bylaws apply.</p>
    <h4>Good Standing</h4>
    <p>A representative shall be deemed to be in good standing, and thus eligible to vote on issues
      coming before the body in which he participates, if the representative has attended (in person
      or telephonically) a minimum of three (3) of the last four (4) body meetings (if there have
      been at least four meetings).</p>
    <p>Appointed representatives on the body may be replaced by the member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a
      body member is unavailable to attend or participate in a meeting of the body, he or she may
      be represented by another Body member by providing written proxy to the Body’s mailing list in advance, which shall be included in determining whether the representative is in good standing. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the body upon the termination of the membership of such representative's member organization.</p>
    <h4>Term and Dates of Elections</h4>
    <p>All representatives shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or re-designation of any
      representative following the completion of that representative's term of office.</p>
    <p>Steering Committee member representatives shall serve in such capacity until their removal and replacement by
      their respective appointing member organization or as otherwise provided for in this charter.
    </p>
    <h4>Elected Representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve from
      April 1<sup>st</sup> to March 31<sup>st</sup> of each calendar year, or until their respective
      successors are elected and qualified, or as otherwise provided for in this charter. Procedures
      governing elections of representatives may be established pursuant to resolutions of the
      Steering Committee provided that such resolutions are not inconsistent with any provision of
      this charter.
    </p>
    <h3>Meeting Management</h3>
    <h4>Place of Meetings</h4>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding body. The corresponding body has to inform the representatives
      about the place of meeting fifteen (15) calendar days prior to the meeting. All meetings may be
      held remotely using phone calls, video calls, or any other means as designated from
      time-to-time by resolution of the corresponding body.</p>
    <h4>Regular Meetings</h4>
    <p>No body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representatives in good standing at least fifteen (15) calendar days
      prior to such meeting, which notice will identify all potential actions to be undertaken by
      the body at the body meeting. No representative will be intentionally excluded from body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the body (as
      defined in the Common Dispositions section above) is represented at the body meeting.
      Electronic voting shall be permitted in conjunction with any and all meetings of the body, the
      subject matter of which requires a vote of the body to be delayed until each such
      representative in attendance has conferred with his or her respective member organization as
      set forth in the voting section above.</p>
    <h4>Actions</h4>
    <p>The body may undertake an action only if it was identified in a body meeting notice or
      otherwise identified in a notice of special meeting.</p>
    <h4>Invitations</h4>
    <p>The body may invite any openPASS member to any of its meetings. These invited attendees have
      no right of vote. The corresponding body has to inform the invited attendees about the agenda
      and the place of meeting fifteen (15) calendar days prior to the meeting.</p>
    <h4>Decisions</h4>
    <p>Decisions shall be taken by simple majority vote, unless specified otherwise. The body has a
      quorum if all representatives have properly been invited. Decisions shall be reported in
      writing. Guests do not have any voting rights.</p>
    <h3>General Assembly</h3>
    <h4>Powers and Duties</h4>
    <ul>
      <li>Approve changing the name of openPASS by unanimous vote of the present openPASS WG members</li>
      <li>Decide on dissolution of the openPASS WG by unanimous vote of the present openPASS WG
        members</li>
      <li>The general assembly enables an exchange and discussion among all openPASS WG members</li>
    </ul>
    <h4>Composition</h4>
    <p>Each member of the WG has a seat on the General Assembly.</p>
    <h4>Votes</h4>
    <p>Each Working Group member has one vote.</p>
    <h4>Meeting Management</h4>
    <p>The General Assembly meets at least once every year. The meetings of the General Assembly are
      organized by the Steering Committee.</p>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Steering Committee is required to</p>
    <ul>
      <li>Define the strategy of the WG</li>
      <li>Discuss and amend the charter and the participation agreement</li>
      <li>Define and follow WG marketing and communication activities</li>
      <li>Initiate and execute an annual membership checkup</li>
      <li>Maintain a list of the current members of the openPASS WG</li>
      <li>Define categories to classify WG results in (at least) "Working Group Private" and
        "Working Group Binary"</li>
      <li>Negotiate the annual working group participation fees towards the Eclipse Foundation with
        the EMO</li>
      <li>Approve the annual budget based upon funds received through fees</li>
      <li>WP Assignment, Delivery and Accounting as defined in the section "Collaboration".
        Decisions on this topic have to be made with a two third majority vote.</li>
    </ul>
    <h4>Composition</h4>
    <p>Persons occupying seats within the steering committee must be empowered by their home
      organizations to drive and make decisions as representatives for their home organization
      concerning its relation with the openPASS community.</p>
    <p>Each Driver Member of the WG has a seat in the Steering Committee.</p>
    <p>At least one seat is allocated to each class of members different to Driver Members
      (Participant Members). An additional seat on the committee shall be allocated to the each
      class of members different to Driver Members for every additional five (5) seats beyond one
      (1) allocated to Driver Members. Participant Member seats are allocated following the Eclipse
      "single transferable vote", as defined in the Eclipse Foundation Bylaws.</p>
    <p>The Steering Committee elects among its members a chairman.</p>
    <p>The Steering Committee will serve for a period of one calendar year, or until respective
      successors are elected and qualified, or as otherwise provided for in this charter.</p>
    <h4>Votes</h4>
    <ul>
      <li>Each Driver member on the Steering Committee has three votes</li>
      <li>Other Steering Committee members have a single vote</li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>
    <h3>Architecture Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Architecture Committee members are required to</p>
    <ul>
      <li>Evaluate and define technologies to be applied</li>
      <li>Establish technical guidelines</li>
      <li>Validate new project proposals and concepts</li>
      <li>Establish the openPASS architecture compliance service and ensure its availability to the
        openPASS related Eclipse projects</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Driver Member of the WG has a seat on the Architecture Committee</li>
      <li>Each Project Manager has a seat on the Architecture Committee, provided the Project
        Manager is employed or designated by a WG member. Otherwise, the Project Manager may be
        invited as a guest</li>
      <li>The Architecture Committee elects a chairman who reports to the Steering Committee. This
        chairman is elected among the members of the Architecture Committee</li>
    </ul>
    <h4>Votes</h4>
    <p>Each member on the Architecture Committee has one vote.</p>
    <h4>Meeting Management and Availability</h4>
    <p>The Architecture Committee meets at least twice a year. The Architecture Committee can be
      contacted through its chairman and shall provide answers on related questions within a
      reasonable response time.</p>
    <h3>Quality Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Quality Committee members are required to</p>
    <ul>
      <li>Define the WG quality kit and maturity process</li>
      <li>Establish and maintain the openPASS integration environment</li>
      <li>Establish the openPASS quality assurance service and ensure its availability to the
        openPASS related Eclipse projects</li>
      <li>Provide in-time information to the steering committee on the maturity of the components
        and results delivered to the community to enable it to decide on the acceptance of a
        deliverable as valid contribution</li>
      <li>Validate that the openPASS related Eclipse projects conform to the WG quality kit</li>
      <li>Validate that the openPASS related Eclipse projects conform to the guidelines established
        by the architecture committee</li>
      <li>Validate that the openPASS related Eclipse projects apply the IP process</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Driver member of the WG has a seat on the Quality Committee</li>
      <li>Each Project Manager has a seat on the Quality Committee, provided the Project Manager is
        employed or designated by a WG member. Otherwise, the Project Manager may be invited as a
        guest</li>
      <li>At least one seat is allocated to each class of members different to Driver members. An
        additional seat on the committee shall be allocated to each class of members different
        to Driver members for every additional five (5) seats beyond one (1) allocated to Driver
        members. Participant member seats are allocated following the Eclipse "single transferable
        vote", as defined in the Eclipse Foundation Bylaws</li>
      <li>The Quality Committee elects a chairman who reports to the Steering Committee. This
        chairman is elected among the members of the Quality Committee</li>
    </ul>
    <h4>Votes</h4>
    <p>Each member on the quality Committee has one vote</p>
    <h4>Meeting Management</h4>
    <p>The Quality Committee meets at least twice a year</p>
    <h2>Membership Summary</h2>
    <h3>Membership Fee Structure</h3>
    <p>The following table lists the annual Eclipse Foundation membership fees and the openPASS
      working group fees payable to the Eclipse Foundation Inc. (EMO). Note the working group fees are in addition to the fees for membership in the Eclipse Foundation, which are listed in the <a href="https://www.eclipse.org/membership/#tab-fees">Eclipse Foundation membership fee table</a>.</p>
    <!--  Member Fee Structure Table -->
    <table class="table table-striped">
      <tr>
        <th><strong>Annual Turnover</strong></th>
        <th><strong>WG Fees Driver Member</strong></th>
        <th><strong>WG Fees User Member</th>
        <th><strong>WG Fees Service Provider </th>
      </tr>
      <tr>
        <td><strong>>$250 million </strong></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
      </tr>
      <tr>
        <td>>$100 Million <= $250 million</strong></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
      </tr>
      <tr>
        <td>>$50 million <= $100 million</strong></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
      </tr>
      <tr>
        <td>>$10 million <= $50 million</td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
      </tr>
      <tr>
        <td>< $10 million</td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
      </tr>
      <tr>
        <td>< $1 million, < 10 empoyees</td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
        <td>$ 0<sup>* </sup></td>
      </tr>
    </table>
    <br />
    <p>
      <sup>* </sup>No additional WG fees payable to the EMO are set.
    </p>
    <p>Additionally, each member has to provide annual participation services to the WG, measured in
      manpower units / money:</p>
    <ul>
      <li>Driver Member: 90 manpower units / year + $ 5.000 / year</li>
      <li>All other Member Classes (except Guest Member): 10 manpower units / year</li>
    </ul>
    <h3>Membership Privileges</h3>
    <table class="table table-striped">
      <tr>
        <th><strong>Privilege</strong></th>
        <th><strong>Driver Member</strong></th>
        <th><strong>User Member</strong></th>
        <th><strong>Service Provider Member</strong>
        </th>
        <th><strong>Project Manager</strong></th>
      </tr>
      <tr>
        <td>Steering Committee</td>
        <td>X</td>
        <td>Elected</td>
        <td>Elected</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Architecture Committee</td>
        <td>X</td>
        <td>-</td>
        <td>-</td>
        <td>X</td>
      </tr>
      <tr>
        <td>Quality Committee</td>
        <td>X</td>
        <td>Elected</td>
        <td>Elected</td>
        <td>X</td>
      </tr>
      <tr>
        <td>GeneralAssembly</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
        <td>-</td>
      </tr>
    </table>

    <ul>
      <li>v1.0 created June 17, 2016</li>
      <li>v1.1 November 26, 2020 Updates in support of the Eclipse Foundation corporate restructuring</li>
    </ul>
  </div>
</div>