<?php
/**
 * Copyright (c) 2019, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Olivier Goulet - Deprecate charter
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <div class="alert alert-danger">
      <strong><?php print $pageTitle;?> has been archived.</strong>
    </div>
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 1.1 Revision history is at end of document.</p>
    <h2>Vision and Scope</h2>
    <p>The Eclipse Edge Native working group will drive the evolution and broad adoption
      of technologies related to Edge Computing.  </p>
    <p>The working group will:</p>
    <ul>
      <li>Promote the "Eclipse Edge Native" brand and its value in the marketplace.</li>
      <li>Provide vendor neutral marketing and other services to the Eclipse Edge Native
        ecosystem. </li>
      <li>Leverage Eclipse Foundation defined licensing and intellectual property flows that encourage community participation,
        protect community members, and encourage usage.</li>
      <li>Manage the overall technical and business strategies for Edge Native projects at Eclipse,
        including Eclipse ioFog and Fog05.</li>
      <li>Establish and drive a funding model that enables this working group and its community to
        operate on a sustainable basis. </li>
    </ul>
    <h2>Governance and Precedence</h2>
    <p><strong>Applicable Documents</strong></p>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement,
      including the requirement set forth in Section 2.2 abide by and adhere to the Bylaws and then-current
      policies of the Eclipse Foundation, including but not limited to the Intellectual Property and
      Antitrust Policies. </p>
    <p>In the event of any conflict between the terms set forth in this Working
      Group&#39;s Charter and the Eclipse Foundation Bylaws, Membership Agreement,
      Development Process, Working Group Process, or any policies of the Eclipse
      Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or
      policy shall take precedence.</p>
    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a <a
        href="/membership/become_a_member/membershipTypes.php#contributing"
      >Contributing Member</a> of the Eclipse Foundation. All members must have executed the Eclipse Edge Native Working Group
      Participation Agreement, and adhere to the requirements set forth in this Charter
      to participate.
    </p>
    <p>There are five classes of Eclipse Edge Native working group membership - Strategic,
      Gold, Silver, Committer, and Guest.  Each of these classes is described in detail
      below.  </p>
    <p>The participation fees associated with each of these membership classes is shown in the tables in Schedule A. These are annual fees, and are established by the Eclipse Edge Native Steering Committee, and will be updated in this charter document accordingly.</p>
    <p>
      The fees associated with membership-at-large in the Eclipse Foundation are separate from any
      working group membership fees, and are decided as described in the <a
        href="/org/documents/eclipse_foundation-bylaws.pdf"
      >Eclipse Foundation Bylaws</a> and detailed in the <a
        href="/org/documents/eclipse_membership_agreement.pdf"
      >Eclipse Foundation Membership Agreement</a>.
    </p>
    <h3>Classes of Eclipse Edge Native membership</h3>
    <h4>Strategic members</h4>
    <p>Strategic Members are organizations that view Edge technology as strategic to their
      organization and are investing significant resources to sustain and shape the activities of
      this working group.</p>
    <p>Strategic Members of the Eclipse Edge Native working group must be at least a
    Contributing Member of the Eclipse Foundation, and at least 2 developers participating on
      Edge-related Eclipse Foundation projects.</p>
    <p>Strategic Members of the Eclipse Edge Native working group are required to commit
      to strategic membership for a three (3) year period. </p>
    <h4>Gold members</h4>
    <p>Gold Members are typically organizations that view Edge technology as a critical
      part of their organization&#39;s business operations. These organizations want to support the
      development and evolution of the Edge Computing technology ecosystem.</p>
    <p>Gold Members of the Eclipse Edge Native working group must be at least Contributing
      Members of the Eclipse Foundation.</p>
    <h4>Silver members</h4>
    <p>Silver Members are typically organizations that deliver products or services based
      on Edge technology. These organizations want to participate in the development of the Edge
      Computing technology ecosystem.</p>
    <p>Silver Members of the Eclipse Edge Native working group must be at least Contributing
      Members of the Eclipse Foundation.</p>
    <h4>Committer members</h4>
    <p>
      Committer are individuals who through a process of meritocracy defined by the Eclipse
      Development Process are able to contribute and commit code to the Eclipse Foundation projects
      included in the scope of this working group. Committers may become members of the working
      group by virtue of working for a member organization, or may choose to complete the
      membership-at-large process independently if they are not, along with completing the Eclipse
      Edge Native Working Group Participation Agreement. For further explanation and details, see the
      <a href="/membership/become_a_member/committer.php">Eclipse Foundation Committer
        Membership</a> page.
    </p>
    <h4>Guest members</h4>
    <p>Guest Members are organizations which are Associate members of the Eclipse
      Foundation who have been invited for one year, renewable, by the Steering Committee to
      participate in particular aspects of the activities of the Working Group. Typical guests
      include JUGs, R&amp;D partners, universities, academic research centers, etc. Guests may be
      invited to participate in committee meetings at the invitation of the respective committee,
      but under no circumstances do Guest members have voting rights. Guest members are required to
      execute the Eclipse Edge Native Working Group Participation Agreement.</p>
    <h3>Membership Summary</h3>
    <table class="table table-bordered">
      <tbody>
        <tr>
          <td> </td>
          <td>
            Strategic Member
          </td>
          <td>
            Gold Member
          </td>
          <td>
            Silver Member
          </td>
          <td>
            Committer Member
          </td>
          <td>
            Guest Member
          </td>
        </tr>
        <tr>
          <td>
            Member of the Steering Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
        </tr>
        <tr>
          <td>
            Member of the Marketing Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            Invitation Only
          </td>
        </tr>
      </tbody>
    </table>
    <p>All matters related to Membership in the Eclipse Foundation and this Eclipse Edge Native
      working group will be governed by the Eclipse Foundation Bylaws, Membership Agreement and
      Working Group Process. These matters include, without limitation, delinquency, payment
      of dues, termination, resignation, reinstatement, and assignment.</p>

    <h2>Governance</h2>
    <p>This Eclipse Edge Native working group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the following governance bodies have been
      defined (each a "Body"):</p>
    <ul>
      <li>The Steering Committee</li>
      <li>The Marketing and Brand Committee</li>
    </ul>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this
        working group. </li>
      <li>Define and manage a process for projects outside of the Eclipse Foundation to be included
        in the Eclipse Edge platform.</li>
      <li>Define and manage the roadmaps.</li>
      <li>Review and approve this charter.</li>
      <li>Review and approve the trademark and branding policy.</li>
      <li>Define the annual fees for all classes of working group membership.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Invite Guest members to participate in the working group.</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Steering Committee.</li>
      <li>At least two seats are allocated to Gold Members. Gold Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse
        Bylaws.</li>
      <li>At least one seat is allocated to Silver Members. Silver Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse
        Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. Committer Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse
        Bylaws.</li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>
    <h3>Marketing and Brand Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Marketing and Brand Committee members are required to:</p>
    <ul>
      <li>Define the trademark and branding policy to be used by all Eclipse Edge Native-related
        projects and programs, and refer it for approval by the Steering Committee.</li>
      <li>Ensure the consistency of logo usage and other marketing materials</li>
      <li>Define and implement marketing and communication activities for the working group</li>
      <li>Define and implement developer outreach programs</li>
      <li>Provide requirements to the Eclipse Foundation for conferences and events related to
        Eclipse Edge Native</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Strategic Member of the working group has a seat on the Marketing Committee.</li>
      <li>At least two seats are allocated to Gold Members. Gold Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse
        Bylaws.</li>
      <li>At least one seat is allocated to Silver Members. Silver Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse
        Bylaws.</li>
      <li>At least one seat is allocated to Committer Members. Committer Member seats are allocated
        following the Eclipse "Single Transferable Vote", as defined in the Eclipse
        Bylaws.</li>
      <li>Guest members that have been invited by the Eclipse Edge Native Steering Committee as
        observers.  Guest members have no voting rights.</li>
      <li>Any additional individuals as designated from time to time by the Executive Director.</li>
      <li>The Committee elects a chair who reports to the Steering Committee. This chair is elected
        among the members of the Committee. They will serve from April 1 to March 31 of each
        calendar year, or until their successor is elected and qualified, or as otherwise provided
        for in this Charter.</li>
    </ul>
    <h4>Meeting Management</h4>
    <p>Marketing Committee meets at least once per quarter.</p>
    <h3>Common Dispositions</h3>
    <p>The dispositions below apply to all governance bodies for this working group,
      unless otherwise specified. For all matters related to membership action, including without
      limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws
      apply.</p>
    <h4>Good Standing</h4>
    <p>A representative shall be deemed to be in Good Standing, and thus eligible to vote
      on issues coming before the Body they participate in, if the representative has attended (in
      person or telephonically) a minimum of three (3) of the last four (4) Body meetings (if there
      have been at least four meetings). Appointed representatives on the Body may be replaced by
      the Member organization they are representing at any time by providing written notice to the
      Steering Committee. In the event a Body member is unavailable to attend or participate in a
      meeting of the Body, they may be represented by another Body member (i.e., by proxy) by providing written notice to the Body’s mailing list in advance, which shall be included in determining whether the representative is in Good Standing. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the Body upon the termination of the membership of such representative's Member organization.</p>
    <h4>Voting</h4>
    <p>
      <strong>Super Majority</strong>
    </p>
    <p>For actions (i) approving or changing the name of the working group; and (ii) approving
      changes to annual Member contribution requirements; any such actions must be approved by no
      less than two-thirds (2/3) of the representatives in Good Standing represented at a committee
      meeting at which a quorum is present.</p>
    <h4>Term and Dates of elections</h4>
    <p>This section only applies to the Steering Committee and the Marketing Committee.</p>
    <p>All representatives shall hold office until their respective successors are
      appointed or elected, as applicable. There shall be no prohibition on re-election or
      re-designation of any representative following the completion of that representative's
      term of office.</p>
    <p>
      <strong>Strategic Members</strong>
    </p>
    <p>Strategic Members Representatives shall serve in such capacity on committees until
      the earlier of their removal and replacement by their respective appointing Member organization or as
      otherwise provided for in this Charter.</p>
    <p>
      <strong>Elected representatives</strong>
    </p>
    <p>Elected representatives shall each serve one-year terms and shall be elected to
      serve from April 1 to March 31 of each calendar year, or until their respective successors are
      elected and qualified, or as otherwise provided for in this Charter. Procedures governing
      elections of Representatives may be established pursuant to resolutions of the Steering
      Committee provided that such resolutions are not inconsistent with any provision of this
      Charter.</p>
    <h4>Meetings Management</h4>
    <p>
      <strong>Place of meetings</strong>
    </p>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding Body. All meetings may be held remotely using phone calls,
      video calls or any other means as designated from time-to-time by resolution of the
      corresponding Body.</p>
    <p>
      <strong>Regular meetings</strong>
    </p>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representatives in Good Standing at least fifteen (15) calendar days
      prior to such meeting, which notice will identify all potential actions to be undertaken by
      the Body at the Body meeting. No representative will be intentionally excluded from Body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the Body is
      represented at the Body meeting. Electronic voting shall be permitted in conjunction with any
      and all meetings of the Body the subject matter of which requires a vote of the Body to be
      delayed until each such representative in attendance thereat has conferred with his or her
      respective Member organization as set forth in Section Voting above.
    </p>
    <p>
      <strong>Actions</strong>
    </p>
    <p>The Body may undertake an action only if it was identified in a Body Meeting notice
      or otherwise identified in a notice of special meeting.</p>
    <h4>Invitations</h4>
    <p>The Body may invite any member to any of its meetings. These invited attendees have
      no right of vote.</p>
    <h3>Edge Native Working Group Annual Participation Fees Schedule A</h3>
    <p>The following fees have been established by the Edge Native Steering Committee. These fees are in addition to each participant's membership fees in the Eclipse Foundation.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="55%">Corporate Revenue</th>
          <th class="text-center" width="15%">Strategic Annual Fees</th>
          <th class="text-center" width="15%">Gold Annual Fees</th>
          <th class="text-center" width="15%">Silver Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than $1&nbsp;billion</td>
          <td class="text-center">$50,000</td>
          <td class="text-center">$25,000</td>
          <td class="text-center">$10,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $500 million but less than or equal to $1&nbsp;billion</td>
          <td class="text-center">$30,000</td>
          <td class="text-center">$15,000</td>
          <td class="text-center">$7,500</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $100 million but less than or equal to $500&nbsp;million</td>
          <td class="text-center">$15,000&nbsp;</td>
          <td class="text-center">$10,000&nbsp;</td>
          <td class="text-center">$5,000&nbsp;</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than $10 million but less than or equal to $100&nbsp;million</td>
          <td class="text-center">$10,000&nbsp;</td>
          <td class="text-center">$7,500&nbsp;</td>
          <td class="text-center">$3,750&nbsp;</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to $10&nbsp;million</td>
          <td class="text-center">$5,000</td>
          <td class="text-center">$3,750</td>
          <td class="text-center">$2,500</td>
        </tr>
        <tr style="border-bottom: 1px solid #ddd;">
          <td>Annual Corporate Revenues less than $1&nbsp;million and &lt; 10 employees</td>
          <td class="text-center">$1,500</td>
          <td class="text-center">$1,000</td>
          <td class="text-center">$0</td>
        </tr>
      </tbody>
    </table>
    <p>Committer members and Guest members pay no participation fees.</p>
    <p class="margin-top-20"><strong>Charter Version History</strong></p>
    <ul>
      <li>v1.1 Update in support of the Eclipse Foundation corporate restructuring</li>
      <li>v1.0 Fee Schedule as per Steering Committee resolution June 25, 2020</li>
      <li>v0.5 Errata Adjustments (remove incorrect Voting URL), etc.</li>
      <li>v0.4  Ratified by Edge Native Steering Committee January 2020</li>
      <li>v0.3 update December, 2019 (Rename to Edge Native)</li>
      <li>v0.2 update March 29, 2019</li>
      <li>v0.1 created December 17, 2018</li>
    </ul>
  </div>
</div>
