<?php
/**
 * Copyright (c) 2014, 2015, 2016, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>
      July 2021. Version v1.4. Revision history is at end of document.
    </p>
    <h2>Definitions</h2>
    <p>
      <strong>ASAM ODS application model</strong><br> The ASAM e.V. (see <a
        href="http://www.asam.net"
      >http://www.asam.net</a>) defines a standard for measured data management (ASAM ODS),
      including means for the definition of a domain specific data model, a physical storage model
      and interfaces for handling measured data and their context. The domain specific data model is
      referred to as "ASAM ODS application model".
    </p>
    <p>
      <strong>Bodies</strong><br> Gremia, Working Groups etc.
    </p>
    <p>
      <strong>BSD-like</strong><br> Similar to the definitions taken by the BSD (Berkeley Systems
      Distribution) licensing conditions
    </p>
    <p>
      <strong>WG quality kit</strong><br> Open Measured Data Management deliverables / results as
      well as Requirement Packages have to meet quality requirements and are to be delivered under
      defined conditions. The WG quality kit defines quality criteria and methods for their delivery
      and evaluation to provide a secure means to determine if the Open Measured Data Management
      deliverables / results can be accepted by the client or the service providers.
    </p>
    <p>
      <strong>LGPL</strong><br> Lesser GNU public license
    </p>
    <p>
      <strong>Eclipse Members</strong><br> Eclipse Members have signed the Eclipse Foundation Membership
      Agreement and have paid their membership fees. For a complete overview over the Eclipse
      Membership process and the current Eclipse members refer to the <a href="https://eclipse.org">Eclipse
        Home Page</a>.
    </p>
    <p>
      <strong>Open Measured Data Management Members</strong><br> Open Measured Data Management WG
      members have signed the Open Measured Data Management WG participation agreement and have paid
      the working group participation fee.
    </p>
    <p>
      <strong>Open Measured Data Management WG deliverables and results</strong><br> As part of
      their collaboration, the members of the Open Measured Data Management Eclipse WG agree on the
      delivery of services and goods (software, documents). Those are referred to in this document
      as Open Measured Data Management WG deliverables / results.
    </p>
    <p>
      <strong>Open Measured Data Management Eclipse projects</strong><br> The Eclipse foundation
      provides a framework for defining, driving and executing projects. As "Open Measured Data
      Management Eclipse projects" the Eclipse projects defined and driven by the Open Measured Data
      Management WG referred to.
    </p>
    <p>
      <strong>Open Measured Data Management WG integration environment</strong><br> As a testing
      reference as well as for demonstration purposes, the Open Measured Data Management WG defines
      and maintains an environment of components, configurations and test data.
    </p>
    <p>
      <strong>Open Measured Data Management WG participation agreement</strong><br> Eclipse members
      have to sign the Open Measured Data Management WG participation agreement to participate in
      the Open Measured Data Management WG.
    </p>
    <p>
      <strong>Open Measured Data Management WG Operational Rules</strong><br> The operational rules
      define the details for the collaboration for the WG members. They are defined and published by
      corresponding bodies of the Open Measured Data Management WG.
    </p>
    <p>
      <strong>Requirements package</strong><br> As part of their collaboration, the members of the
      Open Measured Data Management WG agree on the delivery of services and goods (software,
      documents). Those are referred to in this document as Open Measured Data Management
      deliverables / results (see above). The corresponding specification provided by the client is
      referred to as "Requirements Package". Requirements packages are subject to the WG quality kit
      (see above).
    </p>
    <p>
      <strong>Result package</strong><br> See Open Measured Data Management deliverables / results
    </p>
    <p>
      <strong>Service package</strong><br> See Open Measured Data Management deliverables / results
    </p>
    <h2>Goals and Vision</h2>
    <p>The automotive and other engineering industries are driven by continuous product development
      processes where several partners collaborate in different steps of the process. Almost every
      development phase contains testing of components, subsystems or final products. Usually
      testing is done by computer assistance via automatic measurement and automation systems.</p>
    <p>The bulks of test results which are created are tremendous and are growing constantly due to
      a growing variance of products, a rising number of functions and advancements in measurement
      techniques. The management of the generated measurement data is a big challenge and must meet
      several important requirements:</p>
    <p>
      <strong>Innovation</strong>: Product development is driven by a constant innovative progress
      which cover not only the product features but also the development methods and tools.
      Measurement data management solutions must ensure to cope with innovative methods and
      environments.
    </p>
    <p>
      <strong>Process Integrity and Data Exchange</strong>: According to the distributed product
      development exchange of data is a crucial issue for collaboration. Test results have to be
      passed seamlessly along the process flows between organizations (internal departments or
      external organizations) and between engineering disciplines. It is therefore a matter of the
      tools used to use open and standardized interfaces with respect to measured data management.
    </p>
    <p>
      <strong>Long Term Availability</strong>: Test results and measured data document the features
      and functions of products and are the basis for legal approval. They are for that reason
      precious for the manufacturer and have to be stored carefully throughout the whole product
      lifecycle and beyond. Periods of 30 years are therefore not unusual.
    </p>
    <p>
      <strong>Reusability and Traceability</strong>: Secure and safe storage is not sufficient in
      the long run. Beyond that test results must be ready for reuse. That means not only the used
      data formats have to be readable but also the context of the data acquisition has to be
      meaningful for future use to be able to aggregate or compare test results under variable
      aspects. Traceability has to be ensured as it is postulated by quality assurance. Finally,
      reuse of test results helps save testing costs.
    </p>
    <p>
      <strong>Legal requirements</strong>: Legal and regulatory requirements are changing from time
      to time and from country to country for the automotive industry. Changes have to be covered by
      measurement data management over periods of even 30 years.
    </p>
    <p>
      <strong>Security</strong>: As the recent product development and testing processes take place
      in an environment of various organizations with a dynamic distribution of tasks and
      responsibilities, measured data management solutions have to meet versatile security
      requirements to protect data from unauthorized access and modification not affecting the
      efficient flow of data within the product processes. At the same time, "dangling" data (e.g.
      data, which has lost its relevance or correctness of which is in doubt) represents a risk for
      the owner and has to be identified and eliminated.
    </p>
    <p>
      <strong>Resource economy</strong>: Test results consume huge amounts of storage resources.
      Even for the performing operation and maintenance of the storage systems, but also for cost
      control it is crucial to be able to take lifecycle decisions (migration, deletion). This is
      possible only on the base of a data management methodology.
    </p>
    <h2>
      <strong>Scope and Core Domains</strong>
    </h2>
    <p>
    The open MDM Working group (Open Measured Data Management WG) wants to foster and support an
    open and innovative eco-system providing tools and systems, qualification kits and adapters for
    standardized and vendor independent management of measurement data in accordance with the ASAM
    ODS standard.
    </p>
    <p>
    In particular, the Open Measured Data Management WG will
    </p>
    <ul>
      <li>Define requirements for the development of the Open Measured Data Management core system
        and its related components</li>
      <li>Develop and maintain a standardized, generic business object model detailed by the ASAM
        ODS application model and the corresponding interface definitions</li>
      <li>Help to create, fund and oversee Eclipse projects that provide the necessary software
        components</li>
      <li>Provide the resources for managing the quality and the maturity of these components
        throughout the life-cycle</li>
      <li>Ensure open innovation through the sharing of the research, development, and maintenance
        efforts as far as possible</li>
      <li>Foster exchanges between academics, standardization organizations, industry partners and
        community</li>
      <li>Provide and maintain methods and best practices for standardized and vendor independent
        measurement data management</li>
    </ul>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents and Processes</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li> 
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
    <p>In the event of any conflict between the terms set forth in this working group's charter and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the respective Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>
    <p>The Open Measured Data Management WG will enforce the use of the Eclipse Foundation processes
      where applicable.</p>

    <h3>Collaboration</h3>
    <p>Collaboration of the members is crucial for the success of a vital Open Measured Data
      Management WG. The WG commits itself to cooperation applying principles of openness,
      transparency and meritocracy. Every member will fulfill its commitment to best knowledge and
      conscience.</p>
    <p>The collaboration duties of the Open Measured Data Management WG include, but are not limited
      to</p>
    <ul>
      <li>Annual Plan containing themes, marketing, outreach</li>
      <li>Definition of requirement packages and services modules</li>
      <li>Valuation of the defined packages with respect to estimated man power units, content and
        quality for their delivery</li>
      <li>Assignment of requirement packages and / or service modules to the WG members according to
        their Open Measured Data Management WG membership fees</li>
      <li>If there are no open membership fees left, voluntary donations of man power units by the
        Open Measured Data Management WG members are welcome as well. Otherwise the required
        packages won't be processed.</li>
      <li>The execution or delivery of the assigned packages are fulfilled independently by the
        respective member under its full responsibility and on its own expenses. The Open Measured
        Data Management WG will not act on behalf of its members.</li>
      <li>Acceptance of the corresponding services or results delivered by the Open Measured Data
        Management WG members.</li>
      <li>Monitoring the quality of the deliverables</li>
    </ul>
    <p>The collaboration process is pointed out for better understanding in annex 1 (chart).</p>
    <h3>Working Group Participation</h3>
    <p>In order to participate in the Open Measured Data Management WG, an entity must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation, have executed the Open Measured Data Management WG Participation Agreement, and adhere to the requirements set forth in this charter. The Eclipse Contributing Member fees are determined as described in the Eclipse Foundation Bylaws and detailed in the Eclipse Membership Agreement.</p>
    <p>The Open Measured Data Management WG is open at any time to all new members which meet these
      conditions.</p>
    <h4>Classes of Membership</h4>
    <p>The membership classes of the Open Measured Data Management WG are established to reflect the
      different interest situations of the members. The membership class has to be declared by the
      potential member in his Open Measured Data Management WG participation agreement. The
      membership class of each Open Measured Data Management WG member is checked once a year.</p>
    <h5>Driver Members</h5>
    <p>Driver Members want to influence the definition and further development of Open Measured Data
      Management and all its deliverables. They are members of the Steering Committee and invest an
      essential amount of resources to sustain the WG activities. Typical Driver members include
      industry users of the technologies and results provided by Open Measured Data Management WG.
      Most of them operate testing processes as part of their core business.</p>
    <h5>User Members</h5>
    <p>User Members use the technologies and results provided by the Open Measured Data Management
      WG. They want to keep track of the Open Measured Data Management development but do not want
      to influence in an essential way. Most User Members operate testing processes as part of their
      core business.</p>
    <h5>Application Vendor Members</h5>
    <p>Application Vendor Members view Open Measured Data Management as an important part of their
      corporate and product strategy and mainly offer products based on, or with Open Measured Data
      Management. Typical Application Vendor Members provide applications to the market that
      implement or use technologies and results provided by the Open Measured Data Management WG.</p>
    <h5>Service Provider Members</h5>
    <p>Service Provider Members view Open Measured Data Management as an important part of their
      corporate and product strategy and offer services for deployment, development or maintenance
      of Open Measured Data Management components or systems.</p>
    <h5>Guest Members</h5>
    <p>Guest members are organizations who have been invited for one year by the Steering Committee
      of Open Measured Data Management WG to participate in some aspects of the activities of the
      Working Group. Typical Guest Members include R&D partners, academic entities, and potential
      future full-fledged members who want to have a closer look before deciding on their strategy.
      When Guest Members are invited to an Open Measured Data Management WG body for meetings, they
      have no right to vote. Invitations may be renewed by the Steering Committee. Guests Guest
      Members are required to sign the participation agreement.</p>
    <h4>Open Measured Data Management WG Participation Fees</h4>
    <p>
      To operate properly, the Open Measured Data Management WG will request additional services
      from the Eclipse Management Organization (EMO) or execute certain tasks with resources from
      the WG participants. Participation fees are due in addition to the Eclipse membership fees and
      are outlined in the participation agreement.<br>
    </p>
    <p>On an annual basis, participation fees will be decided and published in the WG participation
      document by the Steering Committee to allow for the execution of these services. While the EMO
      services are payable in Euros, the services for internal deliverables can only be provided as</p>
    <ul>
      <li>Delivery of result packages</li>
      <li>Delivery of service packages</li>
    </ul>
    <p>The amount for these fees will be expressed as an equivalent of manpower units (employee
      service days). The corresponding packages or services have to be offered in advance to and
      committed by the steering committee. The initial annual participation fee after the foundation
      for Open Measured Data Management WG driver member is the equivalent of 60 employee service
      days. The initial annual participation fee after the foundation for all other Open Measured
      Data Management WG members is the equivalent of 10 employee service days or free for
      non-profit organizations.</p>
    <h4>Case of Violation</h4>
    <p>Membership is checked annually by the Steering Committee and can be terminated, suspended or
      changed to a different membership class by the Open Measured Data Management WG steering
      committee if the member fails to deliver participation fees or previously committed results or
      acts against the goals of the Open Measured Data Management WG in any other way. Those
      decisions have to be taken by unanimous vote with exception of the affected member.</p>
    <h4>Termination</h4>
    <p>On observing a 4 (four) weeks period of notice each member shall be entitled to terminate its
      participation by giving written notice to the Steering Committee. Delivered contributions are
      not refundable. After termination the respective member is not in charge of any further
      deliveries.</p>
    <h2>Services</h2>
    <h3>Collaboration Infrastructure</h3>
    <p>The Open Measured Data Management WG leverages the standard Eclipse open source collaboration
      infrastructure. As such, source code repositories, Bugzilla, wikis, forums, project mailing
      lists, and other services provided as the open source collaboration infrastructure are
      publicly visible. Committers of Open Measured Data Management WG related Eclipse projects have
      write access to this infrastructure, and as such have the rights and obligations as set forth
      in the Eclipse Development Process and the various Eclipse committer agreements.</p>
    <p>All Open Measured Data Management WG deliverables and results are published to the Eclipse
      open source infrastructure. The standard license shall be the Eclipse Public License (EPL).
      Exceptions to this rule need to be proposed by the Steering Committee and approved by the
      board of directors of the Eclipse Foundation.</p>
    <h3>Requirements Management</h3>
    <p>The requirements on Open Measured Data Management packages will be collected, consolidated
      and assigned to packages, which can be handled by Eclipse projects. This task is fulfilled by
      the requirements management service of the Open Measured Data Management WG based on the
      Eclipse collaboration infrastructure services.</p>
    <h3>Quality Assurance (QA)</h3>
    <p>Open Measured Data Management packages have to be tested with respect to their functional,
      non-functional requirements and their seamless interoperability with the Open Measured Data
      Management integration environment. This environment is maintained by the Open Measured Data
      Management QA service and represents a superset of all components available to the users. The
      QA reports are the basis for the acceptance of the contributed packages by the Steering
      committee.</p>
    <h3>Architecture Compliance</h3>
    <p>Open Measured Data Management concepts and components have to be evaluated with respect to
      their architectural compliance with the Open Measured Data Management business object model
      and the software architecture. The Open Measured Data Management WG provides such a service.</p>
    <h3>Marketing and Branding</h3>
    <p>
      One major success factor for the Open Measured Data Management WG is the adoption of Open
      Measured Data Management technology and a flourishing ecosystem for many. Good marketing and
      outreach activities are one of the keys to achieve this goal.<br> Creating and protecting a
      good brand aims at rewarding the skills and investment of service providers. Having the right
      to use the brand recognizes that service providers are able to extend or provide quality
      services to the Open Measured Data Management ecosystem.
    </p>
    <p>Additional services or service extensions provided by Eclipse can be contracted on demand if
      necessary.</p>
    <h2>Governance</h2>
    <p>The following governance bodies are defined:</p>
    <ul>
      <li>The Steering Committee</li>
      <li>The General Assembly</li>
      <li>The Architecture Committee</li>
      <li>The Quality Committee</li>
      <li>Project patrons</li>
    </ul>
    </p>
    <h3>Common Dispositions</h3>
    <p>The dispositions below apply to all Open Measured Data Management bodies, unless otherwise
      specified. For all matters related to membership action, including without limitation
      meetings, quorum, voting, electronic voting action without meeting, vacancy, resignation, or
      removal, the respective terms set forth in the Eclipse Foundation Bylaws apply.</p>
    <h4>Good Standing</h4>
    <p>A representative shall be deemed to be in good standing, and thus eligible to vote on issues
      coming before the body in which he participates, if the representative has attended (in person
      or telephonically) a minimum of three (3) of the last four (4) body meetings (if there have
      been at least four meetings).</p>
    <p>Appointed representatives on the body may be replaced by the member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a
      body member is unavailable to attend or participate in a meeting of the body, he or she may
      be represented by another Body member by providing written proxy to the Body’s mailing list in advance, which shall be included in determining whether the representative is in good standing. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the body upon the termination of the membership of such
      representative's member organization.</p>
    <h4>Term and Dates of Elections</h4>
    <p>All representatives shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or re-designation of any
      representative following the completion of that representative's term of office.</p>
    <p>Steering Committee member representatives shall serve in such capacity until their removal and replacement by
      their respective appointing member organization or as otherwise provided for in this charter.</p>
    <h4>Elected Representatives</h4>
    <p>Elected representatives shall each serve one-year terms and shall be elected to serve from
      April 1 to March 31 of each calendar year, or until their respective successors are elected
      and qualified, or as otherwise provided for in this charter. Procedures governing elections of
      representatives may be established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this charter.</p>
    <p>Elected representatives are now entitled to act for or on behalf of any member of the Open
      Measured Data Management WG or represent the Open Measured Data Management WG or any of its
      members.</p>
    <h3>Meeting Management</h3>
    <h4>Place of Meetings</h4>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding body. The corresponding body has to inform the representatives
      about the place of meeting fifteen (15) calendar days prior to the meeting. All meetings may be
      held remotely using phone calls, video calls, or any other means as designated from
      time-to-time by resolution of the corresponding body.</p>
    <h4>Regular Meetings</h4>
    <p>No body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representative in good standing at least fifteen (15) calendar days
      prior to such meeting, which notice will identify all potential actions to be undertaken by
      the body at the body meeting. No representative will be intentionally excluded from body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the body (as
      defined in the Common Dispositions section above) is represented at the body meeting.
      Electronic voting shall be permitted in conjunction with any and all meetings of the body, the
      subject matter of which requires a vote of the body to be delayed until each such
      representative in attendance has conferred with his or her respective member organization as
      set forth in the voting section above.</p>
    <h4>Actions</h4>
    <p>The body may undertake an action only if it was identified in a body meeting notice or
      otherwise identified in a notice of special meeting.</p>
    <h4>Invitations</h4>
    <p>The body may invite any Open Measured Data Management member to any of its meetings. These
      invited attendees have no right of vote. The corresponding body has to inform the invited
      attendees about the agenda and the place of meeting fifteen (15) calendar days prior to the
      meeting.</p>
    <h4>Decisions</h4>
    <p>Decisions shall be taken by simple majority vote, unless specified otherwise. The body has a
      quorum if all representatives have properly been invited. Decisions shall be reported in
      writing. Guests do not have any voting rights.</p>
    <h3>General Assembly</h3>
    <h4>Powers and Duties</h4>
    <p>Approve changing the name of Open Measured Data Management by unanimous vote of the present
      Open Measured Data Management WG members.</p>
    <p>Decide on dissolution of the Open Measured Data Management WG by unanimous vote of the
      present Open Measured Data Management WG members.</p>
    <h4>Composition</h4>
    <p>Each member of the WG has a seat on the General Assembly.</p>
    <h4>Votes</h4>
    <p>Each member of the Working Group has one vote.</p>
    <h4>Meeting Management</h4>
    <p>The General Assembly meets at least once every year. The meetings of the General Assembly are
      organized by the Steering Committee.</p>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Steering Committee is required to</p>
    <ul>
      <li>Define the strategy of the WG</li>
      <li>Discuss and amend the charter and the participation agreement</li>
      <li>Define and follow marketing and communication activities</li>
      <li>Popularize and defend the Open Measured Data Management brand</li>
      <li>Initiate and execute an annual membership checkup</li>
      <li>Define and execute a marketing and branding plan</li>
      <li>Maintain a list of the current members of the Open Measured Data Management WG</li>
      <li>Negotiate the annual working group participation fees towards the Eclipse Foundation with
        the EMO</li>
      <li>Approve the annual budget based upon funds received through fees</li>
      <li>WP Assignment, Delivery and Accounting as defined in the section "Collaboration".
        Decisions on this topic have to be made with a two third majority vote.</li>
    </ul>
    <h4>Composition</h4>
    <p>Persons occupying seats within the steering committee must be empowered by their home
      organizations to drive and make decisions as representatives for their home organization
      concerning its relation with the Open Measured Data Management community.</p>
    <p>Each Member excluding Guest Members of the WG has one seat in the Steering Committee.</p>
    <p>The Steering Committee elects among its members a chairman who will represent the WG.</p>
    <p>The Steering Committee will serve for a period of one calendar year, or until respective
      successors are elected and qualified, or as otherwise provided for in this charter.</p>
    <h4>Votes</h4>
    <ul>
      <li>Each member on the Steering Committee has one vote</li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>
    <h3>Architecture Committee</h3>
    <h4>Powers and Duties</h4>
    <p>
      Architecture Committee members are required to<br>
    </p>
    <ul>
      <li>Ensure the functional consistency of Open Measured Data Management projects</li>
      <li>Ensure the non-functional consistency of Open Measured Data Management projects</li>
      <li>Ensure the technical consistency of Open Measured Data Management projects</li>
      <li>Evaluate and define technologies to be applied</li>
      <li>Establish technical guidelines</li>
      <li>Validate new project proposals and concepts</li>
      <li>Establish the Open Measured Data Management architecture compliance service and ensure its
        availability to the Open Measured Data Management projects</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Driver Member of the WG has a seat on the Architecture Committee</li>
      <li>Each project lead has a seat on the Architecture Committee</li>
      <li>The Architecture Committee elects a chairman who reports to the Steering Committee. This
        chairman is elected among the members of the Architecture Committee</li>
    </ul>
    <h4>Votes</h4>
    <p>Each member on the Architecture Committee has one vote.</p>
    <h4>Meeting Management and Availability</h4>
    <p>The Architecture Committee meets at least twice a year. The Architecture Committee can be
      contacted through its chairman and shall provide answers on related questions within a
      reasonable response time.</p>
    <h3>The Quality Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Quality Committee members are required to</p>
    <ul>
      <li>Define the WG quality kit and maturity process</li>
      <li>Establish and maintain the Open Measured Data Management integration environment</li>
      <li>Establish the Open Measured Data Management quality assurance service and ensure its
        availability to the Open Measured Data Management projects</li>
      <li>Provide in-time information to the steering committee on the maturity of the components
        and results delivered to the community to enable it to decide on the acceptance of a
        deliverable as valid contribution</li>
      <li>Validate that the projects conform to the WG quality kit.</li>
      <li>Validate that the projects conform to the guidelines established by the architecture
        committee</li>
      <li>Validate that the projects apply the IP process</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>Each Driver member of the WG has a seat on the Quality Committee</li>
      <li>Each project lead has a seat on the Quality Committee</li>
      <li>At least one seat is allocated to each class of members different to Driver members. An
        additional seat on the committee shall be allocated to the each class of members different
        to Driver members for every additional five (5) seats beyond one (1) allocated to Driver
        members. Participant member seats are allocated following the Eclipse "single transferable
        vote", as defined in the Eclipse Foundation Bylaws.</li>
      <li>The Quality Committee elects a chairman who reports to the Steering Committee. This
        chairman is elected among the members of the Quality Committee.</li>
    </ul>
    <h4>Votes</h4>
    <p>Each member on the quality Committee has one vote.</p>
    <h4>Meeting Management</h4>
    <p>The Quality Committee meets at least twice a year.</p>
    <h3>Open Measured Data Management sponsored Projects</h3>
    <p>Each Open Measured Data Management sponsored Eclipse project has a steering committee member
      as a patron chosen by the Steering Committee. The patron can be considered the stakeholder
      representative of the project, working closely with the project lead to</p>
    <ul>
      <li>ensure that the requirements for the project are well defined and understood</li>
      <li>ensure that the funding for the project is suitable from the perspective of the Open
        Measured Data Management WG</li>
      <li>ensure that the quality goals of the Open Measured Data Management WG are met by the
        project team</li>
    </ul>
    <h3>Participation Fee Structure</h3>
    <p>The following table lists the annual openMDM working group participation fees payable to the Eclipse Foundation. Note the working group participation fees are in addition to the fees for membership in the Eclipse Foundation, which are listed in the <a href="https://www.eclipse.org/membership/#tab-fees">Eclipse Foundation membership fee table</a>.</p>
    <p>For calendar year 2021, the openMDM working group participation fees are €1 500. The annual working group participation fees for 2022 and beyond are €3 500 billed on an annual basis starting January 1, 2022.</p>
    <!--  Participation Fee Structure Table -->
    <table class="table table-striped">
      <tr>
        <th><strong>Membership Class</strong></th>
        <th><strong>WG Fees</strong></th>
      </tr>
      <tr>
        <td>Driver, User, Application Vendor, Service Provider</td>
        <td>€3 500</td>
      </tr>
      <tr>
        <td>Guest Members pay no participation fees</td>
        <td></td>
      </tr>
    </table>
    <br />

    <h3>Membership Privileges</h3>
    <!-- Membership Privilages Table -->
    <table class="table table-striped">
      <tr>
        <th><strong>Privilege</strong></th>
        <th><strong>Driver Member</strong></th>
        <th><strong>Strategic User</strong></th>
        <th><strong>Application Vendor</strong></th>
        <th><strong>Service Provider</strong></th>
        <th><strong>Project Manager</strong></th>
      </tr>
      <tr>
        <td>Steering Committee</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Architecture Committee</td>
        <td>X</td>
        <td></td>
        <td></td>
        <td></td>
        <td>X</td>
      </tr>
      <tr>
        <td>Quality Committee</td>
        <td>X</td>
        <td>Elected</td>
        <td>Elected</td>
        <td>Elected</td>
        <td>X</td>
      </tr>
      <tr>
        <td>General Assembly</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
        <td>X</td>
      </tr>
    </table>
    <br />
    <p>
      Subscribe to <a href="https://accounts.eclipse.org/mailing-list/open-measured-data-wg">Eclipse
        Open Measured Data Management Working Group</a>
    </p>
    <ul>
      <li>V1.4 July 2021: Establishing Participation Fees as per Steering Committee approval June 9, 2021</li>
      <li>V1.3 November 2020: Updates in support of the Eclipse Foundation corporate restructuring</li>
      <li>V1.2 August 1, 2020: WG fee and in-kind contribution adjustments; steering committee composition and voting rights adjustments </li>
      <li>V1.1 July 22, 2019: WG User Member Fees adopted</li>
      <li>v1.0 created June 28, 2016</li>
    </ul>
  </div>
</div>