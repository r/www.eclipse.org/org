<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>v0.9 - Version history is at the end of document</p>
    <h2>Vision and Scope</h2>
    <p>The AsciiDoc Working Group will drive the standardization, adoption, and evolution
      of the AsciiDoc language. This effort will include tools to comprehend, analyze, and transform
      this language for use in creating technical communication encoded in AsciiDoc. Our goal is to
      support the development, deployment, management, and usage of software documentation,
      applications, and services that use AsciiDoc.</p>
    <p>To execute this mission, the Working Group will:</p>
    <ul>
      <li>Foster the design and development of the AsciiDoc language specification, which includes
        the syntax, rules, built-in attributes, Abstract Semantic Graph (ASG), DOM (Document Object
        Model), API and options, conversion model, referencing system, extension SPI, and
        runtime-agnostic technology compatibility kit (TCK), all to ensure interoperability and
        portability of information encoded in AsciiDoc.</li>
      <li>Collaborate with Working Group members, contributors, subject matter experts, and
        community members from multiple domains to select and define capabilities, terminology, and
        syntax for AsciiDoc based on tangible and emerging use cases, requirements, and important
        external developments.</li>
      <li>Support the development of associated technical specifications, compatible
        implementations, tools, and software libraries.</li>
      <li>Encourage an extensible architecture, pattern reuse, interface consistency, ease of use
        and customization, enterprise-level frontend and backend performance, and secure
        programmatic practices.</li>
    </ul>
    <p>Additionally, the Working Group will:</p>
    <ul>
      <li>Promote the AsciiDoc brand and its value in the marketplace.</li>
      <li>Provide vendor neutral marketing and other services to the AsciiDoc ecosystem.</li>
      <li>Leverage the Eclipse Foundation Specification Process to formalize the specifications that
        are developed within the scope of this Working Group.</li>
      <li>Establish compatibility rules&mdash;including a compatibility and branding process for
        specification implementations&mdash;to ensure application portability and usage patterns
        that remain consistent with the AsciiDoc model.</li>
      <li>Leverage Eclipse-defined licensing and intellectual property flows that encourage
        community participation, protect community members, and promote usage of AsciiDoc and its
        ecosystem.</li>
      <li>Manage the overall technical and business strategies of AsciiDoc and its related open
        source projects.</li>
      <li>Establish and drive a funding model that enables this Working Group and its community to
        operate on a sustainable basis.</li>
    </ul>

    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>.</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to follow the Bylaws and then-current policies of the Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>

    <p>In the event of any conflict between the terms set forth in this Working Group&#39;s Charter and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the respective Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>
    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a <a
        href="/membership/become_a_member/membershipTypes.php#contributing"
      >Contributing Member</a> of the Eclipse Foundation, have executed the AsciiDoc
      Participation Agreement, and adhere to the requirements set forth in this Charter
      to participate.
    </p>
    <p>There are three classes of AsciiDoc Working Group membership - Partner, Commiter,
      and Guest. Each of these classes is described in detail below.</p>
    <p>The participation fees associated with each of these membership classes are shown
      in the Annual Participation Fees section. These are annual fees, and are established by the
      AsciiDoc Steering Committee, and will be updated in this charter document accordingly.</p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any Working
      Group membership fees, and are decided as described in the <a
        href="/org/documents/eclipse_foundation-bylaws.pdf"
      >Eclipse Foundation Bylaws</a> and detailed in the <a
        href="/org/documents/eclipse_membership_agreement.pdf"
      >Eclipse Foundation Membership Agreement</a>.
    </p>
    <h2>Classes of AsciiDoc Membership</h2>
    <h3>Partner members</h3>
    <p>Partner Members are organizations that view open technical communication standards
      and technologies as strategic to their organization and are investing significant resources to
      sustain and shape the activities of this Working Group. These organizations need open
      technical communication standards and technologies to deliver their products or services and
      want to participate in the development of an open technical communication ecosystem.&nbsp;</p>
    <p>Partner Members of the AsciiDoc Working Group must be at least Contributing Members of
      the Eclipse Foundation.</p>

    <h3>Committer members</h3>
    <p>
      Committer Members are individuals who through a process of meritocracy defined by the Eclipse
      Foundation Development Process are able to contribute and commit content or code to the Eclipse
      Foundation projects included in the scope of this Working Group. Committers may be members by
      virtue of working for a member organization, or may choose to complete the membership process
      independently if they are not. For further explanation and details, see the <a
        href="/membership/become_a_member/committer.php"
      >Eclipse Foundation Committer Membership</a> page.
    </p>
    <h3>Guest members</h3>
    <p>Guest Members are organizations which are Associate members of the Eclipse
      Foundation who have been invited for one year, renewable, by the Steering Committee to
      participate in particular aspects of the activities of the Working Group. Typical guests
      include R&amp;D partners, universities, academic research centers, etc. Guests may be invited
      to participate in committee meetings at the invitation of the respective committee, but under
      no circumstances do Guest members have voting rights. Guest members are required to execute
      the AsciiDoc Participation Agreement.</p>
    <h3>Membership Summary</h3>
    <div>
      <table class="table table-stripped">
        <thead>
          <tr>
            <th>&nbsp;</th>
            <th class="text-center">
              Partner Member
            </th>
            <th class="text-center">
              Committer Member
            </th>
            <th class="text-center">
              Guest Member
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              Member of the Steering Committee
            </td>
            <td class="text-center">
              Appointed
            </td>
            <td class="text-center">
              Elected
            </td>
            <td class="text-center">
              N/A
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <h2>Governance</h2>
    <p>This Working Group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the Steering Committee has been defined
      (also referred to as the &quot;Body&quot; below) as described below:</p>
    <ul>
      <li>
        <p>The Steering Committee must fulfill the responsibilities of the Specification
          Committee as defined in the Eclipse Foundation Specification Process.</p>
      </li>
    </ul>
    <h2>Steering Committee</h2>
    <h3>Powers and Duties</h3>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the Working Group.</li>
      <li>Review and approve this charter.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this
        Working Group, and for those projects that are specification projects, they agree to adhere
        to the Eclipse Foundation Specification Process.</li>
      <li>Define and approve the specification process to be used by the AsciiDoc specifications.</li>
      <li>Ensure that all software projects and specification projects operate in an open,
        transparent, and vendor-neutral fashion in compliance with the Eclipse Development Process
        and Eclipse Foundation Specification Process, respectively.</li>
      <li>Work with the AsciiDoc Project Management Committee (AsciiDoc PMC) to ensure that the
        specification process is complied with by all AsciiDoc specification projects.</li>
      <li>Approve specifications for adoption by the community. In this context, the Steering
        Committee takes the role of the Specification Committee for purposes of implementing the
        Eclipse Foundation Specification Process.</li>
      <li>Define and manage the roadmaps.</li>
      <li>Review and approve the trademark policy to ensure compatibility of independent
        implementations of specifications.</li>
      <li>Define the trademark and branding policy to be used by the AsciiDoc specification.</li>
      <li>Ensure the consistency of logo usage and other marketing materials.</li>
      <li>Define the annual fees for all classes of Working Group membership.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Invite Guest members to participate in the Working Group.</li>
    </ul>
    <h3>Composition</h3>
    <ul>
      <li>Each Partner Member of the Working Group has one appointed seat on the Steering Committee.</li>
      <li>Three elected seats are allocated to Committer Members. Committer Member seats are
        allocated following the Eclipse Foundation &quot;Single Transferable Vote&quot;, as defined in the
        Eclipse Foundation Bylaws.</li>
    </ul>
    <h3>Meeting Management</h3>
    <p>The Steering Committee meets at least twice a year.</p>
    <h2>Common Dispositions</h2>
    <p>The dispositions below apply to all governance bodies for this Working Group,
      unless otherwise specified. For all matters related to membership action, including without
      limitation: meetings, quorum, voting, electronic voting action without meeting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws
      apply.</p>
    <h3>Good Standing</h3>
    <p>A representative shall be deemed to be in Good Standing, and thus eligible to vote on issues coming before the Body they participate in, if the representative has attended (in person or telephonically) a minimum of three (3) of the last four (4) Body meetings (if there have been at least four meetings). Appointed representatives on the Body may be replaced by the Member organization they are representing at any time by providing written notice to the Steering Committee. In the event a Body member is unavailable to attend or participate in a meeting of the Body, they may be represented by another Body member by providing written proxy to the Body’s mailing list in advance, and which shall be included in determining whether the representative is in Good Standing. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the Body upon the termination of the membership of such representative&rsquo;s Member organization.</p>
    <h3>Voting</h3>
    <p>For actions (i) requesting that the Eclipse Foundation Board of Directors approve a
      specification license; (ii) approving specifications for adoption; (iii) approving or changing the name of the
      Working Group; and (v) approving changes to annual Member contribution requirements, including annual fees; any such
      actions must be approved by no less than two-thirds (2/3) of the representatives in Good
      Standing represented at a committee meeting at which a quorum is present.</p>
    <h3>Term and Dates of Elections</h3>
    <p>All representatives shall hold office until their respective successors are
      appointed or elected, as applicable. There shall be no prohibition on re-election or
      re-designation of any representative following the completion of that representative&rsquo;s
      term of office.</p>
    <h4>Partner Members</h4>
    <p>Partner Member Representatives shall serve in such capacity on committees until the
      earlier of their removal and replacement by their respective appointing Member organization or as otherwise
      provided for in this Charter.</p>
    <h4>Elected representatives</h4>
    <p>Elected representatives shall each serve one-year terms and shall be elected to
      serve from April 1 to March 31 of each calendar year, or until their respective successors are
      elected and qualified, or as otherwise provided for in this Charter. Procedures governing
      elections of Representatives may be established pursuant to resolutions of the Steering
      Committee provided that such resolutions are not inconsistent with any provision of this
      Charter.</p>
    <h3>Meetings Management</h3>
    <h4>Place of meetings</h4>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding Body. All meetings may be held remotely using phone calls,
      video calls or any other means as designated from time-to-time by resolution of the
      corresponding Body.</p>
    <h4>Regular meetings</h4>
    <p>No Body meeting will be deemed to have been validly held unless a notice of same
      has been provided to each of the representatives in Good Standing at least ten (10) calendar
      days prior to such meeting, which notice will identify all potential actions to be undertaken
      by the Body at the Body meeting. No representative will be intentionally excluded from Body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the Body is
      represented at the Body meeting. Electronic voting shall be permitted in conjunction with any
      and all meetings of the Body the subject matter of which requires a vote of the Body to be
      delayed until each such representative in attendance thereat has conferred with his or her
      respective Member organization as set forth in Section Voting above.</p>
    <h4>Actions</h4>
    <p>The Body may undertake an action only if it was identified in a Body Meeting notice
      or otherwise identified in a notice of special meeting.</p>
    <h3>Invitations</h3>
    <p>The Body may invite any member to any of its meetings. These invited attendees have
      no right of vote.</p>
    <h2>AsciiDoc Working Group Annual Participation Fees</h2>
    <p>The AsciiDoc Working Group shall not charge members any fees in 2020. The Steering
      Committee will establish a fee structure; fees will be due beginning with calendar year 2021.
      The fees are based on the resource requirements needed to achieve the group&#39;s objectives,
      to be described in the working group&rsquo;s program plan and budget.</p>
    <h3>AsciiDoc Partner Member Annual Participation Fees</h3>
    <p>Partner members are required to execute the AsciiDoc Participation
      Agreement. Partner members will pay no participation fees in 2020.</p>

    <table class="table table-stripped" cellspacing="0">
      <thead>
		    <tr>
		    	<th>
		    	<p>Corporate Revenue</p>
		    	</th>
		    	<th>
		    	<p>Annual Fees</p>
		    	</th>
		    </tr>
      </thead>
    <tbody>
		  <tr>
		  	<td>
		  	<p>Annual Corporate Revenues greater than $1 billion</p>
		  	</td>
		  	<td class="text-center">
		  	<p>$10,000</p>
		  	</td>
		  </tr>
		  <tr>
		  	<td>
		  	<p>Annual Corporate Revenues greater than $100 million but less than or equal to $1 billion</p>
		  	</td>
		  	<td class="text-center">
		  	<p>$8,000</p>
		  	</td>
		  </tr>
		  <tr>
		  	<td>
		  	<p>Annual Corporate Revenues greater than $10 million but less than or equal to $100 million</p>
		  	</td>
		  	<td class="text-center">
		  	<p>$6,000</p>
		  	</td>
		  </tr>
		  <tr>
		  	<td>
		  	<p>Annual Corporate Revenues less than or equal to $10 million</p>
		  	</td>
		  	<td class="text-center">
		  	<p>$4,000</p>
		  	</td>
		  </tr>
		  <tr>
		  	<td>
		  	<p>Annual Corporate Revenues less than $1 million and < 10 employees</p>
		  	</td>
		  	<td class="text-center">
		  	<p>$1,500</p>
		  	</td>
		  </tr>
	  </tbody>
  </table>

    <h3>AsciiDoc Committer Member Annual Participation Fees</h3>
    <p>Committer members pay no annual fees, but are required to execute the AsciiDoc Participation Agreement.</p>
    <h3>AsciiDoc Guest Member Annual Participation Fees</h3>
    <p>Guest members pay no annual fees, but are required to execute the AsciiDoc
      Participation Agreement.</p>
    <hr />
    <p>Charter Version History</p>
    <ul>
      <li>v0.1 created April 28, 2019.</li>
      <li>v0.2 updated August 1, 2019.&nbsp; Minor edits&nbsp;</li>
      <li>v0.3 updated February 7, 2020. Update of fees</li>
      <li>v0.4 updated April 4, 2020.&nbsp; Minor edits</li>
      <li>v0.5 updated June 4, 2020. Eliminate Strategic Member class. Change Participant Member
        class name to Partner Member. Each Partner Member appoints one representative to the
        Steering Committee. Eliminate 2020 membership fees. Reserve three Steering Committee seats
        for Committer Members. Update meeting notice from 30 days to 10 days</li>
      <li>v0.6 updated June 16, 2020. Clarify that basis for fees must be described in the working
        group&rsquo;s plan and budget. Reiterate that the Steering Committee&rsquo;s
        responsibilities include those of the Specification Committee as defined by the Eclipse
        Foundation Specification Process.</li>
      <li>v0.7 updated July 21, 2020. Charter ratified by AsciiDoc Steering Committee</li>
      <li>v0.8 updated December 21, 2020. Steering Committee approval of annual participation fee schedule</li>
      <li>v0.9 - Updates in support of the Eclipse Foundation corporate restructuring</li>
    </ul>
  </div>
</div>