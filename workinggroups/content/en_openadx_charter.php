<?php
/**
 * Copyright (c) 2019, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *   Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>
<div class="alert alert-danger">
  <strong><?php print $pageTitle;?> has been archived.</strong>
</div>
<p>Version v1.2. Revision history is at end of document.</p>
<h2>Vision and Scope</h2>
<p>
  Autonomous driving (AD) is an extremely complex challenge with a complex development process,
  which is slowed at every turn by incompatibility between widely used development tools. The tools
  we already use in and of themselves are very good, but they do not work seamlessly with one
  another simply because they were never designed to work with each other.<br /> This is an
  industry-wide issue. &nbsp;It slows us all down in the race to automated driving. It costs money
  and precious resources to reduce the problem to a manageable level. And the thing about it is that
  the tool chain itself is not something we can monetize.
</p>
<p>All major stakeholders/users have issues associated with tool incompatibility, data conversion
  and lost time.</p>
<p>We lessen this problem by creating the leading automated driving ecosystem &#10146; <strong>OpenADx</strong></p>
<p>We leverage open collaboration and open source to</p>
<ul>
  <li>Accelerate time to market</li>
  <li>Increase efficiency</li>
  <li>Focus on customers</li>
</ul>
<p>It is the goal of this working group (WG) to deliver</p>
<ul>
  <li>An industry-wide accepted definition of the AD toolchain</li>
  <li>A reference architecture defining the interoperability of interesting in scope technologies</li>
  <li>Open Source projects for better interoperability and functionality of the established
    development tools</li>
</ul>
<p>The working group will:</p>
<ul>
  <li>Coordinate the development of related Eclipse Foundation projects towards an OpenADx
    framework.</li>
  <li>Define and manage the specifications for interfaces and functions for the framework.</li>
  <li>Promote the OpenADx framework and its value.</li>
  <li>Provide a vendor neutral marketing and other services to the OpenADx ecosystem.</li>
  <li>Define licensing and intellectual property flows that encourage community participation and
    tool adoption.</li>
  <li>Drive the funding model that enables this working group and its community to operate on a
    sustainable basis.</li>
</ul>
<h2>Technical Scope</h2>
<p>The OpenADx working group will coordinate the development of Eclipse Foundation projects on the
  following topics:</p>
<ul>
  <li>Implementation of interfaces between the development steps</li>
  <li>Implementation of common standards from other working groups (e.g. VDA Leitinitiative)</li>
  <li>Definition of an initial setup for fast implementation</li>
  <li>Ensures interoperability between development stages</li>
</ul>
<p>
  <img src="assets/images/openadx_image.png" class="img-responsive"/>
</p>
<p>Projects hosted by the Eclipse Foundation in the context of the OpenADx working group apply the
  Eclipse Development Process and best practices of openness, transparency and meritocracy. As such,
  these projects are open to participation by developers or contributors. They can submit
  contributions to the project that will be considered for inclusion by the project committers.
  Committer status is acquired by contributors after election by existing committers as described in
  the Eclipse Foundation Development Process.</p>

<h2>Governance and Precedence</h2>
<p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
<h3>Applicable Documents</h3>
<ul>
  <li>Eclipse Foundation Bylaws</li>
  <li>Eclipse Foundation Working Group Process</li>
  <li>Eclipse Foundation Working Group Operations Guide</li>
  <li>Eclipse Foundation Membership Agreement</li>
  <li>Eclipse Foundation Intellectual Property Policy</li>
  <li>Eclipse Foundation Antitrust Policy</li>
  <li>Eclipse Foundation Development Process</li>
  <li>Eclipse Foundation Trademark Usage Guidelines</li> 
</ul>
<p>All Members of the working group must be parties to the Eclipse Foundation Membership Agreement, including the
  requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current policies of the Eclipse
  Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
<p>In the event of any conflict between the terms set forth in this working group&#39;s charter and
  the Eclipse Foundation Bylaws, Membership Agreement, Development Process,
  Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse
  Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>
<h2>Membership</h2>
<p>In order to participate in the OpenADx working group, an entity must be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a>​ of the Eclipse Foundation, have executed the OpenADx Participation Agreement, and adhere to the requirements set forth in this Charter.</p>
<p>The OpenADx working group is open at any time to all new members who meet these conditions.</p>

<h3>Classes of OpenADx Membership</h3>
<p>The membership classes of the OpenADx WG are established to reflect the different interest
  situations of the members. The membership class has to be declared by the potential member in his
  OpenADx WG participation agreement. The membership class of each OpenADx WG member is checked once
  a year.</p>
<ul>
  <li><strong>Driver Members</strong></li>
</ul>
<p class="margin-left-60">
  Driver Members are typically organizations that view the OpenADx activities as strategic to their
  organization and are willing to invest significant resources to sustain and shape the activities
  of this working group as well as maintain the scope of OpenADx.<br /> Driver Members include
  industry users and strategic contributors related to the technologies and results provided by the
  OpenADx working group.<br /> Driver members will lead the operation of the working group, support
  the OpenADx projects and will act as first contact of the working group.
</p>
<ul>
  <li><strong>Development Members</strong></li>
</ul>
<p class="margin-left-60">Development Members are organizations that are interested to benefit from the
  OpenADx community. They will lead or participate in OpenADx projects and investigate the technical
  questions. They will propose and execute the OpenADx projects according to the processes defined
  by the OpenADx working group. Development members will invest efforts to the OpenADx projects as
  defined in the proposal.</p>
<ul>
  <li><strong>User Members</strong></li>
</ul>
<p class="margin-left-60">
  User Members use the technologies and results provided by the OpenADx WG. They want to keep track
  of the OpenADx development but do not want to influence in an essential way.<br /> User Members
  are willed and allowed to work on and bring their ideas in this working group, but they have no
  voice in the decision process. Typical User Members include industry users of the technologies and
  results provided by the OpenADx working group. Most User Members operate testing processes as part
  of their core business.
</p>
<ul>
  <li><strong>Guests</strong></li>
</ul>
<p class="margin-left-60">Guests are organizations who have been invited for one year by the Steering
  Committee of OpenADx WG to participate in some aspects of the activities of the Working Group.
  Typical Guests include all stakeholders in automated driving and beyond, such as R&amp;D partners,
  academic entities, legislators, consumer protection advocates, insurance companies, and potential
  future full-fledged members who want to have a closer look before deciding on their strategy. When
  Guests are invited to an OpenADx WG body for meetings, they have no right to vote. Invitations may
  be renewed by the Steering Committee. Guests are required to sign the participation agreement.</p>
<p>Guests need to be at least Eclipse Associate Members.</p>

<h2>OpenADx WG Participation Fees</h2>
<p>The Steering Committee will establish annual participation fees, and these fees will be reviewed
  and ratified by the Steering Committee on an annual basis. &nbsp;All participants in the working
  group will be informed of any changes in the participation fees, and this charter will be updated
  to reflect the established participation fees. The OpenADx Participation Agreement will stipulate
  that all members agree to pay the participation fees.</p>
<p>In addition to annual participation fees, member organizations participating in OpenADx are
  expected to contribute employee service days to the benefit of the working group. &nbsp;Results
  for employee service days can be delivered as:</p>
<ul>
  <li>result packages or</li>
  <li>service packages.</li>
</ul>
<p>The corresponding packages or services have to be offered in advance to and accepted by the
  steering committee.</p>
<p>The Steering Committee shall establish an annual budget, and in conjunction with Eclipse
  Foundation staff, shall administer that budget on behalf of the working group. &nbsp;</p>
<p>All members are welcome to make voluntary donations of both additional funding and employee
  service days to the benefit of the working group. &nbsp;</p>
<h3>Case of Violation</h3>
<p>All members of OpenADx agree to adhere to the Eclipse Bylaws and this charter, including to meet
  their financial and employee service day commitments. &nbsp;The Steering Committee is responsible
  for auditing the employee service day contributions of members, and will inform Eclipse Management
  Organization (EMO) of any member failing to meet its commitments. &nbsp;</p>
<h2>Governance</h2>
<p>This OpenADx working group is designed as:</p>
<ul>
  <li>a member driven organization,</li>
  <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
  <li>a means to organize the community of each project or component so that users and developers
    define the roadmap collaboratively.</li>
</ul>
<p>In order to implement these principles, the &ldquo;Steering Committee&rdquo; as the sole
  governance body for the working group during the incubation phase has been defined.</p>
<p>In addition, a general meeting of the working group, open to all members, shall be held at least
  once annually. &nbsp;</p>
<h2>Steering Committee</h2>
<h3>Powers and Duties</h3>
<p>Steering Committee members are required to:</p>
<ul>
  <li>Define and manage the strategy of the working group.</li>
  <li>Define and manage specification work packages and other non-implementation related activities
    within the scope of this working group.</li>
  <li>Define and manage which Eclipse Foundation projects are included within the scope of this
    working group.</li>
  <li>Define and manage the roadmap of the working group.</li>
  <li>Review and ratify this charter.</li>
  <li>Define the annual fees and employee service days for all classes of working group membership.</li>
  <li>Approve the annual budget based upon funds received through fees.</li>
  <li>Contribute to the acquisition of new working group members by actively engaging in community
    development and evangelism</li>
</ul>
<h3>Composition</h3>
<ul>
  <li>Each Driver Member of the working group has one seat on the Steering Committee.</li>
  <li>Two seats are allocated to Development Members. Development Member seats are allocated following
    the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Foundation Bylaws. Elected
    Development Members have the right to participate in every Steering Committee meeting.</li>
  <li>One seat is allocated to User Members. User Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in the Eclipse Foundation Bylaws. Elected User Members have the right to participate in every Steering Committee meeting.</li>
</ul>
<p>The Steering Committee elects among its Driver Members a chair who will serve as their chair for a 12 month period or until their successor is elected or as otherwise provided for in this Charter.</p>
<h3>Meeting Management</h3>
<ul>
  <li>The Steering Committee meets at least twice a year.</li>
</ul>
<h2>Common Dispositions</h2>
<ul>
  <li>The dispositions below apply to all governance bodies for this working group, unless otherwise
    specified. For all matters related to membership action, including without limitation: meetings,
    quorum, voting, electronic voting action without meeting, vacancy, resignation or removal, the
    respective terms set forth in the Eclipse Foundation Bylaws apply.</li>
</ul>
<h3>Voting</h3>
<ul>
  <li>For actions (i) approving specifications for adoption; (ii) approving or changing the name of the working
    group; and (iii) approving changes to annual Member contribution requirements; any such actions
    must be approved by no less than two-thirds (2/3) of the representatives present at a committee
    meeting.</li>
</ul>
<h3>Term and Dates of Elections</h3>
<ul>
  <li>This section only applies to the Steering Committee. All representatives shall hold office
    until their respective successors are appointed or elected, as applicable. There shall be no
    prohibition on re-election or re-designation of any representative following the completion of
    that representative&rsquo;s term of office.</li>
  <li>Elected representatives shall each serve one-year terms and shall be elected to serve from
    July 1 to June 30 of each calendar year, or until their respective successors are elected and
    qualified.</li>
</ul>
<h3>Meetings Management</h3>
<ul>
  <li>All meetings may be held at any place that has been designated by resolution of the
    corresponding body. All meetings may be held remotely using phone calls, video calls or any
    other means as designated by resolution of the corresponding body.</li>
</ul>
<h3>Infrastructure</h3>
<ul>
  <li>The OpenADx Working Group uses the normal infrastructure provided to Eclipse projects,
    including, among others, mailing lists, forums, bug trackers, source code repositories,
    continuous integration servers, build servers, and web sites.</li>
</ul>
<h3>Community Building</h3>
<ul>
  <li>The Eclipse Foundation will provide access to its network of developers to facilitate the
    community building and the dissemination of the OpenADx innovations. The Eclipse Foundation will
    use its communication channels such as its Newsletter, Twitter, or LinkedIn channels to promote
    key OpenADx events and encourage its members to join and participate in the OpenADx working
    group.</li>
</ul>
<h2>Membership Fee Structure</h2>
<p>The following table lists the OpenADx working group fees payable to the Eclipse
  Foundation. Fee structure to be in place by January 1, 2021. Note these fees are in addition to the fees for membership in the Eclipse Foundation; these fees are listed in the Eclipse Foundation <a href="https://www.eclipse.org/membership/#tab-fees">membership fee table</a>.</p>
<table class="table table-stripped">
  <thead>
    <tr>
      <th>Annual Corporate Revenue</th>
      <th class="text-center">WG Fees Driver Member</th>
      <th class="text-center">WG Fees Development Member</th>
      <th class="text-center">WG Fees User Member</th>
    </tr>
  </thead>
  <tbody>
    <tr class="text-center">
      <td class="text-left">&gt; $250 million</td>
      <td>TBD</td>
      <td>TBD</td>
      <td>TBD</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">&gt; $100 million &lt;= $250 million</td>
      <td>TBD</td>
      <td>TBD</td>
      <td>TBD</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">&gt; $50 million &lt;= $100 million</td>
      <td>TBD</td>
      <td>TBD</td>
      <td>TBD</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">&gt; $10 million &lt;= $50 million</td>
      <td>TBD</td>
      <td>TBD</td>
      <td>TBD</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">&lt; $10 million</td>
      <td>TBD</td>
      <td>TBD</td>
      <td>TBD</td>
    </tr>
    <tr class="text-center">
      <td class="text-left">&lt; $1 million &lt; 10 employees</td>
      <td>TBD</td>
      <td>TBD</td>
      <td>TBD</td>
    </tr>
  </tbody>
</table>
<h2>Membership Privileges</h2>
<table class="table table-stripped">
  <thead>
    <tr>
      <th>Privilege</th>
      <th>Driver Member</th>
      <th>Development Member</th>
      <th>User Member</th>
      <th>Guests</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Steering Committee</td>
      <td>Appointed</td>
      <td>Elected</td>
      <td>Elected</td>
      <td>-</td>
    </tr>
  </tbody>
</table>
<p>
  <strong>Charter Version History</strong>
</p>
<ul>
  <li>v1.2 Updates in support of the Eclipse Foundation corporate restructuring and to remove references to the incubation period</li>
  <li>v1.1 update June 24, 2020 - to reflect post-incubation period changes related to steering committee composition and fee structure</li>
  <li>v1.0 update June 25, 2019 - added election of a steering committee chairman </li>
  <li>v0.2 update May 17, 2019 - clarified participation fees and incubation period</li>
  <li>v0.1.1 update May 16, 2019 - added reference to OpenADx Participation Agreement</li>
  <li>v0.1 created April 28, 2019</li>
</ul>
