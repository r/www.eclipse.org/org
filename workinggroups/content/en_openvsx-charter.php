<?php

/**
 * Copyright (c) 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Zachary Sabourin (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version #1.0 Revision History at End</p>

    <h2>Vision and Scope</h2>
    <p>
      The Open VSX Working Group is formed to ensure the continued sustainability,
      integrity, evolution and adoption of the Open VSX Registry. In particular, it is formed to provide
      governance, guidance, and funding for the communities that support the implementation,
      deployment, maintenance and adoption of the Eclipse Foundation’s Open VSX Registry at
      open-vsx.org.
    </p>
    <p>The Working Group will:</p>
    <ul>
      <li>Provide for the long term viability of the Open VSX Registry (Registry) as both a vibrant
        open source project as well as a stable hosted registry service at open-vsx.org.</li>
      <li>Support and guide relevant open source projects that support the Registry</li>
      <li>Provide governance, resources, and support to enable the hosting, operation,
        maintenance and evolution the Open VSX Registry</li>
      <li>Promote the Registry brand and service in the marketplace.</li>
      <li>Assist in the fostering of the growth and evolution of the ecosystem.</li>
      <li>Provide vendor neutral marketing and other services to the Open VSX Registry
        ecosystem.</li>
      <li>Leverage Eclipse-defined licensing and intellectual property flows that encourage
        community participation, protect community members, and encourage usage.</li>
      <li>Manage the overall technical and business strategies related to the Open VSX Registry
        and its open source projects.</li>
      <li>Establish and drive a funding model that enables this working group and its
        community to operate on a sustainable basis.</li>
    </ul>

    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>
      The following governance documents are applicable to this charter, each of which can be found on the
      <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents</a> page or the
      <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources</a> page:
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
      <li>Eclipse Foundation Open VSX Publisher Agreement</li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership Agreement, including the
      requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current
      policies of the Eclipse Foundation, including but not limited to the Intellectual Property and
      Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this working group’s charter and the
      Eclipse Foundation Bylaws, Membership Agreement, Development Process, Specification
      Process, Working Group Process or any policies of the Eclipse Foundation, the terms of the
      respective Eclipse Foundation Bylaws, Membership Agreement, process or policy shall take
      precedence.
    </p>

    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a
      <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a> of the Eclipse Foundation,
      have executed the Open VSX Working Group Participation Agreement and adhere to the requirements set forth in
      this Charter to participate.
    </p>
    <p>
      The participation fees associated with the membership classes are shown in the Annual
      Participation Fees section. These are annual fees, and are established by the Open VSX Steering
      Committee, and will be updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any Working
      Group membership fees, and are decided as described in the Eclipse Foundation Bylaws and
      detailed in the Eclipse Foundation Membership Agreement.
    </p>
    <p>There are 3 classes of Open VSX working group membership - Participant, Supporter and Guest.</p>

    <h2>Classes of Membership</h2>
    <h3>Participant Members</h3>
    <p>
      Participant Members are typically organizations that deliver products or services based upon
      the Open VSX Registry, have a vested interest in the related open source projects, or view the
      ongoing success of this working group’s objectives as strategic to their organization. These
      organizations want to participate in the development and direction of an open ecosystem
      related to this working group. Participant Members of this working group must be at least a
      Contributing Member of the Eclipse Foundation.
    </p>

    <h3>Supporter Members</h3>
    <p>
      Supporting Members are: (a) organizations that want to participate in an open ecosystem
      related to this working group and wish to show their initial support for it, and (b) organizations
      which are at least Contributing members of the Eclipse Foundation. Organizations may be
      Supporting Members for a maximum period of 1 year only, after which time they are expected
      to change their membership to a different level. Supporting Members may be invited to
      participate in committee meetings at the invitation of the respective committee, but under no
      circumstances do Supporting Members have voting rights in any working group body.
      Supporting Members are required to execute the Working Group's Participation Agreement.
    </p>

    <h3>Guest Members</h3>
    <p>
      Guest Members are organizations which are Associate members of the Eclipse Foundation.
      Typical guests include R&D partners, universities, academic research centers, etc. Guests may be
      invited to participate in committee meetings at the invitation of the respective committee, but
      under no circumstances do Guest members have voting rights. Guest members are required to
      execute the Working Group’s Participation Agreement.
    </p>

    <h3>Membership Summary</h3>
    <table class="table table-stripped">
      <tr>
        <th>Committee Representation</th>
        <th>Participant Member</th>
        <th>Supporter/Guest Member</th>
      </tr>
      <tr>
        <td>Steering Committee</td>
        <td>Appointed</td>
        <td>N/A</td>
      </tr>
    </table>

    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within the Working Group with a
      focus to collaborate around a particular topic or domain of direct interest to the working group.
      SIGs are designed to drive the objectives of a subset of the Members of the Working Group in
      helping them achieve a dedicated set of goals and objectives. The scope of the SIG must be
      consistent with the scope of the Working Group Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each SIG may be either
      temporary or a permanent structure within the working group. SIGs can be disbanded at any
      time by self selection presenting reasons to and seek approval from the Steering Committee.
      Steering Committees may disband a SIG at any time for being inactive or non compliant with the
      Working Group's Charter, or by request of the SIG itself. SIGs operate as a non-governing Body
      of the Working Group. There are no additional annual fees to Members for participation in a
      SIG.
    </p>

    <h2>Sponsorship</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to the working group on
      an ad hoc basis to support the activities of the Working Group and its managed projects. Money
      or services provided by sponsors are used as set forth in the working group annual budget. The
      working group is free to determine whether and how those contributions are recognized. Under
      no condition are sponsorship monies refunded.
    </p>
    <p>Sponsors need not be members of the Eclipse Foundation or of the Working Group.</p>

    <h2>Governance</h2>
    <p>This Open VSX working group is designed as:</p>
    <ul>
      <li>a vendor-neutral, member-driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service
        providers,</li>
      <li>a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.</li>
    </ul>

    <h2>Governing Bodies</h2>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of
        this working group.</li>
      <li>Define and manage the technical roadmap and ensure adequate resources are available
        for the operation of the Registry service.</li>
      <li>Define strategies for global scaling and reliability.</li>
      <li>Drive the funding model that enables this working group and its community to operate
        on a sustainable basis.</li>
      <li>Define strategic marketing priorities, goals, and objectives for the working group.</li>
      <li>Coordinate and support the implementation of developer, publisher, and end-user
        outreach programs.</li>
      <li>Assist the Eclipse Foundation in the working group’s participation in conferences and
        events related to the working group.</li>
      <li>Review and approve this charter.</li>
      <li>Define the annual fees for all classes of the working group members.</li>
      <li>Establish an annual program plan.</li>
      <li>Approve the annual budget based upon funds received through fees and sponsorships.</li>
      <li>Approve the creation of subcommittees and define the purpose, scope, and membership
        of each such subcommittee.</li>
      <li>Approve the creation and retirement of Special Interest Groups (SIGs).</li>
    </ul>


    <h4>Composition</h4>
    <p>Each Participant Member of the working group is entitled to a seat on the Steering Committee.</p>
    <p>
      The Committee elects a chair of the Steering Committee. This chair is elected among the
      members of the Committee. They will serve for a 12 month period or until their successor is
      elected and qualified, or as otherwise provided for in this Charter. There is no limit on the
      number of terms the chair may serve.
    </p>

    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>

    <h2>Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this working group, unless otherwise
      specified. For all matters related to membership action, including without limitation: meetings,
      quorum, voting, vacancy, resignation or removal, the respective terms set forth in the Eclipse
      Foundation Bylaws apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a
      Body member is unavailable to attend or participate in a meeting of the Body, they may be
      represented by another Body member by providing written proxy to the Body’s mailing list in
      advance. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed
      from the Body upon the termination of the membership of such representative’s Member
      organization.
    </p>

    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body
      are determined by a simple majority of the representatives represented at a committee
      meeting at which a quorum is present.
    </p>

    <h4>Super Majority</h4>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors approve a specification
      license; (ii) approving specifications for adoption; (iii) modifying the working group charter; (iv)
      approving or changing the name of the working group; and (v) approving changes to annual
      Member contribution requirements; any such actions must be approved by no less than
      two-thirds (2/3) of the representatives represented at a committee meeting at which a quorum
      is present.
    </p>

    <h3>Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee.</p>
    <p>
      All representatives shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or re-designation of
      any representative following the completion of that representative's term of office.
    </p>
    <p>
      Participant Members Representatives shall serve in such capacity on the Steering Committee
      until the earlier of their removal by their respective appointing Member organization or as
      otherwise provided for in this Charter.
    </p>

    <h4>Elected Representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve for a 12
      month term or until their respective successors are elected and qualified, or as otherwise
      provided for in this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided that such resolutions
      are not inconsistent with any provision of this Charter.
    </p>

    <h3>Meetings Management</h3>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all meetings related to the
      working group will follow a prepared agenda and minutes are distributed two weeks after the
      meeting and approved at the next meeting at the latest, and shall in general conform to the
      Eclipse Foundation Antitrust Policy.
    </p>

    <h4>Place of Meetings</h4>
    <p>
      All meetings may be held at any place that has been designated from time-to-time by resolution
      of the corresponding body. All meetings may be held remotely using phone calls, video calls, or
      any other means as designated from time-to-time by resolution of the corresponding body.
    </p>

    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representatives at least fifteen (15) calendar days prior to such meeting,
      which notice will identify all potential actions to be undertaken by the Body at the Body
      meeting. No representative will be intentionally excluded from Body meetings and all
      representatives shall receive notice of the meeting as specified above; however, Body meetings
      need not be delayed or rescheduled merely because one or more of the representatives cannot
      attend or participate so long as at least a quorum of the Body is represented at the Body
      meeting.
    </p>

    <h4>Actions</h4>
    <p>
      The body may undertake an action only if it was identified in a body meeting notice or
      otherwise identified in a notice of special meeting.
    </p>

    <h4>Invitations</h4>
    <p>
      The Body may invite any member to any of its meetings. These invited attendees have no right
      to vote.
    </p>

    <h2>Working Group Participation Fees</h2>
    <p>
      Established fees cannot be charged retroactively and any fee structure change will be
      communicated to all members via the mailing list and will be charged the next time a member is
      invoiced.
    </p>

    <h2>Working Group Annual Participation Fees Schedule A</h2>
    <p>
      The following fees have been established by the Eclipse Open VSX Steering Committee. These
      fees are in addition to each participant’s membership fees in the Eclipse Foundation.
    </p>
    <p>
      Participant Members joining prior to January 1, 2024 will pay €20 000 for 2023, and
      subsequently pay €40 000 as their Participation Fee beginning January 1, 2024 and on each
      following January 1st.
    </p>
    <p>Supporter and Guest Members pay no Participation Fees.</p>

    <p><strong>Charter History</strong></p>
    <ul>
      <li>v0.1 proposed draft March, 2023</li>
      <li>Ratified by Steering Committee May 23, 2023</li>
    </ul>
  </div>
</div>