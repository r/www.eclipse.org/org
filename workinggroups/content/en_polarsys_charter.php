<?php
/**
 * Copyright (c) 2012, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <strong>December 2, 2015</strong>
    <h2>
      <a name="Goals_And_Vision"></a>Goals and Vision
    </h2>
    <p>PolarSys is the Eclipse Working Group dedicated to industrial open source tools for Embedded
      Systems development. It addresses the needs of the following industries:</p>
      <div class="row">
        <div class="col-sm-12">
          <ul>
            <li>Aeronautics</li>
            <li>Aerospace</li>
            <li>Automotive</li>
            <li>Defense and Security</li>
          </ul>
        </div>
        <div class="col-sm-12">
          <ul>
            <li>Energy</li>
            <li>Health care</li>
            <li>Railway</li>
            <li>Telecommunication</li>
          </ul>
        </div>
      </div>
    <p>PolarSys offers several benefits:</p>
    <blockquote>
      <ul>
        <li><strong>Open collaboration:</strong> PolarSys leverages the open source legal framework,
          processes and infrastructure of the Eclipse Foundation. It includes world class
          intellectual property to ensure that all open source code hosted by PolarSys can be safely
          used by both users and suppliers.</li>
        <li><strong>Open source tools:</strong> Commitment to commercial open source tools without
          vendor lock-in</li>
        <li><strong>Open Innovation:</strong> Ensuring the highest levels of reliability, service,
          safety and performance requires continuous effort in research and development, not only
          for the products themselves but also for development methods and tools. PolarSys ensures
          open innovation through the sharing of research, development, and maintenance efforts and
          fosters exchange between academic and industry partners</li>
        <li><strong>User driven community:</strong> Since its inception, large industry users are
          leading the evolution of PolarSys and setting the focus on the needs of large, industrial
          organizations.</li>
        <li><strong>Extensibility and customizability:</strong> PolarSys fosters collaboration on
          common features and requirements. In addition, it enables software extensibility and
          customizability so that solutions can be adapted to specific domains, or to support a
          methodology that is specific to an organization.</li>
        <li><strong>Option to Make or Buy:</strong> Companies can choose to make or buy their tools
          and frameworks depending on their internal needs.</li>
        <li><strong>Expert Support:</strong> Members receive expert support with direct access to
          creators and developers</li>
        <li><strong>Economy through re-use:</strong> Re-using open source tools enables efficient
          resource allocation in adaptations, extensions, support, development and research.</li>
        <li><strong>Very Long Term Support:</strong> Typical products have life cycles of 20 to 50
          years or more, during which time the tool chains remain operational. PolarSys provides
          support for these life cycles.</li>
        <li><strong>Certification:</strong> From DO178 to ISO26262 and ECSS 40, the development of
          safety-critical and embedded systems needs to comply with strict regulations impacting not
          only the final product, but also the development process and the tools used to build it.
          PolarSys provides opportunities among members to share qualification kits to shorten the
          time to tools qualification and to share the qualification effort among the different
          users of the tools.</li>
      </ul>
    </blockquote>
    <h2>
      <a name="PolarSys_Solutions"></a>PolarSys Solutions
    </h2>
    <p>Each PolarSys Solution provides open source tools, delivered as a package, for a specific
      segment of embedded systems development activities. To be approved by the PolarSys Steering
      Committee, a Solution must meet the following criteria:</p>
    <blockquote>
      <ul>
        <li>Steering committee super majority (2/3) vote in favor of the addition or removal of a
          PolarSys Solution.</li>
        <li>Satisfies the needs of a specific market and provides added value to users.</li>
        <li>Sponsored by a Steering Committee member and has an assigned PolarSys leader.</li>
        <li>Thoroughly tested - public test scenarios and reports are required.</li>
        <li>Well-documented - provides information, downloads and other materials on the PolarSys
          website in addition to getting started documentation and a data sheet.</li>
        <li>Potential Community - endorsed by PolarSys members who have interest in the solution.
          Invitations can be extended to external guests under specific circumstances described
          below.</li>
      </ul>
    </blockquote>
    <h2>
      <a name="PolarSys_technologies"></a><a name="Core_domains"></a>PolarSys technologies
    </h2>
    <p>PolarSys focuses on tools to support the different phases of the development of embedded
      systems:</p>
    <blockquote>
      <ul>
        <li>Operations</li>
        <li>Requirements Engineering and Traceability</li>
        <li>Systems Engineering</li>
        <li>Architecture, Design and Analysis</li>
        <li>Implementation</li>
        <li>Testing, Validation</li>
      </ul>
    </blockquote>
    <p>These development phases are supported by several types of tools including:</p>
    <blockquote>
      <ul>
        <li>Requirements Management tools</li>
        <li>Modeling tools supporting UML, SysML and DSL</li>
        <li>Compilers, Debuggers and Tracers</li>
        <li>Static Analysis tools</li>
        <li>Simulation, emulation and hardware logic (VHDL, SystemC, etc.)</li>
        <li>Integrated Development Environment</li>
        <li>Configuration Management</li>
        <li>Application and Product Lifecycle Management</li>
        <li>Testing and test management tools</li>
        <li>Reporting and documentation tools</li>
        <li>Embedded components and middleware</li>
      </ul>
    </blockquote>
    <h2>
      <a name="Component_Management"></a>Component Management
    </h2>
    <p>PolarSys does not intend to systematically re-develop components. A lot of very good
      solutions answering some PolarSys needs already exist in open source. When specific issues
      such as durability or certification are not taken into account, PolarSys plays its part by
      completing components assets, setting up specific support, and coordinating development and
      support. Two kinds of projects are therefore supported in PolarSys:</p>
    <blockquote>
      <ul>
        <li><strong>Hosted Projects</strong> - where the technical artifacts are hosted by PolarSys.</li>
        <li><strong>Coordination Projects</strong> - where most of the technical artifacts are
          hosted elsewhere, and PolarSys focuses on user coordination and specific artifacts
          (patches, qualification kits, etc.).</li>
      </ul>
    </blockquote>
    <h2>
      <a name="Governance_and_Precedence"></a>Governance and Precedence
    </h2>
    <h3>Applicable Documents</h3>
    <blockquote>
      <ul>
        <li><a href="../documents/eclipse_foundation-bylaws.pdf">Eclipse Bylaws</a></li>
        <li><a href="../workinggroups/industry_wg_process.php">Industry Working Group Process</a></li>
        <li><a href="../documents/Eclipse%20MEMBERSHIP%20AGMT%202010_01_05%20Final.pdf">Eclipse
            Membership Agreement</a></li>
        <li><a href="http://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
            Development Process</a></li>
      </ul>
    </blockquote>
    <p>All PolarSys Members must be party to the Eclipse Membership Agreement, including the
      requirement set forth in Section 2.2 to follow the Bylaws and then-current policies of the
      Eclipse Foundation. In the event of any conflict between the terms set forth in this PolarSys
      Working Group Charter and the Eclipse Foundation Bylaws, Membership Agreement, Eclipse
      Development Process, Eclipse Working Group Process, or any policies of the Eclipse Foundation,
      the terms of the Eclipse Foundation Bylaws, Membership Agreement, Processes, and Policies
      shall take precedence.</p>
    <h2>
      <a name="IP_Management"></a>IP Management
    </h2>
    <p>The Intellectual Property Policy of the Eclipse Foundation will apply to all PolarSys
      activities. PolarSys will follow the Eclipse Foundation&#39;s IP due diligence process in
      order to provide clean open source software released under EPL or any other licenses approved
      by the WG Steering Committee and the Eclipse Foundation Board of Directors, such as BSD-like
      licenses and the LGPL.</p>
    <h2>
      <a name="Development_Process"></a>Development Process
    </h2>
    <p>The Eclipse Foundation Development Process will apply to all PolarSys open source projects.
      In particular, the project lifecycle model and review process will be followed by all PolarSys
      open source projects.</p>
    <h2>
      <a name="Membership"></a>Membership
    </h2>
    <p>
      In order to participate in PolarSys, an entity must be at least a <a
        href="../../membership/become_a_member/membershipTypes.php#solutions"
      >Solutions Member</a> of the Eclipse Foundation, have executed the WG Participation Agreement,
      and adhere to the requirements set forth in this charter. The Eclipse Solutions Member fees
      appear in the tables below for convenience only; they are determined as described in the <a
        href="../documents/eclipse_foundation-bylaws.pdf"
      >Eclipse Bylaws</a> and are detailed in the <a
        href="../documents/Eclipse%20MEMBERSHIP%20AGMT%202010_01_05%20Final.pdf"
      >Eclipse Membership Agreement.</a>
    </p>
    <p>Eclipse Associate members may become PolarSys guest members for one year by invitation from
      the PolarSys Steering Committee.</p>
    <h3>
      <a name="Classes_of_membership"></a>Classes of Membership
    </h3>
    <h4>
      <a name="Steering_Committee_Members"></a>Steering Committee Members
    </h4>
    <p>Steering Committee members are organizations that view PolarSys as a strategic industry
      working group and invest resources to sustain its activities. Typical Steering Committee
      members include industrial users of the technologies and projects supported by PolarSys.</p>
    <p>PolarSys Steering Committee membership is provided at no additional charge to Strategic
      Members paying the maximum level of dues ($250K).</p>
    <p>
      <strong><a name="Polarsys_Steering_Committee_Member_Fees"></a>PolarSys Steering Committee
        Member Fees</strong>
    </p>
    <table class="table table-striped">
      <tbody>
        <tr>
          <td><p>
              <strong>Turnover</strong>
            </p></td>
          <td><p>
              <strong>Eclipse Solutions<br /> Membership
              </strong>
            </p></td>
          <td><p>
              <strong>PolarSys<br /> Membership
              </strong>
            </p></td>
          <td><p>
              <strong>Total</strong>
            </p></td>
        </tr>
        <tr>
          <td><p>&gt;$250 million</p></td>
          <td><p>$20,000</p></td>
          <td><p>$30,000</p></td>
          <td><p>$50,000</p></td>
        </tr>
        <tr>
          <td><p>&gt;$100 million &lt;= $250 million</p></td>
          <td><p>$15,000</p></td>
          <td><p>$30,000</p></td>
          <td><p>$45,000</p></td>
        </tr>
        <tr>
          <td><p>&gt;$50 million &lt;= $100 million</p></td>
          <td><p>$10,000</p></td>
          <td><p>$30,000</p></td>
          <td><p>$40,000</p></td>
        </tr>
        <tr>
          <td><p>&gt;$10 million &lt;= $50 million</p></td>
          <td><p>$7,500</p></td>
          <td><p>$20,000</p></td>
          <td><p>$27,500</p></td>
        </tr>
        <tr>
          <td><p>&lt;$10 million</p></td>
          <td><p>$5,000</p></td>
          <td><p>$20,000</p></td>
          <td><p>$25,000</p></td>
        </tr>
      </tbody>
    </table>
    <h4>
      <a name="Participant_members"></a><a name="Participating_members"></a>Participating Members
    </h4>
    <p>Participating members are organizations that view PolarSys as an important part of their
      corporate and product strategy and offer products and services based on, or with, PolarSys.
      These organizations participate in the development of the PolarSys ecosystem. Typical
      Participating members are service providers for the technologies and products supported by
      PolarSys.</p>
    <p>PolarSys Participating membership is provided at no additional charge to Strategic Members
      paying dues in excess of ($100K).</p>
    <p>
      <a name="Polarsys_Participant_Member_Fees"></a><a name="Polarsys_Participating_Member_Fees"></a>PolarSys
      Participating Member Fees
    </p>
    <table class="table table-striped">
      <tbody>
        <tr>
          <td><p>
              <strong>Turnover</strong>
            </p></td>
          <td><p>
              <strong>Eclipse Solutions<br />Membership
              </strong>
            </p></td>
          <td><p>
              <strong>PolarSys<br />Membership
              </strong>
            </p></td>
          <td><p>
              <strong>Total</strong>
            </p></td>
        </tr>
        <tr>
          <td><p>&gt;$250 million</p></td>
          <td><p>$20,000</p></td>
          <td><p>$10,000</p></td>
          <td><p>$30,000</p></td>
        </tr>
        <tr>
          <td><p>&gt;$100 million &lt;= $250 million</p></td>
          <td><p>$15,000</p></td>
          <td><p>$10,000</p></td>
          <td><p>$25,000</p></td>
        </tr>
        <tr>
          <td><p>&gt;$50 million &lt;= $100 million</p></td>
          <td><p>$10,000</p></td>
          <td><p>$10,000</p></td>
          <td><p>$20,000</p></td>
        </tr>
        <tr>
          <td><p>&gt;$10 million &lt;= $50 million</p></td>
          <td><p>$7,500</p></td>
          <td><p>$7,500</p></td>
          <td><p>$15,000</p></td>
        </tr>
        <tr>
          <td><p>&lt;$10 million</p></td>
          <td><p>$5,000</p></td>
          <td><p>$5,000</p></td>
          <td><p>$10,000</p></td>
        </tr>
        <tr>
          <td><p>&lt;$1 million &amp; &lt; 10 employees</p></td>
          <td><p>$1,500</p></td>
          <td><p>$1,500</p></td>
          <td><p>$3,000</p></td>
        </tr>
      </tbody>
    </table>
    <h4>
      <a name="Guests"></a>Guests
    </h4>
    <p>Guests are organizations who have been invited for one year by the PolarSys Steering
      Committee to participate in particular aspects of the activities of the Working Group. Typical
      guests includes R&amp;D partners, Universities, academic research centers, and potential
      full-fledged members who want to have a closer look before deciding on their participation
      strategy. Guest membership is free of charge and invitations may be renewed once by the
      Steering Committee. Guests are required to sign an Eclipse and PolarSys membership agreement,
      but when participating in meetings of a PolarSys body, they have no voting rights.</p>
    <h4>
      <a name="Research_Centers_Governmental_Agencies"></a>Research centers, governmental agencies
    </h4>
    <p>Organizations such as governmental agencies and research centers, whose financing is based on
      annual (governmental) budget allocations rather than commercial revenue, will be designated
      the membership category of small businesses. That is, their membership dues will be based on
      turnover of less than $10 million but greater than $1 million.</p>
    <h4>
      <a name="Committers"></a>Committers
    </h4>
    <p>
      Committer members are individuals who, through a process of meritocracy defined by the Eclipse
      Development Process, are able to contribute and commit code to PolarSys projects for which
      they are qualified. Committers may be members by virtue of working for a member organization,
      or may choose to complete the membership process independently. For further explanation and
      details, see the <a href="../../membership/become_a_member/committer.php">Eclipse Committer
        Membership</a> page.
    </p>
    <h3>
      <a name="Polarsys_Membership_Summary"></a>PolarSys Membership Summary
    </h3>
    <table class="table table-striped">
      <tbody>
        <tr>
          <td><p>&nbsp;</p></td>
          <td><p>
              <strong>PolarSys <br />Steering Committee <br />Member
              </strong>
            </p></td>
          <td><p>
              <strong>PolarSys <br />Solutions Member<br /></strong>
            </p></td>
          <td><p>
              <strong>Committer</strong>
            </p></td>
          <td><p>
              <strong>Guest</strong>
            </p></td>
        </tr>
        <tr>
          <td><p>Member of the Steering Committee</p></td>
          <td><p>X</p></td>
          <td><p>Elected</p></td>
          <td><p>-</p></td>
          <td><p>Invited</p></td>
        </tr>
        <tr>
          <td><p>Member of the Technical Committee</p></td>
          <td><p>X</p></td>
          <td><p>Elected</p></td>
          <td><p>-</p></td>
          <td><p>Invited</p></td>
        </tr>
        <tr>
          <td><p>Member of the Marketing Committees</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>Invited</p></td>
        </tr>
        <tr>
          <td><p>Access to the open collaboration infrastructure</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
        </tr>
        <tr>
          <td><p>Write Access to open source code repositories</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
        </tr>
        <tr>
          <td><p>Access to the LTS Build Infrastructure</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
        </tr>
        <tr>
          <td><p>Access to LTS binary releases</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
        </tr>
        <tr>
          <td><p>Host custom build on WG infrastructure</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
        </tr>
        <tr>
          <td><p>Maturity assessment program</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
        </tr>
        <tr>
          <td><p>Access to qualification kits</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
        </tr>
        <tr>
          <td><p>IP due diligence</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
        </tr>
        <tr>
          <td><p>Branding process</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>-</p></td>
        </tr>
        <tr>
          <td><p>Host private R&amp;D projects</p></td>
          <td><p>X</p></td>
          <td><p>X</p></td>
          <td><p>-</p></td>
          <td><p>X</p></td>
        </tr>
      </tbody>
    </table>
    <p>
      Services are detailed in the <a href="#Services" class="jump">Services</a> section.
    </p>
    <p>All matters related to membership in the Eclipse Foundation and the PolarSys WG will be
      governed by the Eclipse Foundation Bylaws, Membership Agreement, and Eclipse Working Group
      Process. These matters include, without limitation payment of dues, delinquency, termination,
      resignation, reinstatement, assignment, and the distribution of assets upon dissolution.</p>
    <p>Members who resign or otherwise terminate their membership in the PolarSys WG lose their
      rights to access and use the private assets and data of PolarSys on the date of the
      termination. The private assets of terminated members, such as test cases or custom build
      chains, shall be archived and the archive returned to the terminated member. The private
      assets of terminated members can be removed from PolarSys storage forty-five (45) days after
      the termination date. In addition, the terminated member has the option to make a request to
      the Steering Committee to allow their archived private assets be stored for two (2) years.
      This request shall be sent within thirty (30) days after the termination date and is accepted
      at the sole discretion of the Steering Committee. The terms of any licenses for PolarSys
      private assets acquired by a member during the period of membership shall remain as specified
      in the license.</p>
    <h2>
      <a name="Services"></a>Services
    </h2>
    <h3>
      <a name="Open_source_collaboration_infrastructure"></a>Collaboration Infrastructure
    </h3>
    <p>PolarSys leverages the standard Eclipse open source collaboration infrastructure. As such,
      source code repositories, Bugzilla, wikis, forums, project mailing lists, and other services
      provided as part of the open source collaboration infrastructure, are publicly visible.
      Committers have write access to this infrastructure, and as such have the rights and
      obligations as set forth in the Eclipse Development Process and the various Eclipse committer
      agreements. The catalog of PolarSys components is part of this collaboration infrastructure.</p>
    <p>In addition, a private, members-only collaboration infrastructure for the exclusive use of
      Members of the PolarSys Working Group is provided. Access rights to, and the licensing terms
      for, this infrastructure will be determined by the Steering Committee, with the approval of
      the Eclipse Management Organization (EMO).</p>
    <h3>
      <a name="LTS_build_and_test_infrastructure"></a>Very Long Term Support (VLTS)
    </h3>
    <p>Polarsys aims to provide a specific collaboration, build and test infrastructure for very
      long term support (VLTS). This infrastructure will enable specific VLTS releases for durations
      of up to 10 years in a first version, and up to 50 years or more with the use of
      virtualization technologies. PolarSys LTS and VLTS releases are only accessible to Polarsys
      member companies. Such binary code may not be redistributed unless it is integrated into, or
      updates, a commercial software product or custom-developed software, and is distributed
      pursuant to an Object Code License.</p>
    <h3>
      <a name="Trademark"></a>Trademark
    </h3>
    <p>To create a strong brand, the PolarSys name and logo are protected by trademark registration.
      The PolarSys trademark distinguishes PolarSys from others and communicates that PolarSys goals
      are pursued when the trademark is used. PolarSys members can use the PolarSys name and logo on
      their marketing material to indicate that they are a PolarSys member and to identify they are
      using or providing services for a PolarSys Solution. Other uses of the name and logo must be
      approved by the PolarSys Steering Committee and the EMO. Members who resign or otherwise
      terminate their membership lose their rights to use the PolarSys name and logo upon
      &nbsp;their termination date.</p>
    <h3>
      <a name="Branding_process"></a>Supplier Branding Process
    </h3>
    <p>The branding process aims at rewarding the skills and investments of PolarSys members who
      offer services based on PolarSys solutions. Suppliers can create a web page on polarsys.org to
      advertise their expertise and list their offerings including training, consulting/workshops,
      adaptations, development support and tools.</p>
    <h3>
      <a name="Marketing"></a>Marketing
    </h3>
    <p>Marketing materials are created for PolarSys Solutions. Materials may include datasheets,
      logos, stickers, promotional videos, web page and conference exhibits.</p>
    <p>The marketing plan including conferences attendance and PolarSys booth are approved by a
      steering committee super majority (2/3) vote.</p>
    <p>PolarSys marketing services are also an important channel for supplier branding through, for
      example, publishing supplier datasheets or providing potential customers with information and
      contact to solution suppliers.</p>
    <h3>
      <a name="Industry_Consortium"></a>Industry Consortium
    </h3>
    <p>PolarSys enables the creation of Industry Consortia, sub-groups of PolarSys that focus on a
      specific solution. PolarSys provides the primary collaboration and marketing channel for
      PolarSys solutions at large. An Industry Consortium, on the other hand, provides the best
      granularity to foster close collaboration of its members to create strategies to improve a
      specific solution and to collaboratively fund development.</p>
    <p>Each Industry Consortium will have its own charter. The creation of an Industry Consortium
      must be approved by the PolarSys steering committee.</p>
    <p>Fees from members who are participating in an IC will be allocated as follows:</p>
    <blockquote>
      <ul>
        <li>a base budget is initially allocated to the general fund to cover fixed costs as voted
          by the PolarSys Steering Committee</li>
        <li>the remaining portion of a given membership fee is allocated based on the choice of the
          member to individual ICs or to other specific projects as approved by the Steering
          Committee.</li>
      </ul>
    </blockquote>
    <h3>
      <a name="PolarSys_Enhancements_Program"></a>PolarSys Enhancements Program
    </h3>
    <p>In addition to membership fees, members can add funding to cover a member specific need such
      as improvement of a Solution component, adaptations, adding automated tests, training or other
      services provided by PolarSys members. The results of such enhancement programs must be open
      source and should contribute to the PolarSys solution or component.</p>
    <p>The Enhancement Program can also be used to create special projects to for example, drive
      PolarSys related specifications or standards by becoming a member of a specification
      organization, or to add a major improvement to the PolarSys infrastructure.</p>
    <p>The role of PolarSys is to aggregate funds, maintain the contracts between suppliers and the
      Eclipse Foundation, and to forward payments to contractors. The role is similar to a brokerage
      service where the member providing the funds must also provide the content of the statement of
      work. Invoices are paid by default unless the funds provider notifies PolarSys otherwise. End
      user companies can use the program as a one stop shop and suppliers benefit from qualification
      as a supplier to a member company under the Eclipse Foundation.</p>
    <h3>
      <a name="Maturity_assessment_program"></a><a name="Quality_and_Maturity_Program"></a>Quality
      and Maturity Program
    </h3>
    <p>In order to maintain a high level of quality and maturity for PolarSys components, a testing
      and assessment infrastructure is provided that automates testing and provides quality/maturity
      evaluations based on metrics. The evaluation is done collaboratively by component developers,
      component integrators, and component users.</p>
    <h3>
      <a name="Hosting_custom_builds_on_the_Polarsys_in"></a>Hosting Custom Build on the PolarSys
      Infrastructure
    </h3>
    <p>This service provides the ability to use the PolarSys test and build infrastructure in order
      to create member-specific bundles. These bundles can be a unique combination of PolarSys
      components and are private to the member who defines and uses them.</p>
    <h3>
      <a name="IP_due_diligence"></a>IP Due Diligence
    </h3>
    <p>IP due diligence is necessary to verify that committers have the right to release their
      PolarSys code in open source. It is also necessary to confirm that integrated components have
      compatible licenses. License compatibility is particularly important as PolarSys allows not
      only EPL licensed components, but also BSD-style and LGPL licensed components.</p>
    <h3>
      <a name="Access_to_qualification_kits"></a><a name="Qualification_Kits"></a>Qualification Kits
    </h3>
    <p>Many Polarsys components are used in the development of certified and qualified embedded
      software. As such, specific documentation is needed and is adapted in the context of a given
      certification process. These documents, like development plans and test plans, are part of the
      qualification kits and member companies can share them via a PolarSys private area.</p>
    <h3>
      <a name="Hosting_private_projects"></a>Hosting Private Projects
    </h3>
    <p>In order to promote the maturation of research prototypes and to foster open innovation,
      PolarSys provides the ability to host members&#39; time-limited private projects that may
      become new PolarSys components. Private projects can only be initiated by Steering Committee
      members. The Steering Committee, with the approval of the EMO, will establish the licensing
      requirements for private projects. Guests may participate in private projects at the request
      of the Steering Committee.</p>
    <h2>
      <a name="Governance"></a>Governance
    </h2>
    <p>PolarSys is designed as</p>
    <blockquote>
      <ul>
        <li>A user driven organization</li>
        <li>A means to foster a vibrant and sustainable ecosystem of components and service
          providers</li>
        <li>A means to organize the community of each project or component so that users and
          developers define the roadmap collaboratively</li>
      </ul>
    </blockquote>
    <p>In order to implement these principles, the following governance bodies have been defined
      (each a &quot;body&quot;):</p>
    <blockquote>
      <ul>
        <li>The Steering Committee</li>
        <li>The Technical Committee</li>
        <li>The Marketing Committee</li>
      </ul>
    </blockquote>
    <h3>
      <a name="Common_Dispositions"></a>Common Dispositions
    </h3>
    <p>The dispositions below apply to all PolarSys bodies, unless otherwise specified. For all
      matters related to membership activities, including without limitation meetings, quorum,
      voting, electronic voting without meetings, vacancy, resignation, or removal, the terms set
      forth in Section 6 of the Eclipse Foundation Bylaws apply.</p>
    <h4>
      <a name="Good_Standing"></a>Good Standing
    </h4>
    <p>A representative shall be deemed to be in good standing, and thus eligible to vote on issues
      coming before the body in which he participates, if the representative has attended (in person
      or via voice communication channels) a minimum of three (3) of the last four (4) body meetings
      (if there have been at least four meetings). Appointed representatives on the body may be
      replaced by the member organization they are representing at any time by providing written
      notice to the Steering Committee. In the event a body member is unavailable to attend or
      participate in a meeting of the body, he or she may send a representative and may vote by
      proxy, which shall be included in determining whether the representative is in good standing.
      As per the Eclipse Foundation Bylaws, a representative shall be immediately removed from the
      body upon the termination of the membership of such representative&rsquo;s member
      organization.</p>
    <h4>
      <a name="Voting"></a>Voting
    </h4>
    <p>
      <strong><a name="Super_Majority"></a>Super Majority</strong>
    </p>
    <p>For actions (i) leading to a request to the Eclipse Foundation Board of Directors to approve
      an additional distribution license for PolarSys projects; (ii) amending the terms of the
      PolarSys Participation Agreement; (iii) approving or changing the name of PolarSys; and (iv)
      approving changes to annual member contribution requirements, the actions must be approved by
      no less than two-thirds (2/3) of the representatives in good standing represented at a
      Steering Committee meeting at which a quorum is present.</p>
    <h4>
      <a name="Term_and_Dates_of_elections"></a>Term and Dates of Elections
    </h4>
    <p>This section only applies to the Steering Committee, the Architecture Committee and the
      Quality and Branding Committee.</p>
    <p>All representatives shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or re-designation of any
      representative following the completion of that representative&rsquo;s term of office.</p>
    <p>
      <strong><a name="Steering_Committee_Members_2"></a>Steering Committee Members</strong>
    </p>
    <p>Steering Committee member representatives shall serve in such capacity until their removal by
      their respective appointing member organization or as otherwise provided for in this charter.</p>
    <p>
      <strong><a name="Elected_representatives"></a>Elected Representatives</strong>
    </p>
    <p>Elected representatives shall each serve one-year terms and shall be elected to serve from
      April 1 to March 31 of each calendar year, or until their respective successors are elected
      and qualified, or as otherwise provided for in this charter. Procedures governing elections of
      representatives may be established pursuant to resolutions of the Steering Committee provided
      that such resolutions are not inconsistent with any provision of this charter.</p>
    <h4>
      <a name="Meetings_Management"></a>Meeting Management
    </h4>
    <p>
      <strong><a name="Place_of_meetings"></a>Place of Meetings</strong>
    </p>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding body. All meetings may be held remotely using phone calls,
      video calls, or any other means as designated from time-to-time by resolution of the
      corresponding body.</p>
    <p>
      <strong><a name="Regular_meetings"></a>Regular Meetings</strong>
    </p>
    <p>
      No body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representative in good standing at least thirty (30) calendar days
      prior to such meeting, which notice will identify all potential actions to be undertaken by
      the body at the body meeting. No representative will be intentionally excluded from body
      meetings and all representatives shall receive notice of the meeting as specified above;
      however, body meetings need not be delayed or rescheduled merely because one or more of the
      representatives cannot attend or participate so long as at least a quorum of the body (as
      defined in the <a href="#Common_Dispositions" class="jump">Common Dispositions</a> section
      above) is represented at the body meeting. Electronic voting shall be permitted in conjunction
      with any and all meetings of the body, the subject matter of which requires a vote of the body
      to be delayed until each such representative in attendance has conferred with his or her
      respective member organization as set forth in the <a href="#Voting" class="jump">Voting</a>
      section above.
    </p>
    <p>
      <strong><a name="Actions"></a>Actions</strong>
    </p>
    <p>The body may undertake an action only if it was identified in a body meeting notice or
      otherwise identified in a notice of special meeting.</p>
    <h4>
      <a name="Invitations"></a>Invitations
    </h4>
    <p>The body may invite any PolarSys member to any of its meetings. These invited attendees have
      no right to vote.</p>
    <h3>
      <a name="Steering_Committee"></a>Steering Committee
    </h3>
    <h4>
      <a name="Powers_and_Duties"></a>Powers and Duties
    </h4>
    <p>Steering committee members are required to</p>
    <blockquote>
      <ul>
        <li>Define the yearly objectives such as which areas to focus, type of companies to recruit,
          services development, marketing, community development, infrastructure, etc.</li>
        <li>Define the strategy of the WG</li>
        <li>Define the global roadmap</li>
        <li>Discuss and amend the charter and the participation agreement</li>
        <li>Define the budget and fees each year</li>
        <li>Define and follow marketing and communication activities</li>
        <li>Invite guest members</li>
        <li>Vote with a super majority (2/3) on the yearly objectives, marketing plan, yearly budget
          including fixed cost, and the addition or removal of PolarSys Solutions.</li>
      </ul>
    </blockquote>
    <h4>
      <a name="Composition"></a>Composition
    </h4>
    <blockquote>
      <ul>
        <li>Each Steering Committee member of the WG has a seat on the Steering Committee.</li>
        <li>At least one seat is allocated to participating members. An additional seat on the
          committee shall be allocated to the participating members for every additional five (5)
          seats beyond one (1) allocated to Steering Committee members. Participating member seats
          are allocated following the Eclipse &quot;single transferable vote&quot;, as defined in
          the Eclipse Bylaws.</li>
        <li>The Steering Committee elects among its members a chairman who will represent the WG.
          The Chairman will serve from April 1 to March 31 of each calendar year, or until his/her
          successor is elected and qualified, or as otherwise provided for in this charter.</li>
      </ul>
    </blockquote>
    <h4>
      <a name="Meeting_Management"></a>Meeting Management
    </h4>
    <p>EMO sends a monthly report with a monthly meeting request, those monthly meetings will also
      include quarterly and special committee type of meetings.</p>
    <p>At quarterly Steering Committee meeting, EMO will report on:</p>
    <blockquote>
      <ul>
        <li>Main achievements during the last quarter</li>
        <li>Main activities done during the last quarter regarding marketing, membership, community
          development, infrastructure, and other PolarSys Services.</li>
        <li>How are we achieving our annual objectives</li>
        <li>Main issues and challenges</li>
        <li>Financial report</li>
        <li>Plan of the main activities for the next quarter</li>
      </ul>
    </blockquote>
    <p>At quarterly Steering Committee meeting, chairs of each committee and Industry consortium
      will report on:</p>
    <blockquote>
      <ul>
        <li>Enhancement program activities</li>
        <li>New projects</li>
        <li>Technical roadmap</li>
        <li>Plan for next quarter</li>
      </ul>
    </blockquote>
    <p>Quarterly Steering Committee meetings are pre-schedule by EMO for the whole year.</p>
    <p>EMO provides the minutes of meeting at the end of the meeting which include resolutions and
      action items with corresponding lead.</p>
    <h3>
      <a name="Architecture_Committee"></a><a name="Technical_Committee"></a>Technical Committee
    </h3>
    <h4>
      <a name="Powers_and_Duties_2"></a>Powers and Duties
    </h4>
    <p>Technical Committee members are required to</p>
    <blockquote>
      <ul>
        <li>Ensure the technical consistency of PolarSys projects</li>
        <li>Ensure that PolarSys projects achieve VLTS objectives</li>
        <li>Recommend technologies</li>
        <li>Establish technical guidelines</li>
        <li>Validate new project proposals</li>
      </ul>
    </blockquote>
    <h4>
      <a name="Composition_2"></a>Composition
    </h4>
    <blockquote>
      <ul>
        <li>Each Member of the WG has a seat on the Technical Committee.</li>
        <li>Each PolarSys hosted and coordinated project lead is invited to the Technical Committee</li>
        <li>The Technical Committee elects a chairman who reports to the Steering Committee. This
          Chairman is elected among the members of the Technical Committee. He/she will serve from
          April 1 to March 31 of each calendar year, or until his/her successor is elected and
          qualified, or as otherwise provided for in this charter.</li>
      </ul>
    </blockquote>
    <h4>
      <a name="Meeting_Management_2"></a>Meeting Management
    </h4>
    <p>The Technical Committee meets at least twice per year.</p>
    <h3>
      <a name="Marketing_Committee"></a>The Marketing Committee
    </h3>
    <h4>
      <a name="Powers_and_Duties_3"></a>Powers and Duties
    </h4>
    <p>The Marketing Committee members are required to</p>
    <blockquote>
      <ul>
        <li>Define the annual PolarSys marketing plan including strategic participation at
          conferences to increase PolarSys visibility</li>
        <li>Ensure the consistency of the branding process</li>
      </ul>
    </blockquote>
    <h4>
      <a name="Composition_3"></a>Composition
    </h4>
    <blockquote>
      <ul>
        <li>Each member of the WG has a seat on the Marketing Committee.</li>
        <li>The Marketing Committee elects a chairman who reports to the Steering Committee. This
          chairman is elected among the members of the Marketing Committee. He/she will serve from
          April 1 to March 31 of each calendar year, or until his/her successor is elected and
          qualified, or as otherwise provided for in this charter.</li>
      </ul>
    </blockquote>
    <h4>
      <a name="Meeting_Management_3"></a>Meeting Management
    </h4>
    <p>The Marketing Committee meets at least twice per year.</p>
  </div>
</div>