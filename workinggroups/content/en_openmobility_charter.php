<?php
/**
 * Copyright (c) 2018, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *   Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <div class="alert alert-danger">
      <strong><?php print $pageTitle;?> has been archived.</strong>
    </div>
    <p>Version v1.1 Revision history is at end of document.</p>
    <p>November, 2020</p>
    <h2>Vision and Scope</h2>
    <p>The openMobility working group will drive the evolution and broad
      adoption of mobility modelling and simulation technologies. It
      accomplishes its goal by fostering and leveraging collaborations among
      members and by ensuring the development and availability of the
      “openMobility framework”. This framework will provide the tools for a
      detailed simulation of the movement of vehicles and people as well as
      their communication patterns. It will be valuable for testing driver
      assistance systems, predicting and optimizing traffic as well as
      evaluating new business concepts, such as Mobility-as-a-Service.</p>
    <p>It is the goal of this working group to deliver a framework of tools
      based on validated models, which are accepted as “standard tools” in
      industry applications and academia. Eclipse Foundation projects related to
      the openMobility working group will provide technical implementations of
      API specifications. The working group will:</p>
    <ul>
      <li>Coordinate the development of related Eclipse Foundation projects towards an openMobility framework.</li>
      <li>Define and manage the specifications for interfaces and functions for the framework.</li>
      <li>Promote the openMobility framework and its value.</li>
      <li>Provide a vendor neutral marketing and other services to the openMobility ecosystem.</li>
      <li>Define licensing and intellectual property flows that encourage community participation and tool adoption.</li>
      <li>Drive the funding model that enables this working group and its community to operate on a sustainable basis.</li>
    </ul>
    <h2>Technical Scope</h2>
    <p>The openMobility working group will coordinate the development of Eclipse Foundation projects on the following topics:</p>

    <ul>
      <li>Microscopic and Mesoscopic Traffic Simulation</li>
      <li>Vehicle-to-Vehicle and Vehicle-to-Infrastructure Communication Simulation</li>
      <li>Pedestrian Dynamics Simulation</li>
      <li>Simulator Coupling infrastructure</li>
      <li>Interfaces to Advanced Driver Assistance Systems and Intelligent Transportation Systems solutions</li>
    </ul>
    <p>Projects hosted by the Eclipse Foundation in the context of the
      openMobility working group apply the Eclipse development process and best
      practices of openness, transparency and meritocracy. As such, these
      projects are open to participation by developers or contributors. They can
      submit contributions to the project that will be considered for inclusion
      by the project committers. Committer status is acquired by contributors
      after election by existing committers as described in the Eclipse Foundation
      Development Process.</p>
    <h2>Incubation Period</h2>
    <p>The openMobility working group starts with an incubation period of one
      year that begins after the community review has been successfully
      accomplished. The goal of the incubation period is to:</p>
    <ul>
      <li>Refine the structure and strategy of the working group</li>
      <li>Define membership levels and participation guidelines</li>
      <li>Acquire new members for the working group</li>
      <li>Develop and establish a funding model that enables this working group and its community to operate on a sustainable basis.</li>
    </ul>
    <p>During the incubation period, only two membership classes and one
      governance body are defined (see “Membership”). Participant members and
      Committers of associated Eclipse Foundation projects are expected to
      actively engage in the development of this working group and the
      acquisition of new members. The participation fees for the working group
      will be waived during this period (0 € / USD 0 for all members).</p>
    <p>The incubation period is concluded successfully, if at least four
      Participant members have been acquired and a funding model for the group
      has been developed and agreed upon. From here on, the working group shall
      continue with an updated charter.</p>
    <p>If the operation of this working group cannot be ensured on a sustainable basis, it will be terminated after the incubation phase.</p>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
    </ul>
    <p>All Members of the working group must be parties to the Eclipse Foundation Membership
      Agreement, including the requirement set forth in Section 2.2 to abide by and adhere to
      the Bylaws and then-current policies of the Eclipse Foundation, including
      but not limited to the Intellectual Property and Antitrust Policies.</p>
    <p>In the event of any conflict between the terms set forth in this working
      group's charter and the Eclipse Foundation Bylaws, Membership Agreement,
      Development Process, Working Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation Bylaws, Membership Agreement, process, or policy shall take precedence.</p>
    <h2>Membership</h2>
    <p>In order to participate in the openMobility working group, an entity must
      be at least a <a href="https://www.eclipse.org/membership/#tab-levels">Contributing Member</a>​ of the Eclipse Foundation, have executed
      the openMobility Working Group Participation Agreement, and adhere to the
      requirements set forth in this Charter.</p>
    <p>The openMobility working group is open at any time to all new members who meet these conditions.</p>
    <p>During the incubation phase, there are two classes of openMobility
      working group membership - Participant and Committer. Each of these
      classes is described in detail below.</p>
    <h3>Classes of openMobility Membership</h3>
    <h4>Participant Members</h4>
    <p>Participant Members are typically organizations that view an open
      mobility simulation framework as strategic to their organization and are
      willing to invest significant resources to sustain and shape the
      activities of this working group. Typical Participant Members include
      industry users of the technologies and results provided by the
      openMobility working group.</p>
    <h4>Committer</h4>
    <p>Committer Members are individuals who through a process of meritocracy
      defined by the Eclipse Development Process are able to contribute and
      commit code to the Eclipse Foundation projects included in the scope of
      this working group. Committers may be members by virtue of working for a
      member organization, or may choose to complete the membership process
      independently if they are not. For further explanation and details, see
      the <a href="https://www.eclipse.org/membership/become_a_member/committer.php">“Eclipse Committer Membership”</a> ​page.</p>

    <h2>Governance</h2>
    <p>This openMobility working group is designed as:</p>
    <ul>
      <li>a member driven organization,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of components and service providers,</li>
      <li>a means to organize the community of each project or component so that users and developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the “Steering Committee” as the
      sole governance body for the working group during the incubation phase has
      been defined.</p>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this working group. </li>
      <li>Define and manage the roadmap of the working group.</li>
      <li>Review this charter.</li>
      <li>Define the annual fees for all classes of working group membership.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Contribute to the acquisition of new working group members by actively engaging in community development and evangelism</li>
    </ul>

    <h4>Composition</h4>
    <ul>
      <li>Each Participant Member of the working group has one seat on the Steering Committee.</li>
      <li>Two seats are allocated to Committer Members. Committer Member seats
        are allocated following the Eclipse "Single Transferable Vote", as
        defined in the Eclipse Foundation Bylaws. Elected Committer Members have the right
        to participate in every Steering Committee meeting. They have no voting
        rights.</li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>
    <h2>Common Dispositions</h2>
    <p>The dispositions below apply to all governance bodies for this working
      group, unless otherwise specified. For all matters related to membership
      action, including without limitation: meetings, quorum, voting, electronic
      voting action without meeting, vacancy, resignation or removal, the respective terms
      set forth in the Eclipse Foundation Bylaws apply.</p>
    <h3>Voting</h3>
    <p>For actions (i) approving specifications for adoption; (ii) approving or
      changing the name of the working group; and (iii) approving changes to
      annual Member contribution requirements; any such actions must be approved
      by no less than two-thirds (2/3) of the representatives present at a
      committee meeting.</p>
    <h3>Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee. All representatives
      shall hold office until their respective successors are appointed or
      elected, as applicable. There shall be no prohibition on re-election or
      re-designation of any representative following the completion of that
      representative’s term of office.</p>
    <p>Elected representatives shall each serve one-year terms and shall be
      elected to serve from May 13 to May 12 of each calendar year, or until
      their respective successors are elected and qualified.</p>
    <h3>Meetings Management</h3>
    <p>All meetings may be held at any place that has been designated by
      resolution of the corresponding body. All meetings may be held remotely
      using phone calls, video calls or any other means as designated by
      resolution of the corresponding body.</p>
    <h3>Infrastructure</h3>
    <p>The openMobility Working Group uses the normal infrastructure provided to
      Eclipse projects, including, among others, mailing lists, forums, bug
      trackers, source code repositories, continuous integration servers, build
      servers, and web sites.</p>
    <h3>Community Building</h3>
    <p>The Eclipse Foundation will provide access to its network of developers
      to facilitate the community building and the dissemination of the
      openMobility innovations. The Eclipse Foundation will use its
      communication channels such as its Newsletter, Twitter, or LinkedIn
      channels to promote key openMobility events and encourage its members to
      join and participate in the openMobility working group.</p>
  </div>
  <ul>
    <li>V1.1 Updates in support of the Eclipse Foundation corporate restructuring</li>
    <li>V1.0 updated May 21, 2019 - dates changed in section “Term and Dates of Elections”</li>
    <li>v0.1 created 2018</li>
  </ul>
</div>