<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>
<h2>Fostering open industry collaboration to develop new industry platforms</h2>
<p>
  Companies, organizations, and individuals looking to drive innovation and efficiencies within industry are increasingly looking to external sources to advance new ideas. Commonly referred to as “open innovation,” this paradigm encourages collaboration across organizational boundaries, and is often practiced in the open source community. Working groups allow organizations to combine the best practices of open source development with a set of services required for open innovation, enabling organizations to foster industry collaborations.
</p>
<p>
  At the Eclipse Foundation, working groups provide a vendor-neutral governance structure that allows members of the foundation to collaborate on solving industry problems and to drive shared innovation through the related Eclipse Foundation open source projects. The collaboration promotes and augments Eclipse technologies to meet the needs of specific industries and communities, including through the development and implementation of joint marketing programs to increase awareness, adoption, and contributions. Find out more about the <a href="/org/workinggroups/operations.php#relationship-with-open-source-projects">relationship between working groups and projects</a>. 
</p>
<p>
  Through working groups, member organizations can collaborate to:
</p>
<ul>
  <li>help improve the supply chain of software development tools, frameworks, and runtimes in a particular industry</li>
  <li>develop specifications in open source</li>
  <li>create new technology platforms that increases interoperability among organizations and technologies</li>
  <li>drive the industry adoption and support of strategic open source projects and technologies, including developer advocacy, marketing outreach, brand promotion, compatibility program, etc. </li>
</ul>
<p>Participation in a working group is only open to Eclipse Foundation members.</p>

<h2 id="wg-services" style="font-size: 27px">Services for Working Groups</h3>
<p>
  The Eclipse Foundation provides working groups with <a href="#wg-neutral">vendor-neutral governance</a> , <a href="#wg-eco">ecosystem development and marketing services</a>, <a href="#wg-collaborative">collaborative management</a>, <a href="#wg-spec">specification development</a> and <a href="#wg-branding">branding and compatibility</a>.
</p>
<p>
  The foundation also provides <a href="/org/services/#IP_Management">intellectual property management and licensing</a>, shared <a href="/org/services/#IT">IT infrastructure</a>, and <a href="/org/services/#Development">development processes</a> for the projects complemented and promoted by the working groups.
</p>

<h3 id="wg-neutral">Vendor-Neutral Governance</h3>
<p>
  Good governance controlling how decisions are made, policies are set and disputes resolved is important for any successful collaboration.
</p>
<p>
  The Eclipse Foundation, a not-for-profit corporation, is governed by a set of Bylaws, agreements, and policies, including antitrust and intellectual property management policies, that ensure a vendor-neutral governance model for working group collaborations.
</p>
<p>
  All entities in a working group participate based strictly on their contributions and merit. Working groups are able to define rights based on membership within a working group. This ensures that no one individual or organization has any special status or veto.
</p>

<h3 id="wg-eco">Ecosystem Development and Marketing</h3>
<p>
  Working groups are most effective when a diverse group of organizations get involved and collaborate. The Eclipse Foundation provides marketing expertise, processes, and platforms to enable working group members to jointly grow the awareness of and participation in working groups and their projects.
</p>
<p>
  Our team of marketing professionals support working groups with brand development, press and analyst relations, content creation (e.g. white papers, case studies, eBooks), event coordination, advertising, social media management, and other working group-specific activities. Find out more about the <a href="/org/services/marketing">marketing services</a> provided by the Foundation.
</p>

<h3 id="wg-collaborative">Collaborative management</h3>
<p>
  There is a tight coupling between working groups and open source projects hosted at the Eclipse Foundation that is very valuable to the open source community. Working groups can help coordinate the efforts of otherwise separate open source projects by providing things like shared vision and a roadmap; and determine how to leverage working group resources to best serve the open source community. The Foundation helps set up shared responsibilities, manages budgets and provides governance for these shared initiatives. 
</p>
<p>
  The <a href="https://marketplace.eclipse.org/">Eclipse Marketplace</a> and <a href="https://open-vsx.org/">OpenVSX registry</a> are concrete examples of cross-project initiatives that working groups have built with the collaborative management offered by the Eclipse Foundation.
</p>

<h3 id="wg-spec">Specification Development</h3>
<p>
  A specification provides definitions, descriptions of semantic behavior, data formats, protocols, and/or other referenced specifications, along with technical artifacts, intended to enable the development and verify compatibility of independent implementations.
</p>
<p>
The <a href="/projects/efsp/">Eclipse Foundation Specification Process</a> (EFSP) defines a general framework for the development of specifications in open source at the Eclipse Foundation. All specification development occurs under the purview of a working group that — through their designated specification committee — plays a critical role in the general governance, and management of the intellectual property flows that are particular to specifications.
</p>

<h3 id="wg-branding">Branding and Compatibility</h3>
<p>
  Working groups can build on the value of their specifications and develop a trusted ecosystem of implementers and consumers, by creating a branding and compatibility program. . Products that implement specifications and pass the corresponding Technology Compatibility Kit (TCK) can be branded as compatible implementations. Consumers can trust that branded products are truly compatible.
</p>

<h3>Join a Working Group</h3>
<p><a href="/org/workinggroups/explore.php">Explore our working groups</a> to learn more about their vision and scope, and how to join.</p>