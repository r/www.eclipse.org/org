<?php
/**
 * Copyright (c) 2014, 2018, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Andrea Ross (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *   Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <div class="alert alert-danger">
      <strong><?php print $pageTitle;?> has been archived.</strong>
    </div>
    <p>November 2020 - Revision history at end.</p>
    <h2>Purpose</h2>
    <p>The Science Working Group (SWG) works to solve the problems of making science software
      inter-operable and interchangeable.</p>
    <p>The SWG, hosted by the Eclipse Foundation, is a collaborative effort involving industry,
      academia, and government to develop reusable open source software for scientific research. The
      SWG provides governance and infrastructure to establish common principles, and enable a
      collaborative approach to producing technologies used for interdisciplinary analysis of
      scientific data. The group may package components to regularly release a trusted distribution
      of software. Group efforts help permeate concepts and capabilities to different research areas
      which allows serendipitous discoveries to be made.</p>
    <p>Interoperability is essential for scientific research. To address this issue, the SWG will
      strive to collaboratively develop well defined interfaces, models, definitions, algorithms,
      and reusable software libraries, services, and applications.</p>
    <p>The SWG will strive to become an essential forum for engagement with industry including
      adoption of SWG output and contribution to SWG initiatives.</p>
    <h2>Scope</h2>
    <p>The SWG is language and platform agnostic and develops software in the following domains:</p>
    <ul>
      <li>Standard descriptions and definitions of scientific data.</li>
      <li>Processing and management of 1D, 2D and 3D data including both structured and unstructured
        grids.</li>
      <li>Plotting and visualizations of data in 1D, 2D and 3D.</li>
      <li>Workflow algorithms and their visualization.</li>
      <li>Machine learning, artificial intelligence and data mining.</li>
      <li>Modeling and simulation projects related to the physical sciences, including but not
        limited to physics, chemistry, biology, geology, and hydrology.</li>
      <li>Modeling and simulation projects related to the social sciences, including but not limited
        to sociology and psychology.</li>
      <li>Applied mathematics projects such as common math libraries and mesh management tools and
        with the exception of cryptography.</li>
      <li>Infrastructure to support scientific computing, such as tools for job launching and
        monitoring, parallel debugging, and remote project management.</li>
      <li>Control systems for analytical hardware.</li>
    </ul>
    <h2>Connections with other groups</h2>
    <p>The SWG will also work in collaboration with other related groups and Standards organizations
      in order to avoid fragmentation and duplication of effort. A particularly close relationship
      is anticipated with other Eclipse Foundation working groups such as LocationTech and IoT.</p>
    <p>The SWG acknowledges the contribution of projects such as the Eclipse Parallel Tools Platform
      (PTP) project, which has been developing infrastructure and tools for scientific computing for
      over a decade. The SWG intends to utilize and build upon infrastructure of this nature
      wherever possible.</p>
    <h2>Governance and Precedence</h2>
    <p>All SWG members must be parties to the Eclipse Foundation Membership Agreement, including the
      requirement set forth in Section 2.2 to follow the Bylaws and then-current policies of the
      Eclipse Foundation, including but not limited to the Intellectual Property and Antitrust Policies.</p>
    <p>Online voting will be used as a mechanism for the steering committee to make decisions. A
      vote may be organized by any member. Voting rights are described below. A vote is a way of
      gauging preference from active members of the group.</p>
    <p>In the event of any conflict between the terms set forth in this SWG Charter and the Eclipse
      Foundation Bylaws, Membership Agreement, Development Process, Working Group
      Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation
      Bylaws, Membership Agreement, process, or policy shall take precedence.</p>
    <h2>Applicable documents</h2>
    <p>The following governance documents are applicable to this charter, each of which can be found on the <a href="https://www.eclipse.org/org/documents/">Eclipse Foundation Governance Documents page</a> or the <a href="https://www.eclipse.org/legal/">Eclipse Foundation Legal Resources page</a>:</p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li> 
    </ul>
    <h2>Participation guidelines and membership</h2>
    <p>There are 4 levels of participation in the SWG. The rights and responsibilities are as
      follows.</p>
    <h3>Guest Members</h3>
    <p>Guest members are persons or organizations of note that are invited by the Steering Committee
      to participate for a 1 year term, without dues, with the following obligations. Guest
      membership is intended as a prelude to full participation.</p>
    <p>Guest members are required to:</p>
    <ul>
      <li>Be an Eclipse Foundation member in good standing.</li>
      <li>Regularly participate in the Working Group meetings.</li>
      <li>Guest members are non-voting members.</li>
    </ul>
    <h3>Participating Members</h3>
    <p>Participant Members are organizations that view the SWG technology as an important part of
      their organization's activities. These organizations want to participate in the development of
      the ecosystem.</p>
    <p>Participating members are required to:</p>
    <ul>
      <li>Be an Eclipse Foundation member in good standing.</li>
      <li>Regularly participate in the Working Group meetings.</li>
      <li>Participant members are voting members.</li>
    </ul>
    <h3>Steering Committee Members</h3>
    <p>Steering Committee Members are organizations that view SWG technology as strategic to their
      organization and are investing resources to sustain and shape the activities of this group.</p>
    <p>Steering committee members are required to:</p>
    <ul>
      <li>Be a Strategic or Contributing member of the Eclipse Foundation in good standing.</li>
      <li>Provide sustained year-over-year development from two or more persons, that contribute to
        the activities of the group. The activities can include:</li>
      <ul>  
        <li>committers working on Eclipse open source projects that support the group’s roadmap</li>
        <li>creation of roadmaps and technical documents (including, for example, architectures, blueprints and best practices) supporting the group’s work</li>
        <li>evangelism and community development for the working group and other activities agreed to by the Steering committee</li>
      </ul> 
      <li>Participate in the working group meetings and provide timely feedback on the working group
        documents.</li>
      <li>Provide announcement support for working group announcements, releases, and other output.</li>
    </ul>
    <p>Steering Committee members are voting members.</p>
    <p>Steering Committee members also:</p>
    <ul>
      <li>Nominate and vote to invite Guest and Participant members (without dues).</li>
      <li>Identify and vote to remove members.</li>
      <li>Propose and vote on SWG charter modifications.</li>
    </ul>
    <h2>Committer Members</h2>
    <p>SWG projects are full fledged Eclipse Foundation projects, and committers for SWG projects
      have all the rights and privileges that entails.</p>
    <p>In addition, the SWG Steering Committee will have 1 committer representative nominated and
      elected by their SWG committer peers. 1 additional elected committer representative will be
      added for every 3 Steering Committee members. SWG elections shall use single transferable
      voting.</p>

    <p><strong>Charter Version History</strong></p>
    <ul>
      <li>November, 2020 - Updates in support of the Eclipse Foundation corporate restructuring</li>
    </ul>
  </div>
</div>