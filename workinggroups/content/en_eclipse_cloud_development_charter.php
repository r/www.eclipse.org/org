<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 1.9 Revision history is at end of document.</p>
    <h2>Scope, Goals&nbsp;and Vision</h2>
    <p>The Eclipse Cloud Development Tools Working Group drives the evolution and broad adoption of
      de facto standards for cloud development tools, including language support, extensions, and
      developer workspace definition. In addition, the group explores the impacts and optimizations
      for running developer tools in a cloud environment, including scale out / scale-to-zero
      execution of compute-intensive tasks like CI and testing.</p>
    <p>The working group:</p>
    <ul>
      <li>Engages with the Eclipse and broader developer community to ensure broad adoption of
        emerging standards for cloud-based developer tools</li>
      <li>Coordinates interoperability work on Language Server Protocol&nbsp;(LSP), Debug Adapter
        Protocol&nbsp;(DAP), and other developer tool protocols (workspace lifecycle management,
        source control, continuous integration infrastructure, user management, etc), and propose
        evolutions to these protocols for additional use-cases</li>
      <li>Defines requirements for running developer workspaces, including runtime environments and
        developer tools, in a Cloud Development Environment</li>
      <li>Promotes the adoption of developer tool plugins, and plugin marketplaces to Eclipse Cloud Developer projects</li>
      <li>Collaborates on the joint promotion of standards adoption among Eclipse Cloud Developer
        projects</li>
    </ul>
    <p>The Eclipse Cloud Development Tools Working Group (ECD WG) accelerates the adoption of Cloud
      IDE and container-based workspace management, through the adoption of standards, the
      engagement with 3rd party developer tool providers, and through the promotion of Eclipse
      projects to cloud developers.</p>
    <h2>Definitions</h2>
    <p>
      <strong>Eclipse Cloud Development</strong>
    </p>
    <p>Is an Eclipse top-level project, with a focus on developing applications for and in the
      cloud.</p>
    <p>
      <strong>Web IDE</strong>
    </p>
    <p>Is an Integrated Development Environment whose front-end can run in a web browser.</p>
    <p>
      <strong>Cloud Developer Workspace and IDE</strong>
    </p>
    <p>Is a complete IDE, including developer run-time and developer tools, which runs entirely in a
      cloud environment.</p>
    <p>
      <strong>Workspace Management Solution</strong>
    </p>
    <p>Is a mechanism for composing and managing a complete developer&rsquo;s environment, including
      source code, language run-times and libraries, and developer tools, including debuggers,
      profilers, and other developer tools, in containers, deployed and managed on a cloud platform.</p>
    <p>
      <strong>Rapid Application Development (RAD)</strong>
    </p>
    <p>Is an approach for adaptive software development focused on increasing productivity during
      the development process and to shorten the time to market for ready to use solutions.
      Techniques such as modelling of business processes, domain models, generation by templates,
      data-driven accelerators, DSLs, No-Code/Low-Code tooling, etc. in the Cloud context falls in
      this category and are also in scope for the ECD WG.</p>
    <p>
      <strong>Eclipse Members</strong>
    </p>
    <p>
      Eclipse Members have signed the Eclipse Membership Agreement and are in good standing. For a
      complete overview over the Eclipse Membership process and the current Eclipse members refer to
      the<a href="https://eclipse.org/membership">&nbsp;Eclipse Membership Page.</a>
    </p>
    <p>
      <strong>ECD&nbsp;WG Participants</strong>
    </p>
    <p>ECD WG participants have signed the ECD WG Participation Agreement and have fulfilled the
      requirements as stated in the WG Participation Agreement.</p>
    <p>
      <strong>ECD WG Deliverables and Results</strong>
    </p>
    <p>The services and goods (software, documents, marketing plans and collateral, etc.) to be
      delivered hereunder are referred to in this document as ECD WG deliverables and results.</p>
    <h2>Governance and Precedence</h2>
    <h3>Applicable Documents</h3>
    <p>
      The following governance documents are applicable to this charter, each of which can be found
      on the <a href="/org/documents">Eclipse Foundation Governance Documents page</a>
      or the <a href="/legal">Eclipse Foundation Legal Resources page</a>:
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership Agreement, including the
      requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current
      policies of the Eclipse Foundation, including but not limited to the Intellectual Property and
      Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this Working Group's Charter
      and the Eclipse Foundation Bylaws, Membership Agreement, Development Process, Working
      Group Process, or any policies of the Eclipse Foundation, the terms of the Eclipse Foundation
      Bylaws, Membership Agreement, process, or policy shall take precedence.
    </p>
    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described
      below, an entity must be at least a Contributing Member
      of the Eclipse Foundation, have executed the Eclipse
      Cloud Development Tools Working Group Participation
      and adhere to the requirements
      set forth in this Charter to participate.
    </p>
    <p>
      The participation fees associated with each of these
      membership classes are shown in the Annual
      Participation Fees section. These are annual fees, and are
      established by the Eclipse Cloud Development Tools
      Working Group Steering Committee, and will be updated
      in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse
      Foundation are separate from any Working Group
      membership fees, and are decided as described in the
      Eclipse Foundation Bylaws and detailed in the Eclipse
      Foundation Membership Agreement.
    </p>
    <p>There are two classes of ECD WG membership - Participant and Guest. Each of
      these classes is described in detail below.</p>
    <h3>Classes of Membership</h3>
    <h4>Participant Members</h4>
    <p>
      Participant Members want to influence the definition and further
      development of the ECD ecosystem. Consider interoperability and standards
      adoption related to ECD a relevant addition to their business. They are either
      users of the related technology and service offerings or they are interested in
      adoption, extension and service provisioning for one or more ECD projects.
      Participant Members of this working group must be at least a Contributing
      Member of the Eclipse Foundation.
    </p>
    <h4>Guest Members</h4>
    <p>
      Guest Members are organizations which are Associate members of the
      Eclipse Foundation. Typical guests include R&amp;D partners, universities,
      academic research centers, etc. Guests may be invited to participate in
      committee meetings at the invitation of the respective committee, but
      under no circumstances do Guest members have voting rights. Guest
      members are required to execute the Working Group&apos;s Participation
      Agreement.
    </p>

    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within the Working Group with a
      focus to collaborate around a particular topic or domain of direct interest to the working group.
      SIGs are designed to drive the objectives of a subset of the Members of the Working Group in
      helping them achieve a dedicated set of goals and objectives. The scope of the SIG must be
      consistent with the scope of the Working Group Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each SIG may be either
      temporary or a permanent structure within the working group. SIGs can be disbanded at any
      time by self selection presenting reasons to and seek approval from the Steering Committee.
      Steering Committees may disband a SIG at any time for being inactive or non compliant with the
      Working Group's Charter, or by request of the SIG itself. SIGs operate as a non-governing
      Body of the Working Group. There are no additional annual fees to Members for participation in
      a SIG.
    </p>
    <h2>Sponsorship</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to the working group on
      an ad hoc basis to support the activities of the Working Group and its managed projects. Money
      or services provided by sponsors are used as set forth in the working group annual budget. The
      working group is free to determine whether and how those contributions are recognized. Under
      no condition are sponsorship monies refunded.
    </p>
    <p>Sponsors need not be members of the Eclipse Foundation or of the Working Group.</p>

    <h2>Governance</h2>
    <p>The Eclipse Cloud Development Tools Working Group is designed as:</p>
    <ul>
      <li>a member driven organization, led by its members,</li>
      <li>a means to foster a vibrant and sustainable ecosystem of users, contributors and service providers,</li>
      <li>a means to organize the community of each project or component so that members and developers define the roadmap collaboratively.</li>
    </ul>
    <p>In order to implement these principles, the following governance bodies have been defined (each a "Body"):</p>
    <ul>
      <li>Steering Committee that includes the responsibilities of the Marketing Committee.</li>
    </ul>

    <h2>Governing Bodies</h2>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this working group.</li>
      <li>Ensure the consistency of logo usage and other marketing materials.</li>
      <li>Define and manage the technical roadmap.</li>
      <li>Review and approve this charter.</li>
      <li>Review and approve any trademark policy referred to it by the Marketing and Brand
        Committee.</li>
      <li>Define the annual fees for all classes of working group membership.</li>
      <li>Establish an annual program plan.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Approve the creation of subcommittees and define the purpose, scope, and membership of each such subcommittee.</li>
      <li>Approve the creation and retirement of Special Interest Groups (SIGs)</li>
    </ul>
    <h4>Composition</h4>
    <ul>
      <li>
        Each Participant Member of the working group is entitled to a seat on
        the Steering Committee.
      </li>
      <li>
        The Committee elects a chair of the Steering Committee. This chair is elected among
        the members of the Committee. They will serve for a 12 month period or until their
        successor is elected and qualified, or as otherwise provided for in this Charter. There is
        no limit on the number of terms the chair may serve.
      </li>
    </ul>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all meetings related to the
      working group will follow a prepared agenda and minutes are distributed two weeks after the
      meeting and approved at the next meeting at the latest, and shall in general conform to the
      Eclipse Foundation Antitrust Policy.
    </p>

    <h2>Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this working group, unless otherwise
      specified. For all matters related to membership action, including without limitation: meetings, quorum,
      voting, vacancy, resignation or removal, the respective terms set forth in the Eclipse Foundation Bylaws apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a
      Body member is unavailable to attend or participate in a meeting of the Body, they may be
      represented by another Body member by providing written proxy to the Body&apos;s mailing list in
      advance. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed
      from the Body upon the termination of the membership of such representative&apos;s Member
      organization.
    </p>
    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body
      are determined by a simple majority of the representatives represented at a committee meeting
      at which a quorum is present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors approve a specification
      license; (ii) approving specifications for adoption; (iii) modifying the working group charter; (iv)
      approving or changing the name of the working group; and (v) approving changes to annual
      Member contribution requirements; any such actions must be approved by no less than two-thirds (2/3) of the representatives represented at a committee meeting at which a quorum is
      present.
    </p>
    <h3>Term and Dates of Elections</h3>
    <p>This section only applies to the Steering Committee and the Marketing Committee.</p>
    <p>
      All representatives shall hold office until their respective successors are appointed or elected, as
      applicable. There shall be no prohibition on re-election or re-designation of any representative
      following the completion of that representative's term of office.
    </p>
    <h4>Elected Representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve for a 12
      month period, or until their respective successors are elected and qualified, or as otherwise
      provided for in this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided that such resolutions
      are not inconsistent with any provision of this Charter.
    </p>

    <h3>Meeting Frequency</h3>
    <p>Each&nbsp;governing body meets at least twice a year. All meetings may be held at any
      place that has been designated from time-to-time by resolution of the corresponding Body. All
      meetings may be held remotely using phone calls, video calls, or any other means as designated
      from time-to-time by resolution of the corresponding Body.</p>
    <h3>Place of Meetings</h3>
    <p>All meetings may be held at any place that has been designated from time-to-time by
      resolution of the corresponding body. All meetings may be held remotely using phone calls,
      video calls, or any other means as designated from time-to-time by resolution of the
      corresponding body.</p>
    <h3>Regular Meetings</h3>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representatives at least fifteen (15) calendar days prior to such meeting;
      which notice will identify all potential actions to be undertaken by the Body at the Body meeting.
      No representative may be intentionally excluded from Body meetings and all representatives
      shall receive notice of the meeting as specified above; however, Body meetings need not be
      delayed or rescheduled merely because one or more of the representatives cannot attend or
      participate so long as at least a quorum of the Body is represented at the Body meeting.
    </p>
    <h3>Actions</h3>
    <p>The body may undertake an action only if it was identified in a body meeting notice or
      otherwise identified in a notice of special meeting.</p>
    <h3>Invitations</h3>
    <p>The body may invite any ECD WG member to any of its meetings. These invited attendees have no
      right of vote.</p>
    <h2>Eclipse Cloud Development Tools Working Group Fees</h2>
    <p>
      Established fees cannot be charged retroactively and any fee structure change will be
      communicated to all members via the mailing list and will be charged the next time a
      member is invoiced.
    </p>
    <h3>Eclipse Cloud Development Tools Working Group Annual Participation Fees - Schedule A</h3>
    <p>The annual fees have been established by the Eclipse Cloud Development Tools Steering Committee as follows:</p>
    <h4>Participation Member Annual Participation Fees</h4>
    <p>Participant Members are required to execute the Eclipse Cloud&nbsp;Development Working Group Participation
      Agreement.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="80%">Corporate Revenue</th>
          <th class="text-center" width="20%">Annual Fees&nbsp;
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;1&nbsp;billion</td>
          <td class="text-center">&euro;15,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;500 million but less than or equal to &euro;1&nbsp;billion</td>
          <td class="text-center">&euro;10,000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;100 million but less than or equal to &euro;500&nbsp;million</td>
          <td class="text-center">&euro;7,500&nbsp;</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to &euro;100&nbsp;million</td>
          <td class="text-center">&euro;2,000</td>
        </tr>
      </tbody>
    </table>
    <p>
      Guest members pay no Working Group Participation Fees, but are required to execute the Eclipse Cloud
      Development Working Group Participation Agreement.
    </p>

    <p><strong>Version History</strong></p>
    <ul>
      <li>1.9 Steering Committee assumes the responsibility of the Marketing Committee May 2023</li>
      <li>1.8 Removal of Strategic membership class, adjust fees to Euros and other standard refinements April 13, 2023</li>
      <li>1.7 Removal of three year commitment for Strategic Members and other standard refinements April 26, 2022</li>
      <li>1.6 Updates in support of the Eclipse Foundation corporate restructuring February 4, 2021</li>
      <li>1.5 Updates to fee schedule as per Steering Committee resolution January 23, 2020</li>
      <li>1.0 Ratified by ECD Tools Steering Committee October 3, 2019</li>
      <li>Version 0.9 - Community Draft</li>
      <li>Version 0.8 - Community Draft</li>
    </ul>
  </div>
</div>