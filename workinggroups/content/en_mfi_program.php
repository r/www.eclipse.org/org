<?php
/**
 * Copyright (c) 2017, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Last Updated: December 19, 2017</p>
    <p>As a benefit of membership, the Eclipse Foundation accepts funding from its members, either
      directly or through its industry working groups, and uses these funds to organize, coordinate,
      and oversee a funded initiative on the member&rsquo;s behalf. The Member Funded Initiatives
      Program, described herein, defines the process for accepting and managing these funding
      contributions.</p>
    <h2>Background and Motivation</h2>
    <p>The predominance of development on all Eclipse projects is done by, and/or for the benefit
      of, one or more of our member companies. Communities are built around these projects, and the
      development work on projects happens without any action or engagement taken by the Eclipse
      Foundation.</p>
    <p>Individual member companies often collaborate directly with each other on common initiatives,
      with each of those members funding the various aspects of the projects. Again, the Foundation
      is not directly engaged in this work.</p>
    <p>
      The same is true for <a href="/org/workinggroups/">Eclipse Working Groups</a>, which are
      governance structures created to enable collaborative development and promotion of Eclipse
      projects in a particular vertical or area of interest to the benefit of the working group
      member companies. While the rules for governance are defined by the working group&rsquo;s
      charter, the specific work done on the Eclipse projects that are related to a working group
      does not directly involve the Foundation.
    </p>
    <p>However, there are times when member companies would prefer to have Eclipse Foundation, or
      one of its working groups, lead an initiative, rather than to do so themselves.</p>
    <p>As an example, there are often technologies or capabilities that sit outside the main thrust
      of the mainstream projects that are important to the overall growth of the broader community.
      This can be general capabilities, shared libraries, etc. Funding to support development of
      these capabilities is typically not seen as the responsibility of any particular member, and
      often it is difficult to find creative ways for these technologies and capabilities to get
      developed. In many of these cases, it may be desirable to have the working group direct
      funding from its annual budget to ensure the development gets done.</p>
    <p>As a second example, multiple member companies may wish to &ldquo;pool&rdquo; their financial
      resources to see a shared technology get developed for their direct benefit. That is, rather
      than entering into a separate collaboration agreement with each other to jointly fund a
      project, the member companies would prefer to contribute their share of the cost of a
      development initiative managed by the Foundation or a working group.</p>
    <p>Funding initiatives are not limited to just development tasks. There are also occasions where
      member companies wish to contribute funds to drive, for example, marketing and communications
      initiatives, or to drive community development activities over an extended period of time.</p>
    <h2>Funding Mechanism Details - MFI Agreements</h2>
    <p>Funding to support the Eclipse Foundation carrying out a particular initiative can come from
      any of:</p>
    <ol class="alpha">
      <li>an individual member company wishing to fund an initiative by itself,</li>
      <li>a group of member companies wishing to contribute jointly to a funded initiative, or</li>
      <li>a working group&rsquo;s overall annual budget.</li>
    </ol>
    <p>
      In the case of funding coming from an individual member company or a group of companies, each
      participating member company will enter into an <strong>Eclipse Foundation Member Funded
        Initiative Agreement</strong> (&ldquo;MFI Agreement&rdquo;) with the Foundation. The MFI
      Agreement will specify, at a minimum, the amount of the contribution, the initiative being
      supported, and the desired outcome. In the case of &ldquo;pooled initiatives&rdquo;, each
      member company will enter into an individual MFI Agreement with the Foundation, with a note
      indicating these funds are being combined with other funds to enable the common initiative to
      proceed.
    </p>
    <p>In the case of funding coming from a working group&rsquo;s annual budget, a representative of
      the working group steering committee will make a formal request to the Foundation to initiate
      the MFI. This request must be in accordance with the working group charter, and within its
      approved budget. In this case, the request must state the amount of the commitment and the
      initiative being supported. Unlike funding contributions made by member companies, no MFI
      Agreement will be required when funding comes from the working group budget, as the working
      group is managed directly by the Eclipse Foundation. Rather, a Working Group MFI Statement of
      Work will be agreed to by the steering committee representative. As part of the formal working
      group process, it is expected that all MFI Statements of Work be read into the minutes of the
      working group to ensure maximum transparency among all members.</p>
    <p>In all cases, the Eclipse Foundation will begin an associated Funded Initiative Effort, as
      described below, once the terms are finalized and the contributions are received.</p>
    <p>For its efforts and to help sustain and enhance the MFI Program, beginning January 1, 2018,
      the Eclipse Foundation will charge the fees as shown in Table 1. These fees are charged
      annually, and for each funded initiative. In the case of working groups, each working
      group&rsquo;s funded initiatives will be treated as one.</p>
    <div class="row">
      <div class="col-md-12 col-md-offset-6">
        <table class="table table-bordered table-hover text-center">
          <thead>
            <tr>
              <th class="text-center">Table 1: Annual Fee (per initiative or working group)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>15% of the 1st $500,000 in contributions (min $4000)</td>
            </tr>
            <tr>
              <td>10% of the next $1,500,000 in contributions</td>
            </tr>
            <tr>
              <td>6% of any contributions thereafter</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <p>MFI Agreements will not guarantee the delivery of any particular service or capability, but
      instead will state the intention to do so on a best efforts basis. Note that should Eclipse
      Foundation be unable to carry out the initiative (for example, a suitable subcontractor to
      carry out the development effort cannot be found) within 6 months of receiving a contribution,
      then the funds shall be returned to the funding organization, the MFI Agreement terminated,
      and Eclipse Foundation will charge no fee for their service.</p>
    <h2>Funded Initiative Efforts</h2>
    <p>Eclipse Foundation will, in most cases, engage with members of the Eclipse community and
      other experts, to fulfill the tasks associated with the initiative. For example, the
      Foundation will subcontract with the qualified developers from the community to fulfill
      initiatives that call for development of code.</p>
    <p>All development work subcontracted by the Foundation will be done in accordance with the
      Eclipse Development Process, and the work will be contributed to the appropriate working group
      project(s). While not strictly limited, it is the intention to, whenever possible, subcontract
      with members of the working group community to carry out the work. The reason for this is to
      benefit those with membership, and to strengthen the ecosystem.</p>
    <p>It is expected that Eclipse Foundation will turn to either the contributing member companies
      or the working group itself to assist with the technical specification of the development
      effort&rsquo;s work schedules, and with the oversight of the development efforts as they
      proceed.</p>
    <h2>For More Information</h2>
    <p>
      For more information, or to engage with Eclipse Foundation in a member funded initiative, send
      email to <a href="mailto:membership@eclipse.org">membership@eclipse.org</a>.
    </p>
  </div>
</div>