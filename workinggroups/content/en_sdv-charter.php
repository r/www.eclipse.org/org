<?php

/**
 * Copyright (c) 2019, 2022, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at /legal/epl-2.0/
 *
 * Contributors:
 * Zhou Fang (Eclipse Foundation) - Initial implementation
 * Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>Version 1.1 - Revision history at end of document</p>

    <h2>Vision and Scope</h2>
    <p>
      The mission of the Eclipse Software Defined Vehicle (SDV) Working Group is to provide a forum
      for individuals and organizations to build and promote open source software, specifications, and
      open collaboration models needed to create a scalable, modular, extensible, industry-ready
      open source licensed vehicle software platform to support in-vehicle and around the vehicle
      systems development and deployment.
    </p>
    <p>
      The primary objective of the Eclipse SDV Working Group is to encourage, develop, and promote
      open source solutions that will be able to compete successfully in the challenging and
      fragmented markets of the automotive industry ecosystems around the globe. The working
      group will have a strong focus on implementation and onboarding of existing code artefacts
      from the working group members to build the ecosystem in a “code first” manner.
    </p>
    <p>
      To this end the working group aims to foster a component driven model that supports project
      collections:
    </p>
    <ul>
      <li>
        SDV.Dev focuses on development toolchains and workflows to allow for modern
        development of in-vehicle (and supporting off-vehicle) applications that integrate with
        cloud-based deployment and applications management environments.
      </li>
      <li>
        SDV.Ops focuses on enabling management of software stacks in large fleets of vehicles,
        integrating with existing mobility cloud solutions and supporting open standards where
        such integrations are established.
      </li>
      <li>
        SDV.Edge focuses on bringing cloud-native technologies to a range of in-vehicle software
        domains including quality-managed (QM) and safety relevant domains.
      </li>
    </ul>
    <p>
      Develop a capabilities-based model that recognizes that modern and future vehicles come with
      various technical solutions in various domains, many of which have their justification in their
      problem space. This includes but is not limited to the following capabilities:
    </p>
    <ul>
      <li>microcontroller frameworks</li>
      <li>operating systems</li>
      <li>middleware</li>
      <li>cloud-based solutions for managing vehicles and their software</li>
    </ul>
    <p>To achieve its mission the Working Group will:</p>
    <ul>
      <li>
        Foster open source implementations of needed components for SDV (build a core
        first).
      </li>
      <li>
        Define, document, and promote processes such as quality management, functional 
        safety, and supply chain security that SDV-related open source projects may choose to utilize in their development.
        <ul>
          <li>Define rules and a branding process for projects which make use of these processes to inform the market of their process maturity.</li>
        </ul>
      </li>
      <li>
        Adapt and manage the EFSP to formalize the interface specifications that are defined
        within the scope of this working group.
      </li>
      <li>
        Define compatibility rules and a compatibility and branding process for
        implementations of these components to ensure interoperability.
      </li>
      <li>
        Enable reference distributions and foster integrations in various developer toolchains.
      </li>
      <li>
        Define software platforms which will establish a Software Defined Vehicle ecosystem,
        which can be easily used and extended by the community.
      </li>
      <li>
        Support the developer community in the creation and evolution of the software
        platforms.
      </li>
      <li>
        Drive the funding model that enables this working group and its community to
        operate on a sustainable basis.
      </li>
      <li>
        Promote the Eclipse SDV brand, along with any additional brands as approved by the Steering Committee and the Eclipse Foundation, and their value in the marketplace.
      </li>
      <li>
        Provide vendor neutral marketing and other services to the Eclipse SDV ecosystem.
      </li>
      <li>
        Communicate/Liaise with other open-source and specification communities.
      </li>
      <li>
        Leverage Eclipse-defined licensing and intellectual property flows that encourage
        community participation, protect community members, and encourage usage.
      </li>
      <li>
        Manage the overall technical and business strategies related to its open source
        projects.
      </li>
    </ul>

    <h2>Governance and Precedence</h2>
    <h2>Applicable Documents</h2>
    <p>
      The following governance documents are applicable to this charter, each of which can be found
      on the <a href="/org/documents/">Eclipse Foundation Governance Documents</a> page or the
      <a href="/legal/">Eclipse Foundation Legal Resources</a> page:
    </p>
    <ul>
      <li>Eclipse Foundation Bylaws</li>
      <li>Eclipse Foundation Working Group Process</li>
      <li>Eclipse Foundation Working Group Operations Guide</li>
      <li>Eclipse Foundation Code of Conduct</li>
      <li>Eclipse Foundation Communication Channel Guidelines</li>
      <li>Eclipse Foundation Membership Agreement</li>
      <li>Eclipse Foundation Intellectual Property Policy</li>
      <li>Eclipse Foundation Antitrust Policy</li>
      <li>Eclipse Foundation Development Process</li>
      <li>Eclipse Foundation Trademark Usage Guidelines</li>
      <li>Eclipse Foundation Specification Process</li>
      <li>Eclipse Foundation Specification License</li>
      <li>Eclipse Foundation Technology Compatibility Kit License</li>
    </ul>
    <p>
      All Members must be parties to the Eclipse Foundation Membership Agreement, including the
      requirement set forth in Section 2.2 to abide by and adhere to the Bylaws and then-current
      policies of the Eclipse Foundation, including but not limited to the Intellectual Property and
      Antitrust Policies.
    </p>
    <p>
      In the event of any conflict between the terms set forth in this working group&apos;s charter and the
      Eclipse Foundation Bylaws, Membership Agreement, Development Process, Specification
      Process, Working Group Process or any policies of the Eclipse Foundation, the terms of the
      respective Eclipse Foundation Bylaws, Membership Agreement, process or policy shall take
      precedence.
    </p>

    <h2>Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be at least a
      <a href="/membership/#tab-levels">Contributing Member</a>
      of the Eclipse Foundation, have executed the Eclipse SDV Working Group
      Participation Agreement, and adhere to the requirements set forth in this Charter
      to participate.
    </p>
    <p>
      The participation fees associated with each of these membership classes are shown in the
      Annual Participation Fees section. These are annual fees, and are established by the Eclipse SDV
      Steering Committee, and will be updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are separate from any Working
      Group membership fees, and are decided as described in the Eclipse Foundation Bylaws and
      detailed in the Eclipse Foundation Membership Agreement.
    </p>
    <p>
      There are five classes of the Eclipse SDV Working Group membership - Strategic, Participant,
      Committer, Supporter and Guest.
    </p>
    <h2>Classes of Membership</h2>
    <h3>Strategic Members</h3>
    <p>
      Strategic Members are organizations that view this Working Group standards, specifications and
      technologies as strategic to their organization and are investing significant resources to sustain
      and shape the activities of this working group. Strategic Members of the working group are
      required to commit to strategic membership for a three (3) year period. Strategic Members of
      this working group must be at least a Contributing Member of the Eclipse Foundation.
    </p>
    <h3>Participant Members</h3>
    <p>
      Participant Members are typically organizations that deliver products or services based upon
      related standards, specifications and technologies, or view this working group&apos;s standards and
      technologies as strategic to their organization. These organizations want to participate in the
      development and direction of an open ecosystem related to this working group. Participant
      Members of this working group must be at least a Contributing Member of the Eclipse
      Foundation.
    </p>
    <h3>Committer Members</h3>
    <p>
      Committers are individuals who through a process of meritocracy defined by the Eclipse
      Foundation Development Process are able to contribute and commit code to the Eclipse
      Foundation projects included in the scope of this working group. Committers may become
      members of the working group by virtue of working for a member organization, or may choose
      to complete the membership-at-large process independently if they are not, along with
      completing this working group&apos;s Participation Agreement once defined. For further explanation
      and details, see the
      <a href="/membership/become_a_member/committer.php">Eclipse Committer Membership page</a>.
    </p>
    <h3>Supporting Members</h3>
    <p>
      Supporting Members are: (a) organizations that want to participate in an open ecosystem
      related to this working group and wish to show their initial support for it, and (b) organizations
      which are at least Contributing members of the Eclipse Foundation. Organizations may be
      Supporting Members for a maximum period of 1 year only, after which time they are expected
      to change their membership to a different level. Supporting Members may be invited to
      participate in committee meetings at the invitation of the respective committee, but under no
      circumstances do Supporting Members have voting rights in any working group body. Further,
      Supporting Members are not considered Participant Members as that term is defined in the
      Eclipse Foundation Specification Process. Supporting Members are required to execute the
      Working Group&apos;s Participation Agreement.
    </p>
    <h3>Guest Members</h3>
    <p>
      Guest Members are organizations which are Associate members of the Eclipse Foundation.
      Typical guests include R&D partners, universities, academic research centers, etc. Guests may be
      invited to participate in committee meetings at the invitation of the respective committee, but
      under no circumstances do Guest members have voting rights. Guest members are required to
      execute the Working Group&apos;s Participation Agreement.
    </p>

    <h2>Membership Summary</h2>
    <table class="table table-bordered text-center" cellspacing="0">
      <tbody>
        <tr>
          <td><strong>Committee Representation</strong></td>
          <td>
            <strong>Strategic Member</strong>
          </td>
          <td>
            <strong>Participant Member</strong>
          </td>
          <td>
            <Strong>Committer Member</Strong>
          </td>
          <td>
            <strong>Supporter Member</strong>
          </td>
          <td>
            <strong>Guest Member</strong>
          </td>
        </tr>
        <tr>
          <td>
            Steering Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
          <td>
            N/A
          </td>
        </tr>
        <tr>
          <td>
            Marketing and Brand Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
          <td>
            N/A
          </td>
        </tr>
        <tr>
          <td>
            Specification Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
          <td>
            N/A
          </td>
        </tr>
        <tr>
          <td>
            Technical Advisory Committee
          </td>
          <td>
            Appointed
          </td>
          <td>
            Elected
          </td>
          <td>
            Elected
          </td>
          <td>
            N/A
          </td>
          <td>
            N/A
          </td>
        </tr>
      </tbody>
    </table>

    <h2>Governance</h2>
    <p>This Eclipse SDV Working Group is designed as:</p>
    <ul>
      <li>a vendor-neutral, member-driven organization,</li>
      <li>
        a means to foster a vibrant and sustainable ecosystem of components and service
        providers,
      </li>
      <li>
        a means to organize the community of each project or component so that users and
        developers define the roadmap collaboratively.
      </li>
    </ul>

    <h2>Governing Bodies</h2>
    <h3>Steering Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Steering Committee members are required to:</p>
    <ul>
      <li>Define and manage the strategy of the working group.</li>
      <li>Define and manage which Eclipse Foundation projects are included within the scope of this working group.</li>
      <li>Ensure the consistency of logo usage and other marketing materials.</li>
      <li>Define and manage the technical roadmap.</li>
      <li>Review and approve this charter.</li>
      <li>Review and approve any trademark policy referred to it by the Marketing and Brand Committee.</li>
      <li>Define the annual fees for all classes of the working group members.</li>
      <li>Establish an annual program plan.</li>
      <li>Approve the annual budget based upon funds received through fees.</li>
      <li>Approve (if any) customizations to the Eclipse Foundation Specification Process recommended by the Specification Committee.</li>
      <li>Approve the creation of subcommittees and define the purpose, scope, and membership of each such subcommittee.</li>
      <li>Approve the creation and retirement of Special Interest Groups (SIGs).</li>
    </ul>
    <h4>Composition</h4>
    <p>
      Each Strategic Member of the working group is entitled to a seat on the Steering Committee.
    </p>
    <p>
      Participant Members shall be entitled to at least one (1) seat on the Steering Committee. In
      addition, an additional seat on the Steering Committee shall be allocated to the Participant
      Members for every additional five (5) seats beyond one (1) allocated to Strategic Members via
      election. Participant Member seats are allocated following the Eclipse &quot;Single Transferable
      Vote&quot;, as defined in the Eclipse Foundation Bylaws.
    </p>
    <p>
      One seat is allocated to Committer Members via election. The Committer Member seat is
      allocated following the Eclipse &quot;Single Transferable Vote&quot;, as defined in the Eclipse Bylaws.
    </p>
    <p>
      The Committee elects a chair of the Steering Committee. This chair is elected among the
      members of the Committee. They will serve for a 12 month period or until their successor is
      elected and qualified, or as otherwise provided for in this Charter. There is no limit on the
      number of terms the chair may serve.
    </p>
    <h4>Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year</p>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all meetings related to the
      working group will follow a prepared agenda and minutes are distributed two weeks after the
      meeting and approved at the next meeting at the latest, and shall in general conform to the
      Eclipse Foundation Antitrust Policy.
    </p>

    <h3>Marketing and Brand Committee</h3>
    <h4>Powers and Duties</h4>
    <p>The Marketing and Brand Committee members are required to:</p>
    <ul>
      <li>Define strategic marketing priorities, goals, and objectives for the working group</li>
      <li>Coordinate and support the implementation of developer and end-user outreach programs</li>
      <li>Raise issues for discussion and provide feedback to the Foundation on the execution of marketing, brand, and communications activities for the working group.</li>
      <li>Assist the Eclipse Foundation in the working group&apos;s participation in conferences and events related to the working group</li>
      <li>Define the trademark policy, if applicable, and refer to it for approval by the Steering Committee.</li>
      <li>Collaborate with other working group members to develop co-marketing strategies, amplify messaging on social channels, and share best practices. </li>
    </ul>
    <p>
      Committee members are expected to be leaders in communicating key messaging on behalf of
      the working group, and to play a leadership role in driving the overall success of the working
      group marketing efforts. In particular, members are encouraged to engage their respective
      marketing teams and personnel to amplify and support the achievement of the working group&apos;s
      goals and objectives.
    </p>
    <h4>Composition</h4>
    <p>
      Each Strategic Member of the working group is entitled to a seat on the Marketing Committee.
    </p>
    <p>
      Participant Members shall be entitled to at least one (1) seat on the Marketing Committee. In
      addition, an additional seat on the Marketing Committee shall be allocated to the Participant
      Members for every additional five (5) seats beyond one (1) allocated to Strategic Members via
      election. Participant Member seats are allocated following the Eclipse &quot;Single Transferable
      Vote&quot;, as defined in the Eclipse Bylaws.
    </p>
    <p>
      One seat is allocated to Committer Members via election. The Committer Member seat is
      allocated following the Eclipse “Single Transferable Vote”, as defined in the Eclipse Bylaws.
    </p>
    <p>
      The Committee elects a chair who reports to the Steering Committee. This chair is elected
      among the members of the Committee. They will serve for a 12 month period or until their
      successor is elected and qualified, or as otherwise provided for in this Charter. There is no limit
      on the number of terms the chair may serve.
    </p>
    <h4>Meeting Management</h4>
    <p>The Marketing and Brand Committee meets at least twice a year.</p>

    <h3>Specification Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Specification Committee members are required to:</p>
    <ul>
      <li>Ensure that all specification projects operate in an open, transparent, and vendor-neutral fashion in compliance with the specification process.</li>
      <li>Apply and govern the specification projects in the scope of this working group according to the approved specification process.</li>
      <li>Approve specifications for adoption by the community.</li>
      <li>Work with the related Project Management Committee (PMC) to ensure that the EFSP is complied with by all related working group specification projects.</li>
      <li>Review and approve the trademark policy to ensure compatibility of independent implementations of specifications.</li>
      <li>Define (if any) customizations to the EFSP. The EFSP with the approved customizations is the specification process to be used by all specifications related to this working group, and refer for approval by the Steering Committee.</li>
    </ul>
    <h4>Composition</h4>
    <p>
      Each Strategic Member of the working group is entitled to have a seat on the Specification
      Committee.
    </p>
    <p>
      Participant Members shall be entitled to at least one (1) seat on the Specification Committee. In
      addition, an additional seat on the Specification Committee shall be allocated to the Participant
      Members for every additional five (5) seats beyond one (1) allocated to Strategic Members via
      election. Participant Member seats are allocated following the Eclipse "Single Transferable
      Vote", as defined in the Eclipse Bylaws.
    </p>
    <p>
      One seat is allocated to Committer Members via election. The Committer Member seat is
      allocated following the Eclipse “Single Transferable Vote”, as defined in the Eclipse Bylaws.
    </p>
    <p>
      Any additional individuals as designated from time to time by the Executive Director.
    </p>
    <p>
      The Committee elects a chair who reports to the Steering Committee. This chair is elected
      among the members of the Committee. They will serve for a 12 month period or until their
      successor is elected and qualified, or as otherwise provided for in this Charter. There is no limit
      on the number of terms the chair may serve.
    </p>
    <h4>Meeting Management</h4>
    <p>The Specification Committee meets at least once a quarter.</p>

        <h3>Technical Advisory Committee</h3>
    <h4>Powers and Duties</h4>
    <p>Technical Advisory Committee members are required to:</p>
    <ul>
      <li>
        Recommend to the Steering Committee which Eclipse Foundation open source projects should 
        be included within the purview of the Eclipse SDV Working Group;
      </li>
      <li>
        Establish, evolve, and enforce policies and processes that augment the EDP and the EFSP to 
        support automotive requirements such as quality management, functional safety, and supply 
        chain security for the Eclipse Foundation open source projects that choose to contribute content 
        to the Eclipse SDV-branded releases. Where appropriate, process definition work may be 
        referred to the Specification Committee to be developed under the Eclipse Foundation 
        Specification Process;
      </li>
      <li>
        Establish a roadmap at least annually for review and approval by the Steering Committee. 
        Establish a roadmap process which will solicit input from and build rough consensus with 
        stakeholders including Projects and Project Management Committees.
      </li>
      <li>
        Establish a coordinated release plan that balances the many competing requirements. The 
        release plan describes the themes and priorities that focus these releases, and orchestrates the 
        dependencies among project plans;
      </li>
      <li>
        Coordinate the distribution release process and cross-project planning, facilitate the mitigation 
        of architectural issues and interface conflicts, and generally address all other coordination and 
        integration issues; and
      </li>
      <li>
        Set, evolve, and enforce engineering policies and procedures for the creation and distribution of 
        Eclipse SDV-branded releases, along with all intermediate releases.
      </li>
    </ul>
    <p>
      The Technical Advisory Committee discharges its responsibility via collaborative evaluation, 
      prioritization, and compromise. The Technical Advisory Committee will establish a set of principles to 
      guide its deliberations which will be approved by the Steering Committee and the Executive Director.
    </p>
    <h4>Composition</h4>
    <p>
      Each Strategic Member of the working group is entitled to have a seat on the Technical Advisory 
      Committee.
    </p>
    <p>
      Participant Members shall be entitled to at least one (1) seat on the Technical Advisory Committee. In 
      addition, an additional seat on the Technical Advisory Committee shall be allocated to the Participant 
      Members for every additional five (5) seats beyond one (1) allocated to Strategic Members via election. 
      Participant Member seats are allocated following the Eclipse "Single Transferable Vote", as defined in 
      the Eclipse Bylaws. 
    </p>
    <p>
      One seat is allocated to Committer Members via election. The Committer Member seat is allocated 
      following the Eclipse “Single Transferable Vote”, as defined in the Eclipse Bylaws.
    </p>
    <p>
      One seat is allocated for a representative of each Project Management Committee (PMC) responsible 
      for the governance of one or more projects within the purview of the Eclipse SDV Working Group.
    </p>
    <p>
      Any additional individuals as designated from time to time by the Executive Director.
    </p>
    <p>
      The Committee elects a chair who reports to the Steering Committee. This chair is elected among the 
      members of the Committee. They will serve for a 12 month period or until their successor is elected and 
      qualified, or as otherwise provided for in this Charter. There is no limit on the number of terms the chair 
      may serve.
    </p>
    <h4>Meeting Management</h4>
    <p>
      The Technical Advisory Committee meets at least once a quarter.
    </p>

    <h2>Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within the Working Group with a
      focus to collaborate around a particular topic or domain of direct interest to the working group.
      SIGs are designed to drive the objectives of a subset of the Members of the Working Group in
      helping them achieve a dedicated set of goals and objectives. The scope of the SIG must be
      consistent with the scope of the Working Group Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each SIG may be either
      temporary or a permanent structure within the working group. SIGs can be disbanded at any
      time by self selection presenting reasons to and seek approval from the Steering Committee.
      Steering Committees may disband a SIG at any time for being inactive or non compliant with the
      Working Group&apos;s Charter, or by request of the SIG itself. SIGs operate as a non-governing Body
      of the Working Group. There are no additional annual fees to Members for participation in a
      SIG.
    </p>

    <h2>Sponsorship</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to the working group on
      an ad hoc basis to support the activities of the Working Group and its managed projects. Money
      or services provided by sponsors are used as set forth in the working group annual budget. The
      working group is free to determine whether and how those contributions are recognized. Under
      no condition are sponsorship monies refunded.
    </p>
    <p>Sponsors need not be members of the Eclipse Foundation or of the Working Group.</p>

    <h2>Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this working group, unless otherwise
      specified. For all matters related to membership action, including without limitation: meetings,
      quorum, voting, vacancy, resignation or removal, the respective terms set forth in the 
      Eclipse Foundation Bylaws apply.
    </p>
    <p>
      Appointed representatives on the Body may be replaced by the Member organization they are
      representing at any time by providing written notice to the Steering Committee. In the event a
      Body member is unavailable to attend or participate in a meeting of the Body, they may be
      represented by another Body member by providing written proxy to the Body&apos;s mailing list in
      advance. As per the Eclipse Foundation Bylaws, a representative shall be immediately removed
      from the Body upon the termination of the membership of such representative&apos;s Member
      organization.
    </p>
    <h3>Voting</h3>
    <h4>Simple Majority</h4>
    <p>
      Excepting the actions specified below for which a Super Majority is required, votes of the Body
      are determined by a simple majority of the representatives represented at a committee
      meeting at which a quorum is present.
    </p>
    <h4>Super Majority</h4>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors approve a specification
      license; (ii) approving specifications for adoption; (iii) modifying the working group charter; (iv)
      approving or changing the name of the working group; and (v) approving changes to annual
      Member contribution requirements; any such actions must be approved by no less than
      two-thirds (2/3) of the representatives represented at a committee meeting at which a quorum
      is present.
    </p>

    <h3>Term and Dates of Elections</h3>
    <p>
      This section only applies to the Steering Committee, Specification Committee, and the
      Marketing Committee.
    </p>
    <p>
      All representatives shall hold office until their respective successors are appointed or elected, as
      applicable. There shall be no prohibition on re-election or re-designation of any representative
      following the completion of that representative's term of office.
    </p>
    <h4>Strategic Members</h4>
    <p>
      Strategic Members Representatives shall serve in such capacity on committees until the earlier
      of their removal by their respective appointing Member organization or as otherwise provided
      for in this Charter.
    </p>
    <h4>Elected Representatives</h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be elected to serve for a 12
      month term or until their respective successors are elected and qualified, or as otherwise
      provided for in this Charter. Procedures governing elections of Representatives may be
      established pursuant to resolutions of the Steering Committee provided that such resolutions
      are not inconsistent with any provision of this Charter.
    </p>
    <h3>Meetings Management</h3>
    <h4>Meeting Frequency</h4>
    <p>
      All meetings may be held at any place that has
      been designated from time-to-time by resolution of the corresponding Body. All meetings may
      be held remotely using phone calls, video calls, or any other means as designated from
      time-to-time by resolution of the corresponding Body.
    </p>
    <h4>Place of Meetings</h4>
    <p>
      All meetings may be held at any place that has been designated from time-to-time by resolution
      of the corresponding Body. All meetings may be held remotely using phone calls, video calls, or
      any other means as designated from time-to-time by resolution of the corresponding Body.
    </p>
    <h4>Regular Meetings</h4>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice of same has been
      provided to each of the representatives at least fifteen (15) calendar days prior to such meeting,
      which notice will identify all potential actions to be undertaken by the Body at the Body
      meeting. No representative will be intentionally excluded from Body meetings and all
      representatives shall receive notice of the meeting as specified above; however, Body meetings
      need not be delayed or rescheduled merely because one or more of the representatives cannot
      attend or participate so long as at least a quorum of the Body is represented at the Body
      meeting.
    </p>
    <h4>Actions</h4>
    <p>
      The Body may undertake an action only if it was identified in a Body meeting notice or
      otherwise identified in a notice of special meeting.
    </p>
    <h4>Invitations</h4>
    <p>
      The Body may invite any member to any of its meetings. These invited attendees have no right
      to vote.
    </p>

    <h2>Working Group Participation Fees</h2>
    <p>
      The initial Steering Committee will be tasked with defining a fee structure and budget for the
      current and following years based on resource requirements necessary to achieve the
      objectives of the group. Members agree to pay the annual fees as established by the Steering
      Committee and as agreed to by each participating member in their respective Working Group
      Participation Agreement.
    </p>
    <p>
      All Members who join the working group in 2022 explicitly agree to pay a prorated amount of
      the annual fees in 2022 based on the month in which they join, and to pay the full annual fees
      associated with their participation effective January 2023 and each January thereafter. For
      example, a Member with annual corporate revenues greater than €1 billion joining as a
      Participant Member in April, 2022 agrees to pay 37.500€ for its annual fees in 2022, and to then
      pay 50.000€ each January on an annual basis thereafter. Members who join the working group
      in 2023 or beyond shall pay the full annual fees effective the month they join the working
      group, and each anniversary month thereafter.
    </p>
    <p>
      Established fees cannot be changed retroactively. Any change to the annual fees will be
      communicated to all Members via the mailing list and will be reflected in their next annual fees
      payment.
    </p>

    <h2>Working Group Annual Participation Fees Schedule A</h2>
    <p>
      The following fees have been established by the Eclipse SDV Steering Committee. These fees
      are in addition to each participant’s membership fees in the Eclipse Foundation.
    </p>
    <h3>Eclipse SDV Strategic Member Annual Participation Fees</h3>
    <p>
      Strategic members are required to execute the Eclipse SDV Working Group Participation Agreement.
    </p>
    <p>Strategic members are required to commit to three (3) years membership.</p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="75%">Corporate Revenue</th>
          <th class="text-center" width="25%">Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €1 billion</td>
          <td class="text-center">€200 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €500 million but less than or equal to €1 billion</td>
          <td class="text-center">€150 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €100 million but less than or equal to €500 million </td>
          <td class="text-center">€125 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €10 million but less than or
            equal to €100 million
          </td>
          <td class="text-center">€75 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td class="text-center">€50 000</td>
        </tr>
      </tbody>
    </table>
    <h3>Eclipse SDV Participant Member Annual Participation Fees</h3>
    <p>
      Participant members are required to execute the Eclipse SDV Working Group Participation
      Agreement.
    </p>
    <table class="table table-stripped" cellspacing="0">
      <thead>
        <tr>
          <th width="75%">Corporate Revenue</th>
          <th class="text-center" width="25%">Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than €1 billion</td>
          <td class="text-center">€50 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €500 million but less than or equal to €1 billion</td>
          <td class="text-center">€40 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues greater than €100 million but less than or equal to €500 million </td>
          <td class="text-center">€30 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than €10 million but less than or
            equal to €100 million
          </td>
          <td class="text-center">€20 000</td>
        </tr>
        <tr>
          <td>Annual Corporate Revenues less than or equal to €10 million</td>
          <td class="text-center">€10 000</td>
        </tr>
      </tbody>
    </table>
    <h3>Eclipse SDV Committer Member Annual Participation Fees</h3>
    <p>
      Committer members pay no annual fees, but are required to execute the Eclipse SDV Working
      Group Participation Agreement.
    </p>
    <h3>Eclipse SDV Supporter Member Annual Participation Fees</h3>
    <p>
      Supporter members pay no annual fees, but are required to execute the Eclipse SDV Working
      Group Participation Agreement.
    </p>
    <h3>Eclipse SDV Guest Member Annual Participation Fees</h3>
    <p>
      Guest members pay no annual fees, but are required to execute the Eclipse SDV Working
      Group Participation Agreement.
    </p>

    <p class="margin-top-20"><strong>Charter History</strong></p>
    <ul>
      <li>v0.1 created Dec 8, 2021</li>
      <li>v0.2 created Dec 16, 2021, added fee levels, …</li>
      <li>v0.3 created Jan 14, 2022, refinements based on initial draft</li>
      <li>v0.4 created Jan 25, 2022, Release Candidate</li>
      <li>
        v1.0 approved by Steering Committee March 3, 2022
        <ul>
          <li>approved by Executive Director March 7, 2022</li>
        </ul>
      </li>

      <li>v1.1 adding Technical Advisory Committee Governing Body and other standard refinements</li>
    </ul>
  </div>
</div>