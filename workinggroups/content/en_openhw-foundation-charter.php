<?php

/**
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p class="margin-bottom-0">Draft &mdash; Not Yet Approved</p>
    <p>Version #0.2 Revision History at End</p>

    <h2 id="vision-and-scope">Vision and Scope</h2>
    <p>
      The overarching goal of the OpenHW Foundation is to develop, protect, and
      promote the free and open-source cores, related intellectual property,
      tools, software (such as the CORE-V Family of open source RISC-V
      processors) and related activities. These initiatives may address
      strategic or market analysis, technical requirements, and digital
      sovereignty. By working collaboratively, members can pursue these
      initiatives and have a positive impact with strategic open hardware and
      related open software projects.
    </p>
    <p>
      The specific goals outlined in more detail below are:
    </p>
    <ul>
      <li>
        Ensure the establishment and operation of a global code repository for
        the projects related to OpenHW Foundation.
      </li>
      <li>
        Establish partnerships with global RISC-V organisations and to drive
        dialogue to jointly define open source roadmaps for chip IP, software,
        hardware, and other ecosystem elements that are needed by global
        ecosystems.
      </li>
      <li>
        Gather global-centric requirements and strategic/technology needs for
        an open hardware ecosystem and to incubate projects arising from that
        discussion under the OpenHW top level project.
      </li>
      <li>
        Participate in projects relevant to global centric open hardware and
        related open software initiatives.
      </li>
      <li>
        Support the goal of the EU Chips Act or similar governmental and
        industry initiatives with the goal of building open sovereign supply
        chains.
      </li>
      <li>
        Support the development of vertical solutions, for example in support
        of the automotive industry to develop MVP including HW, Safety related
        SW artefacts like RTOS + an open platform based on OS SDV initiatives.
      </li>
      <li>
        Gather global requirements and strategic/technology needs for an open
        hardware ecosystem and where appropriate to incubate projects arising
        from that discussion under the Working Group top level project.
      </li>
      <li>
        Carry out global marketing initiatives in support of the Working Group
        initiatives.
      </li>
    </ul>
    <p>
      The OpenHW Foundation will:
    </p>
    <ul>
      <li>
        Promote the OpenHW brand and its value in the marketplace.
      </li>
      <li>
        Provide vendor neutral marketing and other services to the OpenHW
        ecosystem.
      </li>
      <li>
        Leverage Eclipse-defined licensing and intellectual property flows that
        encourage community participation, protect community members, and
        encourage usage. 
      </li>
      <li>
        Manage the overall technical and business strategies related to its open source
        projects.
      </li>
      <li>
        Establish and drive a funding model that enables this Foundation and
        its community to operate on a sustainable basis.
      </li>
      <h2 id="governance-and-precedence">Governance and Precedence</h2>
      <p>
        The OpenHW Foundation is operated as an Eclipse Foundation Working Group and operates
        under the governance rules as specified in the Eclipse Foundation Working Group Process. The
        term "OpenHW Foundation Working Group" is used occasionally in this charter for purposes of
        consistency with the Eclipse Foundation Working Group Process and with other working group
        charters. For clarity, the terms "OpenHW Foundation" and "OpenHW Foundation Working
        Group" are interchangeable.
      </p>
      <h3 id="applicable-documents">Applicable Documents</h3>
      <p>
        The following governance documents are applicable to this charter, each
        of which can be found on the <a href="/org/documents/">Eclipse Foundation Governance Documents</a>
        page or the <a href="/legal/">Eclipse Foundation Legal Resources</a> page:
      </p>
      <ul>
        <li>Eclipse Foundation Bylaws</li>
        <li>Eclipse Foundation Working Group Process</li>
        <li>Eclipse Foundation Working Group Operations Guide</li>
        <li>Eclipse Foundation Code of Conduct</li>
        <li>Eclipse Foundation Communication Channel Guidelines</li>
        <li>Eclipse Foundation Membership Agreement</li>
        <li>Eclipse Foundation Intellectual Property Policy</li>
        <li>Eclipse Foundation Antitrust Policy</li>
        <li>Eclipse Foundation Development Process</li>
        <li>Eclipse Foundation Trademark Usage Guidelines</li>
      </ul>
      <br>
      <p>
        All Members must be parties to the Eclipse Foundation Membership
        Agreement, including the requirement set forth in Section 2.2 to abide by
        and adhere to the Bylaws and then-current policies of the Eclipse
        Foundation, including but not limited to the Intellectual Property and
        Antitrust Policies.
      </p>
      <p>
        In the event of any conflict between the terms set forth in this
        Working Group's Charter and the Eclipse Foundation Bylaws, Membership
        Agreement, Development Process, Specification Process, Working Group
        Process or any policies of the Eclipse Foundation, the terms of the
        respective Eclipse Foundation Bylaws, Membership Agreement, process or
        policy shall take precedence.
      </p>
    </ul>
    <h2 id="membership">Membership</h2>
    <p>
      With the exception of Guest members as described below, an entity must be
      at least a <a href="/membership/#tab-levels">Contributing Member</a> of
      the Eclipse Foundation, have executed the OpenHW Foundation Participation
      Agreement once defined and adhere to the requirements set forth in this
      Charter to participate.
    </p>
    <p>
      The participation fees associated with each of these membership classes
      are shown in the Annual Participation Fees section. These are annual fees
      and are established by the OpenHW Foundation Steering Committee and will
      be updated in this charter document accordingly.
    </p>
    <p>
      The fees associated with membership in the Eclipse Foundation are
      separate from any Working Group membership fees and are decided as
      described in the Eclipse Foundation Bylaws and detailed in the Eclipse
      Foundation Membership Agreement.
    </p>
    <p>
      There are 3 classes of OpenHW Foundation membership - Strategic,
      Participant, and Guest.
    </p>

    <h2 id="classes-of-membership">Classes of Membership</h2>
    <h3 id="strategic-members">Strategic Members</h3>
    <p>
      Strategic Members are organisations that view this Working Group
      standards, specifications and technologies as strategic to their
      organisation and are investing significant resources to sustain and shape
      the activities of this Working Group. Strategic Members of this Working
      Group must be at least a Contributing Member of the Eclipse Foundation.
    </p>
    <h3 id="participant-members">Participant Members</h3>
    <p>
      Participant Members are typically organisations that deliver products or
      services based upon related standards, specifications and technologies,
      or view this Working Group’s standards and technologies as strategic to
      their organisation. These organisations want to participate in the
      development and direction of an open ecosystem related to this Working
      Group. Participant Members of this Working Group must be at least a
      Contributing Member of the Eclipse Foundation.
    </p>
    <h3 id="guest-members">Guest Members</h3>
    <p>
      Guest Members are organisations which are Associate members of the
      Eclipse Foundation. Typical guests include universities, academic
      research centres, etc. Guests may be invited to participate in committee
      meetings at the invitation of the respective committee, but under no
      circumstances do Guest members have voting rights. Guest members are
      required to execute the Working Group’s Participation Agreement.
    </p>
    
    <h2 id="special-interest-groups">Special Interest Groups</h2>
    <p>
      A Special Interest Group (SIG) is a lightweight structure formed within
      the working group with a focus to collaborate around a particular topic
      or domain of direct interest to the working group. SIGs are designed to
      drive the objectives of a subset of the Members of the working group in
      helping them achieve a dedicated set of goals and objectives. The scope
      of the SIG must be consistent with the scope of the Working Group
      Charter.
    </p>
    <p>
      The creation of a SIG requires approval of the Steering Committee. Each
      SIG may be either temporary or a permanent structure within the working
      group. SIGs can be disbanded at any time by self selection presenting
      reasons to and seeking approval from the Steering Committee. Steering
      Committees may disband a SIG at any time for being inactive or
      non-compliant with the working group's charter, or by request of the SIG
      itself. SIGs operate as a non-governing Body of the working group. There
      are no additional annual fees to Members for participation in a SIG.
    </p>

    <h2 id="sponsorship">Sponsorship</h2>
    <p>
      Sponsors are companies or individuals who provide money or services to
      the working group on an ad hoc basis to support the activities of the
      working group and its managed projects. Money or services provided by
      sponsors are used as set forth in the working group annual budget. The
      working group is free to determine whether and how those contributions
      are recognised. Under no condition are sponsorship monies refunded.
    </p>
    <p>
      Sponsors need not be members of the Eclipse Foundation or of the Working
      Group.
    </p>

    <h2 id="governance">Governance</h2>
    <p>
      This OpenHW Foundation Working Group is designed as:
    </p>
    <ul>
      <li>
        a vendor-neutral, member-driven organisation,
      </li>
      <li>
        a means to foster a vibrant and sustainable ecosystem of components and
        service providers,
      </li>
      <li>
        a means to organise the community of each project or component so that users and
        developers define the roadmap collaboratively.
      </li>
    </ul>

    <h2 id="governing-bodies">Governing Bodies</h2>
    <h3 id="steering-committee">Steering Committee</h3>
    <h4 id="power-and-duties">Power and Duties</h4>
    <p>
      Steering Committee members are required to:
    </p>
    <ul>
      <li>
        Define and manage the strategy of the Working Group.
      </li>
      <li>
        Define and manage which Eclipse Foundation projects are included within
        the scope of this Working Group.
      </li>
      <li>
        Ensure the consistency of logo usage and other marketing materials.
      </li>
      <li>
        Define and manage the technical roadmap.
      </li>
      <li>
        Review and approve this charter.
      </li>
      <li>
        Review and approve any trademark policy referred to it by the Marketing
        and Brand Committee.
      </li>
      <li>
        Define the annual fees for all classes of the Working Group members.
      </li>
      <li>
        Establish the annual program plan.
      </li>
      <li>
        Approve the annual budget based upon funds received through fees.
      </li>
      <li>
        Approve the creation of subcommittees and define the purpose, scope,
        and membership of each such subcommittee.
      </li>
      <li>
        Approve the creation and retirement of Special Interest Groups (SIGs).
      </li>
    </ul>
    <h4 id="steering-composition">Composition</h4>
    <p>
      Each Strategic Member of the Working Group is entitled to a seat on the
      Steering Committee.
    </p>
    <p>
      Participant Members shall be entitled to at least one (1) seat on the
      Steering Committee. In addition, an additional seat on the Steering
      Committee shall be allocated to the Participant Members for every
      additional five (5) seats beyond one (1) allocated to Strategic Members
      via election. Participant Member seats are allocated following
      the Eclipse "Single Transferable Vote", as defined in the Eclipse
      Foundation Bylaws.
    </p>
    <p>
      The Committee elects a chair of the Steering Committee. This chair is
      elected among the members of the Committee. They will serve for a
      12-month period or until their successor is elected and qualified or as
      otherwise provided for in this Charter. There is no limit on the number
      of terms the chair may serve.
    </p>
    <h4 id="steering-meeting-management">Meeting Management</h4>
    <p>The Steering Committee meets at least twice a year.</p>

    <h3 id="technical-advisory-committee">Technical Advisory Committee</h3>
    <h4 id="technical-advisory-power-and-duties">Power and Duties</h4>
    <p>
      Technical Advisory Committee members are required to:
    </p>
    <ul>
      <li>
        Recommend to the Steering Committee which Eclipse Foundation open
        source projects should be included within the purview of the OpenHW
        Foundation;
      </li>
      <li>
        Establish, evolve, and enforce policies and processes that augment the
        Eclipse Development Process to support OpenHW requirements such as
        quality management, technical information, proposed designs and
        improvements and supply chain security for the Eclipse Foundation open
        source projects that choose to contribute content to the OpenHW branded
        releases.
      </li>
      <li>
        Establish a roadmap at least annually for review and approval by the
        Steering Committee. Establish a roadmap process which will solicit
        input from and build rough consensus with stakeholders including
        Projects and Project Management Committees.
      </li>
      <li>
        Coordinate the distribution release process and cross-project planning,
        facilitate the mitigation of architectural issues and interface
        conflicts, and generally provide input and guidance on all other
        coordination and integration issues; and
      </li>
      <li>
        Set, evolve, and enforce engineering policies and procedures for the
        creation and distribution of OpenHW-branded releases, along with all
        intermediate releases.
      </li>
    </ul>
    <p>
      The Technical Advisory Committee discharges its responsibility via
      collaborative evaluation, prioritisation, and compromise. The Technical
      Advisory Committee will establish a set of principles to guide its
      deliberations which will be approved by the Steering Committee and the
      Executive Director.
    </p>
    <h4 id="technical-advisory-composition">Composition</h4>
    <p>
      Each Strategic Member of the working group is entitled to have a seat on
      the Technical Advisory Committee.
    </p>
    <p>
      Participant Members shall be entitled to at least one (1) seat on the
      Technical Advisory Committee. In addition, an additional seat on the
      Technical Advisory Committee shall be allocated to the Participant
      Members for every additional five (5) seats beyond one (1) allocated to
      Strategic Members via election. Participant Member seats are allocated
      following the Eclipse "Single Transferable Vote", as defined in the
      Eclipse Bylaws.
    </p>
    <p>
      One seat is allocated for a representative of the OpenHW Project
      Management Committee (PMC).
    </p>
    <p>
      Any additional individuals as designated from time to time by the
      Executive Director.
    </p>
    <p>
      The Committee elects a chair who reports to the Steering Committee. This
      chair is elected among the members of the Committee. They will serve for
      a 12 month period or until their successor is elected and qualified, or
      as otherwise provided for in this Charter. There is no limit on the
      number of terms the chair may serve.
    </p>
    <h4 id="technical-advisory-meeting-management">Meeting Management</h4>
    <p>
      The Technical Advisory Committee meets at least once a quarter.
    </p>

    <h2 id="membership-summary">Membership Summary</h2>
    <table class="table">
      <thead>
        <tr>
          <th>Committee Representation</th>
          <th>Strategic Member</th>
          <th>Participant Member</th>
          <th>Guest Member</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Steering Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>N/A</td>
        </tr>
        <tr>
          <td>Technical Advisory Committee</td>
          <td>Appointed</td>
          <td>Elected</td>
          <td>N/A</td>
        </tr>
      </tbody>
    </table>
    
    <h2 id="common-dispositions">Common Dispositions</h2>
    <p>
      The dispositions below apply to all governance bodies for this Working
      Group unless otherwise specified. For all matters related to membership
      action, including without limitation: meetings, quorum, voting, vacancy,
      resignation or removal, the respective terms set forth in the Eclipse
      Foundation Bylaws apply.Appointed representatives on the Body may be
      replaced by the Member organisation they are representing at any time by
      providing written notice to the Steering Committee. In the event a Body
      member is unavailable to attend or participate in a meeting of the Body,
      they may be represented by another Body member by providing written proxy
      to the Body’s mailing list in advance. As per the Eclipse Foundation
      Bylaws, a representative shall be immediately removed from the Body upon
      the termination of the membership of such representative’s Member
      organisation.
    </p>
    <h3 id="voting">Voting</h3>
    <h4 id="simple-majority">Simple Majority</h4>
    <p>
      Accepting the actions specified below for which a Super Majority is
      required, votes of the Body are determined by a simple majority of the
      representatives represented at a committee meeting at which a quorum is
      present.
    </p>
    <h4 id="super-majority">Super Majority</h4>
    <p>
      For actions (i) requesting that the Eclipse Foundation Board of Directors
      approve a specification licence; (ii) approving specifications for
      adoption; (iii) modifying the working group charter; (iv) approving or
      changing the name of the working group; and (v) approving changes to
      annual Member contribution requirements; any such actions must be
      approved by no less than two-thirds (&#8532;) of the representatives
      represented at a committee meeting at which a quorum is present.
    </p>
    <h3 id="term-and-dates-of-elections">Term and Dates of Elections</h3>
    <p>
      All representatives shall hold office until their respective successors
      are appointed or elected, as applicable. There shall be no prohibition on
      re-election or re-designation of any representative following the
      completion of that representative's term of office.
    </p>
    <h4 id="term-strategic">Strategic Members</h4>
    <p>
      Strategic Members Representatives shall serve in such capacity on
      committees until the earlier of their removal by their respective
      appointing Member organisation or as otherwise provided for in this
      Charter.
    </p>
    <h4 id="elected-representatives"></h4>
    <p>
      Elected representatives shall each serve one-year terms and shall be
      elected to serve for a 12-month term or until their respective successors
      are elected and qualified, or as otherwise provided for in this Charter.
      Procedures governing elections of Representatives may be established
      pursuant to resolutions of the Steering Committee provided that such
      resolutions are not inconsistent with any provision of this Charter.
    </p>

    <h2 id="meetings-management">Meetings Management</h2>
    <p>
      As prescribed in the Eclipse Foundation Working Group Process, all
      meetings related to the Working Group will follow a prepared agenda and
      minutes are distributed two weeks after the meeting and approved at the
      next meeting at the latest, and shall in general conform to the Eclipse
      Foundation Antitrust Policy.
    </p>
    <h3 id="meeting-frequency">Meeting Frequency</h3>
    <p>
      Each governing Body meets at least twice a year. All meetings may be held
      at any place that has been designated from time to time by resolution of
      the corresponding Body. All meetings may be held remotely using phone
      calls, video calls, or any other means as designated from time to time by
      resolution of the corresponding Body.
    </p>
    <h3 id="place-of-meetings">Place of Meetings</h3>
    <p>
      All meetings may be held at any place designated from time to time by
      resolution of the corresponding Body. All meetings may be held remotely
      using phone calls, video calls, or any other means as designated from
      time-to-time by resolution of the corresponding Body.
    </p>
    <h3 id="regular-meetings">Regular Meetings</h3>
    <p>
      No Body meeting will be deemed to have been validly held unless a notice
      of same has been provided to each of the representatives at least fifteen
      (15) calendar days prior to such meeting, which notice will identify all
      potential actions to be undertaken by the Body at the Body meeting. No
      representative will be intentionally excluded from Body meetings and all
      representatives shall receive notice of the meeting as specified above;
      however, Body meetings need not be delayed or rescheduled merely because
      one or more of the representatives cannot attend or participate so long
      as at least a quorum of the Body is represented at the Body meeting.
    </p>
    <h3 id="actions">Actions</h3>
    <p>
      The Body may undertake an action only if it was identified in a Body
      meeting notice or otherwise identified in a notice of special meeting.
    </p>
    <h3 id="invitations">Invitations</h3>
    <p>
      The Body may invite any member to any of its meetings. These invited
      attendees have no right to vote.
    </p>

    <h2 id="foundation-participation-fees">Foundation Participation Fees</h2>
    <p>
      The initial Steering Committee will be tasked with defining a fee
      structure and budget for the current and following years based on
      resource requirements necessary to achieve the objectives of the group.
      Members agree to pay the annual fees as established by the Steering
      Committee.
    </p>
    <p>
      If fees are not established for a full calendar year, they will be
      effective one month following the resolution of established fees and will
      be pro-rated for the year. All members are expected to pay the pro-rated
      fees for the associated calendar year. Beginning with the next calendar
      year, fees will be assessed on an annual basis.
    </p>
    <p>
      Established fees cannot be changed retroactively; rather, any change in
      annual participation fees will be communicated to all members via the
      mailing list, and each member will be charged the revised fees the next
      time they are to pay their annual participation fee.
    </p>
    
    <h2 id="foundation-annual-participation-fees-schedule-a">
      Foundation Annual Participation Fees Schedule A
    </h2>
    <p>
      The following fees have been established by the OpenHW Foundation
      Steering Committee. These fees are in addition to each participant’s
      membership fees in the Eclipse Foundation.
    </p>
    <h3 id="strategic-member-annual-fees">
      OpenHW Foundation Strategic Member Annual Participation Fees
    </h3>
    <p>
      Strategic members are required to execute the OpenHW Foundation
      Participation Agreement.
    </p>
    <table class="table margin-bottom-30" aria-describedby="strategic-member-annual-fees">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Annual Corporate Revenues greater than &euro;1 billion</td>
          <td>&euro;150 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;100 million but less than
            or equal to &euro;1 billion
          </td>
          <td>&euro;100 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;50 million but less than or
            equal to &euro;100 million
          </td>
          <td>&euro;60 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;10 million but less than or
            equal to &euro;50 million
          </td>
          <td>&euro;40 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues less than &euro;10 million
          </td>
          <td>&euro;20 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues less than &euro;1 million
          </td>
          <td>&euro;20 000</td>
        </tr>
        <tr>
          <td>
            Govt, Govt Agys, Research Orgs, NGOs, etc.
          </td>
          <td>&euro;25 000</td>
        </tr>
        <tr>
          <td>
            Academic, Publishing Orgs, User Groups, etc
          </td>
          <td>&euro;25 000</td>
        </tr>
      </tbody>
    </table>
    <h3 id="participant-member-annual-fees">
      OpenHW Foundation Participant Member Annual Participation Fees
    </h3>
    <p>
      Participant members are required to execute the OpenHW Foundation
      Participation Agreement.
    </p>
    <table class="table margin-bottom-30" aria-describedby="participant-member-annual-fees">
      <thead>
        <tr>
          <th>Corporate Revenue</th>
          <th>Annual Fees</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;1 billion
          </td>
          <td>&euro;40 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;100 million but less than
            or equal to &euro;1 billion
          </td>
          <td>&euro;25 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;50 million but less than or
            equal to &euro;100 million 
          </td>
          <td>&euro;10 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues greater than &euro;10 million but less than or
            equal to &euro;50 million
          </td>
          <td>&euro;10 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues less than &euro;10 million
          </td>
          <td>&euro;5 000</td>
        </tr>
        <tr>
          <td>
            Annual Corporate Revenues less than &euro;1 million
          </td>
          <td>&euro;1 500</td>
        </tr>
        <tr>
          <td>
            Govt, Govt Agys, Research Orgs, NGOs, etc.
          </td>
          <td>&euro;6 000</td>
        </tr>
        <tr>
          <td>
            Academic, Publishing Orgs, User Groups, etc.
          </td>
          <td>&euro;1 000</td>
        </tr>
      </tbody>
    </table>
    <h3 id="guest-member-participation-fees">
      OpenHW Foundation Guest Member Annual Participation Fees
    </h3>
    <p>
      Guest members pay no annual fees but are required to execute the OpenHW
      Foundation Participation Agreement.
    </p>
    <br>
    <p aria-level="2"><strong>Charter History</strong></p>
    <ul class="margin-bottom-30">
      <li>v0.1 proposed draft September 23 2024</li>
      <li>
        v0.2 added Technical Advisory Committee to provide continuity from
        the Technical Task Force in the original OpenHW Group Bylaws
      </li>
    </ul>
  </div>
</div>
