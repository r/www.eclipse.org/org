<?php
/**
 * Copyright (c) 2011, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();
$App->setOutDated();
$Theme = $App->getThemeClass();
include ($App->getProjectCommon());

$pageTitle = "Eclipse LocationTech Working Group charter";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("location, geospatial, geo, eclipse");
$Theme->setPageAuthor("Ian Skerrett");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();