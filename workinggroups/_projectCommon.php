<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Eric Poirier (Eclipse Foundation)
 *     Christopher Guindon (Eclipse Foundation)
 *     Zhou Fang (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$Nav = new Nav();
$Nav->addNavSeparator("Eclipse Working Groups", "index.php");
$Nav->addCustomNav("Explore Working Groups", "explore.php", "_self", 1);
$Nav->addCustomNav("About Working Groups", "about.php", "_self", 1);
$Nav->addCustomNav("5 Reasons to Collaborate", "/collaborations/working-groups/brief/", "_self", 1);

$Nav->addNavSeparator("Related Links", "");
$Nav->addCustomNav("Working Group Process", "/org/workinggroups/industry_wg_process.php", "_self", 1);
$Nav->addCustomNav("Working Group Operations", "/org/workinggroups/operations.php", "_self", 1);
$Nav->addCustomNav("Working Group Development Effort Guidelines", "/org/workinggroups/wgfi_program.php", "_self", 1);
$Nav->addCustomNav("Member Funded Initiatives", "/org/workinggroups/mfi_program.php", "_self", 1);
$Theme->setNav($Nav);
