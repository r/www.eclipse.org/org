<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App   = new App();
$Theme = $App->getThemeClass();

// Custom theme variables
$variables = array();
$variables['main_container_classes'] = 'container-full';
$App->setThemeVariables($variables);

$pageTitle  = "Working Groups";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse, working, groups");
$Theme->setTitle($pageTitle);

ob_start();
?>
<div class="jumbotron featured-jumbotron margin-bottom-0">
  <div class="container">
    <div class="row">
      <div class="col-md-20 col-md-offset-2 col-sm-18 col-sm-offset-3">
        <h1><?php print $pageTitle;?></h1>
        <p>Fostering open industry collaboration to develop new industry platforms</p>
        <ul class="list-inline margin-top-30">
          <li><a class="btn btn-primary" href="/org/workinggroups/explore.php">Explore Working Groups</a></li>
          <li><a class="btn btn-primary" href="/org/workinggroups/about.php">About Working Groups</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setAttributes('header-wrapper', 'header-default-bg-img');
$Theme->setHtml($html);
$Theme->generatePage();