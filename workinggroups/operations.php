<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "Eclipse Foundation Working Group Operations Guide";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("industry working groups, eclipse");
$Theme->setPageAuthor("Wayne Beaton");

ob_start();
?>

<style>ul.checklist{list-style: none;}</style>

<?php

// Note that the operations guide text (that is, the actual
// content for this page is generated from the Asciidoc source in
// https://github.com/EclipseFdn/EFWGP

?>

<?php
readfile('content/en_operations.html');

$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();

