<?php
/**
 * Copyright (c) 2017, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();
$Theme = $App->getThemeClass();
include ($App->getProjectCommon());

$pageTitle = "openMobility Working Group Charter";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("openmobility, wg, charter, eclipse");
$Theme->setPageAuthor("Ralph Mueller");

ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();