<?php
/**
 * Copyright (c) 2012, 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation)
 *   Mike Milinkovich (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();
$App->setOutDated();

$Theme = $App->getThemeClass();
include ($App->getProjectCommon());

$pageTitle = "Tangle EE Working Group Charter";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("charter, eclipse");
$Theme->setPageAuthor("Mike Milinkovich");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();
