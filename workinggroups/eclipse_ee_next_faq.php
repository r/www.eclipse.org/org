<?php
/**
 * Copyright (c) 2017, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available
 * at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

header("HTTP/1.1 301 Moved Permanently");
header("Location: /org/workinggroups/jakarta_ee_faq.php");