<?php
/**
 * Copyright (c) 2017, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available
 * at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();
$Theme = $App->getThemeClass();

include($App->getProjectCommon());

$Nav->addNavSeparator("Jakarta EE", "/org/workinggroups/jakarta_ee_charter.php");
$Nav->addCustomNav("Charter", "/org/workinggroups/jakarta_ee_charter.php", "_self", 1);
$Nav->addCustomNav("FAQ", "/org/workinggroups/jakarta_ee_faq.php", "_self", 1);

$pageTitle = "Jakarta EE Working Group FAQ";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Jakarta EE, eclipse ee, ee.next, ee, wg, charter, eclipse, ee.next FAQ");
$Theme->setPageAuthor("Paul White");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();
