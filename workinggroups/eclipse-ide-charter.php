<?php
/**
 * Copyright (c) 2019 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available
 * at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

include($App->getProjectCommon());

$pageTitle = "Eclipse IDE Working Group Charter";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Working Group, Eclipse IDE, wg, charter, eclipse");
$Theme->setPageAuthor("Gael Blondelle");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();
