<?php
/**
 * Copyright (c) 2014, 2018, 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *   Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

$pageTitle  = "Explore our Industry Collaborations";
$Theme->setPageTitle($pageTitle);
$Theme->setTitle($pageTitle);
$Theme->setPageKeywords("eclipse, working, interest, groups, industry, collaborations");

ob_start();
?>
<div class="jumbotron featured-jumbotron margin-bottom-0">
  <div class="container">
    <div class="row">
      <div class="col-md-22 col-md-offset-1 col-sm-20 col-sm-offset-2">
        <h1><?php print $pageTitle; ?></h1>
        <p>Eclipse Foundation collaborations allow organizations to combine the best practices of open source development with a set of services required for open innovation, enabling organizations to foster industry collaborations.</p>
        <ul class="hidden-xs list-inline margin-top-30">
          <li><a class="btn btn-primary" href="https://accounts.eclipse.org/contact/membership">Become a Member</a></li>
          <li><a class="btn btn-primary" href="/collaborations/">About Collaborations</a></li>
        </ul>
        <ul class="visible-xs list-inline margin-top-30">
          <li class="col-xs-20 col-xs-offset-2"><a class="btn btn-primary btn-wide" href="https://accounts.eclipse.org/contact/membership">Become a Member</a></li>
          <li class="col-xs-20 col-xs-offset-2"><a class="btn btn-primary btn-wide" href="/collaborations/">About Collaborations</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

$App->AddExtraJSFooter('
<script>
$.fn.matchHeight._maintainScroll = true;
</script>
<script>
    // TODO: Remove this hotfix once members list is fixed
    const target = document.querySelector(\'#wg-openmobility\');
    const observer = new MutationObserver((mutationList, observer) => {
        for (const mutation of mutationList) {
            if (mutation.type === "attributes" && mutation.attributeName === "class") {
                const heading = document.querySelector(\'#interest-groups > h2\');
                if (mutation.target.classList.contains("hide")) {
                    heading.classList.add("hide");
                } else {
                    heading.classList.remove("hide");
                }
            }
        }
    });

    observer.observe(target, { attributes: true });
</script>
');

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->setAttributes('header-wrapper', 'header-default-bg-img');
$Theme->generatePage();
