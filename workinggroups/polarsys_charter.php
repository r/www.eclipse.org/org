<?php
/**
 * Copyright (c) 2012, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Mike Milinkovich (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App();

$Theme = $App->getThemeClass();
include ($App->getProjectCommon());

$pageTitle = "Polarsys Working Group Charter";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("aerospace, systems, engineering, polarsys, safety, iwg, charter, eclipse");
$Theme->setPageAuthor("Mike Milinkovich");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();
