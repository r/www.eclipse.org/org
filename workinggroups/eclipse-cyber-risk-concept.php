<?php
/**
 * Copyright (c) 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available
 * at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *  Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

header('Location: ' . '/org/workinggroups/explore.php');
exit();
