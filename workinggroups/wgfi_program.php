<?php
/**
 * Copyright (c) 2022 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Zhou Fang (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

include($App->getProjectCommon());

$pageTitle = "Guidelines for Management of Working Group Funded Development Efforts and Initiatives";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("benefit, membership, WGFI, Working Group Funded Initiatives Program, member");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();