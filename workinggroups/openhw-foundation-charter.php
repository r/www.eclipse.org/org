<?php
/**
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available
 * at http://eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

include($App->getProjectCommon());

$pageTitle = "OpenHW Foundation Charter";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("openhw, openhw foundation, core-v, risc-v, processors, wg, working group, charter, eclipse");
$Theme->setPageAuthor("Eclipse Foundation");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();

