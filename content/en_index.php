<?php

/**
 * Copyright (c) 2005, 2018, 2021, 2023, 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - initial API and implementation
 * Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 */
?>
<div id="maincontent" class="about-page">
  <div id="midcolumn">
    <section class="container">
      <h1><?php print $pageTitle; ?></h1>
      <p>
        The Eclipse Foundation provides our global community of individuals and
        organisations with a business-friendly environment for open source software
        collaboration and innovation.
      </p>
    </section>
    <section class="container margin-bottom-60">
      <h2 id="what-we-do">What We Do</h2>
      <p>
        We have a proven track record of enabling community-led and
        industry-ready open source innovation earned over two decades. We host
        <a href="https://adoptium.net">Adoptium</a>,
        <a href="https://sdv.eclipse.org">Software Defined Vehicle</a>,
        <a href="https://jakarta.ee">Jakarta EE</a>,
        <a href="https://eclipseide.org">Eclipse IDE</a>,
        and 410+ open source projects, including runtimes, tools, specifications,
        and frameworks for enterprise, cloud, edge, automotive, AI, embedded,
        IoT, systems engineering, open processor designs, and many others.
        Supported by over 360 members globally, the Eclipse Foundation has an
        established international reach and reputation.
      </p>
    </section>
    <section class="what-we-do-links-section dark row text-black">
      <div class="container">
        <div class="row row-no-gutters text-black">
          <div class="col-sm-offset-4 col-sm-4 col-xs-12">
            <a class="link-tile link-unstyled bg-primary-light" href="/topics/automotive-and-mobility/">
              Automotive &amp; Mobility
            </a>
          </div>
          <div class="col-sm-4 col-xs-12">
            <a class="link-tile link-unstyled bg-neutral-flat-dark-darker" href="/topics/edge-and-iot/">
              IoT &amp; Edge
            </a>
          </div>
          <div class="col-sm-4 col-xs-12">
            <a class="link-tile link-unstyled bg-neutral-flat-dark" href="/topics/cloud-native/">
              Cloud Native
            </a>
          </div>
          <div class="col-sm-4 col-xs-12">
            <a class="link-tile link-unstyled bg-neutral-flat" href="/topics/ide/">
              Developer Tools &amp; IDEs
            </a>
          </div>
          <div class="col-xs-24 margin-top-30 text-center fw-500 text-primary-light what-we-do-links-section-sublinks">
            <a class="text-primary-light" href="https://projects.eclipse.org/">Find a Project</a></a>
          </div>
        </div>
      </div>
    </section>
    <section class="our-approach-section row padding-y-60">
      <div class="container">
        <div class="col-sm-12">
          <h2 class="margin-bottom-40">Our Approach to Open Source <br>Development</h2>
          <p>
            Our focus is to create an environment for successful open source
            projects and to promote the adoption of Eclipse technology in
            commercial and open source solutions. Through these services, we
            provide our communities with a proven model for open source
            development:
          </p>
        </div>
        <div class="col-sm-12">
          <div class="big-quote margin-x-auto margin-bottom-60">
            <img class="img img-responsive" src="./images/about/big-quote.png" alt="" />
          </div>
          <p class="margin-top-40">
            &ldquo;Our customers like the idea that we're not reinventing all of the
            technologies ourselves, and that we're using and participating in
            open source projects that are governed by a well-known, independent
            foundation&rdquo;
          </p>
          <p>&mdash; Glenn Ergeerts, CTO, Aloxy</p>
        </div>
      </div>
      <div class="container padding-top-60 padding-bottom-40 text-black">
        <div class="row row-no-gutters text-black">
          <div class="col-sm-4 col-sm-offset-4 col-xs-12">
            <a class="link-tile link-unstyled bg-neutral-flat" href="/org/services/#IP_Management">
              IP Management
            </a>
          </div>
          <div class="col-sm-4 col-xs-12">
            <a class="link-tile link-unstyled bg-neutral-flat-dark" href="/org/services/#Ecosystem">
              Ecosystem Development &amp; Marketing
            </a>
          </div>
          <div class="col-sm-4 col-xs-12">
            <a class="link-tile link-unstyled bg-neutral-flat-dark-darker" href="/org/services/#Development">
              Development Process
            </a>
          </div>
          <div class="col-sm-4 col-xs-12">
            <a class="link-tile link-unstyled bg-primary-light" href="/org/services/#IT">
              IT Infrastructure
            </a>
          </div>
        </div>
      </div>
    </section>
    <section class="driving-shared-innovation-section row dark featured-section-row featured-section-row-dark text-center">
      <div class="container padding-y-60">
        <h3 class="h2 margin-bottom-40">Driving Shared Innovation</h3>
        <p>
Whether you intend on contributing to Eclipse technologies that are
important to your product strategy, or simply want to explore a
specific innovation area with like-minded organizations, the Eclipse
Foundation is the open source home for industry collaboration.
        </p>
      </div>
      <div class="circular-container circular-container-white text-black overflow-hidden">
        <div class="big-quote margin-x-auto margin-bottom-30">
          <img class="img img-responsive" src="./images/about/big-quote.png" alt="" />
        </div>
        <p class="break-spaces">
&ldquo;Even if we're competitors in the market, the ability to work together
on projects and share common platforms is highly valuable.&rdquo;
&mdash; Cédric Brun, CEO, OBEO
        </p>
        <a class="btn btn-secondary btn-lg btn-pill" href="/collaborations/">Learn More</a>
      </div>
      <div class="subsection padding-y-60 margin-top-60">
        <div class="container">
          <h3 class="h2 margin-bottom-40">Our Collaborations</h3>
          <div 
            class="eclipsefdn-weighted-collaborations" 
            data-count="3" 
            data-cache="true" 
            data-skeleton-load="true"
          >
          </div>
        </div>
      </div>
    </section>
    <section class="who-we-are-section row text-center padding-y-60">
      <div class="container">
        <h2 id="who-we-are">Who We Are</h2>
        <p>
          The Eclipse Foundation is an international non-profit association
          supported by member organisations who value open source as a key
          enabler for their business strategies.
        </p>
        <h3 class="h4 fw-600 margin-top-40" id="our-members">Our Members</h3>
        <ul class="eclipsefdn-members-list margin-top-30 margin-bottom-60" data-ml-template="carousel"></ul>
        <div class="row row-no-gutters text-black">
          <div class="col-sm-8">
            <div class="rollover-tile bg-neutral-flat padding-y-30" tabindex="0">
              <h3 class="h4 fw-600" id="join-our-team">Join Our Team</h3>
              <div class="rollover-tile-content">
                <p>
                  Join a collaborative team built around the core values of
                  service, respect, professionalism, and collegiality. We work
                  together to make a difference for our members and open source
                  communities.
                </p>
                <a class="btn btn-primary btn-block margin-top-40" href="/careers/">Join Us</a>
              </div>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="rollover-tile bg-primary-light text-black padding-y-30" tabindex="0">
              <h3 class="h4 fw-600" id="our-history">Our History</h3>
              <div class="rollover-tile-content">
                <p>
                  The Eclipse Project was created by IBM in November 2001 and
                  supported by a consortium of software vendors. The Eclipse
                  Project continues to be used by millions of developers. The
                  Eclipse Foundation was created in January 2004 as an
                  independent not-for-profit corporation to act as the steward
                  of the Eclipse community. The independent not-for-profit
                  corporation was created to allow a vendor-neutral, open, and
                  transparent community to be established around Eclipse.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="rollover-tile bg-neutral-flat-dark-darker text-black padding-y-30" tabindex="0">
              <h3 class="h4 fw-600" id="connection-to-europe">Our Connection to Europe</h3>
              <div class="rollover-tile-content">
                <p>
                  The Eclipse Foundation is an independent non-profit corporation
                  based in Brussels. Our open source development model fosters
                  digital transformation in Europe and beyond.
                </p>
                <div class="btn-group btn-group-justified" role="group">
                  <a class="btn btn-primary" href="https://outreach.eclipse.foundation/hubfs/EuropeanOpenSourceWhitePaper-June2021.pdf">White Paper</a>
                  <a class="btn btn-primary" href="https://www.eclipse.org/europe/eclipse-fact-sheet.pdf">Fact Sheet</a>
                  <a class="btn btn-primary" href="https://www.eclipse.org/europe/eclipse-slide-deck.pdf">Slide Deck</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

