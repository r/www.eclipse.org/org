<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

  <h1><?php print $pageTitle; ?></h1>
  <p>The Eclipse Foundation relies on the support of our members and contributions from the user
    community to service and grow the Eclipse ecosystem. We encourage companies that make extensive
    use of Eclipse technology to support the Eclipse community through the Eclipse Corporate Sponsor
    Program. The generous support of these Corporate Sponsors allows the Foundation to provide
    world-class support for the Eclipse open source projects.</p>
  <p>A company may become a corporate sponsor by making a financial contribution or an in-kind
    contribution of goods and services to the Eclipse Foundation. There are three tiers of
    sponsorship: 1) Platinum, 2) Gold and 3) Silver; each tier representing the level of annual
    sponsorship to the Eclipse Foundation.


  <h4>Platinum Sponsorship</h4>
  <ul>
    <li>Annual contribution of at least US$100,000</li>
    <li>Corporate logo placed on the Eclipse Foundation Sponsor page.</li>
    <li>Joint-press release issued between the Eclipse Foundation and Platinum sponsor.</li>
  </ul>
  <h4>Gold Sponsorship</h4>
  <ul>
    <li>Annual contribution of at least US$25,000</li>
    <li>Corporate logo recognized on the Eclipse Foundation Sponsor page.</li>
    <li>Supporting press quote from the Eclipse Foundation for sponsor announcement.</li>
  </ul>
  <h4>Silver Sponsorship</h4>
  <ul>
    <li>Annual contribution of at least US$5000</li>
    <li>Corporate logo recognized on the Eclipse Foundation Sponsor page.</li>
  </ul>
  <h3>How to Become a Corporate Sponsor</h3>
  <ul>
    <li><h4>Step 1</h4>Determine the level of sponsorship your company would like to make.

    <li><h4>Step 2</h4>Contact the <a href="mailto:membership@eclipse.org">Eclipse Foundation</a> to
      determine method of payment. In-kind contributions will require previous agreement from the
      Eclipse Foundation. Please note that is Eclipse Foundation is a not-for-profit corporation.
      Contributions or gifts to the Eclipse Foundation Inc. are not tax deductible as charitable
      contributions.

    <li><h4>Step 3</h4>Corporate sponsors, by signing our <a
      href="/org/documents/Eclipse%20Logo%20Agreement%20%202008_04_30%20final.pdf"
    >logo agreement</a>, agree to have their corporate logo listed on the Eclipse Corporate Sponsor
      page. If you do not want your logo listed, please inform the Eclipse Foundation.</li>
  </ul>
  <h3>Other Ways to Support Eclipse</h3>
  <p>
    The Eclipse Foundation is supported by our member companies through their contributions to the
    Eclipse projects and membership dues. If your company would like to <a href="/membership/">become
      more involved in the Eclipse community</a>, please explore the different membership options.
    Individuals may also contributed to the Eclipse Foundation through the <a href="/donate/">'Friends
      of Eclipse'</a> program.
  </p>

<div class="sideitem">
  <h6>Support Eclipse</h6>
  <ul>
    <li class=""><a href="http://www.eclipse.org/corporate_sponsors/" class="link">Corporate
        Sponsors</a><br /></li>
    <li class=""><a href="http://www.eclipse.org/membership/exploreMembership.php" class="link">Eclipse
        Members</a><br /></li>
  </ul>
</div>
