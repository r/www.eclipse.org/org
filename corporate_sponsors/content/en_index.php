<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>The Eclipse Foundation relies on the support of our members and contributions from the user
    community to service and grow the Eclipse ecosystem. We'd like to thank the following Corporate
    Sponsors who have generously supported the Eclipse community.</p>
  <p>
    We encourage large corporate users of Eclipse to support the community by <a href="/membership/">becoming
      members</a> and/or joining the <a href="../documents/eclipse-foundation-sponsorship-agreement.pdf">Corporate Sponsorship
      Program</a>.
  </p>
      <div class="sideitem">
      <h6>Corporate Sponsors</h6>
        <ul class="list-inline margin-top-30 margin-bottom-30">
          <li class="margin-right-30">
            <img alt="Debeka" src="images/debeka.jpg" width="140"/>
          </li>
        </ul>
      </div>
</div>
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Supporting Eclipse</h6>
    <div>
      <ul>
        <li><a href="http://www.eclipse.org/membership/exploreMembership.php" class="link">Eclipse
            Members</a><br /></li>
        <li><a href="../documents/eclipse-foundation-sponsorship-agreement.pdf" class="link">Corporate Sponsorship Program</a><br /></li>
      </ul>
    </div>
  </div>
</div>
