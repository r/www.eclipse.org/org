<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>
    There are three simple ways you can financially support the Eclipse Foundation. Choose whichever
    way is best for you, and please do not hesitate to <a href="mailto:membership@eclipse.org">contact
      us.</a>
  </p>
  <h2>1 - Become an Associate Member</h2>
  <ul>
    <li>What are the benefits of becoming an Associate Member?</li>
    <p>
      The key benefit is supporting the infrastructure that brings you <a
        href="http://wiki.eclipse.org/images/c/ca/Eclipse_foundation_about_us.pdf"
      >great open source software</a>! In addition to that, we'll put you on the inside track of
      information about the Eclipse Ecosystem, invite you to members' meetings, and give you
      discounts to Foundation events &mdash; and your staff will appreciate knowing you support free
      software too. If you would like more benefits, such as help promoting your products and
      services to the Eclipse Ecosystem, voting rights, and board representation, consider <a
        href="http://www.eclipse.org/membership/become_a_member/" target="_blank"
      >other membership options</a>.
    </p>
    <li>How do I become an Associate Member?</li>
    <p>
      It's easy &mdash; just complete and fax these three documents listed on our <a
        href="http://www.eclipse.org/membership/become_a_member/membershipProcess.php"
      >Membership Page</a>. It really is quite simple. One doc is simply for your contact
      information. Another doc says it's ok for us to post your logo (and vice versa). The third doc
      has a lot of stuff relevant to other membership classes; for Associate Membership, it
      basically says that you agree to support the Foundation and we agree to invite you to the
      members' meetings and events.
    </p>
    <li>How much is Associate Membership?</li>
    <p>The minimum contribution is $5,000 USD per year. If it's more convenient for you, we can
      break that into quarterly payments.</p>
  </ul>
  <h2>2 - Be a Corporate Sponsor</h2>
  <p>
    If Associate Membership isn't your style, there are other ways to financially support the
    Eclipse Foundation. Please <a href="mailto:membership@eclipse.org">contact us</a> to discuss
    some of the options including:</p>
  <ul>
    <li>Being a logo sponsor of Eclipse Foundation events</li>
    <li>Being a corporate sponsor for some of our <a
      href="http://www.eclipse.org/org/foundation/thankyou.php"
    >infrastructure</a></li>
    <li>Simply making a cash contribution</li>
  </ul>
  <h2>3 - Become a Friend of Eclipse</h2>
  <p>
    If having your organization become a member or sponsor isn't in the cards, and you still want to
    show support for the Eclipse Foundation, consider personally becoming a <a
      href="http://eclipse.org/donate/index.php"
    >Friend of Eclipse</a>. <br> <br> <br>
  </p>
</div>
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Some of our supporters...</h6>
    <div style="text-align: center">
      <?php print $support_logos; ?>
    </div>
  </div>
  <div class="sideitem">
    <h6>More About Our Supporters:</h6>
    <ul>
      <li><a href="http://www.eclipse.org/membership/exploreMembership.php" class="link">Organization
          Supporters</a><br /></li>
      <li><a href="http://eclipse.org/donate/donorlist.php" class="link">Individual Supporters</a><br /></li>
    </ul>
  </div>
</div>
