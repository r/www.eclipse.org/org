<?php

/**
 * Copyright (c) 2021 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - initial API and implementation
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?></h1>
    <p>
      The Eclipse Foundation provides four key services to the Eclipse community: 1) <a href="/org/services/#IP_Management">IP Management</a>, 2) <a href="/org/services/#Ecosystem">Ecosystem Development and Marketing</a>, 3) <a href="/org/services/#Development">Development Process</a>, and 4) <a href="/org/services/#IT">IT Infrastructure</a>. Full-time staff are
      associated with each of these areas and work with the greater Eclipse community to assist in
      meeting the needs of stakeholders.
    </p>
    <h2 id="IP_Management">Intellectual Property (IP) Management</h2>
    <p>
      An important aspect of the Eclipse Foundation is the focus on enabling the use of open source technology in commercial software products and services. We consciously promote and encourage software vendors to use Eclipse technology for building their commercial software products and services. This is made possible by the fact that all Eclipse projects are licensed under a commercial-friendly OSI-approved license.
    </p>
    <p>
      The Eclipse Foundation also undertakes a number of steps to attempt to ensure the pedigree of the intellectual property contained within Eclipse projects. The first step in the due diligence process is to ensure that all contributions are made by the rightful copyright holder and under the project license. All committers are required to either sign the <a href="/legal/committer_process/EclipseIndividualCommitterAgreement.pdf">Individual Committer Agreement</a> that stipulates all of their contributions are their original work and are being contributed under the project license; or work on behalf of an <a href="/membership/">Eclipse Member</a> organization that has signed a <a href="/legal/committer_process/EclipseMemberCommitterAgreement.pdf">Member Committer and Contributor Agreement</a> to ensure the intellectual property rights of the organization are contributed under the project license.
    </p>
    <p>
      The second step is that the source code related to all contributions which are developed outside of the <a href="/projects/dev_process/">Eclipse Foundation Development Process</a> is vetted through the Eclipse Foundation <a href="/projects/handbook/#ip-third-party">third-party content due diligence process</a>. This process includes analyzing selected code contributions to ascertain license compatibility with the project license. Contributions that contain code licensed under licenses that are not compatible with the project license are screened out through this approval process and thus not added to an Eclipse project. The end result is a level of confidence that Eclipse open source projects release technology that can be safely distributed in commercial products.
    </p>
    <h2 id="Ecosystem">Ecosystem Development and Marketing</h2>
    <p>
      The Eclipse Foundation&apos;s vendor-neutral ecosystem development and
      marketing programs promote community and membership growth, drive
      awareness and discoverability of <a href="/projects">projects</a> and <a href="/org/workinggroups/">working groups</a>, and
      increase the commercial adoption of Eclipse technologies.</p>
    <p>
      The Eclipse Foundation coordinates and implements a variety of
      activities, including content creation, press and analyst relations,
      community conferences and events, advertising, social media
      management, and other programs to promote the entire Eclipse
      community and increase engagement.
    </p>
    <p>
      Find out more about the <a href="/org/services/marketing/">marketing services</a> provided by the Foundation.
    </p>
    <h2 id="Development">Development Community Support</h2>
    <p>The Eclipse community has a well-earned reputation for providing quality software in a
      reliable and predictable fashion. This is due to the commitment of the committers and
      organizations contributing to the open source projects. The Eclipse Foundation also provides
      services and support for the projects to help them achieve these goals.</p>
    <p>
      The Foundation staff help open source project teams implement the Eclipse Foundation&apos;s <a href="/projects/handbook/#ip">intellectual property due diligence process</a>, the <a href="/projects/dev_process/">Eclipse Foundation Development Process</a> and—for those projects that are building specifications—the <a href="/projects/efsp/">Eclipse Foundation Specification Process</a>. These processes assist new project startup and ensure that all
      Eclipse projects are run in an open, transparent, and meritocratic manner. As part of this
      process, the Foundation organizes member community reviews for projects to ensure consistent
      interaction between the projects and the broader membership.
    </p>
    <h2 id="IT">IT Infrastructure</h2>
    <p>The Eclipse Foundation manages the IT infrastructure for the Eclipse open source community,
      including Git code repositories and code review tools, bug trackers, Jenkins build farm,
      development-oriented mailing lists and forums, download site and website. The infrastructure
      is designed to provide reliable and scalable service for the committers developing the Eclipse
      technology and the consumers who use the technology.</p>
  </div>
</div>