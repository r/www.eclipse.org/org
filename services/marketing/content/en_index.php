<?php
/**
 * Copyright (c) 2021 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - initial API and implementation
 */
?>

<div id="maincontent">
  <div id="midcolumn">
    <h1><?php print $pageTitle; ?>
    </h1>
    <p>The Eclipse Foundation&rsquo;s vendor-neutral marketing programs promote Eclipse community growth and
      engagement. Our team of product marketing, digital marketing, content marketing, PR/communications, graphic
      design, and event management professionals collaborate with the Eclipse Foundation community to:</p>

    <ul>
      <li>drive the awareness of and participation in the Foundation&rsquo;s projects, working groups, and strategic
        initiatives;</li>
      <li>increase the commercial adoption of Eclipse Foundation technologies;</li>
      <li>grow diverse, global, and sustainable developer communities and ecosystems;</li>
      <li>highlight the open source expertise, contributions, and successes of community members; and</li>
      <li>enable collaboration and co-marketing between member organizations.&nbsp;</li>
    </ul>

    <p>We achieve these goals with the contributions and support of the developer community and member
      companies. To help facilitate collaboration, we have a number of marketing programs you can get involved in:</p>

    <div class="row">
      <div class="panel-container-shadow col-xs-12 match-height-item-by-row">
        <a href="#promoting"> 
          <div class="panel-shadow panel panel-default">
            <div class="panel-heading" aria-hidden="true"><img class="img img-responsive" src="images/promoting.jpg">
            </div>
            <div class="panel-body">
              <p>Promoting Community News &amp; Events</p>
            </div>
          </div>
        </a>
      </div>

      <div class="panel-container-shadow col-xs-12 match-height-item-by-row">
        <a href="#events">
          <div class="panel-shadow panel panel-default">
            <div class="panel-heading" aria-hidden="true"><img class="img img-responsive" src="images/events.jpg">
            </div>
            <div class="panel-body">
              <p>Eclipse Foundation Events</p>
            </div>
          </div>
        </a>
      </div>

      <div class="panel-container-shadow col-xs-12 match-height-item-by-row">
        <a href="#releases_announcements">
          <div class="panel-shadow panel panel-default">
            <div class="panel-heading" aria-hidden="true"><img class="img img-responsive" src="images/releases_announcements.jpg">
            </div>
            <div class="panel-body">
              <p>Press  Releases and Announcements</p>
            </div>
          </div>
        </a>
      </div>

      <div class="panel-container-shadow col-xs-12 match-height-item-by-row">
        <a href="#creative_services">
          <div class="panel-shadow panel panel-default">
            <div class="panel-heading" aria-hidden="true"><img class="img img-responsive" src="images/creative_services.jpg">
            </div>
            <div class="panel-body">
              <p>Creative Services</p>
            </div>
          </div>
        </a>
      </div>
      
      <div class="panel-container-shadow col-xs-12 match-height-item-by-row">
        <a href="#content_creation">
          <div class="panel-shadow panel panel-default">
            <div class="panel-heading" aria-hidden="true"><img class="img img-responsive" src="images/content_creation.jpg">
            </div>
            <div class="panel-body">
              <p>Content Creation</p>
            </div>
          </div>
        </a>
      </div>

      <div class="panel-container-shadow col-xs-12 match-height-item-by-row">
        <a href="#member_case_studies">
          <div class="panel-shadow panel panel-default">
            <div class="panel-heading" aria-hidden="true"><img class="img img-responsive" src="images/member_case_studies.jpg">
            </div>
            <div class="panel-body">
              <p>Member Case Studies</p>
            </div>
          </div>
        </a>
      </div>

      <div class="panel-container-shadow col-xs-12 match-height-item-by-row">
        <a href="#market_intelligence_surveys">
          <div class="panel-shadow panel panel-default">
            <div class="panel-heading" aria-hidden="true"><img class="img img-responsive" src="images/market_intelligence_surveys.jpg">
            </div>
            <div class="panel-body">
              <p>Market Intelligence and Surveys</p>
            </div>
          </div>
        </a>
      </div>

    </div>
    


    <h2 id="promoting">Promoting Community News &amp; Events</h2>

    <p>Working groups, projects, and members can share their news and events with the Eclipse Foundation community by
      submitting the details through our <a href="https://newsroom.eclipse.org/">newsroom</a>.</p>

    <p>The Eclipse newsroom is a content management system that allows us to post and distribute news and
      events to <a href="https://eclipse.org/">eclipse.org</a> and working group sites. Anyone with an Eclipse account
      can submit news (articles, blogs, press releases) or an event (conference, webinar) that they think would be
      relevant to our community. Simply submit the details of your news/event and a member of the Foundation
      staff will review it. Once approved, the news and events will be displayed on eclipse.org and the relevant working
      group sites. We also share these submissions through our social media channels and our newsletters, when
      appropriate.</p>

    <h2 id="events">Eclipse Foundation Events</h2>

    <p>Events offer an opportunity for community members to demonstrate thought leadership by sharing best
      practices while building and driving engagement in our ecosystem. In collaboration with our community and working
      groups, the Foundation plans, coordinates, implements, and promotes conferences, events, and webinars.</p>

    <p>A full list of upcoming events can be found at <a href="https://events.eclipse.org/">events.eclipse.org</a>. If
      you would like to propose an event, present a
      webinar or sponsor an Eclipse Foundation event, please reach out to us at <a
        href="mailto:events@eclipse.org">events@eclipse.org</a>.</p>

    <h2 id="releases_announcements">Press Releases and Announcements</h2>

    <p>The Eclipse Foundation has been very successful in developing strong press relationships resulting in
      community-related news being frequently reported. As the number of projects, working groups, and members grow,
      media interest in the Eclipse ecosystem is also growing. The Eclipse Foundation will provide press assistance and
      quotes for a number of events, which are outlined in our<a
        href="https://www.eclipse.org/org/press-release/pressrelease_guidelines.php"> press release guidelines</a>.</p>

    <h2 id="creative_services">Creative Services</h2>

    <p>Great creative design is a critical component of brand building. The Foundation&rsquo;s creative team
      can assist in the development of logos, graphics, videos, swag, website design, or other creative requests.
      Working groups, projects, and members who are looking for creative assistance should fill out our <a
        href="https://forms.gle/ZPdN6KmiU1YFwf3a6" target="_blank">creative brief</a>, so the Foundation team can determine the scope of
      the project and what support we can provide.&nbsp;</p>

    <p>Projects teams can also engage a graphic design crowdsourcing site or similar service to create a
      project logo. The Foundation will reimburse up to &euro;500 for the logo design. Consult the <a
        href="https://www.eclipse.org/projects/handbook/#trademarks-project-logo">project handbook</a> for more details.
    </p>

    <p>For questions about our creative services, please contact <a
        href="mailto:creative@eclipse-foundation.org">creative@eclipse-foundation.org</a>.&nbsp;
    </p>

    <h2 id="content_creation">Content Creation</h2>
    <p>
      Working with the community, the Eclipse Foundation creates content that showcases the growth and momentum of the projects, working groups, and our ecosystem at large. We collaborate on a wide range of content, including <a href="#member_case_studies">case studies</a>, <a href="#market_intelligence_surveys">surveys</a>, <a href="/community/eclipse_newsletter/">newsletter articles</a>, <a href="https://blogs.eclipse.org/">blog posts</a>, and <a href="https://www.youtube.com/user/EclipseFdn" target="_blank">videos</a>. 
    </P>
    <p>
      Our marketing team partners with individual community members and member organizations to create and cross-promote content for developers and decision-makers with the goal of increasing awareness, contributions, adoption, and support of initiatives and projects that matter to our community.
    </p>
    <p>
      To ensure high quality and consistent content, we follow the <a href="/org/documents/writing-style-guide/">Eclipse Foundation writing style guide</a>. 
    </p>

    <h2 id="member_case_studies">Member Case Studies</h2>

    <p>The Eclipse Foundation publishes a <a href="/org/value/#success_stories">member case study series</a> to highlight how organizations of all sizes
      are leveraging their strategic involvement in our ecosystem to fuel market success. This program features
      real-world use cases of the impact of Eclipse Foundation projects, processes, and governance on our members, their
      customers, and industry stakeholders.</p>

    <p>To create a case study, the Foundation team will interview one or more team members from a member
      organization. This takes approximately an hour. The case studies are then written from the interview and shared
      for review, fact-checking, and any necessary approvals prior to publication.</p>

    <p>We publish all of our member case studies under the Creative Commons Attribution 4.0 International (CC
      BY 4.0) license. As long as you provide attribution according to the terms of the license, you can:&nbsp;</p>

    <ul>
      <li>copy and redistribute the case study content in any medium or format;&nbsp;</li>
      <li>adapt, transform, and build on the material for any purpose.&nbsp;</li>
    </ul>

    <p>All Eclipse Foundation members are welcome to <a href="https://forms.gle/mbEnoWsehD6kEUeM7" target="_blank">participate in our
        member case study initiative</a>.</p>

    <h2 id="market_intelligence_surveys">Market Intelligence and Surveys</h2>
    <p>
      The Foundation’s marketing team engages the community and industry at large to gather market insights by conducting surveys related to our core technology pillars, including enterprise Java, IoT, edge computing, and cloud development tools. We deliver high-quality developer market intelligence and related content that demonstrate our thought leadership and are frequently cited by press and industry analysts. <a href="/org/value/#surveyReportsCtn">Survey reports</a> include data, trends, and insights that are valuable to our projects, members, working groups and the broader industry.
    </p>
  </div>
</div>