<?php
/**
 * Copyright (c) 2005, 2017, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation)
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "About the Eclipse Foundation";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("about, documents, history");
$Theme->setPageAuthor("Mike Milinkovich");
$Theme->setThemeVariables(array('main_container_classes' => 'container-fluid'));

ob_start();
?>
<div class="about-page featured-jumbotron jumbotron">
  <div class="container">
    <p class="sr-only">
      Respect, Community, Innovation, Transparency &mdash; it's in our code.
    </p>
    <div class="video-wrapper">
      <video 
        preload="metadata"
        autoplay
        muted
        loop
      >
        <source src="./images/about/its-in-our-code.mp4" type="video/mp4">
      </video>
    </div>
  </div>
</div>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();

