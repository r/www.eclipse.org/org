<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'


	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Eclipse and Social Coding FAQ";
	$pageKeywords	= "github, bitbucket, development, git, eclipse, social coding";
	$pageAuthor		= "Mike Milinkovich";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
	# $Nav->addCustomNav("My Link", "mypage.php", "_self", 1);
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 1);

	# End: page-specific settings
	#
		
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML

    <div id="midcolumn">
    <h1>$pageTitle</h1>
    <p>Last updated June 20, 2013</p>
    
    <h3>Table of Contents</h3>
	<ol>
		<li><a href="#what">What is the Eclipse Foundation doing?</a></li>
		<li><a href="#why">Why is the Eclipse Foundation doing this?</a></li>
		<li><a href="#whygood">Why would making use of this program be good for my project?</a></li>
		<li><a href="#really">If their day-to-day development is hosted someplace else, are these <i>really</i> Eclipse projects?</a></li>
		<li><a href="#independent">Does this affect the independence of the Eclipse Foundation?
		<li><a href="#which">Which third-party forges are you going to support?
		<li><a href="#future">This sounds interesting, and it&rsquo;s something I might want to do eventually. How can I make sure that my GitHub project&nbsp;could take advantage of this in the future?</a></li>
		<li><a href="#limitations">Any limitations I should know about?</a></li>
		<li><a href="#plugins">Does this only apply to projects written in Java or using the Eclipse plug-in model?</a></li>
		<li><a href="#issues">Where will issues be tracked?</a></li>
		<li><a href="#wikis">What about the other community-facing services such as mailing lists, wikis, etc.?</a></li>
		<li><a href="#existing">When can my existing Eclipse Foundation project move to github?</a></li>
		<li><a href="#move">I&rsquo;m working on an existing Eclipse project. Does this mean I have to move?</a></li>
		<li><a href="#contact">This is cool! Who can I talk to about bringing my project to the Eclipse community? </a></li>
	</ol>
	
	<h3>FAQ</h3>
 	<ol>
 		<li><a name="what">What is the Eclipse Foundation doing?</a><br/><br/>
 		
 		The Eclipse Foundation is going to start allowing projects host their mainline development on third party forges 
 		such as GitHub, and Bitbucket. This means that an Eclipse project will be able to leverage many of the great development 
 		support tools provided by&nbsp;those vendors. The first project that we are going to work with on this is vert.x, which 
 		has been hosted on GitHub&nbsp;since its inception. Our intent is to start small, and continually improve this program 
 		and our support of it. The vert.x project has kindly agreed to be our guinea pig in this experiment.<br/><br/>
 			
 		Projects which choose to take advantage of this will be hosting their mainline development remotely, but all of their code will 
 		be mirrored back to Eclipse&rsquo;s git repositories. The full Eclipse development and IP processes will be applied to 
 		these projects. Project metadata, plans, committer records and votes will be maintained in the Eclipse project management 
 		infrastructure (PMI). Admin rights to the repos will be owned by the Eclipse Foundation webmaster team.<br/><br/>
 		
 		Initially, most of this will be maintained manually, but over time we expect to automate a great deal of it. For 
 		example, initially checking that a pull request&rsquo;s author has signed an Eclipse CLA will be done manually by 
 		the project committer accepting the contribution. Over time that will be automated using git commit hooks. <br/></li>
 			
 		<li><a name="why">Why is the Eclipse Foundation doing this?</a><br/><br/>
 		
 		We want to bring the Eclipse way of vendor neutral, open, transparent, and meritocratic open source development to the 
 		third party forge sites loved by developers around the world.<br/><br/>The key services that Eclipse provides are its 
 		vendor neutrality, its development process and its intellectual property management. We take a lot of pride in our 
 		well-supported development forge. But it is clear that many developers and projects are present on third party forge sites, 
 		and love the tools provided by those sites. We also want to continually make it easier for people to contribute to Eclipse 
 		projects, and the pull request has become the most popular way to do that.<br/></li>
 		
 		<li><a name="whygood">Why would making use of this program be good for my project?</a><br/><br/>
 		
 		It is definitely <i>not</i> for all projects. Not every open source project is interested in adding the governance, 
 		development and IP processes inherent in being part of an open source foundation such as Eclipse.<br/><br/>
 		
 		Third party forges are wonderful for developers building their code. But when an open source project becomes successful, 
 		other stakeholders start to take an interest in the project. Users and adopters - especially commercial ones - are very 
 		interested in matters such as governance, intellectual property management and independence of the projects that they run 
 		their businesses on, or embed in their products. <br/><br/>
 		
 		This program is initially applicable to those projects which have 
 		started on third party forge sites,&nbsp;but now feel that it would be valuable to have vendor-neutral governance, transparent 
 		and meritocratic open source development process, and intellectual property management taken care. After we ramp up, it will 
 		also allow existing Eclipse projects to tap into the diversity and contribution flow of the millions of developers which 
 		frequent these popular sites.<br/></li>
 		
 		<li><a name="really">If their day-to-day development is hosted someplace else, are these <i>really</i> Eclipse projects?</a><br/><br/>
 		
 		Yes. Here are a few things to keep in mind:<br/>
 		
 		<ol>
	 		<li>The operations of the project would be under the auspices of an Eclipse Project Management Committee (PMC), and the full Eclipse Development 
	 		Process would apply.</li>
	 		<li>The project&rsquo;s trademark would be owned by the Eclipse Foundation.</li> 		
	 		<li>The project would be required to maintain its metadata, and manage its committer roles via the Eclipse Project Management Infrastructure.</li>
	 		<li>The project would be required to be in compliance with all of the intellectual property management policies of the Eclipse Foundation, 
	 		including use of IPzilla to manage their IP.</li>
	 		<li>The project can participate in the annual simultaneous release if they wish.</li>
	 		<li>The project would be required to use the Eclipse Foundation signing service to digitally sign their releases as coming from the 
	 		Eclipse Foundation.</li><li>The project would be required to maintain downloads from the Eclipse servers and mirrors.</li>
 		</ol>
 		
 		<li><a name="independent">Does this affect the independence of the Eclipse Foundation?</a><br/><br/>
 		
 		What matters to us is that any particular third party forges vendor could disappear tomorrow, or change their policies regarding 
 		free repositories for open source prjects, and the Eclipse project will be easily able to pick up and carry on.<br/><br/>
 		
 		This would basically not work if it wasn&rsquo;t for the fact that the Eclipse community has almost entirely switched over to using 
 		the git distributed version control system. Because git is inherently distributed, we can easily maintain a complete record of all of 
 		the code of a project on Eclipse servers, even if the day-to-day development is hosted elsewhere. We will also be maintaining complete 
 		records of the IP contributions, dependencies, committer rolls, etc. in the standard Eclipse Foundation systems like the project management 
 		infrastructure (PMI) and IPzilla.<br/></li>
 		
 		<li><a name="which">Which third-party forges are you going to support?</a><br/><br/>
 		
 		We&rsquo;re starting with GitHub, and we&rsquo;ve starting exploring possibilities with Bitbucket. But the general criteria that we&rsquo;re 
 		looking at includes at least the following points:<br/>
 		<ol>
	 		<li>Has to support git. We rely on the distributed nature of git.</li>
	 		<li>Has to have the APIs we need. We need to be able to do things like have commit hooks which check for CLAs, and team APIs that allow us 
	 		to administer the committer lists on a project repo.</li>
	 		<li>Has to be a successful, well-known place for hosting open source projects as a general service.</li>
	 		<li>Has to be worth doing. Building and supporting interfaces will require resources from the Eclipse Foundation, and there has to 
	 		be enough projects who are interested in making that happen.</li>
 		</ol>
 		
 		<li><a name="future">This sounds interesting, and it&rsquo;s something I might want to do eventually. How can I make sure that my GitHub project&nbsp;could take advantage of this in the future?</a><br/><br/>
 		
 		The biggest topics to take care of are licensing and careful tracking of your intellectual property provenance.<br/><br/>
 		
 		With regards to licensing, there are certain licenses which are acceptable to the Eclipse Foundation, and certain ones which 
 		are not. This applies to both your project&rsquo;s code, and to the dependencies you rely upon. The short version is that the 
 		GPL and LGPL are out, and the EPL, BSD and&nbsp;Apache licenses are in. The longer version can be found in our 
 		<a class="c10" href="http://www.eclipse.org/legal/eplfaq.php#3RDPARTY">EPL FAQ</a>.<br/><br/>
 		
 		With regards to provenance, it is critical that you put an OSI-approved license on your project, and that you ensure that all committers 
 		and contributors have explicitly and in writing indicated that they are providing their contributions under that license. You may want to 
 		use a Contributor License Agreement (CLA) to do that. If you want to have the freedom to re-license that project at some point in the 
 		future, you may want have that CLA provide you with sufficient rights to do so.</li><br/>
 		
 		<li><a name="limitations">Any limitations I should know about?</a><br/><br/>
 		
 		There are likely lots, but here are a couple that you should know about.<br/>
 		
 		<ol>
	 		<li>The Eclipse Foundation does not currently allow all that wide a range of licenses. The Eclipse Public License (EPL) is the default. 
	 		With the approval of the Board, we have a number of projects which are dual-licensed under the EPL and the Apache License, and the EPL 
	 		and the BSD(<a href="documents/edl-v10.php">EDL</a>). We are willing to consider other permissive licenses on a case-by-case basis.</li>
	 		<li>There are likely going to be some limitations on how your current repository&rsquo;s history can be brought over to your new Eclipse 
	 		project. We are working out exactly what those limitations are.</li>
 		</ol>

 		<li><a name="plugins">Does this only apply to projects written in Java or using the Eclipse plug-in model?</a><br/><br/>
 		
 		Nope. The Eclipse community is now technology agnostic. We have projects using JavaScript, Lua, C and other languages. What we require of 
 		projects at the Eclipse community are two things: (a) a commitment to follow our principles of openness, transparency, meritocracy and 
 		vendor neutrality, and (b) a willingness to follow our development and intellectual property processes.</li><br/>
 		
 		<li><a name="issues">Where will issues be tracked?</a><br/><br/>
 		
 		At least for the time being, all Eclipse projects must use our instance of Bugzilla for tracking bugs and other issues.</li><br/>
 		
 		<li><a name="wikis">What about the other community-facing services such as mailing lists, wikis, etc.?</a><br/><br/>
 		
 		To be honest, this is still a work in progress. Here is where we are at the moment:<br/>

 		<ol>
	 		<li>As mentioned above, every Eclipse project will have to use our Bugzilla for bug and issue tracking.</li>
	 		<li>Mailing lists and forums are a bit complicated, as lots of people prefer using facilities like Google Groups and Stackoverflow. We are 
	 		going to ask every project to have a dev list hosted at Eclipse, and to monitor the project forum on eclipse.org. But if they have 
	 		other communication channels, that&rsquo;s OK too. We want to encourage people to have discussions about Eclipse projects.</li>
	 		<li>We&rsquo;re still discussing what to do with wikis. </li>
 		</ol><br/>
 		
 		<li><a name="existing">When can my existing Eclipse Foundation project move to github?</a><br/><br/>
 		
 		Please be patient. We want to start small and gradually grow this. In particular, we need to implement a sufficient amount of automation to 
 		ensure that we are not overwhelmed by manually supporting these processes.<br/></li>
 
 		<li><a name="move">I&rsquo;m working on an existing Eclipse project. Does this mean I have to move?</a><br/><br/>
 		
 		No! Eclipse projects can choose where they want to host their day-to-day development. Hosting at eclipse.org, and using the tools and 
 		infrastructure there is perfectly fine.<br/></li>
 		
 		<li><a name="contact">This is cool! Who can I talk to about bringing my project to the Eclipse community?</a><br/><br/>
 		
 		Please contact <a class="c10" href="mailto:emo@eclipse.org">emo@eclipse.org</a>. </li>
 		
 		</ol><br/>
 		
  	</div>
  

EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
