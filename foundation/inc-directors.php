<?php
/**
 * Copyright (c) 2023 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Olivier Goulet (Eclipse Foundation) - initial implementation 
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/directors/directorsList.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ("../_projectCommon.php");

$DirectorsList = DirectorsList::getInstance();

// Begin: page-specific settings. Change these.
$pageTitle = "Eclipse Foundation, Inc. Board of Directors";
$pageKeywords = "foundation, board, legal, inc, us";

$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Contact,email,telephone,support,problem");

// Place your html content in a file called content/en_pagename.php
ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();
