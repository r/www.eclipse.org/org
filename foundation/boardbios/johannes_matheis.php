<div class="row">
  <div class="col-sm-4 col-xs-24">
    <img 
      class="img-responsive" 
      src="boardbios/photos/johannes-matheis.jpg" 
      align="left" 
      style="padding-right: 5px;" 
      width="120"
    />
  </div>
  <div class="col-sm-20">
    Dr. Johannes Matheis is a Senior Manager at Vector Informatik GmbH, a company supporting 
    manufacturers and suppliers in the automotive and related industries with tools, software, and 
    services for developing embedded systems. He currently leads the software development group of 
    PREEvision. Johannes graduated with a diploma in Computer Science in 2004, and while working as 
    a developer at the start-up Aquintos he subsequently completed his PhD at KIT (Karlsruher 
    Institute for Technology). His areas of special interest include "model-based software 
    engineering" and software development in Java. Since September 2021 he has served as a steering 
    committee member on the Eclipse IDE WG.
  </div>
</div>