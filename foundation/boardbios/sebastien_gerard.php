<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/sebastien_gerard.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">

<p>Sébastien Gérard is director of research at CEA and he is leading the LISE laboratory (Laboratory of Model Driven Engineering for Embedded Systems) at <a target="_blank" href="http://www-list.cea.fr">CEA LIST</a>. Working on research issues related to complex and critical system and software design for more than 15 years, his research interests include correct-by-construction specification and design of complex systems, model-based engineering of complex systems, and visual modeling language engineering. He is the CEA representative at Object Management Group (OMG) for more than 15 years. In particular, he is the chair of the MARTE standardization task force. He is also leading the open source project, <a href="www.eclipse.org/papyrus">Papyrus</a>, the UML modeling tools of Eclipse.</p> 
<p>In 1995, he has a diploma in mechanics and aeronautics from the ENSMA high-school, in 2000 he obtained a PhD diploma in Computer Science from the Evry university and in 2013 he got his “habilitation à diriger des recherches” diploma in the domain of computer science from the Orsay Univiersity.</p>

</div>
</div>