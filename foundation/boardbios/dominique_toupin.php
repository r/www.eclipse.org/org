<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Dominique_Toupin.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Dominique Toupin is managing Engineering and IT tools at Ericsson where he has a passion 
for productivity in the development of a broad range of systems, from general purpose databases 
to real-time and specialized systems. He enjoys open innovations and creating significant technology 
improvements by collaborating with different types of companies, creating research projects and open 
source initiatives. At Eclipse he is Chairman of <a href="https://www.polarsys.org/">PolarSys</a>, Director at the Foundation’s Board of 
Directors, he is behind many significant improvements in <a href="https://eclipse.org/cdt/">CDT</a>, 
<a href="https://projects.eclipse.org/proposals/egerrit">EGerrit</a>, <a href="https://www.eclipse.org/emf/compare/">EMF Compare</a>, 
<a href="https://eclipse.org/linuxtools/">Linux Tools</a>, <a href="https://eclipse.org/papyrus/">Papyrus</a>, 
<a href="https://projects.eclipse.org/projects/tools.titan">Titan</a>, 
<a href="http://tracecompass.org/">Trace Compass</a>, and has managed the CDT/TraceCompass/LinuxTools/PTP and modeling summits.</p></div>
</div>