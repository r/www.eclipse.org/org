<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Michael_Bechauf.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
Michael is responsible for SAP's standards strategy and participation in industry consortia. He is also
responsible for the SAP Developer Network, a community of 1.4 million individual developers using the SAP
NetWeaver platform. Michael has been a member of the JCP EC since 2001 and a Board member of the Eclipse
Foundation since 2002.</div>
</div>