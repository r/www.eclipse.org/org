<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Stephane_Lacrampe.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
Stephane Lacrampe is the CEO of Obeo. He's got a technical background in mathematics combined with IT. He
started his career in the Telecom industry before switching to Internet technologies, working in a startup
in London for two years. He also worked for very large insurance, banks and services companies back in
France, where he learned about technical and methodological aspects of working with modeling technologies.
<br><br>
Stephane Lacrampe founded his own private company, Obeo, with two other associates in 2005, in order to
provide innovative modeling and reverse engineering solutions based on Eclipse technologies. He is now in
charge of growing the company and also focuses on growing the French Eclipse community.</div>
</div>