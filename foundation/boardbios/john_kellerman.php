<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/John_Kellerman.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
John Kellerman joined IBM in 1984 with a computer science degree from Purdue University.  He's since completed graduate
degrees in Computer Engineering at North Carolina State and Business Administration at the University of North
Carolina Chapel Hill. He has spent the majority of his years at IBM in the development and management of
application development tool products, including ISPF/PDF, VisualAge Smalltalk, Eclipse and Jazz.  John was
a founding member of the Eclipse project. He is currently Product Manager for Jazz and Eclipse at IBM.
Finally, John co-authored the award winning book, <i>Java Developer's Guide to Eclipse</i>. See
<a href="http://www.jdg2e.com">www.jdg2e.com</a>.</div>
</div>