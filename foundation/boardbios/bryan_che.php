<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Bryan_Che.png" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Bryan Che is Chief Strategy Officer at Huawei. There, he leads Huawei’s vision and strategy across its overall businesses and portfolio, which spans mobile and consumer electronics, telecommunications, enterprise IT, and cloud.</p>
<p>Bryan has tremendous experience building new enterprise businesses and open source technologies. Prior to joining Huawei in China, Bryan spent over 15 years at Red Hat in the US, where he was the general manager of their cloud business and also led overall product strategy across the company. As an American expat employed in China who has traveled 3 million miles to over 40 countries, Bryan has worked extensively with many customers and partners around the world. </p>
<p>Bryan graduated with his Bachelors and Masters degrees in computer science from MIT.</p>
 </div>
</div>