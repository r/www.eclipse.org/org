<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Deborah_Bryant.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
    <p>Deborah Bryant is Senior Director, Open Source Program Office, Office of the CTO at Red Hat.</p>
    <p>Deborah’s involvement in open source began in the 90s in Portland, Oregon where she became deeply involved in the community, helped grow OSUOSL into an international community resource, founded the Government Open Source Conference, and championed the global use of FOSS in the public sector.  Deb lent her voice to supporting open source project and developers, building bridges between academia, industry, and government along the way.  Her published research includes the use of open source in cybersecurity and open source ecosystems in the public sector.</p>
    <p>Deb serves on several non-profit boards where open source is a critical element of their mission in the public interest.  She recently accepted a third appointment as Board Member for Open Source Initiative (OSI), serves on the advisory boards of the Open Source Elections Technology Foundation, DemocracyLab, and OASIS Open Project.  You can follow her at <a href="https://www.twitter.com/debbryant">@debbryant</a>.</p>
  </div>
</div>