<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 */
?>

<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Matthias_Sohn.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Matthias is a product owner and open source enthusiast. He leads the EGit and JGit projects and contributes to Gerrit Code Review.
He also worked on Spark and Kafka and recently started contributing to Gardener (<a href="https://github.com/gardener">https://github.com/gardener</a>) enabling Kubernetes to manage Kubernetes clusters on a large scale. He is also a member of the Eclipse Architecture Council. Matthias works as a product owner for SAP Cloud Platform and leads its git team.</p>
<p>He holds a PhD and diploma degree in Experimental Physics from Karlsruhe Institute of Technology, Germany.</p>
</div>
</div>