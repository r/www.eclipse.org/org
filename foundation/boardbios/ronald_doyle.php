<div class="row">
  <div class="col-sm-4 col-xs-24">
    <img class="img-responsive" src="boardbios/photos/rdoyle.jpg" align="left"
      style="padding-right: 5px;" width="120"
    >
  </div>
  <div class="col-sm-20">Ron is a division-wide lead architect for enterprise and cloud application
    design/delivery, focusing on public SaaS, hybrid cloud and on-premise software products. Before
    the Broadcom acquisition of CA Technologies, Ron was Vice President of Continuous Delivery
    Portfolio Architecture. He drove cross-product integration and product delivery models for
    products in modeling, functional and performance testing, service virtualization and
    orchestration. Previously, Ron was a Distinguished Engineer for DevOps transformation across the
    IBM cloud development organization, focusing on both strategy and hands-on initiatives to
    address the organization, tools and methodologies needed to enable world-class service delivery.
    Ron’s interests include distributed systems and managed resource sharing, focusing on
    virtualization and cloud computing. He led the IBM involvement in the Open Virtualization Format
    (OVF) standard in the DMTF. He spends time working on university accreditation for computer
    science programs. He has been a program evaluator and team chair for years and is on the ABET
    Computing Accreditation Commission Executive Committee and is a Representative Director for the
    CSAB Board of Directors.</div>
</div>