<div class="row">
    <div class="col-sm-4 col-xs-24">
        <img 
            class="img-responsive" 
            src="boardbios/photos/andreas-nauerz.jpg" align="left" 
            style="padding-right: 5px;" 
            width="120" 
        />
    </div>
    <div class="col-sm-20">
        <p>Andreas has studied Computer Science and Law and holds a PhD in the former.</p>
        <p>Today, he is a Member of the Board and CTO & EVP of Bosch Digital, which is Bosch's internal IT unit supporting the digitization of all of its business units with technical and digital services. In this role, he is responsible for Products and Innovations. Prior to his, he was first the VP and later the CEO & CTO of Bosch.IO, where he was leading a global interdisciplinary expert organization driving Bosch's (A)IoT strategy. In this role, he was responsible for the development of the Bosch's IoT Suite, forming the basis on which Bosch and its customers build IoT solutions incorporating Bosch's cross-domain industry know-how and connecting millions of sensors & devices. Andreas joined Bosch in 2019 at Corporate Research.</p>
        <p>Before having joined Bosch, Andreas has been working for IBM's Cloud division, where he was a Program Director (OM) & Senior Technical Staff Member (STSM), primarily for IBM's serverless solution IBM Cloud Functions, where he was responsible for managing the product. He was one of the key faces behind Apache OpenWhisk & IBM Cloud Functions.</p>
        <p>Overall, Andreas has hold various lead positions in the field of software architecture & development and acted as the head of multiple research groups. He is a recognized software architect and one of the experts in the fields of cloud & serverless computing as well as AI, ML, and IoT.</p>
    </div>
</div>
