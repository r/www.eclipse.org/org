<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Gunnar_Wagenknecht.png" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Gunnar Wagenknecht is a software specialist at Salesforce. In his past career he lead Open Source programs, managed engineering teams, worked on great products, and architected and managed the operation of a distributed, multi-tenant SaaS platform serving millions of transactions each day.</p> 
<p>He also is a prolific contributor with many years of experience in the Eclipse Open Source Community where he has been awarded with the Lifetime Contribution Award.</p>
<p>Apart from software and computers he loves chatting about travel, fishing, DIY projects and other things that matters.</p></div>
</div>