<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Steffen_Evers.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Steffen Evers is director open source at Bosch.IO, where he leads the team that provides development services for open source software essential to the company and consults on strategy, community work, software management, and compliance processes in the area of OSS. For nearly 20 years, Steffen has researched, taught, and promoted open source development and supported various companies in the use of OSS to achieve their business objectives.</p> </div>
</div>