<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Max_Rydahl_Andersen.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">

Max Rydahl Andersen is leading the development of JBoss Tools and Red Hat JBoss Developer Studio and have several years working with Eclipse from both outside and inside of eclipse.org. His main interests for Eclipse.org is to keep the Eclipse IDE a viable platform for building desktop based tooling while also getting involved in offering a complementary cloud and web-based tooling offering.
</div>
</div>