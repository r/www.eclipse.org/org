<div class="row">
  <div class="col-sm-4 col-xs-24">
    <img 
      class="img-responsive" 
      src="boardbios/photos/hendrik-ebbers.jpg" 
      align="left" 
      style="padding-right: 5px;" 
      width="120"
    />
  </div>
  <div class="col-sm-20">
    Hendrik Ebbers is a Java Champion, a member of JSR expert groups, and JavaOne Rockstar. He is 
    the founder and leader of the Java User Group Dortmund and gives talks and presentations in 
    user groups and at conferences worldwide. Hendrik is a member of the Jakarta WG and the 
    Adoptium WG. In 2022 Hendrik founded OpenElements to positively impact open source software and 
    the open source community. The main goal of OpenElements is to strengthen open collaboration. 
    For Hendrik, this includes helping companies to use and introduce more OSS by supporting them 
    to set up an open source programming office (OSPO) or providing custom consulting for open 
    source projects like Eclipse Adoptium. Next to Eclipse-related projects, Hendrik contributes to 
    other OSS. He is a core committer of the <a href="https://hedera.com">Hedera Hashgraph</a>, the 
    only open source public ledger written in Java. Here, Hendrik helps Hedera and <a href="https://swirldslabs.com">SwirldsLabs</a> 
    create secure, fast, and reliable components using open source standards, components, and 
    well-known workflows.
  </div>
</div>