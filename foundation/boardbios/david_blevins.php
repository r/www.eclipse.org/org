<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 */
?>

<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/David_Blevins.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Founder &amp; Chief Executive Officer</p>
<p>Prior to founding Tomitribe, David’s extensive experience creating meaningful relationships between business and Open Source includes 7 years at IBM rebranding Apache Geronimo as WebSphere CE, technical leadership in Gluecode (acquired by IBM), and a key role in Apple’s integration and distribution of OpenEJB in WebObjects.</p>
<p>David is a co-founder to OpenEJB (1999), Apache Geronimo (2003) and Apache TomEE (2011), 10-year member of the JCP serving in Java EE, EJB, CDI, JMS and Java EE Security JSRs, JavaOne RockStar for 2012 & 2013, 2015 inductee into the Java Champions and nominated for JCP Member of the Year 2015. He is a contributing author to Component-Based Software Engineering: “Putting the Pieces Together,” from Addison Wesley and a regular speaker at JavaOne, Devoxx, ApacheCon, OSCon, JAX and Java-focused conferences.</p></div>
</div>