<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Tracy_Miranda.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Tracy Miranda is a developer, open source evangelist and veteran of the Eclipse community. She is founder of Kichwa Coders, a consultancy specialising in Eclipse tools for embedded and scientific software. She is also Chair for the Eclipse Science working group. Tracy has a background in electronics system design. She writes for <a href="http://opensouce.com/">opensouce.com</a> and <a href="http://jaxenter.com/">jaxenter.com</a> on tech, open source & diversity.</p></div>
</div>