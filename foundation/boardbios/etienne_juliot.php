<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Etienne_Juliot.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Etienne Juliot is co-founder and vice president of Obeo, a software editor focused on Model Driven solutions. 
He is a senior software architect with a strong experience in IT and embedded systems architecture. He is involved in several Eclipse projects 
(<a href="https://www.eclipse.org/sirius">Sirius</a>, <a href="https://www.eclipse.org/acceleo">Acceleo</a>, <a href="https://www.eclipse.org/emf/compare">EMF Compare</a>, 
<a href="https://www.eclipse.org/sca">SCA</a>, <a href="https://www.eclipse.org/umlgen">UML Generators</a>, <a href="https://www.eclipse.org/intent">Mylyn</a>, ...) and is a steering committee member 
for <a href="http://polarsys.org/">PolarSys</a>.</p>
<p>Finally, Etienne manages international and strategic collaborations with large companies on system engineering and enterprise architecture needs.
Follow <a target="_blank" href="https://twitter.com/ejuliot">@ejuliot on Twitter</a>.</p></div>
</div>