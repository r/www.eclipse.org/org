<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Hans-Christian_Brockmann.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
Chris Brockmann was educated in  Germany, the United Kingdom and the USA. He 
studied business administration in G&ouml;ttingen (Germany) and 
Winston Salem (USA) where he earned an MBA degree. He spent 10 years 
with Baan Company's Solutions Center Automotiv, where he 
built a strong background in production and logistics 
planning. Later he was in charge of automotive retail solution 
projects in Europe and Asia. He founded brox IT-Solutions GmbH in 
1998 with the vision to help businesses make the most of the 
massive amounts of available information. In 2008 brox and 
partners proposed to the SMILA (previously EILF) project to become the 
new open-architecture standard for handling unstructured information.  </div>
</div>