<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Cedric_Carbone.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
C&eacute;dric Carbone is Talend's Chief Technical Officer and Board Member of OW2 consortium (since the inception). Prior to 
joining Talend in 2006, he managed the Java practice at Neurones, a leading systems integrator in Europe. C&eacute;dric 
has also lectured at several universities on technical topics such as XML or Web Services. He holds a master's degree in 
Computer Science and an advanced degree in Document Engineering.</div>
</div>