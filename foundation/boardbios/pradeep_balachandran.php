<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Pradeep_Balachandran.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">

Pradeep is Program Director for Eclipse Strategy and Development in IBM. Prior to this he was 
leading the Rational Software Development organization in IBM India. He has been in IBM Software 
division since 1995 via Rational Software acquisition and has an overall industry experience of 
23 years in Software Engineering Tools development. He setup the first Eclipse open source 
development lab east of Europe, in Bangalore in 2008. In the course his career at Rational and IBM, 
he has built and led various product development teams, including products in the IBM Rational 
Collaborative Life-cycle Management (CLM) solution built on IBM Rational Jazz platform. He is a 
regular speaker in Conferences on Agile, DevOps, Open Source and Software Engineering. He is also 
actively involved in organizing various conferences as a member of the program committee. 
Pradeep has a Masters in Computer Science & Engineering. 

</div>
</div>