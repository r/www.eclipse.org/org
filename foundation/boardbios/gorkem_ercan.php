<div class="row">
  <div class="col-sm-4 col-xs-24">
    <img class="img-responsive" src="boardbios/photos/gorkem_ercan.jpg" align="left" style="padding-right: 5px;" width="120">
  </div>
  <div class="col-sm-20">
    Gorkem Ercan is the architect for developer tools at Red Hat. Gorkem has experience
    working with a diverse range of technologies ranging from building IDEs to mobile phones.
    He is an avid contributor and supporter of open source. He is the project lead for Eclipse Thym project,
    and Eclipse JDT language server and a committer on various projects including Eclipse Che, Eclipse WTP Server,
    Eclipse JSDT, and Eclipse Theia. Gorkem is a member of the Eclipse Architecture Council and he is one of the
    longest serving Eclipse commiters.
</div>
</div>