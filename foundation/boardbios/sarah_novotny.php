<div class="row">
  <div class="col-sm-4 col-xs-24">
    <img class="img-responsive" src="boardbios/photos/sarah-novotny.webp" align="left" style="padding-right: 5px;" width="120">
  </div>
  <div class="col-sm-20">
    <p>
      Sarah Novotny has long been an Open Source champion in projects such as
      Kubernetes, NGINX and MySQL. She is part of the Microsoft Azure Office of
      the CTO, sits on the Linux Foundation Board of Directors, previously led an
      Open Source Strategy group at Google and ran large scale technology
      infrastructures before web-scale had a name.
    </p>
  </div>
</div>
