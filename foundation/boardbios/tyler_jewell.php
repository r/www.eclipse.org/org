<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/tyler_jewell.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">

<p>Tyler is founder and CEO of <a target="_blank" href="https://codenvy.com/">Codenvy</a> and 
a partner with Toba Capital managing investments in middleware 
and application development.  He sits on the boards of WSO2, Sauce Labs, ShiftMobility, and eXo Platform.  
He has investments in ZeroTurnaround, InfoQ, Cloudant, SourceGraph, and AppHarbor.  He is the author of three 
books on Java, including OReilly's Java Web Services and was BEA's chief evangelist starting in 1999.</p>
</div>
</div>