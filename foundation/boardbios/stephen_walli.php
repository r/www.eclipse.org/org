<div class="row">
    <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Stephen_Walli.jpg" align="left" style="padding-right: 5px;" width="120"></div>
    <div class="col-sm-20">
        Stephen R. Walli, Principal Program Manager, Azure Office of the CTO<br><br>
        Stephen is a principal program manager working in the Azure team at Microsoft. Prior to that he was a
        Distinguished Technologist at Hewlett Packard Enterprise. Stephen has been a technical executive, a founder, a
        writer, a systems developer, a software construction geek, and a standards diplomat. He has worked in the IT
        industry since 1980 as both customer and vendor, working with open source for 25+ years. He blogs about open
        source and software business at "Once More unto the Breach", opensource.com, and on Medium.</div>
</div>