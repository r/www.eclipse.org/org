<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/matthew-khouzam.jpg" alt="Matthew Khouzam's photo" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
    <p>
      Matthew Khouzam has been using Eclipse tools since the Callisto release and has been a committer on Eclipse projects for 10 years, with almost 5,000 reviews to his name. He is the product manager for Eclipse Theia, OpenVSX, Eclipse Trace Compass within Ericsson and co-lead for the Eclipse Trace Compass Incubator. Matthew is also co-lead of the CDT.cloud project. He is a technology enthusiast, especially in the field of performance engineering. In the copious amounts of free time he has left, he is a husband, a father, and enjoys cooking, biking and woodworking. He is also a budding doodler and has been co-developing a game in his remaining spare time.
    </p>
    <p>
      Matthew has worked in academia and industry, coordinating between large and small companies as well as university research labs.
    </p>
    <p>
      Finally, he is a great big nerd. He will gladly broach any subject if the other side brings enthusiasm.
    </p>
  </div>
</div>