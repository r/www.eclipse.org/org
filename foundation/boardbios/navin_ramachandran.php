<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Navin_Ramachandran.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
    <p>Navin Ramachandran is a member of the Board of Directors at iota.org, where he leads
      Engineering Strategy.</p>
    <p>Navin is a medical doctor, with interests in data research, healthcare IT and distributed
      ledgers. He is the steering committee chair of the Tangle EE working group and on the advisory
      board of the UK All Party Parliamentary group for blockchain.</p>
  </div>
</div>