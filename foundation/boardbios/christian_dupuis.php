<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Christian_Dupuis.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
Over the past several years Christian has been an active user of Eclipse technologies and 
used the Eclipse platform to build open source and commercial solutions like Spring IDE or the Spring
Tool Suite. During his work on open source technologies, he has come to recognize the unique 
value that the Eclipse Foundation brings to the intersection of open source and corporate software development. 
In conjunction with a large, active and vocal community, the Eclipse Foundation had helped to define the primacy 
of the developer as an agent of change in the world of enterprise software.</div>
</div>