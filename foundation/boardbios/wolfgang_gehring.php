<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/wolfgang-gehring.jpg" align="left" style="padding-right: 5px;" alt="Wolfgang Gehring's photo" width="120"></div>
  <div class="col-sm-20">
    <p>Dr. Wolfgang Gehring is an Ambassador for Open and Inner Source and has been working on enabling and spreading the idea within the Mercedes-Benz Group AG and its IT subsidiary Mercedes-Benz Tech Innovation (MBTI). A software engineer by trade, Wolfgang&rsquo;s goal is to help enable Mercedes-Benz to fully embrace FOSS and become a true Open Source company. He has a passion for communities, leads MBTI&rsquo;s Open Source Program Office, and is a member of the Mercedes-Benz FOSS Center of Competence.</p>
    <p>In his free time, Wolfgang likes to engage in conversations about soccer and is a passionate traveler and scuba diver. He calls Albert Einstein's birth city of Ulm his home in Southern Germany.</p>  </div>
</div>