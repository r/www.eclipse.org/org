<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Stefan_Ferber.jpg" align="left" style="padding-right: 5px;" width="120"></div>

<div class="col-sm-20">
<p>Dr. Stefan Ferber has been Chairman of the Executive Board of Bosch Software Innovations GmbH
since January 1, 2018. He has direct management responsibility for product development,
business development, sales &amp; marketing as well as HR, finance &amp; controlling. Stefan currently
represents Bosch on the board of the Eclipse Foundation and is a member of the European
Internet of Things Council. He has more than 25 years’ experience in software development,
software processes, software product lines, and software architectures for embedded systems,
computer vision, and IT domains. Stefan holds an undergraduate degree and a Ph.D. in computer
science from the University of Karlsruhe, Germany and an M.Sc. in computer science from the
University of Massachusetts Dartmouth, USA. He is a certified ATAM lead evaluator by the
Software Engineering Institute of Carnegie Mellon University, Pittsburgh, USA.</p>
</div>
</div>