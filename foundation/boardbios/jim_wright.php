<div class="row">
    <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/jim_wright.jpg" alt="Jim Wright's photo" align="left" style="padding-right: 5px;" width="120"></div>
    <div class="col-sm-20">
        <p>
            James (Jim) Wright is a well-known software architect and attorney who has been working in and around open source for 20 years. Jim is the author of the Universal Permissive License and has been with Oracle since 2009, where he currently serves as Chief Architect, Open Source Policy, Strategy, Compliance, and Alliances.
        </p>
        <p>
            Before his time at Oracle, Jim was in private practice at Sidley Austin, where he handled a wide variety of technology transactional matters as well as patent litigation. Jim is a graduate of Harvard Law School, and in the decade before starting legal practice, he worked in engineering and management capacities at a variety of different IT enterprises.
        </p>
    </div>
</div>