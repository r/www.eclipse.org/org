<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/angelo-corsaro.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
    <p>
      Angelo Corsaro, Ph.D. is Chief Executive Officer (CEO) and Chief
      Technology Officer (CTO) at ZettaScale Technology.
    </p>
    <p>
      At ZettaScale he is working with a world-class team to change the
      landscape of distributed computing. Angelo and the ZettaScale team are
      working hard to bring to every connected human and machine the
      unconstrained freedom to communicate, compute, and store — anywhere, at
      any scale, efficiently and securely.
    </p>
    <p>
      Angelo has been an active member and contributor in the Open Source
      community for three decades! He was the single-handed author of the first
      open source real-time Java compiler jRate. He has contributed the
      monad/functors Apero library to OCaml and implemented the real-time
      extensions to the TAO ORB in early 2000. Angelo single-handedly wrote
      SimD, an innovative OMG DDS C++ API that eventually was adopted as the
      new standard DDS mapping for C++. He has contributed a Scala API for DDS
      and a Scala library of distributed algorithms called Dada. In recent
      years, Angelo started, and still leads, the Eclipse Zenoh project. He
      co-wrote the first implementation in OCaml and heavily contributed to the
      early days of the Rust rewrite.
    </p>
    <p>
      In addition, to the Open Source contributions, Angelo is recognised as a
      world's top expert in edge/fog computing and a well-known researcher in
      the area of high-performance and large-scale distributed systems. Angelo
      has over 100 publications in referred journals, conferences, workshops,
      and magazines. Angelo has co-authored over ten international standards.
    </p>
 </div>
</div>
