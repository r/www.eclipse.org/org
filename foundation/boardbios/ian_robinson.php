<div class="row">
    <div class="col-sm-4 col-xs-24">
        <img 
            class="img-responsive" 
            src="boardbios/photos/ian-robinson.jpg" align="left" 
            style="padding-right: 5px;" 
            width="120" 
        />
    </div>
    <div class="col-sm-20">
        <p>
            Dr. Ian Robinson is an IBM Distinguished Engineer and CTO of IBM Application Platform, 
            which includes IBM&rsquo;s Java, Liberty and WebSphere technologies. He has more than 30 years&rsquo; 
            experience working in distributed enterprise computing across product development, open standards, 
            and open source. Ian is a steering committee member of the Jakarta EE WG and has IBM team members 
            participating in numerous Eclipse runtime and tools projects including MicroProfile, Adoptium, 
            OpenJ9, Equinox, OSGi and Eclipse JDT.
        </p>
    </div>
</div>