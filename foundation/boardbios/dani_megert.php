<div class="row">
  <div class="col-sm-4 col-xs-24">
    <img class="img-responsive" src="boardbios/photos/daniel_megert.jpg"
      align="left" style="padding-right: 5px;" width="120"
    >
  </div>
  <div class="col-sm-20">Dani is one of the initial Eclipse committers.
    Currently he's a leader of the <a href="https://web.archive.org/web/20170427003027/http://wiki.eclipse.org:80/Platform">Platform</a> and the <a href="https://web.archive.org/web/20170324111504/http://www.eclipse.org:80/jdt/">JDT</a> sub-projects, represents
    the project in the Eclipse <a href="https://web.archive.org/web/20170505234514/http://wiki.eclipse.org:80/Planning_Council">Planning Council</a>, and is also a a member of the
    Eclipse <a href="https://web.archive.org/web/20170516234550/http://wiki.eclipse.org:80/Architecture_Council">Architecture Council</a>. Dani worked at OTI and now works at IBM
    Research GmbH. His interests include user interface and API design, editors,
    software quality and performance.</div>
</div>