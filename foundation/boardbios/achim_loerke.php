<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Achim_Loerke.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
Achim is a software developer and architect at heart. As a managing director at BREDEX GmbH he is responsible
for technology and development strategies. In addition he is the project lead of the Eclipse Jubula project and
represents his company in the Eclipse foundation. </div>
</div>