<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Pat_Huff.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">

Pat Huff is a Program Director in IBM Rational, responsible for Open Source and Eclipse. Pat manages an Open Source Center of Competency and the relationship between the IBM product development community and  Eclipse. Pat has a long history of producing software development tools and frameworks, and in introducing open source and agile programming concepts into corporate infrastructures. His current interests lie in realizing Open Source's potential in commercially successful offerings. In addition to his board duties, Pat is the Chair of the Eclipse IP Advisory Committee and the co-chair of the Eclipse Long term Support Industry Working Group.

</div>
</div>