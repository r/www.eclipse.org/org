<div class="row">
    <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/tim-deboer.jpg" align="left" style="padding-right: 5px;" width="120"></div>
    <div class="col-sm-20">
        <p>
            Tim deBoer is a senior developer experience architect, currently working across several teams including Podman Desktop, Dev Spaces, and Red Hat's IDE extensions.
            Tim has over 25 years of experience building developer tools from IDEs to runtime CLIs, and is passionate about improving developer's lives through useful and usable tools.
        </p>
    </div>
</div>