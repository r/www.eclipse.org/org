<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/adam_gibson.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">Adam is the CTO of Konduit. Before this, Adam was the cofounder of Skymind.
    Adam has been using open and producing open source software since 2010 and has been developing
    machine learning systems since 2012. Adam is a published author and speaker on the field of deep
    learning on topics ranging from deployment of Production Machine Learning Systems to NLP. Adam
    grew up in Michigan in the US, spent a few years in Silicon Valley and now resides in Tokyo,
    Japan.</div>
</div>