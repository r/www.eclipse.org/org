<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 */
?>

<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Kenji_Kazumura.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Kenji Kazumura is Senior Professional Engineer at Fujitsu Limited in Japan.
After more than five years experience of the compiler development
he has been working with Java for the enterprise mission critical systems.
Now he is in charge of Fujitsu middleware products such as Interstage Application Server.</p>
<p>He has a lot of experience to optimize the several huge enterprise applications,
specifically in the field of mobile communication and financial service,
in order to run with high performance and reliability.
These systems contribute a great deal to the social infrastructures in Japan.
He is also now working to make the cloud environment be more useful
with the container and microservices architecture technologies.</p>
</div>
</div>