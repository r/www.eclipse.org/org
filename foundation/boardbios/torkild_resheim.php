<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Torkild_Resheim.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Torkild Ulvøy Resheim is a senior software engineer and part owner of Itema, a software consultancy based in Trondheim, Norway. As a contractor and consultant, he has helped companies use a wide range of Eclipse technologies for more than a decade. Most of this time has been spent on solutions for the semiconductor and marine/offshore sectors. Torkild founded the Trondheim Eclipse User Group in 2013 and has been participating in EclipseCon Europe Program Committee for the past few years. He sits on the Eclipse Architecture Council and on the Eclipse Science PMC.</p></div>
</div>