<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/Kevin_Sutter.jpg" alt="Kevin Sutter's avatar" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">
<p>Kevin is co-lead of both the Jakarta EE Platform project and the MicroProfile project at the Eclipse Foundation.  His involvement with these projects started during the initial investigative phases and continues with all aspects of Code and Specification development – project committer, PMC member, Steering Committee member, and Specification Committee member.</p>
<p>Kevin is co-lead of both the Jakarta EE Platform project and the MicroProfile project at the Eclipse Foundation.  His involvement with these projects started during the initial investigative phases and continues with all aspects of Code and Specification development – project committer, PMC member, Steering Committee member, and Specification Committee member.</p>
</div>
</div>