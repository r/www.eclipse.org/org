<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Denis Roy (Eclipse Foundation)
 * Eric Poirier (Eclipse Foundation)
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>Presented below is the list of Technology Awards judges for the 2010 Eclipse Community Awards.</p>
  <h3>Best Commercial and Open Source Developer Tool Awards</h3>
  <ul>
    <li>Don Dingee - Embedded Computing Design</li>
    <li>Sven Efftinge - itemis &amp; Textual Modeling Framework (TMF) Project</li>
    <li>Daniel Ford - IBM &amp; Spatiotemporal Epidemiological Modeler (STEM) Project</li>
    <li>Jeffrey Hammond - Forrester Research</li>
    <li>Markus Knauer - EclipseSource &amp; g-Eclipse and Packaging Projects</li>
    <li>Daniel Megert - IBM &amp; Java Development Tools (JDT) Project</li>
    <li>Greg Watson - IBM &amp; Parallel Tools Platform (PTP) Project</li>
  </ul>
  <h3>Best EclipseRT Award</h3>
  <ul>
    <li>Nick Boldt - Red Hat &amp; Modeling and Athena Projects</li>
    <li>Michael Cot&eacute; - RedMonk</li>
    <li>Shaun Smith - Oracle &amp; Dali JPA Tools Project

    <li>Alan Zeichick - BZ Media</li>
  </ul>
  <h3>Best RCP Award</h3>
  <ul>
    <li>Chris Aniszczyk - EclipseSource &amp; Plugin Development Environment (PDE) Project</li>
    <li>Nick Boldt - Red Hat &amp; Modeling and Athena Projects</li>
    <li>Michael Cot&eacute; - RedMonk</li>
    <li>Wenfeng Li - Actuate &amp; Business Intelligence and Reporting Tools (BIRT) Project</li>
    <li>Shaun Smith - Oracle &amp; Dali JPA Tools Project

    <li>Alan Zeichick - BZ Media</li>
  </ul>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="winners10.php">2010 Winners</a></li>
    </ul>
  </div>
</div>