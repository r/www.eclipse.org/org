<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "Eclipse Community Awards Judges 2010";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse, foundation, community, award, winners, 2010, judges");

ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();