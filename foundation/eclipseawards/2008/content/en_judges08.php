<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Denis Roy (Eclipse Foundation)
 * Eric Poirier (Eclipse Foundation)
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>Presented below is the list of Technology Awards judges for the 2008 Eclipse Community Awards.</p>
  <div class="homeitem3col">
    <h3>Commercial Awards</h3>
    <ul>
      <li>Ryan Brooks - Boeing</li>
      <li>Michael Cot&eacute; - RedMonk</li>
      <li>Don Dingee - Embedded Computing Design</li>
      <li>Don Dunne - Boeing</li>
      <li>Jeffrey Hammond - Forrester Research</li>
      <li>Alan Zeichick - BZ Media</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Open Source Awards</h3>
    <ul>
      <li>Doug Clarke - EclipseLink Project</li>
      <li>Kenn Hussey - Modeling Project</li>
      <li>Mik Kersten - Mylyn Project</li>
      <li>Markus Knauer - g-Eclipse &amp; Packaging Projects</li>
      <li>Christian Kurzke - Device Software Devlopment Platform (DSDP) Project</li>
      <li>Jeff McAffer - Equinox &amp; Plugin Development Environment (PDE) Projects</li>
      <li>Greg Watson - Parallel Tools Platform (PTP) Project</li>
      <li>David Williams - Web Tools Project</li>
    </ul>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="../winners08.php">Winners &amp; Finalists</a></li>
    </ul>
  </div>
</div>