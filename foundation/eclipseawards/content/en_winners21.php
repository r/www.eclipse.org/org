<?php

/**
 * Copyright (c) 2005, 2018, 2022 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *   Zhou Fang (Eclipse Foundation)
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>Presented below is the list of award winners and finalists for each of the project awards
    catagories as determined by votes from the community.</p>
  <h2>Individual Award Finalists</h2>
  <h3>Top Committer</h3>
  <ul class="fa-ul">
    <li><i class="fa-li fa fa-star orange"></i> Vincent Fugnitto (winner)</li>
    <li>Michael Behrisch</li>
    <li>Harald Mackamul</li>
    <li>Fabrice Tiercelin</li>
    <li>Jakob Erdmann</li>
    <li>Erik Boasson</li>
    <li>Ryan St. James</li>
  </ul>
  <p>
    <a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/28">Full list of nominees</a>
  </p>

  <h3>Top Contributor</h3>
  <ul class="fa-ul">
    <li><i class="fa-li fa fa-star orange"></i> Ed Merks (winner)</li>
    <li>Jonah Graham</li>
    <li>Tetiana Fydorenchyk</li>
  </ul>
  <p>
    <a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/29">Full list of nominees</a>
  </p>

  <h3>Top Newcomer Evangelist</h3>
  <ul class="fa-ul">
    <li><i class="fa-li fa fa-star orange"></i> Andreas Riexinger (co-winner)</li>
    <li><i class="fa-li fa fa-star orange"></i> Jonas Helming (co-winner)</li>
    <li>Lars Vogel</li>
    <li>Wim Jongman</li>
    <li>Emerson Castaneda S.</li>
  </ul>
  <p>
    <a href="https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/30">Full list of nominees</a>
  </p>
</div>

<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
      <li><a target="_blank" href="https://www.flickr.com/photos/108559379@N08/25655295096/in/album-72157665437566641/">Ceremony Photos</a></li>
    </ul>
  </div>
</div>