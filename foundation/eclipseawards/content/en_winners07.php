<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award finalists and winners for each of the
    individual awards catagories.
  </p>
  <div class="homeitem3col">
    <h3>Top Ambassador</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Chris Aniszczyk (Winner)</li>
      <li class="finalist">Alex Blewitt</li>
      <li class="finalist">Doug Schaefer</li>
      <br>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Contributor</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Eugene Kuleshov</li>
      <li class="finalist">Philippe Ombredanne</li>
      <li><i class="fa-li fa fa-star orange"></i>Kimberley Peter (Winner - tie)</li>
      <li><i class="fa-li fa fa-star orange"></i>Tom Schindl (Winner - tie)</li>
      <br>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Mik Kersten</li>
      <li><i class="fa-li fa fa-star orange"></i>Ed Merks (Winner)</li>
      <li class="finalist">Thomas Watson</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Hendrik Maryns</li>
      <li><i class="fa-li fa fa-star orange"></i>Daniel Megert (Winner)</li>
      <li class="finalist">Eric Rizzo</li>
    </ul>
  </div>
  <h2>Technology Award Winners</h2>
  <p>Presented below is the list of award finalists and winners for each of the
    technology awards catagories as determined by this year's judging panels.
  </p>
  <div class="homeitem3col">
    <h3>Best Open Source RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://eclipsetrader.sourceforge.net">EclipseTrader</a>
      </li>
      <li>
        <i class="fa-li fa fa-star orange"></i>
        <a
          href="http://portal.chronos.org/psicat-site/">
          PSICAT (Winner)
      </li>
      <li class="finalist"><a href="http://www.rssowl.org/">RSSOwl</a></li>
      <br>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Commercial RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.tibco.com/devnet/business_studio/product_resources.jsp?tab=downloads">TIBCO
        Business Studio</a> (Winner)
      </li>
      <li class="finalist"><a
        href="http://www.ivis.com/public/products/xprocess/index.cfm">Ivis
        Technologies xProcess</a>
      </li>
      <br>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Open Source Eclipse Based Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.eclemma.org/">EclEmma</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://eclipse-cs.sourceforge.net">eclipse-cs Checkstyle Plugin</a>
        (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Commerical Eclipse Based Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a
        href="http://www.adobe.com/products/flex/productinfo/overview/">Adobe
        Flex 2.0</a>
      </li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.qnx.com/products/development">QNX Momentics IDE</a>
        (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Deployment of Eclipse Technology in an Enterprise</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.bardusch.de">Bardusch GmbH</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.jpmchase.com">JPMorgan Chase</a> (Winner)</li>
      <li class="finalist"><a href="http://www.jpl.nasa.gov/">NASA Ensemble Team</a></li>
    </ul>
  </div>
</div>
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>