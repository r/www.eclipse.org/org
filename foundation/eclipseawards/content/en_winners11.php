<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    individual awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Lifetime Achievement Award</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>David Williams (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Lauren Goubet</li>
      <li class="finalist">Tom Schindl</li>
      <li><i class="fa-li fa fa-star orange"></i>Sebastian Zarnekow (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Contributor</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">James Sugrue</li>
      <li class="finalist">Brian de Alwis</li>
      <li><i class="fa-li fa fa-star orange"></i>Dariusz Luksza (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Cedric Brun</li>
      <li class="finalist">James Sugrue</li>
      <li><i class="fa-li fa fa-star orange"></i>Boris Bokowski (Winner)</li>
    </ul>
  </div>
  <h2>Project Award Winners
  </h2>
  <p>Presented below is the list of award winners and finalists for each of the
    project awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Most Innovative New Feature or Eclipse Project</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Mylyn Builds</li>
      <li class="finalist">Extended Editing Framework (EEF)</li>
      <li><i class="fa-li fa fa-star orange"></i>EGit (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Most Open Project</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Modeling Amalgamation</li>
      <li><i class="fa-li fa fa-star orange"></i>e4 (Winner)</li>
    </ul>
  </div>
  <h2>Technology Award Winners
  </h2>
  <p>Presented below is the list of award winners and finalists for each of the
    technology awards catagories as determined by this year's judging panels.
  </p>
  <div class="homeitem3col">
    <h3>Best Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://diver.sf.net/" target="_blank">Diver</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a href="http://pydev.org/"
        target="_blank">PyDev</a> (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Modeling Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.actifsource.com/" target="_blank">actifsource</a></li>
      <li class="finalist"><a href="http://www.obeodesigner.com/"
        target="_blank">Obeo Designer</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.bonitasoft.com/" target="_blank">Bonita Open Solutions</a>
        (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Mobile Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://genuitec.com/mobile" target="_blank">MobiOne
        Studio</a>
      </li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://developer.motorola.com/docstools/motodevstudio/"
        target="_blank">MOTODEV Studio for Android</a> (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.justinmind.com/" target="_blank">Justinmind
        Prototyper</a>
      </li>
      <li class="finalist"><a href="http://wireframesketcher.com/"
        target="_blank">WireframeSketcher</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.openchrom.net/" target="_blank">OpenChrom</a> (Winner)</li>
    </ul>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>