<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle ?></h1>
  <div class="homeitem3col">
    <h2>2013</h2>
    <table class="table">
      <tr>
        <td><a target="_blank" href="http://chrononsystems.com/chronon4"><img
          src="images/technology/chronon.jpg" width="60"></a></td>
        <td><b>Chronon 4 'Ops'</b><br>Best Application 2013<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://marketplace.eclipse.org/content/findbugs-eclipse-plugin#.UpTZyNLkuy0"><img
          src="images/technology/buggy-sm.png" width="60"></a></td>
        <td><b>FindBugs</b><br>Best Developer Plugin 2013<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="http://www.obeodesigner.com/"><img
          src="images/technology/logood.png" width="60"></a></td>
        <td><b>Obeo Designer</b><br>Best Modeling Product 2013<br>
          <br>
        </td>
        <td><a target="_blank" href="http://wireframesketcher.com/"><img
          src="images/technology/sketch_66.png" width="60"></a></td>
        <td><b>Wireframe Sketcher</b><br>Best Developer Tool 2013<br>
          <br>
        </td>
      </tr>
    </table>
    <br />
    <h2>2012</h2>
    <table class="table">
      <tr>
        <td><a target="_blank"
          href="http://chrononsystems.com/products/chronon-time-travelling-debugger"><img
          src="images/technology/chronon.jpg" width="60"></a></td>
        <td><b>Chronon Time Travelling Debugger</b><br>Best Developer Tool 2012<br>
          <br>
        </td>
        <td><a target="_blank" href="http://www.justinmind.com/"><img
          src="images/technology/justinmind.jpg" width="60"></a></td>
        <td><b>Justinmind Prototyper</b><br>Best Application 2012<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="http://maintainj.com/"><img
          src="images/technology/maintainj.jpg" width="60"></a></td>
        <td><b>MaintainJ</b><br>Best Modeling Tool 2012<br>
          <br>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <br />
    <h2>2011</h2>
    <table class="table">
      <tr>
        <td><a target="_blank" href="http://www.bonitasoft.com/"><img
          src="images/technology/bonita.jpg" width="60"></a></td>
        <td><b>Bonita Open Solutions</b><br>Best Modeling Tool 2011<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://www.sundoginteractive.com/sunblog/posts/motodev-studio-for-android"<img
            src="images/technology/motodev.jpg" width="60"></a>
        </td>
        <td><b>MOTODEV Studio for Android</b><br>Best Mobile Tool 2011<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="https://www.openchrom.net/"><img
          src="images/technology/openchrom.jpg" width="60"></a></td>
        <td><b>OpenChrom</b><br>Best RCP Application 2011<br>
          <br>
        </td>
        <td><a target="_blank" href="http://pydev.org/"><img
          src="images/technology/pydev.jpg" width="60"></a></td>
        <td><b>PyDev</b><br>Best Developer Tool 2011<br>
          <br>
        </td>
      </tr>
    </table>
    <br />
    <h2>2010</h2>
    <table class="table">
      <tr>
        <td><a target="_blank"
          href="http://www.bredex.de/index.php/guidancer_jubula_en.html"><img
          src="images/technology/BredexGUIdancer.png" width="60"></a></td>
        <td><b>Bredex GUIdancer</b><br>Best Commercial Developer Tool 2010<br>
          <br>
        </td>
        <td><a target="_blank" href="http://groovy.codehaus.org/Eclipse+Plugin"><img
          src="images/technology/Groovy.png" width="60"></a></td>
        <td><b>Groovy-Eclipse</b><br>Best Open Source Developer Tool 2010<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank"
          href="http://static.springsource.com/projects/dm-server/1.0.x/user-guide/html/"><img
          src="images/technology/SpringSource.jpeg" width="60"></a></td>
        <td><b>SpringSource dm Server</b><br>Best EclipseRT Application 2010<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://marketplace.eclipse.org/content/tasktop-pro"><img
          src="images/technology/Tasktop.jpeg" width="60"></a></td>
        <td><b>Tasktop Pro</b><br>Best RCP Application 2010<br>
          <br>
        </td>
      </tr>
    </table>
    <br />
    <h2>2009</h2>
    <table class="table">
      <tr>
        <td><a target="_blank" href="http://www.eclipse.org/acceleo/"><img
          src="images/technology/Acceleo.jpg" width="60"></a></td>
        <td><b>Acceleo</b><br>Best Open Source Developer Tool 2009<br>
          <br>
        </td>
        <td><a target="_blank" href="http://directory.apache.org/studio/"><img
          src="images/technology/ApacheDirStudio.jpeg" width="60"></a></td>
        <td><b>Apache Directory Studio</b><br>Best Open Source RCP Application
          2009<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="http://www.pluck-n-play.com/en/"><img
          src="images/technology/ChordScaleGen.png" width="60"></a></td>
        <td><b>Chord Scale Generator</b><br>Best Commercial RCP Application 2009<br>
          <br>
        </td>
        <td><a target="_blank" href="http://www.eclipse.org/windowbuilder/"><img
          src="images/technology/Instantiations.jpg" width="60"></a></td>
        <td><b>Instantiations WindowBuilder Pro</b><br>Best Commercial Developer
          Tool 2009<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="https://launchpad.net/modulefusion"><img
          src="images/technology/ModuleFusion.jpg" width="60"></a></td>
        <td><b>ModuleFusion</b><br>Best Open Source Equinox Application 2009<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://www.prosyst.com/what-we-do/ehealth-aal/products/"><img
          src="images/technology/ProSyst.jpeg" width="60"></a></td>
        <td><b>ProSyst mBedded Server Smart Home Extension</b><br>Best
          Commercial Equinox Application 2009<br>
          <br>
        </td>
      </tr>
    </table>
    <br />
    <h2>2008</h2>
    <table class="table">
      <tr>
        <td><img src="images/technology/Cyrano.jpeg" width="60"></td>
        <td><b>CYRANO</b><br>Best Commercial Equinox Application 2008<br>
          <br>
        </td>
        <td><a target="_blank" href="http://www.eclemma.org/"><img
          src="images/technology/EclEmma.gif" width="60"></a></td>
        <td><b>EclEmma</b><br>Best Open Source Developer Tool 2008<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank"
          href="http://mytourbook.sourceforge.net/mytourbook/"><img
          src="images/technology/MyTourbook.jpeg" width="60"></a></td>
        <td><b>MyTourbook</b><br>Best Open Source RCP Application 2008<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://www.windriver.com/products/workbench/"><img
          src="images/technology/WindRiver.jpg" width="60"></a></td>
        <td><b>Wind River Workbench</b><br>Best Commercial Developer Tool 2008<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="http://www.xmind.net/"><img
          src="images/technology/XMIND.jpeg" width="60"></a></td>
        <td><b>XMIND 2008</b><br>Best Commercial RCP Application 2008<br>
          <br>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <br />
    <h2>2007</h2>
    <table class="table">
      <tr>
        <td><a target="_blank" href="http://eclipse-cs.sourceforge.net/"><img
          src="images/technology/eclipse-cs.png" width="60"></a></td>
        <td><b>eclipse-cs Checkstyle Plugin</b><br>Best Open Source Developer
          Tool 2007<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://www.jpmorganchase.com/corporate/Home/home.htm"><img
          src="images/technology/JPMorgan.gif" width="60"></a></td>
        <td><b>JPMorgan Chase</b><br>Best Deployment of Eclipse Technology in an
          Enterprise 2007<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><img src="images/technology/PSICAT.gif" width="60"></td>
        <td><b>PSICAT</b><br>Best Open Source RCP Application 2007<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://www.qnx.com/products/tools/qnx-momentics.html"><img
          src="images/technology/QNX.gif" width="60"></a></td>
        <td><b>QNX Momentics IDE</b><br>Best Commercial Developer Tool 2007<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="TIBCO Business Studio"<img
          src="images/technology/TIBCO.jpeg" width="60"></a></td>
        <td><b>TIBCO Business Studio</b><br>Best Commercial RCP Application 2007<br>
          <br>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <br />
    <h2>2006</h2>
    <table class="table">
      <tr>
        <td><a target="_blank"
          href="http://docs.oracle.com/cd/E13226_01/workshop/docs92/studio.html"><img
          src="images/technology/BEA.gif" width="60"></a></td>
        <td><b>BEA Workshop Studio 3.0</b><br>Best Commercial Developer Tool
          2006<br>
          <br>
        </td>
        <td><img src="images/technology/CompassGroup.jpg" width="60"></td>
        <td><b>Compass Group Southern Africa via Jigsaw Interactive</b><br>Best
          Deployment of Eclipse Technology in an Enterprise 2006<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="http://en.wikipedia.org/wiki/GumTree"><img
          src="images/technology/GumTree.png" width="60"></a></td>
        <td><b>Gumtree</b><br>Best Open Source RCP Application 2006<br>
          <br>
        </td>
        <td><a target="_blank"
          href="http://www.bpminstitute.org/resources/lombardi-teamworks-60"><img
          src="images/technology/Lombardi.jpeg" width="60"></a></td>
        <td><b>Lombardi Software TeamWorks</b><br>Best Commercial RCP
          Application 2006<br>
          <br>
        </td>
      </tr>
      <tr>
        <td><a target="_blank" href="http://www.aptana.com/products/radrails"><img
          src="images/technology/RadRails.jpeg" width="60"></a></td>
        <td><b>RadRails</b><br>Best Open Source Developer Tool 2006<br>
          <br>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>