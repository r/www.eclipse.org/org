<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>Presented below is the list of award winners and finalists for each of the project awards
    catagories as determined by votes from the community.</p>
  <h2>Individual Award Finalists</h2>
  <h3>Top Committer</h3>
  <ul class="fa-ul">
    <li>Alexander Ny&#223;en</li>
    <li>Johannes Faltermeier</li>
    <li>Marc-André Laperle</li>
    <li>Marcel Bruch</li>
    <li><i class="fa-li fa fa-star orange"></i> Mickael Istria</li>
    <li>Pascal Rapicault</li>
    <li>Simon Bernard</li>
  </ul>
  <p>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=484318">Full list of nominees</a>
  </p>
  <h3>Top Newcomer Evangelist</h3>
  <ul class="fa-ul">
    <li>Christian Pontesegger</li>
    <li><i class="fa-li fa fa-star orange"></i> Jay Jay Billings</li>
    <li>Philip Wenig</li>
  </ul>
  <p>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=484319">Full list of nominees</a>
  </p>
  <h2>Project Award Finalists</h2>
  <h3>Most Innovative Project</h3>
  <ul class="fa-ul">
    <li>CDT Launchbar</li>
    <li><i class="fa-li fa fa-star orange"></i> Eclipse Advanced Scripting Environment (EASE)</li>
    <li>Eclipse Layout Kernel (ELK)</li>
    <li>EMF Forms</li>
  </ul>
  <p>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=484320">Full list of nominees</a>
  </p>
  <h3>Most Open Project</h3>
  <ul class="fa-ul">
    <li>CDT</li>
    <li>EGit</li>
    <li><i class="fa-li fa fa-star orange"></i> Platform UI</li>
  </ul>
  <p>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=484321">Full list of nominees</a>
  </p>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
      <li><a target="_blank"
        href="https://www.flickr.com/photos/108559379@N08/25655295096/in/album-72157665437566641/"
      >Ceremony Photos</a></li>
    </ul>
  </div>
</div>
