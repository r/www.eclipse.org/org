<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1>Eclipse Community Awards<br><small><?= $pageTitle ?></small></h1>
  <p>The Lifetime Achievement award is given to those individuals who have helped shape Eclipse through a long history of contributions to Eclipse projects and active participation in our community.</p>
  <div class="homeitem3col">
    <table class="table">
      <tr>
        <td><img src="images/lifetime/jonah-graham.jpg" width="80"></td>
        <td><b>Jonah Graham</b><br>Lifetime Achievement Award 2023</td>
      </tr>
      <tr>
        <td><img src="images/lifetime/tom-watson.jpg" width="80"></td>
        <td><b>Tom Watson</b><br>Lifetime Achievement Award 2022</td>
      </tr>
      <tr>
        <td><img src="images/lifetime/melanie_bats.jpg" width="80"></td>
        <td><b>Melanie Bats</b><br>Lifetime Achievement Award 2021</td>
      </tr>
      <tr>
        <td><img src="images/lifetime/stephan_herrmann.png" width="80"></td>
        <td><b>Stephan Herrmann</b><br>Lifetime Achievement Award 2019</td>
      </tr>
      <tr>
        <td><img src="images/lifetime/jens.jpg" width="80"></td>
        <td><b>Jens Reimann</b><br>Lifetime Achievement Award 2018</td>
      </tr>
      <tr>
        <td><img src="images/lifetime/Doug.jpg" width="80"></td>
        <td><b>Doug Schaefer</b><br>Lifetime Achievement Award 2017</td>
      </tr>
      <tr>
        <td><img src="images/lifetime/daniel_megert.jpg" width="80"></td>
        <td><b>Dani Megert</b><br>Lifetime Achievement Award 2016</td>
      </tr>
      <tr>
        <td><img src="images/individual/gunnar.png" width="80"></td>
        <td><b>Gunnar Wagenknecht</b><br>Lifetime Achievement Award 2015</td>
      </tr>
      <tr>
        <td><img src="images/individual/markusknauer.JPG" width="80"></td>
        <td><b>Markus Knauer</b><br>Lifetime Achievement Award 2014</td>
      </tr>
      <tr>
        <td><img src="images/individual/Chris_Aniszczyk.jpg" width="80"></td>
        <td><b>Chris Aniszczyk</b><br>Lifetime Achievement Award 2013</td>
      </tr>
      <tr>
        <td><img src="images/individual/Edmerks.JPG" width="80"></td>
        <td><b>Ed Merks</b><br>Lifetime Achievement Award 2012</td>
      </tr>
      <tr>
        <td><img src="images/lifetime/david-williams-award.jpg" width="80"></td>
        <td><b>David Williams</b><br>Lifetime Achievement Award 2011</td>
      </tr>
    </table>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>
