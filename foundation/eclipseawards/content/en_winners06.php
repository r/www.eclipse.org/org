<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    individual awards catagories.
  </p>
  <div class="homeitem3col">
    <h3>Top Ambassador</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Ed Burnette (Winner)</li>
      <li class="finalist">Erich Gamma</li>
      <li class="finalist">Pat McCarthy</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <div class="homeitem3col">
      <h3>Top Committer</h3>
      <ul class="friends-list fa-ul">
        <li><i class="fa-li fa fa-star orange"></i>Alain Magloire (Winner)</li>
        <li class="finalist">Ed Merks</li>
        <li class="finalist">Jeff McAffer</li>
      </ul>
    </div>
    <h3>Top Contributor</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Linda Watson (Winner)</li>
      <li class="finalist">Gunnar Wagenknecht</li>
      <li class="finalist">Mik Kersten</li>
    </ul>
  </div>
  <h2>Technology Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    technology awards catagories.
  </p>
  <div class="homeitem3col">
    <h3>Best Deployment of Eclipse Technology in an Enterprise</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a href="http://www.ji.co.za">Compass
        Group Southern Africa via Jigsaw Interactive</a> (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Commerical Eclipse-Based Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.bea.com/framework.jsp?CNT=index.htm&FP=/content/products/workshop/studio/">BEA
        Workshop Studio 3.0</a> (Winner)
      </li>
      <li class="finalist"><a href="http://www.exadel.com/web/portal/products">Exadel
        Studio Pro 3.5</a>
      </li>
      <li class="finalist"><a href="http://www.qnx.com/products/development/">QNX
        Momentics Development Suite Professional Edition</a>
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Open Source Eclipse-Based Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.radrails.org/">RadRails</a> (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Commercial RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.lombardisoftware.com/bpm-software-teamworks.php">Lombardi
        Software TeamWorks</a> (Winner)
      </li>
      <li class="finalist"><a href="http://www.logicmindguide.com/demo.htm ">Logic
        MindGuide - Plan&amp;Decide</a>
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Open Source RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://gumtree.sourceforge.net/">Gumtree</a> (Winner)</li>
      <li class="finalist"><a href="http://udig.refractions.net/">User-friendly
        Desktop Internet GIS (uDig)</a>
      </li>
    </ul>
  </div>
  <br /> <br />
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>