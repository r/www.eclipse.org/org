<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    individual awards catagories.</p>
  <div class="homeitem3col">
    <h3>Top Ambassador</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Jeff McAffer</li>
      <li><i class="fa-li fa fa-star orange"></i>Ed Merks (Winner)</li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Chris Aniszczyk (Winner)</li>
      <li class="finalist">Mik Kersten</li>
      <li class="finalist">Paul Webster</li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Top Contributor</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Jacek Pospychala</li>
      <li><i class="fa-li fa fa-star orange"></i>Remy Chi Jian Suen (Winner)</li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Mark Dexter</li>
      <li class="finalist">Walter Harley</li>
      <li><i class="fa-li fa fa-star orange"></i>Eric Rizzo (Winner)</li>
    </ul>
  </div>


  <h2>
    <br>Technology Award Winners
  </h2>
  <p>Presented below is the list of award finalists for each of the technology
    awards catagories as determined by this year's judging panels.</p>

  <div class="homeitem3col">
    <h3>Best Commerical Eclipse-Based Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a
        href="http://www.nexaweb.com/home/us/index.html@cid=2301.html">Nexaweb
          Studio</a></li>
      <li class="finalist"><a
        href="http://www.polarion.com/products/alm/team.php">Polarion&reg; ALM <i>Team</i>
          for Subversion&trade;
      </a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.windriver.com/products/development_suite/">Wind River
          Workbench</a> (Winner)</li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Best Open Source Eclipse-Based Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.eclemma.org/">EclEmma</a> (Winner)</li>
      <li class="finalist"><a href="http://springide.org">Spring IDE</a></li>
      <br>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Commercial Equinox Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a
        href="http://www.birt-exchange.com/modules/products/index.php?productid=3 ">BusinessReport
          Studio</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.bandxi.com/cyrano/index.html">CYRANO</a> (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Commercial RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.openmethods.com/products.php">openVXML</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.xmind.org/us/">XMIND 2008</a> (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Open Source RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://directory.apache.org/studio">Apache
          Directory Studio</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://mytourbook.sourceforge.net/">MyTourbook</a> (Winner)</li>
    </ul>
  </div>
</div>

<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>