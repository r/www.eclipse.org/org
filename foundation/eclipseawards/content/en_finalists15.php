<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <h2>Individual Award Finalists</h2>
  <p>Presented below is the list of award finalists for each of the
    individual awards catagories as determined by votes from the
    community.
  </p>
  <h3>Top Committer</h3>
  <ul class="friends-list fa-ul">
    <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Eugen
      Neufeld
    </li>
    <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
      Marc-André Laperle
    </li>
    <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Marcel
      Bruch
    </li>
  </ul>
  <p>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=455175">Full
    list of nominees</a>
  </p>
  <h3>Top Newcomer Evangelist</h3>
  <ul class="friends-list fa-ul">
    <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
      Annamalai Chockalingam
    </li>
    <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Jay Jay
      Billings
    </li>
    <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Russell
      Bateman
    </li>
  </ul>
  <p>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=455176">Full
    list of nominees</a>
  </p>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>