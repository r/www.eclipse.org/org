<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle ?></h1>

  <div class="row">
    <div class="col-md-3">
      <a href="/ease/"><img class="img-responsive"
        src="/org/foundation/eclipseawards/images/ease-compact.png" /></a>
    </div>
    <div class="col-md-15
  ">
      <strong>Eclipse Advanced Scripting Environment (EASE)</strong>
    </div>
    <div class="col-md-15">Most Innovative Project 2016</div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-3">
      <a href="/recommenders/"><img class="img-responsive"
        src="images/project/code-r.jpg" /></a>
    </div>
    <div class="col-md-11">
      <strong>Eclipse Code Recommenders</strong>
    </div>
    <div class="col-md-15">Most Innovative New Feature or Eclipse Project 2012</div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-3">
      <a href="/ecf/"><img class="img-responsive"
        src="/org/foundation/eclipseawards/images/ecflogo.jpg" /></a>
    </div>
    <div class="col-md-11">
      <strong>Eclipse Communication Framework</strong>
    </div>
    <div class="col-md-15">Most Open Project 2012</div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-3">
      <a href="/e4/"><img class="img-responsive"
        src="/org/foundation/eclipseawards/images/e4.png" /></a>
    </div>
    <div class="col-md-11">
      <strong>e4</strong>
    </div>
    <div class="col-md-15">
      Most Open Project 2011<br>Most Open Project 2010
    </div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-3">
      <a href="/egit/"><img class="img-responsive"
        src="images/project/egit.jpg" /></a>
    </div>
    <div class="col-md-11">
      <strong>EGit</strong>
    </div>
    <div class="col-md-15">
      Most Innovative New Feature or Eclipse Project 2011<br>Most Open Project
      2013
    </div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-3">
      <a href="/eclipse/platform-ui/"><img
        class="img-responsive"
        src="/org/foundation/eclipseawards/images/eclipse_orb.png" /></a>
    </div>
    <div class="col-md-11">
      <strong>Platform UI</strong>
    </div>
    <div class="col-md-15">Most Open Project 2016</div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-3">
      <a href="/diffmerge/"><img class="img-responsive"
        src="images/project/Logo_EDM_Small.png" /></a>
    </div>
    <div class="col-md-11">
      <strong>EMF Diff/Merge</strong>
    </div>
    <div class="col-md-15">Most Innovative New Feature or Eclipse Project 2013</div>
  </div>
  <br />

  <div class="row">
    <div class="col-md-3">
      <a href="/Xtext/"><img class="img-responsive"
        src="/org/foundation/eclipseawards/images/xtext.png" /></a>
    </div>
    <div class="col-md-11">
      <strong>Xtext</strong>
    </div>
    <div class="col-md-15">Most Innovative New Feature or Eclilpse Project 2010</div>
  </div>
  <br />

</div>

<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>