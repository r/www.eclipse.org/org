<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>

  <p>Congratulations to all past winners of the Eclipse Community Awards.</p>

  <h3>Past Winners by Category</h3>
  <ul class="midlist">
    <li><a href="winners_lifetime.php">Lifetime Award Winners</a></li>
    <li><a href="winners_project.php">Project Award Winners</a></li>
    <li><a href="winners_technology.php">Technology Award Winners</a></li>
  </ul>

  <h3>Past Winners by Year</h3>
  <ul class="midlist">
    <li><a href="winners22.php">2022 Award Winners</a></li>
    <li><a href="winners21.php">2021 Award Winners</a></li>
    <li><a href="winners16.php">2016 Award Winners</a></li>
    <li><a href="winners15.php">2015 Award Winners</a></li>
    <li><a href="winners14.php">2014 Award Winners</a></li>
    <li><a href="winners13.php">2013 Award Winners</a></li>
    <li><a href="winners12.php">2012 Award Winners</a></li>
    <li><a href="winners11.php">2011 Award Winners</a></li>
    <li><a href="winners10.php">2010 Award Winners</a></li>
    <li><a href="winners09.php">2009 Award Winners</a></li>
    <li><a href="winners08.php">2008 Award Winners</a></li>
    <li><a href="winners07.php">2007 Award Winners</a></li>
    <li><a href="winners06.php">2006 Award Winners</a></li>
  </ul>
</div>

<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
    </ul>
  </div>
</div>