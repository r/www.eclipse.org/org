<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    individual awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Boris Bokowski</li>
      <li class="finalist">Markus Schorn</li>
      <li><i class="fa-li fa fa-star orange"></i>Eike Stepper (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Contributor</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Matt Hall</li>
      <li class="finalist">Laurens Holst</li>
      <li class="finalist">James Sugrue</li>
      <li><i class="fa-li fa fa-star orange"></i>Lars Vogel (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Russell Bateman</li>
      <li><i class="fa-li fa fa-star orange"></i>Walter Harley (Winner)</li>
      <li class="finalist">Lars Vogel</li>
    </ul>
  </div>
  <h2>Project Award Winners
  </h2>
  <p>Presented below is the list of award winners and finalists for each of the
    project awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Most Innovative New Feature or Eclipse Project</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">EclipseLink</li>
      <li class="finalist">Standard Widget Toolkit (SWT) Cocoa Port</li>
      <li><i class="fa-li fa fa-star orange"></i>Textual Modeling Framework
        (TMF) Xtext (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Most Open Project</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>e4 (Winner)</li>
      <li class="finalist">Equinox p2</li>
      <li class="finalist">Mylyn</li>
      <li class="finalist">XML Tools in Web Tools Platform (WTP) Incubator</li>
    </ul>
  </div>
  <h2>Technology Award Winners
  </h2>
  <p>Presented below is the list of award winners and finalists for each of the
    technology awards catagories as determined by this year's judging panels.
  </p>
  <div class="homeitem3col">
    <h3>Best Commerical Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a
        href="http://www.birt-exchange.com/be/marketplace/app-showcase/"
        target="blank">Actuate BIRT Mashboard Application</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.guidancer.com/" target="blank">Bredex GUIdancer</a>
        (Winner)
      </li>
      <li class="finalist"><a href="http://www.zend.com/products/studio"
        target="blank">Zend Studio 7</a></li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Open Source Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://groovy.codehaus.org/Eclipse+Plugin">Groovy-Eclipse</a>
        (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best EclipseRT Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a
        href="http://www.actuate.com/products/iserver/iserver-enterprise/"
        target="blank">Actuate BIRT iServer Enterprise</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.springsource.org/dmserver" target="blank">SpringSource
        dm Server</a> (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best RCP Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.bioclipse.net/" target="blank">Bioclipse</a></li>
      <li class="finalist"><a href="http://www.postergenius.com/" target="blank">PosterGenius</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a href="http://tasktop.com/"
        target="blank">Tasktop Pro</a> (Winner)</li>
    </ul>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>