<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    individual awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Lifetime Achievement Award</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Chris Aniszczyk (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Edward D. Willink</li>
      <li class="finalist">John Arthorne</li>
      <li><i class="fa-li fa fa-star orange"></i>Markus Knauer</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Benjamin Cabé</li>
      <li><i class="fa-li fa fa-star orange"></i>Jonas Helming</li>
      <li class="finalist">Russell Bateman</li>
    </ul>
  </div>
  <h2>Project Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    project awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Most Innovative New Feature or Eclipse Project</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.eclipse.org/paho/">Eclipse Paho</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.eclipse.org/diffmerge/">EMF Diff/Merge</a></li>
      <li class="finalist"><a href="http://www.eclipse.org/intent/">Intent</a></li>
      <li class="finalist"><a href="http://www.eclipse.org/vjet/">VJET Java
        Script IDE</a>
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Most Open Project</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.eclipse.org/cdt/">C/C++
        Development tooling - CDT</a>
      </li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.eclipse.org/egit/">EGit</a></li>
      <li class="finalist"><a href="http://www.eclipse.org/orion/">The Orion
        Project</a>
      </li>
    </ul>
  </div>
  <h2>Technology Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    technology awards catagories as determined by this year's judging panels.
  </p>
  <div class="homeitem3col">
    <h3>Best Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.bonitasoft.com/">Bonita Open
        Solution</a>
      </li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://chrononsystems.com/chronon4">Chronon 4 'Ops'</a></li>
      <li class="finalist"><a
        href="http://www.talend.com/products/talend-open-studio">Talend Open
        Studio</a>
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a
        href="http://www.klocwork.com/products/insight/index.php">Klocwork
        Insight</a>
      </li>
      <li class="finalist"><a href="http://www.sonarsource.org/">Sonar</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://wireframesketcher.com/">WireframeSketcher Wireframing Tool</a></li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Developer Plugin</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.efxclipse.org/">e(fx)clipse</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://findbugs.sourceforge.net/index.html">FindBugs Eclipse
        Plugin</a>
      </li>
      <li class="finalist"><a
        href="http://www.junitloop.org/index.php/JUnitLoop">JUnitLoop</a></li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Modeling Product</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.obeodesigner.com/">Obeo Designer</a></li>
      <li class="finalist"><a href="http://www.umlet.com/">UMLet</a></li>
    </ul>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>