<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle ?></h1>
  <h2>Individual Award Finalists</h2>
  <p>Presented below is the list of award finalists for each of the individual
    awards catagories as determined by votes from the community.</p>

  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul>
      <li class="finalist">Boris Bokowski</li>
      <li class="finalist">Markus Schorn</li>
      <li class="finalist">Eike Stepper</li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Top Contributor</h3>
    <ul>
      <li class="finalist">Matt Hall</li>
      <li class="finalist">Laurens Holst</li>
      <li class="finalist">James Sugrue</li>
      <li class="finalist">Lars Vogel</li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul>
      <li class="finalist">Russell Bateman</li>
      <li class="finalist">Walter Harley</li>
      <li class="finalist">Lars Vogel</li>
    </ul>
  </div>
  <p>
    <a href="individual.php">A complete list of the nominees for the 2010
      Individual Awards can be found here.</a>
  </p>

  <h2>Project Award Finalists</h2>
  <p>Presented below is the list of award finalists for each of the project
    awards catagories as determined by votes from the community.</p>

  <div class="homeitem3col">
    <h3>Most Innovative New Feature or Eclipse Project</h3>
    <ul>
      <li class="finalist">EclipseLink</li>
      <li class="finalist">Standard Widget Toolkit (SWT) Cocoa Port</li>
      <li class="finalist">Textual Modeling Framework (TMF) Xtext</li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Most Open Project</h3>
    <ul>
      <li class="finalist">e4</li>
      <li class="finalist">Equinox p2</li>
      <li class="finalist">Mylyn</li>
      <li class="finalist">XML Tools in Web Tools Platform (WTP) Incubator</li>
    </ul>
  </div>
  <p>
    <a href="project.php">A complete list of the nominees for the 2010 Project
      Awards can be found here.</a>
  </p>

  <h2>Technology Award Finalists</h2>
  <p>Presented below is the list of award finalists for each of the technology
    awards catagories as determined by this year's judging panels.</p>

  <div class="homeitem3col">
    <h3>Best Commerical Developer Tool</h3>
    <ul>
      <li class="finalist"><a
        href="http://www.birt-exchange.com/be/marketplace/app-showcase/"
        target="blank">BIRT Mashboard Application</a></li>
      <li class="finalist"><a href="http://www.guidancer.com/" target="blank">GUIdancer</a></li>
      <li class="finalist"><a href="http://www.zend.com/products/studio"
        target="blank">Zend Studio 7</a></li>
    </ul>
  </div>

  <div class="homeitem3col">
    <h3>Best Open Source Developer Tool</h3>
    <ul>
      <li class="finalist">There is 1 winner in this category with no additional
        finalists. The winning tool will be announced at the Eclipse Awards
        ceremony. <a href="technology.php">See all nominees</a>.
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best EclipseRT Application</h3>
    <ul>
      <li class="finalist"><a
        href="http://www.actuate.com/products/iserver/iserver-enterprise/"
        target="blank">BIRT iServer Enterprise</a></li>
      <li class="finalist"><a href="http://www.springsource.org/dmserver"
        target="blank">SpringSource dm Server</a></li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best RCP Application</h3>
    <ul>
      <li class="finalist"><a href="http://www.bioclipse.net/" target="blank">Bioclipse</a></li>
      <li class="finalist"><a href="http://www.postergenius.com/" target="blank">PosterGenius</a></li>
      <li class="finalist"><a href="http://tasktop.com/" target="blank">Tasktop
          Pro</a></li>
    </ul>
  </div>
  <p>
    <a href="technology.php">A complete list of the nominees for the 2010
      Technology Awards can be found here.</a>
  </p>
</div>

<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
      <li><a href="individual.php">Individual Nominees</a></li>
      <li><a href="project.php">Project Nominees</a></li>
      <li><a href="technology.php">Technology Nominees</a></li>
    </ul>
  </div>
</div>