<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    individual awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Lifetime Achievement Award</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Ed Merks (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Daniel Megert</li>
      <li class="finalist">Kim Moir</li>
      <li><i class="fa-li fa fa-star orange"></i>Sven Efftinge (Winner)</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Contributor</h3>
    <ul class="friends-list fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Stephan Herrmann <span
        class="winner">(Winner)</span></li>
      <li><i class="fa-li fa fa-star orange"></i>Alex Blewitt <span
        class="winner">(Winner)</span></li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist">Russell Bateman</li>
      <li class="finalist">Lu Yang</li>
      <li><i class="fa-li fa fa-star orange"></i> Lars Vogel (Winner)</li>
    </ul>
  </div>
  <h2>Project Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    project awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Most Innovative New Feature or Eclipse Project</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.eclipse.org/orion/">Orion</a></li>
      <li class="finalist"><a href="http://www.eclipse.org/Xtext/">Xtend</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.eclipse.org/recommenders/">Eclipse Code Recommenders</a>
        (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Most Open Project</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.eclipse.org/cdt/">C/C++
        Development Tooling</a>
      </li>
      <li class="finalist"><a
        href="http://www.eclipse.org/projects/project.php?id=modeling.gmp.gmf-tooling">GMF
        Tooling</a>
      </li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.eclipse.org/ecf/">Eclipse Communication Framework</a>
        (Winner)
      </li>
    </ul>
  </div>
  <h2>Technology Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    technology awards catagories as determined by this year's judging panels.
  </p>
  <div class="homeitem3col">
    <h3>Best Developer Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://bndtools.org/">Bndtools</a></li>
      <li class="finalist"><a
        href="http://www.arm.com/products/tools/software-tools/ds-5/community-edition/index.php">DS-5
        Community Edition</a>
      </li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://www.chrononsystems.com/">Chronon Time Travelling Debugger</a>
        (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Modeling Tool</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.umlet.com/">UMLet</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a href="http://maintainj.com/">MaintainJ</a>
        (Winner)
      </li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Best Application</h3>
    <ul class="friends-list fa-ul">
      <li class="finalist"><a href="http://www.bonitasoft.com/">Bonita Open
        Solutions</a>
      </li>
      <li class="finalist"><a
        href="http://www.diligent-it.com/files/ea2012/CCTVnet.html">CCTVnet</a></li>
      <li><i class="fa-li fa fa-star orange"></i><a
        href="http://justinmind.com/">Justinmind Prototyper</a> (Winner)</li>
    </ul>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>