<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle ?></h1>
  <h2>Individual Award Finalists</h2>
  <p>Presented below is the list of award finalists for each of the
    individual awards catagories as determined by votes from the
    community.
  </p>
  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul>
      <li class="finalist">Edward D. Willink</li>
      <li class="finalist">John Arthorne</li>
      <li class="finalist">Markus Knauer</li>
    </ul>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=396183">Full
    list of nominees</a>
  </div>
  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul>
      <li class="finalist">Benjamin Cabé</li>
      <li class="finalist">Jonas Helming</li>
      <li class="finalist">Russell Bateman</li>
    </ul>
    <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=396185">Full
    list of nominees</a>
  </div>
  <br />
  <h2>Project Award Finalists</h2>
  <p>Presented below is the list of award finalists for each of the
    project awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Most Innovative New Feature or Eclipse Project</h3>
    <ul>
      <li class="finalist">Eclipse Paho</li>
      <li class="finalist">EMF Diff/Merge</li>
      <li class="finalist">Intent</li>
      <li class="finalist">VJET Java Script IDE</li>
    </ul>
    <a
      href="http://marketplace.eclipse.org/nominations/most-innovative-project">Full
    list of nominees</a>
  </div>
  <div class="homeitem3col">
    <h3>Most Open Project</h3>
    <ul>
      <li class="finalist">C/C++ Development tooling - CDT</li>
      <li class="finalist">EGit</li>
      <li class="finalist">The Orion Project</li>
    </ul>
    <a
      href="http://marketplace.eclipse.org/nominations/most-open-project">Full
    list of nominees</a>
  </div>
  <br />
  <h2>Technology Award Finalists</h2>
  <p>Presented below is the list of award finalists for each of the
    technology awards catagories as determined by this year's judging
    panels.
  </p>
  <div class="homeitem3col">
    <h3>Best Application</h3>
    <ul>
      <li class="finalist">Bonita Open Solution</li>
      <li class="finalist">Chronon 4 'Ops'</li>
      <li class="finalist">Talend Open Studio</li>
    </ul>
    <a
      href="http://marketplace.eclipse.org/nominations/best-application">Full
    list of nominees</a>
  </div>
  <div class="homeitem3col">
    <h3>Best Developer Tool</h3>
    <ul>
      <li class="finalist">Klocwork Insight</li>
      <li class="finalist">Sonar</li>
      <li class="finalist">WireframeSketcher Wireframing Tool</li>
    </ul>
    <a
      href="http://marketplace.eclipse.org/nominations/best-developer-tool">Full
    list of nominees</a>
  </div>
  <div class="homeitem3col">
    <h3>Best Developer Plugin</h3>
    <ul>
      <li class="finalist">e(fx)clipse</li>
      <li class="finalist">FindBugs Eclipse Plugin</li>
      <li class="finalist">JUnitLoop</li>
    </ul>
    <a
      href="http://marketplace.eclipse.org/nominations/best-developer-plugin">Full
    list of nominees</a>
  </div>
  <div class="homeitem3col">
    <h3>Best Modeling Product</h3>
    <ul>
      <li class="finalist">Obeo Designer</li>
      <li class="finalist">UMLet</li>
    </ul>
    <a
      href="http://marketplace.eclipse.org/nominations/best-modeling-product">Full
    list of nominees</a>
  </div>
  <br />
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>