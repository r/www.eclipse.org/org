<?php

/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *	 Zhou Fang (Eclipse Foundation)
 * 
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<h1>Eclipse Foundation Community Awards</h1>

<p>The Eclipse Foundation Community Awards recognize the best of the Eclipse community.</p>

<h2>Awards</h2>

<h3 id="top-committer">Top Committer</h3>
<p>This award is to recognize the Eclipse committer who best exemplifies community support - through code contributions, fixing bugs, forum and mailing list posts, conference presentations, blogs and other areas.</p>

<hr />

<h3 id="top-newcomer">Top Newcomer Evangelist</h3>
<p>This award is to recognize the individual who best welcomes, engages and educates new people into the Eclipse community through the user forums, blogging, creating resources like demos and tutorials, and Eclipse-related conversations on social media.</p>
<hr />

<h3 id="top-contributor">Top Contributor</h3>
<p>This award is to recognize an individual who best exemplifies support for the Eclipse community through their contributions to forums, submission of patches, comments on bugs, tutorials, conference presentations, blogs, and other areas. Contributors are the individuals that participate due to their passion for the community and technology.&nbsp;</p>
<hr />

<h3 id="lifetime-achievement">Lifetime Achievement&nbsp;</h3>
<p>The Lifetime Achievement award is given to those individuals who have helped shape the Eclipse community through a long history of contributions to Eclipse projects, and active participation and leadership in our community.</p>
<hr />

<h2>Nominations &amp; Voting</h2>
<p>Anyone can nominate a community member for these awards. Once nominations are collected, the community is invited to vote to determine the winners for the Top Committer, Top Contributor, and Top Newcomer Evangelist awards.</p>
<p>The Lifetime Achievement award winner will be selected by the Eclipse Foundation team.</p>

<h2>Past Winners</h2>

<p>See the <a href="https://www.eclipse.org/org/foundation/eclipseawards/pastwinners.php">Past Winners</a> of the Eclipse Community Awards.</p>
