<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Denis Roy (Eclipse Foundation)
 *   Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <h2>Individual Award Winners</h2>
  <p>Presented below is the list of award winners and finalists for each of the
    individual awards catagories as determined by votes from the community.
  </p>
  <div class="homeitem3col">
    <h3>Lifetime Achievement Award</h3>
    <ul class="fa-ul">
      <li><i class="fa-li fa fa-star orange"></i>Markus Knauer</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Committer</h3>
    <ul class="fa-ul">
      <li>Dani Megert</li>
      <li>John Arthorne</li>
      <li>Pascal Rapicault</li>
      <li>Stephan Herrmann</li>
      <li>Silenio Quarti</li>
      <li><i class="fa-li fa fa-star orange"></i>Tom Schindl</li>
      <li>Wim Jongman</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Top Newcomer Evangelist</h3>
    <ul class="fa-ul">
      <li>Cédric Brun</li>
      <li><i class="fa-li fa fa-star orange"></i>Dani Megert</li>
      <li>Nitin Dahyabhai</li>
      <li>Russ Bateman</li>
    </ul>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="index.php">Eclipse Community Awards</a></li>
      <li><a href="pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>