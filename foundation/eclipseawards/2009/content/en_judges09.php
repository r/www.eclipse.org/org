<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Denis Roy (Eclipse Foundation)
 * Eric Poirier (Eclipse Foundation)
 */
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>Presented below is the list of Technology Awards judges for the 2009 Eclipse Community Awards.</p>
  <h3>Commercial Awards</h3>
  <ul>
    <li>Alex Blewitt - InfoQ</li>
    <li>Michael Cot&eacute; - RedMonk</li>
    <li>Don Dingee - Embedded Computing Design</li>
    <li>James Sugrue - DZone</li>
    <li>Jeffrey Hammond - Forrester Research</li>
    <li>Peter Varhol - TechTarget</li>
    <li>Alan Zeichick - BZ Media</li>
  </ul>
  <h3>Open Source Awards</h3>
  <ul>
    <li>Chris Aniszczyk - Plugin Development Environment (PDE) Project</li>
    <li>Rob Elves - Mylyn Project</li>
    <li>Kenn Hussey - Modeling Project</li>
    <li>Markus Knauer - g-Eclipse &amp; Packaging Projects</li>
    <li>Carlos Sanchez - Integration for Apache Maven (IAM) Project</li>
    <li>Tom Schindl - Nebula Project</li>
    <li>Gunnar Wagenknecht - Gyrex Project</li>
    <li>Greg Watson - Parallel Tools Platform (PTP) Project</li>
    <li>David Williams - Web Tools Project</li>
  </ul>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="../index.php">Eclipse Community Awards</a></li>
      <li><a href="../winners09.php">2009 Winners</a></li>
      <li><a href="../pastwinners.php">Past Winners</a></li>
    </ul>
  </div>
</div>