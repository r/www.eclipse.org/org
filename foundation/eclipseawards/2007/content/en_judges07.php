<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Denis Roy (Eclipse Foundation)
 * Eric Poirier (Eclipse Foundation)
 */
?>

<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>Presented below is the list of Technology Awards judges for the 2007 Eclipse Community Awards.</p>
  <div class="homeitem3col">
    <h3>Open Source Awards</h3>
    <ul>
      <li>Doug Gaff - Device Debugging Project</li>
      <li>Mik Kersten - Mylar Project</li>
      <li>Jeff McAffer - Equinox Project</li>
      <li>Ed Merks - Eclipse Modeling Framework (EMF) Project</li>
      <li>Wenfeng Li - Business Intelligence and Reporting Tools (BIRT) Project</li>
      <li>Shaun Smith - Dali JPA Tools Project</li>
      <li>David Williams - Web Standard Tools Project</li>
      <li>Joe Winchester - Visual Editor Project</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Commercial Awards</h3>
    <ul>
      <li>Alex Blewitt - EclipseZone</li>
      <li>Scott Delap - InfoQ</li>
      <li>Don Dingee - Embedded Computing Design</li>
      <li>Daniel Spiewak - EclipseZone</li>
      <li>Alan Zeichick - BZ Media</li>
    </ul>
  </div>
  <div class="homeitem3col">
    <h3>Enterprise Deployment Award</h3>
    <ul>
      <li>Wayne Beaton - Eclipse Foundation Evangelist</li>
      <li>Ed Burnette - Eclipse Platform Project</li>
      <li>Kevin Haaland - Eclipse Platform & Plug-in Development Environment (PDE) Projects</li>
      <li>Per Kroll - Eclipse Process Framework (EPF) Project</li>
    </ul>
  </div>
</div>
<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
  <div class="sideitem">
    <h6>Awards Navigation</h6>
    <ul>
      <li><a href="../winners07.php">Winners</a></li>
    </ul>
  </div>
</div>