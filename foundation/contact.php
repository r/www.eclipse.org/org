<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Eric Poirier (Eclipse Foundation)
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ("../_projectCommon.php");

// Begin: page-specific settings. Change these.
$pageTitle = "Contact Us";

$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Contact,email,telephone,support,problem");

// Place your html content in a file called content/en_pagename.php
ob_start();
include ("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();
