<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/matthias_zimmermann.png" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">

<p>Matthias has joined BSI in 2006 working in the role of a project manager. He has then played an important role in convincing BSI to open source its business application framework as <a href="https://www.eclipse.org/scout/" target="_blank">Eclipse Scout</a> and has been co-lead of the Scout project ever since.</p> 

<p>Over the last years he has focused on growing the open source community and increasing the commercial adoption of the Scout framework. In addition, he is a founding member of the <a target="_blank" href="http://www.jug.ch/sigs.php?sig=1" target="_blank">Swiss Eclipse User Group</a> and co-organizes Eclipse events in both Zurich and Munich.</p> 

<p>Matthias holds a PhD in computer science from the University of Bern in Switzerland and enjoys having time with his family, tinkering with machine learning and IoT and being a flight instructor. You find him via <a target="_blank" href="https://twitter.com/ZimMatthias">@ZimMatthias</a>.</p>

</div>
</div>