<div class="row">
  <div class="col-sm-4 col-xs-24"><img class="img-responsive" src="boardbios/photos/daniel_megert.jpg" align="left" style="padding-right: 5px;" width="120"></div>
  <div class="col-sm-20">

Dani is one of the initial Eclipse committers. Currently he leads the <a href="https://wiki.eclipse.org/Platform">Platform</a> and the <a href="http://www.eclipse.org/jdt/">JDT</a> subprojects, represents the project in the Eclipse <a href="http://wiki.eclipse.org/Planning_Council">Planning Council</a>, and is also a a member of the Eclipse <a href="http://wiki.eclipse.org/Architecture_Council">Architecture Council</a>. Dani worked at OTI and now works at IBM Research GmbH. His interests include user interface and API design, editors, software quality and performance. 
</div>
</div>