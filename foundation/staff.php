<?php
/**
 * *****************************************************************************
 * Copyright (c) 2005, 2011, 2017 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Wayne Beaton (Eclipse Foundation) - initial API and implementation
 * Christopher Guindon (Eclipse Foundation) - Incremental improvements
 * *****************************************************************************
 */
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App = new App();
$Nav = new Nav();
include ($App->getProjectCommon());

$pageTitle = "Eclipse Foundation Staff";
$ThemeClass = $App->getThemeClass($theme);

$ThemeClass->setPageAuthor('Eclipse Foundation');
$ThemeClass->setPageKeywords("Eclipse Foundation staff, Eclipse staff, Foundation staff");
$ThemeClass->setPageTitle($pageTitle);

ob_start();
?>
<div id="midcolumn">
  <h1><?php print $pageTitle; ?></h1>
  <p>The Eclipse Foundation is fortunate to have some very talented
    people working full-time on behalf of the Eclipse community. Below is
    the list of our staff.</p>
  <p>Eclipse email addresses all follow the firstname.lastname@eclipse-foundation.org convention.</p>

    <?php
    // @todo: Create a JS widjet to display staff bio from api.eclipse.org
    $staff = simplexml_load_file(dirname(__FILE__) . '/staff.xml');
    // create proxy to allow sorting
    $staffProxy = array();
    foreach ($staff as $member) {
      $staffProxy[] = $member;
    }
    usort($staffProxy, function($a, $b) {
      if ($a->name == "Mike Milinkovich") {
        return -1;
      } else if ($b->name == "Mike Milinkovich") {
        return 1;
      }

      $aNameParts = explode(' ', $a->name);
      $aLastName = $aNameParts[count($aNameParts) - 1];
      $bNameParts = explode(' ', $b->name);
      $bLastName = $bNameParts[count($bNameParts) - 1];
      return strcmp($aLastName, $bLastName);
    });
    // iterate over proxy rather than raw data
    foreach ($staffProxy as $member) {
      print '<div class="media clearfix">';
      print '<div class="col-xs-5 col-sm-4">';
      if ($member->image) {
        print '<a name="' . $member->id . '"';
        if (!empty($member->username)) {
          print ' href="https://www.eclipse.org/user/' . $member->username . '"';
        }
        print '><img class="img-thumbnail img-responsive margin-top-20" src="' . $member->image . '" alt="' . $member->name . '"></a>';
      }
      print '</div>';
      print '<div class="col-xs-19 col-sm-20">';
      print '<div class="media-body">';
      print '<h3 class="margin-bottom-0">';
      if (!empty($member->username)) {
        print '<a href="https://www.eclipse.org/user/' . $member->username . '">' . $member->name . ' <i class="fa fa-link" aria-hidden="true"></i></a>';
      }
      else {
        print $member->name;
      }
      print '</h3>';
      if ($member->title) {
        print '<h5 class="margin-top-5">' . $member->title . '</h5>';
      }
      print $member->description;
      if ($member->twitter) {
        print $App->getTwitterFollowWidget($member->twitter);
      }
      print '</div></div></div><hr/>';
    }
    ?>
  </div>

<?php
$html = ob_get_contents();
ob_end_clean();

$ThemeClass->setNav($Nav);
$ThemeClass->setHtml($html);
$ThemeClass->generatePage();