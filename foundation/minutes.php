<?php                                                              
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
  require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
  $App = new App();
  $Nav = new Nav();  
  $Menu = new Menu();
  include($App->getProjectCommon()); 

  #*****************************************************************************
  #
  # sample_3col.php
  #
  # Author:     Denis Roy
  # Date:      2005-11-07
  #
  # Description: Type your page comments here - these are not sent to the browser
  #
  #
  #****************************************************************************

  #
  # Begin: page-specific settings.  Change these.
  $pageTitle     = "Meeting Minutes";
  $pageKeywords  = "foundation, minutes, legal";
  $pageAuthor    = "Mike Milinkovich, Nov 24, 2005";

  # Add page-specific Nav bars here
  # Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
  # $Nav->addNavSeparator("My Page Links",   "downloads.php");
  # $Nav->addCustomNav("My Link", "mypage.php", "_self", 1);
  # $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 1);

  # End: page-specific settings
  #

  # Paste your HTML content between the EOHTML markers!
  $html = <<<EOHTML

  <div id="midcolumn">
    <h1><a name="top">$pageTitle</a></h1>
    <blockquote>
      <ul>
        <li><a href="#board">Board of Directors Meetings</a></li>
        <li><a href="#assembly">General Assembly Meetings</a></li>
        <li><a href="#members">Members Meetings</a></li>
        <li><a href="#councils">Council Meetings</a></li>
        <li><a href="#workingGroups">Working Groups</a></li>
        <li><a href="minutes_archive.php">Meeting minutes archive</a></li>
      </ul>
    </blockquote>
    
    <h2 id="board">Board of Directors Meetings</h2>
    
    <div class="panel-group" id="minutes-accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading-aisbl-minutes">
          <h3 class="panel-title" id="boardAISBL">
            <a 
              role="button" 
              href="#aisbl-minutes" 
              data-toggle="collapse" 
              data-parent="#minutes-accordion" 
              aria-expanded="true" 
              aria-controls="aisbl-minutes"
            >
              Eclipse Foundation AISBL
            </a>
          </h3>
        </div>
        <div class="panel-collapse collapse in" id="aisbl-minutes" role="tabpanel" aria-labelledby="heading-aisbl-minutes">
          <div class="panel-body">
            <ul>
              <li>
                Abridged Minutes -
                <a href="boardminutes/2024-10-21-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  October 21, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes -
                <a href="boardminutes/2024-09-18-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  September 18, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes -
                <a href="boardminutes/2024-07-17-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  July 17, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes -
                <a href="boardminutes/2024-06-25-26-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  June 25-26, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-05-15-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  May 15, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-04-10-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  April 10, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-02-21-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  February 21, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-01-17-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  January 17, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-12-20-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  December 20, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-10-16-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  October 16, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-09-20-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  September 20, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-07-19-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  July 19, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-06-27-28-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  June 27-28, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-05-17-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  May 17, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-04-18-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  April 18, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-02-15-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  February 15, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-01-18-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  January 18, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-12-14-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  December 14, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-10-24-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  October 24, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-09-21-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  September 21, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-07-20-eclipse-foundation-aisbl-board-meeting-abridged-minutes.pdf">
                  July 20, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-06-28-29-eclipse-foundation-aisbl-board-meeting-adbridged-minutes.pdf">
                  June 28-29, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022_05_25_Minutes_AISBL.pdf">
                  May 25, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-04-20-minutes-aisbl.pdf">
                  April 20, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022_03_23_24_Minutes_AISBL.pdf">
                  March 23-24, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022_02_16_Minutes_AISBL.pdf">
                  February 16, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_12_15_Minutes_AISBL.pdf">
                  December 15, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_11_17_Minutes_AISBL.pdf">
                  November 17, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_10_20_21_Minutes_AISBL.pdf">
                  October 20-21, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_09_15_Minutes_AISBL.pdf">
                  September 15, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_07_21_Minutes_AISBL.pdf">
                  July 21, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_06_15_17_Minutes_AISBL.pdf">
                  June 15-17, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_05_19_Minutes_AISBL.pdf">
                  May 19, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_04_21_Minutes_AISBL.pdf">
                  April 21, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_03_24_25_Minutes_AISBL.pdf">
                  March 24-25, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_02_17_Minutes_AISBL.pdf">
                  February 17, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_01_20_Minutes_AISBL.pdf">
                  January 20, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_01_13_Minutes_AISBL.pdf">
                  January 13, 2021 (pdf)
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading-inc-minutes">
          <h3 class="panel-title" id="boardInc">
            <a 
              role="button" 
              href="#inc-minutes" 
              data-toggle="collapse" 
              data-parent="#minutes-accordion" 
              aria-expanded="true" 
              aria-controls="inc-minutes"
            >
              Eclipse Foundation, Inc.
            </a>
          </h3>
        </div>
        <div class="panel-collapse collapse" id="inc-minutes" role="tabpanel" aria-labelledby="heading-inc-minutes">
          <div class="panel-body">
            <ul>
              <li>
                Abridged Minutes -
                <a href="boardminutes/2024-10-21-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  October 21, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes -
                <a href="boardminutes/2024-07-17-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  July 17, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-06-26-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  June 26, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-05-15-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  May 15, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-04-10-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  April 10, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-02-21-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  February 21, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2024-01-17-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  January 17, 2024 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-12-20-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  December 20, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-10-16-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  October 16, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-09-20-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  September 20, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-07-19-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  July 19, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-06-28-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  June 28, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-05-17-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  May 17, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-04-18-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  April 18, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2023-02-15-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  February 15, 2023 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-12-14-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  December 14, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-10-24-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  October 24, 2022 (pdf)
                </a>
              </li>
              <li>Abridged Minutes - 
                <a href="boardminutes/2022-07-20-eclipse-foundation-inc-board-meeting-abridged-minutes.pdf">
                  July 20, 2022 (pdf)
                </a>
              </li>
              <li>Abridged Minutes - 
                <a href="boardminutes/2022-06-29-eclipse-foundation-inc-board-meeting-adbridged-minutes.pdf">
                  June 29, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022_05_25_Minutes_Inc.pdf">
                  May 25, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022-04-20-minutes-inc.pdf">
                  April 20, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022_03_24_Minutes_Inc.pdf">
                  March 24, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2022_02_16_Minutes_Inc.pdf">
                  February 16, 2022 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_12_15_Minutes_Inc.pdf">
                  December 15, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_11_17_Minutes_Inc.pdf">
                  November 17, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_10_21_Minutes_Inc.pdf">
                  October 21, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_09_15_Minutes_Inc.pdf">
                  September 15, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_07_21_Minutes_Inc.pdf">
                  July 21, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_06_17_Minutes_Inc.pdf">
                  June 17, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_05_19_Minutes_Inc.pdf">
                  May 19, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_04_21_Minutes_Inc.pdf">
                  April 21, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_03_25_Minutes_Inc.pdf">
                  March 25, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_02_17_Minutes_Inc.pdf">
                  February 17, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2021_01_20_Minutes_Inc.pdf">
                  January 20, 2021 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_12_16_Minutes.pdf">
                  December 16, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_11_18_Minutes.pdf">
                  November 18, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_10_14_15_Minutes.pdf">
                  October 14-15, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_09_16_Minutes.pdf">
                  September 16, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_08_19_Minutes.pdf">
                  August 19, 2020 (pdf)
                </a>
              </li>
              <li>Abridged Minutes - 
                <a href="boardminutes/2020_07_15_16_Minutes.pdf">
                  July 15-16, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_06_16_18_Minutes.pdf">
                  June 16-18, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_05_20_26_Minutes.pdf">
                  May 20 and 26, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_04_15_Minutes.pdf">
                  April 15, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_03_24-25_Minutes.pdf">
                  March 24-25, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_02_26_Minutes.pdf">
                  February 26, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2020_01_15_Minutes.pdf">
                  January 15, 2020 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2019_12_18_Minutes.pdf">
                  December 18, 2019 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2019_11_13_Minutes.pdf">
                  November 13, 2019 (pdf)
                </a>
              </li>
              <li>
                Abridged Minutes - 
                <a href="boardminutes/2019_10_21_Minutes.pdf">
                  October 21, 2019 (pdf)
                </a>
              </li>
            </ul>

            <p class="margin-0">
              Abridged minutes for older board meetings can be found in the 
              <a href="minutes_archive.php#board">minutes archive</a>.
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="homeitem3col">
      <h2 id="assembly">Eclipse Foundation General Assembly Meetings</h2>
      <ul>
        <li>
          Minutes Meeting of the Annual General Assembly - 
          <a href="generalminutes/2024-04-08-minutes-general.pdf">
            April 8, 2024 (pdf)
          </a>
        </li>
        <li>
          Minutes Meeting of the Extraordinary Meeting of the General Assembly - 
          <a href="generalminutes/2023-07-26-minutes-general.pdf">
            July 26, 2023 (pdf)
          </a>
        </li>
        <li>
          Minutes Meeting of the Annual General Assembly - 
          <a href="generalminutes/2023-04-04-minutes-general.pdf">
            April 4, 2023 (pdf)
          </a>
        </li>
        <li>
          Minutes Meeting of the Annual General Assembly - 
          <a href="generalminutes/2022-07-20-minutes-general.pdf">
            July 20, 2022 (pdf)
          </a>
        </li>
        <li>
          Minutes (2nd) Extraordinary Meeting of the General Assembly - 
          <a href="generalminutes/2022-02-16-minutes-general.pdf">
            February 16, 2022 (pdf)
          </a>
        </li>
        <li>
          Minutes Extraordinary Meeting of the General Assembly - 
          <a href="generalminutes/2021-12-15-minutes-general.pdf">
            December 15, 2021 (pdf)
          </a>
        </li>
        <li>
          Minutes Meeting of the Annual General Assembly - 
          <a href="generalminutes/2021-09-08-minutes-general.pdf">
            September 8, 2021 (pdf)
          </a>
        </li>
        <li>
          Minutes Extraordinary Meeting of the General Assembly - 
          <a href="generalminutes/2021-01-13-minutes-general.pdf">
            January 13, 2021 (pdf)
          </a>
        </li>
      </ul>
    </div>

    <div class="homeitem3col">
      <h2 id="councils">Council Meetings</h2>
      <p>Council Meeting minutes are categorized on the council page.</p>
      <ul>
        <li>
          <a href="https://wiki.eclipse.org/Architecture_Council/Meetings">
            Architecture Council/Meetings
          </a>
        </li>
        <li>
          <a href="https://wiki.eclipse.org/Planning_Council#Meetings">
            Planning Council Meetings
          </a>
        </li>
      </ul>
    </div>

    <div class="homeitem3col">
      <h2 id="workingGroups">Working Groups</h2>
      <p>
        Each Eclipse Foundation Working Group&rsquo;s Steering Committee posts its meeting minutes 
        on its respective working group <a href="https://accounts.eclipse.org/mailing-list">mailing list</a>, 
        each of which is archived.
      </p>
    </div>
  </div>

EOHTML;


  # Generate the web page
  $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
