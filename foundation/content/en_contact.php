<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="midcolumn">

  <h1><?php print $pageTitle;?></h1>
    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">Need to contact someone at the Eclipse Foundation?</div>

      <!-- List group -->
      <ul class="list-group">
        <li class="list-group-item"><strong>File an issue on HelpDesk</strong><br> <a
          href="https://accounts.eclipse.org">Register</a>
          and <a href="https://gitlab.eclipse.org/eclipsefdn/helpdesk">report</a>
          a bug/issue</li>
        <li class="list-group-item"><strong>Status of our infrastructure</strong><br>
          <a href="https://www.eclipsestatus.io/">Eclipse Service Status</a></li>
        <li class="list-group-item"><strong>Technical questions /
            Support (via HTTP)</strong><br> <a
          href="https://accounts.eclipse.org">Register</a>
          and join the <a href="../../forums/">Forums</a></li>
        <li class="list-group-item"><strong>Legal questions</strong><br>
          <i class="fa orange fa-envelope"></i> license (at) eclipse.org</li>
        <li class="list-group-item"><strong>Trademark questions</strong><br>
          <i class="fa orange fa-envelope"></i> trademarks (at)
          eclipse-foundation.org</li>
        <li class="list-group-item"><strong>News, events or announcing a
            new plug-in</strong><br> <i class="fa orange fa-envelope"></i>
          news (at) eclipse.org</li>
        <li class="list-group-item"><strong>Joining the Eclipse
            Foundation</strong><br> <i class="fa orange fa-envelope"></i>
          membership (at) eclipse.org</li>
        <li class="list-group-item"><strong>Updating membership information</strong><br>
          <i class="fa orange fa-envelope"></i> membership.coordination (at) eclipse-foundation.org</li>
        <li class="list-group-item"><strong>A project proposal for
            Eclipse.org</strong><br> <i class="fa orange fa-envelope"></i>
          emo (at) eclipse.org</li>
        <li class="list-group-item"><strong>Eclipse.org infrastructure</strong><br>
          <i class="fa orange fa-envelope"></i> infrastructure (at)
          eclipse.org</li>
        <li class="list-group-item"><strong>Account removals, GDPR issues</strong><br>
          <i class="fa orange fa-envelope"></i> privacy (at)
          eclipse.org</li>
        <li class="list-group-item"><strong>General inquiries</strong><br>
          <i class="fa orange fa-envelope"></i> emo (at) eclipse.org</li>
      </ul>
    </div>

  <div class="row">
    <div class="col-sm-24">
    <h2>Offices</h2>
    <p>The mailing address of our headquarters in Belgium is:</p>
      <p>
        Eclipse Foundation AISBL <br />
        Rond Point Schuman 11 <br />
        Brussels 1040 Belgium
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-24">
    <p>The mailing address of our Canadian office is:</p>
      <p>
        Eclipse Foundation Canada<br />
        2934 Baseline Road, Suite 202 <br />
        Ottawa, Ontario, <br />
        Canada, K2H 1B2
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-24">
    <p>The mailing address of our US office is:</p>
      <p>
        Eclipse.org Foundation, Inc<br />
        2934 Baseline Road, Suite 202 <br />
        Ottawa, Ontario, <br />
        Canada, K2H 1B2
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-24">
    <p>The mailing address of our Eclipse Foundation Europe GmbH subsidiary:</p>
      <p>
        Eclipse Foundation Europe GmbH <br />
        Berliner Allee 47 <br />
        64295 Darmstadt <br />
        Germany<br />
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-24">
    <h2>Reach out on Social Media</h2>
      <ul class="list-inline">
        <li>
          <a class="social-media-link fa-stack fa-lg" href="https://twitter.com/EclipseFdn">
            <i class="fa fa-circle-thin fa-stack-2x"></i>
            <i class="fa fa-twitter fa-stack-1x"></i>
          </a>
        </li>
        <li>
          <a class="social-media-link fa-stack fa-lg" href="https://www.facebook.com/eclipse.org">
            <i class="fa fa-circle-thin fa-stack-2x"></i>
            <i class="fa fa-facebook fa-stack-1x"></i>
          </a>
        </li>
        <li>
          <a class="social-media-link fa-stack fa-lg" href="https://www.youtube.com/user/EclipseFdn">
            <i class="fa fa-circle-thin fa-stack-2x"></i>
            <i class="fa fa-youtube fa-stack-1x"></i>
          </a>
        </li>
        <li>
          <a class="social-media-link fa-stack fa-lg" href="https://www.linkedin.com/company/eclipse-foundation">
            <i class="fa fa-circle-thin fa-stack-2x"></i>
            <i class="fa fa-linkedin fa-stack-1x"></i>
          </a>
        </li>
      </ul>

    </div>
  </div>

</div>
