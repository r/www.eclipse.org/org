<h1><?php print $pageTitle; ?></h1>

<p>
  The 5 board members of Eclipse.org Foundation, Inc. are all appointed by, and
  are representatives of Eclipse Foundation AISBL.
</p>

<?php print $DirectorsList->getBoardDirectorsUS();?>

<p>
  Looking for the Eclipse Foundation AISBL Board of Directors?
  <a href="./directors.php">
    See here.
  </a>
</p>

