<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation)
 */

$list[] = array(
  'src' => '/membership/scripts/get_image.php?id=827&size=small',
  'content' => 'For donating numerous servers towards our build and test infrastructure.',
  'name' => 'Google'
);
$list[] = array(
  'src' => '/membership/scripts/get_image.php?id=656&size=small',
  'content' => 'For the donation of two <a href="http://www-304.ibm.com/shop/americas/content/home/store_IBMPublicUSA/en_US/evp/3550M3.html" target="_blank">IBM x3550</a> servers.',
  'name' => 'IBM'
);
$list[] = array(
  'src' => '/membership/scripts/get_image.php?id=657&size=small',
  'content' => 'For the donation of Intel-brand servers and Xeon server processors.',
  'name' => 'Intel'
);
$list[] = array(
  'src' => '/membership/scripts/get_image.php?id=1196&size=small',
  'content' => 'For the donation of <a href="https://azure.microsoft.com/">Azure Cloud </a> resources.',
  'name' => 'Microsoft'
);

$list[] = array(
  'src' => '/membership/scripts/get_image.php?id=721&size=small',
  'content' => 'For the donation of servers helping power our site.',
  'name' => 'Oracle'
);

$list[] = array(
  'src' => '/membership/scripts/get_image.php?id=731&size=small',
  'content' => 'For the donation of <a href="https://access.redhat.com/products/red-hat-openshift-container-platform">OpenShift Container Platform</a> and RHEL software.',
  'name' => 'Red Hat'
);

$list[] = array(
  'src' => '/org/images/suselogo.png',
  'content' => 'For donating <a href="https://www.suse.com/products/server/">SUSE Linux Enterprise</a> distributions for all of our web, san, infrastructure and database servers.',
  'name' => 'SUSE'
);
?>

<div id="midcolumn">
  <h1><?php print $pageTitle;?></h1>
  <p>
    The Eclipse Foundation would like to thank the following companies for their support:
  </p>
  <?php foreach ($list as $member) : ?>
    <div class="panel panel-default">
      <div class="panel-heading"><?php print $member['name'];?></div>
      <div class="panel-body background-white">
        <img class="img-responsive float-right margin-left-20"src="<?php print $member['src'];?>" alt="<?php print $member['name'];?>" width="140">
        <?php print $member['content'];?>
      </div>
    </div>
  <?php endforeach;?>
</div>