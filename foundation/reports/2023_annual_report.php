<?php
/**
 * Copyright (c) 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "2023 Annual Community Report";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse, foundation, community, annual report, 2023, vision, mission, eclipse vision, eclipse foundation mission, eclipse mission");
$Theme->setPageAuthor("Mike Milinkovich");

$Theme->setNav($Nav);
$Theme->setNavPosition('disabled');
$nav = $Theme->getThemeFile('nav');

ob_start();
include ("content/en_2023_annual_report.php");
$html = ob_get_clean();
$html .= <<<EOHTML

  <div id="rightcolumn" class="container">
    <div class="sideitem">
      <h6 id="a_15">Related Links</h6>
      <ul>
      <li><a href="2022_annual_report.php">2022 Annual Report</a></li>
      <li><a href="2021_annual_report.php">2021 Annual Report</a></li>
      <li><a href="2020_annual_report.php">2020 Annual Report</a></li>
      <li><a href="2019_annual_report.php">2019 Annual Report</a></li>
      <li><a href="2018_annual_report.php">2018 Annual Report</a></li>
      <li><a href="2017_annual_report.php">2017 Annual Report</a></li>
      <li><a href="2016_annual_report.php">2016 Annual Report</a></li>
      <li><a href="2015_annual_report.php">2015 Annual Report</a></li>
      <li><a href="2014_annual_report.php">2014 Annual Report</a></li>
      <li><a href="2013_annual_report.php">2013 Annual Report</a></li>
      <li><a href="2012_annual_report.php">2012 Annual Report</a></li>
      </ul>
    </div>
  </div>
EOHTML;

$Theme->setHtml($html);
$Theme->resetAttributes('main-container', 'class');
$Theme->setThemeVariables(array('main_container_classes' => 'container-fluid'));
$Theme->setExtraHeaders('<link href="annual_report.css" media="screen" rel="stylesheet" type="text/css"/>');
$Theme->generatePage();
