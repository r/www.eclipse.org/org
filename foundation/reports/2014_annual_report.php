<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

	#*****************************************************************************
	#
	# 2014_annual_report.php
	#
	# Author: 		Mike Milinkovich
	# Date:			2014-06-22
	#
	# Description: The 2014 Eclipse Community Annual Report
	#
	#
	#****************************************************************************

	#
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= "2014 Annual Community Report";
	$pageKeywords	= "eclipse, foundation, annual report, 2014";
	$pageAuthor		= "Mike Milinkovich";

	# End: page-specific settings
	#

	# Paste your HTML content between the EOHTML markers!

	# Paste your HTML content between the EOHTML markers!

	$html = file_get_contents('2014_annual_report.html');

  $html .= <<<EOHTML

  <div id="rightcolumn">
    <div class="sideitem">
      <h6 id="a_15">Related Links</h6>
      <ul>
	  <li><a href="2020_annual_report.php">2020 Annual Report</a></li>
      <li><a href="2019_annual_report.php">2019 Annual Report</a></li>
      <li><a href="2018_annual_report.php">2018 Annual Report</a></li>
      <li><a href="2017_annual_report.php">2017 Annual Report</a></li>
      <li><a href="2016_annual_report.php">2016 Annual Report</a></li>
      <li><a href="2015_annual_report.php">2015 Annual Report</a></li>
      <li><a href="2014_annual_report.php">2014 Annual Report</a></li>
      <li><a href="2013_annual_report.php">2013 Annual Report</a></li>
      <li><a href="2012_annual_report.php">2012 Annual Report</a></li>
      </ul>
    </div>
  </div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

