<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
    <title>2021 Annual Eclipse Community Report</title>
    <style>
      .member-logos img {
        margin: 20px;
      }
    </style>
  </head>
  <body lang="EN-US">
    <div id="maincontent">
      <div id="midcolumn">
        <h1>2021 Annual Eclipse Foundation Community Report</h1>

        <p>
          Welcome to the tenth annual Eclipse Foundation Community Report.
          Comments and feedback on the style and content would be appreciated at
          <a href="mailto:emo@eclipse.org">emo@eclipse.org</a>.
        </p>

        <p>
          Except where otherwise noted this report covers the period April 1,
          2020 to March 31, 2021.
        </p>

        <p><strong>TABLE OF CONTENTS</strong></p>

        <ul>
          <li>
            <a href="#a_1"
              >Executive Director&rsquo;s Summary: A Transformative Year</a
            >
          </li>
          <li><a href="#a_2">Who We Are</a></li>
          <li><a href="#a_3">2021 Strategy</a></li>
          <li><a href="#a_4">Key Board Decisions</a></li>
          <li><a href="#a_5">Financials</a></li>
          <li><a href="#a_6">Membership</a></li>
        </ul>

        <h2 id="a_1">
          Executive Director&rsquo;s Summary: <br />
          A Transformative Year
        </h2>

        <p>Welcome to the 2021 Eclipse Foundation Community Report!&nbsp;</p>

        <p>
          I&rsquo;ve had the pleasure of serving the Eclipse community as
          Executive Director since its inception in 2004. Over that time, the
          changes to the open source community at large, the Eclipse community
          in particular, and the Foundation itself have been extraordinary. I am
          thrilled to have had the opportunity to see the growth and evolution
          of the Eclipse community over my 17 years with the Eclipse Foundation.
        </p>

        <p>2020-21 has been a transformative year in almost every aspect.</p>

        <p>
          Certainly, the global pandemic has had a dramatic impact on the
          Foundation as well as all of our 300+ member organizations and our
          thousands of committers and contributors. First and foremost, I extend
          my best wishes to all, and hope that all in our community, and their
          families, have been, and continue to be safe. Also, the pandemic has
          brought to a very quick halt the opportunity for us to meet one
          another at various conferences, meetings, and events. After this long
          hiatus from travel, I look forward to seeing our community members
          again in the not-too-distant future.
        </p>

        <p><strong>Important Project Milestones Achieved</strong></p>

        <p>
          I am proud of the major accomplishments achieved by our community over
          the 2020-21 period. Many of our most visible projects have seen
          important milestones. Notably, the Simultaneous Release of the Eclipse
          IDE and packages continues to evolve; since switching to a cadence of
          quarterly releases in 2018, 12 quarterly releases of the Simultaneous
          Release have been completed on schedule, and with enhanced features
          benefitting the IDE’s millions of users. Jakarta EE 9 was released in
          December 2020, representing a significant milestone for the enterprise
          Java ecosystem. Eclipse SUMO has emerged as the most active Eclipse
          Foundation open source project. There were also major releases in our
          IoT and edge communities, our cloud tools community, and many other
          technical areas.
        </p>

        <p>
          In total, 515 project releases were completed over the past 12 months.
          As of March 2021, the Foundation has stewardship of over 260 million
          lines of code in the Eclipse repositories. The Eclipse Foundation
          codebase is conservatively valued at over $13 billion using the
          industry standard COCOMO model.
        </p>

        <p>
          We have also seen the Eclipse Foundation Specification Process (EFSP)
          being used more widely. In addition to Jakarta EE, it is now adopted
          by the MicroProfile, Sparkplug, and AsciiDoc specification projects.
          Further validation of the EFSP came from the migration of OSGi
          specifications from being overseen by the OSGi Alliance to now being
          housed at Eclipse Foundation and using the EFSP.
        </p>

        <p><strong>Working Groups Continue to Thrive</strong></p>

        <p>
          Working groups continue to play a growing role in how members engage
          at the Eclipse Foundation. In total, we have 17 working groups as of
          March 31, 2021. As a proof point of the importance of these working
          groups in bringing members together to work collaboratively in support
          of Eclipse Foundation projects, more than 140 Member organizations now
          participate in one or more working groups. These working groups are
          enabling new initiatives to flourish.
        </p>

        <p>
          To call out just two new working groups formed in early 2021, the
          Adoptium Working Group has been formed as the successor to
          AdoptOpenJDK, and will play a valuable role in the Java ecosystem
          going forward. The Eclipse IDE Working Group has been formed to
          augment the incredible work done by our Committers on the Eclipse IDE
          and the Simultaneous Release, and to ensure the long-term vibrancy of
          the IDE.
        </p>

        <p><strong>New Initiatives at the Foundation</strong></p>

        <p>
          Lastly with respect to our technical initiatives, many of the
          activities emanating from Eclipse Foundation projects and working
          groups are leading to significant, innovative services being hosted at
          the Foundation as their vendor neutral home. For example, the
          <a href="https://open-vsx.org/">OpenVSX Registry</a> is hosted by the
          Eclipse Cloud Development Tools Working Group, and provides a
          marketplace for VS Code extensions that is operated in a vendor
          neutral, transparent manner. The
          <a href="https://jakarta.ee/compatibility/"
            >Jakarta EE Compatible Products</a
          >
          site provides a comprehensive landing area for industry and is seeing
          widespread use. As open source continues to grow in significance in
          the marketplace, the opportunity for the Foundation to provide
          innovative services through our working groups will continue to
          increase in value.
        </p>

        <p>
          <strong>Eclipse Foundation as a European-based Association</strong>
        </p>

        <p>
          For the Foundation and our members, a big part of our year has been in
          redomiciling the Foundation as a European-based, international
          nonprofit association incorporated in Belgium. This shift reflects and
          capitalizes on our unique position as the preeminent European-based
          open source community with global reach. This pivot of our corporate
          structure has been a major lift in terms of both effort and positive
          impact — and one that we see as being beneficial to all our projects
          and members over the coming years.
        </p>

        <p>
          In particular, I want to call out the members of the Eclipse
          Foundation Board for the many dedicated hours they put into this
          initiative, as well as the organizations who led this initiative, be
          it with financial, legal, or community outreach support. This
          redomiciling has been a unique project, one of which I’m proud and
          from which I have certainly enjoyed the learning journey. More details
          about the redomiciling are available
          <a href="/europe">here</a>.
        </p>

        <p>
          In conjunction with redomiciling, the Board and Membership at Large
          approved updates to the Eclipse Bylaws in the fall of 2020. These
          changes better align the Foundation with European-based not-for-profit
          practices, as well as streamline our membership structure. Of note, we
          now have four classes of membership (vs. the six we had previously),
          where the predominance of our corporate members are now in the
          Contributing Member class. What has been retained is the commitment to
          all Committers being welcome and encouraged to be Members of the
          Foundation. This direct, active involvement of our Committers in our
          governance is fairly unique to Eclipse Foundation, and better ensures
          that matters that are important to committers remain front and center
          in governance decisions. The Board further approved updates to other
          policies, notably the Eclipse Foundation Intellectual Property and
          Antitrust policies. All our governance documents are available on our
          <a href="/org/documents">governance page</a>.
        </p>

        <p><strong>A New Approach to This Report</strong></p>
        <p>
          We have decided to condense the format of this year’s Annual Community
          Report. In year’s past, this report served as an encyclopedic update
          on all things happening at the Eclipse Foundation. While a valuable
          reference document, it seemed like the wrong forum to try and capture
          such depth in detail and likely not as valuable to most readers. This
          year, we are using this publication as a means to provide our
          community members with a more succinct and (hopefully!) useful summary
          that you can, in turn, use to communicate to your stakeholders about
          what’s happened at the Foundation. Of course, you can continue to rely
          on our many other marketing and communications channels to understand
          what lies ahead.
        </p>

        <p>
          Enjoy the report — I hope you find it useful! As always, we welcome
          your comments and feedback. Let us know your thoughts at
          <a href="mailto:emo@eclipse.org">emo@eclipse.org</a>
          or on Twitter
          <a href="https://twitter.com/EclipseFdn">@EclipseFdn</a>.
        </p>

        <h2 id="a_2">Who We Are</h2>
        <p>The Eclipse Foundation’s mission is summarized as follows:</p>
        <p style="margin-left: 40px">
          <em
            >The Eclipse Foundation’s purpose is to advance our open source
            software projects and to cultivate their communities and business
            ecosystems.</em
          >
        </p>

        <p>
          The dedication to this purpose makes the Eclipse community a unique
          open source community. We believe the Foundation’s ongoing success
          comes from this unique combination of a consistent interest in
          building open source code and community, and a sustained commitment to
          creating a commercially successful ecosystem around that code to the
          benefit of our community.
        </p>
        <p>Our vision for the Eclipse community is:</p>
        <p style="margin-left: 40px">
          <em
            >To be the leading community for individuals and organizations to
            collaborate on commercially friendly open source.</em
          >
        </p>

        <h2 id="a_3">2021 Strategy</h2>
        <p>
          As established by the Board of Directors, the strategic goals of the
          Eclipse Foundation for 2021 represent a continued drive towards
          meeting our vision for the Eclipse community.
        </p>
        <ol>
          <li>
            <strong
              >Communicate the Eclipse Foundation’s differentiated value
              proposition.</strong
            >
            The Foundation continues to flourish, and to occupy a unique
            position in the open source world. The Eclipse IDE and the
            Simultaneous Release continue to be a flagship set of projects, and
            the technologies delivered by those projects continue to be used by
            literally millions of developers worldwide. Indeed, new innovations
            are continuously added by our developer community, and many of our
            members recently came together to form the Eclipse IDE Working Group
            in support of the IDE.
            <br />
            <br />
            While the Eclipse IDE is well known globally, the Eclipse community
            is much larger, broader, and richer than the IDE. We are stewards of
            approximately 400 open source projects, and the significance of many
            of these projects and their importance in the overall open source
            marketplace continues to grow. Today, the value proposition of
            bringing new projects to the Eclipse Foundation, and the ongoing
            evolution of those projects under the Foundation’s stewardship, is
            more compelling than ever. We continue to offer strong intellectual
            property management services for all projects. We also offer
            infrastructure to our projects that better ensure long-term
            viability and vendor-neutrality, both of which are of strategic
            importance to our members. We also provide a much richer set of
            services around marketing and communications, telling the stories of
            our projects and our members in a way that is of great value to
            both. Overall, the Foundation’s clear differentiator is its focus on
            commercial-friendly open source, and the fact that our antitrust, IP
            policies, and governance structure all drive benefit to our
            projects, members, and community.
          </li>
          <br />
          <li>
            <strong
              >Establish the Eclipse Foundation as the preeminent European OSS
              organization.</strong
            >
            The redomiciling of the Eclipse Foundation as a European-based,
            international not-for-profit association has immediately positioned
            the organization as the largest European-based open source
            foundation when measured by projects, members, or committers. Now
            that the governance transition is complete, we are actively working
            as part of many major European open source initiatives that will
            have global reach in terms of their impact. We have been pleased to
            see the positive engagement thus far by so many of our Members,
            including both European-based Members and others from around the
            globe.
          </li>
          <br />
          <li>
            <strong
              >Promote working groups as an effective model for open governance,
              collaboration, marketing, and industry adoption.</strong
            >
            The Eclipse Foundation’s Working Group model, based on the Eclipse
            Foundation Working Group Process, has evolved into an effective,
            lightweight governance structure that enables member organizations
            to come together to drive shared interests in Eclipse projects and
            promotion of Eclipse technologies and open specifications. A primary
            focus of the Foundation staff continues to be on enabling both new
            and existing members to effectively engage and collaborate using
            this lightweight governance structure to carry out meaningful
            activities that drive adoption of our projects, promote
            participation in those projects, and generally drive the business
            value related to those open source projects.
          </li>
          <br />
          <li>
            <strong
              >Attract and foster new projects, specifications, and working
              groups focused on emerging technologies.</strong
            >
            As open source continues to become ever more strategic to our member
            organizations, the Eclipse Foundation continues to cultivate and
            onboard strategic and emerging industrial and commercial
            technologies. Building on our open specifications process, we
            continue to put a significant emphasis on attracting new open
            specification initiatives, and now have five working groups engaged
            in publishing open specifications under the Eclipse Foundation
            Specification Process.
          </li>
          <br />
          <li>
            <strong
              >Cultivate the growth of our existing projects, communities,
              working groups, and ecosystems.</strong
            >
            Continuing the overall growth and vibrancy of the Eclipse community
            is a constant focus and a key differentiator for the Foundation in
            the marketplace. The growth of the community comes from broader
            adoption of the technologies and enabling new ways for individuals
            and organizations to participate. The Foundation now conducts or
            facilitates dozens of virtual community events of all sizes. This
            model has allowed for each individual Eclipse project community, be
            it large or small, localized or regionally diverse, to extend its
            reach. We have also introduced a variety of marketing and
            communications channels, each of which is aimed at enabling more
            people to learn about Eclipse technologies, to more effectively
            adopt those technologies, and to participate in the community. Our
            working groups in support of cloud native Java, IoT, edge computing,
            automotive, etc. further advance this cultivation of growth.
          </li>
          <br />
          <li>
            <strong>
              Continuously increase value for all its membership
              classes.</strong
            >
            As part of the Foundation’s constant drive to enhance member value,
            we recently introduced new initiatives to focus more on the economic
            benefit of participation. We are launching a new program to
            highlight the benefits of participation to entrepreneurs and the
            entrepreneurial-based initiatives of our Members, providing insight
            and guidance on leveraging the Foundation’s unique characteristics
            that benefit these initiatives. We have also started hosting our own
            GitLab service. Based in Europe, this service provides valuable
            choices for hosting projects and tooling that are appealing to many
            Members, notably to our Committers, who form a critical class of
            membership at the Foundation that is quite unique and uniquely
            beneficial to our ecosystem. And we are launching other initiatives
            to better enable all of our own members to highlight, via their own
            channels, the overall benefit of participation in the Eclipse
            Foundation to their stakeholders.
          </li>
          <br />
          <li>
            <strong
              >Increase and diversify our membership, contributors, and revenue
              sources.</strong
            >
            Aligned with the six strategic goals listed above is an explicit
            goal to increase and diversify our revenue sources, as well as to
            continually diversify our membership. In 2020-2021, led by
            participation in Jakarta EE, we have seen an increase in the number
            of Asia-based organizations that have become members, as well as an
            increase in participation of Java User Groups. Both of these
            represent a broadening of Jakarta’s global community, and impact.
            Our overall revenue growth continues to be driven largely by the
            creation of, and participation in, working groups. We expect this
            trend to continue for the foreseeable future.
          </li>
        </ol>

        <h2 id="a_4">Key Board Decisions</h2>
        <p>
          Over the past year, the Board has made a number of strategic decisions
          designed to enable the Foundation to achieve its objectives and
          fulfill its mission.
        </p>
        <ul>
          <li>
            <strong
              >Approval of steps to support the Foundation’s international
              strategy.</strong
            >
            In March 2020, the Board took the strategic decision to redomicile
            the Foundation as a European organization. Throughout 2020/21, the
            Board executed on this strategy, culminating in the creation in
            November 2020 of Eclipse Foundation AISBL, an international
            not-for-profit association based in Brussels. The Board further
            approved the creation of Eclipse Foundation Canada as a means of
            streamlining the Foundation’s operations, recognizing that the
            majority of the Foundation’s staff are based in Canada. All of these
            changes were made without an impact to overall membership revenues,
            with a number of Members that were supportive of the move
            contributing additional funds to support the initiative, and further
            showing their support for the strategy. More information regarding
            the details of the Foundation’s international strategy can be found
            at
            <a href="https://www.eclipse.org/europe/"
              >https://www.eclipse.org/europe/</a
            >.
          </li>
          <br />
          <li>
            <strong> Adoption of new bylaws.</strong>
            In support of the redomiciling of the Foundation as a Belgian-based
            organization, the Board approved new bylaws and associated
            membership agreement. The bylaws have been modernized to meet the
            needs of the growing and evolving Eclipse community. The new bylaws
            have streamlined the classes of membership from six classes to four
            classes: Strategic, Contributing, Associate, and Committer.
          </li>
          <br />
          <li>
            <strong
              >Approval to enable the Foundation to enter into an OpenJDK
              Community TCK License with Oracle.
            </strong>
            The Board approved the Foundation to enter into an agreement with
            Oracle that will enable the Eclipse Foundation to host and
            distribute Java SE compatible distributions of OpenJDK. This
            decision paved the way for the formation of the Adoptium Working
            Group in support of this initiative. By doing so, the Foundation
            continues to play an ever-important role in the open source Java
            community.
          </li>
          <br />
          <li>
            <strong
              >Approval to enable the OSGi Alliance to contribute its OSGi
              Specifications to the Eclipse Foundation and for the Foundation to
              become the successor organization of OSGi Alliance.</strong
            >
            The Board granted approval to enable the OSGi Alliance to contribute
            its specifications under the Eclipse Foundation Specification
            Process, and for the Foundation to be named as the successor
            organization of OSGi Alliance. This action benefits our Members who
            support the OSGi specifications, and ensures the long-term viability
            of those specifications.
          </li>
          <br />
          <li>
            <strong>Updates to the Eclipse Code of Conduct.</strong>
            As part of its ongoing drive to continually foster an open and
            welcoming community, the Board updated the Eclipse Community Code of
            Conduct to better match the Foundation’s project and community
            structures.
          </li>
          <br />
          <li>
            <strong
              >Restatement of Eclipse Foundation membership fees to be in
              Euros.</strong
            >
            In further support of the redomiciling as a European-based
            organization, the Foundation took the formal step in October, 2020
            to have its fees be stated in Euros rather than U.S. dollars.
          </li>
          <br />
          <li>
            <strong>Approval of OpenVSX Registry.</strong>
            The Board passed a number of resolutions that together have enabled
            the creation and operation of the OpenVSX Registry, including
            allowing the registry to distribute hosted content under a broad set
            of open source licenses.
          </li>
        </ul>

        <h2 id="a_5">Financials</h2>

        <p>
          The Eclipse Foundation’s fiscal year end is December 31. Our auditors
          are the firm Deloitte & Touche, LLP. Our operating headquarters is
          located in Ottawa, Canada, with the remainder of our team based in
          various European countries and the U.S. The Eclipse Foundation is now
          comprised of four organizations:
        </p>
        <ul>
          <li>
            Eclipse Foundation AISBL is an international not-for-profit
            association based in Brussels
          </li>
          <li>
            Eclipse.org Foundation, Inc. is incorporated in the State of
            Delaware, USA as a 501(c)6 not-for-profit
          </li>
          <li>
            Eclipse Foundation Canada is a Canadian nonprofit member-based
            organization incorporated under the laws of Canada
          </li>
          <li>
            Eclipse Foundation Europe GmbH is a wholly-owned Germany subsidiary.
          </li>
        </ul>

        <p>
          The Board approved the 2020 audited financial statements of the
          Foundation at its April 21, 2021 Board meeting.
        </p>
        <p>
          The Foundation continues to operate on a solid financial footing while
          continuing to grow. Despite the challenge created by the global
          COVID-19 pandemic, membership renewals remained strong, including the
          renewal of all Strategic Members. The Foundation also welcomed Daimler
          DSS as a Strategic Member, further demonstrating the importance of the
          Foundation to the automotive industry in general.
        </p>
        <p>
          Working group revenue grew in 2020, notably with the introduction of
          the Eclipse Cloud Development Tools working group. Overall, the
          Foundation has grown to 17 working groups, with each generating
          revenues to meet the specific objectives of the working group, and
          overall strengthening the Foundation’s overall ability to drive member
          value through its initiatives.
        </p>
        <p>
          Of note, the formation of the Adoptium Working Group and the Eclipse
          IDE Working Group in early 2021 both include long-term financial
          commitments from members to the working group model. Looking ahead to
          the full 2021 fiscal year, the Board approved a cash-neutral budget
          that called for an increase of revenues to $7.3M, forecasting a
          deficit of $0.3M. The forecast approved by the Board is considered
          conservative, based on the ongoing uncertainty surrounding the global
          pandemic and its impact on membership.
        </p>
        <p>
          As a matter of governance, management provides quarterly updates to
          the Board’s finance committee. Further, all Members are kept up to
          date regarding the Foundation’s budget through quarterly reports given
          through its Member Newsletter publications.
        </p>

        <p><strong>Eclipse Foundation Income and Expenses, by Year</strong></p>

        <table class="table table-stripped">
          <thead>
            <tr>
              <th>
                <p>In USD millions</p>
              </th>
              <th>
                <p>2018</p>
              </th>
              <th>
                <p>2019</p>
              </th>
              <th>
                <p>2020</p>
              </th>
              <th>
                <p>2021 Budget</p>
              </th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <p>Revenue</p>
              </td>
              <td>
                <p>5.9</p>
              </td>
              <td>
                <p>5.7</p>
              </td>
              <td>
                <p>6.7</p>
              </td>
              <td>
                <p>7.3</p>
              </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                <p>Expenses</p>
              </td>
              <td>
                <p>6.2</p>
              </td>
              <td>
                <p>6.2</p>
              </td>
              <td>
                <p>6.6</p>
              </td>
              <td>
                <p>7.6</p>
              </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                <p>Net Income</p>
              </td>
              <td>
                <p>(0.3)</p>
              </td>
              <td>
                <p>(0.5)</p>
              </td>
              <td>
                <p>0.1</p>
              </td>
              <td>
                <p>(0.3)</p>
              </td>
              <td>&nbsp;</td>
            </tr>
          </tbody>
        </table>

        <h2 id="a_6">Membership</h2>
        <p>
          As of March 31, 2021, the Eclipse Foundation has 14 strategic members.
        </p>
        <div class="member-logos">
          <img
            src="/org/foundation/reports/images/logos/bosch.png"
            alt="Bosch logo"
          />
          <img
            src="/org/foundation/reports/images/logos/list.jpg"
            alt="list ceatech logo"
          />
          <img
            src="/org/foundation/reports/images/logos/daimler_tss.png"
            alt="Daimler TSS logo"
          />
          <img
            src="/org/foundation/reports/images/logos/dlr.png"
            alt="DLR logo"
          />
          <img
            src="/org/foundation/reports/images/logos/fujitsu.png"
            alt="FUJITSU logo"
          />
          <img
            src="/org/foundation/reports/images/logos/fraunhofer_fokus.png"
            alt="Fraunhofer FOKUS logo"
          />
          <img
            src="/org/foundation/reports/images/logos/huawei.png"
            width="100"
            alt="HUAWEI logo"
          />
          <img
            src="/org/foundation/reports/images/logos/ibm.png"
            alt="IBM logo"
          />
          <img
            src="/org/foundation/reports/images/logos/iota.png"
            alt="IOTA logo"
          />
          <img
            src="/org/foundation/reports/images/logos/konduit.png"
            alt="KONDUIT logo"
          />
          <img
            src="/org/foundation/reports/images/logos/obeo.png"
            alt="OBEO logo"
          />
          <img
            src="/org/foundation/reports/images/logos/oracle.png"
            alt="ORACLE logo"
          />
          <img
            src="/org/foundation/reports/images/logos/red_hat.png"
            alt="Red Hat logo"
          />
          <img
            src="/org/foundation/reports/images/logos/sap.png"
            alt="SAP logo"
          />
        </div>
        <p>
          Of note, the Eclipse Foundation also counts over 1,694 committers.
          Committers are entitled to membership in the Foundation, and play a
          valuable role in Eclipse Foundation governance, including
          representation on the Eclipse Board and on many working group steering
          committees.
        </p>
        <p>
          As of March 31, 2021, the Foundation counts 341 organizations as
          members. A total of 58 new companies joined as new members of the
          Foundation from April 1, 2020 through March 31, 2021.
        </p>
        <p>
          As proof of the continued importance of Eclipse working groups to our
          membership, fully 93% of our Strategic Members participate in one or
          more working groups, as do approximately 70% of Contributing Members.
          Further, approximately 50% of new members that joined in 2020 did so
          as a direct result of their involvement in one or more Eclipse
          Foundation working groups. We believe this is proof that engagement in
          working groups continues to be a significant value proposition for
          participation in Eclipse Foundation membership.
        </p>
        <p>
          The Foundation has continued its relationship with OpenHW Group, the
          Canadian-based open hardware nonprofit organization dedicated to
          fostering collaboration among global hardware and software designers
          in the development of open source cores, related IP, tools, and
          software. All OpenHW Group Platinum, Gold, and Silver members are also
          Contributing Members of the Eclipse Foundation, and as of March, 2021,
          this amounted to 49 Contributing Members.
        </p>
        <p>
          A full list of our members can be seen on our
          <a href="membership/exploreMembership.php#allmembers"
            >Explore Our Members</a
          >
          page.
        </p>

        <p><strong>New Members of the Eclipse Foundation</strong></p>
        <p>
          The new members that have joined the Eclipse Foundation between April
          2020 and March 2021 include:
        </p>
        <img
          style="width: 100%; margin-bottom: 40px"
          src="/org/foundation/reports/images/2021/new-members.jpg"
          alt="Bosch logo"
        />
      </div>
      <hr />
      <p style="color: #656668; font-size: 12px; margin-bottom: 40px">
        © 2021 Eclipse Foundation - 2021 Annual Community Report
      </p>
    </div>
  </body>
</html>
