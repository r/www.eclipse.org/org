<?php

/**
 * Copyright (c) 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div class="annual-report-2024" id="maincontent">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-18">
          <h1 class="margin-bottom-20">
            <?php print $pageTitle; ?>
          </h1>
          <p>
            Except where otherwise noted, this report covers the period 1 April
            2023 to 31 March 2024.
          </p>
          <nav class="table-of-contents table-of-contents-block margin-bottom-40">
            <ul>
              <li>
                <a class="link-unstyled" href="#director-summary">Executive Director's Summary</a>
              </li>
              <li>
                <a class="link-unstyled" href="#who-we-are">Who We Are</a>
              </li>
              <li>
                <a class="link-unstyled" href="#key-initiatives">Key Initiatives</a>
              </li>
              <li>
                <a class="link-unstyled" href="#projects">Projects</a>
              </li>
              <li>
                <a class="link-unstyled" href="#industry-collaborations">Industry Collaborations</a>
              </li>
              <li>
                <a class="link-unstyled" href="#membership">Membership</a>
              </li>
              <li>
                <a class="link-unstyled" href="#operations">Operations</a>
              </li>
            </ul>
          </nav>
          <section id="director-summary" class="row">
            <div class="container">
              <div class="director-summary-content">
                <div class="director-summary-image">
                  <img src="images/2024/mike-milinkovich.jpg" alt="Mike Milinkovich" />
                </div>
                <div class="director-summary-content-body">
                  <h2 class="padding-bottom-30 margin-top-0">Executive Director&rsquo;s Summary</h2>
                  <p>
                    As we celebrate the 20th anniversary of the Eclipse
                    Foundation, I am filled with gratitude and admiration for
                    the vibrant community that has been instrumental in our
                    journey. Your unwavering dedication and support have been
                    the cornerstone of our success, and as we reflect on the
                    past two decades, it is clear that our collective
                    achievements are a testament to the power of collaboration
                    and shared vision.
                  </p>
                  <p>
                    This year, we proudly mark two decades of innovation,
                    growth, and community-driven progress. Our anniversary is
                    not just a milestone, but a celebration of the people who
                    have made this journey possible &mdash; our committers,
                    contributors, members, and staff who have contributed their
                    time, expertise, and passion to the Foundation's mission.
                  </p>
                  <p>
                    In this report, we are excited to highlight the significant
                    strides we have made in expanding our presence and
                    initiatives in Europe. Our European community has shown
                    remarkable growth, embodying the Foundation's commitment to
                    fostering global innovation and collaboration.
                  </p>
                  <p>
                    This year also marked a pivotal moment for the Foundation
                    as we ventured into the realm of policy advocacy,
                    influenced by the Cyber Resilience Act. This unexpected yet
                    crucial engagement underscores our commitment to being at
                    the forefront of addressing critical issues that impact the
                    open source ecosystem and the wider software industry. Our
                    involvement in policy advocacy is a new chapter in our
                    mission, reflecting our dedication to championing the
                    interests and sustainability of the open source community.
                  </p>
                  <p>
                    As we celebrate this landmark anniversary and look to the future, I
                    extend my deepest gratitude to each and every past and present
                    member of our community. Thank you for being an integral part of
                    our journey. Here's to celebrating our past achievements and to the
                    exciting opportunites that lie ahead. Together, we will continue to
                    innovate, collaborate, and shape the future of open source.
                  </p>
                  <p>
                    I hope you find this annual report useful and informative. As
                    always, we welcome input from all our community. Let us know your
                    thoughts at <a href="mailto:emo@eclipse.org">emo@eclipse.org</a>
                    or on X <a href="https://x.com/EclipseFdn">@EclipseFdn</a>.
                  </p>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div class="col-sm-6 main-col-sidebar-nav hidden-xs hidden-sm">
          <?php print $nav; ?>
        </div>
      </div>
    </div>
  </div>

  <section id="who-we-are" class="row padding-top-40 padding-bottom-60">
    <div class="container">
      <h2 class="padding-bottom-15">Who We Are</h2>

      <p>
        The long-term success of the foundation is driven by our dedication to
        nurturing open source projects and communities while fostering
        commercially viable ecosystems around these projects. Our core mission
        focuses on:
      </p>
      <ul>
        <li>
          Ensuring user freedoms by providing vendor neutral governance and
          stewardship of our projects;
        </li>
        <li>
          Empowering developers and their communities with programs,
          infrastructure, and events; and
        </li>
        <li>
          Enabling collaboration through our projects and industry collaborations.
        </li>
      </ul>
      <p>
        The Eclipse Foundation is led by our <a href="/org/foundation/staff.php">executive team</a> and
        the <a href="/org/foundation/directors.php">board of directors</a>.
      </p>
      <ul class="grid-gallery grid-gallery-square">
        <li class="link-unstyled cell-row-span-2">
          <figure class="head-figure">
            <img src="./images/2024/mike-milinkovich-2.jpg" alt="">
            <figcaption>
              Mike Milinkovich &mdash; Executive Director
            </figcaption>
          </figure>
        </li>
        <li class="link-unstyled">
          <figure class="head-figure">
            <img src="./images/2024/gael-blondelle.jpg" alt="">
            <figcaption>
              Gaël Blondelle &mdash; Chief Membership Officer
            </figcaption>
          </figure>
        </li>
        <li class="link-unstyled">
          <figure class="head-figure">
            <img src="./images/2024/paul-buck.jpg" alt="">
            <figcaption>
              Paul Buck &mdash; VP, Community Initiatives
            </figcaption>
          </figure>
        </li>
        <li class="link-unstyled">
          <figure class="head-figure">
            <img src="./images/2024/dennis-leung.jpg" alt="">
            <figcaption>
              Dennis Leung &mdash; VP, Program Development 
            </figcaption>
          </figure>
        </li>
        <li class="link-unstyled">
          <figure class="head-figure">
            <img src="./images/2024/thabang-mashologu.jpg" alt="">
            <figcaption>
              Thabang Mashologu &mdash; VP, Community &amp; Outreach
            </figcaption>
          </figure>
        </li>
        <li class="link-unstyled">
          <figure class="head-figure">
            <img src="./images/2024/michael-plagge.jpg" alt="">
            <figcaption>
              Michael Plagge &mdash; VP, Ecosystem Development
            </figcaption>
          </figure>
        </li>
        <li class="link-unstyled">
          <figure class="head-figure">
            <img src="./images/2024/paul-white.jpg" alt="">
            <figcaption>
              Paul White &mdash; Secretary / Treasurer
            </figcaption>
          </figure>
        </li>
      </ul>
  </section>

  <section id="key-initiatives" class="row position-relative padding-top-0 padding-bottom-0">
    <div class="full-background initiatives">
      <div class="container">
        <h2 class="padding-bottom-15">Key Initiatives in 2023-24</h2>
        <p>
          Over the past year, the Eclipse Foundation together with its
          community has achieved great things. None of these achievements would
          be possible without the support of our members.
        </p>
        <ol class="numbered-sections">
          <li>
            <p>
              Digital sovereignty remains at the core of initiatives for
              Europe's digital innovation, positioning the Eclipse Foundation
              as a pivotal player in this field. In 2023-24, we reinforced our
              position as the main European steward for open source of key
              projects contributing to European digital sovereignty; we created
              the <a href="https://dataspace.eclipse.org/">Eclipse Dataspace Working Group</a>,
              and <a href="https://eclipse-tractusx.github.io/">Tractus-X</a>
              was integrated into Catena-X that went into
              production in October.
            </p>
          </li>
          <li>
            <p>
              The Eclipse Foundation made significant strides in enhancing the
              security measures and safeguarding the supply chain of its
              projects, supported by OpenSSF Alpha-Omega project funding. Key
              initiatives included strengthening IT infrastructure, widespread
              adoption of two-factor authentication, refining vulnerability
              management processes, and upgrading development infrastructure
              with secure code signing. The Foundation also introduced OtterDog
              for efficient repository management, conducted security audits to
              foster a culture of security, and positioned <a href="https://adoptium.net/temurin/">Eclipse Temurin</a>
              to achieve SLSA Level 3 compliance.
            </p>
          </li>
          <li>
            <div>
              <p>
                Since the Eclipse Foundation's inception in 2004, events have
                played a key role in our community development;
                <a href="https://opencode-x.org/">Open Community Experience 2024</a>
                is set to continue this tradition, promising to be an
                industry-leading conference for our vibrant community of
                communities.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/ocx-logo.png" alt="Open Community Experience" width="200">
            </div>
          </li>
          <li>
            <div>
              <p>
                <a href="https://sdv.eclipse.org">Eclipse Software Defined Vehicle</a>
                has continued to drive forward automotive open source with 44
                members and 27 projects onboarded. The inaugural SDV hackathon,
                quarterly community days, and the annual Automotive Open Source
                Summit demonstrate the growing engagement and vibrancy of the
                SDV community.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/sdv-logo.png" alt="Software Defined Vehicle" width="250">
            </div>
          </li>
          <li>
            <div class="width-full">
              <p>
                <a href="https://jakarta.ee/specifications/platform/11/">Jakarta EE 11</a>'s
                upcoming release demonstrates the ongoing commitment to
                addressing the evolving needs of enterprises.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/jakarta-ee-logo.png" alt="Jakarta EE 11" width="250">
            </div>
          </li>
          <li>
            <div>
              <p>
                Adoptium's <a href="https://adoptium.net/temurin/">Eclipse Temurin</a>'s
                rise to our most downloaded project, with more than 20 million
                downloads per month, underscores our sustained momentum in
                Java.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/temurin-logo.png" alt="Eclipse Temurin by Adoptium" width="140">
            </div>
          </li>
          <li>
            <div>
              <p>
                The <a href="https://eclipseide.org/">Eclipse IDE</a> working
                group continued its support of the quarterly release drumbeat,
                membership grew, and a Platinum membership level was added.
                Their development effort program, in partnership with the IDE
                Planning Council, continued to fund important enhancements and
                release engineering support for the IDE.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/eclipse-ide-logo.png" alt="Eclipse IDE" width="80">
            </div>
          </li>
          <li>
            <div class="width-full">
              <p>
                We released <a href="https://sparkplug.eclipse.org/specification/">Sparkplug 3.0</a>,
                and has been transposed to now be an ISO international
                standard.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/sparkplug-logo.png" alt="Sparkplug" width="200">
            </div>
          </li>
          <li>
            <div>
              <p>
                <a href="https://threadx.io/">ThreadX</a> was contributed to
                the Eclipse Foundation and is set to become the world's first
                open source real-time operating system, boasting certification
                for functional safety and security.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/threadx-logo.png" alt="Eclipse ThreadX" width="200">
            </div>
          </li>
          <li>
            <div>
              <p>
                In our commitment to fostering good governance in the adoption
                of open source within industry and the public sector, we
                maintain a pivotal role in the initiative now known as the
                <a href="https://ospo-alliance.org/">OSPO Alliance</a>.
                This year, we significantly increased our investment in this
                initiative to further support this community. The Good
                Governance Handbook has been enhanced to be more accessible,
                including translation into six languages. Additionally, we have
                introduced a new public forum to broaden our community's reach.
              </p>
              <img class="img img-responsive margin-auto" src="./images/2024/ospo-alliance-logo.svg" alt="Eclipse ThreadX" width="200">
            </div>
          </li>
          <li>
            <div>
              <p>
                <a href="https://www.eclipse.org/research/">Research at the Eclipse Foundation</a>
                continues to grow with seven new research projects won in the
                following topics:
              </p>
              <ul>
                <li>Secure Computing Continuum (IoT, Edge, Cloud, Dataspaces)</li>
                <li>
                  Cognitive Computing Continuum: Intelligence and automation
                  for more efficient data processing (AI, data and robotics
                  partnership)
                </li>
                <li>
                  Coordination of the European software-defined vehicle
                  platform
                </li>
                <li>
                  Topic on Hardware abstraction layer for a European Vehicle
                  Operating System
                </li>
                <li>
                  Integration of data life cycle, architectures and standards
                  for complex data cycles and/or human factors, language (AI,
                  data and robotics partnership)
                </li>
                <li>Tackling European skills and labour shortages</li>
              </ul>
              <p class="margin-top-30">
                Thus far in 2024, the foundation has been invited to
                participate in 48 research project proposals, which resulted in
                31 proposal submissions.
              </p>
            </div>
          </li>
        </ol>
      </div>
    </div>
  </section>

  <section class="row padding-y-40" id="key-numbers">
    <div class="container text-white text-center">
      <div class="col-xs-24">
        <h2 class="margin-bottom-30">Key Numbers</h2>
      </div>
      <div class="col-sm-8 margin-bottom-30">
        <div class="key-number-tile bg-tertiary match-height-item-by-row">
          <div class="key-number">15</div> hosted events with close to 14,000 attendees
        </div>
      </div>
      <div class="col-sm-8 margin-bottom-30">
        <div class="key-number-tile bg-tertiary match-height-item-by-row">
          <div class="key-number">100,000+</div> developers informed through
          our <a class="multiply-link" href="https://newsroom.eclipse.org/eclipse-newsletter">community newsletter</a>
        </div>
      </div>
      <div class="col-sm-8 margin-bottom-30">
        <div class="key-number-tile bg-tertiary match-height-item-by-row">
          <div class="key-number">4</div> key industry studies
        </div>
      </div>
    </div>
  </section>

  <section id="projects" class="row padding-top-40 padding-bottom-40">
    <div class="full-background projects">
      <div class="container position-relative">
        <h2 class="padding-bottom-15">Projects</h2>

        <p>
          The Eclipse community continues to be engaged in the <a href="https://projects.eclipse.org/">projects</a> 
          and initiatives that impact their respective communities.
        </p>

        <div class="row row-no-gutters padding-top-20 padding-bottom-40">
          <div class="col-xs-6 col-sm-3">
            <div class="bg-secondary box-shadow content-center">
              <p class="h2 text-white"><b>30</b></p>
            </div>
            <div class="text-center padding-top-20">
              <p>new projects in the past year</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="bg-secondary box-shadow content-center">
              <p class="h2 text-white"><b>419</b></p>
            </div>
            <div class="text-center padding-top-20">
              <p>Eclipse Foundation projects overall</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="bg-secondary box-shadow content-center">
              <p class="h2 text-white"><b>14</b></p>
            </div>
            <div class="text-center padding-top-20">
              <p>European research projects supported</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-sm-offset-1">
            <div class="bg-secondary box-shadow content-center">
              <p class="h2 text-white"><b>2,000+</b></p>
            </div>
            <div class="text-center padding-top-20">
              <p>committers</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="bg-secondary box-shadow content-center">
              <p class="h2 text-white"><b>117</b></p>
            </div>
            <div class="text-center padding-top-20">
              <p>organisations participating in commits</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="bg-secondary box-shadow content-center">
              <p class="h2 text-white"><b>495M</b></p>
            </div>
            <div class="text-center padding-top-20">
              <p>lines of code</p>
            </div>
          </div>

          <div class="col-xs-24">
            <p>
              The Eclipse Foundation's open source projects have experienced a
              remarkable year, showcasing significant achievements and
              advancements. OpenVSX, a vibrant marketplace for VS Code
              extensions, has become a hub for developers seeking reliable and
              community-vetted tools, fostering innovation and collaboration
              within the ecosystem. Eclipse Temurin has established itself as a
              cornerstone in the Java ecosystem, with its widespread adoption
              underscoring its reliability and excellence. Meanwhile, ThreadX
              is breaking new ground as the world's inaugural safety-certified
              open source real-time operating system.
            </p>
            <p>
              As is our regular practice, we also consolidated and/or
              terminated 25 open source projects. This includes the Eclipse SOA
              top-level project, which was terminated, and the PMC disbanded
              after we moved the remaining two viable subprojects to the
              purview of other existing top-level projects. On behalf of the
              community, the EMO thanks the SOA PMC for their service. By
              consolidating, especially with more mature Eclipse projects, we
              ensure that both the committers of those projects, as well as the
              downstream users of those projects are better served. Terminating
              projects that are no longer viable or active helps preserve the
              Eclipse “brand” and, more importantly, puts focus on the many
              exceptional projects under the Eclipse umbrella.
            </p>
            <p>
              In this same period, we received proposals for 30 new open source
              projects, covering a broad range of technology areas.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="industry-collaborations" class="row padding-top-40 padding-bottom-40">
    <div class="container">
      <h2 class="padding-bottom-15">Industry Collaborations</h2>
      <p>
        In addition to our flagship Eclipse projects, our Eclipse Foundation
        collaborations continue to play an extremely impactful role in how our
        members engage and contribute!
      </p>
      <p>As of 31 March 2024, we have:</p>
      <div class="row padding-top-30 padding-bottom-60">

        <div class="col-xs-24 margin-bottom-30">
          <div class="position-relative box-shadow match-height-item-by-row">
            <div class="full-background box-gray-gradient">
              <div class="padding-25 text-center">
                <div class="row">
                  <div class="col-sm-16 col-sm-offset-4">
                    <p class="h3 blue-text"><b>21</b></p>
                    <p>
                      <a href="/org/workinggroups/explore.php">Industry Collaborations</a>
                      in our portfolio spanning enterprise Java, tools, IoT, edge,
                      automotive, operating system, and open hardware
                    </p>
                  </div>
                  <div class="col-sm-8 col-sm-offset-2">
                    <p class="h3 blue-text"><b>3</b></p>
                    <p>
                      New Working Groups: Open VSX, Eclipse Cyber Risk Initiative, Eclipse Dataspace
                    </p>
                  </div>
                  <div class="col-sm-8 col-sm-offset-4">
                    <p class="h3 blue-text"><b>2</b></p>
                    <p>
                      New Interest Groups: Models for Privacy, Eclipse ThreadX
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-24 col-sm-12 margin-bottom-30">
          <div class="position-relative box-shadow match-height-item-by-row">
            <div class="full-background box-gray-gradient">
              <div class="padding-25 text-center">
                <p class="h3 blue-text"><b>175+</b></p>
                <p>of our Strategic Members involved in at least one collaboration</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-24 col-sm-12 margin-bottom-30">
          <div class="position-relative box-shadow match-height-item-by-row">
            <div class="full-background box-gray-gradient">
              <div class="padding-25 text-center">
                <p class="h3 blue-text"><b>220+</b></p>
                <p>projects in the purview of various industry collaborations</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <p>
        Collectively, this high level of participation demonstrates our
        members' desire to work transparently and collaboratively in a
        vendor-neutral environment which strongly supports our "code first"
        approach.
      </p>
    </div>
  </section>

  <section>
    <div id="membership" class="row padding-top-40 padding-bottom-40">
      <div class="full-background membership-top">
        <div class="container padding-top-40">
          <h2 class="padding-bottom-15">Membership</h2>

          <p>
            We are a member-supported organisation, and we appreciate all our
            members' commitment and contributions. Thank You!
          </p>
          <p>
            As of 31 March 2024, the Eclipse Foundation has 14 strategic members.
          </p>

          <div class="row padding-top-20">
            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/bosch_invented.png" alt="Bosch logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/2023/cea-logo.jpg" alt="CEA logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/2023/dlr-logo.png" alt="DLR logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/eesa.png" alt="EESA logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/2023/fraunhofer-logo.png" alt="Fraunhofer Fokus logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/fujitsu.png" alt="Fujitsu logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/huawei.png" alt="Huawei logo" width="100" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/ibm.png" alt="IBM logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/mercedes.png" alt="Mercedes-Benz Tech Innovation logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/microsoft.png" alt="Microsoft logo" />
              </div>
            </div>
            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/obeo.png" alt="Obeo logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/2023/oracle-logo.jpg" alt="Oracle logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img src="images/logos/red_hat.png" alt="Red Hat logo" />
              </div>
            </div>

            <div class="col-xs-8 col-sm-4 margin-bottom-30">
              <div class="box-border-dark box-white match-height-item" data-mh="member-logos">
                <img class="padding-20" src="images/2023/sap-logo.jpg" alt="SAP logo" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row padding-bottom-60 bg-white">
      <div class="container">
        <p>
          The Eclipse Foundation counted 2.057 committers as of 31 March 2024.
          All Committers are also entitled to membership in the Foundation, and
          their membership plays a valuable role in Eclipse Foundation
          governance, including representation on the Eclipse Board and many
          working group governing committees.
        </p>
        <p>
          The Foundation counted 368 organisations as members at the end of March
          2024. A total of 37 new companies joined as new members of the
          Foundation from 1 April 2023 through 31 March 2024.
        </p>
        <p>
          The Foundation has continued its relationship with OpenHW Group, the
          Canadian-based open hardware nonprofit organisation dedicated to
          fostering collaboration among global hardware and software designers in
          developing open source cores, related IP, tools, and software. All
          OpenHW Group Platinum, Gold, and Silver members are also Contributing
          Members of the Eclipse Foundation, and as of 31 March 2024, this
          amounted to 47 Contributing Members.
        </p>
        <p>
          A full list of our members can be seen on the
          <a href="https://www.eclipse.org/membership/exploreMembership.php#allmembers">Explore Our Members</a>
          page.
        </p>

        <div class="position-relative">
          <h2 class="padding-top-50 padding-bottom-15">New Members of the Eclipse Foundation</h2>
          <p class="margin-bottom-0">
            The new members that have joined the Eclipse Foundation between
            April 2023 and March 2024 include:
          </p>
          <div class="full-background bg-new-members">
          </div>

          <div class="row position-relative">
            <div class="container">
              <div class="row row-no-gutters padding-bottom-40">
                <div class="col-sm-8">
                  <dl class="list-unstyled">
                    <dt>Amazon Europe Core SARL</dt>
                    <dt>ATOM Automotive Engineering and Technology (Nanjing) Co., Ltd</dt>
                    <dt>Catena-X Automotive Network e.V</dt>
                    <dt>Cirrus Logic</dt>
                    <dt>co4e GmbH</dt>
                    <dt>Corelab Technology Pte.Ltd</dt>
                    <dt>Cypherbridge Systems LLC</dt>
                    <dt>Energy Telecommunications &amp; Electrical Associations</dt>
                    <dt>FEV.io GmbH</dt>
                    <dt>GPS by Design Centre</dt>
                    <dt>Harman International</dt>
                    <dt>International Data Spaces e.V</dt>
                    <dt>Instituto Tecnológico de Informática</dt>
                  </dl>
                </div>
                <div class="col-sm-8">
                  <dl class="list-unstyled">
                    <dt>Integration Objects</dt>
                    <dt>iShare Foundation</dt>
                    <dt>Information Systems & Databases (DBIS, Informatik 5) of RWTH Aachen University</dt>
                    <dt>Jasmine Conseil SARL</dt>
                    <dt>Johannes Kepler University Linz</dt>
                    <dt>KU Leuven</dt>
                    <dt>Litmus Automation Inc.</dt>
                    <dt>Lunatech Labs B.V.</dt>
                    <dt>Nordic Institute for Interoperability Solutions</dt>
                    <dt>Open Forum Europe AISBL</dt>
                    <dt>Politecnico di Torino</dt>
                    <dt>Posit Software, PBC</dt>
                  </dl>
                </div>
                <div class="col-sm-8">
                  <dl class="list-unstyled">
                    <dt>PX5</dt>
                    <dt>Qualcomm Innovation Center</dt>
                    <dt>Reutlingen University</dt>
                    <dt>Software Mansion SA</dt>
                    <dt>Sonatype Inc.</dt>
                    <dt>Stadt Jena</dt>
                    <dt>Storm Reply GmbH</dt>
                    <dt>Synthara AG</dt>
                    <dt>Technology Innovation Institute &mdash; Sole Proprietorship LLC</dt>
                    <dt>The Matrix.org Foundation C.I.C.</dt>
                    <dt>TOSIT Association</dt>
                    <dt>Witekio Holding</dt>
                  </dl>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="operations" class="operations row padding-top-40 padding-bottom-40 margin-bottom-40">
    <div class="container position-relative">
      <h2 class="padding-bottom-15">Operations</h2>
      <p>
        The Eclipse Foundation, with its global network of members and
        committers, is backed by an international team spread across 11
        countries, operating as Eclipse Foundation AISBL, an international
        not-for-profit organisation based in Brussels, Belgium.
      </p>
      <p>
        Overall, 2023 was a strong year for the Foundation, achieving
        financial and operational targets, with growth driven by increasing
        membership and initiative participation. The fiscal year ended (31
        December) with combined revenues for the Eclipse Foundation Group
        (see below for details) of 11.3M&euro;, which surpassed budget
        expectations and a lower-than-expected net operating loss of 0.3M&euro;.
      </p>
      <h3 class="padding-top-50">Eclipse Foundation Income and Expenses, by Year</h3>
      <table class="table margin-top-20 margin-bottom-20">
        <caption class="caption-bottom text-right">
          all figures are in Millions of Euro
        </caption>
        <thead>
          <tr>
            <th></th>
            <th>2024</th>
            <th>2023</th>
            <th>2022</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th class="fw-400" scope="row">Total Revenue</td>
            <td>13.5</td>
            <td>11.3</td>
            <td>9.4</td>
          </tr>
          <tr>
            <th class="fw-400" scope="row">Total Expenses</td>
            <td>14.0</td>
            <td>11.6</td>
            <td>8.6</td>
          </tr>
        </tbody>
        <tfoot class="fw-700">
          <th scope="row">Net Income (Loss)</th>
          <td>(0.5)</td>
          <td>(0.3)</td>
          <td>0.8</td>
        </tfoot>
      </table>
      <p>
        For the 2024 fiscal year, the Board approved a budget that increases
        revenues to 13.5M&euro;, with a forecasted operating loss of
        0.5M&euro;. This continues with the recent trend of revenue growth
        from memberships, working groups, European research initiatives, and
        security-focused initiatives.
      </p>

      <div class="row margin-bottom-40 text-center">
        <div class="col-sm-12 margin-bottom-30 match-height-item-by-row">
          <a href="./images/2024/group-revenues.jpg">
            <img class="img img-responsive blend-darken" src="./images/2024/group-revenues.jpg" alt="2024 Eclipse Group Revenues: 45.7% Working Groups; 35.9% Membership; 6.5% Grant Revenue; 3.0% Conference Revenue and; 9.0% Other">
            <span>View Full Size</span>
          </a>
        </div>
        <div class="col-sm-12 margin-bottom-30 match-height-item-by-row">
          <a href="./images/2024/group-expenses.jpg">
            <img class="img img-responsive blend-darken" src="./images/2024/group-expenses.jpg" alt="2024 Eclipse Group Expenses: 77.0% People (Staff, Consulting); 12.6% Marketing & Conferences; 4.1% I.T. & Infra and; 6.4% Other">
            <span>View Full Size</span>
          </a>
        </div>
        <div class="col-sm-12 margin-bottom-30 padding-top-60 match-height-item-by-row text-left">
          <p>
            Most of our funding is allocated to staff or various initiatives,
            segmented into broad functional areas that, collectively, enable
            us to deliver high-quality services that support our community’s
            endeavours, and directly align with the priorities established by
            our industry initiatives.
          </p>
        </div>
        <div class="col-sm-12 margin-bottom-20 match-height-item-by-row">
          <a href="./images/2024/staff-functional-areas.jpg">
            <img class="img img-responsive blend-darken" src="./images/2024/staff-functional-areas.jpg" alt="Eclipse Staff Functional Areas: 26.3% Community / Membership; 23.7% I.T. & Security; 14.5% Marketing / Outreach; 10.5% Senior Leadership; 9.2% I.P. & Projects; 7.9% Finance & Admin and; 7.9% EU Grant Project Support">
            <span>View Full Size</span>
          </a>
        </div>
      </div>

      <p>
        As a matter of governance, management provides quarterly budget
        updates to the Board’s finance committee, while Members receive
        updates on the Foundation’s membership enrollment and budget through
        quarterly reports in the <a href="/community/newsletter">Member Newsletter</a>. 
        Further, the audited 2022 financial statements and the 2023 and 2024
        budgets were approved by the General Assembly of Eclipse Foundation
        AISBL; the 2023 audited financial statements will be presented to the
        General Assembly once finalised by EY.
      </p>


  </section>

  <section class="row position-relative padding-y-60 margin-bottom-30" id="corporate-organisation">
    <div class="full-background operations">
      <div class="container">
        <h2 class="margin-bottom-30">Corporate Organisation</h2>
        <p>
          The Eclipse Foundation Group is a collection of four companies operated
          in coordination on behalf of Eclipse members. The primary organisation
          in which the predominance of our membership resides is Eclipse
          Foundation AISBL, a Belgian-based international not-for-profit.
          Alongside Eclipse Foundation AISBL, our operations include Eclipse.org
          Foundation, Inc. (our US-based legacy organisation created in 2004),
          Eclipse Foundation Europe GmbH (a German-based wholly owned for-profit
          subsidiary of Eclipse Foundation AISBL), and Eclipse Foundation Canada
          (a member-based Canadian not-for-profit whose members are Eclipse
          Foundation AISBL and Eclipse.org Foundation, Inc). These entities
          collaborate seamlessly, ensuring unified member management, community
          engagement, project support, industry partnerships, and governance.
          Notably, Eclipse Foundation Europe GmbH plays a vital role in community
          services, hosting events, and engaging in European research projects.
          Both Eclipse Foundation AISBL and Eclipse.org Foundation, Inc. are each
          designated as 501(c)6 organisations by the US Internal Revenue Service.
        </p>
        <p>
          For the fiscal year ending 31 December 2023, EY will serve as auditor
          for Eclipse Foundation AISBL and Eclipse Foundation Europe GmbH, and
          BDO will serve as auditor for Eclipse.org Foundation, Inc. and
          Eclipse Foundation Canada.
        </p>
  </section>
</div>
