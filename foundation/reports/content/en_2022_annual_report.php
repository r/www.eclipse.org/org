<?php
/**
 * Copyright (c) 2005, 2022 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation)
 *   Zachary Sabourin (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="maincontent">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-18">
          <h1 class="margin-bottom-20">
            <?php print $pageTitle; ?>
          </h1>
          <p> Except where otherwise noted, this report covers the period April 1, 2021 to March 31, 2022.</p>
          <div class="row row-no-gutters">
            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#director-summary">Executive Director&rsquo;s Summary</a>
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#who-we-are"
                    >Who We <br />
                    Are</a
                  >
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#key-initiatives"
                    >Key <br />
                    Initiatives</a
                  >
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#projects">Projects</a>
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#working-groups">Working Groups</a>
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#membership">Membership</a>
                </div>
              </div>
            </div>

            <div class="col-xs-6 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#operations">Operations</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 main-col-sidebar-nav">
          <?php print $nav; ?>
        </div>
      </div>
    </div>
  </div>

  <div id="director-summary" class="row">
    <div class="container">
      <h2 class="padding-bottom-30">Executive Director&rsquo;s Summary</h2>

      <div class="headshot-wrapper">
        <img src="images/2022/mike_headshot.jpg" alt="Mike Milinkovich" />
      </div>

      <div>
        <p>
          As the Executive Director of the Eclipse Foundation since its inception in 2004, it is
          my great honor to continue to serve this institution and its wonderful community. A
          great deal has changed over that time, and despite numerous external challenges, this
          past year has been one of exceptional growth for Eclipse. As you will see in this
          report, we have been joined by many new projects, working groups, members, and
          committers. It is my great pleasure to welcome them all to the Eclipse community.
        </p>

        <br />

        <p>
          I would also like to extend a personal thank you to the hundreds of members for their
          financial and organizational support, and to the thousands of committers and
          contributors for their talent and energy. You are what makes Eclipse the dynamic and
          innovative community that it is. I would also like to recognize the staff of the
          Foundation and the tireless efforts they apply daily to the success of our community.
        </p>

        <br />

        <p>
          This past year saw the second year of the COVID-19 pandemic, the rise of inflation,
          and the invasion of Ukraine to name just a few of the global challenges we continue to
          face. I extend my best wishes to all, and hope that you and your families are safe and
          well. Despite the many challenges around us, I firmly believe that the Eclipse
          Foundation and the broader open source community continue to make the world a better
          place.
        </p>

        <br />

        <p>
          Looking forward, I am excited about the opportunities ahead of the Eclipse Foundation
          and our communities. Our move to become legally domiciled in Europe is now complete,
          and we are seeing greater interest than ever before from organizations and projects to
          join. We feel that our institution will have a strong role to play in growing the
          worldwide supply chain of open source technologies which are powering innovation and
          prosperity globally.
        </p>

        <br />

        <p>
          We have made a concerted effort to improve the presentation quality of our annual
          community report, and to provide greater information and transparency through a better
          layout and use of graphics. I hope that you enjoy the new look!
        </p>

        <br />

        <p>
          As always, we welcome your comments and feedback. Let us know your thoughts at
          <a href="mailto:emo@eclipse.org">emo@eclipse.org</a> or on Twitter
          <a href="http://twitter.com/eclipsefdn">@EclipseFdn</a>.
        </p>
      </div>
    </div>
  </div>

  <div id="who-we-are" class="row padding-top-40 padding-bottom-20">
    <div class="container">
      <h2 class="padding-bottom-15">Who We Are</h2>

      <p>The Eclipse Foundation&rsquo;s mission is summarized as follows:</p>

      <div class="row row-no-gutters padding-top-20">
        <div class="col-sm-6">
          <div class="match-height-item-by-row box-solid-color box-blue box-angle">
            <div class="padding-30">
              <p class="h3">Mission</p>

              <p>
                The Eclipse Foundation&rsquo;s purpose is to advance our open source software
                projects and to cultivate their communities and business ecosystems.
              </p>
            </div>
          </div>
        </div>

        <div class="col-sm-10 col-sm-offset-1 padding-when-wrapped">
          <div class="box-lightgray">
            <div class="padding-30 padding-top-60 padding-bottom-100">
              <p>
                The Foundation&rsquo;s long-term success comes from our dedication to a unique
                combination of fostering open source projects and community, coupled with our
                commitment to creating sustainable, commercially successful ecosystems around
                those projects.
              </p>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-sm-offset-1 padding-when-wrapped">
          <div class="match-height-item-by-row box-solid-color box-darkgray box-angle">
            <div class="padding-30">
              <p class="h3">Vision</p>

              <p>
                To be the leading community for individuals and organizations to collaborate on
                open technologies.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="key-initiatives" class="row position-relative padding-top-40 padding-bottom-40">
    <div class="full-background initiatives">
      <div class="container initiatives">
        <h2 class="padding-bottom-15">Key Initiatives in 2021-22</h2>

        <p style="color: white">
          Over the past year, the Eclipse Foundation together with its community has achieved
          great things. None of these achievements would be possible without the support of our
          members.
        </p>

        <div class="row row-no-gutters padding-top-20">
          <div class="col-sm-7">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>1</b></p>
              </div>
              <div class="card-description">
                <p>
                  We have completed our migration to a Belgian-based organization. This includes
                  migrating the majority of our members to the Eclipse Foundation AISBL and
                  transferring the ownership of our German subsidiary.
                </p>
              </div>

              <hr />

              <div class="content-center padding-40">     
                <a href="https://www.eclipse.org/europe/">
                  <img src="images/2022/eu_flag.png" alt="Flag of the European Union" />
                </a>          
              </div>
            </div>
          </div>

          <div class="col-sm-7 col-sm-offset-1 padding-when-wrapped">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>2</b></p>
              </div>
              <div class="card-description">
                <p>
                  We have engaged in initiatives, such as the OSPO.Zone, to encourage good
                  governance in open source.
                </p>
              </div>

              <hr />

              <div class="content-center padding-40">
                <a href="https://ospo.zone">
                  <img src="images/logos/ospo_zone.png" alt="Ospo Zone logo" />
                </a>
              </div>
            </div>
          </div>

          <div class="col-sm-7 col-sm-offset-1 padding-when-wrapped">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>3</b></p>
              </div>
              <div class="card-description">                
                <p>We have great new initiatives underway being driven by members:</p>           
              </div>

              <hr />

              <div class="content-center padding-20">
                <a href="https://adoptium.net/">
                  <img src="images/logos/adoptium.png" alt="Adoptium logo" />
                </a>
                
              </div>

              <hr />

              <div class="content-center padding-20">
                <a href="https://oniroproject.org/">
                  <img src="images/logos/oniro.png" alt="Oniro logo" />
                </a>
              </div>

              <hr />

              <div class="content-center padding-bottom-35">
                <a href="https://sdv.eclipse.org/">
                  <img src="images/logos/sdv.png" alt="Software-Defined Vehicles"/>
                </a>                
              </div>           
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-30">
          <div class="col-sm-7">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>4</b></p>
              </div>
              <div class="card-description">
                <p>
                  We also celebrated the success of some of our longest running initiatives.
                </p>
              </div>

              <hr />

              <div class="content-center padding-20">
                <a href="https://iot.eclipse.org/anniversary/">
                  <img src="images/logos/10_years_iot.png" alt="Eclipse IOT 10-year anniversary"/>
                </a>
                
              </div>

              <hr />

              <div class="content-center padding-20">
                <a href="https://eclipseide.org/20anniversary/">
                  <img src="images/logos/20_years_eclipse_ide.png" alt="Eclipse IDE 20-year anniversary"/>
                </a>
                
              </div>
            </div>
          </div>

          <div class="col-sm-15 col-sm-offset-1 padding-when-wrapped">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>5</b></p>
              </div>
              <div class="card-description">
                <p>
                  Our marketing and ecosystem development teams continue to support and promote
                  our projects, working groups and their communities.
                </p>
              </div>

              <div class="grid-wrapper row-2-col">
                <div class="position-relative">
                  <a href="https://outreach.eclipse.foundation/iot-edge-developer-2021">
                    <img src="images/2022/survey_2021_iot_edge.png" alt="IoT & Edge Developer Survey" class="bottom-right"/>
                  </a>
                </div>

                <div class="box-1-2">
                  <a href="https://outreach.eclipse.foundation/iot-edge-commercial-survey">
                    <img src="images/2022/survey_2021_iot_edge_commercial.png" alt="IoT & Edge commercial adoption survey"/>
                  </a>              
                </div>

                <div class="box-2-1 position-relative">
                  <a href="https://outreach.eclipse.foundation/cloud-developer-survey">
                    <img src="images/2022/survey_2021_cloud.png" alt="Cloud Developer survey" class="top-right"/>
                  </a>                 
                </div>

                <div class="box-2-2">
                  <a href="http://outreach.jakartaee.org/2021-developer-survey-report">
                    <img src="images/2022/survey_2021_jakartaee.png" alt="Jakarta EE Developer survey"/>
                  </a>                  
                </div>
              </div>

              <div class="box-elevated-wrapper">
                <div class="box-elevated">
                  <div class="full-background box-blue-gradient-reverse">
                    <div class="content-center padding-top-15">
                      <p>
                        Introduced <q>Featured Committer</q>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="grid-wrapper row-6-col padding-top-10">
                <div class="box-1-single">
                  <div class="box-solid-color box-orange box-shadow">
                    <p class="h3"><b>4</b></p>
                  </div>

                  <div class="text-center">
                    <p>key industry studies</p>
                  </div>
                </div>

                <div class="box-2-single">
                  <div class="box-solid-color box-orange box-shadow">
                    <p class="h3"><b>67</b></p>
                  </div>

                  <div class="text-center">
                    <p>virtual events</p>
                  </div>
                </div>

                <div class="box-3-single">
                  <div class="box-solid-color box-orange box-shadow">
                    <p class="h3"><b>8,898</b></p>
                  </div>

                  <div class="text-center">
                    <p>attendees</p>
                  </div>
                </div>

                <div class="box-4-single">
                  <div class="box-solid-color box-orange box-shadow">
                    <p class="h3"><b>2,186</b></p>
                  </div>

                  <div class="text-center">
                    <p>attendees at EclipseCon 2021</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-30 padding-bottom-30">
          <div class="col-sm-15">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>6</b></p>
              </div>
              <div class="card-description">
                <p>
                  We continue to invest in tools and resources for our members. For example, we
                  launched a new
                  <a href="https://membership.eclipse.org/portal">Member Portal</a>, which
                  offers member organizations new insights into their participation in the
                  Eclipse community.
                </p>
              </div>

              <div class="grid-wrapper row-5-col padding-bottom-20">
                <div class="box-grid-long position-relative">
                  <img src="images/2022/member_portal_sidebar.png" alt="Member portal sidebar" class="top-right"/>
                </div>

                <div class="box-grid-wide position-relative">
                  <img src="images/2022/member_portal_projects.png" alt="Projects and Working Groups"/>
                </div>

                <div class="box-grid-wide bottom-row position-relative">
                  <img src="images/2022/member_portal_org_profile.png" alt="Your organization profile"/>
                </div>
              </div>
            </div>
          </div>

          <div class="col-xs-24 col-sm-7 col-sm-offset-1 padding-when-wrapped">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>7</b></p>
              </div>
              <div class="card-description">
                <p>
                  Our governance continues to evolve to meet today&rsquo;s challenges for the
                  adoption of open source.
                </p>
              </div>

              <div class="box-elevated-large-wrapper">
                <div class="box-elevated-large">
                  <div class="full-background box-blue-gradient box-shadow">
                    <div class="padding-40">
                      <p>
                        We updated both the Eclipse Foundation Specification Process and the
                        Eclipse Foundation Working Group Process.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="projects" class="row padding-top-40 padding-bottom-40">
    <div class="full-background projects">
      <div class="container position-relative">
        <h2 class="padding-bottom-15">Projects</h2>

        <p>
          The Eclipse community continues to be engaged on the projects and initiatives that
          impact their respective communities.
        </p>

        <div class="row row-no-gutters padding-top-20 padding-bottom-40">
          <div class="col-xs-6 col-sm-3">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>30</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>New Projects</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>415</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Eclipse Foundation projects overall</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>6</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>European research projects supported</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>1,750+</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Committers</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>93</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Members participating in commits</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>382M</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Lines of code</p>
            </div>
          </div>
        </div>

        <p>In 2021, we added four new top-level projects:</p>

        <div class="row row-no-gutters padding-top-20 padding-bottom-60">
          <div class="col-xs-11 col-sm-5">
            <div class="full-background box-blue-gradient box-shadow match-height-item-by-row">
              <div class="padding-20 padding-top-40">
                <p>
                  The <b>Eclipse Automotive</b> top-level project, created in January 2021, now
                  has eleven active subprojects, provides a space for open source projects to
                  explore ideas and technologies addressing challenges in the automotive,
                  mobility and transportation domain.
                </p>
              </div>
            </div>
          </div>

          <div class="col-xs-11 col-sm-5 col-xs-offset-2 col-sm-offset-1">
            <div class="full-background box-blue-gradient box-shadow match-height-item-by-row">
              <div class="padding-20 padding-top-40">
                <p>
                  The <b>AsciiDoc</b> top-level project, created in June 2021, now has two
                  subprojects, is concerned with defining and evolving the AsciiDoc language and
                  providing open source libraries, runtimes, platforms, documentation, tools,
                  and integrations necessary to author content in AsciiDoc and transform it into
                  consumable formats for publication.
                </p>
              </div>
            </div>
          </div>

          <div class="col-xs-11 col-sm-5 col-sm-offset-1 padding-when-wrapped">
            <div class="full-background box-blue-gradient box-shadow match-height-item-by-row">
              <div class="padding-20 padding-top-40">
                <p>
                  The <b>Oniro</b> top-level project, created in October 2021, focuses on the
                  design, development, production, and maintenance of an open source software
                  platform. This project has an operating system, an ADK/SDK, standard APIs, and
                  basic applications, e.g. UI, as core elements. Thanks to a next generation
                  multi-kernel architecture, that simplifies the existing landscape of complex
                  systems as well as its deployment across a wide range of devices, it targets
                  different industries
                </p>
              </div>
            </div>
          </div>

          <div class="col-xs-11 col-sm-5 col-xs-offset-2 col-sm-offset-1 padding-when-wrapped">
            <div class="full-background box-blue-gradient box-shadow match-height-item-by-row">
              <div class="padding-20 padding-top-40">
                <p>
                  The <b>Eclipse Digital Twin</b> top-level project, created in November 2021,
                  provides a space for open source projects to produce implementations and
                  increase adoption of solutions, prototypes and supporting software to build
                  and consume information from digital twins.
                </p>
              </div>
            </div>
          </div>
        </div>

        <p>
          In 2021, we terminated and archived nine Eclipse open source projects. Termination of
          projects is a pretty natural part of the process: Some projects just have a short
          expected lifespan, and others reach their natural conclusion for a variety of reasons.
          It&rsquo;s always sad to see a project terminate, but it&rsquo;s a natural and healthy
          part of the process.
        </p>
      </div>
    </div>
  </div>

  <div id="working-groups" class="row padding-top-40 padding-bottom-40">
    <div class="container">
      <h2 class="padding-bottom-15">Working Groups</h2>

      <p>
        In addition to our flagship Eclipse Foundation Projects, our Eclipse Foundation Working
        Groups continue to play an extremely impactful role in how our members engage and
        contribute!
      </p>

      <br />

      <p>On the publication date of this report, we have:</p>

      <div class="row row-no-gutters padding-top-30 padding-bottom-60">
        <div class="col-xs-11 col-sm-5 position-relative">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>20</b></p>
              <p>
                Working groups in our portfolio spanning enterprise Java, tools, IoT, edge,
                automotive, operating system, and open hardware
              </p>
            </div>
          </div>
        </div>

        <div class="col-xs-11 col-sm-5 col-xs-offset-2 col-sm-offset-1">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>200</b></p>
              <p>Members actively involved in working groups</p>
            </div>
          </div>
        </div>

        <div class="col-xs-11 col-sm-5 col-sm-offset-1 padding-when-wrapped">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>87&percnt;</b></p>
              <p>of our Strategic Members involved in at least one working group</p>
            </div>
          </div>
        </div>

        <div class="col-xs-11 col-sm-5 col-xs-offset-2 col-sm-offset-1 padding-when-wrapped">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>200+</b></p>
              <p>projects in purview of various working groups</p>
            </div>
          </div>
        </div>
      </div>

      <p>
        This demonstrates our members&rsquo; desire to work transparently and collaboratively in
        a vendor-neutral environment which strongly supports our &ldquo;code first&rdquo;
        approach! This approach continues to engage and enable new industry initiatives to
        flourish. Notable highlights include:
      </p>

      <div class="row row-no-gutters padding-top-45 padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/adoptium.png" alt="Adoptium" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-16 col-sm-offset-1 padding-when-wrapped">
          <p>
            <a href="https://adoptium.net/">Adoptium</a> was created to bring high-quality, open
            source Java SE runtimes to millions of developers building the next generation of
            enterprise applications. To date, there have been over 26.6M+ downloads of Eclipse
            Temurin, their pre-built, quality tested, free Java runtime. Work continues on the
            creation and launch of the <a href="https://adoptium.net/marketplace">Adoptium Marketplace</a>.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/ecd.png" alt="Eclipse Cloud DevTools" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://ecdtools.eclipse.org/">Cloud DevTools</a> working group
            accelerates the adoption of Cloud IDE and container-based workspace management
            through the adoption of standards, and through the promotion of Eclipse projects to
            cloud developers including <a href="https://theia-ide.org/">Eclipse Theia</a> and
            Eclipse Open VSX. Their <a href="https://open-vsx.org/">Open VSX Registry</a> is
            available to freely distribute popular IDE extensions, over the last year it has
            expanded to 1,700+ extensions.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/eclipse_2014.png" alt="Eclipse Foundation" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://ide-wg.eclipse.org/">Eclipse IDE Working Group</a> celebrated
            its first year anniversary, while in 2021 the Eclipse IDE celebrated its
            <a href="https://eclipseide.org/20anniversary/">20th year anniversary</a>. This
            Working Group was formed to focus on the ongoing success of the Eclipse IDE and the
            Eclipse Simultaneous Release. Highlights include hiring a SimRel Architect and
            Release Engineer and productive collaboration with the Planning Council.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/iot_edge_sparkplug_combo.jpg" alt="Eclipse IoT logo, Edge native logo, Sparkplug logo"/>
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            <a href="https://iot.eclipse.org/">Eclipse IoT</a> celebrated its
            <a href="https://iot.eclipse.org/anniversary/">10th anniversary</a> in 2021; watch
            the pioneers reminisce in
            <a href="https://youtu.be/EiIIG-LUFUc">this video</a>. Eclipse IoT
            has been joined along the way by the Edge Native (2019) and Sparkplug (2020) working
            groups. Our IoT and Edge Computing ecosystem is stronger and more comprehensive than
            ever before.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/jakartaee.png" alt="Jakarta EE" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://jakarta.ee/">Jakarta EE</a> development community was busy
            working on Jakarta EE 10.0, the first major release to deliver innovation for
            enterprise Java. The working group&rsquo;s signature
            <a href="https://jakartaone.org/">JakartaOne Livestream</a> event was a success,
            additional local livestream events were hosted in Portuguese, Spanish, Russian and
            Turkish to a combined audience of 2,100+. Interest in the
            <a href="https://jakarta.ee/compatibility/">Jakarta Compatibility Program</a>
            remains strong with 19 vendor products listed.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/oniro.png" alt="Oniro" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://oniroproject.org/">Oniro</a> Working Group was created in
            February 2022 with six founding members including one at the strategic level. Oniro
            is the Eclipse Foundations first working group and project centered around creation,
            delivery and support of a new operating system. Oniro is a distributed open source
            operating system for consumer devices.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/sdv.png" alt="Software-Defined Vehicles(SDV)" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            In March 2022 the <a href="https://sdv.eclipse.org/">SDV Working group</a> was
            created with three strategic members and eight contributing members, and growing.
            The main focus is on a &ldquo;new&rdquo; way of collaboration in the automotive
            industry, a &ldquo;code first&rdquo; approach with &ldquo;openness&rdquo; and
            &ldquo;transparency&rdquo;, required due to the competitive nature of the automotive
            industry. It was a natural fit at the Eclipse Foundation.
          </p>
        </div>
      </div>

      <h2 class="padding-bottom-15">Other Working Groups</h2>

      <p>
        In addition to the initiatives listed above, it&rsquo;s also important to mention that
        MicroProfile, OSGi, AsciiDoc, openMDM, OpenADx, openPASS, openMobility continue to
        operate as Eclipse Foundation Working Groups.
      </p>
    </div>
  </div>

  <div id="membership" class="row padding-top-40">
    <div class="full-background membership-top">
      <div class="container padding-top-40">
        <h2 class="padding-bottom-15">Membership</h2>

        <p>
          We are funded 100% by our members. Thank You! As of March 31, 2022, the Eclipse
          Foundation has 14 strategic members:
        </p>

        <div class="row row-no-gutters padding-top-20">
          <div class="col-xs-8 col-sm-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/bosch_invented.png" alt="Bosch logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/list.jpg" alt="List logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/mercedes.png" alt="Mercedes-Benz Tech Innovation logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-xs-offset-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/dlr.png" alt="DLR logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/eesa.png" alt="EESA logo" />
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-50">
          <div class="col-xs-8 col-sm-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/fujitsu.png" alt="Fujitsu logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/fraunhofer_fokus.png" alt="Fraunhofer Fokus logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/huawei.png" width="100" alt="Huawei logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-xs-offset-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/ibm.png" alt="IBM logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/microsoft.png" alt="Microsoft logo" />
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-50 padding-bottom-40">
          <div class="col-xs-8 col-sm-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/obeo.png" alt="Obeo logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/oracle.png" alt="Oracle logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/red_hat.png" alt="Red Hat logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-xs-offset-8 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/sap.png" alt="SAP logo" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row" style="background-color: white">
    <div class="container">
      <p>
        The Eclipse Foundation also has over 1,750 committers as of March 31, 2022. Committers
        are also entitled to membership in the Foundation, and play a valuable role in Eclipse
        Foundation governance, including representation on the Eclipse Board and on many working
        group steering committees.
      </p>

      <br />

      <p>
        As of March 31, 2022, the Foundation counted 329 organizations as members. A total of 41
        new companies joined as new members of the Foundation from April 1, 2021 through March
        31, 2022.
      </p>

      <br />

      <p>
        The Foundation has continued its relationship with OpenHW Group, the Canadian-based open
        hardware nonprofit organization dedicated to fostering collaboration among global
        hardware and software designers in the development of open source cores, related IP,
        tools, and software. All OpenHW Group Platinum, Gold, and Silver members are also
        Contributing Members of the Eclipse Foundation, and as of March 2022, this amounted to
        33 Contributing Members.
      </p>

      <br />

      <p>
        A full list of our members can be seen on the
        <a href="https://www.eclipse.org/membership/exploreMembership.php#allmembers">Explore Our Members</a>
        page.
      </p>

      <h2 class="padding-top-50 padding-bottom-15">New Members of the Eclipse Foundation</h2>

      <p>
        The new members that have joined the Eclipse Foundation between April 2021 and March
        2022 include:
      </p>
    </div>
  </div>

  <div class="row position-relative">
    <div class="full-background membership-bottom">
      <div class="container">
        <div class="row row-no-gutters padding-top-20 padding-bottom-40">
          <div class="col-xs-12 col-sm-12">
            <dl class="list-unstyled">
              <dt>Accenture GmbH</dt>
              <dt>Amadeus</dt>
              <dt>Array</dt>
              <dt>Asiainfo</dt>
              <dt>AURA</dt>
              <dt>Beijing Baolande Software Corporation</dt>
              <dt>Beijing Vsettan Data Technology</dt>
              <dt>Capgemini Deutschland GmbH</dt>
              <dt>Cobham Gaisler</dt>
              <dt>Continental Automotive GmbH</dt>
              <dt>Convertigo</dt>
              <dt>Data Intelligence Offensive</dt>
              <dt>DMI</dt>
              <dt>Dolphin Design</dt>
              <dt>Dover Microsystems</dt>
              <dt>ESOP</dt>
              <dt>European Space Agency</dt>
              <dt>Flow Software</dt>
              <dt>Fondazione LINKS</dt>
              <dt>Genuitec LLC</dt>
            </dl>
          </div>

          <div class="col-xs-12 col-sm-12">
            <dl class="list-unstyled">
              <dt>Hefei Whale Microelectronics</dt>
              <dt>ICT CAS</dt>
              <dt>Industrial Digital Twin Association</dt>
              <dt>Jade Design Automation</dt>
              <dt>Linaro Limited</dt>
              <dt>NOI AG SPA</dt>
              <dt>Noosware BV</dt>
              <dt>Opto 22</dt>
              <dt>Reycom</dt>
              <dt>Rumble Development Corp</dt>
              <dt>Scheidt &amp; Bachmann</dt>
              <dt>SECO</dt>
              <dt>Splendit IT Consulting GmbH</dt>
              <dt>SUSE LLC</dt>
              <dt>Synesthesia Srl</dt>
              <dt>The Apache Software Foundation</dt>
              <dt>Trialog</dt>
              <dt>WarnerBioSystems</dt>
              <dt>Wavious</dt>
              <dt>XMOS Ltd</dt>
              <dt>ZF Friedrichshafen AG</dt>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="operations" class="row padding-top-40">
    <div class="full-background operations">
      <div class="container position-relative">
        <h2 class="padding-bottom-15">Operations</h2>

        <p>
          The Eclipse Foundation has members and committers world-wide, and is supported by a
          staff that is distributed across Europe and North America.
        </p>

        <br />

        <p>
          In 2021, the Eclipse Foundation completed its redomiciling initiative that it
          started in 2020. We now operate as Eclipse Foundation AISBL, an international
          not-for-profit organization based in Brussels, Belgium. We still operate our
          US-based legacy organization, Eclipse.org Foundation, Inc. and the two organizations
          together operate collectively as Eclipse Foundation group, or simply as Eclipse
          Foundation. The operations are seamless with respect to member and community
          engagement. As part of the redomiciling initiative, effective February 2022, the
          ownership of the Foundation&rsquo;s Germany-based for-profit services subsidiary
          organization, Eclipse Foundation Europe GmbH, has been transferred to the new
          Belgian-based organization, solidifying our position as a European organization.
        </p>

        <br />

        <p>
          The Eclipse Foundation&rsquo;s fiscal year end is December 31. For the 2021 fiscal
          year, the combined revenues for the Eclipse group of companies was 7.6M USD, and had
          a net income of 0.2M USD. Despite the challenges created by the global COVID-19
          pandemic, both membership and working group revenues were on budget for 2021. For
          the 2021 fiscal year, Eclipse has appointed new auditors, with EY now serving as the
          auditor for Eclipse Foundation AISBL, and BDO for the other organizations.
        </p>

        <br />

        <p>
          Looking ahead to the full 2022 fiscal year, the Board approved a cash-neutral budget
          that called for an increase of revenues to 8.7M&euro;, with a forecasted operating
          deficit of 0.3M&euro;. This represents growth of greater than 20% in overall
          revenues, which is expected to be driven by both membership and working group net
          new growth. Note that beginning in 2022 the Foundation is reporting its financials
          in Euros rather than USD.
        </p>

        <br />

        <p>
          As a matter of governance, management provides quarterly budget updates to the
          Board&rsquo;s finance committee, and all Members are kept up to date on the
          Foundation&rsquo;s budget through quarterly reports in its Member Newsletter
          publications.
        </p>


        <h2 class="padding-top-50">Eclipse Foundation Income and Expenses, by Year</h2>

        <img
          src="images/2022/annual_report_chart.png"
          alt="2022 Annual report chart"
          height="565"
        />
      </div>
    </div>
  </div>
</div>
