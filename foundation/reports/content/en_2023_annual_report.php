<?php
/**
 * Copyright (c) 2023 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div class="annual-report-2023" id="maincontent">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-sm-18">
          <h1 class="margin-bottom-20">
            <?php print $pageTitle; ?>
          </h1>
          <p> Except where otherwise noted, this report covers the period April 1, 2022 to March 31, 2023.</p>
          <div class="row row-no-gutters">
            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#director-summary">Executive Director&rsquo;s Summary</a>
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#who-we-are"
                    >Who We <br />
                    Are</a
                  >
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#key-initiatives"
                    >Key <br />
                    Initiatives</a
                  >
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#projects">Projects</a>
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#industry-collaborations">Industry Collaborations</a>
                </div>
              </div>
            </div>

            <div class="col-xs-7 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#membership">Membership</a>
                </div>
              </div>
            </div>

            <div class="col-xs-6 col-md-3 box-angle-wrap top-nav padding-when-wrapped">
              <div class="box-angle content-center match-height-item-by-row">
                <div class="padding-top-50 padding-bottom-50">
                  <a href="#operations">Operations</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 main-col-sidebar-nav">
          <?php print $nav; ?>
        </div>
      </div>
    </div>
  </div>

  <section id="director-summary" class="row">
    <div class="container">
      <h2 class="padding-bottom-30">Executive Director&rsquo;s Summary</h2>
      <div class="director-summary-content">
        <div class="director-summary-image">
          <img src="images/2023/mike-milinkovich.jpg" alt="Mike Milinkovich" />
        </div>
        <div class="director-summary-content-body">
          <p>
            I’m excited to report on the state of the Eclipse Foundation. I am constantly
            humbled by the support shown by the community, both through the contributions
            of the thousands of committers and contributors to our projects, as well as
            through the financial support of our member organizations. I am also proud of
            the commitment and quality of the efforts of our staff in support of our
            mission. A heartfelt thank you to you all.
          </p> 
          <p>
            As you will see in this report, the breadth and diversity of activities being
            carried out under the broad banner of "the Eclipse Community" continues to
            grow. This stretches from renewed strategic investments being made by our
            members in the eponymous <a href="https://eclipseide.org/">Eclipse IDE</a>
            through to the tremendous achievements of the new <a href="https://sdv.eclipse.org/">Eclipse Software Defined Vehicle</a> 
            as part of the Foundation's focus on the automotive industry. It also
            stretches from a new commitment of the Foundation to be both a thought leader
            and a leader in best practices in implementing supply chain security for open
            source, as well as our recent engagement in public policy impacting the
            software industry, such as the Cyber Resilience Act. Collectively, the
            Foundation's growth and engagement in these initiatives are
            confirmation that we are providing value to our community, projects, and
            members.
          </p>
          <p>
            In closing, I wish to thank all of our community members for their support and
            contributions. Connecting with the community, whether it’s face-to-face or
            virtually, continues to inspire me as we work towards our collective goals &mdash; it
            makes this role I play so fulfilling.
          </p>
          <p>
            I hope you find this annual report useful and informative. As always,
            we welcome input from all of our community. Let us know your thoughts at 
            <a href="mailto:emo@eclipse.org">emo@eclipse.org</a> or on Twitter
            <a href="http://twitter.com/eclipsefdn">@EclipseFdn</a>.
          </p>
        </div>
      </div>
    </div>
  </section>

  <section id="who-we-are" class="row padding-top-40 padding-bottom-20">
    <div class="container">
      <h2 class="padding-bottom-15">Who We Are</h2>

      <p>The Eclipse Foundation&rsquo;s mission and vision is summarized as follows:</p>

      <div class="row row-no-gutters padding-top-20">
        <div class="col-sm-6">
          <div class="match-height-item-by-row box-solid-color box-blue box-angle">
            <div class="padding-30">
              <p class="h3">Mission</p>

              <p>
                The Eclipse Foundation's purpose is to advance our open source software
                projects and to cultivate their communities and business ecosystems.
              </p>
            </div>
          </div>
        </div>

        <div class="col-sm-10 col-sm-offset-1 padding-when-wrapped">
          <div class="box-lightgray">
            <div class="padding-30 padding-top-60 padding-bottom-100">
              <p>
                The Foundation's long-term success comes from our dedication to a unique
                combination of fostering open source projects and community, coupled with our
                commitment to creating sustainable, commercially successful ecosystems around
                those projects.
              </p>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-sm-offset-1 padding-when-wrapped">
          <div class="match-height-item-by-row box-solid-color box-darkgray box-angle">
            <div class="padding-30">
              <p class="h3">Vision</p>

              <p>
                To be the leading community for individuals and organizations to collaborate on
                open technologies.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div id="key-initiatives" class="row position-relative padding-top-40 padding-bottom-40">
    <div class="full-background initiatives">
      <div class="container initiatives">
        <h2 class="padding-bottom-15">Key Initiatives in 2022-23</h2>

        <p style="color: white">
          Over the past year, the Eclipse Foundation together with its community has
          achieved great things. None of these achievements would be possible without the
          support of our members.
        </p>

        <div class="row row-no-gutters padding-top-20">
          <div class="col-sm-7">
            <div class="card box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>1</b></p>
              </div>
              <div class="card-description">
                <p>
                  Eclipse <a href="https://sdv.eclipse.org/">Software Defined
                  Vehicle</a> has experienced momentum in its first year with 35 members, 15
                  projects onboarded and five additional projects pending.
                  SDV has hosted number of engaging events fostering the necessary stakeholder
                  involvement to drive the success of the initiative, and is co-hosting the
                  <a href="https://automotive-oss.org/2023/">Automotive Open
                  Source Summit</a>, which will bring together automotive leaders to discuss the
                  latest trends in automotive-grade open source software, and to learn about the
                  technologies and challenges in automotive software development.
                </p>
              </div>
              <div class="card-end">     
                <a href="https://sdv.eclipse.org/">
                  <img src="images/2023/sdv-logo.png" alt="Eclipse Software Defined Vehicle" />
                </a>          
              </div>
            </div>
          </div>

          <div class="col-sm-7 col-sm-offset-1 padding-when-wrapped">
            <div class="card box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>2</b></p>
              </div>
              <div class="card-description">
                <p>
                  We have prioritized security this year, focusing on providing services to our
                  projects that help improve their security posture, including a focus on  SBOMS,
                  reproducible builds and a SLSA project badging program. This focus includes a
                  commitment to have security become part of our overall project services on an
                  ongoing basis. The Foundation wishes to thank the <a href="https://openssf.org/community/alpha-omega/">OpenSSF’s Alpha-Omega project</a>
                  for their significant funding to help make this work possible.
                </p>
              </div>
            </div>
          </div>

          <div class="col-sm-7 col-sm-offset-1 padding-when-wrapped">
            <div class="card box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>3</b></p>
              </div>
              <div class="card-description">                
                <p>
                  <a href="https://jakarta.ee/">Jakarta EE</a> celebrates its 5 year anniversary along with the release of Jakarta
                  EE 10, the first major release to deliver innovation for enterprise Java.
                </p>
              </div>
              <div class="card-end">
                <a href="https://jakarta.ee/">
                  <img src="images/2023/jakarta-ee-5th-anniversary.png" alt="Jakarta EE's fifth anniversary" />
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-30">
          <div class="col-sm-7">
            <div class="card box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>4</b></p>
              </div>
              <div class="card-description">
                <p>
                  We released <a href="https://sparkplug.eclipse.org/specification/">Sparkplug 3.0</a>, 
                  which is on track to become an ISO international standard later this year.
                </p>
              </div>
              <div class="content-center padding-20">
                <a href="https://sparkplug.eclipse.org/">
                  <img src="images/2023/sparkplug-logo.png" alt="Sparkplug"/>
                </a>
              </div>
            </div>
          </div>

          <div class="col-sm-15 col-sm-offset-1 padding-when-wrapped">
            <div class="card box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>5</b></p>
              </div>
              <div class="card-description">
                <p>
                  Our marketing and ecosystem development teams continue to support and promote
                  our projects, working groups and their communities.
                </p>
              </div>

              <div class="grid-wrapper row-2-col">
                <div class="box-1-1 position-relative">
                  <img src="images/2023/promote-1.jpg" alt="" class="bottom-right" />
                </div>

                <div class="box-1-2">
                  <img src="images/2023/promote-2.jpg" alt=""/>
                </div>

                <div class="box-2-1 position-relative">
                  <img src="images/2023/promote-3.jpg" alt="" class="top-right" />
                </div>

                <div class="box-2-2">
                  <img src="images/2023/promote-4.jpg" alt="" />
                </div>
              </div>
              <ul class="margin-40">
                <li>Hosted 34 events, with over 5,500 attendees &mdash; including the return of EclipseCon 2022 as a face-to-face event</li>
                <li>Provided over 100,000 developers with valuable content through our <a href="https://newsroom.eclipse.org/eclipse-newsletter">community newsletter</a></li>
                <li>Produced 4 key industry studies</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-30 padding-bottom-30">
          <div class="col-sm-7">
            <div class="box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>6</b></p>
              </div>
              <div class="card-description">
                <p>
                  We’ve expanded industry collaboration opportunities, adding to our successful
                  <a href="/org/workinggroups/about.php">Working Group model</a> 
                  to now also include <a href="/collaborations/interest-groups/">Interest Groups</a> 
                  as an alternative, lighter-weight means for our members to 
                  work together. Collectively, we continue to work to meet our
                  members' ambitions to right-size their collaborative efforts
                  in support of our open source projects.
                </p>
              </div>
            </div>
          </div>

          <div class="col-xs-24 col-sm-15 col-sm-offset-1 padding-when-wrapped">
            <div class="card box-white match-height-item-by-row">
              <div class="card-number">
                <p class="h3"><b>7</b></p>
              </div>
              <div class="card-description">
                <p>
                  We continue to play a strategic role in initiatives to encourage good
                  governance in open source adoption in industry and government. In particular,
                  we continue to hold a leading role in the work being done by the <a href="https://ospo.zone/">OSPO.Zone</a>.
                </p>
              </div>
              <div class="card-end">
                <a href="https://ospo.zone/">
                  <img src="images/2023/ospo-zone.png" alt="OSPO.Zone" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section id="projects" class="row padding-top-40 padding-bottom-40">
    <div class="full-background projects">
      <div class="container position-relative">
        <h2 class="padding-bottom-15">Projects</h2>

        <p>
          The Eclipse community continues to be engaged on the projects and initiatives
          that impact their respective communities.
        </p>

        <div class="row row-no-gutters padding-top-20 padding-bottom-40">
          <div class="col-xs-6 col-sm-3">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>31</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>New Projects</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>418</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Eclipse Foundation projects overall</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>11</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>European research projects supported</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>1,900+</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Committers</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>134</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Members participating in commits</p>
            </div>
          </div>

          <div class="col-xs-6 col-sm-3 col-xs-offset-3 col-sm-offset-1">
            <div class="box-solid-color box-blue box-shadow content-center">
              <p class="h2"><b>437M</b></p>
            </div>
            <div class="text-center padding-top-5">
              <p>Lines of code</p>
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-20 padding-bottom-60">
          <div class="col-xs-11 col-sm-7">
            <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
              <div class="padding-20 padding-top-40">
                <p>
                  With the addition of 15 new open source projects, 2022 was an absolutely
                  massive year for the Eclipse Automotive Top-level Project, thanks to the
                  efforts of the <a href="https://sdv.eclipse.org/">Eclipse SDV Working Group</a>. 
                  These additions more than double the number of projects operating under the 
                  purview of the Eclipse Automotive PMC, which, as of March 2023, has a total 
                  of 26 open source projects. Likewise, the Eclipse Digital Twins Top-level 
                  Project has seen significant relative growth
                  with the addition of five new open source projects, increasing its total to
                  six.
                </p>
              </div>
            </div>
          </div>

          <div class="col-xs-11 col-sm-7 col-xs-offset-2 col-sm-offset-1">
            <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
              <div class="padding-20 padding-top-40">
                <p>
                  Altogether, we added 31 new open source projects, including one new addition
                  for Eclipse Adoptium, one for Eclipse Cloud Development Tools, three for
                  Eclipse Oniro, four for Eclipse Technology, one for Eclipse Tools, and one new
                  specification project for EE4J.
                </p>
              </div>
            </div>
          </div>

          <div class="col-xs-11 col-sm-7 col-sm-offset-1 padding-when-wrapped">
            <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
              <div class="padding-20 padding-top-40">
                <p>
                  As is our regular practice, we also consolidated and/or terminated a large
                  number of open source projects. As an example, in the past year we consolidated
                  the 12 subprojects of Eclipse Mylyn into the parent project, and then further
                  converted the parent project into a subproject of the Eclipse Tools top level
                  project. By taking the steps of consolidating, especially with more mature
                  Eclipse projects, we ensure that both the committers of those projects, as well
                  as the downstream users of those projects are better served. Terminating
                  projects that are no longer viable or active, helps to preserve the Eclipse
                  "brand" and more importantly, puts focus on the many exceptional projects under
                  the Eclipse umbrella.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div id="industry-collaborations" class="row padding-top-40 padding-bottom-40">
    <div class="container">
      <h2 class="padding-bottom-15">Industry Collaborations</h2>
      <p>
        In addition to our flagship Eclipse projects, our Eclipse Foundation
        collaborations continue to play an extremely impactful role in how our members
        engage and contribute!
      </p>
      <p>As of March 31, 2023, we have:</p>
      <div class="row row-no-gutters padding-top-30 padding-bottom-60">
        <div class="col-xs-11 col-sm-5 position-relative">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>18</b></p>
              <p>
                <a href="/org/workinggroups/explore.php">Industry Collaborations</a> in our 
                portfolio spanning enterprise Java, tools, IoT, edge, automotive, operating system, and open hardware
              </p>
            </div>
          </div>
        </div>

        <div class="col-xs-11 col-sm-5 col-xs-offset-2 col-sm-offset-1">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>150+</b></p>
              <p>Members actively involved in collaborations</p>
            </div>
          </div>
        </div>

        <div class="col-xs-11 col-sm-5 col-sm-offset-1 padding-when-wrapped">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>93&percnt;</b></p>
              <p>of our Strategic Members involved in at least one collaboration</p>
            </div>
          </div>
        </div>

        <div class="col-xs-11 col-sm-5 col-xs-offset-2 col-sm-offset-1 padding-when-wrapped">
          <div class="full-background box-gray-gradient box-shadow match-height-item-by-row">
            <div class="padding-25 text-center">
              <p class="h3 blue-text"><b>220+</b></p>
              <p>projects in purview of various industry collaborations</p>
            </div>
          </div>
        </div>
      </div>

      <p>
        Collectively, this high level of participation demonstrates our members' desire
        to work transparently and collaboratively in a vendor-neutral environment which
        strongly supports our "code first" approach!  Notable highlights include:
      </p>

      <div class="row row-no-gutters padding-top-45 padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/adoptium.png" alt="Adoptium" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-16 col-sm-offset-1 padding-when-wrapped">
          <p>
            <a href="https://adoptium.net/">Adoptium</a> continues to bring high-quality, 
            open source Java SE runtimes under the Eclipse Temurin project to millions of
            developers. As demonstration of the value of this initiative, Temurin has had
            more than 137 million downloads to date. The Eclipse Temurin project achieved
            <a href="https://adoptium.net/blog/2022/11/slsa2-temurin/">SLSA level 2</a> 
            compliance in Q4 2022 and is working towards SLSA 3 and 4 in 2023. In May
            2022, <a href="https://adoptium.net/marketplace/">Adoptium Marketplace</a> 
            was launched, publishing other OpenJDK vendors that are
            guaranteeing quality under <a href="https://projects.eclipse.org/projects/adoptium.aqavit">AQAvit</a> 
            verification.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/eclipse_2014.png" alt="Eclipse IDE Working Group" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://eclipseide.org/">Eclipse IDE Working Group</a> has 
            continued to gain momentum since its launch in the spring of 2021,
            the focus remains the ongoing success and sustainability of the Eclipse IDE &
            Platform and the Eclipse Simultaneous Release. Over the last 12 months activity
            driven by the working group's <a href="https://eclipseide.org/working-group/development-funding-initiative/">Development Funded Initiatives</a> 
            has gained momentum and addressed a number of the IDE’s Planning
            Council's top technical priorities for the IDE.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/iot_edge_sparkplug_combo.jpg" alt="Eclipse IoT logo, Edge native logo, Sparkplug logo"/>
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://iot.eclipse.org/">Eclipse IoT</a>, 
            <a href="https://edgenative.eclipse.org">Edge Native</a> and 
            <a href="https://sparkplug.eclipse.org">Sparkplug</a> working
            groups together form the largest open source community focused on IoT and Edge
            Computing. Newcomers to it can now get a comprehensive overview in
            "<a href="https://link.springer.com/book/10.1007/978-1-4842-8882-5">Building Enterprise IoT Solutions using Eclipse IoT Technologies: An Open-Source Approach to Edge Computing"</a>,
            published in December 2022 by Apress (ISBN: 978-1484288818). The author, 
            Frederic Desbien, the Program manager for these working groups, was assisted 
            by twelve project leads and committers who served as technical reviewers.
          </p>
          <p>
            In November 2022, the Eclipse Foundation <a href="https://newsroom.eclipse.org/news/announcements/eclipse-foundation-announces-release-sparkplug-30-and-unveils-it-being-fast">submitted the Sparkplug specification to ISO/IEC JTC1 for transposition as an international standard</a> 
            under the publicly available specification (PAS) process. The ballot will be
            open until May 2, 2023. If successful, we expect Sparkplug to be published as
            an ISO/IEC standard by early 2024 at the latest. This would be a first for the
            Eclipse Foundation.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item">
            <img src="images/logos/jakartaee.png" alt="Jakarta EE" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://jakarta.ee/">Jakarta EE</a> development community has delivered Jakarta EE 10.0, the first
            major release to deliver innovation for enterprise Java, with over 20 updated
            individual specifications and new Jakarta EE Core Profile. The community is now
            busy developing <a href="https://jakartaee.github.io/jakartaee-platform/jakartaee11/JakartaEE11ReleasePlan">Jakarta EE 11 release plan</a>. The working group’s signature
            <a href="https://jakartaone.org/">JakartaOne Livestream</a> event
            continues to be a success, additional local livestream events were hosted in
            Chinese, Portuguese, Japanese and German, to a combined audience of 2,000+ and
            YouTube recording views of 3,000+. Interest in the <a href="https://jakarta.ee/compatibility/">Jakarta Compatibility Program</a> 
            remains strong and growing now with 23 vendor products listed.
          </p>
        </div>
      </div>

      <div class="row row-no-gutters padding-bottom-50">
        <div class="col-xs-24 col-sm-6">
          <div class="box-white-shadow match-height-item padding-top-20 padding-bottom-20">
            <img src="images/2023/sdv-logo.png" alt="Eclipse Software Defined Vehicle Working Group" />
          </div>
        </div>

        <div class="col-xs-22 col-xs-offset-1 col-sm-15 col-sm-offset-1 padding-when-wrapped">
          <p>
            The <a href="https://sdv.eclipse.org/">Eclipse SDV Working Group</a> celebrated 
            its first year in March, with 35 members now participating, including members
            from across a world-wide geographical reach and representing a broad
            cross-section of industry roles. The working group continues to live by its
            "code first" approach and strongly supports its growing number of projects (15
            active projects and five project proposals).
          </p>
          <p>
            In March, the first <a href="https://sdv.eclipse.org/sdv-community-day-lisbon-2023/">SDV Community Day</a> 
            event of 2023 was held, with a focus on community engagement and partnership
            building. The community is keeping momentum in the coming year, with the
            inaugural <a href="https://automotive-oss.org/2023/">Automotive Open Source Summit</a> 
            on June 6. This will be the first industry-wide decision-makers
            targeted summit dedicated exclusively to foster open source adoption in the
            automotive industry.
          </p>
        </div>
      </div>
    </div>
  </div>

  <div id="membership" class="row padding-top-40 padding-bottom-40">
    <div class="full-background membership-top">
      <div class="container padding-top-40">
        <h2 class="padding-bottom-15">Membership</h2>

        <p>
          We are a member supported organization. Thank You! As of March 31, 2023, the
          Eclipse Foundation has 14 strategic members.
        </p>

        <div class="row row-no-gutters padding-top-20">
          <div class="col-xs-8 col-sm-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/bosch_invented.png" alt="Bosch logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/2023/cea-logo.jpg" alt="CEA logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/2023/dlr-logo.png" alt="DLR logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1 col-xs-offset-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/eesa.png" alt="EESA logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/fujitsu.png" alt="Fujitsu logo" />
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-50">
          <div class="col-xs-8 col-sm-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/2023/fraunhofer-logo.png" alt="Fraunhofer Fokus logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/huawei.png" width="100" alt="Huawei logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/ibm.png" alt="IBM logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1 col-xs-offset-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/mercedes.png" alt="Mercedes-Benz Tech Innovation logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/microsoft.png" alt="Microsoft logo" />
            </div>
          </div>
        </div>

        <div class="row row-no-gutters padding-top-50 padding-bottom-40">
          <div class="col-xs-8 col-sm-4">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/obeo.png" alt="Obeo logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/2023/oracle-logo.jpg" alt="Oracle logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img src="images/logos/red_hat.png" alt="Red Hat logo" />
            </div>
          </div>

          <div class="col-xs-8 col-sm-4 col-xs-offset-8 col-sm-offset-1">
            <div class="box-border-dark box-white match-height-item">
              <img class="padding-20" src="images/2023/sap-logo.jpg" alt="SAP logo" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row padding-top-60 padding-bottom-60" style="background-color: white">
    <div class="container">
      <p>
        The Eclipse Foundation counted 1,899 committers as of March 31, 2023. All
        Committers are also entitled to membership in the Foundation, and their
        membership plays a valuable role in Eclipse Foundation governance, including
        representation on the Eclipse Board and on many working group governing
        committees.
      </p>
      <p>
        As of March 31, 2023, the Foundation counted 357 organizations as members. A
        total of 59 new companies joined as new members of the Foundation from April 1,
        2022 through March 31, 2023.
      </p>
      <p>
        The Foundation has continued its relationship with OpenHW Group, the
        Canadian-based open hardware nonprofit organization dedicated to fostering
        collaboration among global hardware and software designers in the development
        of open source cores, related IP, tools, and software. All OpenHW Group
        Platinum, Gold, and Silver members are also Contributing Members of the Eclipse
        Foundation, and as of March 31, 2023, this amounted to 47 Contributing Members.
      </p>
      <p>
        A full list of our members can be seen on the
        <a href="https://www.eclipse.org/membership/exploreMembership.php#allmembers">Explore Our Members</a>
        page.
      </p>
      
      <div class="position-relative">
        <h2 class="padding-top-50 padding-bottom-15">New Members of the Eclipse Foundation</h2>
        <div class="full-background membership-bottom-2023">
        <p>
          The new members that have joined the Eclipse Foundation between April 2022 and
          March 2023 include:
        </p>
        </div>

        <div class="row position-relative">
            <div class="container">
              <div class="row row-no-gutters padding-top-20 padding-bottom-40">
                <div class="col-sm-12">
                  <dl class="list-unstyled">
                    <dt>10x Engineers</dt>
                    <dt>23 Technologies GmbH</dt>
                    <dt>ARRK Engineering GmbH</dt>
                    <dt>Advanced Micro Devices (AMD)</dt>
                    <dt>Beijing Institute of Open Source Chip</dt>
                    <dt>Berylls Group GmbH & subsidiaries</dt>
                    <dt>Bloomberg LP</dt>
                    <dt>Cariad SE</dt>
                    <dt>Codasip</dt>
                    <dt>Cubit Innovation Labs</dt>
                    <dt>Cummins Inc</dt>
                    <dt>Dipartimento di Elettronica, Informazione e Bioingegneria - Politecnico di Milano</dt>
                    <dt>EGX Acquisition</dt>
                    <dt>Elektrobit Automotive GmbH</dt>
                    <dt>Empaiot Pte Ltd</dt>
                    <dt>EMQ Technologies</dt>
                    <dt>ETAS GmbH</dt>
                    <dt>FUJIFILM Italia S.p.A</dt>
                    <dt>Gaia-x European Association for Data and Cloud</dt>
                    <dt>General Motors GTO LLC</dt>
                    <dt>Google LLC</dt>
                    <dt>Harvey Mudd College</dt>
                    <dt>IAV</dt>
                    <dt>IoTex Foundation</dt>
                    <dt>Kafein.io</dt>
                    <dt>Kalray SA</dt>
                    <dt>Kentyou</dt>
                    <dt>LG Electronics Inc</dt>
                    <dt>Luxoft GmbH</dt>
                  </dl>
                </div>
                <div class="col-sm-12">
                  <dl class="list-unstyled">
                    <!-- -->
                    <dt>Micro Stream Software GmbH</dt>
                    <dt>MU Electronics</dt>
                    <dt>N3uron Connectivity Systems</dt>
                    <dt>NEC Corporation</dt>
                    <dt>NOUVELLE-AQUITAINE OPEN SOURCE</dt>
                    <dt>Oklahoma State University</dt>
                    <dt>Omnifish OU</dt>
                    <dt>Open Elements GmbH</dt>
                    <dt>OSADL</dt>
                    <dt>PATEO CONNECT+ Technology (Shanghai) Corporation</dt>
                    <dt>PlanV GmbH</dt>
                    <dt>Quantyss</dt>
                    <dt>Rapid Space International</dt>
                    <dt>RIVOS</dt>
                    <dt>Scan Open Source Solutions Sl</dt>
                    <dt>Shenzen Ping An Communication Technology Co., Ltd</dt>
                    <dt>Silicon Assurance</dt>
                    <dt>Sorbonne University</dt>
                    <dt>SOTEC</dt>
                    <dt>Start North ry</dt>
                    <dt>Stellenbosch University</dt>
                    <dt>Sustainable Digital Infrastructure Alliance</dt>
                    <dt>T-Systems International GmbH</dt>
                    <dt>UKRI STFC</dt>
                    <dt>Universidad de Cantabria</dt>
                    <dt>Universidade do Minho</dt>
                    <dt>University of Reading</dt>
                    <dt>Valeo</dt>
                    <dt>Vitesco Technologies GmbH</dt>
                  </dl>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <section id="operations" class="row padding-top-40 padding-bottom-40 margin-bottom-40">
    <div class="full-background operations">
      <div class="container position-relative">
        <h2 class="padding-bottom-15">Operations</h2>
        <p>
          The Eclipse Foundation has members and committers from around the world, and is
          supported by a staff that is distributed across Europe and North America. We
          operate as Eclipse Foundation AISBL, an international not-for-profit
          organization based in Brussels, Belgium. We also operate our US-based legacy
          organization, Eclipse.org Foundation, Inc. and the two organizations together
          operate collectively as Eclipse Foundation group, or simply as Eclipse
          Foundation.The operations are seamless with respect to member and community
          engagement and governance. Both organizations are designated as 501(c)6
          organizations by the US Internal Revenue Service. Eclipse Foundation AISBL also
          has a wholly owned for-profit subsidiary, Eclipse Foundation Europe GmbH which,
          among other things, participates in many European-based research initiatives,
          offers mentoring and consulting services to members, and hosts the Foundation's
          conferences and events.
        </p>
        <p>
          Overall, 2022 was a strong year for the Foundation in terms of operations and
          finances. The Foundation met its targets for growth in membership, led largely
          by Software Defined Vehicle’s success in attracting participating organizations
          as well as relatively strong retention rates of its membership.
        </p>
        <p>
          The Eclipse Foundation's fiscal year end is December 31. For the 2022 fiscal
          year, the combined revenues for the Eclipse Foundation group of companies was
          9.4M€, and had a net income of 0.8M€. For the 2022 fiscal year, EY will be
          serving as the financial auditor for Eclipse Foundation AISBL, and BDO for
          Eclipse.org Foundation, Inc.
        </p>
        <p>
          Looking ahead to the 2023 fiscal year, in December 2022, the Board approved a
          budget that calls for an increase of revenues to 11.1M€, with a forecasted
          operating deficit of 0.6M€. This budget continues with the recent years' trend
          of increased revenues related to membership and working group revenue growth,
          as well as additional revenues in support of the Foundation’s security-focused
          initiatives.
        </p>
        <p>
          As a matter of governance, management provides quarterly budget updates to the
          Board’s finance committee, and all Members are kept up to date on the
          Foundation's budget through quarterly reports in its <a href="/community/newsletter/">Member Newsletter</a>
          publications. Further, both the 2021 audited financial statements and the 2022
          and 2023 budgets were approved by the General Assembly of Eclipse Foundation
          AISBL; the 2022 audited financial statements will be presented to the General
          Assembly once finalized by EY.
        </p>
        <p>
          Finally, the Board took the decision in the fall of 2022 to increase the
          membership fees for most membership levels. This is the first broad change in
          the Foundation's membership fee structure since 2008, and was driven largely by
          inflationary pressures. These new fees went into effect on January 1, 2023,
          though organizations that were members of the Foundation as of December 31,
          2022 will not see the increase in fees until their 2024 renewals.
        </p>

        <h2 class="padding-top-50">Eclipse Foundation Income and Expenses, by Year</h2>
        <table class="table margin-top-20 margin-bottom-20">
          <thead>
            <tr>
              <th>In millions (EUR)</th>
              <th>2021 Actual</th>
              <th>2022 Actual</th>
              <th>2023 Forecast</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Revenue</td>
              <td>6.6</td>
              <td>9.4</td>
              <td>11.1</td>
            </tr>
            <tr>
              <td>Expenses</td>
              <td>6.3</td>
              <td>8.6</td>
              <td>11.7</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
