<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation)
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "2020 Annual Community Report";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse, foundation, community, annual report, 2020, vision, mission, eclipse vision, eclipse foundation mission, eclipse mission");
$Theme->setPageAuthor("Mike Milinkovich");


ob_start();
include ("2020_annual_report.html");
$html = ob_get_clean();
$html .= <<<EOHTML

  <div id="rightcolumn">
    <div class="sideitem">
      <h6 id="a_15">Related Links</h6>
      <ul>
      <li><a href="2020_annual_report.php">2020 Annual Report</a></li>
      <li><a href="2019_annual_report.php">2019 Annual Report</a></li>
      <li><a href="2018_annual_report.php">2018 Annual Report</a></li>
      <li><a href="2017_annual_report.php">2017 Annual Report</a></li>
      <li><a href="2016_annual_report.php">2016 Annual Report</a></li>
      <li><a href="2015_annual_report.php">2015 Annual Report</a></li>
      <li><a href="2014_annual_report.php">2014 Annual Report</a></li>
      <li><a href="2013_annual_report.php">2013 Annual Report</a></li>
      <li><a href="2012_annual_report.php">2012 Annual Report</a></li>
      </ul>
    </div>
  </div>
EOHTML;


$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();

