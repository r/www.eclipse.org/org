<?php
/**
 * Copyright (c) 2021 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation)
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "Eclipse Foundation Writing Style Guide";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("writing style guide, documents, Eclipse Foundation");
$Theme->setPageAuthor("Eclipse Foundation");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();

