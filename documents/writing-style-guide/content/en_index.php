<?php

/**
 * Copyright (c) 2005, 2018, 2021, 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - initial API and implementation
 * Olivier Goulet (Eclipse Foundation) - Update style guide to British English
 */
?>
<div id="maincontent">
  <div id="midcolumn">
    <p>Last updated November 2023</p>
    <h1><?php print $pageTitle; ?></h1>
    <ul>
      <li><a href="#1">Using This Guide</a></li>
      <li><a href="#2">Representing the Brand</a>
        <ul>
          <li><a href="#41">Trademarks and Copyright</a>
          <li><a href="#3">Tone and Voice</a></li>
          <li><a href="#4">Using Our Name</a></li>
          <li><a href="#5">Referring to Projects and Working Groups</a></li>
          <li><a href="#6">Referring to Communities</a></li>
          <li><a href="#7">Referring to People</a></li>
        </ul>
      </li>
      <li><a href="#8">Telling the Story</a>
        <ul>
          <li><a href="#9">Organising Content to Capture Attention</a></li>
          <li><a href="#10">Incorporating SEO Keywords</a></li>
          <li><a href="#11">Writing Effective Headings</a></li>
          <li><a href="#12">Writing for Easy Reading and Quick Scanning</a></li>
          <li><a href="#13">Including Proof Points</a></li>
          <li><a href="#14">Incorporating Links</a></li>
          <li><a href="#15">Referencing Source Material</a></li>
        </ul>
      </li>
      <li><a href="#16">Style and Punctuation</a>
        <ul>
          <li><a href="#17">Abbreviations and Acronyms</a></li>
          <li><a href="#18">Apostrophes</a></li>
          <li><a href="#40">Business Entities</a>
          <li><a href="#19">Capitalisation</a></li>
          <li><a href="#20">Commas</a></li>
          <li><a href="#21">Compound Words and Hyphenation</a></li>
          <li><a href="#22">Contractions</a></li>
          <li><a href="#23">Currencies</a></li>
          <li><a href="#24">Dashes and Slashes</a></li>
          <li><a href="#25">Date and Time</a></li>
          <li><a href="#26">Email, Website, and Social Media References</a></li>
          <li><a href="#28">Exclamation Marks</a></li>
          <li><a href="#29">Geographic Names</a></li>
          <li><a href="#30">Language and Spelling</a></li>
          <li><a href="#31">Latinisms</a></li>
          <li><a href="#32">Lists</a></li>
          <li><a href="#33">Names and Job Titles</a></li>
          <li><a href="#34">Numbers, Statistics, and Prices</a></li>
          <li><a href="#35">Periods, Colons, and Semicolons</a></li>
          <li><a href="#36">Quotation Marks</a></li>
          <li><a href="#37">Symbols</a></li>
          <li><a href="#38">Tables</a></li>
          <li><a href="#39">Technical Standards</a></li>
        </ul>
      </li>
    </ul>
    <h1 id="1">Using This Guide</h1>
    <p>Follow the rules in this document when writing any Eclipse Foundation marketing content for
      any medium. This document covers writing style only and is intended to be used in conjunction
      with the Eclipse Foundation:</p>
    <ul>
      <li>Editorial guidelines for each content type</li>
      <li>Europa Interinstitutional Style Guide</li>
    </ul>
    <h2>Audience</h2>
    <p>The intended audience for this document is anyone writing on behalf of the Eclipse Foundation
      or representing the brand to an external audience. This includes the following:</p>
    <ul>
      <li>Employees of the Eclipse Foundation</li>
      <li>Eclipse Foundation working group members and project leads</li>
      <li>Media members and third-party content developers</li>
      <li>Agencies producing content on behalf of the Eclipse Foundation</li>
    </ul>

    <h1 id="2">Representing the Brand</h1>
    <p>
      Write all content in British English following the spelling in the<a href="https://www.oed.com/"> Oxford English Dictionary</a>. When two spellings are provided, use the first entry.
    </p>
    <p>General guidelines:</p>
    <ul>
      <li>Choose simple, precise words with the fewest meanings. Always ask yourself whether the
        word or phrase will be easily understood by readers who don&rsquo;t have English as a first
        language.</li>
      <li>Never include colloquialisms or culturally specific references.</li>
      <li>Use universally understood terms. Avoid clich&eacute;s, jargon, outdated expressions, and
        phrases that only industry insiders will understand.</li>
      <li>Avoid overused phrases and marketing hype.</li>
    </ul>
    <h2 id="41">Trademarks and Copyright</h2>
    <p>
      Please visit the <a href="https://www.eclipse.org/legal/logo_guidelines.php">Eclipse logo and
        Trademark Guidelines</a> section of the Eclipse Foundation website for further details on
      trademark usage.
    </p>
    <p>Always indicate Eclipse Foundation trademarks on first mention of the trademarked
      term, either &trade; or &reg;. Subsequent mentions of the trademarked term in the same text do
      not need to have the trademark indicated.</p>
    <p>Do not indicate other vendors&rsquo; trademarks in marketing content. It&rsquo;s
      too easy to miss trademarks, mark them inconsistently across content types, or mark them
      incorrectly. Instead, include a general statement at the end of each piece of content, as
      shown below.</p>
    <p>Use the copyright and trademark notation once on all originals and copies of a
      work. It should be placed somewhere on the document so as to give &quot;reasonable notice of
      the claim of copyright.&quot;&nbsp;</p>
    <p>Use the format: &copy; Eclipse Foundation, 2023.</p>
    <p>The copyright year is always the year the content was first published, even if the
      content is updated in a subsequent year.</p>
    <p>Example:</p>
    <ul>
      <li>&copy; Eclipse Foundation, 2023. The Eclipse Foundation
        word, logo, trademarks, and registered trademarks are trademarks of the Eclipse Foundation.
        All third-party product names, logos, and brands are property of their respective owners and
        are used for reference only, and the Eclipse Foundation makes no representation of ownership
        of these marks.</li>
    </ul>
    <h2 id="3">Tone and Voice</h2>
    <p>Write all Eclipse Foundation content in an authoritative and professional voice that reflects
      our experience and leadership in open source software but is not stuffy, old-fashioned, or
      arrogant.</p>
    <p>The tone should be direct, informative, and helpful, but never condescending, patronising, or
      overly casual:</p>
    <ul>
      <li>Always present the information from the audience point of view, even when positioning
        Eclipse Foundation projects and initiatives. The text should demonstrate thought leadership
        with an authoritative understanding of industry and customer needs and challenges.</li>
      <li>Use third-person writing style to identify the audience in the content and for more formal
        content, such as white papers.</li>
      <li>Use first-person writing style for calls to action, instructions for readers, and in more
        casual content, such as blogs and social media posts, after the audience has been
        identified.</li>
    </ul>
    <h2 id="4">Using Our Name</h2>
    <p>Always refer to the Eclipse Foundation by its full name, never as simply
      &ldquo;Eclipse,&rdquo; as this can cause confusion with the Eclipse IDE, which the developer
      community often refers to as Eclipse.</p>
    <p>Avoid referring to the Eclipse Foundation as &ldquo;the Foundation&rdquo; in standalone
      references. However, if the Eclipse Foundation name is already well-established in the
      paragraph, it can be referred to as &ldquo;the Foundation&rdquo; in subsequent mentions within
      that paragraph to improve readability.</p>
    <p>In addition:</p>
    <ul>
      <li>Avoid using the Eclipse Foundation name in a possessive construction such as &ldquo;The
        Eclipse Foundation&rsquo;s &hellip; &rdquo;</li>
      <li>After first use, Eclipse Foundation can be replaced with we, us, or our.</li>
      <li>Only capitalise &ldquo;the&rdquo; in the Eclipse Foundation if it is at the beginning of a
        sentence.</li>
    </ul>
    <h2>Referring to Membership Levels</h2>
    <p>The Eclipse Foundation offers four levels of membership: Strategic, Contributing, Associate,
      and Committer. When referring to each membership level individually, the names should be
      capitalised. When referring to membership in general, the words &ldquo;member&rdquo; and
      &ldquo;membership&rdquo; should be lowercase.&nbsp;</p>
    <ul>
      <li>Eclipse Foundation membership provides certain benefits...</li>
      <li>Eclipse Foundation members gain access to&hellip;</li>
      <li>A Strategic Member of the Eclipse Foundation has access to...</li>
    </ul>
    <h2 id="5">Referring to Projects and Working Groups</h2>
    <p>The Eclipse Foundation hosts open source software projects and working groups. It also
      provides marketing, intellectual property, and support services to them. It does not develop
      open source software itself or manage projects.</p>
    <p>To avoid the impression of ownership or control, refrain from using the following phrases:</p>
    <ul>
      <li>Eclipse Foundation projects</li>
      <li>Eclipse Foundation research projects</li>
      <li>Eclipse Foundation working groups</li>
    </ul>
    <p>Instead use variations that more clearly indicate the relationships:</p>
    <ul>
      <li>Projects hosted at the Eclipse Foundation&hellip;</li>
      <li>Community projects at the Eclipse Foundation&hellip;</li>
      <li>The Eclipse Foundation is home to several working groups&hellip;</li>
      <li>Working groups at the Eclipse Foundation&hellip;</li>
    </ul>
    <p>The open source software projects hosted at the Eclipse Foundation provide technologies that
      anyone can freely use to develop commercial products and solutions. The projects are not
      products or solutions themselves.</p>
    <p>When referring to projects, always use the project&rsquo;s formal name. The formal name
      starts with one of the Eclipse Foundation&rsquo;s brands (e.g., &ldquo;Eclipse&rdquo;,
      &ldquo;LocationTech&rdquo;, or &ldquo;Jakarta&rdquo;), followed by the project&rsquo;s
      distinctive name (e.g., &ldquo;Eclipse Che&rdquo;, &ldquo;LocationTech GeoWave&rdquo;, or
      &ldquo;Jakarta Activation&rdquo;). After the first use, the more informal nickname of the
      project can be used. For example, on the first use, the formal name &ldquo;Eclipse Che&rdquo;
      would be used, but &ldquo;Che&rdquo; can be used in all occurance thereafter.&nbsp;</p>
    <p>
      The <a href="https://projects.eclipse.org/">project page</a> displays the formal name, along
      with other potentially useful information about the project.
    </p>
    <p>There are some exceptions.&nbsp;</p>
    <p>The original top level project is named &ldquo;Eclipse&rdquo;. When referring to it, we
      consistently call it the &ldquo;Eclipse top level project&rdquo;; though it is generally more
      likely that the writer should refer to the &ldquo;Eclipse Platform&rdquo; or &ldquo;Eclipse
      JDT&rdquo; subprojects. Note that &ldquo;Eclipse JDT&rdquo; is often used by the community as
      a synonym for &ldquo;Eclipse IDE&rdquo;.</p>
    <p>The MicroProfile specification project is simply named &ldquo;MicroProfile&rdquo;. Generally,
      though, the writer will likely refer to the products of the project, including the
      &ldquo;MicroProfile Specification&rdquo; or &ldquo;MicroProfile Config&rdquo; specification.</p>
    <p>When referring to the&nbsp; project and code that enables the Open VSX marketplace, use
      &ldquo;Eclipse Open VSX.&rdquo; When writing about the marketplace itself, use &ldquo;Open VSX
      Registry.&rdquo;</p>
    <p>There are also some examples of projects with names that violate our naming standards. The
      EclipseLink project, which predates our application of strict naming guidelines, incorrectly
      concatenates the Eclipse brand with another word; we refer to this project simply as
      &ldquo;EclipseLink&rdquo; and not &ldquo;Eclipse EclipseLink&rdquo;.&nbsp;</p>
    <p>Projects like the Eclipse Modeling Framework or Eclipse Communication Framework, for example,
      are commonly referred to by their acronyms, EMF and ECF. The first occurance of these project
      names should either use the complete name, or--should there be cause to use just the
      acronym--the acronym prepended with the brand (e.g., &ldquo;Eclipse Modeling Framework&rdquo;
      or &ldquo;Eclipse EMF&rdquo;).</p>
    <p>When referring to working groups, include &ldquo;Eclipse&rdquo; only when it is an official
      part of the working group name. Note that several working groups do not include the Eclipse
      branding.&nbsp;</p>
    <p>Capitalise the names of projects, unless the name is purposely lowercase.</p>
    <ul>
      <li>Eclipse Theia project</li>
      <li>Eclipse zenoh project</li>
    </ul>
    <p>Capitalise the phrase &ldquo;working group&rdquo; when it is included in the working group
      name, but not when the concept is referred to in general.&nbsp;</p>
    <p>Examples:</p>
    <ul>
      <li>Eclipse IoT Working Group</li>
      <li>Jakarta EE Working Group</li>
      <li>openGENESIS Working Group</li>
      <li>The working groups hosted at the Eclipse Foundation&hellip;</li>
    </ul>
    <h2 id="6">Referring to Communities</h2>
    <p>Use the word community to refer to the different groups that are associated with the Eclipse
      Foundation:</p>
    <ul>
      <li>The Eclipse Foundation is a community.</li>
      <li>Each project and working group hosted at the Eclipse Foundation is also a community.</li>
      <li>Each Eclipse Foundation community provides benefits to the ecosystem related to that
        technology area.</li>
    </ul>
    <p>Examples:</p>
    <ul>
      <li>We invite the entire Eclipse Foundation community to&hellip;</li>
      <li>The Eclipse Hono community will be hosting&hellip;</li>
      <li>The Jakarta EE community delivers benefits to the Java ecosystem&hellip;</li>
      <li>The Eclipse Edge Native Working Group focuses on edge technologies for the IoT
        ecosystem&hellip;</li>
    </ul>
    <h2 id="7">Referring to People</h2>
    <p>Remember that there are different ways to refer to people associated with the Eclipse
      Foundation and projects hosted by the foundation.</p>
    <p>The organisations and individuals who use the technologies are users.</p>
    <p>The organisations and individuals who incorporate technologies in their own products and
      projects are adopters.</p>
    <p>The developers who actively work on projects are contributors or committers.</p>
    <p>The people who are involved in working groups are members of the working group.</p>
    <p>Organisations that pay an annual fee to participate in the Eclipse Foundation are members of
      the Eclipse Foundation.</p>
    <p>Two general terms are also used to refer to people:</p>
    <ul>
      <li>Consumers are the people who use open source software, the adopters.</li>
      <li>Producers are the developers who create and enhance open source software.</li>
    </ul>
    <h1 id="8">Telling the Story</h1>
    <p>The first step in telling any Eclipse Foundation story is to identify the target audience and
      understand why they will be interested in the topic. Depending on the content type, audiences
      for marketing content may include:</p>
    <ul>
      <li>Software developers</li>
      <li>Software architects and decision-makers</li>
      <li>Product owners</li>
      <li>Chief Technology Officers and other C-level executives</li>
      <li>Academics and researchers</li>
      <li>Eclipse Foundation members</li>
      <li>Staff at industry publications</li>
      <li>Industry analysts</li>
      <li>Media</li>
    </ul>
    <p>Always tell the story from the audience perspective, not the Eclipse Foundation perspective:</p>
    <ul>
      <li>Instead of thinking, &ldquo;what are all of the things I want to tell the audience?&rdquo;
        think, &ldquo;what is it the audience needs to know?&rdquo;</li>
      <li>Present the information in the context of how the Eclipse Foundation helps the audience
        achieve its goals.</li>
    </ul>
    <h2 id="9">Organising Content to Capture Attention</h2>
    <p>Most people will not take the time to read an entire piece of content, so it needs to be
      written in a way that allows them to skim the content and get the main messages.</p>
    <p>Always start marketing content with the most important information for readers so they
      immediately understand why this story is relevant to them, similar to the way news items are
      presented. This writing style is called inverted pyramid, and it&rsquo;s widely considered to
      be the optimal approach for marketing content.</p>
    <p>To follow this style in Eclipse Foundation content, start by summarising the main challenge,
      issue, or trend associated with the topic and what&rsquo;s needed to address it. Start each
      section with a similar approach, summarising the key messages for the section before expanding
      on them in detail.</p>
    <p>Assume most people will stop reading after the first paragraph or two. This will help to
      identify the points that need to be made in the opening sentences. Imagine an editor deleting
      the content, sentence by sentence, from the bottom up to determine how to order the
      information.</p>
    <p>Use the body of the content to expand on the key points made in the opening paragraph and to
      provide statistics, examples, and other proof points that support the points. Describing
      real-world scenarios is often a good way to capture readers&rsquo; attention and help them
      visualise how the topic affects them.</p>
    <h2 id="10">Incorporating SEO Keywords</h2>
    <p>All content provides an opportunity for search engine optimisation (SEO) using keywords.
      Before writing, ask the marketing manager which keywords and phrases to focus on.</p>
    <p>Always include keywords in the following elements when you write Eclipse Foundation content:</p>
    <ul>
      <li>Main headings and webpage titles</li>
      <li>Subheads</li>
      <li>URLs</li>
      <li>Opening statements</li>
    </ul>
    <p>
      Keep in mind the content is for humans, not for Google. Don&rsquo;t sacrifice readability or
      clarity for the sake of keywords. Remember that using too many keywords too close together can
      actually lower the standing of your content in search results. To avoid this problem, use
      synonyms for keywords and be sure to include terms that help to put your keywords in context.
      This concept is called<a href="https://backlinko.com/hub/seo/lsi"> latent semantic indexing</a>.
    </p>
    <p>Depending on the project, you may also be asked to provide:</p>
    <ul>
      <li>Alt text for images</li>
      <li>Title tags</li>
      <li>Meta descriptions</li>
    </ul>
    <p>Each of these text elements should also include keywords.&nbsp;</p>
    <p>The marketing manager will work with the web team to ensure Eclipse Foundation URLs are short
      and include at least one keyword.</p>
    <p>
      If you are unsure about how to incorporate SEO keywords, the marketing manager can provide
      additional guidance. There are also numerous resources available on the web, including<a href="https://backlinko.com/on-page-seo"> this guide</a>.
    </p>
    <h2 id="11">Writing Effective Headings</h2>
    <p>Use the main heading, section headings, and subheadings throughout the content to clearly
      convey the key messages in the story. Ideally, readers can scan only the headings and still
      grasp the storyline and the key messages associated with it.</p>
    <p>Examples:</p>
    <ul>
      <li>Open Source Collaboration Drives Faster, Lower-Cost Innovation to Increase Competitiveness</li>
      <li>Eclipse Foundation Open Source Communities Give European Organisations Crucial Advantages</li>
      <li>The Eclipse Sparkplug Protocol Augments MQTT With IIoT Interoperability Essentials</li>
      <li>Jakarta EE Adoption and Compatible Implementations Are on the Rise</li>
    </ul>
    <p>When you&rsquo;re finished writing the content, go back to the beginning and read only the
      headings to determine whether they adequately convey the storyline. If they don&rsquo;t, you
      may need to add subheadings.</p>
    <p>Avoid using questions to try to capture readers&rsquo; attention. Most readers will
      immediately answer the question in their head, which may stop them from reading on. When
      tempted to write a heading that asks a question, consider the potential answers and whether
      they could stop readers from continuing:</p>
    <ul>
      <li>No.</li>
      <li>Yes.</li>
      <li>I don&rsquo;t care.</li>
      <li>That question doesn&rsquo;t apply to me.</li>
    </ul>
    <p>To encourage people to continue reading, reword the heading to emphasise the benefit for
      readers in the context of the story being told and avoid negative headings.</p>
    <p>Examples:</p>
    <ul>
      <li>Instead of &ldquo;Why Is Open Source Software Gaining Ground in Europe?&rdquo; use
        &ldquo;Why Open Source Software Is Gaining Ground in Europe.&rdquo;</li>
      <li>Instead of &ldquo;Are You Losing Time and Money With Proprietary Software?&rdquo; use
        &ldquo;How Open Source Software Saves Time and Money.&rdquo;</li>
    </ul>
    <h2 id="12">Writing for Easy Reading and Quick Scanning</h2>
    <p>Keep sentences to 25 words or less wherever possible, and keep paragraphs to 50 words or less
      if possible. Long sentences are difficult to parse and long paragraphs are off-putting for
      readers who are short on time or interest level. Vary sentence length within a paragraph to
      increase flow and readability.</p>
    <ul>
      <li><strong>Modifiers</strong>: Avoid combining more than two modifiers. Consider rewording in
        cases where three or more descriptors are required.</li>
      <li><strong>Bullets</strong>: Use bullets with parallel construction to highlight list items,
        especially for lists with three or more items. For more information, see <a href="#h.6ohc93azle1z">Lists</a>.</li>
      <li><strong>Bolding</strong>: Bold text to draw attention to important words in the body, such
        as the first two or three words in a bulleted list. Do not use italics as it is often
        difficult to read. Do not underline text as people will assume it is a hyperlink.</li>
      <li><strong>Contractions</strong>: Use contractions for a natural, more conversational
        approach to the content. Read sentences out loud to determine where contractions make the
        text sound less stilted.</li>
      <li><strong>Conjunctions</strong>: Don&rsquo;t be afraid to start sentences with And or But.
        This technique helps to create flow, and to keep sentences short, easy to read, and easy to
        understand.</li>
      <li><strong>Consistency</strong>: Always use the same terminology when referring to Eclipse
        Foundation offerings and capabilities and to industry trends and best practices. While
        company insiders may use multiple terms to mean the same thing, it&rsquo;s incredibly
        confusing for readers.</li>
      <li><strong>Tense</strong>: In general, active text in the present tense is fastest and
        easiest for people to grasp. However, including passive sentences and the past tense are
        also needed for readability. Just be conscious of where and when these sentences are used.</li>
    </ul>
    <h2 id="13">Including Proof Points</h2>
    <p>When including proof points, always focus on facts and start with the basics:</p>
    <ul>
      <li>Always assume some readers are being introduced to the Eclipse Foundation or to the topic
        for the first time.</li>
      <li>Use adjectives and adverbs with discretion. While these words can be very helpful for
        describing benefits, using too many bloats content and distracts from key messages. Always
        choose factual adjectives that can be quantified or substantiated in the text that follows.</li>
      <li>Quantify all claims with facts and numbers.</li>
      <li>Do not make promises or guarantees of any kind.</li>
      <li>Focus on the strengths and benefits of the Eclipse Foundation and its approach rather than
        disparaging other organisations and approaches. If you need to contrast and compare
        approaches, be generic when possible, referring only to &ldquo;other open source
        foundations&rdquo; or &ldquo;proprietary approaches.&rdquo; If direct comparisons must be
        made, keep them purely factual.</li>
    </ul>
    <h2 id="14">Incorporating Links</h2>
    <p>When the content refers to Eclipse Foundation initiatives, projects, or working groups,
      always embed the link to the relevant webpage in the reference.&nbsp;</p>
    <p>Avoid links that take readers off the Eclipse Foundation website. Links to GitHub pages for
      projects, YouTube channels, calendars, and other community-related pages are exceptions.</p>
    <p>
      When a project website is available (e.g. <a href="https://www.eclipse.org/kura/">eclipse.org/kura</a>),
      the best practice is to link to it instead of the project page (e.g. <a href="https://projects.eclipse.org/projects/iot.kura">projects.eclipse.org/projects/iot.kura</a>).
    </p>
    <p>Avoid using &ldquo;click here&rdquo; as the linked text. Search engines and humans use the
      linked text to help identify what the link is about, so the linked text should indicate where
      the link will lead.&nbsp;</p>
    <p>Examples:</p>
    <ul>
      <li>The<a href="https://www.eclipse.org/kura/"> Eclipse Kura</a> project is an IoT edge
        framework.
      </li>
      <li><a href="https://www.eclipsecon.org/2020">EclipseCon 2020</a> was our first fully virtual
        EclipseCon.</li>
      <li><a href="https://www.eclipse.org/org/workinggroups/">Eclipse Foundation working groups</a>
        foster open industry collaboration.</li>
      <li>Check out the complete list of <a href="https://www.youtube.com/playlist?list=PLy7t4z5SYNaSvv9SjVVi6-8SVdwpk-rxM">Jakarta EE TechTalks</a>.
      </li>
    </ul>
    <h2 id="15">Referencing Source Material</h2>
    <p>When information from published sources other than Eclipse Foundation is included, the
      information must be:</p>
    <ul>
      <li>Identified with a footnote or endnote reference.</li>
      <li>Described with a title, author, and publication date or issue, if available, and a link in
        the format shown below:
        <ul>
          <li>Open Digital Platforms for the Industrial World in Europe 2020, PAC RADAR Report, July
            2020:<a href="https://www.eurotech.com/en/page/open-digital-platforms-for-enterprise-iot-in-europe-2020">
              https://www.eurotech.com/en/page/open-digital-platforms-for-enterprise-iot-in-europe-2020</a>.
          </li>
        </ul>
      </li>
    </ul>
    <h1 id="16">Style and Punctuation</h1>
    <p>
      The Eclipse Foundation follows the Europa Interinstitutional Style Guide
      guidelines for grammar and punctuation. This section briefly summarises
      the guidelines for commonly used grammar and punctuation elements. For
      additional information, refer to the
      <a href="https://publications.europa.eu/code/en/en-000500.htm">Europa Style Guide</a>.
    </p>
    <h2 id="17">Abbreviations and Acronyms</h2>
    <p>Spell out each abbreviation and acronym the first time it is used in the content body unless
      the term is extremely familiar to the audience.</p>
    <p>Avoid using abbreviations and acronyms in headings unless expanding them would be too
      unwieldy or the acronyms are already widely used without expansion across industries and
      technologies. If the acronym is too unwieldy to spell out in a heading, expand it as early as
      possible in the content that follows.</p>
    <p>Examples of familiar acronyms that do not need to be spelled out include:</p>
    <ul>
      <li>API</li>
      <li>IDE</li>
      <li>IoT</li>
      <li>Protocols such as IP and HTTP</li>
      <li>LAN and WAN</li>
      <li>Technology names that include an acronym, such as a NoSQL database</li>
    </ul>
    <p>Avoid overloading content with acronyms.</p>
    <ul>
      <li>If a term or phrase will not be used again in a document, you may not need to include the
        acronym.</li>
      <li>If a term or phrase is commonly used in its expanded form, prefer the expanded form.</li>
      <li>In quotes, provide the expanded form of the acronym if it hasn&rsquo;t already been
        expanded in the content, but do not include the acronym in parentheses as this is not how
        people talk.&nbsp;</li>
    </ul>
    <p>Don&rsquo;t assume the audience is familiar with acronyms that are specific to the technology
      and processes at the Eclipse Foundation. If you&rsquo;re not sure how a phrase is used, a
      quick internet search often helps. For example, searching on the phrase OSS technology returns
      references to operations support systems, not open source software.</p>
    <p>To avoid confusion:</p>
    <ul>
      <li>Always spell out open source software.</li>
      <li>Be very judicious when using acronyms that &ldquo;bury&rdquo; the Eclipse brand, such as
        the Eclipse Cloud DevTools (ECD) Working Group or Eclipse Modeling Framework (EMF).&nbsp;</li>
      <li>When using the acronym IP in the context of intellectual property, always spell out the
        phrase first.</li>
      <li>When a project, working group, or event name includes an acronym, present the official
        project name, but include an expansion of the acronym as close as possible to the name. For
        example:
        <ul>
          <li>Eclipse Cyclone DDS implements the Data Distribution Service (DDS) specification.</li>
          <li>The Eclipse EMF Client Platform is a framework for building client applications that
            are based on the Eclipse Modeling Framework (EMF).&nbsp;</li>
          <li>Open Code Experience (OCX) will feature multi-day collocated events focused on the core pillars of the Eclipse Foundation ecosystem.</li>
        </ul>
      </li>
    </ul>
    <p>In longer content, and when less familiar acronyms are used, it will likely make sense to
      expand abbreviations and acronyms more than once to help readers recall what it means.&nbsp;</p>
    <p>Be careful not to repeat words that are included in the acronym. For example, do not use:</p>
    <ul>
      <li>API interface</li>
      <li>PDF format</li>
    </ul>
    <p>Capitalise the expanded term only if it is a proper name.</p>
    <h2 id="18">Apostrophes</h2>
    <p>The examples below demonstrate correct use of apostrophes.</p>
    <ul>
      <li>When a software vendor&rsquo;s implementation is based on&hellip;</li>
      <li>When software vendors&rsquo; implementations are based on&hellip;</li>
      <li>When software vendors implement specifications&hellip;&nbsp;</li>
    </ul>
    <p>Remember:</p>
    <ul>
      <li>Never use an apostrophe with a trademarked term.</li>
      <li>It&rsquo;s is always a contraction for it is. Its refers back to a noun.
        <ul>
          <li>It&rsquo;s important to focus time and attention on the demonstration unit, its
            performance, and functionality.</li>
        </ul>
      </li>
    </ul>
    <h2 id="40">Business Entities</h2>
    <p>When introducing a company, the general practice is to avoid using the legal entity in the
      business&rsquo; name in announcements, blog posts and press releases.&nbsp;</p>
    <p>Example:</p>
    <ul>
      <li>Red Hat, not Red Hat, Inc.</li>
    </ul>
    <p>
      Only when using a company name in a list or directory (e.g. <a href="https://www.eclipse.org/membership/exploreMembership.php">Explore Our Members page</a>) is the legal entity used.
    </p>
    <h2 id="19">Capitalisation</h2>
    <p>In main headings and subheadings, capitalise each word, except the following prepositions
      when they occur in the middle of the heading:</p>
    <ul>
      <li>a, an, and, at, but, by, for, in, nor, of, on, or, so, the, to, up, yet</li>
    </ul>
    <p>Capitalise a preposition in a title if it:</p>
    <ul>
      <li>Is used as an adjective or an adverb in a title.</li>
      <li>Has four or more letters, such as the word with.</li>
      <li>Is part of a project or working group name.</li>
    </ul>
    <p>Always maintain the capitalisation used in official project and working group names.&nbsp;</p>
    <p>Examples:</p>
    <ul>
      <li>Edge Robotics With Eclipse zenoh and ROS 2</li>
      <li>Following an Open Source Path to the Eclipse Foundation</li>
      <li>2019 Was a Big Year for Eclipse Kuksa</li>
      <li>Eclipse ioFog 2.0 Is Just the Beginning</li>
    </ul>
    <p>In the body of text, capitalise the names of people, places, and geographic regions.</p>
    <h2 id="20">Commas</h2>
    <p>
      Always use serial (Oxford) commas. Although the Interinstitutional Style
      Guide indicates that a serial comma is not required to separate elements
      in a simple series, including the serial comma in all cases helps to avoid
      judgement errors.
    </p>
    <h2 id="21">Compound Words and Hyphenation</h2>
    <p>Use hyphens to join compound adjectives when they are used before a noun, but not in other
      cases.</p>
    <p>Examples:</p>
    <ul>
      <li>Jakarta EE 8-compatible products.</li>
      <li>Products compatible with Jakarta EE 8.</li>
      <li>Real-time access to information.</li>
      <li>Access to information in real time.</li>
    </ul>
    <p>Follow these conventions for commonly used Eclipse Foundation terms:</p>
    <ul>
      <li>Never include a hyphen in open source as this phrase is treated as a collective noun:
        <ul>
          <li>Open source software</li>
          <li>Open source software developers</li>
          <li>Open source initiative</li>
        </ul>
      </li>
      <li>Cloud native should not be hyphenated as cloud-native</li>
      <li>Do include a hyphen when the phrase vendor neutral is used as a modifier:
        <ul>
          <li>The Eclipse Foundation provides vendor-neutral governance.</li>
          <li>A vendor-neutral approach ensures &hellip;</li>
          <li>With governance that is vendor neutral, the Eclipse Foundation&hellip;</li>
        </ul>
      </li>
    </ul>
    <p>Do not use a hyphen after adverbs ending in ly or er.</p>
    <p>Examples:</p>
    <ul>
      <li>Highly flexible architecture</li>
      <li>Fully automated control</li>
      <li>Higher capacity solution</li>
    </ul>
    <h2 id="22">Contractions</h2>
    <p>Use contractions for a natural, more conversational approach to the content. Read sentences
      out loud to determine where contractions make the text sound less stilted. Usually a
      combination of contractions and expansions sounds most natural.</p>
    <h2 id="23">Currencies</h2>
    <p>Include numbers and symbols to specify amounts. When referring to currencies in general, use
      lower case.</p>
    <p>Examples:</p>
    <ul>
      <li>The industry experienced a $4 billion increase in&hellip;.</li>
      <li>According to the report, &euro;42 million will be needed to resolve&hellip;</li>
      <li>Millions of dollars were invested in the project.</li>
      <li>According to the report, millions of euros will be needed to resolve&hellip;</li>
    </ul>
    <h2 id="24">Dashes and Slashes</h2>
    <p>An en dash is equivalent to a hyphen. An en dash is used to indicate number ranges and to act
      as a kind of super-hyphen for compound modifiers.&nbsp;</p>
    <p>The AP Stylebook does not deal with em dashes. These longer dashes are typically used to
      expand on information in a sentence. They should be used judiciously as they can make
      sentences overly long and complex. When used, include a space on each side of the em dash to
      adequately separate the information with the two dashes from the rest of the sentence.</p>
    <p>Verify that the sentence is still clear if the information between the dashes is removed.</p>
    <p>Example:</p>
    <ul>
      <li>Companies across industries &mdash; from hardware design to aerospace, insurance, and
        finance &mdash; are embracing open source software.</li>
    </ul>
    <p>Do not use a slash between words. For clarity, use &ldquo;and&rdquo; or &ldquo;or.&rdquo;</p>
    <h2 id="25">Date and Time</h2>
    <p>When the date includes the day, month, date, and year:</p>
    <ul>
      <li>Spell out the name of day and month.</li>
      <li>Do not use ordinal number suffixes such as 1st, 2nd, 3rd, and 4th.</li>
    </ul>
    <p>Examples:</p>
    <ul>
      <li>The announcement was made on Wednesday 23 September 2020.</li>
      <li>The announcement was made on 10 March 2020.</li>
    </ul>
    <p>If a date range is provided, use a hyphen with no space on each side to separate dates.</p>
    <p>Examples:</p>
    <ul>
      <li>21-22 July 2020</li>
    </ul>
    <p>
      Only use fully numeric dates when absolutely necessary due to space
      restrictions. In these rare cases, use the format DD/MM/YY. When only
      month and year is permitted, use YYYY-MM.
    </p>
    <p>Example:</p>
    <ul>
      <li>21/9/20 for 21 September 2020</li>
      <li>2020-09 for September 2020</li>
    </ul>
    <p>Use the 24-hour system (or 12-hour system with a.m. and p.m.):</p>
    <ul>
      <li>
        17.30 without h or hrs (or 5.30 p.m.) (always use a point)
      </li>
      <li>
        Avoid leading zeros (9.00, not 09.00)
      </li>
      <li>
        The full hour is written with zero minutes: 12.00 (midday), 14.00,
        24.00 (midnight). When using the 12-hour system, write 2 p.m., 2
        o'clock or 2.30 p.m., but not 2.00 p.m.
      </li>
    </ul>
    <h2 id="26">Email, Website, and Social Media References</h2>
    <p>Write email and website addresses in lowercase characters. Do not include www.</p>
    <p>Examples:</p>
    <ul>
      <li><a href="mailto:membership@eclipse.org">membership@eclipse.org</a></li>
      <li><a href="https://www.eclipse.org/membership/">eclipse.org/membership</a></li>
      <li><a href="https://www.eclipse.org/">eclipse.org</a></li>
    </ul>
    <p>Embed the email address or URL into the text. If the address or URL is at the end of a
      sentence, ensure the period is not included in the address.&nbsp;</p>
    <p>Examples:</p>
    <ul>
      <li>To learn more, visit<a href="https://www.eclipse.org/membership/"> eclipse.org/membership</a>.
      </li>
      <li>For more information, email membership@eclipse.org.&nbsp;</li>
    </ul>
    <p>Whenever possible, embed website addresses into the text as a hyperlink.</p>
    <p>Examples:</p>
    <ul>
      <li>The technologies hosted at<a href="https://iot.eclipse.org/"> Eclipse IoT</a> power the
        world&rsquo;s leading commercial IoT solutions.
      </li>
      <li>With<a href="https://theia-ide.org/"> Eclipse Theia</a>, developers can create cloud and
        desktop IDEs.
      </li>
    </ul>
    <p>Link to the website for the project if one exists rather than the Eclipse Foundation project
      page:</p>
    <ul>
      <li><a href="https://theia-ide.org/">Eclipse Theia</a> rather than<a href="https://projects.eclipse.org/projects/ecd.theia"> Eclipse Theia</a>.</li>
    </ul>
    <p>Format general social media and web references as follows:</p>
    <ul>
      <li>To stay connected with the Eclipse Foundation, follow us on social media
        <a href="https://twitter.com/EclipseFdn">@EclipseFdn</a>,
        <a href="https://www.linkedin.com/company/eclipse-foundation">LinkedIn</a>,
        or visit <a href="https://www.eclipse.org/">eclipse.org</a>.
      </li>
    </ul>
    <h2 id="28">Exclamation Marks</h2>
    <p>Avoid using exclamation marks. They weaken the professional voice. They also make text sound
      overly eager and self-promotional.</p>
    <h2 id="29">Geographic Names</h2>
    <p>In general, Eclipse Foundation content is written from a global perspective and specific
      locations are not identified. However, when locations do need to be identified:</p>
    <ul>
      <li>Capitalise geographic locations, even when they are regions.</li>
      <li>Examples:
        <ul>
          <li>Western Hemisphere</li>
          <li>East Coast</li>
          <li>Middle East</li>
          <li>The Balkans</li>
          <li>Sub-Saharan Africa</li>
        </ul>
      </li>
      <li>Use the following country abbreviations, even on first reference:
        <ul>
          <li>U.S. for United States. Do not use USA or United States of America unless it is
            required in the context of the content.</li>
          <li>UK for United Kingdom.</li>
        </ul>
      </li>
      <li>Always spell out continent names in full.</li>
    </ul>
    <h2 id="30">Language and Spelling</h2>
    <p>
      Write all content in British English following the spelling in the <a href="https://www.oed.com/">Oxford English Dictionary</a>.
      When two spellings are provided, use the first entry.
    </p>
    <p>However, keep in mind the audience for Eclipse Foundation content is typically global, so do
      not include words, terms, or phrases that would be unfamiliar to people who don&rsquo;t have
      English as a first language.</p>
    <p>When choosing words and phrases, always choose those that are the most accurate and
      meaningful for the target audience. While we can assume some level of technical understanding
      in all audiences, that level will vary depending on the content type. For example, the
      language used in an in-depth technical article will differ from the language used in a press
      release or annual report.</p>
    <h2 id="31">Latinisms</h2>
    <p>Do not include Latinisms such as e.g., i.e., etc. as they may not be understood by all
      readers, especially those who do not have a strong background in English.&nbsp;</p>
    <p>Use:</p>
    <ul>
      <li>For example, instead of e.g.</li>
      <li>That is, instead of i.e.</li>
      <li>And more, instead of etc.</li>
    </ul>
    <h2 id="32">Lists</h2>
    <p>A list is needed when there are more than three items and when each list item is long enough
      that it is difficult to read in a sentence &mdash; usually four or five words.</p>
    <p>Use bullets to identify list items unless a specific order of operations is required. In
      simple running lists, put list items in alphabetical order.</p>
    <p>Always introduce lists with a sentence or short phrase that ends with a colon.</p>
    <p>Start each list item with a capital letter on a separate line.</p>
    <p>Use parallel construction for each item in the list:</p>
    <ul>
      <li>Use only complete sentences or incomplete sentences.</li>
      <li>Start every list item with the same part of speech, such as a verb or a
        noun.</li>
      <li>Use the same voice (active or passive) and verb tense.&nbsp;</li>
    </ul>
    <p>Put the most important words for the reader to grasp at the beginning of the list item.</p>
    <p>Include a period at the end of the list items that are complete sentences. Do not use any
      punctuation at the end of list items that are incomplete sentences.&nbsp;</p>
    <p>Keep lists to seven items or less if possible. If it is not possible, consider categorising
      list items or using sub-bullets.</p>
    <p>Avoid creating lists of questions. Rephrase the list introduction and items so they form
      statements.</p>
    <h2 id="33">Names and Job Titles</h2>
    <p>When it is necessary to refer to specific individuals, capitalise their job title when it is
      used before their name in a formal way, but use lowercase when it is included after their name
      and when referring to the role in general.</p>
    <p>Examples:</p>
    <ul>
      <li>Company President and CEO, Joe Smith, said he was&hellip;</li>
      <li>Joe Smith, the president and CEO of the company, said he was&hellip;</li>
      <li>As president and CEO of the company, Joe Smith said he was&hellip;</li>
    </ul>
    <h2 id="34">Numbers, Statistics, and Prices</h2>
    <p>Write out the numbers zero to nine. Use numerals for numbers 10 and higher. Do not include
      suffixes such as st, nd, rd, or th on numbers.</p>
    <p>Use a comma to separate numbers that have four or more digits.</p>
    <p>Use dots in telephone numbers, even for the area code. Always include the country code with a
      + symbol.</p>
    <p>Example: +1.123.555.6789</p>
    <p>When making general references to numbers in marketing content, it often makes sense to round
      them up or simply indicate order of magnitude if the number is likely to change. Always
      provide the specific number when referring to technical advances.</p>
    <p>Examples:</p>
    <ul>
      <li>The Eclipse Foundation hosts more than 410 open source software projects.</li>
      <li>The results were 99.9 percent accurate.</li>
      <li>The software is 12 times faster than previous versions.</li>
      <li>Our membership increased by 25 percent.</li>
    </ul>
    <h2 id="35">Periods, Colons, and Semicolons</h2>
    <p>Use a period to end a complete sentence, but not an incomplete sentence such as a list item.</p>
    <p>Always use a colon to introduce a list.</p>
    <p>In running text, capitalise the first word after the colon only if it is a proper noun or the
      start of a complete sentence.</p>
    <p>Examples:</p>
    <ul>
      <li>Eclipse Foundation: The home of entrepreneurial open source software.</li>
      <li>Eclipse Foundation members enjoy key benefits: intellectual property services, marketing
        and IT services, and mentorship.&nbsp;</li>
    </ul>
    <p>Avoid writing sentences that include a semicolon. Use two sentences instead. If you must use
      a semicolon, use it as a greater separation of thought than a comma, but less than a period.</p>
    <p>Example:</p>
    <ul>
      <li>Complete sentences should be punctuated; incomplete sentences should not.</li>
    </ul>
    <h2 id="36">Quotation Marks</h2>
    <p>In general, use quotation marks only when quoting someone.</p>
    <p>Put all punctuation inside quotation marks, making it clear who is being quoted.</p>
    <p>Example:</p>
    <ul>
      <li>&ldquo;The open source software at the Eclipse Foundation has been important to our
        business success,&rdquo; says Mary Smith. &ldquo;We plan to get involved in more projects
        and encourage our developers to become committers.&rdquo;</li>
    </ul>
    <p>Quotation marks should only be used when quoting a complete thought, and are not necessary
      when repeating a few select words from a larger quotation.&nbsp;</p>
    <p>Example:&nbsp;</p>
    <ul>
      <li><strong>Avoid</strong>: Smith said he &ldquo;saw an opportunity&rdquo; to contribute from
        a technology perspective.</li>
      <li><strong>Instead</strong>: Smith said he saw an opportunity to contribute from a technology
        perspective.</li>
    </ul>
    <p>Avoid using quotation marks to call attention to words that are likely to be unfamiliar to
      readers or to highlight a less formal term or phrase. Instead, put unfamiliar and less formal
      words in context to explain their meaning, or use bold to emphasise them.&nbsp;</p>
    <p>In some cases, it may be necessary to use quotation marks to separate a particular word or
      phrase from the surrounding text, but always take a moment to confirm there is no better way
      to communicate the thought.</p>
    <h2 id="37">Symbols</h2>
    <p>Always write out the word percent.</p>
    <p>Use ampersands only when they are part of an official name. Otherwise, always use
      &ldquo;and.&rdquo;</p>
    <p>Avoid putting information in parentheses unless you are expanding an acronym or abbreviation.
      In general, information in parentheses in running text tells the reader they can ignore the
      information. Reword the sentence or find another way to communicate the information in
      parentheses.</p>
    <h2 id="38">Tables</h2>
    <p>Tables are usually used to show a data set or compare categories of information. They are
      often a very clear way to summarise and present complex information.</p>
    <p>When creating a table:</p>
    <ul>
      <li>Organise the structure so that all, or almost all, cells will have information in them</li>
      <li>Don&rsquo;t leave cells blank. Include the words &ldquo;Not applicable.&rdquo;</li>
      <li>Indicate units of measure in the column heading rather than each cell if every cell uses
        the same units.</li>
      <li>Follow the <a href="#h.dds8zk4m0vi2">capitalisation guidelines</a> in column headings.
      </li>
    </ul>
    <p>If notes are needed to qualify information in table cells, manually create a superscript
      footnote reference within the cell and provide the explanation directly below the table using
      the same superscript number.</p>
    <h2 id="39">Technical Standards</h2>
    <p>When referring to technical standards, include the standard title or a description of what
      the standard covers along with the standard number the first time it is referenced. When
      referring to multiple standards, using a bulleted list to convey this information is usually
      the best approach.</p>
    <p>Double-check capitalisation, hyphenation, spacing, and letter revision in the names of
      standards on the standards webpage as these aspects of the official standard name are often
      dropped in casual references.</p>
  </div>
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
        <li><a href="../membership/">Membership</a></li>
        <li><a href="../membership/become_a_member/">Become a member</a></li>
      </ul>
    </div>
    <div class="sideitem">
      <h6>Thank you!</h6>
      <p>
        Thanks to our many <a href="/corporate_sponsors/">Corporate Sponsors</a>
        for their generous donations to our infrastructure.
      </p>
    </div>
  </div>
</div>
