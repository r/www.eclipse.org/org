<?php

/**
 * Copyright (c) 2015, 2019 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Mike Milinkovic (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation) - Update to the latest Contributor Covenant v1.4
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="maincontent">
  <div id="midcolumn">
    <h1 id="community-code-of-conduct">Eclipse Foundation <?php print $pageTitle; ?></h1>
    <p>Version 1.0.</p>
    <div class="homeitem3col">

      <h2 id="purpose">Purpose</h2>
      <p>
        The Eclipse Foundation is a membership-based steward of hundreds of open source projects, working groups, and events. By design we want to enable and empower our many community members to act as spokespersons for their projects or activities. These guidelines are intended to provide guidance to the many community members who represent our projects, working groups, and events through various social media channels.
      </p>

      <h2 id="introduction">Introduction</h2>
      <p>
        In addition to these Guidelines, the <a href="/org/documents/Community_Code_of_Conduct.php">Eclipse Foundation Code of Conduct</a> and <a href="/org/documents/communication-channel-guidelines/">Eclipse Foundation Communication Channel Guidelines</a> apply to all social media activities.
      </p>
      <p>
        The Eclipse Foundation recognizes the importance of the internet in shaping public thinking about our projects, working groups, initiatives, and virtual or in-person events such as EclipseCon or JakartaOne Livestream, each referred to as an “Eclipse Activity”. We also recognize the importance of, and want to encourage, contributions made by our community members to the open source industry discourse and direction through interaction in social media.
      </p>
      <p>
        Engaging in social media on behalf of an Eclipse Activity (that is, as an official representative of the Eclipse Activity) is a privilege, and comes with a great deal of responsibility. These Guidelines are intended to help us make appropriate decisions about the use of social media for official accounts on platforms such as Twitter, LinkedIn, Facebook, and Slack. For the purposes of this document, social media also refers to any service that facilitates conversations online, such as blogs, comments on news articles, YouTube, Reddit, and wikis.
      </p>

      <h2 id="representatives">Representatives</h2>
      <p>
        Individuals who hold official status with an Eclipse Activity (e.g., project committers, working group committee members, selection committee members, etc.) represent the interests of the Eclipse Activity on social media when they are using a designated official channel.
      </p>
      <p>
        When representatives use social media on behalf of the Eclipse Activity, they must adhere to the <a href="/org/documents/Community_Code_of_Conduct.php">Eclipse Foundation Code of Conduct</a> and the <a href="/org/documents/communication-channel-guidelines/">Eclipse Foundation Communication Channel Guidelines</a>.
      </p>
      <p>
        Social Media Guidelines for Eclipse Activity Representatives:
      </p>
      <ol>
        <li>
          <p>Must not express personal opinions when using social media on behalf of an Eclipse Activity.</p>
        </li>
        <li>
          <p>Must not, under any circumstances post ethnic slurs, personal insults, obscenity, offensive, or inflammatory content.</p>
        </li>
        <li>
          <p>Must not upload, post, forward or post a link to any abusive, obscene, discriminatory, harassing, derogatory, or defamatory content.</p>
        </li>
        <li>
          <p>Must never disclose sensitive, private or confidential information.</p>
        </li>
        <li>
          <p>Must not post images or videos relating to other community members, including other Committers, committee members, etc. anywhere without their express permission, including images or recordings of Eclipse Activity meetings. Permission must be obtained in advance via any durable record that captures the identity of the individuals granting permission and is maintained by the Eclipse Foundation, such as a public and archived mailing list or an issue on a GitHub repository or wiki.</p>
        </li>
        <li>
          <p>Must make a genuine attempt to address any differences or controversies with others within the Eclipse Activity first to address matters for long term solutions instead of airing them first online.</p>
        </li>
      </ol>
      <p>
        If you are unsure whether the information you wish to share falls within one of these restricted categories, you may solicit an opinion by reaching out to <a href="mailto:codeofconduct@eclipse.org">codeofconduct@eclipse.org</a>.
      </p>

      <h2 id="Official Channels">Official Channels</h2>
      <p>An Eclipse Activity may, at their discretion, designate official communication channels for their Eclipse Activity.</p>
      <p>To create and manage official channels, the Eclipse Activity’s team must designate members as Social Administrators.</p>
      <ul>
        <li>
          <p>Social Administrators must have an official status with the Eclipse Activity (e.g., committer, project lead, PMC member, working group committee representative, event organizing committee member)</p>
        </li>
        <li>
          <p>
            Only designated Social Administrators may have administrative access to official communication channels. Eclipse Foundation’s Webmaster should be made a Social Administrator.
          </p>
        </li>
        <li>
          <p>
            The Eclipse Activity’s team must establish an open and transparent process for designating Social Administrator status. In most cases this can be done by discussing the decision on an open mailing list associated with the Eclipse Activity.
          </p>
        </li>
      </ul>
      <p>To mitigate against security risks, follow account security and privacy best practices recommended by the respective social media platforms. It is recommended to change account passwords at least every six months.</p>

      <h2 id="inappropriate-content-and-behaviour">Inappropriate Content and Behaviour</h2>
      <p>
        Social Administrators are responsible for removing content that is inappropriate, falls within one of the restricted categories above, or significantly harms the reputation of the Eclipse Activity. The Eclipse Foundation Executive Director has the final say in any decision relating to removing content.
      </p>
      <p>
        Any member of the community who feels that they have been harassed or bullied, or are offended by material posted or uploaded by a community member onto a social media website should report it through the Eclipse Community Code of Conduct.
      </p>

      <h2 id="consequences-of-unacceptable-behavior">Consequences of Unacceptable Behavior</h2>
      <p>
        If an Eclipse Activity Representative or Social Administrator fails to follow these Guidelines, Eclipse Foundation staff and/or Eclipse Activity Leadership (e.g., Project Leadership, Working Group Steering Committee, event organizing committee), may take any action deemed appropriate, up to and including a warning or temporary thirty day ban, along with a permanent ban for repeated or egregious violations of these guidelines.
      </p>
      <p>
        Unless otherwise determined by the Eclipse Foundation via the Code of Conduct resolution process, consequences will escalate as follows:
      </p>
      <ul>
        <li>
          <p>
            For a first offense, a warning will be issued.
          </p>
        </li>
        <li>
          <p>
            For a second offense, the individual will be banned from representing Eclipse Activities on social media for thirty days.
          </p>
        </li>
        <li>
          <p>
            For a third offense, the individual will receive a permanent ban from representing Eclipse Activities on social media.
          </p>
        </li>
      </ul>
    </div>
  </div>
  <br>
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
        <li><a href="https://raw.githubusercontent.com/eclipse/.github/master/CODE_OF_CONDUCT.md">Community Code of Conduct (Markdown)</a></li>
        <li><a href="/org/foundation/directors.php">Board of Directors</a></li>
        <li><a href="/membership/">Membership</a></li>
        <li><a href="/membership/become_a_member/">Become a member</a></li>
      </ul>
    </div>
  </div>
</div>