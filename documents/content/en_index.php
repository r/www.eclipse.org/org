<?php
/**
 * Copyright (c) 2005, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Mike Milinkovich (Eclipse Foundation)
 * Christopher Guindon (Eclipse Foundation)
 * Eric Poirier (Eclipse Foundation)
 */
?>
<div id="maincontent">
  <h1><?php echo $pageTitle; ?></h1>
  <p>The basic governance of the Eclipse Foundation is laid out in the
    following governance documents, processes, and additional policies.</p>
  <?php foreach ($documents as $group => $documents) { ?>
    <h3><?php print $group;?></h3>
    <table class="table">
      <thead>
        <tr>
          <th>Title</th>
          <th>Description</th>
          <th colspan="2"></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($documents as $Document) {print $Document->getTableRow();}?>
      </tbody>
    </table>
  <?php }?>
</div>

<div id="rightcolumn">
  <div class="sideitem">
    <h6>Related Links</h6>
    <ul>
      <li><a href="../../membership/">Membership</a></li>
      <li><a href="../../membership/become_a_member/">Become a member</a></li>
    </ul>
  </div>
</div>