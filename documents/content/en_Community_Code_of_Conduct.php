<?php
/**
 * Copyright (c) 2015, 2019, 2022 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Mike Milinkovic (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation) - Update to the latest Contributor Covenant v1.4
 *    Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div id="maincontent">
  <div id="midcolumn">
    <h1 id="community-code-of-conduct"><?php print $pageTitle;?></h1>
    <p><strong>Version 2.0<br>January 1, 2023</strong>
    </p>
    <div class="homeitem3col">

      <h2 id="our-pledge">Our Pledge</h2>
      <p>
        <img class="pull-right" src="../images/eclipse_code_of_conduct.png" 
          width="246" style="padding: 0px 5px 0px 5px;"
        /> 
        In the interest of fostering an open and welcoming environment, we as community members, 
        contributors, Committers<sup><a href="#footnote1">[1]</a></sup>, and Project Leads (collectively &ldquo;Contributors&rdquo;) 
        pledge to make participation in our projects and our community a harassment-free and 
        inclusive experience for everyone.
      </p>
      <p>
        This Community Code of Conduct (&ldquo;Code&rdquo;) outlines our behavior expectations as members of our 
        community in all Eclipse Foundation activities, both offline and online. It is not intended to 
        govern scenarios or behaviors outside of the scope of Eclipse Foundation activities. Nor is it 
        intended to replace or supersede the protections offered to all our community members under the 
        law. Please follow both the spirit and letter of this Code and encourage other Contributors to 
        follow these principles into our work. Failure to read or acknowledge this Code does not excuse 
        a Contributor from compliance with the Code.
      </p>

      <h2 id="our-standards">Our Standards</h2>
      <p>Examples of behavior that contribute to creating a positive and professional environment include:</p>
      <ul>
        <li>Using welcoming and inclusive language;</li>
        <li>Actively encouraging all voices;</li>
        <li>Helping others bring their perspectives and listening actively. If you find yourself dominating a discussion, it is especially important to encourage other voices to join in;</li>
        <li>Being respectful of differing viewpoints and experiences;</li>
        <li>Gracefully accepting constructive criticism;</li>
        <li>Focusing on what is best for the community;</li>
        <li>Showing empathy towards other community members;</li>
        <li>Being direct but professional; and</li>
        <li>Leading by example by holding yourself and others accountable</li>
      </ul>
      <p>Examples of unacceptable behavior by Contributors include:</p>
      <ul>
        <li>The use of sexualized language or imagery;</li>
        <li>Unwelcome sexual attention or advances;</li>
        <li>Trolling, insulting/derogatory comments, and personal or political attacks;</li>
        <li>Public or private harassment, repeated harassment;</li>
        <li>Publishing others' private information, such as a physical or electronic address, without explicit permission;</li>
        <li>Violent threats or language directed against another person;</li>
        <li>Sexist, racist, or otherwise discriminatory jokes and language;</li>
        <li>Posting sexually explicit or violent material;</li>
        <li>Sharing private content, such as emails sent privately or non-publicly, or unlogged forums such as IRC channel history;</li>
        <li>Personal insults, especially those using racist or sexist terms;</li>
        <li>Excessive or unnecessary profanity;</li>
        <li>Advocating for, or encouraging, any of the above behavior; and</li>
        <li>Other conduct which could reasonably be considered inappropriate in a professional setting.</li>
      </ul>

      <h2 id="our-responsibilities">Our Responsibilities</h2>
      <p>
        With the support of the Eclipse Foundation employees, consultants, officers, and directors (collectively, 
        the &ldquo;Staff&rdquo;), Committers, and Project Leads, the Eclipse Foundation Conduct Committee 
        (the &ldquo;Conduct Committee&rdquo;) is responsible for clarifying the standards of acceptable behavior. The Conduct 
        Committee takes appropriate and fair corrective action in response to any instances of unacceptable 
        behavior.
      </p>

      <h2 id="scope">Scope</h2>
      <p>
        This Code applies within all Project, Working Group, and Interest Group spaces and communication channels 
        of the Eclipse Foundation (collectively, &ldquo;Eclipse spaces&rdquo;), within any Eclipse-organized event 
        or meeting, and in public spaces when an individual is representing an Eclipse Foundation Project, Working 
        Group, Interest Group, or their communities. Examples of representing a Project or community include 
        posting via an official social media account, personal accounts, or acting as an appointed representative 
        at an online or offline event. Representation of Projects, Working Groups, and Interest Groups may be 
        further defined and clarified by Committers, Project Leads, or the Staff.
      </p>

      <h2 id="enforcement">Enforcement</h2>
      <p>
        Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by contacting the 
        Conduct Committee via <a href="mailto:conduct@eclipse-foundation.org">conduct@eclipse-foundation.org</a>. 
        All complaints will be reviewed and investigated and will result in a response that is deemed necessary 
        and appropriate to the circumstances. Without the explicit consent of the reporter, the Conduct Committee 
        is obligated to maintain confidentiality with regard to the reporter of an incident. The Conduct Committee 
        is further obligated to ensure that the respondent is provided with sufficient information about the 
        complaint to reply. If such details cannot be provided while maintaining confidentiality, the Conduct 
        Committee will take the respondent&lsquo;s inability to provide a defense into account in its 
        deliberations and decisions. Further details of enforcement guidelines may be posted separately.
      </p>
      <p>
        Staff, Committers and Project Leads have the right to report, remove, edit, or reject comments, commits, 
        code, wiki edits, issues, and other contributions that are not aligned to this Code, or to block 
        temporarily or permanently any Contributor for other behaviors that they deem inappropriate, 
        threatening, offensive, or harmful. Any such actions will be reported to the Conduct Committee 
        for transparency and record keeping.
      </p>
      <p>
        Any Staff (including officers and directors of the Eclipse Foundation), Committers, Project Leads, or 
        Conduct Committee members who are the subject of a complaint to the Conduct Committee will be recused 
        from the process of resolving any such complaint.
      </p>

      <h2 id="responsibility">Responsibility</h2>
      <p>
        The responsibility for administering this Code rests with the Conduct Committee, with oversight by the 
        Executive Director and the Board of Directors. For additional information on the Conduct Committee and its
        process, please write to <a href="mailto:conduct@eclipse-foundation.org">conduct@eclipse-foundation.org</a>.
      </p>

      <h2 id="code-violations">Investigation of Potential Code Violations</h2>
      <p>
        All conflict is not bad as a healthy debate may sometimes be necessary to push us to do our best. It is, 
        however, unacceptable to be disrespectful or offensive, or violate this Code. If you see someone engaging 
        in objectionable behavior violating this Code, we encourage you to address the behavior directly with those 
        involved. If for some reason, you are unable to resolve the matter or feel uncomfortable doing so, or if 
        the behavior is threatening or harassing, please report it following the procedure laid out below.
      </p>
      <p>
        Reports should be directed to <a href="mailto:conduct@eclipse-foundation.org">conduct@eclipse-foundation.org</a>. 
        It is the Conduct Committee&rsquo;s role to receive and address reported violations of this Code and to ensure a 
        fair and speedy resolution.
      </p>
      <p>
        The Eclipse Foundation takes all reports of potential Code violations seriously and is committed to confidentiality 
        and a full investigation of all allegations. The identity of the reporter will be omitted from the details of the 
        report supplied to the accused.  Contributors who are being investigated for a potential Code violation will have an 
        opportunity to be heard prior to any final determination. Those found to have violated the Code can seek reconsideration 
        of the violation and disciplinary action decisions.  Every effort will be made to have all matters disposed of within 60 
        days of the receipt of the complaint.
      </p>

      <h2 id="actions">Actions</h2>
      <p>
        Contributors  who do not follow this Code in good faith may face temporary or permanent repercussions as determined by 
        the Conduct Committee.
      </p>
      <p>
        This Code does not address all conduct. It works in conjunction with our 
        <a href="/org/documents/communication-channel-guidelines/">Communication Channel Guidelines</a>, 
        <a href="/org/documents/social_media_guidelines.php">Social Media Guidelines</a>, 
        <a href="/org/documents/eclipse-foundation-be-bylaws-en.pdf">Bylaws</a>, 
        and <a href="/org/documents/ef-be-internal-rules.pdf">Internal Rules</a> 
        which set out additional protections for, and obligations of, all contributors. The Foundation has additional policies 
        that provide further guidance on other matters.  
      </p>
      <p>
        It&rsquo;s impossible to spell out every possible scenario that might be deemed a violation of this Code. 
        Instead, we rely on one another&rsquo;s good judgment to uphold a high standard of integrity within all Eclipse Spaces. 
        Sometimes, identifying the right thing to do isn&rsquo;t an easy call. In such a scenario, raise the issue as early 
        as possible.
      </p>

      <h2 id="no-retaliation">No Retaliation</h2>
      <p>
        The Eclipse community relies upon and values the help of Contributors who identify potential problems that may need 
        to be addressed within an Eclipse Space. Any retaliation against a Contributor who raises an issue honestly is a 
        violation of this Code. That a Contributor has raised a concern honestly or participated in an investigation, 
        cannot be the basis for any adverse action, including threats, harassment, or discrimination. If you work with 
        someone who has raised a concern or provided information in an investigation, you should continue to treat the 
        person with courtesy and respect. If you believe someone has retaliated against you, report the matter as 
        described by this Code. Honest reporting does not mean that you have to be right when you raise a concern; 
        you just have to believe that the information you are providing is accurate. 
      </p>
      <p>
        False reporting, especially when intended to retaliate or exclude, is itself a violation of this Code and 
        will not be accepted or tolerated.
      </p>
      <p>
        Everyone is encouraged to ask questions about this Code. Your feedback is welcome, and you will get a response within 
        three business days. Write to <a href="mailto:conduct@eclipse-foundation.org">conduct@eclipse-foundation.org</a>. 
      </p>

      <h2>Amendments</h2>
      <p>
        The Eclipse Foundation Board of Directors may amend this Code from time to time and may vary the procedures 
        it sets out where appropriate in a particular case.
      </p>
      <h3 id="attribution">Attribution</h3>
      <p>
        This Code was inspired by the <a href="https://www.contributor-covenant.org/">Contributor Covenant</a>, version 1.4, 
        available <a href="https://www.contributor-covenant.org/version/1/4/code-of-conduct/">here</a>.
      </p>

      <div class="margin-bottom-40">
        <hr class="margin-left-0" width="200">
        <ul class="list-unstyled">
          <li id="footnote1">[1] Capitalized terms used herein without definition shall have the meanings assigned to them in the Bylaws.</li>
        </ul>
      </div>

    </div>
  </div>
  <div id="rightcolumn">
    <div class="sideitem">
      <h6>Related Links</h6>
      <ul>
        <li><a href="https://raw.githubusercontent.com/eclipse/.github/master/CODE_OF_CONDUCT.md">Community Code of Conduct (Markdown)</a></li>
        <li><a href="/org/foundation/directors.php">Board of Directors</a></li>
        <li><a href="/membership/">Membership</a></li>
        <li><a href="/membership/become_a_member/">Become a member</a></li>
      </ul>
    </div>
  </div>
</div>