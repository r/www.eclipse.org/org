<?php
/**
 * Copyright (c) 2021 Eclipse Foundation.
 *
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which accompanies
 * this distribution, and is available at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *  Mike Milinkovich (Eclipse Foundation)
 *  Christopher Guindon (Eclipse Foundation)
 */
?>

<h1><?php print $pageTitle;?></h1>
<p>
  In addition to the rules below the 
  <a href="https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php">Eclipse Foundation Code of Conduct</a> 
  applies in all areas.
</p>
<h2>Purpose</h2>
<p>The purpose of this document is to:</p>
<ol>
  <li>
    Specify the expected behavior of all participants in Eclipse Foundation
    communication channels such as mailing lists, forums, code review tools, bug
    trackers, issue trackers, Zoom, Slack and Mattermost channels, and
    similar; and
  </li>
  <li>Specify the consequences of unacceptable behavior.</li>
</ol>
<h2>Spamming and Swarming</h2>
<p>
  Please keep your messages on topic for each channel, and please use the
  appropriate channel. Spamming channels with unsolicited or
  commercially-focused messages is not allowed. Frequent messages which result
  in unwarranted churn on a channel (sometimes referred to as
  &ldquo;swarming&rdquo;) is not allowed.
</p>
<p>
  Directly messaging users (unless the user has specifically requested the
  information) offering services, events, employment opportunities or freelance
  projects, is not allowed.
</p>
<h3>Unwanted Voice/Video Calling</h3>
<p>
  Abuse of the voice/video calling features of any of the available channels to
  call users without their permission is not allowed. Accidents sometimes
  happen, please immediately let the user know it was a mistake.
</p>
<h2>Interaction With Staff</h2>
<p>
  In addition to acting as the steward of the Eclipse community, the Eclipse
  Foundation is an employer. As such we are required by law to offer our
  employees a safe and harassment-free workplace. Any actions which are
  interpreted by a staff member as harassment are not allowed, and should they
  occur the Eclipse Foundation is required by law to take prompt and appropriate
  action.
</p>
<h2>Reporting an Issue</h2>
<p>
  Please send an email to
  <a href="mailto:codeofconduct@eclipse.org">codeofconduct@eclipse.org</a> 
  if you wish to report an issue under these guidelines.
</p>
<h2>Consequences of Unacceptable Behavior</h2>
<p>Unacceptable behavior from any community member will not be tolerated.</p>
<p>
  If a community member engages in unacceptable behavior, Eclipse Foundation
  staff may take any action deemed appropriate, up to and including a warning or
  temporary 30 day ban, along with a permanent ban for repeated or egregious
  violation of these guidelines. Unless otherwise determined by the EMO,
  consequences will escalate as follows:
</p>
<ol>
  <li>A warning.</li>
  <li>A temporary 30-day ban from all Eclipse Foundation channels.</li>
  <li>A permanent ban from all Eclipse Foundation channels.</li>
</ol>
