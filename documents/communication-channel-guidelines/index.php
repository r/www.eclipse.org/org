<?php
/**
 * Copyright (c) 2021 Eclipse Foundation.
 *
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which accompanies
 * this distribution, and is available at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *  Mike Milinkovich (Eclipse Foundation)
 *  Christopher Guindon (Eclipse Foundation)
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "Eclipse Foundation Communication Channel Guidelines v1.0";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("Communication Channel Guideline, Communication Guideline, Code of Conduct");
$Theme->setPageAuthor("Mike Milinkovich ");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();