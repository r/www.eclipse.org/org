<?php
/**
 * Copyright (c) 2005, 2017, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License 2.0 which accompanies
 * this distribution, and is available at http://eclipse.org/legal/epl-2.0
 *
 * Contributors:
 *  Mike Milinkovich (Eclipse Foundation)
 *  Christopher Guindon (Eclipse Foundation)
 *  Eric Poirier (Eclipse Foundation)
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ('src/FoundationDocument.class.php');
$App = new App();
$Nav = new Nav();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "Eclipse Foundation Governance Documents";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("about, documents, foundation, governance, bylaws, agreement");
$Theme->setPageAuthor("Mike Milinkovich");

$documents = array();
$doc = new FoundationDocument();
$doc->setTitle('Bylaws');
$doc->setDescription('The Bylaws lay out the basic rules of governance of the Eclipse Foundation.
The <a href="eclipse-foundation-be-bylaws-fr.pdf">French</a> language version is the legal binding version of the Eclipse Foundation Bylaws.
The <a href="eclipse-foundation-be-bylaws-en.pdf">English</a> translated version is provided as a convenience.');
$doc->setDescription('The Bylaws of our Delaware incorporated US organization can be found <a href="eclipse-foundation-us-bylaws.pdf">here</a>.');
$doc->setLink('eclipse-foundation-be-bylaws-en.pdf', 'English');
$doc->setLink('eclipse-foundation-be-bylaws-fr.pdf', 'Fran&ccedil;ais');

$documents['Governance Documents']['bylaws'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Membership Agreement');
$doc->setDescription('The Membership Agreement describes the rights and responsibilities for each class of member in the Eclipse Foundation.');
$doc->setDescription('The Membership Agreement for our Delaware incorporated US organization can be found <a href="eclipse-foundation-us-membership-agreement.pdf">here</a>.');
$doc->setLink('eclipse-foundation-membership-agreement.pdf?v=1.5');
$documents['Governance Documents']['membership_agreement'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Intellectual Property Policy');
$doc->setDescription('The IP Policy describes the policies and mechanisms that the Eclipse Foundation uses for accepting and licensing the intellectual property developed by Eclipse projects and specifications.');
$doc->setLink('Eclipse_IP_Policy.pdf');
$documents['Governance Documents']['intellectual_property_policy'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Antitrust Policy');
$doc->setDescription('The Antitrust Compliance Policy describes the obligations required from all Eclipse Foundation Members to ensure compliance with Antitrust laws.');
$doc->setLink('Eclipse_Antitrust_Policy.pdf');
$documents['Governance Documents']['antitrust_policy'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Development Process');
$doc->setDescription('The Eclipse Foundation Development Process (EDP) describes the fundamental rules for creating and governing projects at the Eclipse community.');
$doc->setLink('/projects/dev_process/');
$documents['Processes']['dev_process'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Specification Process');
$doc->setDescription('The Eclipse Foundation Specification Process (EFSP) describes the fundamental rules for creating and governing specifications by Eclipse working groups and specification projects.');
$doc->setLink('/projects/efsp/');
$documents['Processes']['specification_process'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Working Group Process');
$doc->setDescription('The Eclipse Foundation Working Group Process describes the fundamental rules for creating and governing working groups at the Eclipse community.');
$doc->setLink('/org/workinggroups/process.php');
$documents['Processes']['working_group_process'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Interest Group Process');
$doc->setDescription('The Eclipse Foundation Interest Group Process describes the fundamental rules for creating and governing interest groups at the Eclipse community.');
$doc->setLink('/org/collaborations/interest-groups/process.php');
$documents['Processes']['interest_group_process'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Community Code of Conduct');
$doc->setDescription('The Eclipse community has a culture that embraces and respects diversity.');
$doc->setLink('/org/documents/Community_Code_of_Conduct.php');
$documents['Additional Policies']['coc'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Conduct Committee Charter');
$doc->setDescription('This charter defines the scope and governance of the Eclipse Foundation Conduct Committee.');
$doc->setLink('eclipse-foundation-conduct-committee-charter.pdf');
$documents['Additional Policies']['ccc'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Communication Channel Guidelines');
$doc->setDescription('Specify the expected behavior of all participants in Eclipse Foundation communication channels.');
$doc->setLink('/org/documents/communication-channel-guidelines/');
$documents['Additional Policies']['communication_channel_guidelines'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Social Media Guidelines');
$doc->setDescription('These guidelines are intended to provide guidance to the many community members who represent our projects, working groups, and events through various social media channels.');
$doc->setLink('/org/documents/social_media_guidelines.php');
$documents['Additional Policies']['social_media_guidelines'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Internal Rules');
$doc->setDescription('The purpose of these Internal Rules is to further supplement the Bylaws of the Eclipse Foundation AISBL with a view to ensure that it is equipped with the necessary tools and processes to pursue and fulfil its Purposes.');
$doc->setLink('ef-be-internal-rules.pdf');
$documents['Additional Policies']['ef-aisbl-internal-rules'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Affiliates Membership Guidelines');
$doc->setDescription('These guidelines provide an outline of the benefits, responsibilities and rights of affiliate companies related to the participation of affiliated members.');
$doc->setLink('eclipse_affiliates_membership_guidelines.pdf');
$documents['Additional Policies']['affiliates_membership_guidelines'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Eclipse Foundation Security Policy');
$doc->setDescription('This policy sets forth the general principles under which the Eclipse Foundation manages the reporting, management, discussion, and disclosure of security vulnerabilities discovered in Eclipse software.');
$doc->setLink('/security/policy.php');
$documents['Additional Policies']['efsp'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Third-Party Dependencies');
$doc->setDescription('This document contains the guidelines for the review of third-party dependencies used by Eclipse Foundation open source projects.');
$doc->setLink('Eclipse_Policy_and_Procedure_for_3rd_Party_Dependencies_Final.pdf');
$documents['Additional Policies']['tpd'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Implementing the IP Policy');
$doc->setDescription('This document describes how the Eclipse Management Organization  (EMO) seeks Board approval for the use of open source licenses, or can seek special approval for a third party component which has failed the normal IP due diligence.');
$doc->setLink('implementing_the_ip_policy.pdf');
$documents['Additional Policies']['iipp'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Licensing Example Code');
$doc->setDescription('This policy describes how projects may license their example code.');
$doc->setLink('Licensing_Example_Code.pdf');
$documents['Additional Policies']['lec'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Using Proprietary Tools');
$doc->setDescription('This document outlines the policies and procedures for projects using proprietary tools in their development process.');
$doc->setLink('Eclipse_Using_Proprietary_Tools_Final.php');
$documents['Additional Policies']['upt'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Standards and Specifications Policy');
$doc->setDescription('This document describes how the Eclipse Foundation and Eclipse projects interact with standards and specifications organizations.');
$doc->setLink('Eclipse_SpecOrgs_final.pdf');
$documents['Additional Policies']['ssp'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Recognizing Contributions on Project Web Sites');
$doc->setDescription('This policy details how projects can recognize organizations who make significant contributions to them. ');
$doc->setLink('Recognizing_Project_Contributions_Policy_2008.pdf');
$documents['Additional Policies']['rcpws'] = $doc;

$doc = new FoundationDocument();
$doc->setTitle('Hosted Services Privacy and Acceptable Usage Policy');
$doc->setDescription('This policy specifies the obligations of projects which have virtual servers which may collect personally identifiable information. ');
$doc->setLink('eclipse-foundation-hosted-services-privacy-and-acceptable-usage-policy.pdf');
$documents['Additional Policies']['hspaup'] = $doc;

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setHtml($html);
$Theme->generatePage();
