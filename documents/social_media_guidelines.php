<?php
/**
 * Copyright (c) 2015, 2019 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Mike Milinkovic (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation) - Update to the latest Contributor Covenant v1.4
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

$App = new App();
$Nav = new Nav();
$Menu = new Menu();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());
//
// Begin: page-specific settings. Change these.
$pageTitle = "Social Media Guidelines";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("social media, guidelines, documents, board, resolution, foundation");
$Theme->setPageAuthor("Eclipse Foundation");

// Place your html content in a file called content/en_pagename.php
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setNav($Nav);
$Theme->setMenu($Menu);
$Theme->setHtml($html);
$Theme->generatePage();
