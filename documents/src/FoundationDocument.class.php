<?php

/**
 * FoundationDocument()
 *
 * @author chrisguindon
 */
class FoundationDocument {
  public $title = "";

  public $description = "";

  public $links = array();

  public function setTitle($title) {
    $this->title = $title;
  }

  public function getTitle() {
    return $this->title;
  }

  public function setDescription($description) {
    $this->description[] = $description;
  }

  public function getDescription() {
    return implode('<br/><br/>', $this->description);
  }

  public function setLink($url, $text = 'Read more') {
    $this->links[] = array(
      'url' => $url,
      'text' => $text
    );
  }

  public function getLink() {
    $links = array();
    if (!empty($this->links)) {
      foreach ($this->links as $link) {
        $links[] = '<a href="' . $link['url'] . '" class="btn-sm btn  btn-primary" style="width:100%; font-size:12px;">' . $link['text'] . '</a>';
      }
    }
    return implode('<br/><br/>', $links);
  }

  public function getTableRow() {
    $html = '<tr><th scope="row">' . $this->getTitle() . '</th><td>' . $this->getDescription() . '</td>';
    $html .= '<td class="text-center">' . $this->getLink() . '</td>';
    $html .= '</tr>';
    return $html;
  }
}