<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Eric Poirier (Eclipse Foundation)
 *     Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$Theme = $App->getThemeClass();

$pageTitle = "The Open Innovation Community";

ob_start();
?>
<div class="jumbotron featured-jumbotron featured-jumbotron-astro margin-bottom-0">
  <div class="container">
    <div class="row">
      <div class="col-md-20 col-sm-18">
        <h1>The Home of Entrepreneurial Open Source</h1>
        <p>Community driven. <br>Code first. <br> <span class="text-primary">Commercially-friendly.</span></p>
      </div>
    </div>
  </div>
</div>
<?php
$extra_header_html = ob_get_clean();
$Theme->setExtraHeaderHtml($extra_header_html);

$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("open source, business, technology, strategy, operational value, strategic value, financial value");
$Theme->setTitle("Open Innovation Platform - Business Value");

ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->resetAttributes('main-container', 'class');
$Theme->setThemeVariables(array('main_container_classes' => '')); // Remove the container class from the main-container
$Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');
$Theme->setExtraHeaders('<script src="/org/value/public/js/main.js"></script>');
$Theme->setExtraHeaders('<link href="/org/value/public/css/value.css" media="screen" rel="stylesheet" type="text/css"/>');
$Theme->preventCaching();
$Theme->generatePage();
