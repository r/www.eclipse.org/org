<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>
<p>Innovation drives business sustainability and profitability. Open innovation offers firms the opportunity
to serve customers better, increase business agility and extend value creation beyond traditional
organizational boundaries by participating in an ecosystem of projects and communities. </p>

<h2>Why The Eclipse Foundation?</h2>
<ul>
  <li>Learn from and collaborate with industry-leading innovators through <a href="/org/workinggroups/">Eclipse Working Groups</a></li>
  <li>Gain market access through pre-competitive partnerships</li>
  <li>Mitigate risk of use and participation due to rigorous IP management</li>
  <li>Reduce vendor lock-in through code access and managed compatibility</li>
  <li>Amplify your brand by co-branding with the Eclipse Foundation</li>
</ul>

<div class="block-box">
  <h6>Related Links</h6>
  <ul>
    <li><a href="/org/value">Business Value</a></li>
    <li><a href="operational.php">Operational value</a></li>
  </ul>
</div>
