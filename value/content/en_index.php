<?php

/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>


<div class="container">
  <div class="row">
    <div class="col-sm-16 padding-top-60 padding-bottom-60">
      <p class="big">The Eclipse Foundation provides a proven governance framework and processes for entrepreneurial collaboration on sustainable, commercializable open source software that benefits developers, companies, and users alike.</p>
    </div>
    <div class="col-sm-8 padding-top-60 padding-bottom-60">
      <p class="big text-center"><a class="btn btn-primary btn-lg padding-right-60 padding-left-60 margin-top-20" href="https://accounts.eclipse.org/contact/membership">Join Us</a></p>
    </div>
  </div>
</div>


<div class="background-primary info-section" style="overflow-x: hidden; min-height:350px">
  <div class="container">
    <div class="row">
      <div class="col-sm-24 col-md-9 padding-top-20">
        <h2 class="fw-600"> Why The Eclipse Foundation? </h2>
        <p>Our approach to open innovation offers companies the opportunity to serve customers better, increase business agility and extend value creation beyond traditional organizational boundaries by participating in an ecosystem of projects and communities.</p>
      </div>
      <div class="highlighted-story col-xs-24 col-sm-8 col-md-5" style="background-color: white; min-height:350px">
        <div class="row padding-bottom-5">
          <div class="match-height-item-by-row col-xs-24 padding-20">
            <a class="success-stories-download-link" aria-label="Download eBook for How Open Source Software Drives Innovation" href="https://outreach.eclipse.foundation/innovation-open-source-ebook">
              <img class="img img-responsive success-story" alt="How Open Source Software Drives Innovation eBook" src="/org/value/public/images/accelerating-innovation-through-open-source.jpg">
            </a>
            <a class="btn btn-white success-stories-download padding-top-10 padding-bottom-10" aria-label="Download eBook for How Open Source Software Drives Innovation" href="https://outreach.eclipse.foundation/innovation-open-source-ebook">
              Download
            </a>
          </div>
        </div>
      </div>
      <div class="highlighted-story col-xs-24 col-sm-8 col-md-5" style="background-color: white; min-height:350px">
        <div class="row padding-bottom-5">
          <div class="match-height-item-by-row col-xs-24 padding-20">
            <a class="success-stories-download-link" aria-label="Download eBook for How Open Source Working Groups Drive Collaboration and Innovation" href="https://outreach.eclipse.foundation/working-groups-open-source-ebook">
              <img class="img img-responsive success-story" alt="How Open Source Working Groups Drive Collaboration and Innovation eBook" src="/org/value/public/images/ebook-oepn-source-wg-collaboration.jpg">
            </a>
            <a class="btn btn-white success-stories-download padding-top-10 padding-bottom-10" aria-label="Download eBook for How Open Source Working Groups Drive Collaboration and Innovation" href="https://outreach.eclipse.foundation/working-groups-open-source-ebook">
              Download
            </a>
          </div>
        </div>
      </div>
      <div class="highlighted-story col-xs-24 col-sm-8 col-md-5" style="background-color: white; min-height:350px">
        <div class="row padding-bottom-5">
          <div class="match-height-item-by-row col-xs-24 padding-20">
            <a class="success-stories-download-link" aria-label="Download eBook for Open Source Community Participation Drives Business Success" href="https://outreach.eclipse.foundation/participate-open-source-ebook">
              <img class="img img-responsive success-story" alt="Open Source Community Participation Drives Business Success eBook" src="/org/value/public/images/participate-open-source-ebook.jpg">
            </a>
            <a class="btn btn-white success-stories-download padding-top-10 padding-bottom-10" aria-label="Download eBook for Open Source Community Participation Drives Business Success" href="https://outreach.eclipse.foundation/participate-open-source-ebook">
              Download
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="success_stories" class="container padding-bottom-60 padding-top-40">
  <h2 class="margin-top-40 margin-bottom-40 brand-primary success-stories-title">Success Stories</h2>
  <div class="row">
    <div class="col-sm-24">
      <p class="margin-bottom-30 big">Businesses of all sizes in various industries are benefiting from open source projects hosted at the Eclipse Foundation and participation in Eclipse Foundation working groups.
        <br><br>Discover how companies are leveraging the business value of open source:
      </p>
    </div>
  </div>
  <div class="row margin-top-20 margin-bottom-60">
    <div class="newsroom-resources margin-top-40 margin-bottom-20 padding-left-20 padding-right-20 text-center flex-center gap-30" data-res-wg="eclipse_org" data-res-title="" data-res-type="case_study" data-res-template="image" data-res-limit="4" data-res-class="card-item card-item-success"></div>
  </div>
</div>


<div class="background-secondary text-center nodes-bg-left padding-bottom-60 padding-top-40">
  <div class="container">

    <h2 class="margin-top-40 margin-bottom-40 brand-primary">Industry Expert Testimonials</h2>

    <div class="row">
      <div class="col-sm-18 col-sm-offset-3">
        <p class="text-center margin-bottom-30 big">Learn what industry experts have to say about how the Eclipse Foundation is helping businesses accelerate innovation, drive industry adoption, and grow community.</p>
      </div>
    </div>

    <div class="row margin-top-20 margin-bottom-60">
      <div class="col-sm-8">
        <a class="eclipsefdn-video" aria-label="Youtube: Todd Moore from IBM gives insights on the Business Value of Open Source" href="https://youtu.be/BJ6goUKQjko"></a>
      </div>
      <div class="col-sm-8">
        <a class="eclipsefdn-video" aria-label="Youtube: Deborah Bryant from Red Hat shares her expertise on the Business Value of Open Source" href="https://www.youtube.com/watch?v=wSkPHuNAfhU"></a>
      </div>
      <div class="col-sm-8">
        <a class="eclipsefdn-video" aria-label="Youtube: Farah Papaioannou on the benefits of the Business Value of Open Source" href="https://www.youtube.com/watch?v=7sU4mOYCtuQ"></a>
      </div>
    </div>

  </div>
</div>

<div class="container padding-top-50 padding-bottom-50">

  <div class="col-md-24 text-center">
    <a role="button" data-toggle="collapse" class="btn btn-primary padding-left-50 padding-right-50 width-250 padding-top-10 padding-bottom-10 fw-500" href="#moreResources" expanded="false" aria-controls="moreResources"> More Resources </a>
  </div>

  <div class="col-md-24 collapse margin-top-30" id="moreResources">

    <div class="row panel-group" id="accordion" role="tablist" aria-multiselectable="true">

      <div class="col-md-24 panel text-center">
        <a role="button" data-parent="#accordion" data-toggle="collapse" class="btn btn-default padding-left-50 padding-right-50 width-250 padding-top-10 padding-bottom-10 fw-500" href="#whitepapers" expanded="false" aria-controls="whitepapers"> White Papers </a>
        <div class="collapse panel-collapse" id="whitepapers">
          <div class="newsroom-resources margin-top-40 margin-bottom-20 text-center flex-center gap-60" data-res-wg="eclipse_org" data-res-title="" data-res-type="white_paper" data-res-template="image" data-res-class="card-item"></div>
        </div>
      </div>

      <div class="col-md-24 panel text-center">
        <a id="surveyReportsCtn" role="button" data-parent="#accordion" data-toggle="collapse" class="btn btn-grey padding-left-50 padding-right-50 width-250 padding-top-10 padding-bottom-10 fw-500 collapsed" href="#surveyReports" expanded="false" aria-controls="surveyReports"> Survey Reports </a>
        <div class="collapse panel-collapse" id="surveyReports">
          <div class="newsroom-resources margin-top-40 margin-bottom-20 text-center flex-center gap-60" data-res-wg="eclipse_org" data-res-title="" data-res-type="survey_report" data-res-template="image" data-res-class="card-item"></div>
        </div>
      </div>

      <div class="col-md-24 panel text-center">
        <a role="button" data-parent="#accordion" data-toggle="collapse" class="btn btn-secondary padding-left-20 padding-right-20 width-250 padding-top-10 padding-bottom-10 fw-500 collapsed" href="#additional" expanded="false" aria-controls="additional"> Additional Case Studies </a>
        <div class="collapse panel-collapse" id="additional">
          <div class="newsroom-resources margin-top-40 margin-bottom-20 text-center flex-center gap-60" data-res-wg="eclipse_org" data-res-title="" data-res-type="case_study" data-res-template="image" data-res-class="card-item" data-res-skip="4"></div>
        </div>
      </div>

      <div class="col-md-24 panel text-center">
        <a role="button" data-parent="#accordion" data-toggle="collapse" class="btn btn-secondary btn-black padding-left-20 padding-right-20 width-250 padding-top-10 padding-bottom-10 fw-500 collapsed" href="#podcast" expanded="false" aria-controls="podcast"> Podcast </a>
        <div class="collapse panel-collapse" id="podcast">
          <div class="match-height-item-by-row col-xs-24 col-md-24 padding-40">
            <div id='buzzsprout-large-player-1740291'></div>
            <script type='text/javascript' charset='utf-8' src='https://www.buzzsprout.com/1740291.js?container_id=buzzsprout-large-player-1740291&player=large'></script>
          </div>
        </div>
      </div>

    </div>

  </div>

</div>

<script>
  // Expand and show Survery Reports section, 
  // if user comes here by clicking "Survey reports" links in org/services/marketing/ page

  document.addEventListener('DOMContentLoaded', goToSurveryReports)

  function goToSurveryReports() {
    const currentLocation = window.location.href
    const surveyReportsIdExists = currentLocation.includes('#surveyReportsCtn')

    if (surveyReportsIdExists) {
      const moreResourcesCotent = document.getElementById('moreResources')
      moreResourcesCotent.classList.add('in')

      const surveyReportsContent = document.getElementById('surveyReports')
      surveyReportsContent.classList.add('in')

      const surveyReportsCtn = document.getElementById('surveyReportsCtn')
      surveyReportsCtn.scrollIntoView();
    }
  }
</script>
