<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>
<p>Open source technologies have emerged from the IT cost center to deliver value across the organization.
 Open source is increasingly recognized as driving broader business transformation.</p>


<h2>Why The Eclipse Foundation?</h2>
<ul>
  <li>Increase agility due to permissionless innovation fostered by <a href="/org/workinggroups/">Eclipse Working Groups</a></li>
  <li>Gain access to an international pool of technological specialists</li>
  <li>Allow your developers to tap into peers and commercially-oriented best practices</li>
  <li>Leverage Foundation event and channel marketing opportunities</li>
  <li>Improve talent acquisition and retention by participating in widely adopted projects</li>
</ul>

<div class="block-box">
  <h6>Related Links</h6>
  <ul>
    <li><a href="/org/value">Business Value</a></li>
    <li><a href="strategic.php">Strategic value</a></li>
  </ul>
</div>