<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<h1><?php print $pageTitle; ?></h1>
<p>The open source model is being widely adopted across all industry sectors to enable
  customer-centric innovation and accelerated growth. While originally the business case centered on
  lower total cost of ownership, many firms have embraced open source software as an integral part
  of their strategy to rapidly build differentiated products and drive digital transformation.</p>
<h2>Key Services - A Business Perspective</h2>

<p>Key value added services offered by the Foundation are <a href="https://www.eclipse.org/org/#IP_Management">Intellectual Property (IP) Management</a>,
<a href="https://www.eclipse.org/org/#Ecosystem">Ecosystem Development</a>, <a href="https://www.eclipse.org/org/#IT">IT Infrastructure</a>, <a href="https://www.eclipse.org/org/#Development">Development Community Support</a>.</p>

<h2>A Framework for Open Innovation</h2>

<p>The Eclipse Foundation helps leading technology innovators develop, deliver and sustain open source software which
solves real-world problems. We provide to our diverse group of member organizations services, governance frameworks,
mature processes, events and robust infrastructure, all of which is required to establish and nurture business-grade
open source ecosystems. We leverage our many years of experience working with thousands of developers around the
world to cultivate the significant community engagement required to deliver high quality, commercializable code.</p>

<h2>Enabling Industry Collaboration</h2>

<p>The Eclipse Foundation provides the services, tools and infrastructure for some of the world’s most innovative
companies to collaborate on solutions to real-world business problems. A shared infrastructure for
projects and businesses enables end users and vendors to engage in a transparent and neutral environment.
We serve a diverse community of business leaders, developers and end-users in an increasing number of
industry sectors. We have a strong track record of enabling our members to work together to deliver and
expand economic benefits across the value chain.</p>

<p>So which open source projects will deliver the best ROI for your organization? The Eclipse Foundation is a
focal point for projects with widespread market adoption in production deployments.
Joining the Eclipse Foundation in support of a project that is strategic to your business
demonstrates support and commitment, and further contributes to that project’s adoption and momentum.</p>
