/*!
 * Copyright (c) 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

(function (document) {
  document.addEventListener('DOMContentLoaded', function () {
    // Randomly select between jumbotron images.
    var img_root = '/org/value/public/images/jumbotron';
    var items = ['a', 'b', 'c'];
    var item = items[Math.floor(Math.random() * items.length)];
    var url =  img_root + "/" + item + ".jpg";

    var jumbotronElement = document.querySelector('.featured-jumbotron');
    if (!jumbotronElement) return

    jumbotronElement.setAttribute('style', "background-image:url("+url+")");

  });
})(document);
