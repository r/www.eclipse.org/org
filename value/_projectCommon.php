<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Eric Poirier (Eclipse Foundation)
 *     Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/breadcrumbs.class.php");

$App->Promotion = TRUE;

$Nav = new Nav();
$Nav->addNavSeparator("Innovate with Us", "");
$Nav->addCustomNav("Become a member", "/membership/become_a_member/", "_self", 1);
$Nav->addCustomNav("Discover Eclipse projects", "https://projects.eclipse.org", "_self", 1);
$Nav->addCustomNav("Start an Eclipse project", "/projects/handbook/#starting", "_self", 1);
$Nav->addCustomNav("Explore our Marketplace", "https://marketplace.eclipse.org", "_self", 1);
$Theme->setNav($Nav);

// Set breadcrumb for this section
$Breadcrumb = new Breadcrumb();
$Breadcrumb->insertCrumbAt(2, "Business Value", "/org/value/index.php", "_self");
$Theme->setBreadcrumb($Breadcrumb);
