module.exports = function(grunt) {
    // Initializing the configuration object
    grunt.initConfig({     
        // Task configuration  
        less: {
            development: {
                options: {
                    compress: true,
                    // minifying the result
                },
                files: {
                    // compiling styles.less into styles.css
                    "assets/css/main.min.css": "assets/less/main.less",
                }
            }
        },
        watch: {
            less: {
                files: ['assets/less/*.less', 'assets/less/**/*.less'],
                // watched files
                tasks: ['less'],
                // tasks to run
            },
        }
    });
    // Plugin loading
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    // Task definition
    grunt.registerTask('default', ['watch']);
};