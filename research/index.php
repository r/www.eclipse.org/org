<?php
/*******************************************************************************
 * Copyright (c) 2014, 2018 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");

$App = new App();
$Nav = new Nav();
$Menu = new Menu();
include ($App->getProjectCommon()); // All on the same line to unclutter the user's desktop'

// Custom theme variables
$variables = array();
$variables['main_container_classes'] = 'container-full';
$App->setThemeVariables($variables);

$pageTitle = "Research @ Eclipse Foundation";
$pageAuthor = "Eric Poirier";
$pageKeywords = "eclipse, research";

ob_start();
?>

<div class="site_container breadcrumbs-offset">
  <header class="header">
    <div class="container">
      <div class="row">
        <div class="col-sm-24">
          <h1><?php print $pageTitle; ?></h1>
        </div>
      </div>
    </div>
  </header>
  <!--  Section #1 -->
  <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-sm-18">
          <h2 class="margin-top-60">Business-friendly open source and open innovation underpins exploitation, community building and dissemination strategy for European projects</h2>
          <p>The Eclipse Foundation has been a partner in many publicly funded research projects since 2013. We help organizations to successfully create, publish, and sustain an open source
            software platform, making the results of the research projects available for commercial or public exploitation.
          </p>
        </div>
        <div class="col-sm-6 hidden-xs">
          <img style="margin-top: -5px;" src="assets/images/shape-4.png" class="img-responsive" width="196" height="173" class="img-responsive">
        </div>
      </div>
      <div class="row padding-bottom-30">
        <div class="col-sm-12">
          <p/>
          <p>The Eclipse Foundation welcomes the opportunity to work with new industry research projects. If you would like more information please contact us at <a href="mailto:research@eclipse.org">research@eclipse.org</a></p>
          <p/>
          <p>Learn more about both <a href=#Proj>current and successfully completed projects</a> where we have partnered.</p>
        </div>
        <div class="col-sm-12">
          <div class="blue_box margin-bottom-20">
            <h3 style="margin: 0 0 10px 0;">What we offer:</h3>
            <h3 style="display: inline-block; margin: 0 0 8px 0;">- Sustainability</h3>
            for research results (source code, documentation, …)<br />
            <h3 style="display: inline-block; margin: 0 0 8px 0;">- Guidance</h3>
            in creating and managing an open source project<br />
            <h3 style="display: inline-block; margin: 0 0 8px 0;">- Expertise</h3>
            in community building and support<br />
            <h3 style="display: inline-block; margin: 0 0 8px 0;">- Eclipse ecosystem</h3>
            visibility in, and interaction with the wider ecosystem<br />
            <h3 style="display: inline-block; margin: 0 0 8px 0;">- Open Collaboration</h3>
            best practices<br />
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--  Section #2 -->
  <section class="row-gray section">
    <div class="container">
      <h3>Funding organizations</h3>
      <div class="row ">
        <ul class="list-inline list-org text-center center-block">
          <li>
            <img src="assets/images/h2020.png" class="img-responsive"/>
          </li>
          <li >
            <img src="assets/images/itea3.png" class="img-responsive"/>
          </li>
          <li >
            <img src="assets/images/ecsel.png" class="img-responsive"/>
          </li>
          <li >
            <img src="assets/images/bundes.png" class="img-responsive"/>
          </li>
        </ul>
      </div>
    </div>
  </section>
  <!--  Section #3 -->
  <section class="section">
    <div class="container">
      <div class="row padding-top-40 padding-bottom-50">
        <div class="col-sm-14">
          <div class="wrapper">
            <h3>Open Source: the catalyst among academics, SMEs, and large organizations</h3>
            <p>Academics bring new innovations that enable better productivity and new processes in large organizations. SMEs industrialize the innovation transferred through open source projects to support
              a product-based or expertise-based business model.
            </p>
            <h3>Research Exploitation</h3>
            <p>The Eclipse Foundation experts enable the research team to create, evolve and maintain open source software projects that capture the results of the research.</p>
            <p>The proven processes of the Eclipse Foundation and services for IP management and community building assist the research partners throughout the lifecycle of the research project. They ensure that the
              open source code can be used at any given time for research or commercial exploitation.
            </p>
            <p>
              These processes are set out in the Eclipse Foundation governance model, the Eclipse Development Process and the Eclipse IP management practices. In addition, all code is made available under the <a href="https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html">Eclipse Public License</a>, a commercial-friendly Open Source license approved by the Open Source Initiative.
            </p>
          </div>
        </div>
        <div class="col-sm-10">
          <img style="margin-top: 30px;" src="assets/images/eclipse3.png" width="446" height="324" class="img-responsive">
        </div>
      </div>
    </div>
  </section>
  <!--  Section #4 -->
  <section class="row-gray section">
    <div class="container">
      <div style="position: relative">
        <div class="element">
          <img src="assets/images/shape-2.png" width="140" height="135" class="img-responsive">
        </div>
      </div>
      <div class="row padding-top-20 padding-bottom-30">
        <div class="col-sm-12">
          <h3>More exploitation opportunities</h3>
          <p>
            Exploitation is driven by availability, open governance, quality, and IP cleanliness. Providing the platform under an OSS license such as the <a
              href="https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html">Eclipse Public License</a> supports these goals very well. Users and adopters can trust the open governance model that
            provides a level playing field and commercial exploitation under this business-friendly open source license. The Eclipse IP process ensures that the IP created is tracked and properly managed.
          </p>
        </div>
        <div class="col-sm-12">
          <h3>Extensibility, sustainability and maturity</h3>
          <p>In some industries, platforms and tools must be available for a lifecycle of well over 20 years. With the experience of the Eclipse Foundation and the Eclipse ecosystem, it is possible to
            provide long-term support and design the platform in a way that allows for continuous and open innovation.
          </p>
          <p>Industrial applications require measurable indicators of project maturity. The Eclipse Foundation deploys assessment processes and tools in Polarsys to help members evaluate the maturity of
            Eclipse projects and to support projects in achieving higher maturity levels.
          </p>
        </div>
      </div>
    </div>
  </section>
  <!--  Section #5.1 -->
  <section class="section list-partners-projects-stories  padding-top-30">
    <div class="container">
      <div class="row">
        <div class="col-sm-18">
          <div class="wrapper">
            <h2 ID="Proj">The Eclipse Foundation Partners in these Projects</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <img src="assets/images/agail.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="http://eclipse.org/agail">AGILE - Eclipse Agail</a>
          </h3>
          <p>
            The Eclipse Agail is a language-agnostic, modular software and hardware gateway framework for the Internet of Things with support for protocol interoperability, device and data management, IoT
            application execution, trusted data sharing and external Cloud communication.
          </p>
          <p> <a href="http://eclipse.org/agail">Read more...</a>
          </p>
        </div>
      </div>
      <div class="row ">
        <div class="col-sm-6">
          <img src="assets/images/amass.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="https://www.polarsys.org/opencert/">AMASS - Eclipse OpenCert</a>
          </h3>
          <p>
            OpenCert is a customizable safety assurance and certification tool environment integrated into existing manufacturers’ development and safety assurance processes and tooling.
          </p>
          <p> <a
            href="https://www.polarsys.org/opencert/">Read more...</a>
          </p>
        </div>
      </div>
      <div class="row" style="vertical-align: middle;">
        <div class="col-sm-6">
          <img src="assets/images/crossminer.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="http://eclipse.org/crossmeter">CROSSMINER - Eclipse Crossmeter</a>
          </h3>
          <p>
            Software engineers spend most of their time learning to understand the software they maintain or depend on (or will depend on). The goal of this learning process is to support decision-making.
            In this project, we focus on the increasing dependence on open-source software (OSS) over the last years and the decisions related to depending on open-source software. Eclipse CROSSMETER will
            support the eﬃcient and eﬀective decision-making regarding dependence on OSS projects and components thereof. This entails both decisions on the architecture level (to decide to select and OSS
            project) and on the code level (to design the use of the OSS project). In particular, CROSSMETER will provide techniques and tools for extracting knowledge from existing open source components,
            and use such knowledge to properly select and reuse existing software to develop new systems. The activity of the developer will be continuously monitored in order to raise alerts related to the
            quality of the selected OSS projects and to give suggestions that can reduce the development eﬀort and increase the quality of the ﬁnal software products.
          </p>
          <p> <a
            href="http://eclipse.org/crossmeter">Read more...</a>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <img src="assets/images/robmosys.png" class="img-responsive center-block" />
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="https://robmosys.eu/">RobMoSys</a>
          </h3>
          <ul>
            <li>RobMoSys envisions an integrated approach built on top of the current code-centric robotic platforms, by applying model-driven methods and tools.</li>
            <li>RobMoSys will enable the management of the interfaces between different robotics-related domains in an efficient and systematic way according to each system’s needs.</li>
            <li>RobMoSys aims to establish Quality-of-Service properties, enabling a composition-oriented approach while preserving modularity.</li>
            <li>RobMoSys will drive the non-competitive part of building a professional quality ecosystem by encouraging the community involvement.</li>
            <li>RobMoSys will elaborate many of the common robot functionalities based on broad involvement of the community via two Open Calls.</li>
          </ul>
          <p><a href="https://robmosys.eu/">Read more...</a></p>
        </div>
      </div>
      <div class="row" style="vertical-align: middle;">
        <div class="col-sm-6">
          <img src="assets/images/kuksa.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="http://eclipse.org/kuksa">APPSTACLE - Eclipse Kuksa</a>
          </h3>
          <p>Because today's software-intensive automotive systems are still developed in silos by each car manufacturer or OEM in-house, long-term challenges in the industry are yet unresolved.
            Establishing a standard for car-to-cloud scenarios significantly improves comprehensive domain-related development activities and opens the market to external applications, service provider, and
            the use of open source software wherever possible without compromising security. Connectivity, OTA maintenance, automated driving, electric mobility, and related approaches increasingly demand
            technical innovations applicable across automotive players.
          </p>
          <p>
            The open and secure Eclipse Kuksa project will contain a cloud platform that interconnects a wide range of vehicles to the cloud via in-car and internet connections. This platform will be
            supported by an integrated open source software development environment including technologies to cope especially with software challenges for vehicles designed in the IoT, Cloud, and digital
            era.
          </p>
          <p> <a href="http://eclipse.org/kuksa">Read more...</a>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <img src="assets/images/basyx.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="https://eclipse.org/basyx">BaSys - Eclipse BaSyx</a>
          </h3>
          <p>
            The goal of the BaSys project is to provide virtual middleware for industrial automation which implements Industry 4.0 concepts leading to efficient changeability and adaptability in production processes. The platform will accomplish this by enabling the networking and integration of existing technologies such as OPC-UA and oneM2M.
          </p>
          <p> <a
            href="https://eclipse.org/basyx">Read more...</a>
          </p>
        </div>
      </div>
      <div class="row" style="vertical-align: middle;">
        <div class="col-sm-6">
          <img src="assets/images/brain.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="http://www.brain-iot.eu/">BRAIN-IoT</a>
          </h3>
          <p>
            BRAIN-IoT looks at heterogeneous IoT scenarios where instances of IoT architectures can be built dynamically combining and federating a distributed set of IoT services, IoT platforms and other
            enabling functionalities made available in marketplaces and accessible by means of open and standard IoT APIs and protocols.
          </p>
          <p><a href="http://www.brain-iot.eu/">Read more...</a></p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <img src="assets/images/pdp4e.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>PDP4E</h3>
          <p>PDP4E is an innovation action that will provide software and system engineers with methods and software tools to systematically apply data protection principles in the projects they carry out,
            so that the products they create comply with the General Data Protection Regulation (GDPR), thus bringing the principles of Privacy and Data Protection by Design to practice.
          </p>
          <p>PDP4E will integrate privacy and data protection engineering functionalities into existent, mainstream software tools that are already in use by engineers, focusing on open-source tools that
            will be integrated in the Eclipse ecosystem, The approach will integrate methods proposed by the privacy engineering community (e.g. LINDDUN, ISO/IEC 27550 Privacy engineering), and the industry
            of software and system engineering tools (e.g. MUSE, PAPYRUS or OpenCert) using a model driven engineering approach. PDP4E will introduce privacy and data protection into software and system
            engineering disciplines (Risk Management, Requirements Engineering, Model-Driven Design, and Assurance), which drive the everyday activities of engineers.
          </p>
        </div>
      </div>
    </div>
  </section>
  <!--  Section #6.1 -->
  <section class="section row-gray list-partners-projects-stories padding-top-20">
    <div class="container">
      <div class="row">
        <div class="col-sm-18">
          <div class="wrapper">
            <h2>Success stories</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <img src="assets/images/app4mc.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="https://www.eclipse.org/app4mc/">Amalthea4Public - Eclipse APP4MC</a>
          </h3>
          <p>
            The Eclipse Foundation is a partner in the Amalthea4Public project. The mandate of the Foundation was to manage the dissemination and exploitation activities of the project. This included
            mentoring and providing support for:
          </p>
          <ul>
            <li> Creating Eclipse projects for Amalthea results</li>
            <li>Providing means for collaboration
            <li>and hosting the web presence including information, downloads and community</li>
          </ul>
          <p>
            <a href="https://www.eclipse.org/app4mc/">
            Read more...
            </a>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <img src="assets/images/polarsys.png" class="img-responsive center-block"/>
        </div>
        <div class="col-sm-18">
          <h3>
            <a href="https://www.polarsys.org/">OPEES - Polarsys</a>
          </h3>
          <p>One of the outstanding results of OPEES is the creation of PolarSys, the Eclipse Working Group for open source tools for embedded systems.</p>
          <p>
            The growing PolarSys community of members includes Airbus, CEA LIST, Ericsson, Thales, and several SMEs. Projects provide open source solutions for systems engineering, from requirements
            engineering to verification and validation, including development and debug.
          </p>
          <p><a href="https://www.polarsys.org/">Read more...</a></p>
        </div>
      </div>
    </div>
  </section>
  <!--  Section #end -->
  <section class="end section">
    <div class="container  contact-us">
      <div class="row">
        <div class="col-sm-18">
          <h3>Contact Us</h3>
          <p>
            The Eclipse Foundation welcomes the opportunity to work with new industry research projects. We specialize in creating open collaborative development projects and the dissemination of the technology to
            the larger community. If you would like more information please contact us at <a href="mailto:research@eclipse.org">research@eclipse.org</a>
          </p>
        </div>
      </div>
    </div>
  </section>
</div>

<?php
$html = ob_get_clean();
$App->AddExtraHtmlHeader('<link type="text/css" href="assets/css/main.min.css" rel="stylesheet"/>');

$App->generatePage($theme, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);