<?php
/**
 * Copyright (c) 2022 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Eric Poirier (Eclipse Foundation)
 *     Christopher Guindon (Eclipse Foundation)
 *     Zachary Sabourin (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$Nav = new Nav();

// Removed until more IG content is available

// $Nav->addNavSeparator("Eclipse Interest Groups", "");

// $Nav->addNavSeparator("Related Links", "");
// $Nav->addCustomNav("Interest Group Process", "/org/interest-groups/process.php", "_self", 1);

// $Theme->setNav($Nav);
