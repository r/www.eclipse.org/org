<div>
  <p>Version 1.0</p>
  <p>Table of Contents</p>

  <ul>
    <li>
      <a href="#ig-governance">Governance</a>
      <ul>
        <li><a href="#ig-governance-docs">Related Governance Documents</a></li>
      </ul>
    </li>
    <li><a href="#ig-principles">Guiding Principles</a></li>
    <li><a href="#ig-relation">Relationship to Projects</a></li>
    <li><a href="#ig-participation">How to Participate</a></li>
    <li><a href="#ig-naming">Naming and Trademarks</a></li>
    <li><a href="#ig-scope">Scope</a></li>
    <li><a href="#ig-materials">Materials</a></li>
    <li><a href="#ig-leadership">Leadership</a></li>
    <li><a href="#ig-services">Services and Shared Resources</a></li>
    <li>
      <a href="#ig-lifecycle">Interest Group Lifecycle</a>
      <ul>
        <li><a href="#ig-pre-proposal">Pre-Proposal Phase</a></li>
        <li><a href="#ig-proposal">Proposal Phase</a></li>
        <li><a href="#ig-operational">Operational Phase</a></li>
        <li><a href="#ig-archived">Archived Phase</a></li>
      </ul>
    </li>
    <li><a href="#ig-modifying">Modifying This Process</a></li>
    <li><a href="#ig-exceptions">Exceptions</a></li>
  </ul>

  <p>
    This document describes the process for creating and managing <i>Eclipse Foundation Interest
    Groups</i> ("Interest Groups”). In particular it describes how the Eclipse Foundation member
    organizations ("Members"), which includes Strategic, Contributing, and Associate members, can
    lead, influence, and collaborate within Interest Groups.
  </p>
  <p>
    Interest Groups are established to facilitate the collaboration between participating Eclipse
    Members and to drive shared innovation, and all participating Members may contribute to the
    initiative.
  </p>
</div>

<div>
  <h2 id="ig-governance"><a href="#ig-governance">Governance</a></h2>

  <p>
    Eclipse Interest Groups are a light-weight association of a subset of the Members that share a
    common interest in a topic or domain (“Participating Members”). Eclipse Interest Groups inherit
    and rely upon the Eclipse Foundation’s overall governance, sufficient to enable the Participating
    Members to collaborate effectively. All participants in Interest Groups must comply with the
    agreements, policies and procedures of the Eclipse Foundation. In particular, and without
    limiting the foregoing, at all times, participants in all Eclipse Interest Groups must conform to the
    Eclipse Foundation’s Intellectual Property and Antitrust Policies.
  </p>
  <p>
    Each Participating Member will designate an employee, consultant, director, officer, or agent (a
    “Participant Representative”) to represent them in the Interest Group. Each Member is further
    welcome to designate other employees, consultants, directors, officers, or agents that may
    participate in the initiatives and activities of the Interest Group (“Participants”).
  </p>
  <p>
    Interest Groups may hold meetings, provided that those meetings are open to all Participating
    Members, that an agenda for each meeting is published in advance, and that minutes of the
    meeting are published on the Interest Group’s mailing list in a timely fashion.
  </p>
  <p>
    Interest Groups shall not form any committees, sub-committees, or sub-groups.
  </p>
  <p>
    The governance structure for Interest Groups is intentionally informal, with most actions taken
    by the Interest Group being done so collegially by Participants and based on lazy consensus.
    However, at any time an Interest Group wishes to formalize a decision, such a decision is to be
    taken by a vote of the Participant Representatives<a href="#footnote1"><sup>1</sup></a>, 
    excluding Participant Representatives of Associate Members<a href="#footnote2"><sup>2</sup></a>
    , of the Interest Group (the “Eligible Voters”). A vote of the Interest Group
    may be taken at any meeting provided that: a) notice of the vote is included on the agenda in
    advance, and b) at least half of the Eligible Voters are represented. Electronic votes may also
    be held on the Interest Group’s mailing list, provided that any vote remains open for a minimum
    period of 14 days and that a minimum of half of the Eligible Voters vote. All votes require a
    simple majority to pass; that is, the number of votes cast in favor must be greater than the
    number of votes cast against. Note that abstentions, while permitted, are not counted in the
    determination of the outcome of the vote.
  </p>
  <p>
    For purposes of interpretation within the context of the Bylaws, Interest Groups are deemed to
    be Working Groups as that term is defined in the Bylaws. Also, for purposes of clarity, Interest
    Groups are distinct from and operate differently than Working Group “Special Interest Groups”,
    as that term is used in the context of Working Groups.
  </p>

  <div>
    <h3 id="ig-governance-docs"><a href="#ig-governance-docs">Related Governance Documents</a></h3>

    <p>
      The following documents define the governance of the Eclipse Foundation in general, and
      Interest Groups.
    </p>

    <ul>
      <li><a href="/org/documents/eclipse-foundation-be-bylaws-en.pdf">Eclipse Foundation Bylaws</a></li>
      <li><a href="/org/interest-groups/process.php">Eclipse Foundation Interest Group Process</a></li>
      <li><a href="/org/documents/eclipse-foundation-membership-agreement.pdf">Eclipse Foundation Membership Agreement</a></li>
      <li><a href="/org/documents/Eclipse_IP_Policy.pdf">Eclipse Foundation Intellectual Property Policy</a></li>
      <li><a href="/org/documents/Eclipse_Antitrust_Policy.pdf">Eclipse Foundation Antitrust Policy</a></li>
      <li><a href="/legal/privacy.php">Eclipse Foundation Privacy Policy</a></li>
      <li><a href="/org/documents/Community_Code_of_Conduct.php">Eclipse Foundation Code of Conduct</a></li>
      <li><a href="/org/documents/communication-channel-guidelines/">Eclipse Foundation Communication Channel Guidelines</a></li>
      <li><a href="/legal/logo_guidelines.php">Eclipse Foundation Trademark Usage Guidelines</a></li>
    </ul>

    <p>
      This Interest Group Process inherits its governance principles from the Eclipse Foundation
      Bylaws. Should any conflict exist between this Interest Group Process and the Bylaws, the
      Bylaws shall prevail. Further, in any cases where this Interest Group Process is silent on a
      particular topic, the Bylaws shall serve as guidance. In such cases, the Eclipse Foundation
      Management Organization (EMO) shall clarify, and provide any necessary guidance on how
      best to comply.
    </p>
  </div>

  <div>
    <hr align="left" width="200px">

    <p id="footnote1">
      <sup>1</sup> For voting purposes, a Participating Member may designate either an alternate Participant from their
      organization to represent their Participant Representative at any time, or to assign their vote via proxy to
      another Participant Representative.
    </p>

    <p id="footnote2">
      <sup>2</sup> Associate Members of Eclipse Foundation are non-voting members, as defined in the Bylaws.
    </p>
  </div>
</div>

<div>
  <h2 id="ig-principles"><a href="#ig-principles">Guiding Principles</a></h2>

  <p>
    All Interest Groups must operate in an open and transparent manner.
  </p>

  <p>
    <b>Open</b> - Everyone participates with the same rules; there are no rules to exclude any potential
    collaborators which include, of course, direct competitors in the marketplace.
  </p>

  <p>
    <b>Transparent</b> - Minutes, plans, and other artifacts are open and easily accessible to all.
  </p>

  <p>
    Interest Groups, like all communities associated with the Eclipse Foundation, must operate in
    adherence to the Foundation’s Code of Conduct.
  </p>
</div>

<div>
  <h2 id="ig-relation"><a href="#ig-relation">Relationship to Projects</a></h2>

  <p>
    Interest Groups may declare interest in Eclipse Foundation open source projects and specifications.
  </p>
</div>

<div>
  <h2 id="ig-participation"><a href="#ig-participation">How to Participate</a></h2>

  <p>
    Participation in an Interest Group is open to Strategic, Contributing, and Associate Members.
    Participation requires the Member to execute the <a href="/org/documents/eclipse-foundation-membership-agreement.pdf">Eclipse Foundation Membership Agreement</a>
    and <a href="/legal/committer_process/EclipseMemberCommitterAgreement.pdf">Eclipse Foundation Member Committer and Contributor Agreement</a> and to follow any other
    steps on how to participate that are defined from time to time by the EMO.
  </p>

  <p>
    There are no fees associated with participating in Interest Groups.
  </p>
</div>

<div>
  <h2 id="ig-naming"><a href="#ig-naming">Naming and Trademarks</a></h2>

  <p>
    Interest Groups must choose a name. The Eclipse Foundation retains ownership of the Interest
    Group’s name and related trademarks on behalf of the community.
  </p>
</div>

<div>
  <h2 id="ig-scope"><a href="#ig-scope">Scope</a></h2>

  <p>
    Interest Groups must have a defined Scope and all initiatives and activities are required to
    reside within that Scope. The initiatives and resulting materials developed by Participants
    should be in line with the Scope; initiatives and materials that are found to be outside the Scope
    of the Interest Group are to be avoided, and in extreme violations, may result in the termination
    of the Interest Group.
  </p>
</div>

<div>
  <h2 id="ig-materials"><a href="#ig-materials">Materials</a></h2>

  <p>
    Interest Groups must produce agendas and minutes for all meetings. Interest Groups may, at
    their discretion, produce artifacts such as documents, whitepapers, architectures, blueprints,
    diagrams, presentations, and the like; however, Interest Groups must not develop software,
    software documentation, or specifications.
  </p>
</div>

<div>
  <h2 id="ig-leadership"><a href="#ig-leadership">Leadership</a></h2>

  <p>
    Each Interest Group must have one or more designated Interest Group Leads (IGL). The IGL
    must ensure that the Interest Group:
  </p>

  <ul>
    <li>
      operates effectively by guiding the overall direction and by removing obstacles, solving
      problems, and resolving conflicts;
    </li>
    <li>
      operates using open source rules of engagement of transparency and open
      participation; and
    </li>
    <li>
      conforms to the Eclipse Foundation Intellectual Property and Antitrust Policies and
      associated processes.
    </li>
  </ul>
</div>

<div>
  <h2 id="ig-services"><a href="#ig-services">Services and Shared Resources</a></h2>

  <p>
    Interest Groups share resources, communications channels, and so on. These services and
    shared resources must provide for a level playing field to ensure all Participants have access to
    resources, and that the resources are maintained to the benefit of the Interest Group. Use of
    these shared resources must adhere to the principles of freedom of access for all Participants,
    and a freedom of action for those Participants.
  </p>
  <p>
    Each Interest Group is provided an Eclipse approved collaborative platform such as GitLab and
    an Eclipse Foundation-hosted public archived mailing list open to the participating Members of
    the Interest Group. All services and shared resources of Interest Groups must adhere to the
    Foundation’s Privacy Policy, Terms of Use, Code of Conduct, and other related policies.
  </p>
</div>

<div>
  <h2 id="ig-lifecycle"><a href="#ig-lifecycle">Interest Group Lifecycle</a></h2>

  <p>
    Following is a description of the lifecycle of an Interest Group:
  </p>

  <img src="assets/images/ig-lifecycle.png" alt="Interest Group Lifecycle" style="max-width: 350px;">

  <div>
    <h3 id="ig-pre-proposal"><a href="#ig-pre-proposal">Pre-Proposal Phase</a></h3>

    <p>
      Any Eclipse Foundation Strategic, Contributing, or Associate Member may initiate the creation
      of an Interest Group. To begin the process, the Member may submit a request using a process
      that will be defined by the EMO and updated from time to time.
    </p>

    <p>
      One or more Member organization(s) declares their interest in, and rationale for establishing an
      Interest Group. The EMO will assist such groups in the preparation of an Interest Group
      Proposal.
    </p>

    <p>
      The Pre-Proposal phase ends when the proposal is ready to be announced to the membership by the EMO.
    </p>
  </div>

  <div>
    <h3 id="ig-proposal"><a href="#ig-proposal">Proposal Phase</a></h3>

    <p>
      The proposing Member(s), in conjunction with the community, collaborate in public to enhance,
      refine, and clarify the proposal. This phase is intended to garner support and participation in the
      Interest Group in order to validate the viability of the proposed Interest Group, including to
      demonstrate there is sufficient interest by Members.
    </p>

    <p>
      The Proposal Phase ends with a Creation Review, or withdrawal. The proposal may be
      withdrawn by the proposing Member(s) at any point before the start of a Creation Review. The
      EMO will withdraw a proposal that has been inactive for more than six months.
    </p>

    <p>
      Successful completion of the Proposal Phase requires Executive Director approval.
    </p>

    <p>Success criteria:</p>

    <ul>
      <li>At least three members participating</li>
      <li>A successful creation review</li>
      <li>Executive Director approval</li>
    </ul>

    <p>Upon completion of this phase, the Interest Group moves into the Operational Phase.</p>
  </div>

  <div>
    <h3 id="ig-operational"><a href="#ig-operational">Operational Phase</a></h3>

    <p>
      During the Operational Phase, the Members involved in the Interest Group will work
      collaboratively on the various initiatives and materials that they collectively identify. This
      collaborative work must conform to this Interest Group Process, but otherwise Members are
      generally left to decide the specifics.
    </p>

    <p>
      Interest Groups must produce an annual activity report and pass an annual program review.
    </p>

    <p>
      Success criteria for the annual program review is as follows:
    </p>

    <ul>
      <li>Operating in an open and transparent manner</li>
      <li>Activity on the Interest Group mailing list</li>
      <li>At least three participating members</li>
      <li>Produce an annual activity report</li>
      <li>Pass an annual Program Review</li>
      <li>Executive Director approval to continue in the Operational Phase or move to Termination</li>
    </ul>
  </div>

  <div>
    <h3 id="ig-archived"><a href="#ig-archived">Archived Phase</a></h3>
  
    <p>
      At some point in time the operations of an Interest Group may need to be terminated. Two
      events may trigger the termination of an Interest Group: 1) following a super-majority
      (two-thirds) affirmative vote of the Members, the Interest Group requests that the Executive
      Director terminate the Interest Group; or 2) the Interest Group is deemed inactive,
      non-compliant, or otherwise not viable by the Executive Director at their sole discretion.
    </p>
    <p>
      The Eclipse Foundation will terminate an Interest Group by putting a public notice on the
      Interest Group’s communication channel. All materials created by the terminated Interest Group
      will be archived by the Eclipse Foundation.
    </p>
  </div>
</div>

<div>
  <h2 id="ig-modifying"><a href="#ig-modifying">Modifying This Process</a></h2>

  <p>
    The Eclipse founcation is responsible for maintaining this document and all changes must be approved by the Board of Directors.
  </p>
</div>

<div>
  <h2 id="ig-exceptions"><a href="#ig-exceptions">Exceptions</a></h2>
  
  <p>
    Exceptions to this Eclipse Foundation Interest Group Process may be granted at the sole discretion of the Executive Director.
  </p>
</div>